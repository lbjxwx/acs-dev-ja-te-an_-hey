package com.cn.sdk.demo;
import android.app.Activity;
import android.os.Bundle;


import androidx.appcompat.app.AppCompatActivity;

import android.os.Handler;
import android.os.RemoteException;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.cn.device.mqtt.sdk.aidl.IHTTPListener;
import com.cn.device.mqtt.sdk.aidl.IMqttServerListener;
import com.cn.sdk.mqtt.MqttManager;
import com.cn.sdk.mqtt.util.MD5Utils;

import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends Activity {
    private String TAG="txq-mqtt"+MainActivity.class.getSimpleName();
    private MqttManager mqttManager;
    private String sn="test000001";
    private TextView mTextView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mTextView=findViewById(R.id.txt_msg);
        mqttManager=MqttManager.getInstance(this);

        System.out.println("aaaaaaaaaaaaaaaaaaaa");
        mqttManager.start(new MqttManager.DeviceManagerListener() {
            @Override
            public int serviceEventNotify(int event) {
                System.out.println("aaaa serviceEventNotify " + event);
                if(event== MqttManager.DeviceManagerListener.EVENT_SERVICE_CONNECTED){
                    Log.e(TAG,"EVENT_SERVICE_CONNECTED");
                    sendMessage(null);
                    mqttManager.setMqttListener(sn,mListener);
                }
                else {
                    Log.e(TAG,"EVENT_SERVICE_DISCONNECTED");
                }
                return 0;
            }
        });
    }
    public void sendMessage(final String msg){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(msg==null){
                    mTextView.setText("");
                }else{
                    mTextView.append(msg+"\n");
                }
            }
        });
    }
    private Handler mHander=new Handler();
    private IMqttServerListener.Stub mListener=new IMqttServerListener.Stub() {
        @Override
        public void messageArrived(String topic,String msg) throws RemoteException {
            Log.e(TAG,"messageArrived"+topic+" "+msg);
            sendMessage("MQTT接收"+topic+" "+msg);
            if(topic.indexOf("DEVICE_INFO")!=-1){
                sendDeviceInfo();
            }else if(topic.indexOf("DEVICE_CONFIGURATION")!=-1){
                savePassRecord();
            }else{
                deviceConfig();
            }

        }

        @Override
        public void onConect(boolean isConnect) throws RemoteException {
            sendMessage("MQTT连接"+(isConnect?"成功":"失败"));
        }
    };
    private void deviceConfig(){
        JSONObject json=new JSONObject();
        try{
            json.put("sn",sn);
            json.put("version", "V1.1");

        }catch (JSONException e){
        }
        mqttManager.deviceConfig(json.toString(), new IHTTPListener.Stub(){
            @Override
            public void onNotify(int retCode, String retMsg) throws RemoteException {
                Log.e(TAG,"配置设备信息上报"+retCode+" "+(retMsg==null?"":retMsg));
                sendMessage("配置设备信息上报"+retCode+" "+(retMsg==null?"":retMsg));
            }
        });
    }
    private void savePassRecord(){
        JSONObject json=new JSONObject();
        try{
            json.put("sn",sn);
            json.put("deviceModel", MD5Utils.TripleMd5Encrypt(sn));
            json.put("healthCodeStatus", "1");
            json.put("temperature", "36.5");
            json.put("riskTemperature", "0");
            json.put("riskAreas", "0");
            json.put("transitTime", "2021-12-14 12:12:12");
            json.put("verifyAddress", "福州市马尾区");
            json.put("transitMethod", "31");
            json.put("rctCardType", "0x33");
            json.put("position", "119.27345,26.04769");

        }catch (JSONException e){
        }
        mqttManager.savePassRecord(json.toString(), new IHTTPListener.Stub(){
            @Override
            public void onNotify(int retCode, String retMsg) throws RemoteException {
                Log.e(TAG,"数据上报"+retCode+" "+(retMsg==null?"":retMsg));
                sendMessage("数据上报"+retCode+" "+(retMsg==null?"":retMsg));
            }
        });
    }
    private void sendDeviceInfo(){
        JSONObject json=new JSONObject();
        try{
            json.put("sn",sn);
            json.put("deviceModel", MD5Utils.TripleMd5Encrypt(sn));
            json.put("deviceMemory", "8909");
            json.put("systemVersion", "android9");
            json.put("versionNumber", "V1.1");
            json.put("softwareName", "V1.1");
            json.put("reportTime", "2021-12-14 12:12:12");
            json.put("deviceSetting", new JSONObject());
            json.put("syncTime", "2021-12-14 12:12:12");
            json.put("status", "0");

        }catch (JSONException e){
        }
        mqttManager.sendDeviceInfo(json.toString(), new IHTTPListener.Stub(){
            @Override
            public void onNotify(int retCode, String retMsg) throws RemoteException {
                Log.e(TAG,"设备信息上报"+retCode+" "+(retMsg==null?"":retMsg));
                sendMessage("设备信息上报"+retCode+" "+(retMsg==null?"":retMsg));
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        if(mqttManager!=null){
            mqttManager.stop();
        }
        super.onDestroy();
    }
    public void sendInfo(View view){
        mHander.postDelayed(new Runnable() {
            @Override
            public void run() {
                mqttManager.onPublish("DEVICE_INFO/"+sn,sn);
            }
        },500);
    }
    public void savePassRecord(View view){
        mHander.postDelayed(new Runnable() {
            @Override
            public void run() {
                mqttManager.onPublish("DEVICE_CONFIGURATION/"+sn,sn);
            }
        },500);
    }
    public void deviceConfig(View view){
        mHander.postDelayed(new Runnable() {
            @Override
            public void run() {
                mqttManager.onPublish("UPGRADE_SYSTEM/"+sn,sn);
            }
        },500);

    }
}
