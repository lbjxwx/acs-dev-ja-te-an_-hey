package com.nlp.cloundcbsappdemo.util;

import com.nlp.cloundcbsappdemo.model.responseHealth;

import java.util.Comparator;

public class sortClass implements Comparator {
    public int compare(Object arg0,Object arg1){
        responseHealth.HealthMsgBean.DataBean.HealthyReportBean user0 = (responseHealth.HealthMsgBean.DataBean.HealthyReportBean)arg0;

        responseHealth.HealthMsgBean.DataBean.HealthyReportBean user1 = (responseHealth.HealthMsgBean.DataBean.HealthyReportBean)arg1;

        int flag = user0.getDate().compareTo(user1.getDate());

        return flag;

    }
}
