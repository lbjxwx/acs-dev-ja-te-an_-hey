package com.nlp.cloundcbsappdemo.util;

import android.util.Log;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.nlp.cloundcbsappdemo.model.HealthyPersonEntity;

public class MessageDealUtil {
    static String TAG = "QZdemo";

    //核验接口结果处理
    public static String VerificationResult(String strResult) {
        String strName = "";//扫码人员名字
        String strIDNum = "";//扫码人员身份证号
        String strStatus = "";//防疫类型 ，返回数据
        String strXCJL = "";//行程记录

        //返回结果解析
        JSONObject jsonObjectResult = JSONObject.parseObject(strResult);
        Log.i(TAG, "jsonObjectResult:" + jsonObjectResult.toString());
        Log.i(TAG, "code:" + jsonObjectResult.getString("code"));

        jsonObjectResult = jsonObjectResult.getJSONObject("healthMsg");
        Log.i(TAG, "healthMsg:" + jsonObjectResult.toString());
        JSONObject jsonObjectHead = jsonObjectResult.getJSONObject("head");
        Log.i(TAG, "head:" + jsonObjectHead.toString());
        JSONObject jsonObjectDate = jsonObjectResult.getJSONObject("data");
        Log.i(TAG, "data:" + jsonObjectDate.toString());

        JSONObject jsonObjectReturn = new JSONObject();
        if (Integer.valueOf(jsonObjectHead.getString("status")) != 0) {
            //接口失败
            jsonObjectReturn.put("code", jsonObjectHead.getString("status"));
            jsonObjectReturn.put("msg", jsonObjectHead.getString("message"));
        } else {
            //结果转换 //接口成功
            jsonObjectReturn.put("code", "10000");
            jsonObjectReturn.put("msg", "成功");
            JSONObject jsonObjectPerson = jsonObjectDate.getJSONObject("person");
            Log.i(TAG, "person:" + jsonObjectPerson.toString());
            JSONObject jsonObjectWarn = jsonObjectDate.getJSONObject("yqWarn");
            Log.i(TAG, "yqWarn:" + jsonObjectWarn.toString());
            strName = jsonObjectPerson.getString("desensitizeName");
            strIDNum = jsonObjectPerson.getString("desensitizeCardId");
            //码状态
            strStatus = "00";//默认为绿码
            String strDateNew = "0000-00-00";//最小日期
            String strDate = "";//当前处理记录时间
            JSONObject jsonObjectReport = new JSONObject();
            JSONArray jsonObjectStatus = jsonObjectDate.getJSONArray("healthyReport");
            Log.i(TAG, "healthyReport:" + jsonObjectStatus.toString());//取出行程报告列表
            for (int i = 0; i < jsonObjectStatus.size(); i++) {
                jsonObjectReport = jsonObjectStatus.getJSONObject(i);//取出单个行程报告
                strDate = jsonObjectReport.getString("date");//当前处理记录时间
                //Log.i(TAG, "strDate = " + strDate + " strDateNew = " + strDateNew);
                if (strDate.compareTo(strDateNew) > 0) {
                    //当前处理日期最新，取最新报告
                    //Log.i(TAG, "if(strDate.compareTo(strDateNew) > 0)");
                    strDateNew = strDate;
                    strStatus = jsonObjectReport.getString("healthyStaus");//最新健康报告状态
                }
            }

            //行程记录
            JSONArray jsonObjectXCJL = jsonObjectDate.getJSONArray("xcgj");
            Log.i(TAG, "xcgj:" + jsonObjectXCJL.toString());
            if (jsonObjectXCJL.size() <= 0) strXCJL = "";
            else {
                for (int i = 0; i < jsonObjectXCJL.size(); i++) {
                    strXCJL += jsonObjectXCJL.getJSONObject(i).getString("stopoverCityName");
                    strXCJL += ",";
                }
                strXCJL = strXCJL.substring(0, strXCJL.length() - 1);
            }

            Log.i(TAG, "strName:" + strName);
            Log.i(TAG, "strIDNum:" + strIDNum);
            Log.i(TAG, "strStatus:" + strStatus);

            JSONObject jsonObjectDataRet = new JSONObject();
            jsonObjectDataRet.put("address", strXCJL);
            jsonObjectDataRet.put("date", "");
            jsonObjectDataRet.put("idCard", strIDNum);
            jsonObjectDataRet.put("name", strName);
            jsonObjectDataRet.put("qrCodeType", strStatus);

            jsonObjectReturn.put("data", jsonObjectDataRet);
        }
        return jsonObjectReturn.toString();
    }


    //核验接口结果处理-带核酸数据
    public static HealthyPersonEntity VerificationResultAll(String strResult) {
        String strName = "";//扫码人员名字
        String strIDNum = "";//扫码人员身份证号
        String strStatus = "";//防疫类型 ，返回数据
        String strXCJL = "";//行程记录
        String strHSJC = "";//核酸检测
        //返回结果解析
//        {
//            "healthMsg":{
//            "head":{
//                "status":"0",
//                        "message":"接口调用成功"
//            },
//            "data":{
//                "person":{
//                    "desensitizeCardId":"**************2930", "desensitizeName":"陈**"
//                },
//                "xcgj":[],
//                "hsjc":{
//                },
//                "healthyReport":[{
//                    "cdTimeVersion":1, "dataSource":"山东省", "date":1598821200000, "reason":"正常",
//                            "healthyStaus":"00", "healthyId":
//                    "137-fb10a463efdd87a823a030586ab530546d4be2836af0159284e1561a2086049a"
//                },
//                {
//                    "cdTimeVersion":4, "dataSource":"湖北省", "date":1582750800000, "healthyStaus":
//                    "00",
//                            "healthyId":
//                    "142-fb10a463efdd87a823a030586ab530546d4be2836af0159284e1561a2086049a"
//                },
//                {
//                    "cdTimeVersion":1, "dataSource":"四川省", "date":1626123600000, "reason":
//                    "无", "healthyStaus":"00",
//                        "healthyId":
//                    "151-fb10a463efdd87a823a030586ab530546d4be2836af0159284e1561a2086049a"
//                },
//                {
//                    "cdTimeVersion":3, "dataSource":"河北省", "date":1629061200000, "reason":
//                    "无", "isVaccinated":"00",
//                        "healthyStaus":"00", "healthyId":
//                    "113-fb10a463efdd87a823a030586ab530546d4be2836af0159284e1561a2086049a"
//                },
//                {
//                    "cdTimeVersion":29, "dataSource":"广东省", "date":1633208400000, "reason":
//                    "无", "isVaccinated":"12", "healthyStaus":"00",
//                        "healthyId":
//                    "144-fb10a463efdd87a823a030586ab530546d4be2836af0159284e1561a2086049a"
//                },
//                {
//                    "cdTimeVersion":3, "dataSource":"福建省", "date":1597784400000, "reason":
//                    "无", "healthyStaus":"00",
//                        "healthyId":
//                    "135-fb10a463efdd87a823a030586ab530546d4be2836af0159284e1561a2086049a"
//                }],
//                "yqReports":[],
//                "yqWarn":{
//                    "warnType":"无"
//                }
//            }
//        }
//        }

        JSONObject jsonObjectResult = JSONObject.parseObject(strResult);
        Log.i(TAG, "jsonObjectResult:" + jsonObjectResult.toString());
        Log.i(TAG, "code:" + jsonObjectResult.getString("code"));
        jsonObjectResult = jsonObjectResult.getJSONObject("healthMsg");
        Log.i(TAG, "healthMsg:" + jsonObjectResult.toString());
        JSONObject jsonObjectHead = jsonObjectResult.getJSONObject("head");
        Log.i(TAG, "head:" + jsonObjectHead.toString());
        JSONObject jsonObjectDate = jsonObjectResult.getJSONObject("data");
        Log.i(TAG, "data:" + jsonObjectDate.toString());
//        JSONObject jsonObjectReturn = new JSONObject();
        HealthyPersonEntity healthyPersonEntity = null;
        if (Integer.valueOf(jsonObjectHead.getString("status")) != 0) {
            //接口出错
            Log.i(TAG, "接口出错:返回结果");
            String errorMsg = jsonObjectHead.getString("msg");
            healthyPersonEntity = new HealthyPersonEntity(false, "", "", "", "", "", errorMsg);
            return healthyPersonEntity;
        }
        //接口成功
        JSONObject jsonObjectPerson = jsonObjectDate.getJSONObject("person");
        Log.i(TAG, "person:" + jsonObjectPerson.toString());
        JSONObject jsonObjectWarn = jsonObjectDate.getJSONObject("yqWarn");
        Log.i(TAG, "yqWarn:" + jsonObjectWarn.toString());
        strName = jsonObjectPerson.getString("desensitizeName");
        strIDNum = jsonObjectPerson.getString("desensitizeCardId");
        //码状态
        strStatus = "00";//默认为绿码
        String strDateNew = "0000-00-00";//最小日期
        String strDate = "";//当前处理记录时间
        JSONObject jsonObjectReport = new JSONObject();
        JSONArray jsonObjectStatus = jsonObjectDate.getJSONArray("healthyReport");
        Log.i(TAG, "healthyReport:" + jsonObjectStatus.toString());//取出行程报告列表
        for (int i = 0; i < jsonObjectStatus.size(); i++) {
            jsonObjectReport = jsonObjectStatus.getJSONObject(i);//取出单个行程报告
            strDate = jsonObjectReport.getString("date");//当前处理记录时间
            //Log.i(TAG, "strDate = " + strDate + " strDateNew = " + strDateNew);
            if (strDate.compareTo(strDateNew) > 0)//当前处理日期最新，取最新报告
            {
                //Log.i(TAG, "if(strDate.compareTo(strDateNew) > 0)");
                strDateNew = strDate;
                strStatus = jsonObjectReport.getString("healthyStaus");//最新健康报告状态
            }
        }
        //行程记录
        JSONArray jsonObjectXCJL = jsonObjectDate.getJSONArray("xcgj");
        Log.i(TAG, "xcgj:" + jsonObjectXCJL.toString());
        if (jsonObjectXCJL.size() <= 0) {
            strXCJL = "您14天内未曾到访过中高风险地区";
        } else {
            for (int i = 0; i < jsonObjectXCJL.size(); i++) {
                strXCJL += jsonObjectXCJL.getJSONObject(i).getString("stopoverCityName");
                strXCJL += ",";
            }
            strXCJL = strXCJL.substring(0, strXCJL.length() - 1);
        }
        //核酸检测
        JSONObject jsonObjectHSJC = jsonObjectDate.getJSONObject("hsjc");
        Log.i(TAG, "hsjc:" + jsonObjectHSJC.toString());
        if (jsonObjectHSJC.isEmpty()) {
            strHSJC += "";
        } else {
            strHSJC += "结果： " + jsonObjectHSJC.getString("hsjcjg") + "\n";
            strHSJC += "时间： " + jsonObjectHSJC.getString("hsjcsj") + "\n";
            strHSJC += "检测机构： " + jsonObjectHSJC.getString("hsjcjgmc") + "\n";
        }
        healthyPersonEntity = new HealthyPersonEntity(true, strHSJC, strXCJL, strIDNum,
                strStatus, strName, "解析成功");
        Log.i(TAG, "解析成功:返回结果 = " + healthyPersonEntity.toString());
        return healthyPersonEntity;
    }

}
