package com.nlp.cloundcbsappdemo.util;

public class Const {
    //测试服务器入口地址
    //public static String serverUrl= "https://open.digitalcitizen.com.cn:18888";//"https://anli.nlpublic.com:55474";
    //设备接入
    //public static String applyUrl="/tb/jkm/gb/v1/apply";//bsn申请地址
    //public static String healthByIdAndName="/tb/jkm/gb/v1/info/healthVerificationByIdAndName"; //身份信息获取健康信息接口
    //健康核验
    //public static String JkmQrDataUrl ="/tb/jkm/gb/v1/info/healthVerificationByQrCode";  //设备接入-健康二维码核验地址
    //public static String CtidQrDataUrl ="/jkm/tbgw/union/qrCode/validate";  //设备接入-身份二维码核验地址



    //国办健康码参数
    public static String serverUrl= "https://open.digitalcitizen.com.cn:18888";//"http://192.168.138.66:9600";
    public static String applyUrl="/tb/jkm/gb/v1/apply";//bsn申请地址
    public static String healthByIdAndName="/tb/jkm/gb/v1/info/healthVerificationByIdAndName"; //身份信息获取健康信息接口
    public static String JkmQrDataUrl ="/tb/jkm/gb/v1/info/healthVerificationByQrCode";  //设备接入-健康二维码核验地址
    public static String CtidQrDataUrl ="/jkm/tbgw/union/qrCode/validate";  //设备接入-身份二维码核验地址

    //蓉城通获取身份证信息参数
    public static final String MESSAGE_URL = "http://rcyktcard.fzsmk.cn:8092/epidemic/app/identity/userMessageByCardId";//url

    public static final String APP_KEY = "bdb2ca7a989641d49085a54dd4dc9daf";// 应用id
    public static final String APP_SECRET = "174d58f1a42e3feda6b02e1350efe684395a5759";// 应用密钥
    public static final String ENCRYPT_KEY = "D829954D2335CD47A31C41A774B934DC";// 服务端数据加密公钥(32位)
    public static final String ENCRYPT_IV = "43695068911831192233933895229435";// 服务端数据加密初始向量(32位)
    public static final String AES_KEY = "alkJF65jgCBJK284";//身份信息加密秘钥
    public static final String AES_IV = "hptpnyen6ngw3imq";//身份信息加密初始向量

    //闽政通获取通讯指令参数(初始化为测试平台)
    public static final String mzt_url_name = "/mztsfrz/dataset/AppSerController/invokeservice.do";
    public static final String MZT_URL = "https://mztapp.fujian.gov.cn:9089" + mzt_url_name;//url
    public static final String INVOKECALLER_CODE = "402881e47738893b01773961522b000c402881e47738893b";//调用方授权码
    public static String IMEI = "803358414207336448201931";//设备IMEI号，正式环境请用Const.strIMEI

    //TMS
    public static final String TMS_REPORT_DATA_URL = "http://fyxtng.fzsmk.cn:1443/epidemic/app/passRecord/savePassRecord";//数据上报url

    public static String strPosition = "";//定位信息
    public static String strSN = "0199999800000065";//设备sn
    public static String strIMEI = "0199999800000065";//设备IMEI

}
