package com.nlp.cloundcbsappdemo.util;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ImageFormat;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.YuvImage;

import java.io.ByteArrayOutputStream;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class Nv21Utils {

    private static Disposable disposable;

    public static void nv21ToBitmap(byte[] nv21, int width, int height, int rotate, OnToBitmapListener listener) {
        if (disposable != null) {
            disposable.dispose();
        }
        disposable = Observable.fromCallable(() -> {
            YuvImage yuvImage = new YuvImage(nv21, ImageFormat.NV21, width, height, null);
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            yuvImage.compressToJpeg(new Rect(0, 0, width, height), 85, stream);
            Bitmap bitmap = BitmapFactory.decodeByteArray(stream.toByteArray(), 0, stream.size());
            Matrix mat = new Matrix();
            mat.postRotate(-rotate);
            bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), mat, true);
            return bitmap;
        }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(bitmap -> {
                    if (listener != null) {
                        listener.onBitmap(bitmap);
                    }
                }, throwable -> {
                    throwable.printStackTrace();
                    if (listener != null) {
                        listener.onBitmap(null);
                    }
                });
    }

    public interface OnToBitmapListener{
        void onBitmap(Bitmap bitmap);
    }
}
