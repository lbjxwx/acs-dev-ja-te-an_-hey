package com.nlp.cloundcbsappdemo.viewmodel;

import android.content.Context;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;

import androidx.lifecycle.ViewModel;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.device.PageTool;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.model.HttpHeaders;
import com.nlp.cloundcbsappdemo.listener.HealthyMessageListener;
import com.nlp.cloundcbsappdemo.model.*;
import com.nlp.cloundcbsappdemo.net.jkmCallback;
import com.nlp.cloundcbsappdemo.util.Bussiness;
import com.nlp.cloundcbsappdemo.util.Const;
import com.sm4.Sm4Utils;
import com.ys.acsdev.util.SpUtils;

import org.json.JSONException;

import java.io.File;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;

import cn.anicert.polymerizeDevice.DeviceManager;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class MainViewModel extends ViewModel {
    private static final String TAG = "MainViewModel";

    public String gModSn = ""; //模组sn
    int MAX_RANGE = 9999;
    Bussiness bussiness = new Bussiness();
    String gBsn, gRandomNum;
    Context context;
    private String qrCodeValue; //识别到的码值
    private String identifyData;//身份证md5加密值
    private static final String PUBLIC_KEY_VER = "V1.0";//公钥版本
    private static final String PUBLIC_KEY =
            "3149B557D6E101D02D4FB3D67DEDB33DDE32484EC87EA4BDE5C250E7AFE164FE1068F7DA5C35D99317D92F6EEAE63CB153B442F69264D38FD4439EF2FCC28C62"; //生产公钥
    String result_IdentityChiper = "";//身份码核验第一包请求返回的密文
    private Handler handler = new Handler();

    public MainViewModel(Context context) {
        this.context = context;
    }

    public void initHealthyCodeManager(String port) {
        int code = initPort(port);
        if (listener != null) {
            listener.initHealthyStatues(code == 0, code);
        }
    }

    //0、初始化端口 成功返回模组sn，失败返回错误代码
    private int initPort(String port) {
        int index = 0;
        String acmDev = port;
        File acm = new File(acmDev);
        while (!acm.exists() && index < 101) {
            index++;
            acmDev = "/dev/ttyACM" + index;
            acm = new File(acmDev);
        }
        int res = DeviceManager.getInstance().init(context, acmDev);
        if (res == 0) {
            String modSn = DeviceManager.getInstance().getModSn();
            if (modSn.length() > 2) {
                gModSn = modSn;
            }
        }
        return res;
    }

    public void closePort() {
        DeviceManager.getInstance().close();
    }

    //请求头部加入网关校验密文（公用方法）
    private void makeHeader(String gwAuthinfo) {
        OkGo.getInstance()
                .addCommonHeaders(new HttpHeaders("checkdata", gwAuthinfo))
                .addCommonHeaders(new HttpHeaders("Content-Type", "application/json;charset=utf-8"));
        Log.e("makeHeader-checkdata", "花花说这里需要一个日志: " + gwAuthinfo);
    }

    //组装请求bsn数据实体（公用方法）
    public void requestBsn() {
        int rnd = ThreadLocalRandom.current().nextInt(1000, MAX_RANGE);
        String time = bussiness.getUnitTime();
        if (time.length() == 0) {
            Log.e(TAG, "获取unixtime错误");
            return;
        }
        Log.e(TAG, gModSn + "  " + time + " " + rnd);
        String gwAuthInfo = DeviceManager.getInstance().deviceToGWIdentityAuthInfo(gModSn, String.valueOf(rnd), time);
        if (!TextUtils.isEmpty(gwAuthInfo)) {
            requestBsn _requestBsn = new requestBsn();
            _requestBsn.setBizType(7003);
            requestBsn.BizDataBean _bizdatabean = new requestBsn.BizDataBean();
            _bizdatabean.setAuthMode("0x42");
            _requestBsn.setBizData(_bizdatabean);
            String body = JSON.toJSONString(_requestBsn);
            getBsn(gwAuthInfo, body, new jkmCallback<responseBsn>() {
                @Override
                public void onSuccess(String msg, responseBsn data) {
                    try {
                        gBsn = data.getBsn();
                        gRandomNum = data.getRandomNumber();
                        if (result_IdentityChiper.length() > 0) {
                            result_IdentityChiper = DeviceManager.getInstance().deIdentityChiperAndGetCheckData(
                                    result_IdentityChiper, PUBLIC_KEY, PUBLIC_KEY.length(), PUBLIC_KEY_VER, gRandomNum);
                            //backMessageToViewMain(new MessageEvent(JSON.toJSONString(data), 6));
                            getHealthReportByCtidQrCode();
                        } else {
                            backMessageToViewMain(new MessageEvent(JSON.toJSONString(data), 7));
                        }

                    } catch (Exception ex) {
                        Log.e(TAG, ex.getMessage());
                        backMessageToViewMain(new MessageEvent("执行状态 失败", 0));
                    }
                }

                @Override
                public void onFail(String code, String msg) {
                    backMessageToViewMain(new MessageEvent(msg, 2));
                }

                @Override
                public void onError(com.lzy.okgo.model.Response<String> response) {
                    super.onError(response);
                    backMessageToViewMain(new MessageEvent(PageTool.NETWORK_ERROR_MSG, 5));
                }
            });
        } else {
            backMessageToViewMain(new MessageEvent("生成网关鉴权密文失败", 0));
        }
    }

    //获取bsn（公用方法）
    public void getBsn(String gwAuthinfo, String body, jkmCallback jkmCallback) {
        String url = SpUtils.getGKUrl() + Const.applyUrl;
        makeHeader(gwAuthinfo);
        OkGo.post(url).tag(this).upJson(body).execute(jkmCallback);
    }

    //1、身份两要素请求健康信息-生成身份信息签名密文
    public void requestHealthByIdAndName(String name, String id) {
        int rnd = ThreadLocalRandom.current().nextInt(1000, MAX_RANGE);
        String gwAuthInfo = DeviceManager.getInstance().deviceToGWIdentityAuthInfo(gModSn, String.valueOf(rnd), bussiness.getUnitTime());
        JSONObject checkData = null;
        try {
            checkData = DeviceManager.getInstance().getIdentityCheckData(
                    //姓名
                    name,
                    //身份证号码
                    id,
                    //公钥
                    PUBLIC_KEY,
                    //公钥版本号
                    PUBLIC_KEY_VER,
                    //服务端返回的随机数
                    gRandomNum
            );
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (gwAuthInfo == null || checkData == null) {
            backMessageToViewMain(new MessageEvent("身份信息校验失败", 0));
        } else {
            getHealthReportByIdAndName(gwAuthInfo, checkData);
        }
    }

    //1.1 组装身份两要素请求健康信息body
    public void getHealthReportByIdAndName(String gwAuthInfo, JSONObject checkData) {
        requestHealthByIdAndName _reqHealth = new requestHealthByIdAndName();
        _reqHealth.setRequestIdentityData("jkbg,xcgj,hsjc");//健康报告，行程轨迹，核酸检测
        _reqHealth.setBizType("7003");
        _reqHealth.setBsn(gBsn);
        requestHealthByIdAndName.BizDataBean _bizdata = new requestHealthByIdAndName.BizDataBean();
        _bizdata.setAuthMode("0x42");
        _bizdata.setRandomNumber(gRandomNum);
        requestHealthByIdAndName.BizDataBean.IdentityCheckDataBean _identity = JSON.parseObject(checkData.toJSONString(), requestHealthByIdAndName.BizDataBean.IdentityCheckDataBean.class);
        _bizdata.setIdentityCheckData(_identity);
        _reqHealth.setBizData(_bizdata);
        String body = JSON.toJSONString(_reqHealth);
        postRequestByIdAndName(gwAuthInfo, body, new jkmCallback<responseHealth>() {
            @Override
            public void onSuccess(String msg, responseHealth data) {
                if (data.getHealthMsg() == null || data.getHealthMsg().getHead() == null || !"0".equals(data.getHealthMsg().getHead().getStatus())) {
                    backMessageToViewMain(new MessageEvent(PageTool.NETWORK_ERROR_MSG, 5));
                } else {
                    backMessageToViewMain(new MessageEvent(JSON.toJSONString(data), 1));
                }
            }

            @Override
            public void onFail(String code, String msg) {
                Log.e(TAG, "code " + code + " | " + msg);
                backMessageToViewMain(new MessageEvent(msg, 2));
            }

            @Override
            public void onError(com.lzy.okgo.model.Response<String> response) {
                super.onError(response);
                backMessageToViewMain(new MessageEvent(PageTool.NETWORK_ERROR_MSG, 5));
            }
        });
    }

    //1.2 用身份两要素密文请求健康报告
    public void postRequestByIdAndName(String gwAuthinfo, String body, jkmCallback jkmCallback) {
        String url = SpUtils.getGKUrl() + Const.healthByIdAndName;
        makeHeader(gwAuthinfo);
        OkGo.post(url).tag(this).upJson(body).execute(jkmCallback);
    }

    //2.2闵康码 健康码核验接口
    public void sendPostHealthVerification(String strCodeData, String strTemperature) {
        // 请求url
        String url = SpUtils.getMZTUrl();
        Log.i(TAG, "url=" + url);

        qrCodeValue = strCodeData;
        //初始化json对象
        org.json.JSONObject jsonObjectSend = new org.json.JSONObject();
        try {
            jsonObjectSend.put("INVOKESERVICE_CODE", "H046");
            jsonObjectSend.put("INVOKECALLER_CODE", SpUtils.getMztInvokeCode());
            jsonObjectSend.put("USER_MOBILE", SpUtils.getMqttSn());
            jsonObjectSend.put("QR_CONTENT", strCodeData);
            jsonObjectSend.put("CEHCK_TWNR", strTemperature);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        String requestData = jsonObjectSend.toString();
        Log.i(TAG, "客户端发送数据：" + requestData);

        //发起http请求
        OkHttpClient okHttpClient = new OkHttpClient();
        RequestBody requestBody = new FormBody.Builder()
                .add("POSTPARAM_JSON", requestData)
                .build();
        //创建一个请求对象
        Request request = new Request.Builder()
                .url(url)
                .post(requestBody)
                .build();
        okHttpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.e(TAG, "code " + e.toString());
                backMessageToViewMain(new MessageEvent(PageTool.NETWORK_ERROR_MSG, 5));
            }

            @Override
            public void onResponse(Call call, Response responseData) throws IOException {
                //{"msg":null,"data":{"CHECK_RYLX":"TX"},"success":true,"INVOKELOG_BTIME":"2022-01-04 16:55:14"}
                String strClearResult = responseData.body().string();
                System.out.println("服务端响应数据：" + strClearResult);
                try {
                    org.json.JSONObject jsonObjectResult = new org.json.JSONObject(strClearResult);
                    String msg = jsonObjectResult.getString("msg");
                    Log.i(TAG, "success:" + jsonObjectResult.getString("success"));
                    if (!jsonObjectResult.getString("success").equals("true")) {
                        //请求出错
                        backMessageToViewMain(new MessageEvent(msg, 2));
                    } else {
                        //请求成功
                        jsonObjectResult = jsonObjectResult.getJSONObject("data");
                        String check = jsonObjectResult.getString("CHECK_RYLX");
                        String codeVal = "TX".equals(check) ? "00" : "100";
                        String jsonRes = String.format("{\"healthMsg\":{\"data\":{\"healthyReport\":[{\"cdTimeVersion\":24,\"dataSource\":\"广东省\",\"date\":%s,\"healthyId\":\"144-407dafc3821360335bb360ae3007000bfefc2e967b371596ef4b3856b21d2e88\",\"healthyStaus\":\"%s\",\"isVaccinated\":\"12\",\"reason\":\"无\"}],\"hsjc\":{},\"person\":{\"desensitizeCardId\":\"%s\",\"desensitizeName\":\"%s\"},\"xcgj\":[],\"yqReports\":[],\"yqWarn\":{\"warnType\":\"无\"}},\"head\":{\"message\":\"%s\",\"status\":\"0\"}}}", System.currentTimeMillis(), codeVal, "", "", msg);
                        //{"healthMsg":{"data":{"healthyReport":[{"cdTimeVersion":24,"dataSource":"广东省","date":%s,"healthyId":"144-407dafc3821360335bb360ae3007000bfefc2e967b371596ef4b3856b21d2e88","healthyStaus":"%s","isVaccinated":"12","reason":"无"}],"hsjc":{},"person":{"desensitizeCardId":"%s","desensitizeName":"%s"},"xcgj":[],"yqReports":[],"yqWarn":{"warnType":"无"}},"head":{"message":"接口调用成功","status":"0"}}}
                        backMessageToViewMain(new MessageEvent(jsonRes, 1));

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    backMessageToViewMain(new MessageEvent(e.getMessage(), 2));
                }
            }
        });
    }

    //2.1.3闽政通 身份证核验接口
    public void sendPostHealthVerification(String strName, String strCardId, String strTemperature) {
        // 请求url
        String url = SpUtils.getMZTUrl();
        Log.i(TAG, "url=" + url);

        //初始化json对象
        JSONObject jsonObjectSend = new JSONObject();
        jsonObjectSend.put("INVOKESERVICE_CODE", "H053");
        jsonObjectSend.put("INVOKECALLER_CODE", SpUtils.getMztInvokeCode());
        jsonObjectSend.put("USER_MOBILE", SpUtils.getMqttSn());
        //jsonObjectSend.put("USER_IDCARD", Sm4Utils.getSm4EncryptByCBC(strCardId));
        //jsonObjectSend.put("USER_NAME", Sm4Utils.getSm4EncryptByCBC(strName));
        //EncryptCard
        try {
            jsonObjectSend.put("USER_IDCARD", Sm4Utils.getSm4EncryptByCBC(strCardId));
            jsonObjectSend.put("USER_NAME", Sm4Utils.getSm4EncryptByCBC(strName));
        } catch (Exception e) {
            e.printStackTrace();
        }
        jsonObjectSend.put("CEHCK_TWNR", strTemperature);

        String requestData = jsonObjectSend.toString();
        Log.i(TAG, "客户端发送数据：" + requestData);

        //发起http请求
        OkHttpClient okHttpClient = new OkHttpClient();
        RequestBody requestBody = new FormBody.Builder()
                .add("POSTPARAM_JSON", requestData)
                .build();
        //创建一个请求对象
        Request request = new Request.Builder()
                .url(url)
                .post(requestBody)
                .build();
        okHttpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.e(TAG, "code " + e.toString());
                backMessageToViewMain(new MessageEvent(PageTool.NETWORK_ERROR_MSG, 5));
            }

            @Override
            public void onResponse(Call call, Response responseData) throws IOException {
                String strClearResult = responseData.body().string();
                Log.i(TAG, "服务端响应数据：" + strClearResult);
                try {
                    JSONObject jsonObjectResult = JSON.parseObject(strClearResult);
                    Log.i(TAG, "success:" + jsonObjectResult.getString("success"));
                    if (!jsonObjectResult.getString("success").equals("true")) {
                        //请求出错
                        backMessageToViewMain(new MessageEvent(jsonObjectResult.getString("msg"), 2));
                    } else {
                        //请求成功
                        jsonObjectResult = jsonObjectResult.getJSONObject("data");
                        String check = jsonObjectResult.getString("CHECK_RYLX");
                        String codeVal = "TX".equals(check) ? "00" : "100";
                        String cardNo = strCardId;
                        if (strCardId.length() > 3) {
                            cardNo = "*********" + strCardId.substring(strCardId.length() - 3);
                        }
                        String jsonRes = String.format("{\"healthMsg\":{\"data\":{\"healthyReport\":[{\"cdTimeVersion\":24,\"dataSource\":\"广东省\",\"date\":%s,\"healthyId\":\"144-407dafc3821360335bb360ae3007000bfefc2e967b371596ef4b3856b21d2e88\",\"healthyStaus\":\"%s\",\"isVaccinated\":\"12\",\"reason\":\"无\"}],\"hsjc\":{},\"person\":{\"desensitizeCardId\":\"%s\",\"desensitizeName\":\"%s**\"},\"xcgj\":[],\"yqReports\":[],\"yqWarn\":{\"warnType\":\"无\"}},\"head\":{\"message\":\"接口调用成功\",\"status\":\"0\"}}}", System.currentTimeMillis(), codeVal, cardNo, strName.charAt(0));
                        //{"healthMsg":{"data":{"healthyReport":[{"cdTimeVersion":24,"dataSource":"广东省","date":%s,"healthyId":"144-407dafc3821360335bb360ae3007000bfefc2e967b371596ef4b3856b21d2e88","healthyStaus":"%s","isVaccinated":"12","reason":"无"}],"hsjc":{},"person":{"desensitizeCardId":"%s","desensitizeName":"%s"},"xcgj":[],"yqReports":[],"yqWarn":{"warnType":"无"}},"head":{"message":"接口调用成功","status":"0"}}}
                        backMessageToViewMain(new MessageEvent(jsonRes, 1));

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    backMessageToViewMain(new MessageEvent(e.getMessage(), 2));
                }
            }
        });
    }


    //2、国康码核验
    //2.1 用国康码请求健康信息

    public void requestHealthByQrCode(JSONObject _cd, String strTempture, String strPosition) {
        int rnd = ThreadLocalRandom.current().nextInt(1000, MAX_RANGE);
        String time = bussiness.getUnitTime();
        Log.e("backMessageToView", "requestHealthByQrCode ");
        if (time.length() == 0) {
            Log.e(TAG, "获取unixtime错误");
            return;
        }
        Log.e(TAG, gModSn + "  " + time + " " + String.valueOf(rnd));
        String gwAuthInfo = DeviceManager.getInstance().deviceToGWIdentityAuthInfo(gModSn, String.valueOf(rnd), time);
        requestHealthByQrCode _reqHealthByQrCode = new requestHealthByQrCode();
        _reqHealthByQrCode.setRequestIdentityData("jkbg,xcgj,hsjc");
        _reqHealthByQrCode.setBizType("7001");
        requestHealthByQrCode.BizDataBean _bizdata = new requestHealthByQrCode.BizDataBean();
        _bizdata.setFirmMark("QZ");//按实际填写厂家代码
        _bizdata.setBodyTemperature(strTempture);//实际测温数据
        _bizdata.setPosition((strPosition != null ? strPosition : ""));//实测经纬度或poi信息
        _bizdata.setAuthMode("V033");
        _bizdata.setCheckData(_cd);
        _reqHealthByQrCode.setBizData(_bizdata);
        String body = JSON.toJSONString(_reqHealthByQrCode);
        getHealthByQrCode(gwAuthInfo, body, new jkmCallback<responseHealth>() {
            @Override
            public void onSuccess(String msg, responseHealth data) {
                backMessageToViewMain(new MessageEvent(JSON.toJSONString(data), 1));
            }

            @Override
            public void onFail(String code, String msg) {
                Log.e(TAG, "code " + code + " | " + msg);
                backMessageToViewMain(new MessageEvent(msg, 2));
            }

            @Override
            public void onError(com.lzy.okgo.model.Response<String> response) {
                super.onError(response);
                backMessageToViewMain(new MessageEvent(PageTool.NETWORK_ERROR_MSG, 5));
            }
        });
    }

    public void getHealthByQrCode(String gwAuthinfo, String body, jkmCallback jkmCallback) {
        String url = SpUtils.getGKUrl() + Const.JkmQrDataUrl;
        makeHeader(gwAuthinfo);
        OkGo.post(url).tag(this).upJson(body).execute(jkmCallback);
    }

    //3、CTID身份码核验
    public void requestQrValidate(String qrdata) {
        qrCodeValue = qrdata;
        Log.e("backMessageToView", "requestQrValidate ");
        int rnd = ThreadLocalRandom.current().nextInt(1000, MAX_RANGE);
        String gwAuthInfo = DeviceManager.getInstance().deviceToGWIdentityAuthInfo(gModSn, String.valueOf(rnd), bussiness.getUnitTime());
        if (!TextUtils.isEmpty(gwAuthInfo)) {
            requestByQrGetIdentityChiper _req = new requestByQrGetIdentityChiper();
            _req.setCodeContent(qrdata);
            _req.setPhotoData("");
            _req.setAuthMode("V000");
            _req.setCtidAppId("01234001");
            _req.setRequestId(UUID.randomUUID().toString());
            _req.setRequestIdentityData("name,number");
            String body = JSON.toJSONString(_req);
            getIdentityChiperByQrdata(gwAuthInfo, body, new jkmCallback<responseHealthByQrCode>() {
                @Override
                public void onSuccess(String msg, responseHealthByQrCode data) {
                    result_IdentityChiper = data.getIdentityInfo();
                    //backMessageToViewMain(new MessageEvent(result_IdentityChiper, 3));
                    requestBsn();
                }

                @Override
                public void onFail(String code, String msg) {
                    Log.e(TAG, "code " + code + " | " + msg);
                    backMessageToViewMain(new MessageEvent(msg, 2));
                }

                @Override
                public void onError(com.lzy.okgo.model.Response<String> response) {
                    super.onError(response);
                    backMessageToViewMain(new MessageEvent(PageTool.NETWORK_ERROR_MSG, 5));
                }
            });
        } else {
            backMessageToViewMain(new MessageEvent("生成网关鉴权密文失败", 0));
        }
    }

    //3.1 用身份码请求身份密文
    public void getIdentityChiperByQrdata(String gwAuthinfo, String body, jkmCallback jkmCallback) {
        String url = SpUtils.getGKUrl() + Const.CtidQrDataUrl;
        makeHeader(gwAuthinfo);
        OkGo.post(url).tag(this).upJson(body).execute(jkmCallback);
    }

    //3.2 用CTID身份码请求健康报告
    public void getHealthReportByCtidQrCode() {
        Log.e(TAG, "----> 用CTID身份码请求健康报告");
        int rnd = ThreadLocalRandom.current().nextInt(1000, MAX_RANGE);
        String gwAuthInfo = DeviceManager.getInstance().deviceToGWIdentityAuthInfo(gModSn, String.valueOf(rnd), bussiness.getUnitTime());
        requestHealthByIdAndName _reqHealth = new requestHealthByIdAndName();
        _reqHealth.setRequestIdentityData("jkbg,xcgj,hsjc");//健康报告，行程轨迹，核酸检测
        _reqHealth.setBizType("7003");
        _reqHealth.setBsn(gBsn);
        requestHealthByIdAndName.BizDataBean _bizdata = new requestHealthByIdAndName.BizDataBean();
        _bizdata.setAuthMode("0x42");
        _bizdata.setRandomNumber(gRandomNum);
        requestHealthByIdAndName.BizDataBean.IdentityCheckDataBean _identity = JSON.parseObject(result_IdentityChiper, requestHealthByIdAndName.BizDataBean.IdentityCheckDataBean.class);
        _bizdata.setIdentityCheckData(_identity);
        _reqHealth.setBizData(_bizdata);
        String body = JSON.toJSONString(_reqHealth);
        postRequestByIdAndName(gwAuthInfo, body, new jkmCallback<responseHealth>() {
            @Override
            public void onSuccess(String msg, responseHealth data) {
                result_IdentityChiper = "";
                backMessageToViewMain(new MessageEvent(JSON.toJSONString(data), 1));
            }

            @Override
            public void onFail(String code, String msg) {
                result_IdentityChiper = "";
                Log.e(TAG, "code " + code + " | " + msg);
                backMessageToViewMain(new MessageEvent(msg, 2));
            }

            @Override
            public void onError(com.lzy.okgo.model.Response<String> response) {
                super.onError(response);
                backMessageToViewMain(new MessageEvent(PageTool.NETWORK_ERROR_MSG, 5));
            }
        });

    }

    private void backMessageToViewMain(MessageEvent messageEvent) {
        if (listener == null) {
            return;
        }
        handler.post(() -> listener.backMessageToView(messageEvent));
    }

    HealthyMessageListener listener;

//    身份证过闸的话-》身份证号用这个MD5
//    二维码过闸-》二维码串的MD5

    public void setIdentifyData(String identify) {
        this.identifyData = identify;
    }

    public String getQrCodeValue() {
        return qrCodeValue;
    }

    public void setQrCodeValue(String qrCodeValue) {
        this.qrCodeValue = qrCodeValue;
    }

    public String getIdentifyData() {
        return identifyData;
    }

    public void setMessageListener(HealthyMessageListener listener) {
        this.listener = listener;
    }
}
