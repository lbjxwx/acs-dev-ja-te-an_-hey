//package com.nlp.cloundcbsappdemo.view;
//
//import androidx.appcompat.app.AppCompatActivity;
//
//import android.os.Bundle;
//import android.text.TextUtils;
//import android.util.Log;
//
//import com.alibaba.fastjson.JSONObject;
//import com.nlp.cloundcbsappdemo.R;
//import com.nlp.cloundcbsappdemo.listener.HealthyMessageListener;
//import com.nlp.cloundcbsappdemo.model.*;
//import com.nlp.cloundcbsappdemo.util.MessageDealUtil;
//import com.nlp.cloundcbsappdemo.viewmodel.MainViewModel;
//
//import org.greenrobot.eventbus.EventBus;
//import org.greenrobot.eventbus.Subscribe;
//import org.greenrobot.eventbus.ThreadMode;
//
//import cn.anicert.polymerizeDevice.DeviceManager;
//import cn.anicert.polymerizeDevice.event.CtidQRCodeScannedEvent;
//
//public class MainActivity extends AppCompatActivity {
//    String TAG = "QZdemo";
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_main);
//        EventBus.getDefault().register(this);
//        initHealModel();
//    }
//
//    MainViewModel viewModel;
//    String port = "/dev/ttyACM0";
//
//    private void initHealModel() {
//        viewModel = new MainViewModel(MainActivity.this);
//        viewModel.setMessageListener(messageListener);
//        viewModel.initHealthyCodeManager(port);
//    }
//
//    private HealthyMessageListener messageListener = new HealthyMessageListener() {
//        @Override
//        public void initHealthyStatues(boolean isSuccess, String errorDesc) {
//            prependMsg("初始化状态: " + isSuccess + " / " + errorDesc);
//        }
//
//        @Override
//        public void backMessageToView(MessageEvent messageEvent) {
//            Log.e("MAIN mode", messageEvent.getMessage() + "  " + messageEvent.getMsgClass() + "");
//            switch (messageEvent.getMsgClass()) {
//                case 0: //执行状态
//                    Log.e("执行状态", messageEvent.getMessage());
//                    prependMsg(messageEvent.getMessage());
//                    break;
//                case 1: //展示健康结果
//                    //Log.e("展示健康结果",messageEvent.getMessage());
//                    String msg = messageEvent.getMessage();
//                    prependMsg("核验结果(简版)：" + MessageDealUtil.VerificationResult(msg));
//                    prependMsg("核验结果（较全版）：" + MessageDealUtil.VerificationResultAll(msg));
//                    prependMsg("可继续扫码核验。。。" + "\n");
//                    break;
//            }
//        }
//    };
//
//    //扫码后回调入口
//    @Subscribe(threadMode = ThreadMode.MAIN)
//    public void onQrCodeScannedEvent(CtidQRCodeScannedEvent event) {
//        String qrCodeValue = DeviceManager.getInstance().getCtidQRCode().getRawValue();
//        if (TextUtils.isEmpty(qrCodeValue)) {
//            return;
//        }
//        int type = DeviceManager.getInstance().getQrType(qrCodeValue);
//        if (type == 1) {
//            JSONObject checkData = null;
//            try {
//                checkData = DeviceManager.getInstance().getCheckData();
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//            viewModel.requestHealthByQrCode(checkData, "36", "深圳市高新中一道");
//            return;
//        }
//        viewModel.requestQrValidate(qrCodeValue);
//    }
//
//    @Override
//    protected void onDestroy() {
//        super.onDestroy();
//        viewModel.closePort();
//        if (EventBus.getDefault().isRegistered(this)) {
//            EventBus.getDefault().unregister(this);
//        }
//    }
//
//    //界面报文显示
//    private void prependMsg(final String msg) {
//        Log.i(TAG, msg);
//    }
//}