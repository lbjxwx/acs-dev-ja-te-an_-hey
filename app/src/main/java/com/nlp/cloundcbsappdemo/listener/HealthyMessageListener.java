package com.nlp.cloundcbsappdemo.listener;

import com.nlp.cloundcbsappdemo.model.MessageEvent;

public interface HealthyMessageListener {

    void initHealthyStatues(boolean isSuccess, int code);

    void backMessageToView(MessageEvent messageEvent);

}
