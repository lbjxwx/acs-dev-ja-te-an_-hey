package com.nlp.cloundcbsappdemo.model;

import java.io.Serializable;

public class checkData implements Serializable {

    /**
     * lt : 0000
     * qrCode : {"sign":"0704ef65fa56f042dc9c7d264276a42094febd300e18049add00311ddde0fd3d93a5ded82cb75869f5c5ed274f7c8d29139514c541b1e7d42a7819bf8020eae700","sn":"0199999600000079","value":"ABj+Ac7qoVdueOd0tZPSW6qolzy3m0MkAGkCAUpLTTExMTAwMDAAR0JHU/c8TGHu8wJ0d/+VdpYCg0vhZHAn9n6HS1PPuPed4QMxtjbDCR099a+eh1e+57PiSOro4RD36mWSvPy3QKvpguS0ollFfZ6IT1DlZbAT9yEyZq8guMSKR0JHUyAGDAEznHfJtI1XjLKizyssjX0m8AiADuJAWu3XWsHgTnuiXE2P3CyVRH2JLAOJngsaU+v3EIBlDI05285xvYSGRA+I","gbCode":1}
     */

    private String lt;
    private QrCodeBean qrCode;

    public String getLt() {
        return lt;
    }

    public void setLt(String lt) {
        this.lt = lt;
    }

    public QrCodeBean getQrCode() {
        return qrCode;
    }

    public void setQrCode(QrCodeBean qrCode) {
        this.qrCode = qrCode;
    }

    public static class QrCodeBean implements Serializable {
        /**
         * sign : 0704ef65fa56f042dc9c7d264276a42094febd300e18049add00311ddde0fd3d93a5ded82cb75869f5c5ed274f7c8d29139514c541b1e7d42a7819bf8020eae700
         * sn : 0199999600000079
         * value : ABj+Ac7qoVdueOd0tZPSW6qolzy3m0MkAGkCAUpLTTExMTAwMDAAR0JHU/c8TGHu8wJ0d/+VdpYCg0vhZHAn9n6HS1PPuPed4QMxtjbDCR099a+eh1e+57PiSOro4RD36mWSvPy3QKvpguS0ollFfZ6IT1DlZbAT9yEyZq8guMSKR0JHUyAGDAEznHfJtI1XjLKizyssjX0m8AiADuJAWu3XWsHgTnuiXE2P3CyVRH2JLAOJngsaU+v3EIBlDI05285xvYSGRA+I
         * gbCode : 1
         */

        private String sign;
        private String sn;
        private String value;
        private int gbCode;

        public String getSign() {
            return sign;
        }

        public void setSign(String sign) {
            this.sign = sign;
        }

        public String getSn() {
            return sn;
        }

        public void setSn(String sn) {
            this.sn = sn;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        public int getGbCode() {
            return gbCode;
        }

        public void setGbCode(int gbCode) {
            this.gbCode = gbCode;
        }
    }
}
