package com.nlp.cloundcbsappdemo.model;

import java.io.Serializable;

public class requestByJKMQrData implements Serializable {

    /**
     * requestIdentityData : jkbg,xcgj,hsjc
     * bizType : 7001
     * bizData : {"authMode":"V033","checkData":{"qrCode":{"value":"ABj+AVkZvT1W7JDEmzepIpL4nSeCP8ABAGkCAUpLTTExMTAwMDAAR0JHU9U4Q2Ha0mtjDB1EukRMoNLGNmbOMz72tREgGdBXamD0FOSGK+nsDb+fkvjXTKCf3WeT57WGJCnaaBnXdhGRyXd0gPIRnT7FVl6JeoQiX1abYjmBOvadR0JHUyARDAGGD6lsQF2Dg8P64Q3OHQKtXQxkc/0Jjqb0zNRJcmigghDmGfEQEZhcT5ZpSkmi59rBDPR6qIGygPfqZLA/wMXE","sign":"5559dc1e4ff4d05ee27f8e02560a398c7127be769dbda49811daf4b539a9d741bf20701e43b250bba7b59bc705cd2f92f38c9aa439beaefd7723afac2ce6a49000","sn":"0199999600000079","gbCode":1}},"firmMark":"xdl","bodyTemperature":"36","position":"北京市朝阳门地铁站A口"}
     */

    private String requestIdentityData;
    private String bizType;
    private BizDataBean bizData;

    public String getRequestIdentityData() {
        return requestIdentityData;
    }

    public void setRequestIdentityData(String requestIdentityData) {
        this.requestIdentityData = requestIdentityData;
    }

    public String getBizType() {
        return bizType;
    }

    public void setBizType(String bizType) {
        this.bizType = bizType;
    }

    public BizDataBean getBizData() {
        return bizData;
    }

    public void setBizData(BizDataBean bizData) {
        this.bizData = bizData;
    }

    public static class BizDataBean implements Serializable {
        /**
         * authMode : V033
         * checkData : {"qrCode":{"value":"ABj+AVkZvT1W7JDEmzepIpL4nSeCP8ABAGkCAUpLTTExMTAwMDAAR0JHU9U4Q2Ha0mtjDB1EukRMoNLGNmbOMz72tREgGdBXamD0FOSGK+nsDb+fkvjXTKCf3WeT57WGJCnaaBnXdhGRyXd0gPIRnT7FVl6JeoQiX1abYjmBOvadR0JHUyARDAGGD6lsQF2Dg8P64Q3OHQKtXQxkc/0Jjqb0zNRJcmigghDmGfEQEZhcT5ZpSkmi59rBDPR6qIGygPfqZLA/wMXE","sign":"5559dc1e4ff4d05ee27f8e02560a398c7127be769dbda49811daf4b539a9d741bf20701e43b250bba7b59bc705cd2f92f38c9aa439beaefd7723afac2ce6a49000","sn":"0199999600000079","gbCode":1}}
         * firmMark : xdl
         * bodyTemperature : 36
         * position : 北京市朝阳门地铁站A口
         */

        private String authMode;
        private CheckDataBean checkData;
        private String firmMark;
        private String bodyTemperature;
        private String position;

        public String getAuthMode() {
            return authMode;
        }

        public void setAuthMode(String authMode) {
            this.authMode = authMode;
        }

        public CheckDataBean getCheckData() {
            return checkData;
        }

        public void setCheckData(CheckDataBean checkData) {
            this.checkData = checkData;
        }

        public String getFirmMark() {
            return firmMark;
        }

        public void setFirmMark(String firmMark) {
            this.firmMark = firmMark;
        }

        public String getBodyTemperature() {
            return bodyTemperature;
        }

        public void setBodyTemperature(String bodyTemperature) {
            this.bodyTemperature = bodyTemperature;
        }

        public String getPosition() {
            return position;
        }

        public void setPosition(String position) {
            this.position = position;
        }

        public static class CheckDataBean implements Serializable {
            /**
             * qrCode : {"value":"ABj+AVkZvT1W7JDEmzepIpL4nSeCP8ABAGkCAUpLTTExMTAwMDAAR0JHU9U4Q2Ha0mtjDB1EukRMoNLGNmbOMz72tREgGdBXamD0FOSGK+nsDb+fkvjXTKCf3WeT57WGJCnaaBnXdhGRyXd0gPIRnT7FVl6JeoQiX1abYjmBOvadR0JHUyARDAGGD6lsQF2Dg8P64Q3OHQKtXQxkc/0Jjqb0zNRJcmigghDmGfEQEZhcT5ZpSkmi59rBDPR6qIGygPfqZLA/wMXE","sign":"5559dc1e4ff4d05ee27f8e02560a398c7127be769dbda49811daf4b539a9d741bf20701e43b250bba7b59bc705cd2f92f38c9aa439beaefd7723afac2ce6a49000","sn":"0199999600000079","gbCode":1}
             */

            private QrCodeBean qrCode;

            public QrCodeBean getQrCode() {
                return qrCode;
            }

            public void setQrCode(QrCodeBean qrCode) {
                this.qrCode = qrCode;
            }

            public static class QrCodeBean implements Serializable {
                /**
                 * value : ABj+AVkZvT1W7JDEmzepIpL4nSeCP8ABAGkCAUpLTTExMTAwMDAAR0JHU9U4Q2Ha0mtjDB1EukRMoNLGNmbOMz72tREgGdBXamD0FOSGK+nsDb+fkvjXTKCf3WeT57WGJCnaaBnXdhGRyXd0gPIRnT7FVl6JeoQiX1abYjmBOvadR0JHUyARDAGGD6lsQF2Dg8P64Q3OHQKtXQxkc/0Jjqb0zNRJcmigghDmGfEQEZhcT5ZpSkmi59rBDPR6qIGygPfqZLA/wMXE
                 * sign : 5559dc1e4ff4d05ee27f8e02560a398c7127be769dbda49811daf4b539a9d741bf20701e43b250bba7b59bc705cd2f92f38c9aa439beaefd7723afac2ce6a49000
                 * sn : 0199999600000079
                 * gbCode : 1
                 */

                private String value;
                private String sign;
                private String sn;
                private int gbCode;

                public String getValue() {
                    return value;
                }

                public void setValue(String value) {
                    this.value = value;
                }

                public String getSign() {
                    return sign;
                }

                public void setSign(String sign) {
                    this.sign = sign;
                }

                public String getSn() {
                    return sn;
                }

                public void setSn(String sn) {
                    this.sn = sn;
                }

                public int getGbCode() {
                    return gbCode;
                }

                public void setGbCode(int gbCode) {
                    this.gbCode = gbCode;
                }
            }
        }
    }
}
