package com.nlp.cloundcbsappdemo.model;

import java.io.Serializable;

public class requestHealthByIdAndName implements Serializable {


    /**
     * requestIdentityData : jkbg,xcgj,hsjc
     * bizType : 7003
     * bsn : 023E6ADB0000005000017BF783036010
     * bizData : {"authMode":"0x42","randomNumber":"LP62S1y34hooL3Pn9MvgWPA8MZvDyibCyvrqOYMv7IQpW2ftqGzEzWHiNKcNhnq6mXEU6w91skfFr7WC8LjjvNgqJfhU2OlQehpuASt3WtajanB0fvEhQasq","identityCheckData":{"lt":"0000","publicVer":"V1.0","identity":{"idinfo":"cb5d9f51aa63d39a64ef240bf57bfebd98aba157c9bed8a5566cafb64a9719df37410219e128d7e1dd995eadcf1fb80312a4839032e92b9bf979d48b4441f34e8b8d8356de9f44db1a5927c92362e0a5f97a9fa20f77c9c2c5d4bab4d99e07950fda77e8a1434e2529a737cb95585b5546c70c72aa7c68c9e0a2fc3473c7324279e928044d1cfce5baa76d3ea90d45a56617647b8645b6d993da963a3e9e89bb332387da19b69912a39e54dc811507891a7c0d2087521fa53629ae37743c01e6769766d3bc60c898f1f761a690c9e36e2eb448fe3e31b82e8cd204f5ba7d24ccb1a089513c44bc9669230cd1cf2377b8eb31dd26c108b1bafd2d22ee28c30e3148b1422d015bea074bd0942aec39d82709b69c047e","sign":"23c03264aba0b6c3ded8de893aa7087e390a1af1b42aa70ccd12186fb977cf9c0d5e5097a9502bd707d7f4990462acffb62ef972fc39bd2b1b8175fa6c826650","sn":"0199999600000079"}}}
     */

    private String requestIdentityData;
    private String bizType;
    private String bsn;
    private BizDataBean bizData;

    public String getRequestIdentityData() {
        return requestIdentityData;
    }

    public void setRequestIdentityData(String requestIdentityData) {
        this.requestIdentityData = requestIdentityData;
    }

    public String getBizType() {
        return bizType;
    }

    public void setBizType(String bizType) {
        this.bizType = bizType;
    }

    public String getBsn() {
        return bsn;
    }

    public void setBsn(String bsn) {
        this.bsn = bsn;
    }

    public BizDataBean getBizData() {
        return bizData;
    }

    public void setBizData(BizDataBean bizData) {
        this.bizData = bizData;
    }

    public static class BizDataBean implements Serializable {
        /**
         * authMode : 0x42
         * randomNumber : LP62S1y34hooL3Pn9MvgWPA8MZvDyibCyvrqOYMv7IQpW2ftqGzEzWHiNKcNhnq6mXEU6w91skfFr7WC8LjjvNgqJfhU2OlQehpuASt3WtajanB0fvEhQasq
         * identityCheckData : {"lt":"0000","publicVer":"V1.0","identity":{"idinfo":"cb5d9f51aa63d39a64ef240bf57bfebd98aba157c9bed8a5566cafb64a9719df37410219e128d7e1dd995eadcf1fb80312a4839032e92b9bf979d48b4441f34e8b8d8356de9f44db1a5927c92362e0a5f97a9fa20f77c9c2c5d4bab4d99e07950fda77e8a1434e2529a737cb95585b5546c70c72aa7c68c9e0a2fc3473c7324279e928044d1cfce5baa76d3ea90d45a56617647b8645b6d993da963a3e9e89bb332387da19b69912a39e54dc811507891a7c0d2087521fa53629ae37743c01e6769766d3bc60c898f1f761a690c9e36e2eb448fe3e31b82e8cd204f5ba7d24ccb1a089513c44bc9669230cd1cf2377b8eb31dd26c108b1bafd2d22ee28c30e3148b1422d015bea074bd0942aec39d82709b69c047e","sign":"23c03264aba0b6c3ded8de893aa7087e390a1af1b42aa70ccd12186fb977cf9c0d5e5097a9502bd707d7f4990462acffb62ef972fc39bd2b1b8175fa6c826650","sn":"0199999600000079"}}
         */

        private String authMode;
        private String randomNumber;
        private IdentityCheckDataBean identityCheckData;

        public String getAuthMode() {
            return authMode;
        }

        public void setAuthMode(String authMode) {
            this.authMode = authMode;
        }

        public String getRandomNumber() {
            return randomNumber;
        }

        public void setRandomNumber(String randomNumber) {
            this.randomNumber = randomNumber;
        }

        public IdentityCheckDataBean getIdentityCheckData() {
            return identityCheckData;
        }

        public void setIdentityCheckData(IdentityCheckDataBean identityCheckData) {
            this.identityCheckData = identityCheckData;
        }

        public static class IdentityCheckDataBean implements Serializable {
            /**
             * lt : 0000
             * publicVer : V1.0
             * identity : {"idinfo":"cb5d9f51aa63d39a64ef240bf57bfebd98aba157c9bed8a5566cafb64a9719df37410219e128d7e1dd995eadcf1fb80312a4839032e92b9bf979d48b4441f34e8b8d8356de9f44db1a5927c92362e0a5f97a9fa20f77c9c2c5d4bab4d99e07950fda77e8a1434e2529a737cb95585b5546c70c72aa7c68c9e0a2fc3473c7324279e928044d1cfce5baa76d3ea90d45a56617647b8645b6d993da963a3e9e89bb332387da19b69912a39e54dc811507891a7c0d2087521fa53629ae37743c01e6769766d3bc60c898f1f761a690c9e36e2eb448fe3e31b82e8cd204f5ba7d24ccb1a089513c44bc9669230cd1cf2377b8eb31dd26c108b1bafd2d22ee28c30e3148b1422d015bea074bd0942aec39d82709b69c047e","sign":"23c03264aba0b6c3ded8de893aa7087e390a1af1b42aa70ccd12186fb977cf9c0d5e5097a9502bd707d7f4990462acffb62ef972fc39bd2b1b8175fa6c826650","sn":"0199999600000079"}
             */

            private String lt;
            private String publicVer;
            private IdentityBean identity;

            public String getLt() {
                return lt;
            }

            public void setLt(String lt) {
                this.lt = lt;
            }

            public String getPublicVer() {
                return publicVer;
            }

            public void setPublicVer(String publicVer) {
                this.publicVer = publicVer;
            }

            public IdentityBean getIdentity() {
                return identity;
            }

            public void setIdentity(IdentityBean identity) {
                this.identity = identity;
            }

            public static class IdentityBean implements Serializable {
                /**
                 * idinfo : cb5d9f51aa63d39a64ef240bf57bfebd98aba157c9bed8a5566cafb64a9719df37410219e128d7e1dd995eadcf1fb80312a4839032e92b9bf979d48b4441f34e8b8d8356de9f44db1a5927c92362e0a5f97a9fa20f77c9c2c5d4bab4d99e07950fda77e8a1434e2529a737cb95585b5546c70c72aa7c68c9e0a2fc3473c7324279e928044d1cfce5baa76d3ea90d45a56617647b8645b6d993da963a3e9e89bb332387da19b69912a39e54dc811507891a7c0d2087521fa53629ae37743c01e6769766d3bc60c898f1f761a690c9e36e2eb448fe3e31b82e8cd204f5ba7d24ccb1a089513c44bc9669230cd1cf2377b8eb31dd26c108b1bafd2d22ee28c30e3148b1422d015bea074bd0942aec39d82709b69c047e
                 * sign : 23c03264aba0b6c3ded8de893aa7087e390a1af1b42aa70ccd12186fb977cf9c0d5e5097a9502bd707d7f4990462acffb62ef972fc39bd2b1b8175fa6c826650
                 * sn : 0199999600000079
                 */

                private String idinfo;
                private String sign;
                private String sn;

                public String getIdinfo() {
                    return idinfo;
                }

                public void setIdinfo(String idinfo) {
                    this.idinfo = idinfo;
                }

                public String getSign() {
                    return sign;
                }

                public void setSign(String sign) {
                    this.sign = sign;
                }

                public String getSn() {
                    return sn;
                }

                public void setSn(String sn) {
                    this.sn = sn;
                }
            }
        }
    }
}
