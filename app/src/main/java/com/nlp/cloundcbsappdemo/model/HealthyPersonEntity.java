package com.nlp.cloundcbsappdemo.model;

public class HealthyPersonEntity {
    boolean parsenerStatues;  //解析状态
    String HSJC;    //核算检测信息
    String XCJL;      //行程信息
    String IDNum;    //身份证信息
    String Status;    //00 绿码   01黄码 10红码
    String Name;    //名字
    String errorDesc;
//    {"HSJC":"","XCJL":"您14天内未曾到访过中高风险地区","IDNum":"**************2930","Status":"绿码","Name":"陈**"}

    public HealthyPersonEntity(boolean parsenerStatues, String HSJC, String XCJL, String IDNum, String status, String name, String errorDesc) {
        this.parsenerStatues = parsenerStatues;
        this.HSJC = HSJC;
        this.XCJL = XCJL;
        this.IDNum = IDNum;
        Status = status;
        Name = name;
        this.errorDesc = errorDesc;
    }

    public boolean isParsenerStatues() {
        return parsenerStatues;
    }

    public void setParsenerStatues(boolean parsenerStatues) {
        this.parsenerStatues = parsenerStatues;
    }

    public String getErrorDesc() {
        return errorDesc;
    }

    public void setErrorDesc(String errorDesc) {
        this.errorDesc = errorDesc;
    }

    public String getHSJC() {
        return HSJC;
    }

    public void setHSJC(String HSJC) {
        this.HSJC = HSJC;
    }

    public String getXCJL() {
        return XCJL;
    }

    public void setXCJL(String XCJL) {
        this.XCJL = XCJL;
    }

    public String getIDNum() {
        return IDNum;
    }

    public void setIDNum(String IDNum) {
        this.IDNum = IDNum;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    @Override
    public String toString() {
        return "HealthyPersonEntity{" +
                "parsenerStatues=" + parsenerStatues +
                ", HSJC='" + HSJC + '\'' +
                ", XCJL='" + XCJL + '\'' +
                ", IDNum='" + IDNum + '\'' +
                ", Status='" + Status + '\'' +
                ", Name='" + Name + '\'' +
                ", errorDesc='" + errorDesc + '\'' +
                '}';
    }
}
