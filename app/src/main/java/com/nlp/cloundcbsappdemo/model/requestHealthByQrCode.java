package com.nlp.cloundcbsappdemo.model;

import com.alibaba.fastjson.JSONObject;

import java.io.Serializable;

public class requestHealthByQrCode implements Serializable {

    /**
     * requestIdentityData : jkbg,xcgj,hsjc
     * bizType : 7001
     * bizData : {"authMode":"V033","checkData":{"qrCode":{"value":"xxxx","sign":"xxxx","sn":"0199999600000079","gbCode":1}},"firmMark":"xdl","bodyTemperature":"36","position":"北京市朝阳门地铁站A口"}
     */

    private String requestIdentityData;
    private String bizType;
    private BizDataBean bizData;

    public String getRequestIdentityData() {
        return requestIdentityData;
    }

    public void setRequestIdentityData(String requestIdentityData) {
        this.requestIdentityData = requestIdentityData;
    }

    public String getBizType() {
        return bizType;
    }

    public void setBizType(String bizType) {
        this.bizType = bizType;
    }

    public BizDataBean getBizData() {
        return bizData;
    }

    public void setBizData(BizDataBean bizData) {
        this.bizData = bizData;
    }

    public static class BizDataBean implements Serializable {
        /**
         * authMode : V033
         * checkData : {"qrCode":{"value":"xxxx","sign":"xxxx","sn":"0199999600000079","gbCode":1}}
         * firmMark : xdl
         * bodyTemperature : 36
         * position : 北京市朝阳门地铁站A口
         */

        private String authMode;
        private JSONObject checkData;
        private String firmMark;
        private String bodyTemperature;
        private String position;

        public String getAuthMode() {
            return authMode;
        }

        public void setAuthMode(String authMode) {
            this.authMode = authMode;
        }

        public JSONObject getCheckData() {
            return checkData;
        }

        public void setCheckData(JSONObject checkData) {
            this.checkData = checkData;
        }

        public String getFirmMark() {
            return firmMark;
        }

        public void setFirmMark(String firmMark) {
            this.firmMark = firmMark;
        }

        public String getBodyTemperature() {
            return bodyTemperature;
        }

        public void setBodyTemperature(String bodyTemperature) {
            this.bodyTemperature = bodyTemperature;
        }

        public String getPosition() {
            return position;
        }

        public void setPosition(String position) {
            this.position = position;
        }
    }
}
