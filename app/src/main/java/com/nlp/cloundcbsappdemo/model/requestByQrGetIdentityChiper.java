package com.nlp.cloundcbsappdemo.model;

import java.io.Serializable;

public class requestByQrGetIdentityChiper implements Serializable {

    /**
     * codeContent : Y3NUZFOTk2RkIzMjQwMgAPMEUCIH66w6xQhR9xMxLBIEEwDQ/l28veIMXG2UnGyjkqx1ENAiEAoZxtlYLqoSePDpiRVuBm2H5pydnY3NUZFOTk2RkIzMjQwMgAPMEUCIH66w6xQhR9xMxLBIEEwDQ/l28veIMXG2UnGyjkqx1ENAiEAoZxtlYLqoSePDpiRVuBm2H5pydn=
     * photoData  :
     * authMode : V000
     * ctidAppId : 01234001
     * requestId : a1ab2d2ee14df32g13q
     */

    private String codeContent;
    private String photoData;
    private String authMode;
    private String ctidAppId;
    private String requestId;

    public String getRequestIdentityData() {
        return requestIdentityData;
    }

    public void setRequestIdentityData(String requestIdentityData) {
        this.requestIdentityData = requestIdentityData;
    }

    private String requestIdentityData;

    public String getCodeContent() {
        return codeContent;
    }

    public void setCodeContent(String codeContent) {
        this.codeContent = codeContent;
    }

    public String getPhotoData() {
        return photoData;
    }

    public void setPhotoData(String photoData) {
        this.photoData = photoData;
    }

    public String getAuthMode() {
        return authMode;
    }

    public void setAuthMode(String authMode) {
        this.authMode = authMode;
    }

    public String getCtidAppId() {
        return ctidAppId;
    }

    public void setCtidAppId(String ctidAppId) {
        this.ctidAppId = ctidAppId;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }
}
