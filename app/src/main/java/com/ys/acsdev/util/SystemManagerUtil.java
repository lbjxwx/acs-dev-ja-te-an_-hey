package com.ys.acsdev.util;

import android.content.Context;
import android.content.Intent;
import android.provider.Settings;
import android.util.Log;

import com.ys.acsdev.commond.AppInfo;
import com.ys.acsdev.ui.SplashActivity;
import com.ys.acsdev.ui.StartActivity;
import com.ys.rkapi.MyManager;

import java.io.File;

public class SystemManagerUtil {

    Context context;

    public SystemManagerUtil(Context context) {
        this.context = context;
    }

    /**
     * 获得系统亮度
     *
     * @return
     */
    public int getSystemBrightness() {
        int systemBrightness = 0;
        try {
            systemBrightness = Settings.System.getInt(context.getContentResolver(), Settings.System.SCREEN_BRIGHTNESS);
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
        }
        return systemBrightness;
    }

    /**
     * 设置当前屏幕亮度值  0--100
     */
    public void changeAppBrightness(int paramInt) {
        try {
            Settings.System.putInt(context.getContentResolver(), Settings.System.SCREEN_BRIGHTNESS, paramInt);
        } catch (Exception localException) {
            localException.printStackTrace();
        }
    }

//    /**
//     * 保存当前的屏幕亮度值，并使之生效
//     */
//    private void setScreenBrightness(int paramInt) {
//        Window localWindow = context.getWindow();
//        WindowManager.LayoutParams localLayoutParams = localWindow.getAttributes();
//        float f = paramInt / 255.0F;
//        localLayoutParams.screenBrightness = f;
//        localWindow.setAttributes(localLayoutParams);
//    }

    /***=========================================================================
     * 清理shared信息
     */
    public static void clearSharedInfo() {
        delLocalFile();
        delLocalAllDbInfo();
        try {
            String path = "/data/data/com.ys.acsdevs/shared_prefs";
            File file = new File(path);
            if (!file.exists()) {
                LogUtil.cdl("===文件不存在，不用清理==");
                return;
            }
            File[] fileLists = file.listFiles();
            if (fileLists == null || fileLists.length < 1) {
                LogUtil.cdl("===fileLists 文件不存在，不用清理==");
                return;
            }
            LogUtil.cdl("========获取文件个数====" + fileLists.length);
            for (int i = 0; i < fileLists.length; i++) {
                File fileData = fileLists[i];
                if (fileData.exists()) {
                    LogUtil.cdl("=======执行删除程序");
                    fileData.delete();
                }
            }
        } catch (Exception e) {
            LogUtil.cdl("===clearSharedInfo==" + e.toString());
        }
    }

    /***
     * 清理本地多有的数据库
     */
    private static void delLocalAllDbInfo() {
        try {
            String dbPath = "/sdcard/Android/data/com.ys.acsdevs/files/databases/face_db.db";
            File fileDb = new File(dbPath);
            if (fileDb.exists()) {
                FileUtils.deleteDirOrFile(dbPath, "客户手动清理数据");
                fileDb.delete();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /***
     * 清理本地所有的文件
     */
    private static void delLocalFile() {
        try {
            String facePath = AppInfo.BASE_LOCAL_PATH;
            File fileFace = new File(facePath);
            if (fileFace.exists()) {
                FileUtils.deleteDirOrFile(facePath, "清除人脸所有的数据");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * 每次写入大数据的时候，调用一次
     */
    public static void syncSdcard() {
        try {
            Process process = Runtime.getRuntime().exec("sync");
            process.waitFor();
        } catch (Exception e) {
            Log.e("exect", e.getMessage(), e);
        }
    }


    /***
     * 重启设备
     * @param context
     */
    public static void rebootDev(Context context) {
        try {
            MyManager manager = MyManager.getInstance(context);
            manager.reboot();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 重启应用
     *
     * @param context
     */
    public static void rebootApp(Context context) {
        try {
            Intent intent = new Intent(context, StartActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
            android.os.Process.killProcess(android.os.Process.myPid());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
