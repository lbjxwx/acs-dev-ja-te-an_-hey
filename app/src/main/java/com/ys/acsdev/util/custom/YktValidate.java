package com.ys.acsdev.util.custom;

import com.ys.acsdev.util.CodeUtil;
import com.ys.acsdev.util.LogUtil;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

public class YktValidate {

    public static String getFaceFromWebCodeSing(long currentTime, String macCode) {
        SortedMap<Object, Object> map = new TreeMap<>();
        map.put("appid", "hn_betteredu");
        map.put("sn", macCode);
        map.put("timestamp", currentTime + "");
//        LogUtil.persoon("====加密数据====" + CodeUtil.getUniquePsuedoID() + " / " + System.currentTimeMillis());
        String singCode = createSign(map, "9A0A8659F005D6984697E2CA0A9CF3B7");
        return singCode;
    }

    public static String getUploadAccessRecordSing(long currentTime, String validCode, String passType, String reocrdTime, String sn) {
        SortedMap<Object, Object> map = new TreeMap<>();
        map.put("appid", "hn_betteredu");
        map.put("passType", passType);
        map.put("reocrdTime", reocrdTime);
        map.put("gateEquipId", sn);
        map.put("timestamp", currentTime + "");
        map.put("validCode", validCode);
//        LogUtil.persoon("====加密数据====" + CodeUtil.getUniquePsuedoID() + " / " + System.currentTimeMillis());
        String singCode = createSign(map, "9A0A8659F005D6984697E2CA0A9CF3B7");
        return singCode;
    }


    public static String createSign(SortedMap<Object, Object> parameters, String key) {
        StringBuffer sb = new StringBuffer();
        Set set = parameters.entrySet(); // 所有参与传参的参数按照accsii排序（升序）
        Iterator it = set.iterator();
        while (it.hasNext()) {
            Map.Entry entry = (Map.Entry) it.next();
            Object v = entry.getValue();
            // 空值不传递,不参与签名组串
            if (null != v && !"".equals(v)) {
                sb.append(v + "&");
            }
        }
        sb = sb.append(key);
//        LogUtil.persoon("字符串:" + sb.toString());
        // MD5加密,结果转换为大写字符
        String sign = MD5Util.MD5Encode(sb.toString()).toUpperCase();
//        String test = "hn_betteredu&301F9A8098FA&2&2020-07-10 17:18:44&1594372724620&s1&9A0A8659F005D6984697E2CA0A9CF3B7";
//        String sign = MD5Util.MD5Encode(test).toUpperCase();
//        LogUtil.persoon("MD5加密值:" + sign);
        return sign;
    }

}