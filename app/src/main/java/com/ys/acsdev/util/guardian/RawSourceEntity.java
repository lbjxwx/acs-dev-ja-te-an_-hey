package com.ys.acsdev.util.guardian;

/**
 * Created by jsjm on 2018/11/9.
 */
public class RawSourceEntity {

    int rawId;
    long fileLength;
    String name;

    public RawSourceEntity(int rawId, long fileLength, String name) {
        this.rawId = rawId;
        this.fileLength = fileLength;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getRawId() {
        return rawId;
    }

    public void setRawId(int rawId) {
        this.rawId = rawId;
    }

    public long getFileLength() {
        return fileLength;
    }

    public void setFileLength(long fileLength) {
        this.fileLength = fileLength;
    }

    @Override
    public String toString() {
        return "RawSourceEntity{" +
                "rawId=" + rawId +
                ", fileLength=" + fileLength +
                ", name=" + name +
                '}';
    }
}
