package com.ys.acsdev.util;

import android.util.Log;

import com.ys.acsdev.MyApp;
import com.ys.acsdev.R;
import com.ys.acsdev.bean.LocalARBean;
import com.ys.acsdev.commond.AppInfo;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.format.Colour;
import jxl.write.Label;
import jxl.write.WritableCell;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;

public class ExcelUtil {

    public static WritableFont arial14font = null;

    public static WritableCellFormat arial14format = null;
    public static WritableFont arial10font = null;
    public static WritableCellFormat arial10format = null;
    public static WritableFont arial12font = null;
    public static WritableCellFormat arial12format = null;

    public final static String UTF8_ENCODING = "UTF-8";
    public final static String GBK_ENCODING = "GBK";


    /**
     * 单元格的格式设置 字体大小 颜色 对齐方式、背景颜色等...
     */
    public static void format() {
        try {
            arial14font = new WritableFont(WritableFont.ARIAL, 14, WritableFont.BOLD);
            arial14font.setColour(jxl.format.Colour.LIGHT_BLUE);
            arial14format = new WritableCellFormat(arial14font);
            arial14format.setAlignment(jxl.format.Alignment.CENTRE);
            arial14format.setBorder(jxl.format.Border.ALL, jxl.format.BorderLineStyle.THIN);
            arial14format.setBackground(jxl.format.Colour.VERY_LIGHT_YELLOW);

            arial10font = new WritableFont(WritableFont.ARIAL, 10, WritableFont.BOLD);
            arial10format = new WritableCellFormat(arial10font);
            arial10format.setAlignment(jxl.format.Alignment.CENTRE);
            arial10format.setBorder(jxl.format.Border.ALL, jxl.format.BorderLineStyle.THIN);
            arial10format.setBackground(Colour.GRAY_25);

            arial12font = new WritableFont(WritableFont.ARIAL, 10);
            arial12format = new WritableCellFormat(arial12font);
            arial10format.setAlignment(jxl.format.Alignment.CENTRE);//对齐格式
            arial12format.setBorder(jxl.format.Border.ALL, jxl.format.BorderLineStyle.THIN); //设置边框

        } catch (WriteException e) {
            e.printStackTrace();
        }
    }

    /**
     * 初始化Excel
     *
     * @param fileName
     * @param colName
     */
    public static void initExcel(String fileName, String sheetName, String[] colName, ExcelListener listener) {
        format();
        WritableWorkbook workbook = null;
        try {
            File file = new File(fileName);
            if (!file.getParentFile().exists()) {
                file.getParentFile().mkdirs();
            }

            if (!file.exists()) {
                file.createNewFile();
            }
            workbook = Workbook.createWorkbook(file);
            WritableSheet sheet = workbook.createSheet(sheetName, 0);
            //创建标题栏
            sheet.addCell((WritableCell) new Label(0, 0, fileName, arial14format));
            for (int col = 0; col < colName.length; col++) {
                sheet.addCell(new Label(col, 0, colName[col], arial10format));
            }
            sheet.setRowView(0, 340); //设置行高
            workbook.write();
        } catch (Exception e) {
            e.printStackTrace();
            listener.onExcelResult(-1, e.getMessage());
            return;
        } finally {
            if (workbook != null) {
                try {
                    workbook.close();
                } catch (Exception e) {
                    e.printStackTrace();
                    listener.onExcelResult(-2, e.getMessage());
                    return;
                }
            }
        }
        listener.onExcelResult(0, "success");
    }

    @SuppressWarnings("unchecked")
    public static void writeObjListToExcel(boolean isOffline, List<LocalARBean> objList, String fileName, ExcelListener listener) {
        if (objList == null || objList.size() < 1) {
            listener.onExcelResult(-1, "data is null");
            return;
        }
        WritableWorkbook writebook = null;
        InputStream in = null;
        try {
            WorkbookSettings setEncode = new WorkbookSettings();
            setEncode.setEncoding(UTF8_ENCODING);
            in = new FileInputStream(new File(fileName));
            Workbook workbook = Workbook.getWorkbook(in);
            writebook = Workbook.createWorkbook(new File(fileName), workbook);
            WritableSheet sheet = writebook.getSheet(0);

            File file;
            for (int j = 0; j < objList.size(); j++) {
                LocalARBean projectBean = objList.get(j);
                if (projectBean == null) {
                    continue;
                }
                List<String> list = new ArrayList<>();
                String nameCache = projectBean.getName();
                if (AppInfo.PASSTYPE_UNKNOW.equals(projectBean.getPassType())) { //陌生人的通行方式，这里为了照顾，身份证比对
                    if (nameCache != null && nameCache.length() > 1) {
                        nameCache = projectBean.getName();
                    } else {
                        nameCache = MyApp.getInstance().getString(R.string.stranger);
                    }
                    LogUtil.attendance("====导出的人的名字=陌生人方式====" + nameCache);
                } else {
                    if (nameCache == null || nameCache.length() < 1) {
                        nameCache = MyApp.getInstance().getString(R.string.stranger);
                    }
                }
                LogUtil.attendance("====导出的人的名字=====" + nameCache);
                list.add(nameCache);
                String type = MyApp.getInstance().getString(R.string.unknown);
                switch (projectBean.getPassType()) {
                    case AppInfo.PASSTYPE_FACE:
                        type = MyApp.getInstance().getString(R.string.face_recognition);
                        break;
                    case AppInfo.PASSTYPE_CARD:
                        type = MyApp.getInstance().getString(R.string.use_card);
                        break;
                    case AppInfo.PASSTYPE_UNKNOW:
                        type = MyApp.getInstance().getString(R.string.face_recognition);
                        break;
                    case AppInfo.PASSTYPE_TEMP_PWD:
                        type = MyApp.getInstance().getString(R.string.temporary_password);
                        break;
                }
                list.add(type);
                String cardNum = projectBean.getCardNum() == null || projectBean.getCardNum().isEmpty() ? MyApp.getInstance().getString(R.string.none) : projectBean.getCardNum();
                list.add(cardNum);
                list.add(projectBean.getTemperature());
                list.add(projectBean.getRecordTime());
                list.add(projectBean.getFilePath());
                for (int i = 0; i < list.size(); i++) {
                    String cell = list.get(i);
                    if (cell == null) {
                        cell = "";
                    }
                    sheet.addCell(new Label(i, j + 1, cell, arial12format));
                    if (i == list.size() - 1 && isOffline) {
                        sheet.setColumnView(i, 13);
                    } else {
                        if (cell.length() <= 4) {
                            //设置列宽
                            sheet.setColumnView(i, cell.length() + 8);
                        } else {
                            //设置列宽
                            sheet.setColumnView(i, cell.length() + 5);
                        }
                    }
                }
                //设置行高
//                if (isOffline) {
//                    sheet.setRowView(j + 1, 1450);
//                } else {
                sheet.setRowView(j + 1, 350);
//                }
            }

            writebook.write();
            listener.onExcelResult(0, "success");
//                Toast.makeText(c, "导出Excel成功", Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            e.printStackTrace();
            listener.onExcelResult(-2, e.getMessage());
        } finally {
            if (writebook != null) {
                try {
                    writebook.close();
                } catch (Exception e) {
                    listener.onExcelResult(-2, e.getMessage());
                    e.printStackTrace();
                }

            }
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    listener.onExcelResult(-2, e.getMessage());
                    e.printStackTrace();
                }
            }
        }
    }


    public interface ExcelListener {
        void onExcelResult(int code, String msg);
    }
}
