package com.ys.acsdev.util;

import com.ys.acsdev.MyApp;
import com.ys.acsdev.R;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class SimpleDateUtil {

    /***
     * 将String时间转化成long__时间戳
     * @param time
     */
    public static long StringToLongTime(String time) {
        long timeBack = -1;
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            Date dateStart = format.parse(time);
            timeBack = dateStart.getTime();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return timeBack;
    }

    public static long StringToLongTimeMini(String time) {
        long timeBack = -1;
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date dateStart = format.parse(time);
            timeBack = dateStart.getTime();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return timeBack;
    }

    /**
     * 将long转化成时间戳
     *
     * @param time
     * @return
     */
    public static long StringToLongTimePower(String time) {
        long timeBack = -1;
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmss");
        try {
            Date dateStart = format.parse(time);
            timeBack = dateStart.getTime();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return timeBack;
    }


    /***
     * 获取当前的星期
     * @return
     */
    public static int getCurrentWeekDay() {
        Calendar calendar = Calendar.getInstance();
        return getWeekDayByCal(calendar);
    }

    /**
     * 获取昨天的星期
     *
     * @return
     */
    public static int getLastWeekDay() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, -1);
        return getWeekDayByCal(calendar);
    }

    /**
     * 获取明天的星期
     *
     * @return
     */
    public static int getNextWeekDay() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, 1);
        return getWeekDayByCal(calendar);
    }

    public static int getWeekDayByCal(Calendar calendar) {
        int weekDay = 0;
        int currentDayWeek = calendar.get(Calendar.DAY_OF_WEEK);  //今天的工作星期
        switch (currentDayWeek) {
            case 1: //7
                weekDay = 7;
                break;
            case 2: //1
                weekDay = 1;
                break;
            case 3: //2
                weekDay = 2;
                break;
            case 4: //3
                weekDay = 3;
                break;
            case 5: //4
                weekDay = 4;
                break;
            case 6: //5
                weekDay = 5;
                break;
            case 7: //6
                weekDay = 6;
                break;
        }
        return weekDay;
    }

    public static String formatTaskTimeShow(long time) {
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(time);
    }

    public static String formatNowTime() {
        long time = System.currentTimeMillis();
        return new SimpleDateFormat("MM-dd-yyyy HH:mm").format(time);
    }

    public static String formatCurrentTimeMin() {
        long time = System.currentTimeMillis();
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(time);
    }


    public static String format(String paramString) {
        long l = Long.parseLong(paramString);
        return new SimpleDateFormat("yyyy/MM/dd/ HH:mm:ss").format(l);
    }

    /***
     * 将时间戳转换成时间数据
     * @param paramLong
     * @return
     */
    public static long formatBig(long paramLong) {
        return Long.parseLong(new SimpleDateFormat("yyyyMMddHHmmss").format(paramLong));
    }

    public static String formatBigString(long paramLong) {
        return new SimpleDateFormat("yyyyMMddHHmmss").format(paramLong);
    }

    /***
     * 获取日期
     * @return
     */
    public static String getDate() {
        long l = System.currentTimeMillis();
        String str = new SimpleDateFormat("yyyy/MM/dd").format(l);
        return str;
    }

    /**
     * 获取昨天的日期
     *
     * @return
     */
    public static String getLastDateSingle() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, -1);
        String str = new SimpleDateFormat("yyyy-MM-dd").format(calendar.getTime());
        return str;
    }

    /**
     * 获取明天的日期
     *
     * @return
     */
    public static String getNextDateSingle() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, 1);
        String str = new SimpleDateFormat("yyyy-MM-dd").format(calendar.getTime());
        return str;
    }

    /**
     * 获取前天的日期
     *
     * @return
     */
    public static String getBeforeLastDateSingle() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, -2);
        String str = new SimpleDateFormat("yyyy-MM-dd").format(calendar.getTime());
        return str;
    }


    public static String getDateSingle() {
        long l = System.currentTimeMillis();
        String str = new SimpleDateFormat("yyyy-MM-dd").format(l);
        return str;
    }


    public static String getTime() {
        long time = System.currentTimeMillis();
        String str = new SimpleDateFormat("HH:mm").format(time);
        return str;
    }

    public static String getCurrentTime() {
        long time = System.currentTimeMillis();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm:ss");
        String str = simpleDateFormat.format(time);
        return str;
    }


    public static String getCurrentTimeJSTHm() {
        long time = System.currentTimeMillis();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        String str = simpleDateFormat.format(time);
        String week = "  周" + getWeek();
        return str + week;


//        long time = System.currentTimeMillis();
//        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM-dd-yyyy hh:mm");
//        String str = simpleDateFormat.format(time);
//        Calendar mCalendar = Calendar.getInstance();
//        mCalendar.setTimeInMillis(time);
//        int apm = mCalendar.get(Calendar.AM_PM);
//        if (apm == 0) {
//            str = str + "  AM";
//        } else {
//            str = str + "  PM";
//        }
//        return str;
    }

    public static String getTimeByMillis(long time) {
        String str = new SimpleDateFormat("HH:mm").format(time);
        return str;
    }

    public static String getCurrentTimelONG() {
        long time = System.currentTimeMillis();
        String timeBack = new SimpleDateFormat("yyyyMMddHHmmss").format(time);
        return timeBack;
    }


    public static String getWeek() {
        Date localDate = new Date();
        Calendar localCalendar = Calendar.getInstance();
        localCalendar.setTime(localDate);
        int j = localCalendar.get(Calendar.DAY_OF_WEEK) - 1;
        int i = j;
        if (j < 0) {
            i = 0;
        }
        return new String[]{MyApp.getInstance().getString(R.string.sunday),
                MyApp.getInstance().getString(R.string.monday),
                MyApp.getInstance().getString(R.string.tuesday),
                MyApp.getInstance().getString(R.string.wednesday),
                MyApp.getInstance().getString(R.string.thursday),
                MyApp.getInstance().getString(R.string.friday),
                MyApp.getInstance().getString(R.string.saturday),
        }[i];
    }

    public static String getCurrentMin(long currentTime) {
        return new SimpleDateFormat("mm").format(currentTime);
    }

    /***
     * 获取当前的日期
     * @return
     */
    public static long getCurrentDateLong() {
        long current = System.currentTimeMillis();
        String currentDate = new SimpleDateFormat("yyyyMMdd").format(current);
        return Long.parseLong(currentDate);
    }

    /**
     * 获取当前的时分
     *
     * @return
     */
    public static int getCurrentHourMin() {
        long current = System.currentTimeMillis();
        String timeString = new SimpleDateFormat("HHmm").format(current);
        return Integer.parseInt(timeString);
    }


    /**
     * 获取当前的时分秒
     *
     * @return
     */
    public static long getCurrentHourMinSecondByTime(long millis) {
        String backTime = new SimpleDateFormat("HHmmss").format(millis);
        return Long.parseLong(backTime);
    }

    /**
     * 获取当日零点
     *
     * @return
     */
    public static Date getStartTime() {
        Calendar todayStart = Calendar.getInstance();
        todayStart.set(Calendar.HOUR_OF_DAY, 0);
        todayStart.set(Calendar.MINUTE, 0);
        todayStart.set(Calendar.SECOND, 0);
        todayStart.set(Calendar.MILLISECOND, 0);
        return todayStart.getTime();
    }

    /**
     * 获取昨日零点
     *
     * @return
     */
    public static Date getLastStartTime() {
        Calendar todayStart = Calendar.getInstance();
        todayStart.add(Calendar.DATE, -1);
        todayStart.set(Calendar.HOUR_OF_DAY, 0);
        todayStart.set(Calendar.MINUTE, 0);
        todayStart.set(Calendar.SECOND, 0);
        todayStart.set(Calendar.MILLISECOND, 0);
        return todayStart.getTime();
    }


    /**
     * 获取当日23：59：59:999
     *
     * @return
     */
    public static Date getnowEndTime() {
        Calendar todayEnd = Calendar.getInstance();
        todayEnd.set(Calendar.HOUR_OF_DAY, 23);
        todayEnd.set(Calendar.MINUTE, 59);
        todayEnd.set(Calendar.SECOND, 59);
        todayEnd.set(Calendar.MILLISECOND, 999);
        return todayEnd.getTime();
    }

    /**
     * 获取明日23：59：59:999
     *
     * @return
     */
    public static Date getNextEndTime() {
        Calendar todayEnd = Calendar.getInstance();
        todayEnd.add(Calendar.DATE, 1);
        todayEnd.set(Calendar.HOUR_OF_DAY, 23);
        todayEnd.set(Calendar.MINUTE, 59);
        todayEnd.set(Calendar.SECOND, 59);
        todayEnd.set(Calendar.MILLISECOND, 999);
        return todayEnd.getTime();
    }

}
