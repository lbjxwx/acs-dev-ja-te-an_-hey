package com.ys.acsdev.util;


import com.ys.acsdev.commond.AppInfo;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.lang.Thread.UncaughtExceptionHandler;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class CrashExceptionHandler implements UncaughtExceptionHandler {
    private static CrashExceptionHandler carshInstance;

    @Override
    public void uncaughtException(Thread td, Throwable tb) {
        tb.printStackTrace();
        writeToSDCard(td, tb);
        if (td.getId() == 1) {// UI异常

        } else {

        }
        android.os.Process.killProcess(android.os.Process.myPid());
        System.exit(1);
    }

    public void init() {
        Thread.setDefaultUncaughtExceptionHandler(this);
    }

    public void writeToSDCard(Thread td, Throwable tb) {
        FileUtils.creatPathNotExcit();
        FileOutputStream fos = null;
        try {
            String crashLogPath = AppInfo.BASE_CRASH_LOG;
            File logFile = new File(crashLogPath);
            LogUtil.e("tb===" + tb.toString(), "crash");
            if (!logFile.exists()) {
                if (!logFile.mkdirs()) {
                    LogUtil.e("create crash file fail", "crash");
                    return;
                }
            }
            String logPath = crashLogPath + "/"
                    + new SimpleDateFormat("yyMMdd_HHmm", Locale.getDefault())
                    .format(new Date()) + ".txt";
            File file = new File(logPath);
            if (!file.exists()) {
                file.createNewFile();
            }
            fos = new FileOutputStream(logPath);
            PrintStream ps = new PrintStream(fos);
            tb.printStackTrace(ps);
            ps.flush();
            ps.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public CrashExceptionHandler() {

    }

    public static CrashExceptionHandler getCrashInstance() {
        if (carshInstance == null) {
            synchronized (CrashExceptionHandler.class) {
                if (carshInstance == null) {
                    carshInstance = new CrashExceptionHandler();
                }
            }
        }
        return carshInstance;
    }
}