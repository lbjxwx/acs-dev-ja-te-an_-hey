package com.ys.acsdev.util.capture;

import android.content.Context;
import android.os.Handler;
import android.os.Message;

import com.ys.acsdev.commond.AppInfo;
import com.ys.acsdev.util.LogUtil;

import java.io.File;
import java.io.OutputStream;

/**
 * 截图
 */
public class CaptureRunnable implements Runnable {

    Context context;
    CaptureImageListener listener;

    /**
     * 截图工具
     *
     * @param context
     * @param listener 图片上传监听
     */
    public CaptureRunnable(Context context, CaptureImageListener listener) {
        this.context = context;
        this.listener = listener;
    }

    @Override
    public void run() {
        LogUtil.cdl("利用API截图，速度慢", true);
        captuenPic();
    }

    private void captuenPic() {
        String picPath = AppInfo.CAPTURE_IMAGE_PATH;
        LogUtil.update("===================开始截图==" + System.currentTimeMillis());
        try {
            File dirFile = new File(picPath);
            if (!dirFile.exists()) {
                dirFile.createNewFile();
            }
            //请求root权限
            Process su = Runtime.getRuntime().exec("su");
            //以下两句代表重启设备
            // String strcmd = "/system/bin/screencap -p " + picPath;
            //0 主屏  1 副屏
            String strcmd = "screencap -d 0 -p " + picPath;
            strcmd = strcmd + "\n exit\n";
            OutputStream os = su.getOutputStream();
            os.write(strcmd.getBytes());
            os.flush();
            os.close();
            if ((su.waitFor() != 0)) {
                throw new SecurityException();
            }
            LogUtil.update("===================截图结束==" + System.currentTimeMillis());
            Message message = new Message();
            message.what = MESSAGE_SUCCESS;
            message.obj = picPath;
            handler.sendMessageDelayed(message, 500);
        } catch (Exception e) {
            LogUtil.update("===截图失败==" + e.getMessage());
            handler.sendEmptyMessage(MESSAGE_FAILED);
            e.printStackTrace();
        }
    }

    private static final int MESSAGE_SUCCESS = 786;
    private static final int MESSAGE_FAILED = 787;
    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (listener == null) {
                return;
            }
            if (msg.what == MESSAGE_FAILED) {
                listener.getCaptureImagePath(false, "");
            } else if (msg.what == MESSAGE_SUCCESS) {
                String savePath = (String) msg.obj;
                listener.getCaptureImagePath(true, savePath);
            }
        }
    };


}
