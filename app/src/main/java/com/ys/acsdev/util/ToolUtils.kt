package com.ys.acsdev.util

import com.ys.acsdev.bean.LocalARBean
import com.ys.acsdev.commond.AppInfo
import com.ys.acsdev.data.MySDCard
import com.ys.acsdev.runnable.ImageRoateRunnable
import com.ys.acsdev.runnable.upload.RoateImageListener
import com.ys.acsdev.service.AcsService

object ToolUtils {

    //对离线通行记录进行时间倒序
    @JvmStatic
    fun sortLocalAR(source: List<LocalARBean>): List<LocalARBean> {
        return source.sortedByDescending { it.recordTime }
    }


    @JvmStatic
    fun roatetranBitmap(
        previewData: ByteArray,
        width: Int,
        height: Int,
        rotate: Int = 0,
        roateImageListener: RoateImageListener
    ) {
        var SAVE_PIC_SIZE = 1024;
        var LAST_SD_SIZE =
            MySDCard.getAvailableExternalMemorySize(
                AppInfo.BASE_PATH_INNER,
                1024 * 1024
            );  //获取得单位 M
        if (LAST_SD_SIZE < SAVE_PIC_SIZE) {
            LogUtil.cdl("内存检测--内存不够了===中止操作==")
            roateImageListener.backImageListener(false, "内存检测--内存不够了===中止操作==")
            return
        }
        LogUtil.cdl("内存检测--剩余内存==" + LAST_SD_SIZE)
        var roateRunnable =
            ImageRoateRunnable(previewData, width, height, rotate, roateImageListener);
        AcsService.mInstance?.executor(roateRunnable);
    }

//    /***
//     * bitmap保存到本地
//     */
//    fun sateBitmapToLocal(bitmap: Bitmap?, savePath: String, listener: WriteBitmapToLocalListener) {
//        var bitmapWriteLocalRunnable = BitmapWriteLocalRunnable(bitmap, savePath, listener);
//        MyService.mInstance?.executor(bitmapWriteLocalRunnable)
//    }

    /**
     * 将byte[]数组转化为String类型
     * @param arg
     * 需要转换的byte[]数组
     * @return 转换后的String队形
     */
    @JvmStatic
    fun toHexString(arg: ByteArray?): String {
        if (arg == null) {
            return ""
        }
        var result = ""
        arg.forEach {
            val temp = Integer.toHexString(if (it < 0) it + 256 else it.toInt())
            result += if (temp.length == 1) "0" + temp else temp
            result += " "
        }
        return result
    }
}