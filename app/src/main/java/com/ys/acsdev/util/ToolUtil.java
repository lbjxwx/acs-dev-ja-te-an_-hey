package com.ys.acsdev.util;


import android.util.Base64;

public class ToolUtil {

    /***
     * Text 转 Base64
     * @param textDesc
     * @return
     *
     */
    public static String textToBase64Info(String textDesc) {
//        Base64 base64 = new Base64();
//        String base64Sign = base64.encodeToString(textDesc.getBytes());

        String base64Sign = Base64.encodeToString(textDesc.getBytes(), Base64.DEFAULT);
        return base64Sign;
    }


    /**
     * 将字节数组转十六进制String打印
     *
     * @param bytes
     * @return
     */
    public static String bytesToHex(byte[] bytes) {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < bytes.length; i++) {
            String hex = Integer.toHexString(bytes[i] & 0xFF);
            if (hex.length() < 2) {
                sb.append(0);
            }
            sb.append(hex);
        }
        return sb.toString();
    }


    //将字节数组转为字符串
    public static String decode(byte[] no) {
        String result = "";
        try {
            long t = 0;
            final int start = 3;
            final int end = -1;
            for (int i = start; i > end; i--) {
                t <<= 8;
                t |= no[i] & 0xff;
            }
            result = String.valueOf(t);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }


    //将十六进制的数组数据转为十进制数据
    public static String hexToDecimal(byte[] data) {
        String result = "";
        try {
            String s = bytesToHex(data);
            long l = Long.parseLong(s, 16);
            result = l + "";
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

}
