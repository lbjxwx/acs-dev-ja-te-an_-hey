package com.ys.acsdev.util;


import android.app.Activity;
import android.content.Context;
import android.util.DisplayMetrics;

import com.ys.acsdev.bean.LocalARBean;
import com.ys.acsdev.commond.AppInfo;
import com.ys.acsdev.data.MySDCard;
import com.ys.acsdev.db.DbTempCompensate;
import com.ys.acsdev.db.dao.LocalARDao;
import com.ys.acsdev.light.LightManager;
import com.ys.acsdev.service.AcsService;
import com.ys.acsdev.ui.SplashActivity;

import java.io.File;
import java.util.List;

/***
 * 用来初始化程序的工具类
 */
public class InitInfoUtil {


    public static void initSplashInfo() {
        //清理缓存信息。安装包留下来的缓存信息
        RootCmd.clearApkCache();
        FileUtils.deleteDirOrFile(AppInfo.BASE_APK_PATH, "开机删除APk缓存文件");
        FileUtils.delOldSaveLog();
        //更新温度补偿值
        DbTempCompensate.getCurrentTempAdd();
        //删除缓存文件
        if (SpUtils.isFirstLogin()) {
            File file = new File(AppInfo.BASE_LOCAL_PATH);
            if (file.exists()) {
                FileUtils.deleteDirOrFile(file, "第一次进入应用，将缓存文件删除");
            }
        }
    }

    public static void getDefaultInfo(Activity context) {
        getScreenSize(context);
        getSavePersonNum();
    }

    private static void getSavePersonNum() {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                List<LocalARBean> localARBeanList = LocalARDao.getAll();
                if (localARBeanList == null || localARBeanList.size() < 1) {
                    return;
                }
                AppInfo.CURRENT_SAVE_PERSON_NUM = localARBeanList.size();
            }
        };
        AcsService.getInstance().executor(runnable);
    }

    /***
     * 获取屏幕的分辨率尺寸
     * @param context
     */
    private static void getScreenSize(Activity context) {
        DisplayMetrics localDisplayMetrics = new DisplayMetrics();
        context.getWindowManager().getDefaultDisplay().getRealMetrics(localDisplayMetrics);
        int width = localDisplayMetrics.widthPixels;
        int height = localDisplayMetrics.heightPixels;
        SpUtils.setScreenwidth(width);
        SpUtils.setScreenheight(height);
    }


}
