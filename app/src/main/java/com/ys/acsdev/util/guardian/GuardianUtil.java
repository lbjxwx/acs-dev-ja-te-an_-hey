package com.ys.acsdev.util.guardian;

import android.content.Context;
import android.content.Intent;

import com.ys.acsdev.api.AppConfig;
import com.ys.acsdev.commond.AppInfo;

public class GuardianUtil {

    /**
     * 通知守护进程，从现在开始计时
     * 一般退出程序得时候调用
     */
    public static void startToGuardianTime(Context context) {
        Intent intent = new Intent(AppInfo.START_PROJECTOR_GUARDIAN_TIMER);
        context.sendBroadcast(intent);
    }

    /****
     * 修改是否显示选择APP 得button
     * @param context
     */
    public static void modifyAppShowChooicePackageButton(Context context) {
        Intent intent = new Intent(AppInfo.MODIFY_APP_PACKAGE_CHOOICE);
        intent.putExtra(AppInfo.MODIFY_APP_PACKAGE_CHOOICE, false);
        context.sendBroadcast(intent);
    }

    /**
     * 修改守护进程得包名
     *
     * @param context
     */
    public static void modifyGuardianPackageName(Context context) {
        Intent intent = new Intent();
        intent.putExtra("packageName", "com.ys.acsdevs");
        String action = AppInfo.MODIFY_GUARDIAN_PACKAGENAME;
        intent.setAction(action);
        context.sendBroadcast(intent);
    }

    /**
     * 修改守护进程得状态
     *
     * @param context
     * @param enable
     */
    public static void modifyGuardianStatues(Context context, boolean enable) {
        Intent intent = new Intent(AppInfo.CHANGE_GUARDIAN_STATUES);
        intent.putExtra(AppInfo.CHANGE_GUARDIAN_STATUES, enable);
        context.sendBroadcast(intent);
    }


    /**
     * 修改守护进程得时间
     *
     * @param context
     * @param time
     */
    public static void modifyGuardianTime(Context context, int time) {
        if (time < 15 * 1000) {
            return;
        }
        Intent intent = new Intent(AppInfo.MODIFY_GUARDIAN_TIME);
        intent.putExtra(AppInfo.MODIFY_GUARDIAN_TIME, time);
        context.sendBroadcast(intent);
    }

    public static void modifyLauncherDelayTime(Context context, int time) {
        try {
            Intent intent = new Intent(AppInfo.MODIFY_START_DELAY_TIME);
            intent.putExtra(AppInfo.MODIFY_START_DELAY_TIME, time);
            context.sendBroadcast(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 修改延时启动得动画
     *
     * @param context
     * @param isShowAnimal
     */
    public static void modifyLauncherAnimal(Context context, boolean isShowAnimal) {
        try {
            Intent intent = new Intent(AppInfo.MODIFY_START_DELAY_ANIMAL);
            intent.putExtra(AppInfo.MODIFY_START_DELAY_ANIMAL, isShowAnimal);
            context.sendBroadcast(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
