package com.ys.acsdev.util.file;

import android.content.Context;

import com.ys.acsdev.R;
import com.ys.acsdev.bean.LocalARBean;
import com.ys.acsdev.commond.CommondCallBack;
import com.ys.acsdev.service.AcsService;
import com.ys.acsdev.util.ExcelUtil;
import com.ys.acsdev.util.LogUtil;

import java.util.List;

public class ExportFileUtil {

    Context context;

    public ExportFileUtil(Context contex) {
        this.context = contex;
    }

    public void writExcelSd(boolean isOffline, List<LocalARBean> data, String fileSavePath, CommondCallBack callBack) {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                if (data == null || data.size() < 1) {
                    callBack.onCommondCallBack(1, context.getString(R.string.no_data));
                    return;
                }
                LogUtil.export("====文件导出得路径===" + fileSavePath);
                String[] title = {
                        context.getString(R.string.tv_name),
                        context.getString(R.string.tv_passtype),
                        context.getString(R.string.tv_cardno),
                        context.getString(R.string.tv_temp),
                        context.getString(R.string.tv_access_time),
                        context.getString(R.string.tv_face)};
                ExcelUtil.initExcel(fileSavePath, context.getString(R.string.access_record), title, (code, msg) -> {
                    if (code != 0) {
                        callBack.onCommondCallBack(code, context.getString(R.string.export_failed) + fileSavePath + " / " + msg);
                        return;
                    }
                    ExcelUtil.writeObjListToExcel(isOffline, data, fileSavePath, (code1, msg1) -> {
                        if (code1 != 0) {
                            callBack.onCommondCallBack(code, context.getString(R.string.export_failed) + fileSavePath + " / " + msg1);
                            return;
                        }
                        callBack.onCommondCallBack(0, context.getString(R.string.export_success) + fileSavePath);
                    });
                });
            }
        };
        AcsService.getInstance().executor(runnable);
    }
}
