package com.ys.acsdev.util.language;

import android.content.Context;
import android.content.res.Configuration;

import com.ys.acsdev.R;
import com.ys.acsdev.setting.entity.LanguageEntity;
import com.ys.acsdev.util.LogUtil;
import com.ys.acsdev.util.SpUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class LanguageChangeUtil {

    Context context;


    public LanguageChangeUtil(Context context) {
        this.context = context;
    }


    /**
     * 获取当前语言
     *
     * @return
     */
    public String getLocaleLanguage() {
        Locale locale = Locale.getDefault();
        String language = locale.getLanguage();
        LogUtil.cdl("======获取得语言===" + language);
        return language;
    }

    /**
     * 程序启动，更新本地语言
     */
    public void uploadLanguageFromLocal() {
        String language = getLocaleLanguage();
        if (language.contains(LanguageEntity.LANGUAGE_TYPE_CHAINA)) {
            SpUtils.setLanguageDesc(0);
        } else if (language.contains(LanguageEntity.LANGUAGE_TYPE_ENGLISH)) {
            SpUtils.setLanguageDesc(1);
        }
        changeLanguage();
    }

    /**
     * 设置当前语言
     */
    public void changeLanguage() {
        int languageStatues = SpUtils.getLanguageDesc();
        Configuration config = context.getResources().getConfiguration();
        switch (languageStatues) {
            case 0:  //中文
                Locale.setDefault(Locale.CHINESE);
                config.locale = Locale.CHINESE;
                break;
            case 1: //英文
                Locale.setDefault(Locale.ENGLISH);
                config.locale = Locale.ENGLISH;
                break;
            case 2://意大利文
                break;
        }
        context.getResources().updateConfiguration(config, context.getResources().getDisplayMetrics());
    }


    public List<LanguageEntity> getLanguageInfo() {
        List<LanguageEntity> languageEntityList = new ArrayList<LanguageEntity>();
        languageEntityList.add(new LanguageEntity(R.mipmap.icon_cn, "中文", LanguageEntity.LANGUAGE_TYPE_CHAINA));
        languageEntityList.add(new LanguageEntity(R.mipmap.icon_en, "英文", LanguageEntity.LANGUAGE_TYPE_ENGLISH));
        languageEntityList.add(new LanguageEntity(R.mipmap.icon_ita, "意大利文", LanguageEntity.LANGUAGE_TYPE_ITALIAN));
        return languageEntityList;
    }


    public static String getLanguageFromResurce(Context context, int resourceId) {
        if (context == null) {
            return null;
        }
        String desc = "";
        try {
            desc = context.getResources().getString(resourceId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return desc;
    }

    public static String getLanguageFromResurceWithPosition(Context context, int resourceId, String desc) {
        if (context == null) {
            return null;
        }
        String startResult = "";
        try {
            String stringStart = context.getResources().getString(resourceId);
            startResult = String.format(stringStart, desc);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return startResult;
    }


}
