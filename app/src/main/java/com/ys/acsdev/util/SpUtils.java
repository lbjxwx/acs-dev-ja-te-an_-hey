package com.ys.acsdev.util;

import com.alibaba.fastjson.JSON;
import com.cn.sdk.demo.ConfigInfo;
import com.google.gson.Gson;
import com.nlp.cloundcbsappdemo.util.Const;
import com.ys.acsdev.MyApp;
import com.ys.acsdev.api.AppConfig;
import com.ys.acsdev.bean.DeviceBean;
import com.ys.acsdev.commond.AppInfo;
import com.ys.acsdev.setting.entity.TempModelEntity;

import java.util.ArrayList;
import java.util.Arrays;

public class SpUtils {


    /***
     * 公交卡数据
     * @return
     */
    public static boolean getCarCardEnable() {
        return (boolean) MyApp.getInstance().getData("carCardEnable", false);
    }

    public static void setCarCardEnable(boolean carCardEnable) {
        MyApp.getInstance().saveData("carCardEnable", carCardEnable);
    }

    /***
     * 人证比对 + 健康吗
     * @return
     */
    public static boolean getHealthyCompareEnable() {
        return (boolean) MyApp.getInstance().getData("HealthyCompareEnable", true);
    }

    public static void setHealthyCompareEnable(boolean HealthyCompareEnable) {
        MyApp.getInstance().saveData("HealthyCompareEnable", HealthyCompareEnable);
    }

    /***
     * 人证比对
     * @return
     */
    public static boolean getCompareEnable() {
        return (boolean) MyApp.getInstance().getData("CompareEnable", false);
    }

    public static void setCompareEnable(boolean CompareEnable) {
        MyApp.getInstance().saveData("CompareEnable", CompareEnable);
    }

    /***
     * 获取刷卡得串口选择
     * @return
     */
    public static String getCardTtysChooice() {
        String defValue = "/dev/ttyS2";
        String cardTtysChooice = ((String) MyApp.getInstance().getData("cardTtysChooice", defValue));
        return cardTtysChooice;
    }

    public static void setCardTtysChooice(String cardTtysChooice) {
        MyApp.getInstance().saveData("cardTtysChooice", cardTtysChooice);
    }

    /**
     * 是否加载广告
     *
     * @return
     */
    public static boolean getShowAdInfo() {
        boolean isDefaultStatues = true;
        boolean showAdInfo = ((boolean) MyApp.getInstance().getData("showAdInfo", false));
        return showAdInfo;
    }

    public static void setShowAdInfo(boolean showAdInfo) {
        MyApp.getInstance().saveData("showAdInfo", showAdInfo);
    }

//    /***
//     * 健康码的地区
//     * @return
//     */
//    public static String getHealthAddress() {
//        String cpuModel = ((String) MyApp.getInstance().getData("HealthAddress", "广东省深圳市"));
//        return cpuModel;
//    }
//
//    /***
//     * 健康码的地区
//     */
//    public static void setHealthAddress(String value) {
//        MyApp.getInstance().saveData("HealthAddress", value);
//    }

    /***
     * 图片上传分辨
     * 0：低分辨率
     * 1：中分辨率
     * 2：高分辨率
     * @return
     */
    public static int getUpdatePicQuity() {
        return (int) MyApp.getInstance().getData("UpdatePicQuity", 0);
    }

    /***
     * 图片上传分辨
     * @return
     */
    public static void setUpdatePicQuity(int UpdatePicQuity) {
        MyApp.getInstance().saveData("UpdatePicQuity", UpdatePicQuity);
    }

    /***
     * 获取上一次得请求时间撮
     * @return
     */
    public static long getUpdatePersonTime() {
        return (long) MyApp.getInstance().getData("updateTime", 0L);
    }

    /***
     * 设定下次请求得时间
     * @param
     */
    public static void setUpdatePersonTime(long updateTime, String tag) {
        LogUtil.persoon("==setUpdatePersonTime==" + updateTime + " / " + tag);
        MyApp.getInstance().saveData("updateTime", updateTime);
    }

    /***
     * 操作完成，获取请求时间，设定同步时间
     */
    public static long getStartTime() {
        return (long) MyApp.getInstance().getData("startTime", 0L);
    }

    /***
     * 请求开始，设定当前时间
     * @param
     */
    public static void setStartTime(long startTime) {
        MyApp.getInstance().saveData("startTime", startTime);
    }


    //上传人脸图片开关
    public static boolean getUploadFace() {
        boolean def = true;
        return (boolean) MyApp.getInstance().getData("UploadFace", def);
    }


    public static void setUploadFace(boolean value) {
        MyApp.getInstance().saveData("UploadFace", value);
    }


    //隐藏wifi图标
    public static boolean getHideWifiIcon() {
        boolean def = false;
        return (boolean) MyApp.getInstance().getData("HideWifiIcon", def);
    }

    public static void setHideWifiIcon(boolean value) {
        MyApp.getInstance().saveData("HideWifiIcon", value);
    }

    /***
     * 扫描健康码
     * @return
     */
    public static boolean getHealthCodeEnable() {
        return (boolean) MyApp.getInstance().getData("HealthCodeEnable", false);
    }

    /****
     * 扫描健康码
     * @param value
     */
    public static void setHealthCodeEnable(boolean value) {
        MyApp.getInstance().saveData("HealthCodeEnable", value);
    }

    /***
     * 设置复位
     */
    public static void setSharedDefaultInfo() {
        boolean isInitFace = SpUtils.getmInitFace();
        MyApp.getInstance().clearAllSharedInfo();
        SpUtils.setmInitFace(isInitFace);
    }


    //自动关门时间
    public static float getCloseDoorTime() {
        return (float) MyApp.getInstance().getData("CloseDoorTime", 1f);
    }

    public static void setCloseDoorTime(float value) {
        MyApp.getInstance().saveData("CloseDoorTime", value);
    }

    //休眠时间
    public static int getSleepTime() {
        int defaultTime = 30;
        return (int) MyApp.getInstance().getData("sleepTime", defaultTime);
    }

    public static void setSleepTime(int value) {
        MyApp.getInstance().saveData("sleepTime", value);
    }

    //单目活体开关，默认开
    public static boolean getSingleLivingEnable() {
        boolean defaultValue = true;
        switch (AppConfig.APP_CUSTOM_TYPE) {
            case AppConfig.APP_CUSTOM_JTA_DEFAULT:
            case AppConfig.APP_CUSTOM_JTA_HID:
            case AppConfig.APP_CUSTOM_JTA_ID_CARD:
                defaultValue = false;
                break;
        }
        return (boolean) MyApp.getInstance().getData("livingEnable", defaultValue);
    }

    /****
     * 单目活体开关
     * @param value
     */
    public static void setSingleLivingEnable(boolean value) {
        MyApp.getInstance().saveData("livingEnable", value);
    }


    /***
     * 打印机串口
     * @return
     */
    public static String getPrintSerialPort() {
        String cpuModel = ((String) MyApp.getInstance().getData("printSerialPort", ""));
        return cpuModel;
    }

    /***
     * 打印机串口
     */
    public static void setPrintSerialPort(String value) {
        MyApp.getInstance().saveData("printSerialPort", value);
    }


    //打印机波特率
    public static int getPrintBaudrate() {
        return (int) MyApp.getInstance().getData("printBaudrate", 0);
    }

    public static void setPrintBaudrate(int data) {
        MyApp.getInstance().saveData("printBaudrate", data);
    }

    /***
     * 隐藏温度
     * 测温显示温度数值
     * @return
     */
    public static boolean getHideTempNumberEnable() {
        boolean defValue = false;
        return (boolean) MyApp.getInstance().getData("hideTempNumber", defValue);
    }

    /****
     * 隐藏温度
     * @param value
     */
    public static void setHideTempNumberEnable(boolean value) {
        MyApp.getInstance().saveData("hideTempNumber", value);
    }

    /***
     * 0：logo
     * 1: face
     * @return
     */
    public static int getShowImageType() {
        int defaultLogoInfo = 0;
        return (int) MyApp.getInstance().getData("ShowImageType", defaultLogoInfo);
    }

    /***
     * 设置底部显示logo还是人脸
     * @param ShowImageType
     */
    public static void setShowImageType(int ShowImageType) {
        MyApp.getInstance().saveData("ShowImageType", ShowImageType);
    }


    /***
     * 高温播放警报音
     * @return
     */
    public static boolean getPlayWearMedia() {
        boolean isDefaultOpen = true;
        return (boolean) MyApp.getInstance().getData("PlayWearMedia", isDefaultOpen);
    }

    public static void setPlayWearMedia(boolean PlayWearMedia) {
        MyApp.getInstance().saveData("PlayWearMedia", PlayWearMedia);
    }

    /**
     * 是否播报语音
     * 默认 true
     *
     * @return
     */
    public static boolean getSpeakTempNum() {
        boolean isDefaultSpeakNum = true;
        return (boolean) MyApp.getInstance().getData("SpeakTempNum", isDefaultSpeakNum);
    }

    /**
     * 是否播报语音
     *
     * @param SpeakTempNum
     */
    public static void setSpeakTempNum(boolean SpeakTempNum) {
        MyApp.getInstance().saveData("SpeakTempNum", SpeakTempNum);
    }


    /***
     * 精简模式，打开后，测温将只播报验证通过与否
     * @return
     */
    public static boolean getSimpleModel() {
        return (boolean) MyApp.getInstance().getData("simpleModel", false);
    }

    public static void setSimpleModel(boolean value) {
        MyApp.getInstance().saveData("simpleModel", value);
    }

    /***
     * 设置精简模式
     * @return
     */
    public static boolean getSettingSimpleModel() {
        boolean isDrefault = false;
        return (boolean) MyApp.getInstance().getData("SettingSimpleModel", isDrefault);
    }

    public static void setSettingSimpleModel(boolean SettingSimpleModel) {
        MyApp.getInstance().saveData("SettingSimpleModel", SettingSimpleModel);
    }

    /***
     * 仅检测不识别开关
     * @return
     */
    public static boolean getOnlyCheckEnable() {
        return (boolean) MyApp.getInstance().getData("onlyCheckEnable", false);
    }

    public static void setOnlyCheckEnable(boolean value) {
        MyApp.getInstance().saveData("onlyCheckEnable", value);
    }

    /***
     * 是否显示体温图标
     * @return
     */
    public static boolean getShowTempIcon() {
        boolean isShow = false;
        return (boolean) MyApp.getInstance().getData("ShowTempIcon", isShow);
    }

    public static void setShowTempIcon(boolean ShowTempIcon) {
        MyApp.getInstance().saveData("ShowTempIcon", ShowTempIcon);
    }


    /***
     * 识别显示开关
     * @return
     */
    public static boolean getRecogShowEnable() {
        boolean defValue = false;
        return (boolean) MyApp.getInstance().getData("recogShowEnable", defValue);
    }

    public static void setRecogShowEnable(boolean value) {
        MyApp.getInstance().saveData("recogShowEnable", value);
    }


    /***
     * 获取CPU的类型
     * @return
     */
    public static String getCpuModel() {
        String cpuModel = ((String) MyApp.getInstance().getData("cpuModel", ""));
        return cpuModel;
    }

    /***
     * 设置CPU的类型
     * @param cpuModel
     */
    public static void setCpuModel(String cpuModel) {
        MyApp.getInstance().saveData("cpuModel", cpuModel);
    }

    /****
     * 工作模式
     * 0网络模式， 1单机模式
     * @return
     */
    public static int getWorkModel() {
        int worlModel = AppInfo.MODEL_NET;
        return (int) MyApp.getInstance().getData("model", worlModel);
    }

    public static void setWokModel(int data) {
        MyApp.getInstance().saveData("model", data);
    }

    /***
     * 访客 显示登记二维码
     * @return
     */
    public static boolean getQrCodeEnable() {
        return (boolean) MyApp.getInstance().getData("qrCodeEnable", false);
    }

    /***
     * 访客 二维码功能
     * @param value
     */
    public static void setQrCodeEnable(boolean value) {
        MyApp.getInstance().saveData("qrCodeEnable", value);
    }

    /***
     * 问禁表功能
     * @return
     */
    public static boolean getShowAskErCode() {
        return (boolean) MyApp.getInstance().getData("isShowAskErCode", false);
    }

    /***
     * 问禁表功能
     * @return
     */
    public static void setShowAskErCode(boolean isShowAskErCode) {
        MyApp.getInstance().saveData("isShowAskErCode", isShowAskErCode);
    }

    /***
     * 口罩拦截
     * @return
     */
    public static boolean getMaskIntercept() {
        boolean defaultOpen = false;
        return (boolean) MyApp.getInstance().getData("maskIntercept", defaultOpen);
    }

    public static void setMaskIntercept(boolean value) {
        MyApp.getInstance().saveData("maskIntercept", value);
    }


    /***
     * 是否播报人名
     * @return
     */
    public static boolean getSpeakName(String printTag) {
//        LogUtil.cdl("==getSpeakName==" + printTag);
        boolean isOpenDefaulr = false;
        return (boolean) MyApp.getInstance().getData("speakName", isOpenDefaulr);
    }

    public static void setSpeakName(boolean speakNameEnable) {
        MyApp.getInstance().saveData("speakName", speakNameEnable);
    }

    /***
     * 是否显示人名
     * @return
     */
    public static boolean getShowPersonName() {
        return (boolean) MyApp.getInstance().getData("showPersonName", false);
    }

    public static void setShowPersonName(boolean showPersonName) {
        MyApp.getInstance().saveData("showPersonName", showPersonName);
    }

    //分辨率宽
    public static int getResolutionWidth() {
        return (int) MyApp.getInstance().getData("ResolutionWidth", 0);
    }

    public static void setResolutionWidth(int width) {
        MyApp.getInstance().saveData("ResolutionWidth", width);
    }

    //分辨率高
    public static int getResolutionHeight() {
        return (int) MyApp.getInstance().getData("ResolutionHeight", 0);
    }

    public static void setResolutionHeight(int height) {
        MyApp.getInstance().saveData("ResolutionHeight", height);
    }

    //离线记录提交时间
    public static String getUpdateStartTime() {
        return (String) MyApp.getInstance().getData("updateStartTime", "01:30");
    }

    //离线记录提交时间
    public static void setUpdateStartTime(String updateStartTime) {
        MyApp.getInstance().saveData("updateStartTime", updateStartTime);
    }

    //离线记录提交时间
    public static String getUpdateEndTime() {
        return (String) MyApp.getInstance().getData("updateEndTime", "05:00");
    }

    //离线记录提交时间
    public static void setUpdateEndTime(String updateEndTime) {
        MyApp.getInstance().saveData("updateEndTime", updateEndTime);
    }

    /***
     * 是否开启语音TTS
     * @return
     */
    public static boolean getOpenTTSVoice() {
        return (boolean) MyApp.getInstance().getData("openTTSVoice", true);
    }

    public static void setOpenTTSVoice(boolean openTTSVoice) {
        MyApp.getInstance().saveData("openTTSVoice", openTTSVoice);
    }

    /***
     * 底部信息显示开关
     * true 显示
     * false 隐藏
     * @return
     */
    public static boolean getInfoEnable() {
        boolean isShow = true;
        return (boolean) MyApp.getInstance().getData("infoEnable", isShow);
    }

    //底部信息显示开关
    public static void setInfoEnable(boolean doorEnable) {
        MyApp.getInstance().saveData("infoEnable", doorEnable);
    }

    //开门开关
    public static boolean getDoorEnable() {
        boolean defValue = false;
        return (boolean) MyApp.getInstance().getData("doorEnable", defValue);
    }

    //设置开门开关
    public static void setDoorEnable(boolean doorEnable) {
        MyApp.getInstance().saveData("doorEnable", doorEnable);
    }

    //考勤开关
    public static boolean getAttendEnable() {
        boolean isDefaultOpen = false;
        return (boolean) MyApp.getInstance().getData("attendEnable", isDefaultOpen);
    }

    //设置考勤开关属性
    public static void setmAttendEnable(boolean attendEnable) {
        MyApp.getInstance().saveData("attendEnable", attendEnable);
    }

    //口罩模式
    public static boolean getmMaskEnable() {
        boolean defaultType = false;
        switch (AppConfig.APP_CUSTOM_TYPE) {
            case AppConfig.APP_CUSTOM_JTA_DEFAULT:
            case AppConfig.APP_CUSTOM_JTA_ID_CARD:
            case AppConfig.APP_CUSTOM_JTA_HID:
                defaultType = true;
                break;
        }

        return (boolean) MyApp.getInstance().getData("maskEnable", defaultType);
    }

    /***
     * 设置口罩模式
     * @param maskEnable
     */
    public static void setMaskEnable(boolean maskEnable) {
        MyApp.getInstance().saveData("maskEnable", maskEnable);
    }

    //温度补偿值
    public static float getmTempComp() {
        return (float) MyApp.getInstance().getData("tempComp", 0f);
    }

    public static void setmTempComp(float tempComp) {
        MyApp.getInstance().saveData("tempComp", tempComp);
    }

    /****
     * 活体检测
     * 红外开关
     * 默认关闭
     * @return
     */
    public static boolean getAutoSwitchCamera() {
        return (boolean) MyApp.getInstance().getData("autoSwitchCamera", false);
    }

    /***
     * 活体检测
     * @param autoSwitchCamera
     */
    public static void setmAutoSwitchCamera(boolean autoSwitchCamera) {
        MyApp.getInstance().saveData("autoSwitchCamera", autoSwitchCamera);
    }

    //有效温度
    public static float getmTempLow() {
        float defValue = 35f;
        return (float) MyApp.getInstance().getData("tempLow", defValue);
    }

    public static void setmTempLow(float tempLow) {
        MyApp.getInstance().saveData("tempLow", tempLow);
    }

    //    测温波特率
//    0    "9600"));
//    1    "19200"));
//    2    "57600"));
//    3    "115200"));
//    4    "230400"));
//    5   "460800"));
//    6    "921600"));
//    7    "1152000"));

    public static int getmTempRare() {
        return (int) MyApp.getInstance().getData("tempRare", 3);
    }

    public static void setmTempRare(int tempRare) {
        MyApp.getInstance().saveData("tempRare", tempRare);
    }

    //测温间隔时间 --默认 0 度 ，提高检测效率
    public static float getmTempTime() {
        float defValue = 1.0f;
        return (float) MyApp.getInstance().getData("tempTime", defValue);
    }

    public static void setmTempTime(float tempTime) {
        MyApp.getInstance().saveData("tempTime", tempTime);
    }


    public static String getTempDevPath() {
        String defValue = "/dev/ttyS3";
        boolean isChange = false;
        if (getmTempDev() != 3) {
            isChange = true;
        }
        if (isChange) {
            switch (getmTempDev()) {
                case 0:
                    defValue = "/dev/ttyS0";
                    break;
                case 1:
                    defValue = "/dev/ttyS1";
                    break;
                case 2:
                    defValue = "/dev/ttyS2";
                    break;
                case 4:
                    defValue = "/dev/ttyS4";
                    break;
            }
        }
        return (String) MyApp.getInstance().getData("tempDevPath", defValue);
    }

    public static void setTempDevPath(String tempDev) {
        MyApp.getInstance().saveData("tempDevPath", tempDev);
    }


    //测温串口
//   case 0 "ttyS0";
//   case 1:  "ttyS1";
//   case 2:  "ttyS2";
//   case 3: "ttyS3";
//   tempDev = "ttyS4";
    public static int getmTempDev() {
        int defaultTTY = 3;
        return (int) MyApp.getInstance().getData("tempDev", defaultTTY);
    }

    public static void setmTempDev(int tempDev) {
        MyApp.getInstance().saveData("tempDev", tempDev);
    }

    //测温单位 0->摄氏度   1->华式摄氏度
    public static int getTempUnit() {
        int defaultUnit = 0;
        return (int) MyApp.getInstance().getData("tempUnit", defaultUnit);
    }

    /***
     * 设置温度单位
     * @param tempUnit
     */
    public static void setTempUnit(int tempUnit) {
        MyApp.getInstance().saveData("tempUnit", tempUnit);
    }

    //测温设备
    public static int getmTempType() {
        int defaultType = TempModelEntity.IIC_MLX_90621_BAB;
        return (int) MyApp.getInstance().getData("tempType", defaultType);
    }

    /***
     * 温度类型
     * 温度模块
     * @param tempType
     */
    public static void setmTempType(int tempType) {
        MyApp.getInstance().saveData("tempType", tempType);
    }

    /***
     * 获取测温的最大识别距离
     * @return
     */
    public static int getFaceRectLength() {
        int rectMax = 100;
        int getmFaceLen = getmFaceLen();
        switch (getmFaceLen) {
            case 0:
                rectMax = 50;
                break;
            case 1:
                rectMax = 80;
                break;
            case 2:
                rectMax = 100;
                break;
            case 3:
                rectMax = 150;
                break;
            case 4:
                rectMax = 200;
                break;
        }
        return rectMax;
    }

    //设置最小人脸尺寸
    // 0  -》0.5
    // 1  -》0.8
    // 2  -》1
    // 3  -》1.5
    // 4  -》2
    public static int getmFaceLen() {
        int defaultLength = 3;
        return (int) MyApp.getInstance().getData("faceLen", defaultLength);
    }


    /****
     * 识别距离
     * @param faceLen
     */
    public static void setFaceLen(int faceLen) {
        MyApp.getInstance().saveData("faceLen", faceLen);
    }

    /****
     * 0 :镜像
     * 1：非镜像
     * @return
     */
    //人脸框水平镜像(虹软)
    public static int getmFaceRectHor() {
        int defaultRaote = 1;
        return (int) MyApp.getInstance().getData("faceRectHor", defaultRaote);
    }

    public static void setmFaceRectHor(int faceRectHor) {
        MyApp.getInstance().saveData("faceRectHor", faceRectHor);
    }

    /***
     * 0: 镜像
     * 1：非镜像
     * @return
     */
    //人脸框垂直镜像(虹软)
    public static int getmFaceRectVertical() {
        int defaultRaote = 1;
        return (int) MyApp.getInstance().getData("faceRectVertical", defaultRaote);
    }


    public static void setmFaceRectVertical(int faceRectVertical) {
        MyApp.getInstance().saveData("faceRectVertical", faceRectVertical);
    }

    //识别阀值
    public static float getmSearchThreshold() {
        return (float) MyApp.getInstance().getData("searchThreshold", 80f);
    }

    public static void setSearchThreshold(float searchThreshold) {
        MyApp.getInstance().saveData("searchThreshold", searchThreshold);
    }

    //活体阀值
    public static float getLivenessThreshold() {
        return (float) MyApp.getInstance().getData("livenessThreshold", 65f);
    }

    public static void setLivenessThreshold(float searchThreshold) {
        MyApp.getInstance().saveData("livenessThreshold", searchThreshold);
    }

    /***
     * 人脸框角度(旷视
     *     0 auto
     *     1  0
     *     2  90
     *     3  180
     *     4  270
     * @return
     */
    public static int getRectRotate() {
        int defaultRoate = 0;
        return (int) MyApp.getInstance().getData("rectRotate", defaultRoate);
    }

    public static void setRectRotate(int rectRotate) {
        MyApp.getInstance().saveData("rectRotate", rectRotate);
    }

    /****
     * 人脸框镜像(旷视)
     * 0： 自动
     * 1： 镜像
     * 2： 非镜像
     * @return
     */
    public static int getmFaceRect() {
        int defaultFaceRect = 2;
        return (int) MyApp.getInstance().getData("faceRect", defaultFaceRect);
    }

    public static void setmFaceRect(int faceRect) {
        MyApp.getInstance().saveData("faceRect", faceRect);
    }

    /**
     * 前置摄像头镜像
     * 0: 跟随系统
     * 1: 镜像
     * 2: 非镜像
     *
     * @return
     */
    public static int getFrontImage() {
        int defValue = 0;
        return (int) MyApp.getInstance().getData("frontImage", defValue);
    }

    public static void setFrontImage(int value) {
        MyApp.getInstance().saveData("frontImage", value);
    }

    /**
     * 后置摄像头镜像
     * 0: 跟随系统
     * 1: 镜像
     * 2: 非镜像
     *
     * @return
     */
    public static int getBackImage() {
        return (int) MyApp.getInstance().getData("backImage", 0);
    }

    public static void setBackImage(int value) {
        MyApp.getInstance().saveData("backImage", value);
    }

    /***
     * 人脸识别算法角度
     * 0-0
     * 1-90
     * 2-180
     * 3-270
     */
    public static int getmFaceRotate() {
        int defaultFaceRotate = 3;
        switch (AppConfig.APP_CUSTOM_TYPE) {
            case AppConfig.APP_CUSTOM_JTA_DEFAULT:
            case AppConfig.APP_CUSTOM_JTA_ID_CARD:
                defaultFaceRotate = 1;
                break;
            default:
                defaultFaceRotate = 3;
                break;
        }
        return (int) MyApp.getInstance().getData("faceRotate", defaultFaceRotate);
    }

    public static void setFaceRotate(int faceRotate) {
        MyApp.getInstance().saveData("faceRotate", faceRotate);
    }

    /***相机抓拍角度
     * 0 -自动
     * 1 -> 0
     * 2 -> 90
     *3 -> 180
     *else -> 270
     * @return
     */
    public static int getSaveRotate() {
        int defaultNum = 2;
        switch (AppConfig.APP_CUSTOM_TYPE) {
            case AppConfig.APP_CUSTOM_JTA_DEFAULT:
            case AppConfig.APP_CUSTOM_JTA_ID_CARD:
            case AppConfig.APP_CUSTOM_JTA_HID:
                defaultNum = 4;
                break;
        }
        return (int) MyApp.getInstance().getData("saveRotate", defaultNum);
    }

    public static void setSaveRotate(int saveRotate) {
        MyApp.getInstance().saveData("saveRotate", saveRotate);
    }

    /***
     * 相机预览角度
     * 0-0
     * 1-90
     * 2-180
     * 3-270
     */
    public static int getPrewRotate() {
        int defaultRoate = 1;
        return (int) MyApp.getInstance().getData("prewRotate", defaultRoate);
    }

    public static void setPrewRotate(int prewRotate) {
        MyApp.getInstance().saveData("prewRotate", prewRotate);
    }

    /****
     * 相机方向
     * 是否前置摄像头
     * true-前置
     * false-后置
     */
    public static boolean getCameraFacing() {
        boolean defaultCamera = false;
        return (boolean) MyApp.getInstance().getData("cameraFacing", defaultCamera);
    }

    /***
     * 相机方向
     * 设置摄像头前置，后置
     * true 0  前置
     * false 1 后置
     * @param cameraFacing
     */
    public static void setCameraFacing(boolean cameraFacing) {
        MyApp.getInstance().saveData("cameraFacing", cameraFacing);
    }

    /***
     * 软件是否授权
     */
    public static boolean getmInitFace() {
        return (boolean) MyApp.getInstance().getData("initFace", false);
    }

    public static void setmInitFace(boolean initFace) {
        MyApp.getInstance().saveData("initFace", initFace);
    }

    /***
     * 软件是否授权
     */
    public static boolean getmFaceEnable() {
        return (boolean) MyApp.getInstance().getData("faceEnable", true);
    }

    public static void setmFaceEnable(boolean faceEnable) {
        MyApp.getInstance().saveData("faceEnable", faceEnable);
    }

    /***
     * 获取用户名字
     * @return
     */
    public static String getUserName() {
        String userName = (String) MyApp.getInstance().getData("userName", "");
        return userName;
    }

    public static void setUserName(String userName) {
        MyApp.getInstance().saveData("userName", userName);
    }

    public static String getmServerIp() {
        String defaultIP = AppInfo.IP_DEFAULT_URL;
        String ipAddress = (String) MyApp.getInstance().getData("server_ip", defaultIP);
        return ipAddress;
    }

    public static void setmServerIp(String server_ip) {
        MyApp.getInstance().saveData("server_ip", server_ip);
    }

    public static String getmServerPort() {
        String ServerPort = (String) MyApp.getInstance().getData("server_port", AppInfo.SERVER_PORT);
        return ServerPort;
    }

    public static void setmServerPort(String server_port) {
        MyApp.getInstance().saveData("server_port", server_port);
    }

    /***
     * 刷卡开关
     * IC卡开关
     * @return
     */
    public static boolean getInputCardEnable() {
        boolean isOpenDefault = false;
        return (boolean) MyApp.getInstance().getData("inputCardEnable", isOpenDefault);
    }

    /***
     * 刷卡
     * @param inputCardEnable
     */
    public static void setInputCardEnable(boolean inputCardEnable) {
        MyApp.getInstance().saveData("inputCardEnable", inputCardEnable);
    }

    /***
     * 设备序列号-厂商识别码
     */
    public static String getSerDeviceId() {
        return (String) MyApp.getInstance().getData("SerDeviceId", "");
    }

    /***
     * 设备序列号-厂商识别码
     * @param SerDeviceId
     */
    public static void setSerDeviceId(String SerDeviceId) {
        MyApp.getInstance().saveData("SerDeviceId", SerDeviceId);
    }


    public static String getDeviceId() {
        return (String) MyApp.getInstance().getData("deviceId", "");
    }

    public static void setDeviceId(String deviceId) {
        MyApp.getInstance().saveData("deviceId", deviceId);
    }

    public static int getScreenwidth() {
        return ((int) MyApp.getInstance().getData("screenwidth", 800));
    }

    public static void setScreenwidth(int screenwidth) {
        MyApp.getInstance().saveData("screenwidth", screenwidth);
    }

    public static int getScreenheight() {
        return ((int) MyApp.getInstance().getData("screenheight", 1280));
    }

    public static void setScreenheight(int screenheight) {
        MyApp.getInstance().saveData("screenheight", screenheight);
    }

    //设置密码
    public static String getSettingPassword() {
        return ((String) MyApp.getInstance().getData("settingPassword", "123456"));
    }

    public static void setSettingPassword(String settingPassword) {
        MyApp.getInstance().saveData("settingPassword", settingPassword);
    }

    public static int getLanguageDesc() {
        return ((int) MyApp.getInstance().getData("LanguageDesc", 0));
    }

    public static void setLanguageDesc(int LanguageDesc) {
        MyApp.getInstance().saveData("LanguageDesc", LanguageDesc);
    }

    public static String getQinguan_password() {
        return ((String) MyApp.getInstance().getData("qinguan_password", "8085"));
    }

    public static void setQinguan_password(String qinguan_password) {
        MyApp.getInstance().saveData("qinguan_password", qinguan_password);
    }

    //保存信息到本地，服务器
    public static boolean getSaveInfoToLocal() {
        boolean isDefault = true;
        return ((boolean) MyApp.getInstance().getData("nosaveinfo", isDefault));
    }

    //保存信息到本地，服务器
    public static void setSaveInfoToLocal(boolean nosaveinfo) {
        MyApp.getInstance().saveData("nosaveinfo", nosaveinfo);
    }

    /***
     * 高温预警功能
     * 以前是String 类型，现在是float类型，抛弃以前得数据类型
     * @return
     */
    public static float getTempHeightWearNew() {
        float defaultTemp = 37.5f;
        return ((float) MyApp.getInstance().getData("tempHeightWearNew", defaultTemp));
    }

    public static void setTempHeightWearNew(float tempHeightWearNew) {
        MyApp.getInstance().saveData("tempHeightWearNew", tempHeightWearNew);
    }

    public static DeviceBean getDeviceInfo() {
        String mDeviceInfo = getmDeviceInfo();
        if (mDeviceInfo == null || mDeviceInfo.length() < 2) {
            return null;
        }
        DeviceBean deviceBean = new Gson().fromJson(mDeviceInfo, DeviceBean.class);
        return deviceBean;
    }

    /****
     * 获取上传的ID
     * @return
     */
    public static String getDeviceBeanId() {
        DeviceBean deviceBean = getDeviceInfo();
        if (deviceBean == null) {
            return "";
        }
        String deviceId = deviceBean.getId();
        return deviceId;
    }

    public static void setDeviceInfo(DeviceBean info, String printTag) {
        if (info == null) {
            setmDeviceInfo("", printTag);
            return;
        }
        setmDeviceInfo(new Gson().toJson(info), printTag);
    }

    public static String getmDeviceInfo() {
        return (String) MyApp.getInstance().getData("deviceInfo", "");
    }

    public static void setmDeviceInfo(String deviceInfo, String printTag) {
        LogUtil.cdl("======设置deviceInfo==" + printTag + " / " + deviceInfo);
        MyApp.getInstance().saveData("deviceInfo", deviceInfo);
    }

    /***
     * 判断软件是否是第一次登陆
     * @return
     */
    public static boolean isFirstLogin() {
        return (boolean) MyApp.getInstance().getData("firstLogin", true);
    }

    public static void setFirstLogin(boolean firstLogin) {
        MyApp.getInstance().saveData("firstLogin", firstLogin);
    }

    /***
     * 是否显示识别框
     * @param isShowChooice
     */
    public static void setShowFaceRectView(boolean isShowChooice) {
        MyApp.getInstance().saveData("isShowChooice", isShowChooice);
    }

    /***
     * 是否显示人脸背景框
     * @param isShowChooice
     */
    public static void setShowFaceBgRectView(boolean isShowChooice) {
        MyApp.getInstance().saveData("isShowBgChooice", isShowChooice);
    }

    public static boolean getShowFaceBgRectView() {
        boolean isShowDefault = true;
        return (boolean) MyApp.getInstance().getData("isShowBgChooice", isShowDefault);
    }

    /***
     *    默认显示，显示更大气
     * @return
     */
    public static boolean getShowFaceRectView() {
        boolean isShowDefault = true;
        return (boolean) MyApp.getInstance().getData("isShowChooice", isShowDefault);
    }


    public static void setGetFaceUrl(String getFaceUrl) {
        MyApp.getInstance().saveData("getFaceUrl", getFaceUrl);
    }

    public static void setUpdateFaceToWeb(String updateFaceToWeb) {
        MyApp.getInstance().saveData("updateFaceToWeb", updateFaceToWeb);
    }

    public static void setSyncTimeUrl(String syncTimeUrl) {
        MyApp.getInstance().saveData("syncTimeUrl", syncTimeUrl);
    }

    /***
     * 设置人脸同步时间
     * @param FaceSyncTime
     */
    public static void setFaceSyncTime(String FaceSyncTime) {
        MyApp.getInstance().saveData("FaceSyncTime", FaceSyncTime);
    }

    public static String getFaceSyncTime() {
        return (String) MyApp.getInstance().getData("FaceSyncTime", SimpleDateUtil.formatTaskTimeShow(System.currentTimeMillis()));
    }

    public static String getAppid() {
        return (String) MyApp.getInstance().getData("appid", "");
    }

    public static void setAppid(String appid) {
        MyApp.getInstance().saveData("appid", appid);
    }

    /***
     * 0  中创
     * 1  德科
     * @return
     */
    public static int getCardComType() {
        return (int) MyApp.getInstance().getData("CardComType", 1);
    }

    public static void setCardComType(int CardComType) {
        MyApp.getInstance().saveData("CardComType", CardComType);
    }

    public static void setHealthPlatform(int platform) {
        MyApp.getInstance().saveData("health_platform", platform);
    }

    // 0 国康码 1闵政通
    public static int getHealthPlatform() {
        return (int) MyApp.getInstance().getData("health_platform", 0);
    }

    public static void setDevConfigInfo(String config) {
        MyApp.getInstance().saveData("dev_config", config);
    }

    public static final String defConfig = "{\"config\":0,\"faceMaskType\":\"2\",\"healthCodeWay\":1,\"highTemperature\":\"39\",\"isCtidCheck\":1,\"isGps\":1,\"isLignting\":1,\"isRiskArea\":1,\"isTemperatureCheck\":1,\"lowTemperature\":\"35\",\"rctCardType\":[\"0\",\"48\",\"54\",\"49\",\"55\"],\"reportType\":1}";

    public static ConfigInfo getDevConfigInfo() {
        ConfigInfo config;
        try {
            String json = (String) MyApp.getInstance().getData("dev_config", defConfig);
            config = JSON.parseObject(json, ConfigInfo.class);
        } catch (Exception e) {
            config = new ConfigInfo();
        }
        config.faceMaskType = 2;
        if (getmMaskEnable()) {
            config.faceMaskType = 1;
        }
        if (getmMaskEnable() && getMaskIntercept()) {
            config.faceMaskType = 0;
        }
        config.isCtidCheck = getFaceAndCardEnable() ? 1 : 0;
        config.isTemperatureCheck = (getmTempType() == TempModelEntity.CLOSE ? 0 : 1);
        config.highTemperature = getTempHeightWearNew();
        config.lowTemperature = getmTempLow();
        config.healthCodeWay = (getHealthPlatform() == 0 ? 1 : 0);
        config.reportType = getDataReplyEnable() ? 2 : 0;
        config.isLignting = getWhiteLightEnable() ? 1 : 0;
        config.ctidAuthUrl = getRongCardUrl();
        config.mztUrl = getMZTUrl();
        config.gbUrl = getGKUrl();
        config.rctCardType = new ArrayList<>();
        String ctype = getRongCardType();
        config.rctCardType.addAll(Arrays.asList(ctype.split(",")));
        config.verifyAddress = getVerifyAddr();


        return config;
    }

    public static void setDeviceEnable(boolean enable) {
        MyApp.getInstance().saveData("dev_enable", enable);
    }

    public static boolean getDeviceEnable() {
        return (boolean) MyApp.getInstance().getData("dev_enable", true);
    }

    //白色补光灯
    public static void setWhiteLightEnable(boolean enable) {
        MyApp.getInstance().saveData("wlight_enable", enable);
    }

    public static boolean getWhiteLightEnable() {
        return (boolean) MyApp.getInstance().getData("wlight_enable", true);
    }

    //是否开启人证合一比对
    public static void setFaceAndCardEnable(boolean enable) {
        MyApp.getInstance().saveData("fc_enable", enable);
    }

    public static boolean getFaceAndCardEnable() {
        return (boolean) MyApp.getInstance().getData("fc_enable", true);
    }

    //是否设置数据上报
    public static void setDataReplyEnable(boolean enable) {
        MyApp.getInstance().saveData("dataReplay_enable", enable);
    }

    public static boolean getDataReplyEnable() {
        return (boolean) MyApp.getInstance().getData("dataReplay_enable", true);
    }

    //设置支持的榕城通卡类型 0300,0800
    public static void setRongCardType(String array) {
        MyApp.getInstance().saveData("rct_cardType", array);
    }

    public static String getRongCardType() {
        return (String) MyApp.getInstance().getData("rct_cardType", "0300,0800");
    }

    //设置榕城通卡接口地址
    public static void setRongCardUrl(String httpUrl) {
        MyApp.getInstance().saveData("rct_cardUrl", httpUrl);
    }

    public static String getRongCardUrl() {
        return (String) MyApp.getInstance().getData("rct_cardUrl", Const.MESSAGE_URL);
    }

    //设置闽政通获取通讯指令参数接口地址
    public static void setMZTUrl(String httpUrl) {
        MyApp.getInstance().saveData("mzt_url", httpUrl);
    }

    public static String getMZTUrl() {
        return (String) MyApp.getInstance().getData("mzt_url", Const.MZT_URL);
    }

    //设置国康码接口地址
    public static void setGKUrl(String httpUrl) {
        MyApp.getInstance().saveData("gk_url", httpUrl);
    }

    public static String getGKUrl() {
        return (String) MyApp.getInstance().getData("gk_url", Const.serverUrl);
    }

    //当识别到人脸时，设置文字提示
    public static void setTextTip(String tip) {
        MyApp.getInstance().saveData("ready_tip", tip);
    }

    public static String getTextTip() {
        return (String) MyApp.getInstance().getData("ready_tip", "请刷榕城通敬老卡、学生卡或身份证");
    }

    //设置闵政通授权码
    public static void setMztInvokeCode(String authCode) {
        MyApp.getInstance().saveData("mzt_auth_code", authCode);
    }

    public static String getMztInvokeCode() {
        return (String) MyApp.getInstance().getData("mzt_auth_code", Const.INVOKECALLER_CODE);
    }

    //设置mqtt 需要的sn
    public static void setMqttSn(String sn) {
        MyApp.getInstance().saveData("mqtt_sn", sn);
        if (sn != null && sn.length() >= 6) {
            StringBuilder sb = new StringBuilder();
            String last6 = sn.substring(sn.length() - 6);
            for (int i = 0; i < last6.length(); i++) {
                char ch = last6.charAt(i);
                int num = Character.digit(ch, 10);
                if (num == -1) {
                    sb.append(ch);
                    continue;
                }
                if (num == 9) {
                    sb.append(0);
                    continue;
                }
                sb.append(num + 1);
            }
            setSettingPassword(sb.toString());
        }
    }

    public static String getMqttSn() {
        return (String) MyApp.getInstance().getData("mqtt_sn", "FZ000XDL22020003");
    }

    public static void setVerifyAddr(String addr) {
        MyApp.getInstance().saveData("verify_addr", addr);
    }

    public static String getVerifyAddr() {
        return (String) MyApp.getInstance().getData("verify_addr", "");
    }

}
