package com.ys.acsdev.util;

import android.app.Service;
import android.content.Context;
import android.media.AudioManager;

/**
 * 音量控制管理器
 */
public class VoiceManager {

    public static void redusMoreVoiceNum(Context context) {
        AudioManager mAudioManager = (AudioManager) context.getSystemService(Service.AUDIO_SERVICE);
        int currentVoiceNum = mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
        currentVoiceNum--;
        if (currentVoiceNum < 1) {
            currentVoiceNum = 0;
        }
        mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, currentVoiceNum, 0);
    }

    public static void addMoreVoiceNum(Context context) {
        AudioManager mAudioManager = (AudioManager) context.getSystemService(Service.AUDIO_SERVICE);
        int currentVoiceNum = mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
        currentVoiceNum++;
        if (currentVoiceNum > 14) {
            currentVoiceNum = 14;
        }
        mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, currentVoiceNum, 0);
    }


    public static int getCurrentVoiceNum(Context context) {
        int mediacurrent = 7;
        try {
            AudioManager mAudioManager = (AudioManager) context.getSystemService(Service.AUDIO_SERVICE);
            mediacurrent = mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mediacurrent;
    }

    /**
     * 按照比例来修改音量
     *
     * @param context
     */
    public static void changeMusicVoice(Context context, int progressNum) {
        try {
            AudioManager mAudioManager = (AudioManager) context.getSystemService(Service.AUDIO_SERVICE);
            if (progressNum == 0) {
                mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, 0, 0);
                return;
            }
            int voiceNum = (progressNum * 15) / 100;
            mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, voiceNum, 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static void setMediaVoiceNum(Context context, int currentMediaNum) {
        try {
            AudioManager mAudioManager = (AudioManager) context.getSystemService(Service.AUDIO_SERVICE);
            mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, currentMediaNum, 0);  //设置媒体音量为 0
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /***
     * 设置音量最大
     */
    public static void addMediaMaxVoice(Context context) {
        try {
            AudioManager mAudioManager = (AudioManager) context.getSystemService(Service.AUDIO_SERVICE);
            mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, 15, 0);  //设置媒体音量为 0
        } catch (Exception e) {
        }
    }

    /***
     * 静音
     */
    public static void stopMediaVoice(Context context) {
        try {
            AudioManager mAudioManager = (AudioManager) context.getSystemService(Service.AUDIO_SERVICE);
            mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, 0, 0);  //设置媒体音量为 0
        } catch (Exception e) {
        }
    }


}