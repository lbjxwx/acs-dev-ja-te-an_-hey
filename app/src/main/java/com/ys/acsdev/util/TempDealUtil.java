package com.ys.acsdev.util;

import com.ys.acsdev.commond.AppInfo;
import com.ys.acsdev.setting.entity.TempModelEntity;

import java.math.BigDecimal;

public class TempDealUtil {
    /***
     * 根据单位判断温度是否是低温
     * 返回
     */
    public static String mathTempByUnit(float temperatureUpdate) {
        String tempShowBack = temperatureUpdate + "";
        if (SpUtils.getmTempType() == TempModelEntity.CLOSE) {  //测试设备关闭
            return AppInfo.TEMP_TYPE_NOT_OPEN;
        }
        int unitTemp = SpUtils.getTempUnit();
        float lowTemp = SpUtils.getmTempLow();
        if (unitTemp == 0) {
            //摄氏度
            if (temperatureUpdate < lowTemp) {
                tempShowBack = AppInfo.TEMP_TYPE_LOW_TEMP + " : " + temperatureUpdate;
            }
        } else {
            if (temperatureUpdate < lowTemp) {
                tempShowBack = AppInfo.TEMP_TYPE_LOW_TEMP + " : " + temperatureUpdate;
            }
        }
        return tempShowBack;
    }

    /***
     * 判断当前是否发热，发烧
     */
    public static String getTempIsFever(String tempNum) {
        LogUtil.update("======是否发烧=====" + tempNum);
        if (tempNum.contains(AppInfo.TEMP_TYPE_NOT_OPEN)
                || tempNum.contains(AppInfo.TEMP_TYPE_LOW_TEMP)) {
            return AppInfo.TEMP_TYPE_NORMAL;
        }
        // tempNum = tempNum.replace(new Regex("℃|℉"), "");
        if (tempNum.contains("℃")) {
            tempNum = tempNum.replace("℃", "").trim();
        }
        if (tempNum.contains("℉")) {
            tempNum = tempNum.replace("℉", "").trim();
        }
        float tempFloat = 0.0f;
        try {
            tempFloat = Float.parseFloat(tempNum);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (tempFloat < 1) {
            return AppInfo.TEMP_TYPE_NORMAL;
        }
        int unitTemp = SpUtils.getTempUnit();
        float tempWear = SpUtils.getTempHeightWearNew();
        LogUtil.update("======是否发烧=111====" + tempWear);
        if (unitTemp == 0) {
            //摄氏度
            if (tempFloat > tempWear) {
                return AppInfo.TEMP_TYPE_HEIGHT;
            }
        } else {
            if (tempFloat > tempWear) {
                return AppInfo.TEMP_TYPE_HEIGHT;
            }
        }
        return AppInfo.TEMP_TYPE_NORMAL;
    }


    public static String getUpdateTempNum(String temperatureUpdate) {
        if (SpUtils.getmTempType() == TempModelEntity.CLOSE) {  //测试设备关闭
            return AppInfo.TEMP_TYPE_NOT_OPEN;
        }
        int unitTemp = SpUtils.getTempUnit();
        String unitTempAfter = "℃";
        if (unitTemp == 0) {
            //摄氏度
            unitTempAfter = " ℃";
        } else {
            unitTempAfter = " ℉";
        }
        return temperatureUpdate + unitTempAfter;
    }

    public static float getFloatOnePoint(float priceCar) {
        if (priceCar < 1) {
            priceCar = 1.0f;
        }
        // 设置位数
        int scale = 2;
        // 表示四舍五入，可以选择其他舍值方式，例如去尾，等等.
        int roundingMode = 4;
        BigDecimal bd = new BigDecimal(priceCar);
        bd = bd.setScale(scale, roundingMode);
        priceCar = bd.floatValue();
        return priceCar;
    }

}
