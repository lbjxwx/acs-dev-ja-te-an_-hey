package com.ys.acsdev.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ImageFormat;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.YuvImage;
import android.os.Build;
import android.os.storage.StorageManager;
import android.util.Log;

import com.ys.acsdev.MyApp;
import com.ys.acsdev.R;
import com.ys.acsdev.api.AppConfig;
import com.ys.acsdev.commond.AppInfo;
import com.ys.acsdev.manager.ImageManager;
import com.ys.acsdev.service.AcsService;
import com.ys.acsdev.ui.SplashActivity;
import com.ys.acsdev.util.guardian.FileRawWriteRunnable;
import com.ys.acsdev.util.guardian.WriteSdListener;
import com.ys.qrcodelib.QRCodeUtil;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Method;
import java.nio.channels.FileChannel;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class FileUtils {

    /***
     * 文件保存路径
     * @param saveFilePath
     * 下载文件路径
     * @param fileLength
     * @return
     */
    public static boolean ifFaceFileHasExict(String saveFilePath, String fileLength) {
        try {
            fileLength = fileLength.trim();
            if (fileLength == null || fileLength.length() < 1) {
                fileLength = "1024";
            }
            File fileSave = new File(saveFilePath);
            if (!fileSave.exists()) {
                return false;
            } else {  //文件存在去比对文件大小
                long fileDownLength = fileSave.length();
                long downFileLengthLong = Long.parseLong(fileLength);
                LogUtil.task("=========文件比对===" + fileDownLength + " / " + downFileLengthLong);
                if (fileDownLength < 1024 * 10) {
                    return false;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }

    public static boolean ifFileHasExicTask(String saveFilePath, String fileLength) {
        fileLength = fileLength.trim();
        if (fileLength == null || fileLength.length() < 1) {
            fileLength = "1024";
        }
        File fileSave = new File(saveFilePath);
        if (!fileSave.exists()) {
            return false;
        } else {  //文件存在去比对文件大小
            long fileDown = fileSave.length();
            long downFileLengthLong = Long.parseLong(fileLength);
            long distanceSize = Math.abs(fileDown - downFileLengthLong);
            LogUtil.task("=========文件比对===" + fileDown + " / " + downFileLengthLong + " / " + distanceSize);
            if (distanceSize > (10 * 1024)) {
                //下载的文件和保存的文件内存不对称 ,需要重新下载
                LogUtil.task("=========文件比对=文件不完整，重新下载=");
                return false;
            }
        }
        LogUtil.task("=========文件比对=执行完毕=");
        return true;
    }

    /**
     * 判断USB升级文件是否存在
     *
     * @param context
     * @param sdUsbPath
     * @return
     */
    public static String updateFileIsExict(Context context, String sdUsbPath) {
        if (sdUsbPath == null || sdUsbPath.length() < 5 || context == null) {
            return null;
        }
        boolean isOrSd = isMountSD(context, sdUsbPath);
        LogUtil.e("======判断文件是否是SD卡====" + isOrSd, "FileUtil");
        if (!isOrSd) { //USB设备
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {  // 7.0
                sdUsbPath = sdUsbPath + "/udisk0";
            }
        }
        LogUtil.e("====USB path==" + sdUsbPath, "FileUtil");
        if (sdUsbPath != null && !sdUsbPath.contains("null") && sdUsbPath.length() > 5) {
            String path = getFileFormSave(sdUsbPath);
            if (path != null && path.length() > 2) {
                return path;
            }
        }
        return null;
    }

    /**
     * 判断路径是否为SD卡
     *
     * @param context
     * @param path
     * @return
     */
    public static boolean isMountSD(Context context, String path) {
        if (Build.VERSION.SDK_INT < 23) {
            return path.toLowerCase().contains("external_sd");
        } else {
            return isMountSD(context);
        }
    }

    /**
     * 当前挂载是否为SD
     *
     * @param context
     * @return
     */
    private static boolean isMountSD(Context context) {
        StorageManager storageManager = (StorageManager) context.getSystemService(Context.STORAGE_SERVICE);
        Class<?> volumeInfoClazz = null;
        Class<?> diskInfoClazz = null;
        try {
            diskInfoClazz = Class.forName("android.os.storage.DiskInfo");
            Method isSd = diskInfoClazz.getMethod("isSd");
            volumeInfoClazz = Class.forName("android.os.storage.VolumeInfo");
            Method getType = volumeInfoClazz.getMethod("getType");
            Method getDisk = volumeInfoClazz.getMethod("getDisk");
            Method getState = volumeInfoClazz.getMethod("getState");
            Method getDescriptionComparator = volumeInfoClazz.getMethod("getDescriptionComparator");
            Method getVolumes = storageManager.getClass().getMethod("getVolumes");
            List<Class<?>> result = (List<Class<?>>) getVolumes.invoke(storageManager);
            Collections.sort(result, (Comparator<? super Class<?>>) getDescriptionComparator.invoke(volumeInfoClazz));
            for (int i = 0; i < result.size(); i++) {
                Object volumeInfo = result.get(i);
                if ((int) getType.invoke(volumeInfo) == 0) {
                    Object disk = getDisk.invoke(volumeInfo);
                    if (disk != null) {
                        if ((boolean) isSd.invoke(disk)) {
                            int status = (int) getState.invoke(volumeInfo);
                            LogUtil.e("isMountSD()--status-->" + status, "FileUtil");
                            if (status == 2) {
                                return true;
                            }
                            return false;
                        }
                    }
                }
            }
            return false;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    /***
     * 判断该路径下有没有想要的文件
     * @param sdPath
     * @return
     */
    private static String getFileFormSave(String sdPath) {
        String StringBack = null;
        try {
            File file = new File(sdPath);
            if (!file.exists()) {
                return null;
            }
            File[] files = file.listFiles();
            for (File fileSave : files) {
                String filePath = fileSave.getPath();
                String fileName = fileSave.getName();
                LogUtil.e("======便利数据路径====" + filePath, "FileUtil");
                if (filePath.contains(".apk")) {
                    fileName = fileName.toLowerCase();
                    if (fileName.startsWith("ysface") ||
                            fileName.startsWith("eface")) {
                        StringBack = filePath;
                        break;
                    }
                } else if (filePath.contains(AppInfo.USB_APK_MODIFY_IP)) {
                    StringBack = filePath;
                    break;
                } else if (filePath.contains(AppInfo.USB_APK_ADD_ASK_FORBIDEN)) {
                    StringBack = filePath;
                    break;
                } else if (filePath.contains(AppInfo.SINGLE_TASK_DIR)) {
                    StringBack = filePath;
                    break;
                } else if (filePath.contains(AppInfo.BOOTANIMAL)) {
                    StringBack = filePath;
                    break;
                } else if (filePath.contains(AppInfo.FACE_REGISTER_DIR)) {
                    StringBack = filePath;
                    break;
                }
            }
            return StringBack;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return StringBack;
    }

    public static void delOldSaveLog() {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                try {
                    File file = new File(AppInfo.LOG_PATH);
                    if (!file.exists())
                        return;
                    File[] logFiles = file.listFiles();
                    if (logFiles.length == 0)
                        return;
                    String lastDate = SimpleDateUtil.getLastDateSingle().replace("-", "");
                    String beforeLastDate = SimpleDateUtil.getBeforeLastDateSingle().replace("-", "");
                    String todayDate = SimpleDateUtil.getDateSingle().replace("-", "");
                    for (File logFile : logFiles) {
                        String fileName = logFile.getName();
                        if (fileName.contains(lastDate) || fileName.contains(beforeLastDate) || fileName.contains(todayDate)) {
                            continue;
                        }
                        LogUtil.deleteFile("删除旧日志文件: " + logFile.getName(), true);
                        logFile.delete();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        AcsService.getInstance().executor(runnable);
    }

    /***
     * 删除访客二维码去
     */
    public static void delQrCodePath(String tag) {
        FileUtils.deleteDirOrFile(AppInfo.QR_VISITOR_PATH, tag);
        FileUtils.deleteDirOrFile(AppInfo.QR_VISITOR_DOWN_PATH, tag);
    }

    //删除目录或者文件
    public static boolean deleteDirOrFile(String Path, String tag) {
        return deleteDirOrFile(new File(Path), tag);
    }

    public static boolean deleteDirOrFile(File file, String tag) {
        LogUtil.cdl("文件删除调用====" + (file.getPath()) + " / " + tag, true);
        if (file.isFile()) {
            file.delete();
            return false;
        }
        if (file.isDirectory()) {
            File[] childFile = file.listFiles();
            if (childFile == null || childFile.length == 0) {
                file.delete();
                return false;
            }
            for (File f : childFile) {
                deleteDirOrFile(f, tag);
            }
            file.delete();
        }
        creatPathNotExcit();
        return false;
    }

    public static void creatPathNotExcit(String positionPath) {
        try {
            List<String> listsPath = new ArrayList<String>();
            String baceFacePath = positionPath + "/face";
            listsPath.add(baceFacePath);              //sdcard/face
            listsPath.add(baceFacePath + "/image");   //sdcard/image
            listsPath.add(baceFacePath + "/passRecord");   //sdcard/passRecord
            for (String path : listsPath) {
                File file = new File(path);
                if (!file.exists()) {
                    file.mkdirs();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static void creatPathNotExcit() {
        try {
            List<String> listsPath = new ArrayList<String>();
            listsPath.add(AppInfo.BASE_LOCAL_PATH);          //sdcard/face
            listsPath.add(AppInfo.PATH_ANDROID);   //sdcard/Android
            listsPath.add(AppInfo.BASE_CRASH_LOG);   //sdcard/Android/log
            listsPath.add(AppInfo.BASE_APK_PATH);        //sdcard/face/apk
            listsPath.add(AppInfo.BASE_IMAGE_PATH);            //sdcard/face/image
            listsPath.add(AppInfo.BASE_VIDEO_PATH);         //sdcard/face/video
            listsPath.add(AppInfo.BASE_TASK_PATH);           //sdcard/face/TASK
            listsPath.add(AppInfo.LOG_PATH);
            listsPath.add(AppInfo.BASE_PATH_CACHE);
            listsPath.add(AppInfo.PASS_RECORD_PATH);
            listsPath.add(AppInfo.CAPTURE_IMAGE_LOCAL);         //sdcard/face/local
            for (String path : listsPath) {
                File file = new File(path);
                if (!file.exists()) {
                    file.mkdirs();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String getTxtInfoFromTxtFile(String filePath) {
        return getTxtInfoFromTxtFile(filePath, TYPE_UTF_8);
    }

    /***
     * 从文本中获取txt信息
     * @param filePath
     * @type type
     * @return
     */
    public static final String TYPE_UTF_8 = "UTF-8";
    public static final String TYPE_GBK = "GBK";

    public static String getTxtInfoFromTxtFile(String filePath, String type) {
        String result = null;
        try {
            File file = new File(filePath);
            if (!file.exists()) {
                return result;
            }
            int length = (int) file.length();
            byte[] buff = new byte[length];
            FileInputStream fin = new FileInputStream(file);
            fin.read(buff);
            fin.close();
            result = new String(buff, type);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }


    public static Bitmap tranBitmap(byte[] data, int width, int height, int rotate) {
        try {
            YuvImage img = new YuvImage(
                    data,
                    ImageFormat.NV21,
                    width,
                    height,
                    null
            );
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            img.compressToJpeg(new Rect(0, 0, width, height), 15, stream);
            Bitmap bitmap = BitmapFactory.decodeByteArray(stream.toByteArray(), 0, stream.size());
            Matrix mat = new Matrix();
            mat.postRotate(rotate);
            bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), mat, true);
            stream.close();
            return bitmap;
        } catch (Exception e) {
            LogUtil.update("=====pecFace: tranBitmap error ");
//            backImageListener(false, e.toString());
            e.printStackTrace();
        }
        return null;
    }


    public interface OnReplaceListener {
        boolean onReplace(File srcFile, File destFile);
    }

    public static final void writeDefaultFaceImageToLocal(Context context) {
        if (context == null) {
            return;
        }
        //写入一张默认得图片到本地
        int faceDefaultImage = R.raw.face_default;
        int imageDefault = 5580;
        String saveImagePath = AppInfo.DEFAULT_FACE_IMAGE_PATH;
        FileRawWriteRunnable runnableImage = new FileRawWriteRunnable(context, faceDefaultImage, saveImagePath, imageDefault, new WriteSdListener() {
            @Override
            public void writeProgress(int progress) {
                Log.e("write", "==image=progress===" + progress);
            }

            @Override
            public void writeSuccess(String savePath) {
                Log.i("write", "image==suucess==" + savePath);
            }

            @Override
            public void writrFailed(String errorDesc) {
                Log.e("write", "image==writrFailed==" + errorDesc);
            }
        });
        AcsService.getInstance().executor(runnableImage);
    }

    public static String getFileMD5(File file) {
        if (!file.isFile()) {
            return "";
        }
        MessageDigest digest = null;
        FileInputStream in = null;
        byte[] buffer = new byte[1024];
        int len;
        try {
            digest = MessageDigest.getInstance("MD5");
            in = new FileInputStream(file);
            while ((len = in.read(buffer, 0, 1024)) != -1) {
                digest.update(buffer, 0, len);
            }
            in.close();
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
        return bytesToHexString(digest.digest());
    }

    public static String bytesToHexString(byte[] src) {
        StringBuilder stringBuilder = new StringBuilder("");
        if (src == null || src.length <= 0) {
            return "";
        }
        for (byte b : src) {
            int v = b & 0xFF;
            String hv = Integer.toHexString(v);
            if (hv.length() < 2) {
                stringBuilder.append(0);
            }
            stringBuilder.append(hv);
        }
        return stringBuilder.toString();
    }

}
