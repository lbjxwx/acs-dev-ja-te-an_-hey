package com.ys.acsdev.util

import android.util.Log
import com.ys.acsdev.commond.AppInfo
import org.jetbrains.anko.doAsync
import java.io.File

object LogUtil {
    private var isOpen: Boolean = true
    private val TAG: String = "kermitye"

    @JvmStatic
    fun d(msg: String, tag: String = TAG) {
        if (isOpen) {
            Log.d(if (tag.isNullOrEmpty()) TAG else tag, msg)
        }
    }

    @JvmStatic
    fun i(msg: String, tag: String = TAG) {
        if (isOpen) {
            Log.i(if (tag.isNullOrEmpty()) TAG else tag, msg)
        }
    }

    @JvmStatic
    fun e(msg: String, tag: String = TAG) {
        if (isOpen) {
            e(msg, tag, false)
        }
    }

    @JvmStatic
    fun e(msg: String, tag: String = TAG, isPrint: Boolean) {
        if (isOpen) {
            Log.e(if (tag.isNullOrEmpty()) TAG else tag, msg)
            if (isPrint) {
                saveLogToLocal(msg)
            }
        }
    }

    @JvmStatic
    fun file(msg: String) {
        if (isOpen) {
            Log.d("file", msg)
        }
    }

    @JvmStatic
    fun e(msg: String) {
        e(msg, TAG)
    }

    @JvmStatic
    fun db(s: String) {
        e(s, "db")
    }

    @JvmStatic
    fun attendance(s: String) {
        e(s, "attendance")
    }

    @JvmStatic
    fun task(s: String) {
        e(s, "task")
    }

    @JvmStatic
    fun faceRegist(s: String) {
        e(s, "faceRegist")
    }

    @JvmStatic
    fun down(s: String) {
        e(s, "down")
    }

    @JvmStatic
    fun cdl(s: String) {
        cdl(s, false)
    }

    @JvmStatic
    fun cdl(s: String, isPrint: Boolean) {
        e(s, "cdl")
        if (isPrint) {
            saveLogToLocal(s)
        }
    }

    @JvmStatic
    fun guardian(s: String) {
        e(s, "guardian")
    }

    @JvmStatic
    fun logo(s: String) {
        e(s, "logoInfo")
    }

    @JvmStatic
    fun logo(s: String, isPrint: Boolean) {
        logo(s);
        if (isPrint) {
            saveLogToLocal(s)
        }
    }

    @JvmStatic
    fun message(s: String) {
        message(s, false)
    }

    @JvmStatic
    fun register(s: String) {
        e(s, "register")
    }

    @JvmStatic
    fun register(s: String, isPrint: Boolean) {
        if (isPrint) {
            saveLogToLocal(s)
        }
        register(s)
    }

    @JvmStatic
    fun message(s: String, isPrint: Boolean) {
        if (isPrint) {
            saveLogToLocal(s)
        }
        e(s, "message")
    }

    fun sdcard(s: String) {
        sdcard(s, false)
    }

    @JvmStatic
    fun sdcard(s: String, isPrint: Boolean) {
        if (isPrint) {
            saveLogToLocal(s)
        }
        e(s, "sdcard")
    }

    fun deleteFile(s: String) {
        deleteFile(s, false)
    }

    @JvmStatic
    fun deleteFile(s: String, isPrint: Boolean) {
        if (isPrint) {
            saveLogToLocal(s)
        }
        e(s, "deleteFile")
    }

    @JvmStatic
    fun visitor(s: String) {
        e(s, "visitor")
    }

    @JvmStatic
    fun blackList(s: String) {
        e(s, "blackList")
    }

    @JvmStatic
    fun persoon(s: String) {
        persoon(s, false)
    }

    @JvmStatic
    fun persoon(s: String, isPrint: Boolean) {
        if (isPrint) {
            saveLogToLocal(s)
        }
        e(s, "persoon")
    }

    @JvmStatic
    fun update(s: String) {
        update(s, false)
    }

    @JvmStatic
    fun update(s: String, isPrint: Boolean) {
        if (isPrint) {
            saveLogToLocal(s)
        }
        e(s, "update")
    }

    @JvmStatic
    fun cardManager(s: String) {
        cardManager(s, false)
    }

    @JvmStatic
    fun cardManager(s: String, isPrint: Boolean) {
        if (isPrint) {
            saveLogToLocal(s)
        }
        e(s, "cardManager")
    }

    @JvmStatic
    fun saveLogToLocal(msg: String) {
        doAsync {
            try {
                val path = AppInfo.LOG_PATH + "/" + SimpleDateUtil.getDateSingle().replace(
                    "-", ""
                ) + ".txt"
                val file = File(path)
                if (!file.parentFile.exists()) {
                    file.parentFile.mkdirs()
                }
                file.appendText(SimpleDateUtil.formatCurrentTimeMin() + "\t ${msg}\n")
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    /***
     * �����������Դ�ӡ��
     */
    @JvmStatic
    fun saveLogToLocalSingle(msg: String) {
        doAsync {
            try {
                val path = AppInfo.LOG_PATH + "/tempTest.txt"
                val file = File(path)
                if (!file.parentFile.exists()) {
                    file.parentFile.mkdirs()
                }
                file.appendText(SimpleDateUtil.formatCurrentTimeMin() + "\t ${msg}\n")
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }


    @JvmStatic
    fun saveTemp(msg: String) {
        doAsync {
            try {
                val path = AppInfo.LOG_PATH + "/" + "temperature.txt"
                val file = File(path)
                if (!file.parentFile.exists()) {
                    file.parentFile.mkdirs()
                }
                file.appendText(SimpleDateUtil.formatCurrentTimeMin() + "\t ${msg}\n")
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    @JvmStatic
    fun temperature(msg: String) {
        d(msg, "temperature")
    }

    @JvmStatic
    fun gpio(s: String) {
//        e( s,"gpio")
    }

    @JvmStatic
    fun http(s: String) {
        e(s, "http")
    }

    @JvmStatic
    fun test(s: String) {
//        e(s, "testLight")
    }

    @JvmStatic
    fun zip(s: String) {
        e(s, "ziputil")
    }

    @JvmStatic
    fun door(desc: String) {
        e(desc, "door")
    }

    @JvmStatic
    fun export(s: String) {
        e(s, "export")
    }

    @JvmStatic
    fun healthy(s: String) {
        e(s, "healthy")
    }
}
