package com.ys.acsdev.util;

import com.ys.acsdev.bean.RedioEntity;

import java.util.ArrayList;
import java.util.List;

/**
 * 所有和时间相关得判断
 * 都在这里
 */
public class TimerUtil {

    public static List<RedioEntity> getTimeStartInfo() {
        List<RedioEntity> redioEntityList = new ArrayList<RedioEntity>();
        for (int i = 0; i < 24; i++) {
            RedioEntity redioEntity = new RedioEntity(i + ":00");
            redioEntityList.add(redioEntity);
        }
        return redioEntityList;
    }

    public static boolean isUpdateAcrodToTime() {
        String updateStartTime = SpUtils.getUpdateStartTime();
        String updateEndTime = SpUtils.getUpdateEndTime();
        if (updateStartTime.contains(":")) {
            updateStartTime = updateStartTime.replace(":", "");
        }
        if (updateEndTime.contains(":")) {
            updateEndTime = updateEndTime.replace(":", "");
        }
        if (updateEndTime == null || updateEndTime.length() < 2) {
            return false;
        }
        if (updateStartTime == null || updateStartTime.length() < 2) {
            return false;
        }
        int startTime = -1;
        int endTime = -1;
        try {
            startTime = Integer.parseInt(updateStartTime);
            endTime = Integer.parseInt(updateEndTime);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (startTime < 0 || endTime < 0) {
            return false;
        }
        int currentTime = SimpleDateUtil.getCurrentHourMin();
        if (startTime < endTime) {
            if (currentTime < startTime || currentTime > endTime) {
                return false;
            }
            return true;
        }
        //下面得就是start end
        if ((currentTime > endTime && currentTime < 2359) && (currentTime > 0 && currentTime < startTime)) {
            return true;
        }
        return false;
    }


}
