package com.ys.acsdev.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.LinkAddress;
import android.net.NetworkInfo;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.text.TextUtils;

import com.ys.acsdev.util.net.WifiMgr;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;

public class NetWorkUtils {

    /**
     * 判断是否有网络连接
     *
     * @param context
     * @return
     */
    public static boolean isNetworkConnected(Context context) {
        if (context == null) {
            return false;
        }
        try {
            if (context != null) {
                ConnectivityManager mConnectivityManager = (ConnectivityManager) context
                        .getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo mNetworkInfo = mConnectivityManager.getActiveNetworkInfo();
                if (mNetworkInfo != null) {
                    return mNetworkInfo.isAvailable();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    /***
     * 获取IP地址
     * 这里会区分4G网卡，WIFI类型以及以太网中得
     * 固定IP或动态IP
     * @param context
     * @return
     */
    public static String getIPAddress(Context context) {
        String ipAddress = "0.0.0.0";
        if (context == null) {
            return ipAddress;
        }
        if (!NetWorkUtils.isNetworkConnected(context)) {
            return ipAddress;
        }
        try {
            int netType = NetWorkUtils.getNetworkState(context);
            if (netType == NetWorkUtils.NETWORN_WIFI) {
                ipAddress = WifiMgr.getInstance(context).getCurrentIpAddress();
            } else if (netType == NetWorkUtils.NETWORK_MOBILE) {    //手机2.3.4G网络
                for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements(); ) {
                    NetworkInterface intf = en.nextElement();
                    for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements(); ) {
                        InetAddress inetAddress = enumIpAddr.nextElement();
                        if (!inetAddress.isLoopbackAddress() && inetAddress instanceof Inet4Address) {
                            ipAddress = inetAddress.getHostAddress();
                        }
                    }
                }
            } else if (netType == NetWorkUtils.NETWORK_ETH_NET) {
                //以太网,先使用系统API，
                ipAddress = getEthIpAddress(context);
            }
        } catch (Exception e) {
            ipAddress = "0.0.0.0";
            e.printStackTrace();
        }
        //防止IP获取失败
        if (TextUtils.isEmpty(ipAddress) || ipAddress == null) {
            ipAddress = "192.168.1.251";
        }
        return ipAddress;
    }

    public static String getEthIpAddress(Context context) {
        if (context == null) {
            return "";
        }
        if (isDHCP(context)) {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
                return getDynamicEthIPAddress();
            } else {
                return getDynamicEthIPAddress(context);
            }
        } else {
            return getStaticEthIpAddress(context);
        }
    }


    /***
     * 获取静态IP地址
     * @return
     */
    //ETH IP address prop
    public final static String ETH_IP_ADDRESS_PROP = "dhcp.eth0.ipaddress";

    private static String getDynamicEthIPAddress() {
        return getValueFromProp(ETH_IP_ADDRESS_PROP);
    }

    public static String getValueFromProp(String key) {
        String value = "";
        try {
            Class<?> classType = Class.forName("android.os.SystemProperties");
            Method getMethod = classType.getDeclaredMethod("get", new Class<?>[]{String.class});
            value = (String) getMethod.invoke(classType, new Object[]{key});
        } catch (Exception e) {
        }
        return value;
    }

    private static String getDynamicEthIPAddress(Context context) {
        try {
            if (context == null) {
                return "";
            }
            Object ethernet = context.getSystemService("ethernet");
            Class<?> ithernetManager = Class.forName("android.net.EthernetManager");
            Method getConfiguration = ithernetManager.getMethod("getIpAddress");
            Object ipc = getConfiguration.invoke(ethernet);
            if (ipc == null) return "";
            return ipc.toString();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return "";
    }


    public static String getStaticEthIpAddress(Context context) {
        try {
            if (context == null) {
                return "";
            }
            Object ethernet = context.getSystemService("ethernet");
            Class<?> ithernetManager = Class.forName("android.net.EthernetManager");
            Method getConfiguration = ithernetManager.getMethod("getConfiguration");
            Object ipc = getConfiguration.invoke(ethernet);
            Class<?> ipConfiguration = Class.forName("android.net.IpConfiguration");
            Method getStaticIpConfiguration = ipConfiguration.getMethod("getStaticIpConfiguration");
            Object sic = getStaticIpConfiguration.invoke(ipc);
            if (sic == null) return "";
            Class<?> staticIpConfiguration = Class.forName("android.net.StaticIpConfiguration");
            Field ipAddress = staticIpConfiguration.getDeclaredField("ipAddress");
            LinkAddress la = (LinkAddress) ipAddress.get(sic);
            if (la == null) return "";
            return la.getAddress().getHostAddress();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    /**
     * 以太网模式：是否位动态模式
     *
     * @return true 动态模式 false 静态模式
     */
    private static boolean isDHCP(Context context) {
        try {
            if (context == null) {
                return true;
            }
            Object ethernet = context.getSystemService("ethernet");
            Class<?> ithernetManager = Class.forName("android.net.EthernetManager");
            Method getConfiguration = ithernetManager.getMethod("getConfiguration");
            Class<?> ipConfiguration = Class.forName("android.net.IpConfiguration");
            Field ipAssignment = ipConfiguration.getDeclaredField("ipAssignment");
            Object configutation = getConfiguration.invoke(ethernet);
            Object ia = ipAssignment.get(configutation);
            Class<?> ipA = Class.forName("java.lang.Enum");
            Method ordinal = ipA.getMethod("ordinal");
            Object ordinalResult = ordinal.invoke(ia);
            return ordinalResult.toString().equals("1");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }


    /***
     * 字节转km G
     * @param bytes
     * @return
     */
    public static String bytes2kb(long bytes) {
        BigDecimal filesize = new BigDecimal(bytes);
        BigDecimal megabyte = new BigDecimal(1024 * 1024);
        float returnValue = filesize.divide(megabyte, 2, BigDecimal.ROUND_UP)
                .floatValue();
        if (returnValue > 1)
            return (returnValue + "MB");
        BigDecimal kilobyte = new BigDecimal(1024);
        returnValue = filesize.divide(kilobyte, 2, BigDecimal.ROUND_UP)
                .floatValue();
        return (returnValue + "KB");
    }


    //没有网络连接
    public static final int NETWORN_NONE = 0;
    //wifi连接
    public static final int NETWORN_WIFI = 1;
    //手机2.3.4G网络
    public static final int NETWORK_MOBILE = 2;
    //以太网
    public static final int NETWORK_ETH_NET = 3;

    /**
     * 获取当前网络连接类型
     *
     * @param context
     * @return
     */
    public static int getNetworkState(Context context) {
        if (context == null) {
            return NETWORN_NONE;
        }
        //获取系统的网络服务
        ConnectivityManager connManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (null == connManager) {
            return NETWORN_NONE;
        }
        //获取当前网络类型，如果为空，返回无网络
        NetworkInfo activeNetInfo = connManager.getActiveNetworkInfo();
        if (activeNetInfo == null || !activeNetInfo.isAvailable()) {
            return NETWORN_NONE;
        }
        boolean isLineEth = getEthType(connManager);
        if (isLineEth) {
            return NETWORK_ETH_NET;
        }
        boolean isLineWifi = getWifiType(connManager);
        if (isLineWifi) {
            return NETWORN_WIFI;
        }
        boolean isMobileType = getMobileType(activeNetInfo, connManager);
        if (isMobileType) {
            return NETWORK_MOBILE;
        }
        return NETWORN_NONE;
    }

    private static boolean getMobileType(NetworkInfo activeNetInfo, ConnectivityManager connManager) {
        NetworkInfo networkInfo = connManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if (null != networkInfo) {
            NetworkInfo.State state = networkInfo.getState();
            String strSubTypeName = networkInfo.getSubtypeName();
            if (strSubTypeName != null) {
                if (strSubTypeName.equalsIgnoreCase("TD-SCDMA") || strSubTypeName.equalsIgnoreCase("WCDMA") || strSubTypeName.equalsIgnoreCase("CDMA2000")) {
                    return true;
                }
            }
            if (null != state) {
                if (state == NetworkInfo.State.CONNECTED || state == NetworkInfo.State.CONNECTING) {
                    switch (activeNetInfo.getSubtype()) {
                        case TelephonyManager.NETWORK_TYPE_GPRS: // 联通2g
                        case TelephonyManager.NETWORK_TYPE_CDMA: // 电信2g
                        case TelephonyManager.NETWORK_TYPE_EDGE: // 移动2g
                        case TelephonyManager.NETWORK_TYPE_1xRTT:
                        case TelephonyManager.NETWORK_TYPE_IDEN:
                        case TelephonyManager.NETWORK_TYPE_EVDO_A: // 电信3g
                        case TelephonyManager.NETWORK_TYPE_UMTS:
                        case TelephonyManager.NETWORK_TYPE_EVDO_0:
                        case TelephonyManager.NETWORK_TYPE_HSDPA:
                        case TelephonyManager.NETWORK_TYPE_HSUPA:
                        case TelephonyManager.NETWORK_TYPE_HSPA:
                        case TelephonyManager.NETWORK_TYPE_EVDO_B:
                        case TelephonyManager.NETWORK_TYPE_EHRPD:
                        case TelephonyManager.NETWORK_TYPE_HSPAP:
                        case TelephonyManager.NETWORK_TYPE_LTE:
                            return true;
                    }
                }
            }
        }
        return false;
    }

    /***
     * 获取是否是wifi类型
     * @param connManager
     * @return
     */
    private static boolean getWifiType(ConnectivityManager connManager) {
        boolean isWifi = false;
        // 判断是不是连接的是不是wifi
        NetworkInfo wifiInfo = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        if (null != wifiInfo) {
            NetworkInfo.State state = wifiInfo.getState();
            if (null != state)
                if (state == NetworkInfo.State.CONNECTED || state == NetworkInfo.State.CONNECTING) {
                    isWifi = true;
                }
        }
        return isWifi;
    }

    private static boolean getEthType(ConnectivityManager connManager) {
        boolean isEthNet = false;
        //判断以太网是否链接上了
        NetworkInfo ethNetInfo = connManager.getNetworkInfo(ConnectivityManager.TYPE_ETHERNET);
        if (null != ethNetInfo) {
            NetworkInfo.State state = ethNetInfo.getState();
            if (null != state)
                if (state == NetworkInfo.State.CONNECTED || state == NetworkInfo.State.CONNECTING) {
                    isEthNet = true;
                }
        }
        return isEthNet;
    }
}
