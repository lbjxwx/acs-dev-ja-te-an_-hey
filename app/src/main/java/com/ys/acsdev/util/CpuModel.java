package com.ys.acsdev.util;

import android.util.Log;


public class CpuModel {

    /***
     * 获取CPU型号
     * @return
     * rk3399
     */
    public static String getMobileType() {
        String cpuModel = SpUtils.getCpuModel();
        if (cpuModel != null && cpuModel.length() > 2) {
            return cpuModel;
        }
        cpuModel = android.os.Build.PRODUCT;
        if (cpuModel.contains("_")) {
            cpuModel = cpuModel.substring(0, cpuModel.indexOf("_"));
        }
        SpUtils.setCpuModel(cpuModel);
        return cpuModel;
    }


    /***
     * 判断主板是不是高通得主板
     * @return
     */
    public static boolean isGTCPU() {
        String cpuModel = getMobileType();
        Log.e("CPUMODEL", "=====cpuModel==" + cpuModel);
        if (cpuModel.startsWith("sdm") || cpuModel.startsWith("msm")) {
            return true;
        }
        return false;
    }

}
