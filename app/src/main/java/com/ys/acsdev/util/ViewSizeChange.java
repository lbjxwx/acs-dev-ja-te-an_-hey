package com.ys.acsdev.util;

import android.view.TextureView;
import android.widget.FrameLayout;

import com.ys.facepasslib.camera.CameraPreview;

public class ViewSizeChange {


    /***
     * 原理
     * 按照1280*720得显示比例进行拉伸
     * 以屏幕得宽度为拉伸基础
     * @param cameraPreview
     */
    public static void setCameraPassFaceSize(CameraPreview cameraPreview) {
        int Screenwidth = SpUtils.getScreenwidth();
        int Screenheight = SpUtils.getScreenheight();

        if (Screenheight > Screenwidth) {
            //竖屏
            int heightShow = 1280 * Screenwidth / 720;
            LogUtil.cdl("=====view的高度000===" + heightShow);
            FrameLayout.LayoutParams localLayoutParams = (FrameLayout.LayoutParams) cameraPreview.getLayoutParams();
            localLayoutParams.width = Screenwidth;
            localLayoutParams.height = heightShow;
            LogUtil.cdl("=====人脸识别显示得尺寸===" + Screenwidth + " / " + heightShow, true);
            cameraPreview.setLayoutParams(localLayoutParams);
            return;
        }
        //横屏
        FrameLayout.LayoutParams localLayoutParams = (FrameLayout.LayoutParams) cameraPreview.getLayoutParams();
        localLayoutParams.width = Screenwidth;
        localLayoutParams.height = Screenheight;
        LogUtil.cdl("=====人脸识别显示得尺寸===" + Screenwidth + " / " + Screenheight, true);
        cameraPreview.setLayoutParams(localLayoutParams);
    }


    /***
     * 原理
     * 按照1280*720得显示比例进行拉伸
     * 以屏幕得宽度为拉伸基础
     * @param cameraPreview
     */
    public static void setCameraArcFaceSize(TextureView cameraPreview) {
        int Screenwidth = SpUtils.getScreenwidth();
        int Screenheight = SpUtils.getScreenheight();
        int heightShow = 1280 * Screenwidth / 720;
        LogUtil.cdl("=====view的高度000===" + heightShow);
        FrameLayout.LayoutParams localLayoutParams = (FrameLayout.LayoutParams) cameraPreview.getLayoutParams();
        localLayoutParams.width = Screenwidth;
        localLayoutParams.height = heightShow;
        LogUtil.cdl("=====人脸识别显示得尺寸===" + Screenwidth + " / " + heightShow, true);
        cameraPreview.setLayoutParams(localLayoutParams);
    }

}
