package com.ys.acsdev.util.yface;

import android.content.Context;
import android.util.Log;

import java.io.File;
import java.lang.reflect.Method;

public class GPIOManager {
    private static final String TAG = "GPIOManager";
    private static final String USB_OTG_3288 = "/sys/class/gpio/gpio12/value";
    private static final String USB_OTG_3368 = "/sys/devices/platform/misc_power_en/otg_vbus";
    private static final String USB_OTG_3399 = "/sys/kernel/debug/usb@fe800000/rk_usb_force_mode";
    private static final String USB1_3288 = "/sys/class/gpio/gpio257/value";
    private static final String USB2_3288 = "/sys/class/gpio/gpio170/value";
    private static final String USB3_3288 = "/sys/class/gpio/gpio14/value";
    private static final String USB1_3368 = "/sys/devices/platform/misc_power_en/vbus_drv5";
    private static final String USB2_3368 = "/sys/devices/platform/misc_power_en/vbus_drv3";
    private static final String USB3_3368 = "/sys/devices/platform/misc_power_en/vbus_drv4";
    private static final String USB1_3399 = "/sys/devices/platform/misc_power_en/vbus_drv5";
    private static final String USB2_3399 = "/sys/devices/platform/misc_power_en/vbus_drv4";
    private static final String USB3_3399 = "/sys/devices/platform/vcc5v0-sys/usb30_5v_en";
    private static final String VOICE_3288 = "/sys/bus/i2c/devices/2-0010/spk";
    private static final String VOICE_3368 = "/sys/devices/platform/misc_power_en/spk_ctl";
    private static final String VOICE_3399 = "/sys/class/gpio/gpio1152/value";
    private static final String FAN_CONTROL_3288 = "/sys/class/custom_class/custom_dev/fan";
    private static final String FAN_CONTROL_3368 = "/sys/devices/platform/misc_power_en/fan_ctl";
    private static final String FAN_CONTROL_3399 = "/sys/devices/platform/misc_power_en/fan_ctl";
    private static final String INFRARED_LED_3288 = "/sys/class/custom_class/custom_dev/ir";
    private static final String INFRARED_LED_3368 = "/sys/devices/platform/misc_power_en/red_led";
    private static final String INFRARED_LED_3399 = "/sys/devices/platform/misc_power_en/ir_led";
    private static final String RELAY_3288 = "/sys/class/custom_class/custom_dev/relay";
    private static final String RELAY_3368 = "/sys/devices/platform/misc_power_en/rtl_ctl";
    private static final String RELAY_3399 = "/sys/devices/platform/misc_power_en/rtl_ctl";
    private static final String GREEN_LIGHT_3288 = "/sys/class/custom_class/custom_dev/green_led";
    private static final String GREEN_LIGHT_3368 = "/sys/devices/platform/misc_power_en/g_led";
    private static final String GREEN_LIGHT_3399 = "/sys/devices/platform/misc_power_en/g_led";
    private static final String RED_LIGHT_3288 = "/sys/class/custom_class/custom_dev/red_led";
    private static final String RED_LIGHT_3368 = "/sys/devices/platform/misc_power_en/r_led";
    private static final String RED_LIGHT_3399 = "/sys/devices/platform/misc_power_en/r_led";
    private static final String WHITE_LIGHT_3288 = "/sys/class/custom_class/custom_dev/white_led";
    private static final String WHITE_LIGHT_3368 = "/sys/devices/platform/misc_power_en/w_led";
    private static final String WHITE_LIGHT_3399 = "/sys/devices/platform/misc_power_en/w_led";
    private static final String SCALING_MAX_FREQ_3288 = "/sys/devices/system/cpu/cpufreq/policy0/scaling_max_freq";
    private static final String SCALING_MIN_FREQ_3288 = "/sys/devices/system/cpu/cpufreq/policy0/scaling_min_freq";
    private static GPIOManager myManager;
    private boolean isRk3288;
    private boolean isRk3368;
    private boolean isRk3399;
    private Context mContext;

    private GPIOManager(Context context) {
        this.mContext = context;
        this.isRk3288 = this.isRk3288();
        this.isRk3399 = this.isRk3399();
        this.isRk3368 = this.isRk3368();
        GpioFaceUtils.upgradeRootPermissionForExport();
        if (this.isRk3288()) {
            this.rootGpio(162);
            this.rootGpio(163);
            this.rootGpio(169);
            this.rootGpio(171);
            GpioFaceUtils.upgradeRootPermissionForGpioValue(257);
            GpioFaceUtils.upgradeRootPermissionForGpioValue(170);
            GpioFaceUtils.upgradeRootPermissionForGpioValue(14);
            GpioFaceUtils.upgradeRootPermission("/sys/devices/system/cpu/cpufreq/policy0/scaling_max_freq");
            GpioFaceUtils.upgradeRootPermission("/sys/devices/system/cpu/cpufreq/policy0/scaling_min_freq");
        } else if (this.isRk3399()) {
            this.rootGpio(1067);
            this.rootGpio(1066);
            this.rootGpio(1071);
            this.rootGpio(1072);
            GpioFaceUtils.upgradeRootPermission("/sys/devices/platform/vcc5v0-sys/usb30_5v_en");
        }

    }

    public static synchronized GPIOManager getInstance(Context context) {
        if (myManager == null) {
            myManager = new GPIOManager(context);
        }

        return myManager;
    }

    public String getUSBOTGStatus() {
        if (this.isRk3288) {
            return Utils.readGpioPG("/sys/class/gpio/gpio12/value");
        } else if (this.isRk3368) {
            return Utils.readGpioPG("/sys/devices/platform/misc_power_en/otg_vbus");
        } else {
            if (this.isRk3399) {
                String status = Utils.readGpioPG("/sys/kernel/debug/usb@fe800000/rk_usb_force_mode");
                Log.d("chenhuan", "status = " + status);
                if (status.contains("peripheral")) {
                    return "0";
                }

                if (status.contains("host")) {
                    return "1";
                }
            }

            return "";
        }
    }

    public void pullUpUSBOTG() {
        if (this.isRk3288) {
            Utils.writeStringFileFor7(new File("/sys/class/gpio/gpio12/value"), "1");
        } else if (this.isRk3368) {
            Utils.writeStringFileFor7(new File("/sys/devices/platform/misc_power_en/otg_vbus"), "1");
        } else if (this.isRk3399) {
            Utils.writeStringFileFor7(new File("/sys/kernel/debug/usb@fe800000/rk_usb_force_mode"), "host");
        }

    }

    public void pullDownUSBOTG() {
        if (this.isRk3288) {
            Utils.writeStringFileFor7(new File("/sys/class/gpio/gpio12/value"), "0");
        } else if (this.isRk3368) {
            Utils.writeStringFileFor7(new File("/sys/devices/platform/misc_power_en/otg_vbus"), "0");
        } else if (this.isRk3399) {
            Utils.writeStringFileFor7(new File("/sys/kernel/debug/usb@fe800000/rk_usb_force_mode"), "peripheral");
        }

    }

    public String getUSB1Status() {
        if (this.isRk3288) {
            return Utils.readGpioPG("/sys/class/gpio/gpio257/value");
        } else if (this.isRk3368) {
            return Utils.readGpioPG("/sys/devices/platform/misc_power_en/vbus_drv5");
        } else {
            return this.isRk3399 ? Utils.readGpioPG("/sys/devices/platform/misc_power_en/vbus_drv5") : "";
        }
    }

    public void pullUpUSB1() {
        if (this.isRk3288) {
            GpioFaceUtils.writeGpioValue(257, "1");
        } else if (this.isRk3368) {
            Utils.writeStringFileFor7(new File("/sys/devices/platform/misc_power_en/vbus_drv5"), "1");
        } else if (this.isRk3399) {
            Utils.writeStringFileFor7(new File("/sys/devices/platform/misc_power_en/vbus_drv5"), "1");
        }

    }

    public void pullDownUSB1() {
        if (this.isRk3288) {
            GpioFaceUtils.writeGpioValue(257, "0");
        } else if (this.isRk3368) {
            Utils.writeStringFileFor7(new File("/sys/devices/platform/misc_power_en/vbus_drv5"), "0");
        } else if (this.isRk3399) {
            Utils.writeStringFileFor7(new File("/sys/devices/platform/misc_power_en/vbus_drv5"), "0");
        }

    }

    public String getUSB2Status() {
        if (this.isRk3288) {
            return Utils.readGpioPG("/sys/class/gpio/gpio170/value");
        } else if (this.isRk3368) {
            return Utils.readGpioPG("/sys/devices/platform/misc_power_en/vbus_drv3");
        } else {
            return this.isRk3399 ? Utils.readGpioPG("/sys/devices/platform/misc_power_en/vbus_drv4") : "";
        }
    }

    public void pullUpUSB2() {
        if (this.isRk3288) {
            Utils.writeStringFileFor7(new File("/sys/class/gpio/gpio170/value"), "1");
        } else if (this.isRk3368) {
            Utils.writeStringFileFor7(new File("/sys/devices/platform/misc_power_en/vbus_drv3"), "1");
        } else if (this.isRk3399) {
            Utils.writeStringFileFor7(new File("/sys/devices/platform/misc_power_en/vbus_drv4"), "1");
        }

    }

    public void pullDownUSB2() {
        if (this.isRk3288) {
            Utils.writeStringFileFor7(new File("/sys/class/gpio/gpio170/value"), "0");
        } else if (this.isRk3368) {
            Utils.writeStringFileFor7(new File("/sys/devices/platform/misc_power_en/vbus_drv3"), "0");
        } else if (this.isRk3399) {
            Utils.writeStringFileFor7(new File("/sys/devices/platform/misc_power_en/vbus_drv4"), "0");
        }

    }

    public String getUSB3Status() {
        if (this.isRk3288) {
            return Utils.readGpioPG("/sys/class/gpio/gpio14/value");
        } else if (this.isRk3368) {
            return Utils.readGpioPG("/sys/devices/platform/misc_power_en/vbus_drv4");
        } else {
            return this.isRk3399 ? Utils.readGpioPG("/sys/devices/platform/vcc5v0-sys/usb30_5v_en") : "";
        }
    }

    public void pullUpUSB3() {
        if (this.isRk3288) {
            Utils.writeStringFileFor7(new File("/sys/class/gpio/gpio14/value"), "1");
        } else if (this.isRk3368) {
            Utils.writeStringFileFor7(new File("/sys/devices/platform/misc_power_en/vbus_drv4"), "1");
        } else if (this.isRk3399) {
            Utils.writeStringFileFor7(new File("/sys/devices/platform/vcc5v0-sys/usb30_5v_en"), "1");
        }

    }

    public void pullDownUSB3() {
        if (this.isRk3288) {
            Utils.writeStringFileFor7(new File("/sys/class/gpio/gpio14/value"), "0");
        } else if (this.isRk3368) {
            Utils.writeStringFileFor7(new File("/sys/devices/platform/misc_power_en/vbus_drv4"), "0");
        } else if (this.isRk3399) {
            Utils.writeStringFileFor7(new File("/sys/devices/platform/vcc5v0-sys/usb30_5v_en"), "0");
        }

    }

    public String getVoiceStatus() {
        if (this.isRk3288) {
            return Utils.readGpioPG("/sys/bus/i2c/devices/2-0010/spk");
        } else if (this.isRk3368) {
            return Utils.readGpioPG("/sys/devices/platform/misc_power_en/spk_ctl");
        } else {
            return this.isRk3399 ? Utils.readGpioPG("/sys/class/gpio/gpio1152/value") : "";
        }
    }

    public void pullUpVoice() {
        if (this.isRk3288) {
            Utils.writeStringFileFor7(new File("/sys/bus/i2c/devices/2-0010/spk"), "1");
        } else if (this.isRk3368) {
            Utils.writeStringFileFor7(new File("/sys/devices/platform/misc_power_en/spk_ctl"), "1");
        } else if (this.isRk3399) {
            Utils.writeStringFileFor7(new File("/sys/class/gpio/gpio1152/value"), "1");
        }

    }

    public void pullDownVoice() {
        if (this.isRk3288) {
            Utils.writeStringFileFor7(new File("/sys/bus/i2c/devices/2-0010/spk"), "0");
        } else if (this.isRk3368) {
            Utils.writeStringFileFor7(new File("/sys/devices/platform/misc_power_en/spk_ctl"), "0");
        } else if (this.isRk3399) {
            Utils.writeStringFileFor7(new File("/sys/class/gpio/gpio1152/value"), "0");
        }

    }

    public String getFanStatus() {
        if (this.isRk3288) {
            return Utils.readGpioPG("/sys/class/custom_class/custom_dev/fan");
        } else if (this.isRk3368) {
            return Utils.readGpioPG("/sys/devices/platform/misc_power_en/fan_ctl");
        } else {
            return this.isRk3399 ? Utils.readGpioPG("/sys/devices/platform/misc_power_en/fan_ctl") : "";
        }
    }

    public void pullUpFan() {
        if (this.isRk3288) {
            Utils.writeStringFileFor7(new File("/sys/class/custom_class/custom_dev/fan"), "1");
        } else if (this.isRk3368) {
            Utils.writeStringFileFor7(new File("/sys/devices/platform/misc_power_en/fan_ctl"), "1");
        } else if (this.isRk3399) {
            Utils.writeStringFileFor7(new File("/sys/devices/platform/misc_power_en/fan_ctl"), "1");
        }

    }

    public void pullDownFan() {
        if (this.isRk3288) {
            Utils.writeStringFileFor7(new File("/sys/class/custom_class/custom_dev/fan"), "0");
        } else if (this.isRk3368) {
            Utils.writeStringFileFor7(new File("/sys/devices/platform/misc_power_en/fan_ctl"), "0");
        } else if (this.isRk3399) {
            Utils.writeStringFileFor7(new File("/sys/devices/platform/misc_power_en/fan_ctl"), "0");
        }

    }

    public String getInfraredLedStatus() {
        if (this.isRk3288) {
            return Utils.readGpioPG("/sys/class/custom_class/custom_dev/ir");
        } else if (this.isRk3368) {
            return Utils.readGpioPG("/sys/devices/platform/misc_power_en/red_led");
        } else {
            return this.isRk3399 ? Utils.readGpioPG("/sys/devices/platform/misc_power_en/ir_led") : "";
        }
    }

    public void pullUpInfraredLed() {
        if (this.isRk3288) {
            Utils.writeStringFileFor7(new File("/sys/class/custom_class/custom_dev/ir"), "1");
        } else if (this.isRk3368) {
            Utils.writeStringFileFor7(new File("/sys/devices/platform/misc_power_en/red_led"), "1");
        } else if (this.isRk3399) {
            Utils.writeStringFileFor7(new File("/sys/devices/platform/misc_power_en/ir_led"), "1");
        }

    }

    public void pullDownInfraredLed() {
        if (this.isRk3288) {
            Utils.writeStringFileFor7(new File("/sys/class/custom_class/custom_dev/ir"), "0");
        } else if (this.isRk3368) {
            Utils.writeStringFileFor7(new File("/sys/devices/platform/misc_power_en/red_led"), "0");
        } else if (this.isRk3399) {
            Utils.writeStringFileFor7(new File("/sys/devices/platform/misc_power_en/ir_led"), "0");
        }

    }

    public String getRelayStatus() {
        if (this.isRk3288) {
            return Utils.readGpioPG("/sys/class/custom_class/custom_dev/relay");
        } else if (this.isRk3368) {
            return Utils.readGpioPG("/sys/devices/platform/misc_power_en/rtl_ctl");
        } else {
            return this.isRk3399 ? Utils.readGpioPG("/sys/devices/platform/misc_power_en/rtl_ctl") : "";
        }
    }

    public void pullUpRelay() {
        if (this.isRk3288) {
            Utils.writeStringFileFor7(new File("/sys/class/custom_class/custom_dev/relay"), "1");
        } else if (this.isRk3368) {
            Utils.writeStringFileFor7(new File("/sys/devices/platform/misc_power_en/rtl_ctl"), "1");
        } else if (this.isRk3399) {
            Utils.writeStringFileFor7(new File("/sys/devices/platform/misc_power_en/rtl_ctl"), "1");
        }

    }

    public void pullDownRelay() {
        if (this.isRk3288) {
            Utils.writeStringFileFor7(new File("/sys/class/custom_class/custom_dev/relay"), "0");
        } else if (this.isRk3368) {
            Utils.writeStringFileFor7(new File("/sys/devices/platform/misc_power_en/rtl_ctl"), "0");
        } else if (this.isRk3399) {
            Utils.writeStringFileFor7(new File("/sys/devices/platform/misc_power_en/rtl_ctl"), "0");
        }

    }

    public String getGreenLightStatus() {
        if (this.isRk3288) {
            return Utils.readGpioPG("/sys/class/custom_class/custom_dev/green_led");
        } else if (this.isRk3368) {
            return Utils.readGpioPG("/sys/devices/platform/misc_power_en/g_led");
        } else {
            return this.isRk3399 ? Utils.readGpioPG("/sys/devices/platform/misc_power_en/g_led") : "";
        }
    }

    public void pullUpGreenLight() {
        if (this.isRk3288) {
            Utils.writeStringFileFor7(new File("/sys/class/custom_class/custom_dev/green_led"), "1");
        } else if (this.isRk3368) {
            Utils.writeStringFileFor7(new File("/sys/devices/platform/misc_power_en/g_led"), "1");
        } else if (this.isRk3399) {
            Utils.writeStringFileFor7(new File("/sys/devices/platform/misc_power_en/g_led"), "1");
        }

    }

    public void pullDownGreenLight() {
        if (this.isRk3288) {
            Utils.writeStringFileFor7(new File("/sys/class/custom_class/custom_dev/green_led"), "0");
        } else if (this.isRk3368) {
            Utils.writeStringFileFor7(new File("/sys/devices/platform/misc_power_en/g_led"), "0");
        } else if (this.isRk3399) {
            Utils.writeStringFileFor7(new File("/sys/devices/platform/misc_power_en/g_led"), "0");
        }

    }

    public String getRedLightStatus() {
        if (this.isRk3288) {
            return Utils.readGpioPG("/sys/class/custom_class/custom_dev/red_led");
        } else if (this.isRk3368) {
            return Utils.readGpioPG("/sys/devices/platform/misc_power_en/r_led");
        } else {
            return this.isRk3399 ? Utils.readGpioPG("/sys/devices/platform/misc_power_en/r_led") : "";
        }
    }

    public void pullUpRedLight() {
        if (this.isRk3288) {
            Utils.writeStringFileFor7(new File("/sys/class/custom_class/custom_dev/red_led"), "1");
        } else if (this.isRk3368) {
            Utils.writeStringFileFor7(new File("/sys/devices/platform/misc_power_en/r_led"), "1");
        } else if (this.isRk3399) {
            Utils.writeStringFileFor7(new File("/sys/devices/platform/misc_power_en/r_led"), "1");
        }

    }

    public void pullDownRedLight() {
        if (this.isRk3288) {
            Utils.writeStringFileFor7(new File("/sys/class/custom_class/custom_dev/red_led"), "0");
        } else if (this.isRk3368) {
            Utils.writeStringFileFor7(new File("/sys/devices/platform/misc_power_en/r_led"), "0");
        } else if (this.isRk3399) {
            Utils.writeStringFileFor7(new File("/sys/devices/platform/misc_power_en/r_led"), "0");
        }

    }

    public String getWhiteLightStatus() {
        if (this.isRk3288) {
            return Utils.readGpioPG("/sys/class/custom_class/custom_dev/white_led");
        } else if (this.isRk3368) {
            return Utils.readGpioPG("/sys/devices/platform/misc_power_en/w_led");
        } else {
            return this.isRk3399 ? Utils.readGpioPG("/sys/devices/platform/misc_power_en/w_led") : "";
        }
    }

    public void pullUpWhiteLight() {
        if (this.isRk3288) {
            Utils.writeStringFileFor7(new File("/sys/class/custom_class/custom_dev/white_led"), "1");
        } else if (this.isRk3368) {
            Utils.writeStringFileFor7(new File("/sys/devices/platform/misc_power_en/w_led"), "1");
        } else if (this.isRk3399) {
            Utils.writeStringFileFor7(new File("/sys/devices/platform/misc_power_en/w_led"), "1");
        }

    }

    public void pullDownWhiteLight() {
        if (this.isRk3288) {
            Utils.writeStringFileFor7(new File("/sys/class/custom_class/custom_dev/white_led"), "0");
        } else if (this.isRk3368) {
            Utils.writeStringFileFor7(new File("/sys/devices/platform/misc_power_en/w_led"), "0");
        } else if (this.isRk3399) {
            Utils.writeStringFileFor7(new File("/sys/devices/platform/misc_power_en/w_led"), "0");
        }

    }

    public String getReservedIO1Status() {
        String status = "";
        if (this.isRk3288) {
            status = GpioFaceUtils.getGpioValue(162);
        } else if (this.isRk3368) {
            status = GpioFaceUtils.getGpioValue(91);
        } else if (this.isRk3399) {
            status = GpioFaceUtils.getGpioValue(1067);
        }

        return status;
    }

    public String getReservedIO2Status() {
        String status = "";
        if (this.isRk3288) {
            status = GpioFaceUtils.getGpioValue(163);
        } else if (this.isRk3368) {
            status = GpioFaceUtils.getGpioValue(90);
        } else if (this.isRk3399) {
            status = GpioFaceUtils.getGpioValue(1066);
        }

        return status;
    }

    public String getReservedIO3Status() {
        String status = "";
        if (this.isRk3288) {
            status = GpioFaceUtils.getGpioValue(169);
        } else if (this.isRk3368) {
            status = GpioFaceUtils.getGpioValue(111);
        } else if (this.isRk3399) {
            status = GpioFaceUtils.getGpioValue(1071);
        }

        return status;
    }

    public String getReservedIO4Status() {
        String status = "";
        if (this.isRk3288) {
            status = GpioFaceUtils.getGpioValue(171);
        } else if (this.isRk3368) {
            status = GpioFaceUtils.getGpioValue(109);
        } else if (this.isRk3399) {
            status = GpioFaceUtils.getGpioValue(1072);
        }

        return status;
    }

    public String getReservedIO4Direction() {
        String status = "";
        if (this.isRk3288) {
            status = GpioFaceUtils.getGpioDirection(171);
        } else if (this.isRk3368) {
            status = GpioFaceUtils.getGpioDirection(109);
        } else if (this.isRk3399) {
            status = GpioFaceUtils.getGpioDirection(1072);
        }

        return status;
    }

    public void setReservedIO4Out() {
        if (this.isRk3288) {
            GpioFaceUtils.setGpioDirection(171, 0);
        } else if (this.isRk3368) {
            GpioFaceUtils.setGpioDirection(109, 0);
        } else if (this.isRk3399) {
            GpioFaceUtils.setGpioDirection(1072, 0);
        }

    }

    public void setReservedIO4In() {
        if (this.isRk3288) {
            GpioFaceUtils.setGpioDirection(171, 1);
        } else if (this.isRk3368) {
            GpioFaceUtils.setGpioDirection(109, 1);
        } else if (this.isRk3399) {
            GpioFaceUtils.setGpioDirection(1072, 1);
        }

    }

    public boolean isIO4DirectionOut() {
        String direction = "out";
        String temp = "";
        if (this.isRk3288) {
            temp = GpioFaceUtils.getGpioDirection(171);
        } else if (this.isRk3368) {
            temp = GpioFaceUtils.getGpioDirection(109);
        } else if (this.isRk3399) {
            temp = GpioFaceUtils.getGpioDirection(1072);
        }

        return direction.equals(temp);
    }

    public void pullUpIO4() {
        if (this.isRk3288) {
            GpioFaceUtils.writeGpioValue(171, "1");
        } else if (this.isRk3368) {
            GpioFaceUtils.writeGpioValue(109, "1");
        } else if (this.isRk3399) {
            GpioFaceUtils.writeGpioValue(1072, "1");
        }

    }

    public void pullDownIO4() {
        if (this.isRk3288) {
            GpioFaceUtils.writeGpioValue(171, "0");
        } else if (this.isRk3368) {
            GpioFaceUtils.writeGpioValue(109, "0");
        } else if (this.isRk3399) {
            GpioFaceUtils.writeGpioValue(1072, "0");
        }

    }

    public void setMaxFreq(int value) {
        if (this.isRk3288) {
            Utils.writeStringFileFor7(new File("/sys/devices/system/cpu/cpufreq/policy0/scaling_max_freq"), value * 1000 + "");
        }

    }

    public void setMinFreq(int value) {
        if (this.isRk3288) {
            Utils.writeStringFileFor7(new File("/sys/devices/system/cpu/cpufreq/policy0/scaling_min_freq"), value * 1000 + "");
        }

    }

    public String getMaxFreq() {
        return this.isRk3288 ? Utils.readGpioPG("/sys/devices/system/cpu/cpufreq/policy0/scaling_max_freq") : "";
    }

    public String getMinFreq() {
        return this.isRk3288 ? Utils.readGpioPG("/sys/devices/system/cpu/cpufreq/policy0/scaling_min_freq") : "";
    }

    private String getVersion() {
        return this.getValueFromProp("ro.build.description").substring(0, 6);
    }

    public String getValueFromProp(String key) {
        String value = "";

        try {
            Class<?> classType = Class.forName("android.os.SystemProperties");
            Method getMethod = classType.getDeclaredMethod("get", String.class);
            value = (String) getMethod.invoke(classType, key);
        } catch (Exception var5) {
        }

        return value;
    }

    private boolean isRk3288() {
        return this.getVersion().contains("rk3288");
    }

    private boolean isRk3368() {
        return this.getVersion().contains("rk3368");
    }

    private boolean isRk3399() {
        return this.getVersion().contains("rk3399");
    }

    private void rootGpio(int index) {
        if (GpioFaceUtils.exportGpio(index)) {
            GpioFaceUtils.upgradeRootPermissionForGpio(index);
        }

    }
}
