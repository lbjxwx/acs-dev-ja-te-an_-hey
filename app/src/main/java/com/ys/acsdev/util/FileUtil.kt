//package com.ys.acsdev.util
//
//import java.io.File
//
//object FileUtil {
//
//    /***
//     * 这里有一个逻辑，如果内存小于500M.停止写入。保护系统
//     */
//    @JvmStatic
//    fun cutFile(sourceFile: File, targetFile: File) {
//        try {
//            if (!sourceFile.exists()) {
//                return
//            }
//            var fileLength = sourceFile.length() / 1024;
//            sourceFile.copyTo(targetFile, true)
//            sourceFile.delete()
//        } catch (e: Exception) {
//            e.printStackTrace()
//        }
//    }
//
//}