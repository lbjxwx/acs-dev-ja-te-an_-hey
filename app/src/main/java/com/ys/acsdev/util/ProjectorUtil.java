//package com.ys.acsdev.util;
//
//import android.content.Context;
//
//import com.ys.acsdev.bean.StoreEntity;
//import com.ys.acsdev.commond.AppInfo;
//import com.ys.acsdev.data.MySDCard;
//import com.ys.acsdev.service.AcsService;
//
//import java.util.List;
//
//public class ProjectorUtil {
//
//    /**
//     * 设置保存路径
//     *
//     * @param context
//     */
//    public static void setProjectorSavePath(Context context, String tag) {
//        Runnable runnable = new Runnable() {
//            @Override
//            public void run() {
//                getSdLastSize(context);
//                setProjector(context, tag);
//            }
//        };
//        AcsService.getInstance().executor(runnable);
//    }
//
//    private static void getSdLastSize(Context context) {
//        MySDCard mySDCard = new MySDCard(context);
//        String sdPath = AppInfo.BASE_PATH_INNER;
//        long lastSize = mySDCard.getAvailableExternalMemorySize(sdPath, 1);
//        LogUtil.cdl("====获取剩余空间===" + lastSize + " K ");
//        AppInfo.LAST_SD_SIZE = lastSize;
//    }
//
//    /**
//     * 设置存储设备信息
//     *
//     * @param context
//     */
//    private static void setProjector(Context context, String tag) {
//        LogUtil.cdl("ProjectorUtil=====startCheck===" + tag, true);
//        try {
//            MySDCard mySDCard = new MySDCard(context);
//            List<StoreEntity> saveList = mySDCard.getStroageFileList();
//            if (saveList == null || saveList.size() < 2) {  //只有一个存储设备
//                setProSavePath("");
//                return;
//            }
//            String usbPath = "";
//            for (StoreEntity storeEntity : saveList) {
//                int type = storeEntity.getType();
//                if (type == StoreEntity.TYPE_USB) {
//                    usbPath = storeEntity.getPath();
//                }
//            }
//            setProSavePath(usbPath);
//        } catch (Exception e) {
//            LogUtil.e("sdcard", "===ProjectorUtil===error=" + e.toString());
//            e.printStackTrace();
//        }
//    }
//
//    private static void setProSavePath(String savePath) {
//        LogUtil.cdl("ProjectorUtil=====setProSavePath===" + savePath);
//        SpUtils.setUSBPath(savePath);
//        if (savePath != null && savePath.length() > 3) {
//            FileUtils.creatPathNotExcit(savePath);
//        }
//    }
//
//}
