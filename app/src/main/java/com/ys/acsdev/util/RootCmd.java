package com.ys.acsdev.util;

import android.util.Log;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Method;

public class RootCmd {

    private static boolean mHaveRoot = false;

    public static final String CAMERA_FONR_MIRROR = "persist.hal.fcam.mirror";   //前摄像头显示镜像  true=镜像  false=非镜像
    public static final String CAMERA_BACK_MIRROR = "persist.hal.bcam.mirror";   //后摄像头显示镜像  true=镜像  false=非镜像

    //  获取和设置SystemProperties属性的代码
    public static String getProperty(String key, String defaultValue) {
        String value = defaultValue;
        try {
            Class<?> c = Class.forName("android.os.SystemProperties");
            Method get = c.getMethod("get", String.class, String.class);
            value = (String) (get.invoke(c, key, defaultValue));
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            logInfo("====获取得属性值===" + value);
            return value;
        }
    }

    public static void setProperty(String key, String value) {
        try {
            Class<?> c = Class.forName("android.os.SystemProperties");
            Method set = c.getMethod("set", String.class, String.class);
            set.invoke(c, key, value);
            logInfo("======写入属性success==" + key + " / " + value);
        } catch (Exception e) {
            logInfo("======写入属性error==" + e.toString());
            e.printStackTrace();
        }
    }

    private static void logInfo(String info) {
        LogUtil.cdl("===write=" + info);
    }


    /***
     * 判断当前设备有没有权限
     * @return
     */
    public static boolean haveRoot() {
        if (!mHaveRoot) {
            int ret = execRootCmdSilent("echo test"); // 通过执行测试命令来检测
            if (ret != -1) {
                mHaveRoot = true;
            } else {
            }
        } else {
        }
        return mHaveRoot;
    }

    // 执行命令但不关注结果输出
    public static int execRootCmdSilent(String cmd) {
        int result = -1;
        DataOutputStream dos = null;
        try {
            Process p = Runtime.getRuntime().exec("su");
            dos = new DataOutputStream(p.getOutputStream());
            dos.writeBytes(cmd + "\n");
            dos.flush();
            dos.writeBytes("exit\n");
            dos.flush();
            p.waitFor();
            result = p.exitValue();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (dos != null) {
                try {
                    dos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return result;
    }

    /**
     * 执行命令并且输出结果
     */
    public static String execRootCmdBackInfo(String cmd) {
        String result = "";
        DataOutputStream dos = null;
        DataInputStream dis = null;
        try {
            Process p = Runtime.getRuntime().exec("su");// 经过Root处理的android系统即有su命令
            dos = new DataOutputStream(p.getOutputStream());
            dis = new DataInputStream(p.getInputStream());
            Log.e("cdl", cmd);
            dos.writeBytes(cmd + "\n");
            dos.flush();
            dos.writeBytes("exit\n");
            dos.flush();
            String line = null;
            while ((line = dis.readLine()) != null) {
                Log.d("result", line);
                result += line;
            }
            p.waitFor();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (dos != null) {
                try {
                    dos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (dis != null) {
                try {
                    dis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return result;
    }


    /***
     * @param command
     * @return
     */
    public static boolean exusecmd(String command) {
        logInfo("===========" + command);
        Process process = null;
        DataOutputStream os = null;
        try {
            process = Runtime.getRuntime().exec("su");
            os = new DataOutputStream(process.getOutputStream());
            os.writeBytes(command + "\n");
            os.writeBytes("exit\n");
            os.flush();
            logInfo("======000==writeSuccess======");

            // 标准输入流（必须写在 waitFor 之前）
            String inStr = consumeInputStream(process.getInputStream());
            // 标准错误流（必须写在 waitFor 之前）
            String errStr = consumeInputStream(process.getErrorStream());

            int retCode = process.waitFor();
            if (retCode == 0) {
                //防止OOM挂起问题
//                System.out.println("程序正常执行结束");
            }
        } catch (Exception e) {
            logInfo("======111=writeError======" + e.toString());
            return false;
        } finally {
            try {
                if (os != null) {
                    os.close();
                }
                if (process != null) {
                    process.destroy();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return true;
    }


    /**
     * 消费inputstream，并返回
     */
    public static String consumeInputStream(InputStream is) {
        StringBuilder sb = new StringBuilder();
        BufferedReader br = null;
        try {
            br = new BufferedReader(new InputStreamReader(is));
            String s;
            while ((s = br.readLine()) != null) {
                logInfo("======consumeInputStream:" + s);
//            System.out.println(s);
                sb.append(s);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (br != null) {
                    br.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }

    public static void writeFileToSystemApp(String filePath, String sysFilePath) {
        exusecmd("mount -o rw,remount /system");
        exusecmd("rm -rf /system/app/guardian.apk");
        exusecmd("cp  " + filePath + " " + sysFilePath);
        exusecmd("chmod 777 /system/app/guardian.apk");
    }


    /***
     * 清理缓存目录下的文件
     */
    public static void clearApkCache() {
        exusecmd("rm -rf /data/user/0/com.android.packageinstaller/cache/*.apk");
    }

}
