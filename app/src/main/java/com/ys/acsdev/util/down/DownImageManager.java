package com.ys.acsdev.util.down;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.media.MediaScannerConnection;
import android.provider.MediaStore;
import android.util.Log;
import android.webkit.DownloadListener;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.etv.down.XutilDownFileEntity;
import com.etv.down.XutilDownStateListener;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;


/****
 * 图片下载工具类
 */
public class DownImageManager {

    Context context;
    XutilDownStateListener downloadListener;
    String imageDownPath;
    String savePathDown;
    boolean isAdd = false;

    public DownImageManager(Context context) {
        this.context = context;
    }

    public void setImageAddMediaCache(boolean isAdd) {
        this.isAdd = isAdd;
    }

    public void downImageWithGlide(String imagePath, String savePath, XutilDownStateListener downloadListener) {
        this.imageDownPath = imagePath;
        this.savePathDown = savePath;
        this.downloadListener = downloadListener;

        Glide.with(context).downloadOnly().load(imageDownPath).into(new SimpleTarget<File>() {
            @Override
            public void onResourceReady(@NonNull File resource, @Nullable Transition<? super File> transition) {
                try {
                    File destFile = new File(savePathDown);
                    copyFileToLocal(resource, destFile);
                } catch (Exception e) {
                    Log.e("down", "httpUtils=下载失败: " + e.toString());
                    XutilDownFileEntity xutilDownFileEntity =
                            new XutilDownFileEntity(XutilDownFileEntity.DOWN_STATE_FAIED,
                                    0, e.toString(), imageDownPath, savePathDown, 0);
                    backDownStatuesToView(xutilDownFileEntity);
                }
            }
        });
    }

    private void backDownStatuesToView(XutilDownFileEntity xutilDownFileEntity) {
        if (downloadListener == null) {
            return;
        }
        downloadListener.downStateInfo(xutilDownFileEntity);
    }

    /**
     * 复制文件
     *
     * @param source 输入文件
     * @param target 输出文件
     * @return 文件是否复制成功
     */
    private void copyFileToLocal(File source, File target) {
        FileInputStream fileInputStream = null;
        FileOutputStream fileOutputStream = null;
        try {
            fileInputStream = new FileInputStream(source);
            fileOutputStream = new FileOutputStream(target);
            byte[] buffer = new byte[1024];
            while (fileInputStream.read(buffer) > 0) {
                fileOutputStream.write(buffer);
            }
        } catch (Exception e) {
            XutilDownFileEntity xutilDownFileEntity = new XutilDownFileEntity(XutilDownFileEntity.DOWN_STATE_FAIED, 0, e.toString(), imageDownPath, savePathDown, 0);
            backDownStatuesToView(xutilDownFileEntity);
            e.printStackTrace();
        } finally {
            try {
                fileInputStream.close();
                fileOutputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (isAdd) {
            addMediaStore(source, source.getAbsolutePath());
        }
        XutilDownFileEntity xutilDownFileEntity = new XutilDownFileEntity(XutilDownFileEntity.DOWN_STATE_SUCCESS,
                0, "XUTIL 下载成功: " + savePathDown, imageDownPath, savePathDown, 0);
        backDownStatuesToView(xutilDownFileEntity);
    }


    /**
     * 把文件插入到系统图库
     *
     * @param targetFile 要保存的照片文件
     * @param path       要保存的照片的路径地址
     */
    public void addMediaStore(File targetFile, String path) {
        ContentResolver resolver = context.getContentResolver();
        ContentValues newValues = new ContentValues(5);
        newValues.put(MediaStore.Images.Media.DISPLAY_NAME, targetFile.getName());
        newValues.put(MediaStore.Images.Media.DATA, targetFile.getPath());
        newValues.put(MediaStore.Images.Media.DATE_MODIFIED, System.currentTimeMillis() / 1000);
        newValues.put(MediaStore.Images.Media.SIZE, targetFile.length());
        newValues.put(MediaStore.Images.Media.MIME_TYPE, "image/png");
        resolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, newValues);
        MediaScannerConnection.scanFile(context, new String[]{path}, null, null);//刷新相册
    }
}
