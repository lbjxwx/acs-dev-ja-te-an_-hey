package com.ys.acsdev.util.guardian;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.ys.acsdev.R;
import com.ys.acsdev.api.AppConfig;
import com.ys.acsdev.commond.AppInfo;
import com.ys.acsdev.util.APKUtil;
import com.ys.acsdev.util.CodeUtil;
import com.ys.acsdev.util.RootCmd;
import com.ys.acsdev.util.SystemManagerUtil;

/**
 * 安装守护进程方法
 */
public class InstallGuardianUtil {

    Context context;

    public InstallGuardianUtil(Context context) {
        this.context = context;
    }

    /**
     * 修改守护进程的状态
     * true 打开
     * false 关闭
     *
     * @param b
     */
    public static void setGuardianStaues(Context context, boolean b) {
        Intent intent = new Intent(AppInfo.CHANGE_GUARDIAN_STATUES);
        intent.putExtra(AppInfo.CHANGE_GUARDIAN_STATUES, b);
        context.sendBroadcast(intent);
    }

    public void startInstallGuardian() {
        boolean isApkInstall = APKUtil.ApkState(context, AppConfig.GUARDIAN_PACKAGE_NAME);
        if (isApkInstall) {  //已经安装就去检查版本号
            checkGuardianAppVersion();
        } else {   //没有安装，直接安装
            installGUardianApp();
        }
    }

    /***
     * 检查版本号，需不需要升级
     */
    private void checkGuardianAppVersion() {
        try {
            Log.i("write", "==程序已经安装===");
            String guardianPackName = AppConfig.GUARDIAN_PACKAGE_NAME;
            int guardianCode = APKUtil.getOtherAppVersion(context, guardianPackName);
            int updateCode = AppConfig.GUARDIAN_VERSION;
            if (updateCode > guardianCode) {
                installGUardianApp();
            }
            Log.i("write", "==程序已经安装00===" + guardianCode);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /***
     * anzhuang
     */
    private void installGUardianApp() {
        RawSourceEntity rawSourceEntity = getResourceEntity();
        if (rawSourceEntity == null) {
            return;
        }
        Log.e("write", "==获取的raw守护信息===" + rawSourceEntity.toString());
        int rourseId = rawSourceEntity.getRawId();
        long fileLength = rawSourceEntity.getFileLength();
        String savePath = AppInfo.BASE_APK_PATH + "/" + AppConfig.GUARDIAN_APP_NAME;
        FileRawWriteRunnable runnable = new FileRawWriteRunnable(context, rourseId, savePath, fileLength, new WriteSdListener() {
            @Override
            public void writeProgress(int progress) {
                Log.e("write", "===progress===" + progress);
            }

            @Override
            public void writeSuccess(String savePath) {
                Log.i("write", "====suucess==" + savePath);
                SystemManagerUtil.syncSdcard();
                RootCmd.writeFileToSystemApp(savePath, "/system/app/guardian.apk");
            }

            @Override
            public void writrFailed(String errorDesc) {
                Log.e("write", "writrFailed==" + errorDesc);
            }
        });
        runnable.setIdDelOldFile(true);
        Thread thread = new Thread(runnable);
        thread.start();
    }

    private RawSourceEntity getResourceEntity() {
        RawSourceEntity rawSourceEntity = new RawSourceEntity(R.raw.guardian, AppConfig.GUARDIAN_APK_LENGTH_71, context.getString(R.string.version_7));
        return rawSourceEntity;
    }
}
