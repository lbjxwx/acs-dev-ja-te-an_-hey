package com.ys.acsdev.util;

import android.content.Context;
import android.os.Build;
import android.text.TextUtils;

import com.ys.acsdev.api.AppConfig;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.LineNumberReader;

/**
 * 获取设备相关的编码
 */

public class CodeUtil {

    public static String getUniquePsuedoID() {
        String onlyCode = SpUtils.getDeviceId();
        if (onlyCode != null && onlyCode.length() > 2) {
            return onlyCode;
        }
        onlyCode = getEthMAC();
        onlyCode = onlyCode.replace(":", "");
        SpUtils.setDeviceId(onlyCode);
        return onlyCode;
    }

    /***
     * 获取设备的Mac地址
     * @return
     */
    private static String getEthMAC() {
        String macSerial = null;
        String str = "";
        try {
            Process ex = Runtime.getRuntime().exec("cat /sys/class/net/eth0/address");
            InputStreamReader ir = new InputStreamReader(ex.getInputStream());
            LineNumberReader input = new LineNumberReader(ir);
            while (null != str) {
                str = input.readLine();
                if (str != null) {
                    macSerial = str.trim();
                    break;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return macSerial != null && !macSerial.equals("") && macSerial.length() == 17 ? macSerial.toUpperCase() : "";
    }

    public static String getVersionAll(Context context) {
        if (context == null) {
            return "";
        }
        int appVersionCode = APKUtil.getVersionCode(context);
        String appVersionName = APKUtil.getVersionName(context);
//        String version = appVersionName + "_" + appVersionCode;
        String version = appVersionName + "_" + AppConfig.APP_CUSTOM_TYPE;
        return version;
    }


    /***
     * 获取主板的序列号
     * @return
     */
    public static String getSerDevNum() {
        String devNum = SpUtils.getSerDeviceId();
        if (devNum != null && devNum.length() > 2) {
            return devNum;
        }
        devNum = RootCmd.getProperty("persist.ys.factroy.code", "");
        SpUtils.setSerDeviceId(devNum);
        return devNum;
    }

    /**
     * 把一个文件转化为byte字节数组。
     *
     * @return
     */
    public static byte[] fileConvertToByteArray(File file) {
        byte[] data = null;

        try {
            FileInputStream fis = new FileInputStream(file);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();

            int len;
            byte[] buffer = new byte[1024];
            while ((len = fis.read(buffer)) != -1) {
                baos.write(buffer, 0, len);
            }

            data = baos.toByteArray();

            fis.close();
            baos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return data;
    }

    //将字节数组转为字符串
    public static String decode(byte[] no) {
        String result = "";
        try {
            long t = 0;
            final int start = 3;
            final int end = -1;
            for (int i = start; i > end; i--) {
                t <<= 8;
                t |= no[i] & 0xff;
            }
            result = String.valueOf(t);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }


    /***
     * 获取系统版本号
     * 板卡型号 + 系统版本 + 版本号 +软件版本号
     * @param context
     * @return
     */
    public static String getSystCodeVersion(Context context) {
        String codeBack = "";
        String sysCode = getSysVersion();
        String appVersion = "(" + getAppVersion(context) + ")";
        codeBack = sysCode + appVersion;
        return codeBack;
    }

    /**
     * 获取系统版本号
     *
     * @return
     */
    public static String getSysVersion() {
        String sysCodeBack = "";
        String cpuModule = CpuModel.getMobileType();
        String systemVersion = getSystemVersion();
        String imgVersion = getImgVersion();
        sysCodeBack = cpuModule + "_" + systemVersion + "_" + imgVersion;
        return sysCodeBack;
    }

    /***
     * 获取固件版本号
     * @return
     */
    public static String getImgVersion() {
        String version = Build.DISPLAY.toString().trim();
        try {
            if (TextUtils.isEmpty(version) || version.length() < 2) {
                return "-获取失败-";
            }
            //前面得应为全部都不要了
            version = version.substring(version.indexOf("20"));
            //先获取前面得年月日
            String date = version.substring(0, version.indexOf("."));
            //去掉年月日后边得数据
            String versionOther = version.substring(version.indexOf(".") + 1);
            //抽取后边得数字
            String backNum = getAlbNum(versionOther);
            version = date + "." + backNum;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return version;
    }

    /**
     * 从字符串中抽取数字
     *
     * @param versionNum
     * @return
     */
    private static String getAlbNum(String versionNum) {
        versionNum = versionNum.trim();
        String str2 = "";
        if (versionNum != null && !"".equals(versionNum)) {
            for (int i = 0; i < versionNum.length(); i++) {
                if (versionNum.charAt(i) >= 48 && versionNum.charAt(i) <= 57) {
                    str2 += versionNum.charAt(i);
                }
            }
        }
        return str2;
    }


    public static String getAppVersion(Context context) {
        String appVersion = APKUtil.getVersionName(context) + "_"
                + APKUtil.getVersionCode(context);
        return appVersion;
    }

    /**
     * 获取当前手机系统版本号
     *
     * @return 系统版本号
     */
    public static String getSystemVersion() {
//        String systemVersion = "4.4";   //7.0
//        int sdkVersion = android.os.Build.VERSION.SDK_INT;
//        if (sdkVersion == Build.VERSION_CODES.KITKAT) {   //19
//            systemVersion = "4.4";
//        } else if (sdkVersion == Build.VERSION_CODES.LOLLIPOP) {  //21
//            systemVersion = "5.0";
//        } else if (sdkVersion == Build.VERSION_CODES.LOLLIPOP_MR1) {  //22
//            systemVersion = "5.1";
//        } else if (sdkVersion == Build.VERSION_CODES.M) {  //23
//            systemVersion = "6.0";
//        } else if (sdkVersion == Build.VERSION_CODES.N) {  //24
//            systemVersion = "7.0";
//        } else if (sdkVersion == Build.VERSION_CODES.N_MR1) {  //25
//            systemVersion = "7.1";
//        } else if (sdkVersion == Build.VERSION_CODES.O) {  //26
//            systemVersion = "8.0";
//        } else {
//            systemVersion = "9.0";
//        }
//        return systemVersion;
        return android.os.Build.VERSION.RELEASE;
    }

}
