//package com.ys.acsdev.util.camera;
//
//import android.graphics.Bitmap;
//import android.graphics.BitmapFactory;
//import android.graphics.Matrix;
//import android.hardware.Camera;
//import android.os.Handler;
//import android.util.Log;
//
//
//import com.ys.acsdev.api.AppConfig;
//import com.ys.acsdev.commond.AppInfo;
//import com.ys.acsdev.util.FileUtil;
//
//import java.io.BufferedOutputStream;
//import java.io.File;
//import java.io.FileOutputStream;
//
///**
// * Created by jsjm on 2017/12/9.
// */
//
//public class PicSaveFile {
//    public final String TAG = "CameraSimple";
//
//    public void saveImage(final int mCameraId, final byte[] data, final WritrFileListener listener) {
//        Log.e(TAG, "=======拍照回掉===");
//        FileUtil.creatPathNotExcit();
//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//                try {
//                    String picturePath = AppInfo.BASE_IMAGE_PATH + "/" + System.currentTimeMillis() + ".jpg";
//                    File file = new File(picturePath);
//                    if (!file.exists()) {
//                        file.createNewFile();
//                    }
//                    Log.e(TAG, "拍照回掉===0000000000000");
//                    // 获取当前旋转角度, 并旋转图片
//                    Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0,
//                            data.length);
//                    if (mCameraId == Camera.CameraInfo.CAMERA_FACING_BACK) {
//                        bitmap = rotateBitmapByDegree(bitmap, 90);
//                    } else {
//                        bitmap = rotateBitmapByDegree(bitmap, -90);
//                    }
//                    Log.e(TAG, "拍照回掉===111111111111111");
//                    BufferedOutputStream bos = new BufferedOutputStream(
//                            new FileOutputStream(file));
//                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bos);
//                    bos.flush();
//                    bos.close();
//                    Log.e(TAG, "拍照回掉===222222222222222222");
//                    bitmap.recycle();
//                    Log.e(TAG, "拍照回掉===3333333333333333333");
//                    backState(picturePath, true, listener);
//                } catch (Exception e) {
//                    backState("", false, listener);
//                    Log.e(TAG, "拍照回掉异常===" + e.toString());
//                    e.printStackTrace();
//                }
//            }
//        }).start();
//    }
//
//    private Handler handler = new Handler();
//
//    public void backState(final String savePath, final boolean isSuccess, final WritrFileListener listener) {
//        handler.post(new Runnable() {
//            @Override
//            public void run() {
//                if (isSuccess) {
//                    listener.writeSuccess(savePath);
//                } else {
//                    listener.writeFailed();
//                }
//            }
//        });
//    }
//
//    // 旋转图片
//    public Bitmap rotateBitmapByDegree(Bitmap bm, int degree) {
//        Bitmap returnBm = null;
//        Matrix matrix = new Matrix();
//        matrix.postRotate(degree);
//        try {
//            returnBm = Bitmap.createBitmap(bm, 0, 0, bm.getWidth(),
//                    bm.getHeight(), matrix, true);
//        } catch (OutOfMemoryError e) {
//        }
//        if (returnBm == null) {
//            returnBm = bm;
//        }
//        if (bm != returnBm) {
//            bm.recycle();
//        }
//        return returnBm;
//    }
//
//    public interface WritrFileListener {
//        void writeSuccess(String savePath);
//
//        void writeFailed();
//    }
//
//}