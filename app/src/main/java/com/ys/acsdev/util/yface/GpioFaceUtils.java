
package com.ys.acsdev.util.yface;

import android.os.SystemClock;
import android.util.Log;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class GpioFaceUtils {
    private static final String TAG = "GpioUtils";
    private static long mTime = 0L;
    private static int mFailTimes = 0;

    public GpioFaceUtils() {
    }

    public static void upgradeRootPermissionForExport() {
        upgradeRootPermission("/sys/class/gpio/export");
    }

    public static boolean exportGpio(int gpio) {
        return writeNode("/sys/class/gpio/export", "" + gpio);
    }

    public static void upgradeRootPermissionForGpio(int gpio) {
        upgradeRootPermission("/sys/class/gpio/gpio" + gpio + "/direction");
        upgradeRootPermission("/sys/class/gpio/gpio" + gpio + "/value");
    }

    public static void upgradeRootPermissionForGpioValue(int gpio) {
        upgradeRootPermission("/sys/class/gpio/gpio" + gpio + "/value");
    }

    public static boolean setGpioDirection(int gpio, int arg) {
        String gpioDirection = "";
        if (arg == 0) {
            gpioDirection = "out";
        } else {
            if (arg != 1) {
                return false;
            }

            gpioDirection = "in";
        }

        return writeNode("/sys/class/gpio/gpio" + gpio + "/direction", gpioDirection);
    }

    public static String getGpioDirection(int gpio) {
        String gpioDirection = "";
        gpioDirection = readNode("/sys/class/gpio/gpio" + gpio + "/direction");
        return gpioDirection;
    }

    public static boolean writeGpioValue(int gpio, String arg) {
        return writeNode("/sys/class/gpio/gpio" + gpio + "/value", arg);
    }

    public static String getGpioValue(int gpio) {
        return readNode("/sys/class/gpio/gpio" + gpio + "/value");
    }

    public static boolean upgradeRootPermission(String path) {
        Process process = null;
        DataOutputStream os = null;

        try {
            String cmd = "chmod 777 " + path;
            process = Runtime.getRuntime().exec("su");
            os = new DataOutputStream(process.getOutputStream());
            os.writeBytes(cmd + "\n");
            os.writeBytes("exit\n");
            os.flush();
            process.waitFor();
        } catch (Exception var14) {
        } finally {
            try {
                if (os != null) {
                    os.close();
                }

                process.destroy();
            } catch (Exception var12) {
            }

        }

        try {
            return process.waitFor() == 0;
        } catch (InterruptedException var13) {
            var13.printStackTrace();
            return false;
        }
    }

    private static boolean writeNode(String path, String arg) {
        Log.d("GpioUtils", "Gpio_test set node path: " + path + " arg: " + arg);
        if (path != null && arg != null) {
            FileWriter fileWriter = null;
            Object bufferedWriter = null;

            boolean var5;
            try {
                fileWriter = new FileWriter(path);
                fileWriter.write(arg);
                return true;
            } catch (Exception var15) {
                Log.e("GpioUtils", "Gpio_test write node error!! path" + path + " arg: " + arg);
                var15.printStackTrace();
                var5 = false;
            } finally {
                try {
                    if (fileWriter != null) {
                        fileWriter.close();
                    }
                    if (bufferedWriter != null) {
                        ((BufferedWriter) bufferedWriter).close();
                    }
                } catch (IOException var14) {
                    var14.printStackTrace();
                }

            }

            return var5;
        } else {
            Log.e("GpioUtils", "set node error");
            return false;
        }
    }

    private static String readNode(String path) {
        String result = "";
        FileReader fread = null;
        BufferedReader buffer = null;
        try {
            fread = new FileReader(path);
            buffer = new BufferedReader(fread);
            String str = null;
            if ((str = buffer.readLine()) != null) {
                result = str;
            }
            mFailTimes = 0;
        } catch (IOException var13) {
            Log.e("GpioUtils", "IO Exception");
            var13.printStackTrace();
            if (mTime == 0L || SystemClock.uptimeMillis() - mTime < 1000L) {
                ++mFailTimes;
            }
            if (mFailTimes >= 3) {
                Log.d("GpioUtils", "read format node continuous failed three times, exist thread");
            }
        } finally {
            try {
                if (buffer != null) {
                    buffer.close();
                }
                if (fread != null) {
                    fread.close();
                }
            } catch (IOException var12) {
                var12.printStackTrace();
            }
        }
        return result;
    }
}
