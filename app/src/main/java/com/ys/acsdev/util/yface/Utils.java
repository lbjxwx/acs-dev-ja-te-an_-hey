package com.ys.acsdev.util.yface;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

public class Utils {
    public Utils() {
    }

    public static String readGpioPG(String path) {
        String str = "";
        File file = new File(path);

        try {
            FileInputStream fileInputStream = new FileInputStream(file);
            byte[] buffer = new byte[16];
            fileInputStream.read(buffer);
            fileInputStream.close();
            str = new String(buffer);
        } catch (FileNotFoundException var5) {
            var5.printStackTrace();
        } catch (UnsupportedEncodingException var6) {
            var6.printStackTrace();
        } catch (IOException var7) {
            var7.printStackTrace();
        }

        return str;
    }

    public static void writeStringFileFor7(File file, String content) {
        FileOutputStream out = null;

        try {
            out = new FileOutputStream(file);
            out.write(content.getBytes());
            out.close();
        } catch (FileNotFoundException var14) {
            var14.printStackTrace();
        } catch (IOException var15) {
            var15.printStackTrace();
        } finally {
            if (out != null) {
                try {
                    out.close();
                } catch (IOException var13) {
                    var13.printStackTrace();
                }
            }

        }

    }
}
