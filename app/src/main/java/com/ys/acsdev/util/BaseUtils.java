package com.ys.acsdev.util;

import android.content.Context;

public class BaseUtils {

    public static int dp2px(Context context, float dipValue) {
        if (context == null)
            return 0;
        float scale = context.getResources().getDisplayMetrics().density;
        float backNum = dipValue * scale + 0.5f;
        return (int) backNum;
    }

    public static int getScreenWidth(Context context) {
        return SpUtils.getScreenwidth();
    }

    public static int getScreenHeight(Context context) {
        return SpUtils.getScreenheight();
    }


}
