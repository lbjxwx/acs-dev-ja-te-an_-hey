package com.ys.acsdev.view.dialog;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.ys.acsdev.R;
import com.ys.acsdev.listener.SeekBarBackListener;
import com.ys.acsdev.util.SpUtils;

/***
 * 通用dialog,一句话，两个按钮
 */
public class SeekBarDialog {
    private Context context;
    private Dialog dialog;
    SeekBarBackListener dialogClick;
    public Button btn_sure;
    Button btn_no;
    public TextView dialog_title;
    SeekBar seekBar_progress;
    RelativeLayout rela_click_close;
    TextView tv_progress;

    public SeekBarDialog(Context context) {
        this.context = context;
        dialog = new Dialog(context, R.style.MyDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View dialog_view = View.inflate(context, R.layout.seekbar_dialog, null);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(SpUtils.getScreenwidth(), SpUtils.getScreenheight());
        dialog.setContentView(dialog_view, params);
        dialog.setCancelable(true); // true点击屏幕以外关闭dialog

        rela_click_close = (RelativeLayout) dialog_view.findViewById(R.id.rela_click_close);
        rela_click_close.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                dissmiss();
            }
        });
        btn_sure = (Button) dialog_view.findViewById(R.id.btn_dialog_yes);
        btn_no = (Button) dialog_view.findViewById(R.id.btn_dialog_no);
        tv_progress = (TextView) dialog_view.findViewById(R.id.tv_progress);
        dialog_title = (TextView) dialog_view.findViewById(R.id.dialog_title);
        seekBar_progress = (SeekBar) dialog_view.findViewById(R.id.seekBar_progress);
        btn_sure.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                dissmiss();
            }
        });

        btn_no.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                dissmiss();
            }
        });

        seekBar_progress.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean b) {
                tv_progress.setText(progress + "");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                int progress = seekBar.getProgress();
                if (dialogClick != null) {
                    dialogClick.backNumInfo(progress);
                }
            }
        });
    }

    /***
     * 通用 100 得进度
     * @param title
     * @param progress
     */
    public void show(String title, int progress) {
        try {
            tv_progress.setText(progress + "");
            seekBar_progress.setProgress(progress);
            dialog_title.setText(title);
            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /***
     * 高度定制化得seekBar
     * @param title
     * @param progress
     * @param totalProgress
     */
    public void show(String title, int progress, int totalProgress) {
        try {
            seekBar_progress.setMax(totalProgress);
            tv_progress.setText(progress + "");
            seekBar_progress.setProgress(progress);
            dialog_title.setText(title);
            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void dissmiss() {
        try {
            if (dialog != null && dialog.isShowing()) {
                dialog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void setOnDialogClickListener(SeekBarBackListener dc) {
        dialogClick = dc;
    }

}
