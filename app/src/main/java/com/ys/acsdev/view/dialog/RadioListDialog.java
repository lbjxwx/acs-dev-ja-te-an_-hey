package com.ys.acsdev.view.dialog;

import android.app.Dialog;
import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.ys.acsdev.R;
import com.ys.acsdev.adapter.RadioItemAdapter;
import com.ys.acsdev.bean.RedioEntity;
import com.ys.acsdev.listener.AdapterItemClickListener;
import com.ys.acsdev.listener.RadioChooiceListener;
import com.ys.acsdev.util.SpUtils;

import java.util.ArrayList;
import java.util.List;

public class RadioListDialog implements AdapterItemClickListener {

    private Context context;
    private Dialog dialog;
    private Button btn_sure, btn_dialog_cacel;
    private TextView dialog_title;
    private ListView lv_content_view;
    private RadioItemAdapter radioItemAdapter;
    private List<RedioEntity> listRadio = null;
    private TextView tv_desc_menu;

    public RadioListDialog(Context context) {
        this.context = context;
        dialog = new Dialog(context, R.style.MyDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View dialog_view = View.inflate(context, R.layout.radio_list_dialog, null);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(SpUtils.getScreenwidth(), SpUtils.getScreenheight());
        dialog.setContentView(dialog_view, params);
        dialog.setCancelable(true); // true点击屏幕以外关闭dialog
        tv_desc_menu = (TextView) dialog_view.findViewById(R.id.tv_desc_menu);
        btn_sure = (Button) dialog_view.findViewById(R.id.btn_dialog_yes);
        btn_dialog_cacel = (Button) dialog_view.findViewById(R.id.btn_dialog_cacel);
        dialog_title = (TextView) dialog_view.findViewById(R.id.dialog_title);
        lv_content_view = (ListView) dialog_view.findViewById(R.id.lv_content_view);
        listRadio = new ArrayList<RedioEntity>();
        radioItemAdapter = new RadioItemAdapter(context, listRadio);
        radioItemAdapter.setAdapterItemClickListener(this);
        lv_content_view.setAdapter(radioItemAdapter);
        btn_sure.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                dissmiss();
                if (radioChooiceListener == null) {
                    return;
                }
                radioChooiceListener.backChooiceInfo(redioEntityCache, chooicePositionCache);
            }
        });
        btn_dialog_cacel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dissmiss();
            }
        });
    }

    RedioEntity redioEntityCache;
    int chooicePositionCache = 0;

    public void show(String title, List<RedioEntity> lists, int checkPosition, String desc) {
        try {
            chooicePositionCache = checkPosition;
            if (lists != null && lists.size() > 1 && chooicePositionCache != -1) {
                redioEntityCache = lists.get(checkPosition);
            }
            if (desc == null || desc.length() < 2) {
                tv_desc_menu.setVisibility(View.GONE);
            } else {
                tv_desc_menu.setVisibility(View.VISIBLE);
            }
            tv_desc_menu.setText(desc);
            dialog_title.setText(title);
            this.listRadio = lists;
            radioItemAdapter.setRadioList(listRadio, checkPosition);
            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void adapterItemClick(int id, Object object, int position) {
        radioItemAdapter.setRadioList(listRadio, position);
        redioEntityCache = listRadio.get(position);
        chooicePositionCache = position;
    }


    public void dissmiss() {
        try {
            if (dialog != null && dialog.isShowing()) {
                dialog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    RadioChooiceListener radioChooiceListener;

    public void setRadioChooiceListener(RadioChooiceListener radioChooiceListener) {
        this.radioChooiceListener = radioChooiceListener;
    }


}
