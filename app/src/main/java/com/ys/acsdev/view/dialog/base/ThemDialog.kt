package com.kermitye.baselib.dialog

import android.content.DialogInterface
import android.os.Bundle
import androidx.annotation.LayoutRes

/**
 * Created by kermitye on 2018/10/19 11:44
 */
class ThemDialog : BaseMsgDialog() {
    private var convertListener: ViewConvertListener? = null
    private var onDismiss :((DialogInterface?) -> Unit)? = null
    private var mOnViewStateRestored: (() -> Unit)? = null

    companion object {
        fun init(): ThemDialog = ThemDialog()
    }

    override fun intLayoutId(): Int = layoutId

    override fun convertView(holderDialog: DialogViewHolder, dialog: BaseMsgDialog) {
        convertListener?.convertView(holderDialog, dialog)
    }

    fun setOnViewStateRestored(onViewStateRestored: () -> Unit) = apply {
        mOnViewStateRestored = onViewStateRestored
    }

    fun setLayoutId(@LayoutRes layoutId: Int) = apply { this.layoutId = layoutId }

    fun setConvertListener(listener: ViewConvertListener) = apply { this.convertListener = listener }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (savedInstanceState != null)
            convertListener = savedInstanceState.getParcelable("listener")
    }

    fun setOnDimiss(listener: (DialogInterface?) -> Unit) {
        onDismiss = listener
    }

    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)
        onDismiss?.let { it(dialog) }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putParcelable("listener", convertListener)
    }

    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        mOnViewStateRestored?.let { it() }
        super.onViewStateRestored(savedInstanceState)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        convertListener = null
    }

    fun isShowing(): Boolean = dialog != null && dialog?.isShowing ?: false
}