package com.ys.acsdev.view.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.ys.acsdev.R;
import com.ys.acsdev.api.AppConfig;
import com.ys.acsdev.commond.AppInfo;
import com.ys.acsdev.listener.QrDismissListener;
import com.ys.acsdev.manager.ImageManager;
import com.ys.acsdev.service.AcsService;
import com.ys.acsdev.util.LogUtil;
import com.ys.qrcodelib.QRCodeUtil;

import java.io.File;

public class QrBottomDialog implements View.OnClickListener {

    Activity activity;
    Dialog dialog;
    ImageView dq_iv;
    TextView tv_bottom_desc;
    Button btn_questtion;

    public QrBottomDialog(Activity activity) {
        DisplayMetrics metric = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(metric);
        int mScreenWidth = metric.widthPixels;
        this.activity = activity;
        dialog = new Dialog(activity, R.style.BottomDialog);
        View root = LayoutInflater.from(activity).inflate(
                R.layout.dialog_qr, null);
        dq_iv = (ImageView) root.findViewById(R.id.dq_iv);
        tv_bottom_desc = (TextView) root.findViewById(R.id.tv_bottom_desc);
        btn_questtion = (Button) root.findViewById(R.id.btn_questtion);
        btn_questtion.setVisibility(View.GONE);
        btn_questtion.setOnClickListener(this);
        dialog.setContentView(root);
        Window dialogWindow = dialog.getWindow();
        dialogWindow.setGravity(Gravity.BOTTOM);
        WindowManager.LayoutParams lp = dialogWindow.getAttributes(); // 获取对话框当前的参数值
        lp.x = 0; // 新位置X坐标
        lp.y = 0; // 新位置Y坐标
        lp.width = mScreenWidth;
        root.measure(0, 0);
        lp.height = root.getMeasuredHeight();
        lp.alpha = 9f; // 透明度
        dialogWindow.setAttributes(lp);
    }

    @Override
    public void onClick(View view) {
//        if (view.getId() == R.id.btn_questtion) {
//            Intent intent = new Intent(activity, AskForbidenActivity.class);
//            activity.startActivity(intent);
//        }
    }

    public void showByText() {
        String data = AppInfo.getQrCodeUrl();
        LogUtil.persoon("==二维码显示的信息==" + data);
        disPlayQrImaage(data);
        dialog.show();

        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                if (mListener != null) {
                    mListener.onDismiss();
                }
            }
        });
    }

    private void disPlayQrImaage(String data) {
        String visitorDownPath = AppInfo.QR_VISITOR_DOWN_PATH;
        File fileDownWeb = new File(visitorDownPath);
        if (fileDownWeb.exists()) {
            loadVisitorQrCodePath(visitorDownPath);
            return;
        }
        File defaultQrCode = new File(AppInfo.QR_VISITOR_PATH);
        if (AppInfo.resetVisitorQr && defaultQrCode.exists()) {
            defaultQrCode.delete();
        }
        QRCodeUtil runnable = new QRCodeUtil(data, AppInfo.QR_VISITOR_PATH, new QRCodeUtil.ErCodeBackListener() {
            @Override
            public void createErCodeState(String errorDes, boolean isCreate, String path) {
                LogUtil.logo("加载访客二维码===加载自动胜场的二维码: " + errorDes + " / " + isCreate + " / " + path);
                if (!isCreate) {
                    tv_bottom_desc.setText("Qr code generation failed, please restart");
                    return;
                }
                AppInfo.resetVisitorQr = false;
                loadVisitorQrCodePath(path);
            }
        });
        AcsService.getInstance().executor(runnable);
    }

    private void loadVisitorQrCodePath(String visitorDownPath) {
        Glide.with(activity).clear(dq_iv);
        ImageManager.displayImagePathNoCache(activity, visitorDownPath, dq_iv);
    }

    public void dismissQrDialog() {
        if (dialog == null) {
            return;
        }
        if (dialog.isShowing()) {
            dialog.dismiss();
        }
    }

    private QrDismissListener mListener;

    public void setOnDismissListener(QrDismissListener listener) {
        this.mListener = listener;
    }


}


