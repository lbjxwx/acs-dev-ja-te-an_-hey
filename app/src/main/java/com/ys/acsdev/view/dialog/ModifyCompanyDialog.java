package com.ys.acsdev.view.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.text.InputType;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ys.acsdev.R;
import com.ys.acsdev.util.KeyBoardUtil;
import com.ys.acsdev.view.MyToastView;


/***
 * 带一个输入框的
 */
public class ModifyCompanyDialog implements OnClickListener {
    private Context context;
    private Dialog dialog;
    Button btn_cacel, btn_modify;
    EditText et_company;
    EditText et_department;
    EditText et_nickname;

    public ModifyCompanyDialog(Context context) {
        this.context = context;
        dialog = new Dialog(context, R.style.MyDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View dialog_view = View.inflate(context, R.layout.dialog_edit_company, null);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        dialog.setContentView(dialog_view, params);
        et_company = (EditText) dialog_view.findViewById(R.id.et_company);
        et_department = (EditText) dialog_view.findViewById(R.id.et_department);
        et_nickname = (EditText) dialog_view.findViewById(R.id.et_nickname);
        btn_modify = (Button) dialog_view.findViewById(R.id.btn_modify);
        btn_cacel = (Button) dialog_view.findViewById(R.id.btn_cacel);
        btn_modify.setOnClickListener(this);
        btn_cacel.setOnClickListener(this);
    }

    public void show(String company, String department, String nickname) {
        dissmiss();
        et_company.setText(company);
        et_department.setText(department);
        et_nickname.setText(nickname);
        dialog.show();
    }

    public void dissmiss() {
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_modify:
                String company = getCompany();
                String department = getDepartment();
                String devNick = getDevNickName();
                boolean isRight = jujleInfoRight(company, department, devNick);
                if (!isRight) {
                    return;
                }
                if (litener != null) {
                    litener.backCompanyInfo(company, department, devNick);
                }
                dissmiss();
                break;
            case R.id.btn_cacel:
                dissmiss();
                break;
        }
    }

    private boolean jujleInfoRight(String company, String department, String devNick) {
        if (TextUtils.isEmpty(company)) {
            showToastView(context.getString(R.string.input_succ_info));
            return false;
        }
        if (TextUtils.isEmpty(department)) {
            showToastView(context.getString(R.string.input_succ_info));
            return false;
        }
        if (TextUtils.isEmpty(devNick)) {
            showToastView(context.getString(R.string.input_succ_info));
            return false;
        }
        return true;
    }

    private void showToastView(String desc) {
        MyToastView.getInstance().Toast(context, desc);
    }

    private String getCompany() {
        return et_company.getText().toString().trim();
    }

    private String getDepartment() {
        return et_department.getText().toString().trim();
    }

    private String getDevNickName() {
        return et_nickname.getText().toString().trim();
    }

    public void setModifyCompanyLitener(ModifyCompanyLitener litener) {
        this.litener = litener;
    }

    ModifyCompanyLitener litener;

    public interface ModifyCompanyLitener {
        void backCompanyInfo(String company, String department, String nickName);
    }

}
