package com.ys.acsdev.view.dialog

import android.content.Context
import android.content.DialogInterface
import android.widget.TextView
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.customview.customView
import com.afollestad.materialdialogs.customview.getCustomView
import com.ys.acsdev.R
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import kotlinx.android.synthetic.main.dialog_check_healthcode.view.*
import java.util.concurrent.TimeUnit

// Created by kermitye on 2021/1/19 10:08
class CheckHealthCodeDialog(val context: Context) {

    var timeDisposable: Disposable? = null
    val dialog: MaterialDialog
    var listener: DialogInterface.OnCancelListener? = null

    init {
        dialog = MaterialDialog(context)
            .customView(R.layout.dialog_check_healthcode)
            .cancelOnTouchOutside(false)
            .apply {
                setOnCancelListener {
                    cancelTime()
                    listener?.onCancel(it)
                }
            }
    }


    fun show() {
        if (!dialog.isShowing)
            dialog.show()
        startTime()
    }

    fun hide() {
        cancelTime()
        if (!dialog.isShowing)
            return
        dialog.cancel()
    }


    fun isShowing() = dialog.isShowing

    fun startTime() {
        cancelTime()
        timeDisposable = Observable.intervalRange(0, 11, 0, 1, TimeUnit.SECONDS)
            .map { 10 - it }
            .observeOn(AndroidSchedulers.mainThread())
            .doOnNext {
                dialog.getCustomView().dch_tv_time.text = "$it"
            }.subscribe()
    }

    fun cancelTime() {
        if (timeDisposable != null && !timeDisposable!!.isDisposed) {
            timeDisposable?.dispose()
        }
        timeDisposable = null
    }



    fun setOnCancelListener(listener: DialogInterface.OnCancelListener) {
        this.listener = listener
    }

}