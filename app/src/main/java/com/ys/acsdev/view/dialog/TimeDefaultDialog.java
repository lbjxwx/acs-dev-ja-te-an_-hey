package com.ys.acsdev.view.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Build;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TimePicker;

import com.ys.acsdev.R;
import com.ys.acsdev.listener.TimeBackListener;
import com.ys.acsdev.util.SpUtils;

public class TimeDefaultDialog {

    private Context context;
    private Dialog dialog;
    TimeBackListener dialogClick;
    public Button btn_sure;
    Button btn_no;
    ListView lv_time_list;
    TimePicker timepicker;

    public TimeDefaultDialog(Context context) {
        this.context = context;
        dialog = new Dialog(context, R.style.MyDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View dialog_view = View.inflate(context, R.layout.dialog_time_chooice, null);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(SpUtils.getScreenwidth(), SpUtils.getScreenheight());
        dialog.setContentView(dialog_view, params);
        dialog.setCancelable(true); // true点击屏幕以外关闭dialog
        lv_time_list = (ListView) dialog_view.findViewById(R.id.lv_time_list);
        btn_sure = (Button) dialog_view.findViewById(R.id.btn_dialog_yes);
        btn_no = (Button) dialog_view.findViewById(R.id.btn_dialog_no);
        timepicker = (TimePicker) dialog_view.findViewById(R.id.timepicker);
        timepicker.setIs24HourView(true);   //设置时间显示为24小时
        timepicker.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener() {
            @Override
            public void onTimeChanged(TimePicker timePicker, int hours, int mins) {
                hourShow = hours;
                minShow = mins;
            }
        });

        btn_sure.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (dialogClick != null) {
                    dialogClick.backTimeListener(hourShow, minShow);
                }
                dissmiss();
            }
        });

        btn_no.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                dissmiss();
            }
        });
    }

    int hourShow;
    int minShow;

    public void show(String ok, String cacle, int hourShow, int minShow) {
        this.hourShow = hourShow;
        this.minShow = minShow;
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                timepicker.setHour(hourShow);
                timepicker.setMinute(minShow);
            }
            btn_sure.setText(ok);
            btn_no.setText(cacle);
            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void dissmiss() {
        try {
            if (dialog != null && dialog.isShowing()) {
                dialog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setOnDialogClickListener(TimeBackListener dc) {
        dialogClick = dc;
    }

}
