package com.ys.acsdev.view.dialog;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;

import com.ys.acsdev.R;
import com.ys.acsdev.bean.RedioEntity;
import com.ys.acsdev.listener.RadioChooiceListener;
import com.ys.acsdev.util.SpUtils;
import com.ys.acsdev.util.TimerUtil;
import com.ys.acsdev.view.MyToastView;

import java.util.List;

/***
 * 选择开始时间和结束时间
 */
public class IntervalDialog implements View.OnClickListener {

    private Context context;
    private Dialog dialog;
    public Button btn_dialog_submit;
    Button btn_dialog_cacel;
    Button btn_start;
    Button btn_end;

    public IntervalDialog(Context context) {
        if (context == null) {
            return;
        }
        this.context = context;
        dialog = new Dialog(context, R.style.MyDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View dialog_view = View.inflate(context, R.layout.dialog_interval, null);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(SpUtils.getScreenwidth(), SpUtils.getScreenheight());
        dialog.setContentView(dialog_view, params);
        dialog.setCancelable(true); // true点击屏幕以外关闭dialog
        btn_dialog_submit = (Button) dialog_view.findViewById(R.id.btn_dialog_submit);
        btn_dialog_cacel = (Button) dialog_view.findViewById(R.id.btn_dialog_cacel);

        btn_start = (Button) dialog_view.findViewById(R.id.btn_start);
        btn_end = (Button) dialog_view.findViewById(R.id.btn_end);

        btn_start.setOnClickListener(this);
        btn_end.setOnClickListener(this);


        btn_dialog_submit.setOnClickListener(this);
        btn_dialog_cacel.setOnClickListener(this);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_start:
                showStartTimeShowDialog();
                break;
            case R.id.btn_end:
                showEndTimeShowDialog();
                break;
            case R.id.btn_dialog_submit:
                jujleTimeBackListener();
                break;
            case R.id.btn_dialog_cacel:
                dissmiss();
                break;
        }
    }

    private void jujleTimeBackListener() {
        if (listener == null) {
            return;
        }
//        String startTime = btn_start.getText().toString();
//        String endTime = btn_end.getText().toString();
        if (startTime == null || startTime.length() < 2) {
            showToastView(context.getString(R.string.input_succ_info));
            return;
        }
        if (endTime == null || endTime.length() < 2) {
            showToastView(context.getString(R.string.input_succ_info));
            return;
        }
        if (startTime.equals(endTime)) {
            showToastView(context.getString(R.string.input_succ_info));
            return;
        }
        listener.backTime(startTime, endTime);
        dissmiss();
    }


    private void showToastView(String desc) {
        MyToastView.getInstance().Toast(context, desc);
    }

    String startTime = "06:00";
    String endTime = "18:00";

    public void show(String startTime, String endTime) {
        try {
            this.startTime = startTime;
            this.endTime = endTime;
            if (startTime != null && startTime.length() > 2) {
                btn_start.setText(startTime);
            }
            if (endTime != null && endTime.length() > 2) {
                btn_end.setText(endTime);
            }
            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void dissmiss() {
        try {
            if (dialog != null && dialog.isShowing()) {
                dialog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void showStartTimeShowDialog() {
        if (context == null) {
            return;
        }
        if (startTime == null || startTime.length() < 2) {
            startTime = "06:00";
        }
        int hour = 6;
        String hourTimeString = startTime.substring(0, startTime.indexOf(":"));
        try {
            hour = Integer.parseInt(hourTimeString);
        } catch (Exception e) {
            e.printStackTrace();
        }

        List<RedioEntity> listRadio = TimerUtil.getTimeStartInfo();
        int showPosition = hour;
        RadioListDialog radioListDialog = new RadioListDialog(context);
        radioListDialog.setRadioChooiceListener(new RadioChooiceListener() {
            @Override
            public void backChooiceInfo(RedioEntity redioEntity, int chooicePosition) {
                startTime = redioEntity.getRadioText();
                btn_start.setText(startTime);
            }
        });
        radioListDialog.show("Time", listRadio, showPosition, "");
    }


    private void showEndTimeShowDialog() {
        if (context == null) {
            return;
        }
        int hour = 18;
        if (endTime == null || endTime.length() < 2) {
            hour = 6;
        }
        String hourTimeString = endTime.substring(0, endTime.indexOf(":"));
        try {
            hour = Integer.parseInt(hourTimeString);
        } catch (Exception e) {
            e.printStackTrace();
        }
        List<RedioEntity> listRadio = TimerUtil.getTimeStartInfo();
        int showPosition = hour;
        RadioListDialog radioListDialog = new RadioListDialog(context);
        radioListDialog.setRadioChooiceListener(new RadioChooiceListener() {
            @Override
            public void backChooiceInfo(RedioEntity redioEntity, int chooicePosition) {
                endTime = redioEntity.getRadioText();
                btn_end.setText(endTime);
            }
        });
        radioListDialog.show("Time", listRadio, showPosition, "");
    }


    InviTimeBackListener listener;

    public void setOnInvitListener(InviTimeBackListener listener) {
        this.listener = listener;
    }

    public interface InviTimeBackListener {
        void backTime(String startTime, String endTime);
    }

}