package com.ys.acsdev.view.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;

import com.ys.acsdev.R;


public class PopBottomDialog implements View.OnClickListener {

    Activity activity;
    Dialog dialog;
    Button btn_ok, btn_refuse, btn_cacel;

    public PopBottomDialog(Activity activity) {
        DisplayMetrics metric = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(metric);
        int mScreenWidth = metric.widthPixels;
        this.activity = activity;
        dialog = new Dialog(activity, R.style.BottomDialog);
        LinearLayout root = (LinearLayout) LayoutInflater.from(activity).inflate(
                R.layout.popview_order, null);
        btn_ok = (Button) root.findViewById(R.id.btn_ok);
        btn_refuse = (Button) root.findViewById(R.id.btn_refuse);
        btn_cacel = (Button) root.findViewById(R.id.btn_cancel);
        btn_ok.setOnClickListener(this);
        btn_refuse.setOnClickListener(this);
        btn_cacel.setOnClickListener(this);

        dialog.setContentView(root);
        Window dialogWindow = dialog.getWindow();
        dialogWindow.setGravity(Gravity.BOTTOM);
        WindowManager.LayoutParams lp = dialogWindow.getAttributes(); // 获取对话框当前的参数值
        lp.x = 0; // 新位置X坐标
        lp.y = 0; // 新位置Y坐标
        lp.width = mScreenWidth;
        root.measure(0, 0);
        lp.height = root.getMeasuredHeight();
        lp.alpha = 9f; // 透明度
        dialogWindow.setAttributes(lp);
    }

    public void showPopDialog() {
        showPopDialog("确定", "取消", "撤销");
    }

    public void showFirsButton(boolean isShow) {
        if (isShow) {
            btn_ok.setVisibility(View.VISIBLE);
        } else {
            btn_ok.setVisibility(View.GONE);
        }
    }

    public void showSecondButton(boolean isShow) {
        if (isShow) {
            btn_refuse.setVisibility(View.VISIBLE);
        } else {
            btn_refuse.setVisibility(View.GONE);
        }
    }

    public void showPopDialog(String ok, String not, String cacel) {
        btn_ok.setText(ok);
        btn_refuse.setText(not);
        btn_cacel.setText(cacel);
        dialog.show();
    }

    PopBottomDialogListener listener;

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btn_ok) {
            listener.clickPosition(1);
        } else if (view.getId() == R.id.btn_refuse) {
            listener.clickPosition(2);
        } else if (view.getId() == R.id.btn_cancel) {
            listener.clickPosition(3);
        }
        dialog.dismiss();
    }

    public interface PopBottomDialogListener {
        void clickPosition(int position);
    }

    public void setOnBottomClickListener(PopBottomDialogListener listener) {
        this.listener = listener;
    }


}


