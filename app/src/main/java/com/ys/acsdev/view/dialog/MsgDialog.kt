package com.ys.acsdev.view.dialog

import android.view.View
import android.widget.TextView
import com.huiming.huimingstore.ext.setHtmlStr
import com.huiming.huimingstore.ext.setVisible
import com.kermitye.baselib.dialog.BaseMsgDialog
import com.kermitye.baselib.dialog.DialogViewHolder
import com.kermitye.baselib.dialog.ThemDialog
import com.kermitye.baselib.dialog.ViewConvertListener
import com.ys.acsdev.R

/**
 * Created by kermitye on 2018/10/19 14:39
 */
class MsgDialog (val manager: androidx.fragment.app.FragmentManager, var layoutId: Int = R.layout.layout_msg_dialog) {
    private var mDialog: ThemDialog
    private var mConfimListener: ((dialog: MsgDialog) -> Unit)? = null
    private var mCancelListener: ((dialog: MsgDialog) -> Unit)? = null
    private lateinit var mTitle: String
    private lateinit var mMsg: String
    private var mConfim: String = "确定"
    private var mCancel: String = "取消"
    private var mConfimVisible = true
    private var mCancelVisible = true
    private var mCancelTxtColor = 0

    init {
        mDialog = ThemDialog.init().setLayoutId(layoutId)
        mDialog.setWidth(450)
    }

    fun setConfimListener(listener: (dialog: MsgDialog) -> Unit) = apply { mConfimListener = listener }
    fun setConfimListener(tag: String, listener: (dialog: MsgDialog) -> Unit) = apply {
        mConfim = tag
        mConfimListener = listener
    }

    fun setCancelListener(listener: (dialog: MsgDialog) -> Unit) = apply { mCancelListener = listener }
    fun setCancelListener(tag: String, listener: ((dialog: MsgDialog) -> Unit)? = null) = apply {
        mCancel = tag
        mCancelListener = listener
    }

    fun setCancelTxtColor(colorResId: Int) = apply { mCancelTxtColor = colorResId }


    fun setConfimVisible(visible: Boolean) = apply { mConfimVisible = visible }
    fun setCancelVisible(visible: Boolean) = apply { mCancelVisible = visible }


    fun show(title: String, msg: String = "", htmlEnable: Boolean = false) {
        dismiss()
        mMsg = msg
        mTitle = title
        mDialog.setConvertListener(object : ViewConvertListener() {
            override fun convertView(holder: DialogViewHolder, dialog: BaseMsgDialog) {
                holder.getConvertView().let { v ->
                    var mTvConfim = v.findViewById<TextView>(R.id.mTvConfim)
                    var mTvCancel = v.findViewById<TextView>(R.id.mTvCancel)
                    var mTvMsgTitle = v.findViewById<TextView>(R.id.mTvMsgTitle)
                    var mTvMsgContent = v.findViewById<TextView>(R.id.mTvMsgContent)
                    var mLine = v.findViewById<View>(R.id.mLine)
                    if (mCancelTxtColor != 0) {
                        mTvCancel.setTextColor(dialog.resources.getColor(mCancelTxtColor))
                    }

                    mTvConfim.setVisible(mConfimVisible)
                    mTvCancel.setVisible(mCancelVisible)
                    if (!mConfimVisible || !mCancelVisible) {
                        mLine.setVisible(false)
                    }
                    mTvConfim.text = mConfim
                    mTvCancel.text = mCancel
                    mTvMsgTitle.text = mTitle
                    if (htmlEnable) {
                        mTvMsgContent.setHtmlStr(msg)
                    } else {
                        mTvMsgContent.text = mMsg
                    }

                    mTvMsgTitle.setVisible(!mTitle.isEmpty())
                    mTvMsgContent.setVisible(!mMsg.isEmpty())
                    mTvCancel.setOnClickListener {
                        mCancelListener?.let { it(this@MsgDialog) }
                        dismiss()
                    }
                    mTvConfim.setOnClickListener { mConfimListener?.let { it(this@MsgDialog) } }
                }
            }
        }).setMargin(30).show(manager)
    }

    fun dismiss() {
        if (isShowing())
            mDialog.dismiss()
    }

    fun isShowing(): Boolean = mDialog.isShowing()
}