package com.ys.acsdev.view.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.text.InputType;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ys.acsdev.R;
import com.ys.acsdev.util.KeyBoardUtil;

/***
 * 带一个输入框的
 */
public class EditTextDialog {
    private Activity context;
    private Dialog dialog;
    EditTextDialogListener dialogClick;
    public Button btn_del_all, btn_hiddle_test;
    public TextView dialog_title, btn_modify;
    public EditText et_username_edit;
    public boolean clickDissmiss = true;
    public boolean mIsPwd = false;
    TextView btn_cacel;


    public EditTextDialog(final Activity context) {
        this(context, false);
    }

    public EditTextDialog(final Activity context, boolean isPwd) {
        this.context = context;
        dialog = new Dialog(context, R.style.MyDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View dialog_view = View.inflate(context, R.layout.dialog_edit_commit, null);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        dialog.setContentView(dialog_view, params);
        btn_modify = (TextView) dialog_view.findViewById(R.id.btn_modify);
        et_username_edit = (EditText) dialog_view.findViewById(R.id.et_username_edit);
        dialog_title = (TextView) dialog_view.findViewById(R.id.tv_dialog_title);
        btn_del_all = (Button) dialog_view.findViewById(R.id.btn_del_all);
        btn_hiddle_test = (Button) dialog_view.findViewById(R.id.btn_hiddle_test);

        btn_cacel = (TextView) dialog_view.findViewById(R.id.btn_cacel);
        if (isPwd) {
//            et_username_edit.setInputType(EditorInfo.TYPE_NUMBER_VARIATION_PASSWORD);
            et_username_edit.setInputType(InputType.TYPE_NUMBER_VARIATION_PASSWORD | InputType.TYPE_CLASS_NUMBER);
//            et_username_edit.setTransformationMethod(PasswordTransformationMethod.getInstance());
        }

        btn_hiddle_test.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                dissmiss();
                if (dialogClick != null) {
                    dialogClick.hiddleTestClick();
                }
            }
        });

        btn_del_all.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                et_username_edit.setText("");
            }
        });

        btn_modify.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                if (dialogClick != null) {
                    String modifyName = et_username_edit.getText().toString().trim();
                    if (modifyName.contains(" ")) { //去掉空格
                        modifyName = modifyName.replace(" ", "");
                    }
//                    if (TextUtils.isEmpty(modifyName)) {
//                        MyToastView.getInstance().Toast(context, "请输入！");
//                        return;
//                    }
//                    if (modifyName.trim().length() > 50) {
//                        MyToastView.getInstance().Toast(context, "请输入小于50位字符");
//                        return;
//                    }
                    dialogClick.commit(modifyName);
                }
                if (clickDissmiss)
                    dissmiss();
            }
        });

        et_username_edit.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b) {
                    KeyBoardUtil.showKeyBord(view);
                } else {
                    KeyBoardUtil.hiddleBord(view);
                }
            }
        });

        btn_cacel.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                dissmiss();
            }
        });
    }

    public void show(String title, String content, String commit, String hit) {
        dialog_title.setText(title);
        et_username_edit.setText(content);
        et_username_edit.setHint(hit);
        btn_modify.setText(commit);
        et_username_edit.setSelection(et_username_edit.getText().toString().length());
        dialog.show();
    }

    public void dissmiss() {
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }
    }

    public void setOnDialogClickListener(EditTextDialogListener dc) {
        dialogClick = dc;
    }

    public interface EditTextDialogListener {
        void commit(String content);

        /***
         * 测试按钮功能，不具备任何功能，隐藏功能
         */
        void hiddleTestClick();
    }
}
