package com.ys.acsdev.attendance

import android.os.Bundle
import com.huiming.huimingstore.ext.setVisible
import com.hwangjr.rxbus.RxBus
import com.hwangjr.rxbus.annotation.Subscribe
import com.hwangjr.rxbus.thread.EventThread
import com.ys.acsdev.R
import com.ys.acsdev.attendance.bean.ClockInEntity
import com.ys.acsdev.attendance.dao.ClockInDao
import com.ys.acsdev.bean.SyncAttendEvent
import com.ys.acsdev.commond.AppInfo
import com.ys.acsdev.db.DataRepository
import com.ys.acsdev.ui.BaseActivity
import com.ys.acsdev.util.LogUtil
import com.ys.acsdev.util.NetWorkUtils
import com.ys.acsdev.util.SpUtils
import kotlinx.android.synthetic.main.activity_access_record.mIvBack
import kotlinx.android.synthetic.main.activity_clockin_record.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.toast
import java.util.ArrayList

/*
* Created by kermitye on 2019/5/29 15:26
* 打卡记录
*/
class ClockInRecordActivity : BaseActivity() {

    var mData = arrayListOf<ClockInEntity>()
    val mAdapter by lazy { ClockInRecordAdapter(this, mData) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_clockin_record)
        initView()
    }

    override fun onStart() {
        super.onStart()
        RxBus.get().register(this)
        initData()
    }


    override fun onDestroy() {
        RxBus.get().unregister(this)
        super.onDestroy()
    }

    private fun initView() {
        mIvBack.setOnClickListener { onBackPressed() }
        mRvAccess.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(this)
        mRvAccess.adapter = mAdapter
        mBtnClean.setOnClickListener { cleanFace() }
        mBtnSync.setOnClickListener { sync() }
        mBtnSync.setVisible(SpUtils.getWorkModel() == AppInfo.MODEL_NET)
    }

    private fun initData() {
        showWait("Loading...")
        doAsync {
            val data = ClockInDao.getAllClockIn()
            runOnUiThread {
                hideWait()
                LogUtil.attendance("=======clockinrecord: $data")
                data?.let {
                    mData.clear()
                    mData.addAll(it)
                    mData.reverse()
                    mAdapter.notifyDataSetChanged()
                }
            }
        }
    }

    private fun sync() {
        if (!NetWorkUtils.isNetworkConnected(this)) {
            showToast(getString(R.string.no_network))
            return
        }
        if (!AppInfo.isConnectServer) {
            showToast(getString(R.string.dev_not_online))
            return
        }
        showWait("")
        DataRepository.instance.syncAttendRecord()
    }

    private fun cleanFace() {
        doAsync {
            ClockInDao.delAlCockIn()
            runOnUiThread {
                initData()
            }
        }
    }

    @Subscribe(thread = EventThread.MAIN_THREAD)
    fun sycnCallBack(event: SyncAttendEvent) {
        showToast(getString(R.string.sync_complete) + "\n${getString(R.string.punch_success_jdq)}: ${event.success} \n${getString(R.string.failed)}:${event.failed}")
        initData()
    }

}