package com.ys.acsdev.attendance

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.ys.acsdev.R
import com.ys.acsdev.attendance.bean.ClockInEntity
import kotlinx.android.synthetic.main.item_clockin_record.view.*

/*
* Created by kermitye on 2019/5/29 10:11
*/

class ClockInRecordAdapter(
    var mContext: Context,
    var data: ArrayList<ClockInEntity> = arrayListOf()
) :
    RecyclerView.Adapter<ClockInRecordAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(mContext)
            .inflate(R.layout.item_clockin_record, parent, false)
        return ViewHolder(view)

    }

    override fun getItemCount(): Int = data.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val entity = data.get(position)
        holder.mItemRoot.setBackgroundResource(R.color.transparent)
        var type = ""
        type = if (entity.isEmpty) {
            mContext.getString(R.string.punch_no)
        } else if (entity.isSignIn) {
            mContext.getString(R.string.punch_in)
        } else {
            mContext.getString(R.string.punch_out)
        }
        holder.mTvClockName.text = entity.name
        holder.mTvClockDate.text = entity.time
        holder.mTvClockType.text = type
    }

    class ViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        var mItemRoot: LinearLayout
        var mTvClockName: TextView
        var mTvClockDate: TextView
        var mTvClockType: TextView
        var mTvClockState: TextView? = null

        init {
            mItemRoot =
                itemView.findViewById<View>(R.id.mItemRoot) as LinearLayout
            mTvClockName =
                itemView.findViewById<View>(R.id.mTvClockName) as TextView
            mTvClockDate =
                itemView.findViewById<View>(R.id.mTvClockDate) as TextView
            mTvClockType =
                itemView.findViewById<View>(R.id.mTvClockType) as TextView
        }
    }

}