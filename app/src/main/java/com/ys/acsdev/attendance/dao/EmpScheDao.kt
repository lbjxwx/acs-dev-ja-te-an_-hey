package com.ys.acsdev.attendance.dao

import com.ys.acsdev.attendance.bean.*
import org.litepal.LitePal
import org.litepal.extension.deleteAll

// Created by kermitye on 2019/11/1 17:26
// 员工排班
object EmpScheDao {

    fun getEmpAttendByEmpId(empId: String): List<EmpAttenInfoEntity> {
        return LitePal.where("empId=?", empId).find(EmpAttenInfoEntity::class.java)
    }


    fun insertAllEmpAttend(datas: List<EmpAttenInfoEntity>, needComp: Boolean = true) {
        delAllAttends()
        LitePal.saveAll(datas)
    }

    @JvmStatic
    fun delAllAttends() {
        LitePal.deleteAll<EmpAttenInfoEntity>()
    }

    fun getFixScheById(attendId: String): List<FixScheduleEntity> {
        var data = LitePal.where("attendId=?", attendId).find(FixScheduleEntity::class.java)
        data.forEach { entity ->
            entity.scheduleAttends =
                LitePal.where("parentId=?", entity.attendId).find(FixScheduleAttend::class.java)
            entity.scheduleAttends.forEach { attend ->
                attend.attends =
                    LitePal.where("parentId=?", attend.id.toString()).find(FixAttend::class.java)
            }
        }
        return data
    }


    fun insertAllFixSche(datas: List<FixScheduleEntity>) {
        delAllFixSche()
        datas.forEach { entity ->
            entity.scheduleAttends.forEach { attend ->
                attend.parentId = entity.attendId
                attend.save()
                attend.attends.forEach {
                    it.parentId = attend.id
                    it.save()
                }
            }
            entity.save()
        }
    }

    @JvmStatic
    fun delAllFixSche() {
        LitePal.deleteAll<FixAttend>()
        LitePal.deleteAll<FixScheduleAttend>()
        LitePal.deleteAll<FixScheduleEntity>()
    }


    //===========================排班制数据============================


    @JvmStatic
    fun getAllSche(): List<EmptyScheduling> {
        return LitePal.findAll(EmptyScheduling::class.java)
    }

    @JvmStatic
    fun insertAllSche(datas: List<EmptyScheduling>) {
        delAllSche()
        LitePal.saveAll(datas)
    }

    @JvmStatic
    fun delAllSche() {
        LitePal.deleteAll<EmptyScheduling>()
    }

}