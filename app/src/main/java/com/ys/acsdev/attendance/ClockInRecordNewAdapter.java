package com.ys.acsdev.attendance;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.ys.acsdev.R;
import com.ys.acsdev.attendance.bean.ClockInEntity;

import java.util.ArrayList;
import java.util.List;

public class ClockInRecordNewAdapter extends RecyclerView.Adapter<ClockInRecordNewAdapter.ViewHolder> {

    List<ClockInEntity> mFaceData = new ArrayList<>();
    Context context;

    public ClockInRecordNewAdapter(Context context, ArrayList<ClockInEntity> faceData) {
        this.context = context;
        this.mFaceData = faceData;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_clockin_record, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ClockInEntity entity = mFaceData.get(position);
        holder.mItemRoot.setBackgroundResource(R.color.transparent);
//            if (helper.adapterPosition % 2 == 0) {
//                view.mItemRoot.setBackgroundResource(R.color.transparent)
//            } else {
//                view.mItemRoot.setBackgroundResource(R.color.item_tran_bg)
//            }
        String type =  "";
        if (entity.isEmpty()) {
            type =  context.getString(R.string.punch_no);
        } else if (entity.isSignIn()) {
            type =  context.getString(R.string.punch_in);
        } else {
            type =  context.getString(R.string.punch_out);
        }
        holder.mTvClockName.setText(entity.getName());
        holder.mTvClockDate.setText(entity.getDate());
        holder.mTvClockType.setText(type);
    }

    @Override
    public int getItemCount() {
        return mFaceData.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        LinearLayout mItemRoot;
        TextView mTvClockName, mTvClockDate, mTvClockType, mTvClockState;

        public ViewHolder(View itemView) {
            super(itemView);
            mItemRoot = (LinearLayout) itemView.findViewById(R.id.mItemRoot);
            mTvClockName = (TextView) itemView.findViewById(R.id.mTvClockName);
            mTvClockDate = (TextView) itemView.findViewById(R.id.mTvClockDate);
            mTvClockType = (TextView) itemView.findViewById(R.id.mTvClockType);
        }
    }


}
