package com.ys.acsdev.attendance.dao

import com.ys.acsdev.attendance.bean.ClockInEntity
import org.litepal.LitePal
import org.litepal.extension.deleteAll
import org.litepal.extension.findAll

/***
 * 考勤打卡数据库管理类
 */
object ClockInDao {

    fun getAllClockIn(): List<ClockInEntity> {
        return LitePal.findAll<ClockInEntity>()
    }

    @JvmStatic
    fun delAlCockIn() {
        LitePal.deleteAll<ClockInEntity>()
    }

    fun deleteClock(entity: ClockInEntity) {
        entity.delete()
    }

}