package com.ys.acsdev.attendance.bean;

public class AttendBean {

    private String sort;
    private String workOffTime;
    private String workOnTime;
    private boolean inAcorss;
    private boolean outAcross;

    public AttendBean(String sort, String workOffTime, String workOnTime, boolean inAcorss, boolean outAcross) {
        this.sort = sort;
        this.workOffTime = workOffTime;
        this.workOnTime = workOnTime;
        this.inAcorss = inAcorss;
        this.outAcross = outAcross;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    public String getWorkOffTime() {
        return workOffTime;
    }

    public void setWorkOffTime(String workOffTime) {
        this.workOffTime = workOffTime;
    }

    public String getWorkOnTime() {
        return workOnTime;
    }

    public void setWorkOnTime(String workOnTime) {
        this.workOnTime = workOnTime;
    }

    public boolean isInAcorss() {
        return inAcorss;
    }

    public void setInAcorss(boolean inAcorss) {
        this.inAcorss = inAcorss;
    }

    public boolean isOutAcross() {
        return outAcross;
    }

    public void setOutAcross(boolean outAcross) {
        this.outAcross = outAcross;
    }
}
