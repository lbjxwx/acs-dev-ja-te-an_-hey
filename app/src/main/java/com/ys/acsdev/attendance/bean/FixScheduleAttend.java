package com.ys.acsdev.attendance.bean;

import org.litepal.crud.LitePalSupport;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kermitye on 2019-05-30 09:40
 */
public class FixScheduleAttend extends LitePalSupport {
    public String parentId;
    public int id;
    public List<FixAttend> attends = new ArrayList<>();
    public int weekDay;

    @Override
    public String toString() {
        return "FixScheduleAttend{" +
                "parentId='" + parentId + '\'' +
                ", attends=" + attends +
                ", weekDay=" + weekDay +
                '}';
    }
}
