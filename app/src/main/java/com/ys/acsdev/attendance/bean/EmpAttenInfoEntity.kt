package com.ys.acsdev.attendance.bean

import org.litepal.crud.LitePalSupport

data class EmpAttenInfoEntity(
    val attendId: String,
    val empId: String,
    val type: String
) : LitePalSupport()
