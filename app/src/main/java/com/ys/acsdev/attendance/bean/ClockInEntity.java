package com.ys.acsdev.attendance.bean;

import org.litepal.crud.LitePalSupport;

public class ClockInEntity extends LitePalSupport {

    boolean isSignIn = true;
    String sort = "-1";
    String date = "";
    String time = "";
    boolean isEmpty = true;
    String type = "3";
    String empId = "";
    String name = "";

    public ClockInEntity() {
    }

    public ClockInEntity(String sort, String type, boolean isEmpty) {
        this.sort = sort;
        this.isEmpty = isEmpty;
        this.type = type;
    }

    public ClockInEntity(String sort, String type, boolean isEmpty, String date) {
        this.sort = sort;
        this.date = date;
        this.isEmpty = isEmpty;
        this.type = type;
    }


    public ClockInEntity(String sort, String type, boolean isEmpty, String time, String date, String empId) {
        this.isSignIn = true;
        this.sort = sort;
        this.date = date;
        this.time = time;
        this.isEmpty = isEmpty;
        this.type = type;
        this.empId = empId;
        this.name = name;
    }

    public ClockInEntity(boolean isSignIn, String sort, String date, String time, boolean isEmpty, String type, String empId, String name) {
        this.isSignIn = isSignIn;
        this.sort = sort;
        this.date = date;
        this.time = time;
        this.isEmpty = isEmpty;
        this.type = type;
        this.empId = empId;
        this.name = name;
    }

    public boolean isSignIn() {
        return isSignIn;
    }

    public void setSignIn(boolean signIn) {
        isSignIn = signIn;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public boolean isEmpty() {
        return isEmpty;
    }

    public void setEmpty(boolean empty) {
        isEmpty = empty;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getEmpId() {
        return empId;
    }

    public void setEmpId(String empId) {
        this.empId = empId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "ClockInEntity{" +
                "isSignIn=" + isSignIn +
                ", sort='" + sort + '\'' +
                ", date='" + date + '\'' +
                ", time='" + time + '\'' +
                ", isEmpty=" + isEmpty +
                ", type='" + type + '\'' +
                ", empId='" + empId + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
