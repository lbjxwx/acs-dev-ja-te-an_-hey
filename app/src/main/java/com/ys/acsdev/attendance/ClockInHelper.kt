package com.ys.acsdev.attendance

import com.ys.acsdev.attendance.bean.AttendBean
import com.ys.acsdev.attendance.bean.ClockInEntity
import com.ys.acsdev.attendance.dao.EmpScheDao
import com.ys.acsdev.util.LogUtil
import com.ys.acsdev.util.SimpleDateUtil
import java.util.*

/* 
* Created by kermitye on 2019/5/17 15:00
* 打卡工具类
*/

object ClockInHelper {

    @JvmStatic
    fun getSignParm(
        clockInEntity: ClockInEntity
    ): Map<String, String> {
        var timeStrKey: String
        when (clockInEntity.sort) {
            "1" -> timeStrKey = if (clockInEntity.isSignIn) "signInOne" else "signOutOne"
            "2" -> timeStrKey = if (clockInEntity.isSignIn) "signInTwo" else "signOutTwo"
            "3" -> timeStrKey = if (clockInEntity.isSignIn) "signInThree" else "signOutThree"
            "-1" -> {
                timeStrKey = "signTime"
                var signType = "inGroup"
                if (clockInEntity.type == "3") {
                    signType = "notInGroup"
                }
                return mapOf(
                    "empId" to clockInEntity.empId,
                    "signDate" to clockInEntity.date,
                    timeStrKey to clockInEntity.time,
                    "signType" to signType
                )
            }
            else -> timeStrKey = "signTime"
        }

        return mapOf(
            "empId" to clockInEntity.empId,
            "signDate" to clockInEntity.date,
            timeStrKey to clockInEntity.time
        )
    }


    @JvmStatic
    fun getClockInEntity(empId: String, time: Long): ClockInEntity? {
        var entity: ClockInEntity?
        var cal = Calendar.getInstance()
        cal.timeInMillis = time

        var year = cal.get(Calendar.YEAR)
        var month = cal.get(Calendar.MONTH) + 1
        var day = cal.get(Calendar.DAY_OF_MONTH)
        var hour = cal.get(Calendar.HOUR_OF_DAY)
        var minute = cal.get(Calendar.MINUTE)
        var second = cal.get(Calendar.SECOND)
        val weekDay = SimpleDateUtil.getCurrentWeekDay()
        val lastWeekDay = SimpleDateUtil.getLastWeekDay()
        val nextWeekDay = SimpleDateUtil.getNextWeekDay()
        val lastDate = SimpleDateUtil.getLastDateSingle()
        val nextDate = SimpleDateUtil.getNextDateSingle()
        val curDate = SimpleDateUtil.getDateSingle()

        LogUtil.e("======clockin lastDate: ${lastDate}")
        LogUtil.e("======clockin nextDate: ${nextDate}")
        LogUtil.e("======clockin curDate: ${curDate}")

        val attendInfoList = EmpScheDao.getEmpAttendByEmpId(empId)
        LogUtil.e("======clockin: attendInfo $attendInfoList")

        val timeStrValue = SimpleDateUtil.formatTaskTimeShow(time)


        if (attendInfoList.isNullOrEmpty())
            return ClockInEntity(
                "-1",
                "3",
                true,
                timeStrValue,
                SimpleDateUtil.getDateSingle(),
                empId
            )

        when (attendInfoList.first().type) {
            "1" -> {    //固定班次 只取一条
                var scheduleAttends =
                    EmpScheDao.getFixScheById(attendInfoList.first().attendId)
                        .firstOrNull()?.scheduleAttends
                LogUtil.e("======clockin: fixSchedule $scheduleAttends")
                if (scheduleAttends.isNullOrEmpty())
                    return ClockInEntity(
                        "-1",
                        attendInfoList.first().type,
                        true,
                        timeStrValue,
                        SimpleDateUtil.getDateSingle(),
                        empId
                    )

                //今日班次
                var attends =
                    scheduleAttends.filter { it.weekDay == weekDay }.firstOrNull()
                        ?.attends?.map {
                        AttendBean(
                            it.sort,
                            "${if (it.outAcross == "1") nextDate else curDate} ${it.workOffTime}:00",
                            "${if (it.inAcorss == "1") nextDate else curDate} ${it.workOnTime}:00",
                            it.inAcorss == "1",
                            it.outAcross == "1"
                        )
                    }

                //昨日班次
                var lastAttends =
                    scheduleAttends.filter { it.weekDay == lastWeekDay }.firstOrNull()
                        ?.attends?.map {
                        AttendBean(
                            it.sort,
                            "${if (it.outAcross == "1") curDate else lastDate} ${it.workOffTime}:00",
                            "${if (it.inAcorss == "1") curDate else lastDate} ${it.workOnTime}:00",
                            it.inAcorss == "1",
                            it.outAcross == "1"
                        )
                    }

                //明日班次
                var nextAttends = scheduleAttends.filter { it.weekDay == nextWeekDay }.firstOrNull()
                    ?.attends?.map {
                    AttendBean(
                        it.sort,
                        "${if (it.outAcross == "1") nextDate else nextDate} ${it.workOffTime}:00",
                        "${if (it.inAcorss == "1") nextDate else nextDate} ${it.workOnTime}:00",
                        it.inAcorss == "1",
                        it.outAcross == "1"
                    )
                }
                entity =
                    handlerClockIn(
                        attends,
                        nextAttends,
                        lastAttends,
                        time,
                        attendInfoList.first().type
                    )
            }
            "2" -> {    //排班制
                var allAttendId = attendInfoList.map { it.attendId }
                var allSche = EmpScheDao.getAllSche()
                    .filter { allAttendId.contains(it.attendId) }

                LogUtil.e("======clockin: allSche $allSche")
                //今日排班
                var currentSche = allSche.filter { it.dateStr == curDate }
                    .sortedBy { it.sort.toInt() }.map {
                        AttendBean(
                            it.sort,
                            it.workOffTime,
                            it.workOnTime,
                            it.inAcorss == "1",
                            it.outAcross == "1"
                        )
                    }
                //明日排班
                var nextSche = allSche.filter { it.dateStr == nextDate }
                    .sortedBy { it.sort.toInt() }.map {
                        AttendBean(
                            it.sort,
                            it.workOffTime,
                            it.workOnTime,
                            it.inAcorss == "1",
                            it.outAcross == "1"
                        )
                    }
                //昨日排班
                var lastSche = allSche.filter { it.dateStr == lastDate }
                    .sortedBy { it.sort.toInt() }.map {
                        AttendBean(
                            it.sort,
                            it.workOffTime,
                            it.workOnTime,
                            it.inAcorss == "1",
                            it.outAcross == "1"
                        )
                    }

                LogUtil.e("======clockin: cacheSche $currentSche")
                if (allSche.isNullOrEmpty()) {
                    entity = ClockInEntity(
                        "-1",
                        attendInfoList.first().type,
                        true,
                        SimpleDateUtil.getDateSingle(),
                        timeStrValue,
                        empId
                    )
                } else {
                    entity =
                        handlerClockIn(
                            currentSche,
                            nextSche,
                            lastSche,
                            time,
                            attendInfoList.first().type
                        )
                }
            }
            else -> {    //无考勤
                entity =
                    ClockInEntity(
                        "-1",
                        attendInfoList.first().type,
                        true,
                        timeStrValue,
                        SimpleDateUtil.getDateSingle(),
                        empId
                    )
            }
        }
        entity?.empId = empId
        entity?.type = attendInfoList.first().type
//        var timeStrValue = SimpleDateUtil.formatTaskTimeShow(time)
        entity?.time = timeStrValue

        return entity
    }


    fun handlerClockIn(
        curAttends: List<AttendBean>?,
        nextAttends: List<AttendBean>?,
        lastAttends: List<AttendBean>?,
        time: Long,
        type: String
    ): ClockInEntity? {
        var entity: ClockInEntity? = null

        //先获取是否是昨日的班次
        if (!lastAttends.isNullOrEmpty()) {
            //筛选昨天是否有跨日班次
            var temp = lastAttends.filter { it.isInAcorss() || it.isOutAcross() }
            if (!temp.isNullOrEmpty()) {
                entity =
                    calcClockIn(
                        lastAttends,
                        curAttends,
                        null,
                        time,
                        true
                    )
                entity?.date = SimpleDateUtil.getLastDateSingle()
            }
        }
        LogUtil.e("======clockin: 是否是昨天 $entity")
        if (entity == null || entity.isEmpty) {
//            LogUtil.e("======clockin: attend $attends")
            if (curAttends.isNullOrEmpty()) {
                entity = ClockInEntity(
                    "-1",
                    type,
                    true,
                    SimpleDateUtil.getDateSingle()
                )
            } else {
                entity =
                    calcClockIn(
                        curAttends,
                        nextAttends,
                        lastAttends,
                        time
                    )
                if (entity == null) {
                    entity = ClockInEntity(
                        "-1",
                        type,
                        true
                    )
                }
                entity.date = SimpleDateUtil.getDateSingle()
                entity.type = type
                LogUtil.e("======clockin: 获取成功 $entity")
            }
        }
        return entity
    }


    fun calcClockIn(
        attends: List<AttendBean>,
        nextAttends: List<AttendBean>?,
        lastAttends: List<AttendBean>?,
        time: Long,
        isLast: Boolean = false
    ): ClockInEntity? {
        LogUtil.e("======clockin attends: $attends")
        LogUtil.e("======clockin nextAttends: $nextAttends")
        LogUtil.e("======clockin lastAttends: $lastAttends")

        var entity: ClockInEntity?
        if (attends.isNullOrEmpty()) {
            return null
        } else {
            entity = ClockInEntity()
            run outside@{
                attends.forEachIndexed { index, attend ->
                    if (attend.workOnTime.isNullOrEmpty() || attend.workOffTime.isNullOrEmpty()) {
//                        entity.isEmpty = true
//                        entity.sort = "-1"
                        return@outside
                    }

                    //上班打卡时间戳
                    var signInTime =
                        SimpleDateUtil.StringToLongTime(attend.workOnTime)
                    //下班打卡时间戳
                    var signOutTime =
                        SimpleDateUtil.StringToLongTime(attend.workOffTime)

                    var signInStart: Long   //上班开始时间
                    var signInEnd: Long     //上班结束时间
                    //上班打卡有效区间
                    var signInRange: LongRange

                    var signOutStart: Long  //下班开始时间
                    var signOutEnd: Long    //下班结束时间
                    //下班打卡有效区间
                    var signOutRange: LongRange


                    if (index == 0) {
                        //获取上班打卡区间
                        signInStart = SimpleDateUtil.getStartTime().time
                        if (isLast) {
                            signInStart = SimpleDateUtil.getLastStartTime().time
                        }
                        if (lastAttends != null) {
                            //获取昨天最后一班次
                            var lastEndAttend = lastAttends.lastOrNull()
                            //昨天最后一班跨天
                            if (lastEndAttend != null && lastEndAttend.isOutAcross()) {
                                var lastSignOutTime =
                                    SimpleDateUtil.StringToLongTime(lastEndAttend.workOffTime)
                                signInStart =
                                    (signInTime - lastSignOutTime) / 2 + lastSignOutTime + 1
                            }
                        }
                        signInEnd = (signOutTime - signInTime) / 2 + signInTime

                        //获取下班打卡区间
                        signOutStart = signInEnd + 1
                        signOutEnd = SimpleDateUtil.getnowEndTime().time
                        if (attends.size == 1) {
                            if (attend.isOutAcross()) {
                                signOutEnd = SimpleDateUtil.getNextEndTime().time
                            }
                            if (nextAttends != null) {
                                //获取明天第一班次
                                var tomorrow = nextAttends.firstOrNull()
                                if (tomorrow != null) {
                                    var nextSignInTime =
                                        SimpleDateUtil.StringToLongTime(tomorrow.workOnTime)
                                    signOutEnd = (nextSignInTime - signOutTime) / 2 + signOutTime
                                }
                            }
                        } else if (attends.size > 1) {
                            var nextEmpty = attends[index + 1]
                            var nextSignInTime =
                                SimpleDateUtil.StringToLongTime(nextEmpty.workOnTime)
                            signOutEnd = (nextSignInTime - signOutTime) / 2 + signOutTime
                        }

                    } else if (index == attends.size - 1) {
//                        signInStart = SimpleDateUtil.getStartTime().time
                        var lastEmpty = attends[index - 1]
                        var lastSignOutTime =
                            SimpleDateUtil.StringToLongTime(lastEmpty.workOffTime)
                        signInStart = (signInTime - lastSignOutTime) / 2 + lastSignOutTime + 1
                        signInEnd = (signOutTime - signInTime) / 2 + signInTime

                        signOutEnd = SimpleDateUtil.getnowEndTime().time
                        if (attend.isOutAcross()) {
                            signOutEnd = SimpleDateUtil.getNextEndTime().time
                        }
                        if (nextAttends != null && attend.isOutAcross()) {
                            //获取明天第一班次
                            var tomorrow = nextAttends.firstOrNull()
                            if (tomorrow != null) {
                                var nextSignInTime =
                                    SimpleDateUtil.StringToLongTime(tomorrow.workOnTime)
                                signOutEnd = (nextSignInTime - signOutTime) / 2 + signOutTime
                            }
                        }
                        signOutStart = signInEnd + 1

                    } else {
                        var lastEmpty = attends[index + 1]
                        var lastSignOutTime =
                            SimpleDateUtil.StringToLongTime(lastEmpty.workOffTime)
                        signInStart = (signInTime - lastSignOutTime) / 2 + lastSignOutTime + 1
                        signInEnd = (signOutTime - signInTime) / 2 + signInTime

                        var nextEmpty = attends[index + 1]
                        var nextSignInTime =
                            SimpleDateUtil.StringToLongTime(nextEmpty.workOnTime)
                        signOutEnd = (nextSignInTime - signOutTime) / 2 + signOutTime
                        signOutStart = signInEnd + 1
                    }

                    signInRange = signInStart..signInEnd
                    signOutRange = signOutStart..signOutEnd

                    if (signInRange.contains(time)) {
                        entity?.isEmpty = false
                        entity?.sort = attend.sort
                        entity?.isSignIn = true
                        return@outside
                    } else if (signOutRange.contains(time)) {
                        entity?.isEmpty = false
                        entity?.sort = attend.sort
                        entity?.isSignIn = false
                        return@outside
                    }
                }
            }
        }
        return entity
    }
}