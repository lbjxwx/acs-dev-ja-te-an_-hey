package com.ys.acsdev.attendance.bean

import org.litepal.crud.LitePalSupport


data class EmptyScheduling(
    val dateStr: String,
    val sort: String,
    val workOffTime: String,
    val workOnTime: String,
    val attendId: String,
    val inAcorss: String,
    val outAcross: String
) : LitePalSupport()