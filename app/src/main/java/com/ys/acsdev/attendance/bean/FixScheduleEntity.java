package com.ys.acsdev.attendance.bean;

import org.litepal.crud.LitePalSupport;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kermitye on 2019-05-30 09:39
 */
public class FixScheduleEntity extends LitePalSupport {
    public String attendId;
    public List<FixScheduleAttend> scheduleAttends = new ArrayList<>();


    @Override
    public String toString() {
        return "FixScheduleEntity{" +
                "attendId='" + attendId + '\'' +
                ", scheduleAttends=" + scheduleAttends +
                '}';
    }
}
