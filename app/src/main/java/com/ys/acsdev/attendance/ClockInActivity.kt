package com.ys.acsdev.attendance

import android.os.Bundle
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.list.listItems
import com.bigkoo.pickerview.builder.TimePickerBuilder
import com.huiming.common.ext.schedulers
import com.yisheng.facerecog.net.HttpObserver
import com.ys.acsdev.R
import com.ys.acsdev.api.ApiHelper
import com.ys.acsdev.bean.PersonBean
import com.ys.acsdev.db.dao.PersonDao
import com.ys.acsdev.manager.TtsManager
import com.ys.acsdev.ui.BaseActivity
import com.ys.acsdev.util.LogUtil
import com.ys.acsdev.util.SimpleDateUtil
import kotlinx.android.synthetic.main.activity_clockin.*
import org.jetbrains.anko.toast


class ClockInActivity : BaseActivity() {

    var mClockInTime: Long = -1L

    val mEmpDialog by lazy { MaterialDialog(this).title(text = "Chooice Employees") }

    val mTimePicker by lazy {
        TimePickerBuilder(this@ClockInActivity) { date, v ->
            mClockInTime = date.time
            var time = SimpleDateUtil.formatTaskTimeShow(mClockInTime)
//            toast("=======selected time: $time")
            mTvTime.text = time
        }.setType(booleanArrayOf(true, true, true, true, true, false)).build()
    }

    var mSelectedEmp: PersonBean? = null

    var mData = arrayListOf<PersonBean>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_clockin)
        initView()
        initData()
    }

    fun initView() {
        mIvBack.setOnClickListener { onBackPressed() }
        mBtnClockIn.setOnClickListener { clockIn() }
        mLlTime.setOnClickListener {
            mTimePicker.show()
        }
        mLlEmp.setOnClickListener {
            //mEmpDialog.show("选择员工", -1)
            mEmpDialog.show {
                listItems(items = mData.map { it.name }) { dialog, index, text ->
                    val item = mData[index]
                    updateNameText(item.name)
                    mSelectedEmp = item
                }
            }
        }
    }

    fun initData() {
        val persons = PersonDao.getAllPerson()
        mData.clear()
        mData.addAll(persons)
    }

    fun updateNameText(text: String) {
        mEtCardNum.text = text
    }

    fun clockIn() {
        if (mSelectedEmp == null) {
            showToast(getString(R.string.chooice_emploer))
            return
        }
        if (mClockInTime == -1L) {
            showToast(getString(R.string.chooice_card_time))
            return
        }
        showWait(getString(R.string.please_wait))
        insertWorkerToDev(mSelectedEmp!!.name, mSelectedEmp!!.perId)
    }

    /**
     * 打卡
     * empId: 员工ID
     */
    fun insertWorkerToDev(name: String, empId: String) {
        val entity = ClockInHelper.getClockInEntity(empId, mClockInTime)
        if (entity == null) {
            hideWait()
            LogUtil.attendance("====clockIn: 没有此人排班")
            showToast(getString(R.string.no_play_this_man))
            return
        }
        LogUtil.attendance("====clockIn:${entity.toString()}")
        var speakText =
            name + getSpeechText(SimpleDateUtil.getCurrentHourMinSecondByTime(mClockInTime))
        ApiHelper.freeSignIn(ClockInHelper.getSignParm(entity), this).schedulers()
            .subscribe(object : HttpObserver<Any?>() {
                override fun onSuccess(o: Any?) {
                    LogUtil.attendance("提交同行记录success==")
                    hideWait()
                    showToast(speakText + getString(R.string.punch_success))
                    TtsManager.getInstance()
                        .speakText(speakText + getString(R.string.punch_success))
//                        ClockInRecordEntity(name, entity.time, "无考勤", true).save()
                }

                override fun onFailed(code: Int, msg: String) {
                    LogUtil.attendance("提交同行记录failed==" + msg)
                    hideWait()
                    showToast(getString(R.string.punch_error))
                    TtsManager.getInstance()
                        .speakText(speakText + getString(R.string.punch_error))
//                        ClockInRecordEntity(name, entity.time, "无考勤", false).save()
                }
            })
    }

    fun getSpeechText(currentTime: Long): String? {
        if (60000 < currentTime && currentTime < 110000) { //早上好
            return getString(R.string.good_moring)
        } else if (110000 < currentTime && currentTime < 130000) { //中午好
            return getString(R.string.good_noon)
        } else if (130000 < currentTime && currentTime < 180000) { //下午好
            return getString(R.string.good_afternoon)
        } else if (180000 < currentTime && currentTime < 230000) { //晚上好
            return getString(R.string.good_night)
        } else if (230000 < currentTime && currentTime < 235959 || 1 < currentTime && currentTime < 60000) { //深夜
            return getString(R.string.too_late)
        }
        return getString(R.string.hello_nice)
    }

}