package com.ys.acsdev.attendance.bean;

import org.litepal.crud.LitePalSupport;

/**
 * Created by kermitye on 2019-05-30 09:40
 */
public class FixAttend extends LitePalSupport {
    int parentId;
    String sort;
    String workOffTime;
    String workOnTime;
    String inAcorss;
    String outAcross;

    public int getParentId() {
        return parentId;
    }

    public void setParentId(int parentId) {
        this.parentId = parentId;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    public String getWorkOffTime() {
        return workOffTime;
    }

    public void setWorkOffTime(String workOffTime) {
        this.workOffTime = workOffTime;
    }

    public String getWorkOnTime() {
        return workOnTime;
    }

    public void setWorkOnTime(String workOnTime) {
        this.workOnTime = workOnTime;
    }

    public String getInAcorss() {
        return inAcorss;
    }

    public void setInAcorss(String inAcorss) {
        this.inAcorss = inAcorss;
    }

    public String getOutAcross() {
        return outAcross;
    }

    public void setOutAcross(String outAcross) {
        this.outAcross = outAcross;
    }

    @Override
    public String toString() {
        return "FixAttend{" +
                "parentId=" + parentId +
                ", sort='" + sort + '\'' +
                ", workOffTime='" + workOffTime + '\'' +
                ", workOnTime='" + workOnTime + '\'' +
                ", inAcorss='" + inAcorss + '\'' +
                ", outAcross='" + outAcross + '\'' +
                '}';
    }
}
