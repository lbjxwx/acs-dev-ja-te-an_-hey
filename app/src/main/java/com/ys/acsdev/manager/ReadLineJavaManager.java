//package com.ys.acsdev.manager;
//
//import com.ys.acsdev.listener.HumanInductionListener;
//import com.ys.acsdev.util.GpioUtils;
//import com.ys.acsdev.util.LogUtil;
//import com.ys.acsdev.util.SpUtils;
//
//import io.reactivex.disposables.Disposable;
//
//public class ReadLineJavaManager {
//
//    private static final String TAG = "ReadLineJavaManager";
//
//    private Disposable mDisposable = null;
//    private HumanInductionListener mStatusChangeListener = null;
//    boolean mHasPerson = false;
//    //    int gpIO = 195
//    int gpIO = 162;
//    //    int mChangeCount = 0
//
//    public ReadLineJavaManager() {
//
//    }
//
//    public void init() {
//        LogUtil.e(TAG, "======init=========");
//        GpioUtils.upgradeRootPermissionForExport();
//        GpioUtils.upgradeRootPermissionForGpio(gpIO);
//        boolean isTrue = checkIO();
//        LogUtil.e(TAG, "======init=========" + isTrue);
//        if (mStatusChangeListener == null) {
//            return;
//        }
//        mStatusChangeListener.initStatues(isTrue, "");
//    }
//
//
//    private String getGpioValue(int gpio) {
//        return GpioUtils.getGpioValue(gpio);
//    }
//
//    private void stopReadLine() {
//        if (mDisposable == null) {
//            return;
//        }
//        if (mDisposable.isDisposed()) {
//            mDisposable.dispose();
//            mDisposable = null;
//        }
//    }
//
//
//    private boolean checkIO() {
//        if (GpioUtils.exportGpio(gpIO)) {
//            GpioUtils.upgradeRootPermissionForGpio(gpIO);
//            String status = GpioUtils.getGpioDirection(gpIO);
//            if (status == null || status.length() < 2) {
//                return false;
//            } else {
//                return true;
//            }
//        }
//        return false;
//    }
//
//
//    boolean isTrue = false;
//
//    public void startReadLine() {
//        if (!SpUtils.getReadLineStatues()) {
//            return;
//        }
//        LogUtil.e(TAG, "======startReadLine=========");
//        isTrue = true;
//        stopReadLine();
//        while (isTrue) {
//            try {
//                Thread.sleep(500);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//            String value = getGpioValue(gpIO);
//            LogUtil.e(TAG, "========getGpioValue:" + value);
//            if (mHasPerson && value == "0") {
//                mHasPerson = false;
//                backReadLineHasHuman(false);
//            } else if (!mHasPerson && value == "1") {
//                mHasPerson = true;
//                backReadLineHasHuman(true);
//            }
//        }
//    }
//
//    private void backReadLineHasHuman(boolean b) {
//        if (mStatusChangeListener == null) {
//            return;
//        }
//        mStatusChangeListener.onHasHuman(b);
//    }
//
//
//    public void destory() {
//        mStatusChangeListener = null;
//        stopReadLine();
//        isTrue = false;
//        mHasPerson = false;
//    }
//
//    public void setStatusChangeListener(HumanInductionListener listener) {
//        this.mStatusChangeListener = listener;
//    }
//
//}
