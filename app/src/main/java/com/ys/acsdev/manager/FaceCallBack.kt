package com.ys.acsdev.manager

import mcv.facepass.types.FacePassFace


// Created by kermitye on 2019/7/31 15:47
interface FaceCallBack {
    fun faceResult(detectResult: Array<FacePassFace>)
    fun onInited(isSuccess: Boolean)
}