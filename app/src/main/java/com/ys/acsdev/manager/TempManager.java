package com.ys.acsdev.manager;

import android.graphics.Point;
import android.util.Log;

import com.ys.acsdev.MyApp;
import com.ys.acsdev.api.AppConfig;
import com.ys.acsdev.setting.entity.TempModelEntity;
import com.ys.acsdev.util.SpUtils;
import com.ys.temperaturelib.device.IMatrixThermometer;
import com.ys.temperaturelib.device.IPointThermometer;
import com.ys.temperaturelib.device.MeasureDevice;
import com.ys.temperaturelib.device.MeasureResult;
import com.ys.temperaturelib.device.i2cmatrix.IIC_HPTA_32;
import com.ys.temperaturelib.device.i2cmatrix.IIC_MLX_90621_BAA;
import com.ys.temperaturelib.device.i2cmatrix.IIC_MLX_90621_BAB;
import com.ys.temperaturelib.device.i2cmatrix.IIC_MLX_90621_BAB_HY;
import com.ys.temperaturelib.device.i2cmatrix.IIC_MLX_90621_BAB_XLZD;
import com.ys.temperaturelib.device.i2cmatrix.IIC_MLX_90641;
import com.ys.temperaturelib.device.i2cmatrix.IIC_OTPA_16;
import com.ys.temperaturelib.device.i2cmatrix.IIC_OTPA_16_HEAD;
import com.ys.temperaturelib.device.i2cpoint.IIC_MLX_90614;
import com.ys.temperaturelib.device.serialport.ProductImp;
import com.ys.temperaturelib.device.serialport.SP_HM5_0_STM_YS;
import com.ys.temperaturelib.device.serialport.SP_HM_32X32;
import com.ys.temperaturelib.device.serialport.SP_HM_MT031;
import com.ys.temperaturelib.device.serialport.SP_HM_V11;
import com.ys.temperaturelib.device.serialport.SP_KM_D801;
import com.ys.temperaturelib.device.serialport.SP_MLX_90621_BAA;
import com.ys.temperaturelib.device.serialport.SP_MLX_90621_BAA_SDYS;
import com.ys.temperaturelib.device.serialport.SP_MLX_90621_BAA_ZM;
import com.ys.temperaturelib.device.serialport.SP_MLX_90621_HY;
import com.ys.temperaturelib.device.serialport.SP_MLX_90621_HY1;
import com.ys.temperaturelib.device.serialport.SP_MLX_90621_JSF;
import com.ys.temperaturelib.device.serialport.SP_MLX_90621_RR;
import com.ys.temperaturelib.device.serialport.SP_MLX_90621_STM;
import com.ys.temperaturelib.device.serialport.SP_MLX_90621_TIR_SN;
import com.ys.temperaturelib.device.serialport.SP_MLX_90641_ESF;
import com.ys.temperaturelib.device.serialport.SP_MLX_90641_STM;
import com.ys.temperaturelib.device.serialport.SP_MLX_90641_YNC;
import com.ys.temperaturelib.device.serialport.SP_OMR_D6T_4X4;
import com.ys.temperaturelib.device.serialport.SP_OTPA_16;
import com.ys.temperaturelib.device.serialport.SP_XRJ_S60;
import com.ys.temperaturelib.device.serialport.USB_OTPA_MC3216;
import com.ys.temperaturelib.temperature.TemperatureEntity;

public class TempManager {

    private TempManager() {
    }

    private static class SingletonHolder {
        public static final TempManager INSTANCE = new TempManager();
    }

    public static TempManager getInstance() {
        return SingletonHolder.INSTANCE;
    }

    public static final int CALIBRATION_COMPLE = -101;

    public MeasureDevice mDevice;
    public volatile float mTemperatue = -99f;
    private TempCallBack mTempCallBack;
    public int mRectDistance;
    private TemperatureEntity tempEntity;
    public Point headPoint;

    public void init(int type) {
        if (mDevice != null)
            destory();
        Log.e("IMLX90621", "=======温度模块初始化：" + type);
        String dev = SpUtils.getTempDevPath();
        int rare = SpUtils.getmTempRare();
        switch (type) {
            case TempModelEntity.CLOSE:
                destory();
                break;
            case TempModelEntity.IIC_MLX_90621_BAB:
                initI2CMatrix(new IIC_MLX_90621_BAB());
                break;
            case TempModelEntity.SP_MLX_90621_RR:
                //SP_MLX_90621_RR
                initSerialMatrix(new SP_MLX_90621_RR(dev, rare));
                break;
//            case TempModelEntity.mhy_10724:  //铭鸿宇客户
//                initSerialMHY(dev, rare);
//                break;
            case TempModelEntity.SP_MLX_90621_STM:
                //SP_MLX_90621_STM
                initSerialMatrix(new SP_MLX_90621_STM(dev, rare));
                break;
            case TempModelEntity.IIC_MLX_90640_0AA1547511:
                //IIC_MLX_90640_0AA1547511
//                initI2CMatrix(new IIC_MLX_90640_0AA1547511());
                break;
            case TempModelEntity.IIC_MLX_90614:
                //IIC_MLX_90614
                initI2CPoint(new IIC_MLX_90614());
                break;
            case TempModelEntity.SP_XRJ_S60:
                //SP_XRJ_S60
                initSerialMatrix(new SP_XRJ_S60(dev, rare));
                break;
            case TempModelEntity.SP_HM_32X32:
                //SP_HM_32X32
                initSerialMatrix(new SP_HM_32X32(dev, rare));
                break;
            case TempModelEntity.IIC_MLX_90641:
                //IIC_MLX_90641
                initI2CMatrix(new IIC_MLX_90641());
                break;
            case TempModelEntity.SP_MLX_90641_ESF:
                //SP_MLX_90641_ESF
                initSerialMatrix(new SP_MLX_90641_ESF(dev, rare));
                break;
            case TempModelEntity.SP_MLX_90621_TIR_SN:
                //SP_MLX_90621_TIR_SN
                initSerialMatrix(new SP_MLX_90621_TIR_SN(dev, rare));
                break;
            case TempModelEntity.SP_MLX_90621_BAA:
                initSerialMatrix(new SP_MLX_90621_BAA(dev, rare));
                break;
            case TempModelEntity.SP_MLX_90621_BAA_ZM_TXT:
                initSerialMatrix(new SP_MLX_90621_BAA_ZM(dev, rare, SP_MLX_90621_BAA_ZM.PARSE_TYPE_STR));
                break;
            case TempModelEntity.SP_MLX_90621_BAA_ZM_HEX:
                initSerialMatrix(new SP_MLX_90621_BAA_ZM(dev, rare, SP_MLX_90621_BAA_ZM.PARSE_TYPE_HEX));
                break;
            case TempModelEntity.SP_MLX_90621_HY:
                initSerialMatrix(new SP_MLX_90621_HY(dev, rare));
                break;
            case TempModelEntity.SP_MLX_90621_HY1:
                initSerialMatrix(new SP_MLX_90621_HY1(dev, rare));
                break;
            case TempModelEntity.SP_MLX_90621_JST:
                initSerialMatrix(new SP_MLX_90621_JSF(dev, rare));
                break;
            case TempModelEntity.IIC_MLX_90621_BAA:
                initI2CMatrix(new IIC_MLX_90621_BAA());
                break;
            case TempModelEntity.SP_HM_MT031:
                initSerialMatrix(new SP_HM_MT031(dev, rare));
                break;
            case TempModelEntity.SP_KM_D801:
                initSerialMatrix(new SP_KM_D801(dev, rare));
                break;
            case TempModelEntity.SP_OTPA_16:
                initSerialMatrix(new SP_OTPA_16(dev, rare));
                break;
            case TempModelEntity.SP_MLX_90641_YNC:
                initCustom(new SP_MLX_90641_YNC(dev, MyApp.getInstance().getApplicationContext()));
                break;
            case TempModelEntity.SP_OMR_D6T_4X4:
                initSerialMatrix(new SP_OMR_D6T_4X4(dev, rare));
                break;
            case TempModelEntity.IIC_OTPA_16:
                initI2CMatrix(new IIC_OTPA_16());
                break;
            case TempModelEntity.SP_MLX_90641_STM:
                initSerialMatrix(new SP_MLX_90641_STM(dev, rare));
                break;
            case TempModelEntity.SP_HM_V11:
                initSerialMatrix(new SP_HM_V11(dev, rare));
                break;
            case TempModelEntity.USB_OTPA_MC3216:
                initSerialMatrix(new USB_OTPA_MC3216(dev, rare));
                break;
            case TempModelEntity.IIC_HPTA_32:
                initI2CMatrix(new IIC_HPTA_32());
                break;
            case TempModelEntity.SP_EXTERNAL_SMALL_PLATE:
                initSerialMatrix(new SP_OMR_D6T_4X4(dev, rare));
                break;
            case TempModelEntity.SP_HM5_0_STM_YS:
                initSerialMatrix(new SP_HM5_0_STM_YS(dev, rare));
                break;
        }
    }

    private void initI2CPoint(IPointThermometer deivce) {
        mDevice = deivce;
        mDevice.startUp(new MeasureResult() {
            @Override
            public Point getHeadPoint() {
                return headPoint;
            }

            @Override
            public int getDistance() {
                return mRectDistance;
            }

            @Override
            public void onResult(TemperatureEntity entity, Object oneFrame) {
                if (entity == null)
                    return;
                tempEntity = entity;
//                mTemperatue = entity.temperatue;
                if (mTempCallBack != null) {
                    mTempCallBack.onTempResult(entity, mRectDistance);
                }
                mTemperatue = entity.temperatue;
                log("=====温度: " + mTemperatue);
            }
        }, 100);
    }


    /***
     * i2C矩阵
     * @param device
     */
    private void initI2CMatrix(IMatrixThermometer device) {
        mDevice = device;
        mDevice.startUp(new MeasureResult() {
            @Override
            public Point getHeadPoint() {
                return headPoint;
            }

            @Override
            public int getDistance() {
                return mRectDistance;
            }

            @Override
            public void onResult(TemperatureEntity entity, Object oneFrame) {
                if (entity == null)
                    return;
                tempEntity = entity;
                if (mTempCallBack != null) {
//                    mTempCallBack.onTempCallBack(entity, oneFrame, ((IMatrixThermometer) mDevice).getParm(), true, true);
                    mTempCallBack.onTempResult(entity, mRectDistance);
                }
                mTemperatue = entity.temperatue;
                log("=====温度: " + mTemperatue);

            }
        }, ((IMatrixThermometer) mDevice).getParm().perid);
    }

    /***
     * 串口矩阵
     * @param device
     */
    private void initSerialMatrix(ProductImp device) {
        mDevice = device;
        ((ProductImp) mDevice).startUp(new MeasureResult() {
            @Override
            public Point getHeadPoint() {
                return headPoint;
            }

            @Override
            public int getDistance() {
                return mRectDistance;
            }

            @Override
            public void onResult(TemperatureEntity entity, Object oneFrame) {
                log("=====温度: ===onResult=====");
                if (entity == null)
                    return;
                tempEntity = entity;
                if (entity.temperatue == CALIBRATION_COMPLE) {
                    if (mTempCallBack != null) {
                        mTempCallBack.onCalibrationComplete();
                    }
                    return;
                }

                if (mTempCallBack != null) {
//                    mTempCallBack.onTempCallBack(entity, oneFrame, ((ProductImp) mDevice).getParm(), false, true);
                    mTempCallBack.onTempResult(entity, mRectDistance);
                }

                mTemperatue = entity.temperatue;
                log("=====温度: " + mTemperatue);
//                mTemperatue = entity.temperatue;
            }
        });

        byte[] order = ((ProductImp) mDevice).getOrderDataOutputQuery();
        mDevice.order(order);
    }

    private void initCustom(MeasureDevice device) {
        mDevice = device;
        mDevice.startUp(new MeasureResult() {
            @Override
            public Point getHeadPoint() {
                return headPoint;
            }

            @Override
            public int getDistance() {
                return mRectDistance;
            }

            @Override
            public void onResult(TemperatureEntity entity, Object oneFrame) {
                log("=====温度: ===onResult=====");
                if (entity == null)
                    return;
                tempEntity = entity;
                if (entity.temperatue == CALIBRATION_COMPLE) {
                    if (mTempCallBack != null) {
                        mTempCallBack.onCalibrationComplete();
                    }
                    return;
                }

                if (mTempCallBack != null) {
//                    mTempCallBack.onTempCallBack(entity, oneFrame, ((ProductImp) mDevice).getParm(), false, true);
                    mTempCallBack.onTempResult(entity, mRectDistance);
                }

                mTemperatue = entity.temperatue;
                log("=====温度: " + mTemperatue);
//                mTemperatue = entity.temperatue;
            }
        }, mDevice.getParm() == null ? 0 : mDevice.getParm().perid);
    }


    public void setTempCallBack(TempCallBack callBack) {
        this.mTempCallBack = callBack;
    }


    public void sendTempData() {
        if (mDevice != null && mDevice instanceof SP_MLX_90621_HY1) {
            if (mDevice != null && mDevice instanceof ProductImp) {
                byte[] order = ((ProductImp) mDevice).getOrderDataOutputQuery();
                if (order != null && order.length > 0)
                    mDevice.order(order);
            }
        }
    }

    //调用模块本身的校准
    public void calibration(float targetTemp) {
        if (mDevice instanceof SP_HM_MT031) {
            ((SP_HM_MT031) mDevice).calibration(targetTemp);
        }
    }

    /**
     * 获取温度
     *
     * @param rectDistance
     * @return
     * @Param headPoint 额头坐标
     */
    public float getTemp(int rectDistance) {
        float temp = mTemperatue;
        float old = temp;
        if (temp == -99f) {
            return temp;
        }
        temp += SpUtils.getmTempComp();
        log("===========getTemp: old = " + old + " /temp =  " + temp + " / " + rectDistance);
        if (mDevice != null) {
            //自动校准补温后显示前做处理
            temp = mDevice.afterHandler(temp, rectDistance);
            log("===========getTemp afterHandler:" + temp);
        }
        //获取后重置，
//        mTemperatue = 0;
        return temp;
    }


    public void destory() {
        mTemperatue = -99f;
        if (mDevice != null) mDevice.destroy();
        mDevice = null;
    }

    private void log(String msg) {
//        LogUtil.temperature(msg);
    }

    public interface TempCallBack {

        public void onTempResult(TemperatureEntity temperature, int rectDistance);

        //校准完成回调
        void onCalibrationComplete();

    }

}
