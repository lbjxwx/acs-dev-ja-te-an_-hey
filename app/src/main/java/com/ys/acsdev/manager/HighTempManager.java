package com.ys.acsdev.manager;

import android.os.Handler;
import android.os.Message;

import com.ys.acsdev.util.GpioUtils;
import com.ys.acsdev.util.LogUtil;

// Created by kermitye on 2020/6/1 19:03
public class HighTempManager {
    private HighTempManager() {
    }

    private static class SingletonHolder {
        public static final HighTempManager INSTANCE = new HighTempManager();
    }

    public static HighTempManager getInstance() {
        return SingletonHolder.INSTANCE;
    }

    private static final int gpIO = 68;
    private static final int MSG_CLOSE = 1;
    private static final int MSG_CHECK = 2;
    private static final long CLOSE_TIME = 1000;


    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            mHandler.removeMessages(msg.what);
            switch (msg.what) {
                case MSG_CLOSE:
                    close();
                    break;
                case MSG_CHECK:
                    checkIO();
                    //设置为输出口
                    GpioUtils.setGpioDirection(gpIO, 0);
                    sendEmptyMessageDelayed(MSG_CLOSE, 500);
                    break;
            }
        }
    };


    public void init() {
        GpioUtils.upgradeRootPermissionForExport();
        mHandler.sendEmptyMessageDelayed(MSG_CHECK, 500);
    }


    public boolean open() {
        log("=======HighTempManager======开启");
        boolean success = GpioUtils.writeGpioValue(gpIO, "1");
        mHandler.sendEmptyMessageDelayed(MSG_CLOSE, CLOSE_TIME);
        return true;
    }

    private boolean checkIO() {
        if (GpioUtils.exportGpio(gpIO)) {
            GpioUtils.upgradeRootPermissionForGpio(gpIO);
            String status = GpioUtils.getGpioDirection(gpIO);
            if ("" == status || status == null) {
                return false;
            } else {
                return true;
            }
        }
        return false;
    }

    private void close() {
        log("=======HighTempManager======关闭");
        GpioUtils.writeGpioValue(gpIO, "0");
    }


    private String getGpioValue() {
        return GpioUtils.getGpioValue(gpIO);
    }


    private void log(String msg) {
        LogUtil.e(msg, "HighTempManager");
    }
}
