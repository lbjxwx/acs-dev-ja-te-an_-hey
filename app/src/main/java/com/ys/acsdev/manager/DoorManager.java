package com.ys.acsdev.manager;

import com.ys.acsdev.api.AppConfig;
import com.ys.acsdev.door.BaseDoor;
import com.ys.acsdev.door.RelayDoor;
import com.ys.acsdev.door.SerialportDoor;
import com.ys.acsdev.util.Biantai;
import com.ys.acsdev.util.LogUtil;
import com.ys.acsdev.util.SimpleDateUtil;
import com.ys.acsdev.util.SpUtils;

// Created by kermitye on 2020/4/18 13:48
public class DoorManager {
    private DoorManager() {
    }

    private static class SingletonHolder {
        public static final DoorManager INSTANCE = new DoorManager();
    }

    public static DoorManager getInstance() {
        return SingletonHolder.INSTANCE;
    }

    private BaseDoor mDoor;

    public void init() {
        boolean isOpenDoor = SpUtils.getDoorEnable();
        if (isOpenDoor) {
            mDoor = RelayDoor.getInstance();
            mDoor.setCloseTime(SpUtils.getCloseDoorTime());
            mDoor.init();
        }
    }

    public void open(String printTag) {
        if (mDoor == null) {
            return;
        }
        if (!SpUtils.getDoorEnable()) {
            return;
        }
        if (Biantai.openDoorDistanceTime()) {
            return;
        }
        mDoor.open();
    }

    public void close() {
        if (mDoor == null)
            return;
        mDoor.close();
    }

    public void destory() {
        if (mDoor == null)
            return;
        mDoor.destory();
        mDoor = null;
    }

}
