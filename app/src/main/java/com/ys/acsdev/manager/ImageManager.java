package com.ys.acsdev.manager;

import android.content.Context;
import android.graphics.Bitmap;
import android.media.Image;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.signature.ObjectKey;
import com.ys.acsdev.MyApp;
import com.ys.acsdev.R;
import com.ys.acsdev.service.AcsService;

import java.io.File;

public class ImageManager {

    /***
     * 加载bitmap
     * @param context
     * @param bitmap
     * @param mIvLogo
     */
    public static void loadBitmap(Context context, Bitmap bitmap, ImageView mIvLogo) {
        int defaultLogo = R.mipmap.app_icon;
        Glide.with(context)
                .load(bitmap)
                .apply(new RequestOptions().placeholder(defaultLogo)
                        .error(defaultLogo)
                        .skipMemoryCache(true)    //禁止Glide内存缓存
                        .diskCacheStrategy(DiskCacheStrategy.NONE)    //不缓存资源
                        .signature(new ObjectKey(System.currentTimeMillis()))).into(mIvLogo);
    }

    public static void displayImagePathNoCache(Context context, String imagePath, ImageView imageView) {
        Glide.with(context)
                .load(imagePath)
                .apply(new RequestOptions()
                        .skipMemoryCache(true)    //禁止Glide内存缓存
                        .diskCacheStrategy(DiskCacheStrategy.NONE)    //不缓存资源
                        .signature(new ObjectKey(System.currentTimeMillis()))).into(imageView);

    }

    public static void displayImagePath(String imagePath, ImageView imageView) {
        Glide.with(MyApp.getInstance())
                .load(imagePath)
                .into(imageView);
    }


    /***
     * 加载本地图片
     * @param imageId
     * @param imageView
     */
    public static void displayImageResource(int imageId, ImageView imageView) {
        Glide.with(MyApp.getInstance())
                .load(imageId)
                .into(imageView);
    }

    public static void displayImageDefault(String path, ImageView imageView, int defaultLogo) {
        Glide.with(MyApp.getInstance())
                .load(path)
                .apply(new RequestOptions().placeholder(defaultLogo)
                        .error(defaultLogo)
                        .signature(new ObjectKey(System.currentTimeMillis()))).into(imageView);
    }

    public static void displayImageNoCache(File file, ImageView imageView, int defaultLogo) {
        Glide.with(MyApp.getInstance())
                .load(file)
                .apply(new RequestOptions().placeholder(defaultLogo)
                        .error(defaultLogo)
                        .skipMemoryCache(true)    //禁止Glide内存缓存
                        .diskCacheStrategy(DiskCacheStrategy.NONE)    //不缓存资源
                        .signature(new ObjectKey(System.currentTimeMillis()))).into(imageView);
    }

    /**
     * 清除缓存
     */
    public static void clearCache() {
        clearMemoryCache(MyApp.getInstance());
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                clearDiskCache(MyApp.getInstance());
            }
        };
        AcsService.getInstance().executor(runnable);
    }

    /**
     * 清除内存缓存
     */
    public static void clearMemoryCache(Context context) {
        Glide.get(context).clearMemory();
    }

    /**
     * 清除磁盘缓存
     */
    public static void clearDiskCache(Context context) {
        Glide.get(context).clearDiskCache();
    }

}
