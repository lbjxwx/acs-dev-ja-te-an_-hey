package com.ys.acsdev.task.layout;


/**
 * 任务播放状态回调
 */
public interface TaskPlayStateListener {

    int TAG_PLAY_VIDEO = 0;  //视频
    int TAG_PLAY_PICTURE = 3; //图片

    /***
     * 播放完毕
     */
    void playComplete();

    /**
     * 播放异常，重启一次
     *
     * @param errorDesc
     */
    void reStartPlayProgram(String errorDesc);
}
