package com.ys.acsdev.task.banner;

import android.content.Context;

import androidx.viewpager.widget.ViewPager;

import com.ys.acsdev.task.banner.code.Banner;
import com.ys.acsdev.task.layout.MediAddEntity;
import com.ys.acsdev.task.layout.TaskPlayStateListener;

import java.util.List;

public class BannerUtil {

    Banner banner;
    Context context;
    ViewPager.OnPageChangeListener listener;
    TaskPlayStateListener taskPlayStateListener;

    public BannerUtil(Context context, Banner paramBanner,
                      ViewPager.OnPageChangeListener listener,
                      TaskPlayStateListener taskPlayStateListener) {
        this.context = context;
        this.banner = paramBanner;
        this.listener = listener;
        this.taskPlayStateListener = taskPlayStateListener;
    }

    public void setPlayListPic(List<MediAddEntity> imageList) {
        try {
            if (imageList == null || imageList.size() < 1) {
                return;
            }
            banner.setImageLoader(new GlideImageLoader());
            banner.setImages(imageList);
            banner.isAutoPlay(true);
            banner.setOffscreenPageLimit(8);  //预加载的数量
            banner.setBannerItemClickListener(taskPlayStateListener);
            banner.setOnPageChangeListener(listener);
            banner.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void startPlay() {
        try {
            banner.startAutoPlay("bannerUtil.startPlay()");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void stopPlay() {
        try {
            if (banner == null) {
                return;
            }
            banner.stopAutoPlay();
            banner.releaseBanner("============banneUtil stopPlay==");
            banner = null;
        } catch (Exception e) {
        }
    }
}
