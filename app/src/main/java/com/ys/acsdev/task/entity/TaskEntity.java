package com.ys.acsdev.task.entity;

//import org.litepal.crud.DataSupport;

import org.litepal.crud.LitePalSupport;

import java.util.ArrayList;

public class TaskEntity extends LitePalSupport {

    String taskId;
    String taskName;
    String screenType;
    //    String startTime;
//    String endTime;
    ArrayList<SourceEntity> list;

    public ArrayList<SourceEntity> getList() {
        return list;
    }

    public void setList(ArrayList<SourceEntity> list) {
        this.list = list;
    }


    public TaskEntity(String taskId, String taskName, String screenType) {
        this.taskId = taskId;
        this.taskName = taskName;
        this.screenType = screenType;
    }

//    public TaskEntity(String taskId, String taskName, String screenType, String startTime, String endTime) {
//        this.taskId = taskId;
//        this.taskName = taskName;
//        this.screenType = screenType;
//        this.startTime = startTime;
//        this.endTime = endTime;
//    }

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public String getScreenType() {
        return screenType;
    }

    public void setScreenType(String screenType) {
        this.screenType = screenType;
    }

//    public String getStartTime() {
//        return startTime;
//    }
//
//    public void setStartTime(String startTime) {
//        this.startTime = startTime;
//    }
//
//    public String getEndTime() {
//        return endTime;
//    }
//
//    public void setEndTime(String endTime) {
//        this.endTime = endTime;
//    }

    @Override
    public String toString() {
        return "TaskEntity{" +
                "taskName='" + taskName + '\'' +
                ", screenType='" + screenType + '\'' +
                ", list=" + list +
                '}';
    }
}
