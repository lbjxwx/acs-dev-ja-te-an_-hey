package com.ys.acsdev.task.banner.code;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;

import androidx.viewpager.widget.ViewPager;

import com.ys.acsdev.R;
import com.ys.acsdev.task.banner.adapter.BannerPagerAdapter;
import com.ys.acsdev.task.banner.adapter.BannerViewPager;
import com.ys.acsdev.task.banner.code.loader.ImageLoaderInterface;
import com.ys.acsdev.task.layout.MediAddEntity;
import com.ys.acsdev.task.layout.TaskPlayStateListener;
import com.ys.acsdev.util.LogUtil;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;


public class Banner extends FrameLayout implements ViewPager.OnPageChangeListener {
    public String tag = "banner";
    private boolean isAutoPlay = BannerConfig.IS_AUTO_PLAY;
    private boolean isScroll = BannerConfig.IS_SCROLL;
    private int count = 0;
    private int currentItem;
    private List<MediAddEntity> ListUrls;
    private List<View> imageViews;
    private Context context;
    private BannerViewPager viewPager;
    private ImageLoaderInterface imageLoader;
    private BannerPagerAdapter adapter;
    private ViewPager.OnPageChangeListener mOnPageChangeListener;
    private BannerScroller mScroller;
    private boolean isLongClick;

    public Banner(Context context) {
        this(context, null);
    }

    public Banner(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public Banner(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.context = context;
        ListUrls = new ArrayList<MediAddEntity>();
        imageViews = new ArrayList<View>();
        initView(context, attrs);
    }

    private WeakHandler handler;

    private void initView(Context context, AttributeSet attrs) {
        Log.e(tag, "============initView======");
        releaseBanner("初始化之前，先释放");
        handler = new WeakHandler();
        imageViews.clear();
        View view = LayoutInflater.from(context).inflate(R.layout.banner, this, true);
        viewPager = (BannerViewPager) view.findViewById(R.id.banner_viewpager);
        initViewPagerScroll();
    }

    /**
     * 设置pager切换的时间
     */
    private void initViewPagerScroll() {
        try {
            Field mField = ViewPager.class.getDeclaredField("mScroller");
            mField.setAccessible(true);
            mScroller = new BannerScroller(viewPager.getContext());
            mScroller.setDuration(BannerConfig.DURATION);
            mField.set(viewPager, mScroller);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Banner isAutoPlay(boolean isAutoPlay) {
        this.isAutoPlay = isAutoPlay;
        return this;
    }

    public Banner setImageLoader(ImageLoaderInterface imageLoader) {
        this.imageLoader = imageLoader;
        return this;
    }

    /**
     * 设置播放的间隔时间
     *
     * @param transformer
     * @return
     */
    public Banner setBannerAnimation(Class<? extends ViewPager.PageTransformer> transformer) {
        try {
            setPageTransformer(false, transformer.newInstance());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return this;
    }

    public Banner setPageTransformer(boolean reverseDrawingOrder, ViewPager.PageTransformer transformer) {
        viewPager.setPageTransformer(reverseDrawingOrder, transformer);
        return this;
    }


    public Banner setOffscreenPageLimit(int limit) {
        if (viewPager != null) {
            viewPager.setOffscreenPageLimit(limit);
        }
        return this;
    }

    public Banner setViewPagerIsScroll(boolean isScroll) {
        this.isScroll = isScroll;
        return this;
    }

    public Banner setImages(List<MediAddEntity> imageList) {
        this.ListUrls = imageList;
        this.count = ListUrls.size();
        return this;
    }

    public void update(List<MediAddEntity> imageUrls) {
        ListUrls.clear();
        ListUrls.addAll(imageUrls);
        count = ListUrls.size();
        start();
    }

    public Banner start() {
        setImageList(ListUrls);
        setData();
        return this;
    }

    private void setImageList(List<MediAddEntity> imageUrls) {
        if (imageUrls == null || imageUrls.size() <= 0) {
            return;
        }
        imageViews.clear();
        for (int i = 0; i <= count + 1; i++) {
            ImageView imageView = null;
            if (imageLoader != null) {
                imageView = imageLoader.createImageView(context);
            }
            if (imageView == null) {
                imageView = new ImageView(context);
            }
            imageView.setScaleType(ScaleType.FIT_XY);
            imageViews.add(imageView);
        }
    }

    private void setData() {
        currentItem = 1;
        if (adapter == null) {
            adapter = new BannerPagerAdapter(context, ListUrls, imageViews, imageLoader);
            viewPager.addOnPageChangeListener(this);
            adapter.setBannerItemClickListener(taskPlayStateListener);
        }
        viewPager.setAdapter(adapter);
        viewPager.setFocusable(true);
        viewPager.setCurrentItem(1);
        if (isScroll && count > 1) {
            viewPager.setScrollable(true);
        } else {
            viewPager.setScrollable(false);
        }
        if (isAutoPlay) {
            startAutoPlay("setData");
        }
    }

    private final Runnable task = new Runnable() {
        @Override
        public void run() {
            if (handler == null) {
                return;
            }
            if (count == 1) {
                //一个任务多个节目，一张图片没有切换的过程
                if (mOnPageChangeListener == null) {
                    return;
                }
                mOnPageChangeListener.onPageSelected(-1);
            } else if (count > 1 && isAutoPlay) {
                goToNextViewPage();
            }
        }
    };

    private int delayTime = BannerConfig.TIME;

    /**
     * 播放下一页
     */
    private void goToNextViewPage() {
        currentItem = currentItem % (count + 1) + 1;
        if (currentItem == 1) {
            viewPager.setCurrentItem(currentItem, false);
            if (handler != null) {
                handler.post(task);
            }
        } else {
            viewPager.setCurrentItem(currentItem);
            if (handler != null) {
                handler.postDelayed(task, delayTime);
            }
        }
    }

    public void startAutoPlay(String tag) {
        if (handler == null) {
            return;
        }
        handler.removeCallbacks(task);
        handler.postDelayed(task, delayTime);
    }


    public void stopAutoPlay() {
        if (handler != null) {
            handler.removeCallbacks(task);
            handler.removeCallbacksAndMessages(null);
        }
    }

    public void releaseBanner(String tag) {
        try {
            count = 0;
            ListUrls.clear();
            if (handler != null) {
                handler.removeCallbacks(task);
                handler.removeCallbacksAndMessages(null);
                handler = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (isAutoPlay) {
            int action = ev.getAction();
            if (action == MotionEvent.ACTION_UP || action == MotionEvent.ACTION_CANCEL || action == MotionEvent
                    .ACTION_OUTSIDE) {
                if (!isLongClick)
                    startAutoPlay("dispatchTouchEvent");
            } else if (action == MotionEvent.ACTION_DOWN) {
                isLongClick = false;
                stopAutoPlay();
            }
        }
        return super.dispatchTouchEvent(ev);
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        int action = event.getAction();
        int keyCode = event.getKeyCode();
        if (isAutoPlay) {
            if (keyCode == KeyEvent.KEYCODE_DPAD_LEFT || keyCode == KeyEvent.KEYCODE_DPAD_RIGHT) {
                switch (action) {
                    case KeyEvent.ACTION_DOWN:
                        stopAutoPlay();
                        break;
                    case KeyEvent.ACTION_UP:
                        startAutoPlay("dispatchKeyEvent");
                        break;
                }
            }
        }
        if (event.getAction() == KeyEvent.ACTION_DOWN) {
            switch (event.getKeyCode()) {
                case KeyEvent.KEYCODE_DPAD_LEFT:
                case KeyEvent.KEYCODE_DPAD_RIGHT:
                    //handled = arrowScroll(FOCUS_RIGHT);
                    if (currentItem == count + 1) {
                        viewPager.setCurrentItem(1, false);
                    } else if (currentItem == 0) {
                        viewPager.setCurrentItem(count, false);
                    }
                    break;
            }
        }
        return super.dispatchKeyEvent(event);

    }

    TaskPlayStateListener taskPlayStateListener;


    public void setBannerItemClickListener(TaskPlayStateListener taskPlayStateListener) {
        this.taskPlayStateListener = taskPlayStateListener;
    }

    @Override
    public void onPageScrollStateChanged(int state) {
        LogUtil.task("=====BANNER====onPageScrollStateChanged====");
        if (mOnPageChangeListener != null) {
            mOnPageChangeListener.onPageScrollStateChanged(state);
        }
        currentItem = viewPager.getCurrentItem();
        if (state == ViewPager.SCROLL_STATE_IDLE) { //空闲状态

        } else if (state == ViewPager.SCROLL_STATE_DRAGGING) {   //拖曳
            if (currentItem == count + 1) {
                viewPager.setCurrentItem(1, false);
            } else if (currentItem == 0) {
                viewPager.setCurrentItem(count, false);
            }
        }
    }


    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        LogUtil.task("=====BANNER====onPageScrolled====");
        if (mOnPageChangeListener != null) {
            mOnPageChangeListener.onPageScrolled(position, positionOffset, positionOffsetPixels);
        }

    }


    @Override
    public void onPageSelected(int position) {
        LogUtil.task("=====BANNER====onPageSelected====" + position);
        if (mOnPageChangeListener != null) {
            mOnPageChangeListener.onPageSelected(position);
        }
        //如果只有一张图片，不执行动画
        if (ListUrls == null || ListUrls.size() < 2) {
            return;
        }
    }

    public void setOnPageChangeListener(ViewPager.OnPageChangeListener onPageChangeListener) {
        mOnPageChangeListener = onPageChangeListener;
    }

}
