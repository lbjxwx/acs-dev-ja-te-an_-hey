package com.ys.acsdev.task.layout;

public interface VideoPlayListener {

    void initOver();

    void playCompletion(String tag);

    /***
     * splash界面专项接口\
     * 播放时长
     */
    void playCompletionSplash(int position, int playTimeCurrent);

    void playError(String errorDesc);

    /**
     * 播放出错了，这里重启y一次播放一次
     *
     * @param errorDesc
     */
    void reStartPlayProgram(String errorDesc);


}
