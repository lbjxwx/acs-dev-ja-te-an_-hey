package com.ys.acsdev.task.banner.code.loader;

import android.content.Context;
import android.widget.ImageView;

import com.ys.acsdev.task.banner.code.loader.ImageLoaderInterface;

public abstract class ImageLoader implements ImageLoaderInterface<ImageView> {

    @Override
    public ImageView createImageView(Context context) {
        ImageView imageView = new ImageView(context);
        return imageView;
    }

}
