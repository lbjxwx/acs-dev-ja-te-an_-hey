package com.ys.acsdev.task.parsener;

import android.content.Context;
import android.widget.AbsoluteLayout;

import com.ys.acsdev.task.layout.Generator;
import com.ys.acsdev.task.layout.ViewSleepViewGenertrator;
import com.ys.acsdev.task.view.TaskPlayView;
import com.ys.acsdev.util.LogUtil;
import com.ys.acsdev.util.SpUtils;
import com.ys.ysim.task.model.TaskModel;
import com.ys.ysim.task.model.TaskModelmpl;

public class TaskPlayerParsener {
    Context context;
    TaskPlayView taskPlayView;
    AbsoluteLayout ab_view;
    Generator generateView = null;

    public TaskPlayerParsener(Context context, TaskPlayView taskPlayView) {
        this.context = context;
        this.taskPlayView = taskPlayView;
        getView();
    }

    private void getView() {
        ab_view = taskPlayView.getAbView();
    }


    public void getTaskFromDb() {
        LogUtil.task("======getTaskFromDb======");
        toShowDefaultAnmalView();
    }

    private void toShowDefaultAnmalView() {
        LogUtil.task("========加载默认旋转动画选项=====");
        clearTaskView();
        generateView = new ViewSleepViewGenertrator(
                context, 0, 0, SpUtils.getScreenwidth(), SpUtils.getScreenheight());
        ab_view.addView(generateView.getView(), generateView.getLayoutParams());
        generateView.updateView(null);
    }

    public void clearTaskView() {
        LogUtil.task("====清理view  clearTaskView===");
        if (generateView != null) {
            LogUtil.task("====清理view    generateView.clearMemory===");
            generateView.clearMemory("");
            generateView = null;
        }
        if (ab_view != null) {
            LogUtil.task("====清理view   ab_view.removeAllViews();===");
            ab_view.removeAllViews();
        }
    }

    /***
     * 更新睡眠时间
     */
    public void updateSleepTvOnTime() {
        if (generateView == null) {
            return;
        }
        generateView.updateView(null);
    }

}
