package com.ys.acsdev.task.db;

import android.content.ContentValues;

import com.ys.acsdev.task.entity.SourceEntity;
import com.ys.acsdev.task.entity.TaskEntity;
import com.ys.acsdev.util.LogUtil;

import org.litepal.LitePal;

import java.util.List;

public class DbTaskManagerJava {

    public static boolean saveSourceInfoToDb(SourceEntity sourceEntity) {
        if (sourceEntity == null) {
            return false;
        }
        return addSourceInfoToDb(sourceEntity);
    }

    private static boolean addSourceInfoToDb(SourceEntity sourceEntity) {
        boolean isSave = false;
        if (sourceEntity == null) {
            return isSave;
        }
        try {
            isSave = sourceEntity.save();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return isSave;
    }


    public static boolean saveTaskInfoToDb(TaskEntity taskEntity) {
        if (taskEntity == null) {
            return false;
        }
        LogUtil.db("===========saveTaskInfoToDb===" + taskEntity.toString());
        String taskId = taskEntity.getTaskId();
        List<TaskEntity> taskList = LitePal.where("taskId=?", taskId).find(TaskEntity.class);
        if (taskList == null || taskList.size() < 1) {
            LogUtil.cdl("===0000====没有数据，添加到数据库");
            return addTaskInfoToDb(taskEntity);
        } else {
            return modifyTaskInfo(taskEntity);
        }
    }

    /**
     * 修改
     */
    private static boolean modifyTaskInfo(TaskEntity taskEntity) {
        LogUtil.db("===0000====modifyTaskInfo");
        if (taskEntity == null) {
            return false;
        }
        String taskId = taskEntity.getTaskId();
        String taskName = taskEntity.getTaskName();
        String screenType = taskEntity.getScreenType();
        ContentValues values = new ContentValues();
        values.put("taskName", taskName);
        values.put("screenType", screenType);
        int modifyNum = LitePal.updateAll(TaskEntity.class, values, "taskId=?", taskId);
        if (modifyNum > 0) {
            return true;
        }
        return false;
    }


    private static boolean addTaskInfoToDb(TaskEntity taskEntity) {
        LogUtil.db("===0000====addTaskInfoToDb");
        boolean isSave = false;
        if (taskEntity == null) {
            return isSave;
        }
        try {
            isSave = taskEntity.save();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return isSave;
    }

    public static boolean deleTaskInfoByTask(TaskEntity taskEntity) {
        if (taskEntity == null) {
            return true;
        }
        String taskId = taskEntity.getTaskId();
        try {
            int delSenNum = LitePal.deleteAll(TaskEntity.class, "taskId= ? ", taskId);
            if (delSenNum > 0) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }


    /**
     * 获取所有的任务集合
     */
    public static List<TaskEntity> getAllTaskFromDb() {
        List<TaskEntity> raskEntityLists = LitePal.findAll(TaskEntity.class);
        return raskEntityLists;
    }

    /**
     * 根据任务ID 去获取素材下载列表
     */
    public static List<SourceEntity> getSourceListByTaskId(String taskId) {
        List<SourceEntity> sourceList = LitePal.where("taskId=?", taskId).find(SourceEntity.class);
        return sourceList;
    }

    public static void deleteAllTaskInfo() {
        try {
            LogUtil.db("=========清理所有得数据");
            LitePal.deleteAll(TaskEntity.class);
            LitePal.deleteAll(SourceEntity.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
