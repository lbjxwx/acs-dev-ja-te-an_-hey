package com.ys.acsdev.task.layout;

import android.content.Context;
import android.view.View;
import android.widget.AbsoluteLayout;


/**
 * 动态生成控件基类
 * Created by Neo on 2015/11/2 0002.
 */
public abstract class Generator {

    public Generator(Context context, int x, int y, int width, int height) {
        this.setX(x);
        this.setY(y);
        this.setWidth(width);
        this.setHeight(height);
    }

    private int x, y;                    //控件左上角坐标
    private int width, height;           //控件宽高

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    /**
     * 获取生成的控件
     *
     * @return 生成好的控件
     */
    public abstract View getView();

    public abstract void clearMemory(String tag);

    /***
     * 提供一个接口去刷新界面
     */
    public abstract void updateView(Object object);

    /***
     * 播放完毕回调界面
     */
    public abstract void playComplet();


    public TaskPlayStateListener listener;

    public void setPlayStateChangeListener(TaskPlayStateListener listener) {
        this.listener = listener;
    }

    /**
     * 获取在AbsoluteLayout中的布局参数,可根据需要进行覆盖
     *
     * @return
     */
    public AbsoluteLayout.LayoutParams getLayoutParams() {
        AbsoluteLayout.LayoutParams params = new AbsoluteLayout.LayoutParams(getWidth(), getHeight(), getX(), getY());
        return params;
    }


}
