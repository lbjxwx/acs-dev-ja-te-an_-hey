package com.ys.acsdev.task.entity;

import org.litepal.crud.LitePalSupport;

public class SourceEntity extends LitePalSupport {

    String type;
    String name;
    long size;
    String taskId;
    String sourceId;
    String path;

    public SourceEntity(String taskId, String sourceId, String type, String name, long size, String path) {
        this.taskId = taskId;
        this.sourceId = sourceId;
        this.type = type;
        this.name = name;
        this.size = size;
        this.path = path;
    }

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public String getSourceId() {
        return sourceId;
    }

    @Override
    public String toString() {
        return "SourceEntity{" +
                "type='" + type + '\'' +
                ", name='" + name + '\'' +
                ", size=" + size +
                ", taskId='" + taskId + '\'' +
                ", sourceId='" + sourceId + '\'' +
                ", path='" + path + '\'' +
                '}';
    }

    public void setSourceId(String sourceId) {
        this.sourceId = sourceId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
