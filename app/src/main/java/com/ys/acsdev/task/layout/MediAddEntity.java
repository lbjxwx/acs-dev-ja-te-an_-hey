package com.ys.acsdev.task.layout;

import java.io.Serializable;

public class MediAddEntity implements Serializable {

    public String url;  //播放地址
    public String midId;   //素材ID
    public int fileType;      // 文件类型
    public String taskId;

    public MediAddEntity(String url, String midId, int fileType, String taskId) {
        this.url = url;
        this.midId = midId;
        this.fileType = fileType;
        this.taskId = taskId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getMidId() {
        return midId;
    }

    public void setMidId(String midId) {
        this.midId = midId;
    }

    public int getFileType() {
        return fileType;
    }

    public void setFileType(int fileType) {
        this.fileType = fileType;
    }

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    @Override
    public String toString() {
        return "MediAddEntity{" +
                "url='" + url + '\'' +
                ", midId='" + midId + '\'' +
                ", fileType=" + fileType +
                ", taskId='" + taskId + '\'' +
                '}';
    }
}
