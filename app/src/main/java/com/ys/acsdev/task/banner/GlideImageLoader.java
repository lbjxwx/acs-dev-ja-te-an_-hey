package com.ys.acsdev.task.banner;

import android.content.Context;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.ys.acsdev.task.banner.code.loader.ImageLoader;

public class GlideImageLoader extends ImageLoader {

    public void displayImage(Context paramContext, Object paramObject, final ImageView paramImageView) {
        try {
            Glide.with(paramContext)
                    .load(paramObject)
                    .thumbnail(0.1f)
//                    .diskCacheStrategy(DiskCacheStrategy.SOURCE)
//                    .fitCenter()
                    .into(paramImageView);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
