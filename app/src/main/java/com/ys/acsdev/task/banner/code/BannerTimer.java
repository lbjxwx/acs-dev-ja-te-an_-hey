//package com.ys.acsdev.task.banner.code;
//
//import android.content.Context;
//import android.os.Handler;
//import android.os.Message;
//import android.util.AttributeSet;
//import android.util.Log;
//import android.view.KeyEvent;
//import android.view.LayoutInflater;
//import android.view.MotionEvent;
//import android.view.View;
//import android.widget.FrameLayout;
//import android.widget.ImageView;
//import android.widget.ImageView.ScaleType;
//
//import androidx.viewpager.widget.ViewPager;
//
//import com.ys.acsdev.R;
//import com.ys.acsdev.task.banner.adapter.BannerPagerAdapter;
//import com.ys.acsdev.task.banner.adapter.BannerViewPager;
//import com.ys.acsdev.task.banner.code.loader.ImageLoaderInterface;
//import com.ys.acsdev.task.layout.MediAddEntity;
//import com.ys.acsdev.task.layout.TaskPlayStateListener;
//
//import java.lang.reflect.Field;
//import java.util.ArrayList;
//import java.util.List;
//import java.util.Timer;
//import java.util.TimerTask;
//
//public class BannerTimer extends FrameLayout implements ViewPager.OnPageChangeListener {
//    public String tag = "banner";
//    private boolean isAutoPlay = BannerConfig.IS_AUTO_PLAY;
//    private boolean isScroll = BannerConfig.IS_SCROLL;
//    private int count = 0;
//    private int currentItem;
//    private List<MediAddEntity> ListUrls;
//    private List<View> imageViews;
//    private Context context;
//    private BannerViewPager viewPager;
//    private ImageLoaderInterface imageLoader;
//    private BannerPagerAdapter adapter;
//    private ViewPager.OnPageChangeListener mOnPageChangeListener;
//    private BannerScroller mScroller;
//    private boolean isLongClick;
//    private int delayTime = BannerConfig.TIME;
//
//
//    public BannerTimer(Context context) {
//        this(context, null);
//    }
//
//    public BannerTimer(Context context, AttributeSet attrs) {
//        this(context, attrs, 0);
//    }
//
//    public BannerTimer(Context context, AttributeSet attrs, int defStyle) {
//        super(context, attrs, defStyle);
//        this.context = context;
//        ListUrls = new ArrayList<MediAddEntity>();
//        imageViews = new ArrayList<View>();
//        initView(context, attrs);
//    }
//
//    private void initView(Context context, AttributeSet attrs) {
//        Log.e(tag, "============initView======");
//        releaseBanner("初始化之前，先释放");
//        imageViews.clear();
//        View view = LayoutInflater.from(context).inflate(R.layout.banner, this, true);
//        viewPager = (BannerViewPager) view.findViewById(R.id.banner_viewpager);
//        initViewPagerScroll();
//    }
//
//    /**
//     * 设置pager切换的时间
//     */
//    private void initViewPagerScroll() {
//        try {
//            Field mField = ViewPager.class.getDeclaredField("mScroller");
//            mField.setAccessible(true);
//            mScroller = new BannerScroller(viewPager.getContext());
//            mScroller.setDuration(BannerConfig.DURATION);
//            mField.set(viewPager, mScroller);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//    public BannerTimer isAutoPlay(boolean isAutoPlay) {
//        this.isAutoPlay = isAutoPlay;
//        return this;
//    }
//
//    public BannerTimer setImageLoader(ImageLoaderInterface imageLoader) {
//        this.imageLoader = imageLoader;
//        return this;
//    }
//
//    public BannerTimer setDelayTime(int delayTime) {
//        this.delayTime = delayTime;
//        return this;
//    }
//
//    /**
//     * 设置播放的间隔时间
//     *
//     * @param transformer
//     * @return
//     */
//    public BannerTimer setBannerAnimation(Class<? extends ViewPager.PageTransformer> transformer) {
//        try {
//            setPageTransformer(false, transformer.newInstance());
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return this;
//    }
//
//    public BannerTimer setPageTransformer(boolean reverseDrawingOrder, ViewPager.PageTransformer transformer) {
//        viewPager.setPageTransformer(reverseDrawingOrder, transformer);
//        return this;
//    }
//
//
//    public BannerTimer setOffscreenPageLimit(int limit) {
//        if (viewPager != null) {
//            viewPager.setOffscreenPageLimit(limit);
//        }
//        return this;
//    }
//
//    public BannerTimer setViewPagerIsScroll(boolean isScroll) {
//        this.isScroll = isScroll;
//        return this;
//    }
//
//    public BannerTimer setImages(List<MediAddEntity> imageList) {
//        this.ListUrls = imageList;
//        this.count = ListUrls.size();
//        return this;
//    }
//
//    public void update(List<MediAddEntity> imageUrls) {
//        ListUrls.clear();
//        ListUrls.addAll(imageUrls);
//        count = ListUrls.size();
//        start();
//    }
//
//    public BannerTimer start() {
//        setImageList(ListUrls);
//        setData();
//        return this;
//    }
//
//    private void setImageList(List<MediAddEntity> imageUrls) {
//        if (imageUrls == null || imageUrls.size() <= 0) {
//            return;
//        }
//        imageViews.clear();
//        for (int i = 0; i <= count + 1; i++) {
//            ImageView imageView = null;
//            if (imageLoader != null) {
//                imageView = imageLoader.createImageView(context);
//            }
//            if (imageView == null) {
//                imageView = new ImageView(context);
//            }
//            imageView.setScaleType(ScaleType.FIT_XY);
//            imageViews.add(imageView);
//        }
//    }
//
//    private void setData() {
//        currentItem = 1;
//        if (adapter == null) {
//            adapter = new BannerPagerAdapter(context, ListUrls, imageViews, imageLoader);
//            viewPager.addOnPageChangeListener(this);
//            adapter.setBannerItemClickListener(taskPlayStateListener);
//        }
//        viewPager.setAdapter(adapter);
//        viewPager.setFocusable(true);
//        viewPager.setCurrentItem(1);
//        if (isScroll && count > 1) {
//            viewPager.setScrollable(true);
//        } else {
//            viewPager.setScrollable(false);
//        }
//        if (isAutoPlay) {
//            startAutoPlay("setData");
//        }
//    }
//
//    private void startTimerToPlay(long timerDelay, String tag) {
//        cacelAutoPlauTimer();
//        aotuPlaytimer = new Timer();
//        autoPlayTimerTask = new AutoPlayTimerTask();
//        aotuPlaytimer.schedule(autoPlayTimerTask, timerDelay);
//    }
//
//    private void cacelAutoPlauTimer() {
//        if (autoPlayTimerTask != null) {
//            autoPlayTimerTask.cancel();
//        }
//        if (aotuPlaytimer != null) {
//            aotuPlaytimer.cancel();
//        }
//        if (handlerBanner != null) {
//            handlerBanner.removeMessages(AUTO_TO_PLAY_MESSAGE);
//        }
//    }
//
//    AutoPlayTimerTask autoPlayTimerTask;
//    private Timer aotuPlaytimer;
//
//    class AutoPlayTimerTask extends TimerTask {
//
//        @Override
//        public void run() {
//            handlerBanner.sendEmptyMessage(AUTO_TO_PLAY_MESSAGE);
//        }
//    }
//
//    private static final int AUTO_TO_PLAY_MESSAGE = 564;
//    private Handler handlerBanner = new Handler() {
//        @Override
//        public void handleMessage(Message msg) {
//            super.handleMessage(msg);
//            switch (msg.what) {
//                case AUTO_TO_PLAY_MESSAGE:
//                    if (count == 1) {
//                        //一个任务多个节目，一张图片没有切换的过程
//                        if (mOnPageChangeListener == null) {
//                            return;
//                        }
//                        //如果只有一张图片，就停止滚动
//                        stopAutoPlay();
//                        mOnPageChangeListener.onPageSelected(-1);
//                    } else if (count > 1 && isAutoPlay) {
//                        goToNextViewPage();
//                    }
//                    break;
//            }
//        }
//    };
//
//
//    /**
//     * 播放下一页
//     */
//    private void goToNextViewPage() {
//        currentItem = currentItem % (count + 1) + 1;
//        if (currentItem == 1) {
//            //这里是正对最后一张和第一张之间的切换
////            MyLog.cdl("=========当前播放的位置000====" + currentItem);
//            viewPager.setCurrentItem(currentItem, false);
//            long duartionFirat = getRealPositionDelayTime(currentItem);
//            startTimerToPlay(duartionFirat, "goToNextViewPage currentItem == 1");
//        } else {
//            viewPager.setCurrentItem(currentItem);
//            int delayTime = getRealPositionDelayTime(currentItem);
//            startTimerToPlay(delayTime, "goToNextViewPage currentItem != 1");
//        }
//    }
//
//    public void startAutoPlay(String tag) {
//        cacelAutoPlauTimer();
//        if (ListUrls.size() > 1) {
//            delayTime = getDelayTime(0);
//        }
//        if (delayTime < 3000) {
//            delayTime = 3000;
//        }
//        startTimerToPlay(delayTime, tag);
//    }
//
//
//    /**
//     * 暂停
//     */
//    public void pauseDisplayView() {
//        cacelAutoPlauTimer();
//    }
//
//    /**
//     * 恢复播放
//     */
//    public void resumePlayView() {
//        startAutoPlay("==resumePlayView");
//    }
//
//    public void stopAutoPlay() {
//        cacelAutoPlauTimer();
//    }
//
//    public void releaseBanner(String tag) {
//        try {
//            count = 0;
//            ListUrls.clear();
//            cacelAutoPlauTimer();
//        } catch (Exception e) {
//        }
//    }
//
//
//    /**
//     * 返回真实的位置
//     *
//     * @param position
//     * @return 下标从0开始
//     */
//    public int getRealPositionDelayTime(int position) {
//        int delayTime = BannerConfig.TIME;
//        try {
//            int realPosition = 0;
//            if (position == 0) {
//                realPosition = ListUrls.size() - 1;
//            } else if (position == ListUrls.size() + 1) {
//                realPosition = 0;
//            } else {
//                realPosition = position - 1;
//            }
//            MediAddEntity mediAddEntity = ListUrls.get(realPosition);
//            String timeDelay = mediAddEntity.getPlayParam().trim();
//            if (timeDelay == null || timeDelay.length() < 1) {
//                return delayTime;
//            }
//            delayTime = Integer.parseInt(timeDelay);
//            if (delayTime < 5) {
//                delayTime = 5;
//            }
//            delayTime = delayTime * 1000;
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return delayTime;
//    }
//
//
//    /**
//     * 获取间隔时间
//     *
//     * @param position 传递当前播放的位置
//     * @return 返回间隔的时间
//     */
//    public int getDelayTime(int position) {
//        int delayTime = BannerConfig.TIME;
//        if (ListUrls == null || ListUrls.size() < 1) {
//            return delayTime;
//        }
//        if (position > ListUrls.size() - 1) {
//            position = 0;
//        }
//        MediAddEntity mediAddEntity = ListUrls.get(position);
//        String timeDelay = mediAddEntity.getPlayParam().trim();
//        if (timeDelay == null || timeDelay.length() < 1) {
//            return delayTime;
//        }
//        delayTime = Integer.parseInt(timeDelay);
//        if (delayTime < 3) {
//            delayTime = 3;
//        }
//        delayTime = delayTime * 1000;
//        return delayTime;
//    }
//
//    @Override
//    public boolean dispatchTouchEvent(MotionEvent ev) {
//        if (isAutoPlay) {
//            int action = ev.getAction();
//            if (action == MotionEvent.ACTION_UP || action == MotionEvent.ACTION_CANCEL || action == MotionEvent
//                    .ACTION_OUTSIDE) {
//                if (!isLongClick)
//                    startAutoPlay("dispatchTouchEvent");
//            } else if (action == MotionEvent.ACTION_DOWN) {
//                isLongClick = false;
//                stopAutoPlay();
//            }
//        }
//        return super.dispatchTouchEvent(ev);
//    }
//
//    @Override
//    public boolean dispatchKeyEvent(KeyEvent event) {
//        int action = event.getAction();
//        int keyCode = event.getKeyCode();
//        if (isAutoPlay) {
//            if (keyCode == KeyEvent.KEYCODE_DPAD_LEFT || keyCode == KeyEvent.KEYCODE_DPAD_RIGHT) {
//                switch (action) {
//                    case KeyEvent.ACTION_DOWN:
//                        stopAutoPlay();
//                        break;
//                    case KeyEvent.ACTION_UP:
//                        startAutoPlay("dispatchKeyEvent");
//                        break;
//                }
//            }
//        }
//        if (event.getAction() == KeyEvent.ACTION_DOWN) {
//            switch (event.getKeyCode()) {
//                case KeyEvent.KEYCODE_DPAD_LEFT:
//                case KeyEvent.KEYCODE_DPAD_RIGHT:
//                    //handled = arrowScroll(FOCUS_RIGHT);
//                    if (currentItem == count + 1) {
//                        viewPager.setCurrentItem(1, false);
//                    } else if (currentItem == 0) {
//                        viewPager.setCurrentItem(count, false);
//                    }
//                    break;
//            }
//        }
//        return super.dispatchKeyEvent(event);
//
//    }
//
//    TaskPlayStateListener taskPlayStateListener;
//
//
//    public void setBannerItemClickListener(TaskPlayStateListener taskPlayStateListener) {
//        this.taskPlayStateListener = taskPlayStateListener;
//    }
//
//    @Override
//    public void onPageScrollStateChanged(int state) {
//        if (mOnPageChangeListener != null) {
//            mOnPageChangeListener.onPageScrollStateChanged(state);
//        }
//        currentItem = viewPager.getCurrentItem();
//        if (state == ViewPager.SCROLL_STATE_IDLE) { //空闲状态
//
//        } else if (state == ViewPager.SCROLL_STATE_DRAGGING) {   //拖曳
//            if (currentItem == count + 1) {
//                viewPager.setCurrentItem(1, false);
//            } else if (currentItem == 0) {
//                viewPager.setCurrentItem(count, false);
//            }
//        }
//    }
//
//
//    @Override
//    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
//        if (mOnPageChangeListener != null) {
//            mOnPageChangeListener.onPageScrolled(position, positionOffset, positionOffsetPixels);
//        }
//
//    }
//
//    int lastPostitoin = -1;
//
//    @Override
//    public void onPageSelected(int position) {
//        if (mOnPageChangeListener != null) {
//            mOnPageChangeListener.onPageSelected(position);
//        }
//        //如果只有一张图片，不执行动画
//        if (ListUrls == null || ListUrls.size() < 2) {
//            return;
//        }
////        LogUtil.e("=========kermitye nextAnimal position: " + position + " / " + lastPostitoin);
////        if (position != lastPostitoin) {
////            lastPostitoin = position;
////            LogUtil.e("=========kermitye onPageScrolled: " + System.currentTimeMillis());
////            try {
////                int realPosition = getRealPositionDelayTime(currentItem);
////                String animalTrans = ListUrls.get(realPosition).getCartoon();
////                LogUtil.e("=========kermitye onPageScrolled=====当前动画属性===" + animalTrans);
////                Class<? extends ViewPager.PageTransformer> transformer = TaskDealUtil.getCartonChange(animalTrans);
////                viewPager.setPageTransformer(true, transformer.newInstance());
////            } catch (Exception e) {
////                e.printStackTrace();
////            }
////        }
//    }
//
//    public void setOnPageChangeListener(ViewPager.OnPageChangeListener onPageChangeListener) {
//        mOnPageChangeListener = onPageChangeListener;
//    }
//
//    /**
//     * 快进或者快退
//     *
//     * @param b true  快进
//     *          false 快退
//     */
//    public void moveViewForward(boolean b) {
//
//    }
//}
