package com.ys.acsdev.task.banner.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.viewpager.widget.PagerAdapter;
import com.bumptech.glide.Glide;
import com.ys.acsdev.task.banner.code.loader.ImageLoaderInterface;
import com.ys.acsdev.task.layout.MediAddEntity;
import com.ys.acsdev.task.layout.TaskPlayStateListener;

import java.util.ArrayList;
import java.util.List;

public class BannerPagerAdapter extends PagerAdapter {

    private List<MediAddEntity> imageUrls;
    private List<String> listString;
    private List<View> imageViews;
    private ImageLoaderInterface imageLoader;
    Context context;

    public BannerPagerAdapter(Context context, List<MediAddEntity> imageUrls, List<View> imageViews,
                              ImageLoaderInterface imageLoader) {
        this.context = context;
        this.imageUrls = imageUrls;
        this.imageViews = imageViews;
        this.imageLoader = imageLoader;
        listString = new ArrayList<String>();
        if (imageUrls == null || imageUrls.size() < 1) {
            return;
        }
        for (int i = 0; i < imageUrls.size(); i++) {
            listString.add(imageUrls.get(i).getUrl());
        }
    }

    @Override
    public int getCount() {
        return imageViews.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        ImageView view = (ImageView) imageViews.get(position);
        if (imageUrls != null && imageUrls.size() > 0) {
            int showPosition = getRealPosition(position);
            String url = imageUrls.get(showPosition).getUrl();
            if (imageLoader != null) {
                imageLoader.displayImage(context, url, view);
            }
        }

        container.addView(view);
        return view;
    }


    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        View view = (View) object;
        if (view == null) {
            return;
        }
        Glide.with(context).clear((View) object);
        container.removeView(view);
    }

    TaskPlayStateListener taskPlayStateListener;

    public void setBannerItemClickListener(TaskPlayStateListener taskPlayStateListener) {
        this.taskPlayStateListener = taskPlayStateListener;
        notifyDataSetChanged();
    }

    /**
     * 返回真实的位置
     *
     * @param position
     * @return 下标从0开始
     */
    public int getRealPosition(int position) {
        int realPosition = 0;
        if (position == 0) {
            realPosition = imageUrls.size() - 1;
        } else if (position == imageUrls.size() + 1) {
            realPosition = 0;
        } else {
            realPosition = position - 1;
        }
        return realPosition;
    }

}