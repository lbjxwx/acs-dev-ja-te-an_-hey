package com.ys.acsdev.task.banner.code;


public class BannerConfig {

    /**
     * banner
     */
    public static final int TIME = 10 * 1000;    //默认播放时间
    public static final int DURATION = 800;  //动画切换的时间
    public static final boolean IS_AUTO_PLAY = true;
    public static final boolean IS_SCROLL = true;


}
