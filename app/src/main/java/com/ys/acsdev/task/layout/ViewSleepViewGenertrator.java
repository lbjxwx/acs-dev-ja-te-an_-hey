package com.ys.acsdev.task.layout;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.signature.ObjectKey;
import com.ys.acsdev.R;
import com.ys.acsdev.util.SimpleDateUtil;

/***
 * 睡眠等待得View
 */
public class ViewSleepViewGenertrator extends Generator {

    View view;
    TextView tv_date;
    Context context;

    public ViewSleepViewGenertrator(Context context, int startX, int StartY, int width, int height) {
        super(context, startX, StartY, width, height);
        this.context = context;
        view = LayoutInflater.from(context).inflate(R.layout.view_sleep, null);
        initViewSleep();
    }


    ImageView iv_app_icon;

    private void initViewSleep() {
        iv_app_icon = (ImageView) view.findViewById(R.id.iv_app_icon);
        Glide.with(context)
                .load(R.mipmap.ic_bg)
                .apply(new RequestOptions()
                        .skipMemoryCache(true)    //禁止Glide内存缓存
                        .diskCacheStrategy(DiskCacheStrategy.NONE)    //不缓存资源
                        .signature(new ObjectKey(System.currentTimeMillis()))
                ).into(iv_app_icon);
        tv_date = (TextView) view.findViewById(R.id.tv_date);
        tv_date.setText(SimpleDateUtil.getCurrentTimeJSTHm());
    }

    @Override
    public View getView() {
        return view;
    }

    @Override
    public void clearMemory(String tag) {
        if (iv_app_icon != null) {
            Glide.with(context).clear(iv_app_icon);  //防止缓存，不刷新
        }
    }

    @Override
    public void updateView(Object object) {
        String timeShow = SimpleDateUtil.getCurrentTimeJSTHm();
        if (tv_date != null) {
            tv_date.setText(timeShow);
        }
    }

    @Override
    public void playComplet() {

    }
}
