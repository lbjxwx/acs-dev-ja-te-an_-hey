package com.ys.acsdev.task.banner.code.loader;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;

import java.io.Serializable;


public interface ImageLoaderInterface<T extends View> extends Serializable {

    void displayImage(Context context, Object path, ImageView imageView);

    ImageView createImageView(Context context);
}
