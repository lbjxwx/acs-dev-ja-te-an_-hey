package com.ys.acsdev.task.layout;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;

import androidx.viewpager.widget.ViewPager;

import com.ys.acsdev.R;
import com.ys.acsdev.task.banner.BannerUtil;
import com.ys.acsdev.task.banner.code.Banner;
import com.ys.acsdev.task.banner.code.BannerConfig;
import com.ys.acsdev.task.util.GlideCacheUtil;
import com.ys.acsdev.util.LogUtil;

import java.util.List;

/***
 * 加载图片资源
 */
public class ViewImageGenertrator extends Generator implements
        ViewPager.OnPageChangeListener {

    Context context;
    View view;
    List<MediAddEntity> imageList;
    int width;
    int height;
    String taskId;


    public ViewImageGenertrator(Context context, int startX, int StartY, int width, int height, List<MediAddEntity> imageList) {
        super(context, startX, StartY, width, height);
        this.width = width;
        this.height = height;
        this.imageList = imageList;
        view = LayoutInflater.from(context).inflate(R.layout.view_image, null);
    }

    Banner banner;
    BannerUtil bannerUtil;

    private void initView(View view) {

        banner = (Banner) view.findViewById(R.id.banner_image);
        try {
            if (bannerUtil != null) {
                bannerUtil.stopPlay();
                bannerUtil = null;
            }
            bannerUtil = new BannerUtil(context, banner, this, listener);
            bannerUtil.setPlayListPic(imageList);
            bannerUtil.startPlay();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void updateView(Object object) {
        LogUtil.cdl("=====view初始化===图片初始化====");
        initView(view);
    }

    @Override
    public void playComplet() {
        if (bannerUtil == null) {
            return;
        }
        if (listener != null) {
            listener.playComplete();
        }
    }

    public void toUpdatePlayNum(int position) {
        try {
            LogUtil.e("===============PlayStateChangeListener: toUpdatePlayNum image / " + (listener == null), "");
            if (listener == null) {
                return;
            }
            if (imageList == null || imageList.size() < 1) {
                LogUtil.e("===============PlayStateChangeListener: toUpdatePlayNum image is null", "");
                return;
            }
            if (position == 0) {
                position = imageList.size() - 1;
            } else if (position > imageList.size()) {
                position = 0;
            } else if (position == 1) { //防止多次提交
                return;
            } else {
                position = position - 1;
            }
            String midId = imageList.get(position).getMidId();

            LogUtil.e("===============PlayStateChangeListener: toUpdatePlayNum", "");
            taskId = imageList.get(position).taskId;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public View getView() {
        return view;
    }

    @Override
    public void clearMemory(String tag) {
        if (imageList != null) {
            imageList.clear();
        }
        if (bannerUtil != null) {
            bannerUtil.stopPlay();
            bannerUtil = null;
        }
        try {
            GlideCacheUtil.getInstance().clearImageMemoryCache(context);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
    }

    int lastPosition = 0;

    @Override
    public void onPageSelected(int position) {
        try {
            if (position > -1) {
                toUpdatePlayNum(position);
            }
            //只有一张图片,直接回调回去
            if (position < 0) {
//                MyLog.banner("===playComplet==只有一张图片============");
                toUpdatePlayNum(0);
                playComplet();
                lastPosition = position;
                return;
            }
//            MyLog.banner("=====onPageSelected: " + position + " / " + lastPosition + " / " + imageList.size());
            if (position < lastPosition || position > imageList.size()) {
//                MyLog.banner("=====playComplet==onPageSelected:===多张图片=== " + +position + " / " + lastPosition + " / " + imageList.size());
                playComplet();
            }
            lastPosition = position;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {
    }

}
