//package com.ys.acsdev.task.layout;
//
//import android.content.Context;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.widget.ImageView;
//import android.widget.TextView;
//
//import com.bumptech.glide.Glide;
//import com.bumptech.glide.load.engine.DiskCacheStrategy;
//import com.bumptech.glide.request.RequestOptions;
//import com.bumptech.glide.signature.ObjectKey;
//import com.ys.acsdev.R;
//import com.ys.acsdev.commond.AppInfo;
//import com.ys.acsdev.util.LogUtil;
//import com.ys.acsdev.util.SimpleDateUtil;
//import com.ys.entity.FileEntity;
//
//import java.io.File;
//import java.util.ArrayList;
//import java.util.List;
//
///***
// * 睡眠等待得View
// */
//public class ViewSleepViewGenertratorBack extends Generator {
//
//    View view;
//    TextView tv_time;
//    TextView tv_date;
//    Context context;
//
//    public ViewSleepViewGenertratorBack(Context context, int startX, int StartY, int width, int height) {
//        super(context, startX, StartY, width, height - 20);
//        this.context = context;
//        view = LayoutInflater.from(context).inflate(R.layout.view_sleep, null);
//        initViewSleep();
//    }
//
//    VideoViewBitmap videoView;
//    ImageView iv_app_icon;
//
//    private void initViewSleep() {
//        iv_app_icon = (ImageView) view.findViewById(R.id.iv_app_icon);
//        String playUrl = AppInfo.BASE_VIDEO_PATH + "/sleep_media.avi";
//        File file = new File(playUrl);
//        if (!file.exists()) {
//            int defaultLogo = R.mipmap.app_icon;
//            File fileShow = new File(AppInfo.LOGO_PATH);
//            Glide.with(context)
//                    .load(fileShow)
//                    .apply(new RequestOptions()
//                            .error(defaultLogo)
//                            .skipMemoryCache(true)    //禁止Glide内存缓存
//                            .diskCacheStrategy(DiskCacheStrategy.NONE)    //不缓存资源
//                            .signature(new ObjectKey(System.currentTimeMillis()))
//                    ).into(iv_app_icon);
//            return;
//        }
//        List<MediAddEntity> playUrlList = new ArrayList<MediAddEntity>();
//        playUrlList.add(new MediAddEntity(playUrl, "123", FileEntity.STYLE_FILE_VIDEO, "123"));
//
//        tv_time = (TextView) view.findViewById(R.id.tv_time);
//        tv_date = (TextView) view.findViewById(R.id.tv_date);
//        tv_date.setText(SimpleDateUtil.getDateSingle());
//
//        videoView = (VideoViewBitmap) view.findViewById(R.id.video_view);
//        videoView.setVideoClickListen(listener);
//        videoView.setVideoPlayListener(new VideoPlayListener() {
//
//            @Override
//            public void initOver() {
//                videoView.setPlayList(playUrlList);
//            }
//
//            @Override
//            public void playCompletion(String tag) {
//                LogUtil.cdl("====视频列表播放完毕了==这里回调====" + tag);
//                playComplet();
//            }
//
//            @Override
//            public void playCompletionSplash(int position, int playTime) {
//                //每次播放完毕都会调用这里
//            }
//
//            @Override
//            public void playError(String errorDesc) {
//                LogUtil.cdl("error==startToPlay==" + errorDesc);
////                ErrorToastView.getInstance().Toast(context, errorDesc);
//            }
//
//            @Override
//            public void reStartPlayProgram(String errorDesc) {
//                LogUtil.cdl("error==reStartPlayProgram==" + errorDesc);
//                if (listener != null) {
//                    listener.reStartPlayProgram(errorDesc);
//                }
//            }
//        });
//
//    }
//
//    @Override
//    public View getView() {
//        return view;
//    }
//
//    @Override
//    public void clearMemory(String tag) {
//        if (videoView != null) {
//            videoView.clearMemory();
//        }
//        if (iv_app_icon != null) {
//            Glide.with(context).clear(iv_app_icon);  //防止缓存，不刷新
//        }
//    }
//
//    @Override
//    public void updateView(Object object) {
//        String timeShow = SimpleDateUtil.getCurrentTimeJSTHm();
//        if (tv_time != null) {
//            tv_time.setText(timeShow + "");
//        }
//    }
//
//    @Override
//    public void playComplet() {
//
//    }
//}
