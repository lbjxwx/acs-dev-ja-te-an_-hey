//package com.ys.acsdev.task;
//
//import android.app.Service;
//import android.content.BroadcastReceiver;
//import android.content.Context;
//import android.content.Intent;
//import android.content.IntentFilter;
//import android.os.IBinder;
//
//import androidx.annotation.Nullable;
//
//import com.ys.acsdev.api.AppConfig;
//import com.ys.acsdev.commond.AppInfo;
//import com.ys.acsdev.runnable.down.DownFileEntity;
//import com.ys.acsdev.runnable.down.DownRunnable;
//import com.ys.acsdev.runnable.down.DownStateListener;
//import com.ys.acsdev.service.AcsService;
//import com.ys.acsdev.task.entity.SourceEntity;
//import com.ys.acsdev.task.entity.TaskEntity;
//import com.ys.acsdev.util.CodeUtil;
//import com.ys.acsdev.util.LogUtil;
//import com.ys.acsdev.util.NetWorkUtils;
//import com.ys.acsdev.util.SpUtils;
//import com.ys.acsdev.util.SystemManagerUtil;
//import com.ys.ysim.task.model.TaskDealListener;
//import com.ys.ysim.task.model.TaskModel;
//import com.ys.ysim.task.model.TaskModelmpl;
//
//import org.jetbrains.annotations.NotNull;
//
//import java.io.File;
//import java.util.ArrayList;
//import java.util.List;
//import java.util.Random;
//
//public class TaskWorkService extends Service {
//
//    public static TaskWorkService mInstance;
//
//    public static TaskWorkService getInstance() {
//        if (mInstance == null) {
//            synchronized (TaskWorkService.class) {
//                if (mInstance == null) {
//                    mInstance = new TaskWorkService();
//                }
//            }
//        }
//        return mInstance;
//    }
//
//    private BroadcastReceiver receiver = new BroadcastReceiver() {
//        @Override
//        public void onReceive(Context context, Intent intent) {
//            String action = intent.getAction();
//            if (action.equals(AppInfo.RECEIVE_MESSAGE_TO_REQUEST_TASK)) {
//                String desc = intent.getStringExtra(AppInfo.RECEIVE_MESSAGE_TO_REQUEST_TASK);
//                LogUtil.task("==============接收到广播，去请求任务信息===" + desc);
//                requestTaskFromServer();
//            }
//        }
//    };
//
//    @Override
//    public void onCreate() {
//        super.onCreate();
//        initOther();
//        initReceiver();
//    }
//
//
//    private void initReceiver() {
//        IntentFilter filter = new IntentFilter();
//        filter.addAction(AppInfo.RECEIVE_MESSAGE_TO_REQUEST_TASK);
//        registerReceiver(receiver, filter);
//    }
//
//    TaskModel taskModel;
//
//    private void initOther() {
//        if (taskModel == null) {
//            taskModel = new TaskModelmpl();
//        }
//    }
//
//    private void requestTaskFromServer() {
//        if (SpUtils.getWorkModel() == AppInfo.MODEL_STAND_ALONE) {
//            return;
//        }
//        try {
//            if (!NetWorkUtils.isNetworkConnected(TaskWorkService.this)) {
//                startToPlayViewActivity("没有网络，不去请求任务信息，直接取播放界面");
//                return;
//            }
//            initOther();
//            taskModel.getTaskFromServer(new TaskDealListener() {
//                @Override
//                public void checkFileIsFullBackView(boolean isSuccess, @org.jetbrains.annotations.Nullable ArrayList<SourceEntity> sourceList, @NotNull String errorDesc) {
//
//                }
//
//                @Override
//                public void backTaskFromDbToView(boolean isSuccess, @org.jetbrains.annotations.Nullable TaskEntity taskEntity, @NotNull String errorDesc) {
//
//                }
//
//                @Override
//                public void backTaskInfoToView(boolean isSuccess, @NotNull String errorDesc) {
//                    LogUtil.task("===========服务器请求成功,去本地获取===" + errorDesc);
//                    getPlayTaskFromDb();
//                }
//            });
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//    /***
//     * 从数据库里面检查文件
//     */
//    private void getPlayTaskFromDb() {
//        initOther();
//        taskModel.getPlayTaskFromDb(new TaskDealListener() {
//            @Override
//            public void checkFileIsFullBackView(boolean isSuccess, ArrayList<SourceEntity> sourceList, String errorDesc) {
//
//            }
//
//            @Override
//            public void backTaskFromDbToView(boolean isSuccess, TaskEntity taskEntity, String errorDesc) {
//                LogUtil.task("==backTaskFromDbToView数据返回=" + isSuccess + " / " + errorDesc + " / " + (taskEntity == null));
//                if (!isSuccess) {
//                    startToPlayViewActivity("===检查数据库，没有数据才会跳转===" + errorDesc);
//                    return;
//                }
//                if (taskEntity == null) {
//                    startToPlayViewActivity("===检查数据库，没有数据才会跳转===" + errorDesc);
//                    return;
//                }
//                checkTaskDownFileUtil(taskEntity);
//            }
//
//            @Override
//            public void backTaskInfoToView(boolean isSuccess, @NotNull String errorDesc) {
//
//            }
//        });
//    }
//
//    /**
//     * 检查是否有需要下载得文件
//     */
//    private void checkTaskDownFileUtil(TaskEntity taskEntity) {
//        initOther();
//        taskModel.checkSourceListIsExict(taskEntity, new TaskDealListener() {
//
//            @Override
//            public void backTaskInfoToView(boolean isSuccess, String errorDesc) {
//
//            }
//
//            @Override
//            public void backTaskFromDbToView(boolean isSuccess, TaskEntity taskEntity, String errorDesc) {
//
//            }
//
//            @Override
//            public void checkFileIsFullBackView(boolean isSuccess, ArrayList<SourceEntity> sourceList, String errorDesc) {
//                LogUtil.task("==checkFileIsFullBackView=" + isSuccess + " / " + errorDesc);
//                //没有需要下载得文件，直接去播放
//                if (!isSuccess) {
//                    startToPlayViewActivity("没有需要下载得文件，checkTaskDownFileUtil");
//                    return;
//                }
//                if (sourceList == null || sourceList.size() < 1) {
//                    startToPlayViewActivity("没有需要下载得文件，checkTaskDownFileUtil");
//                    return;
//                }
//                //去下载文件
//                toDownFileList(sourceList);
//            }
//        });
//    }
//
//    List<SourceEntity> downFileList = new ArrayList<SourceEntity>();
//    List<SourceEntity> downFileCacheList = new ArrayList<SourceEntity>();
//    DownRunnable downTaskrunnable = null;
//    boolean isDownIng = false;
//    long downloadAllFileSize = 0;
//
//    private void toDownFileList(List<SourceEntity> sourceList) {
//        LogUtil.task("========准备开始下载任务了======");
//        this.downFileList = sourceList;
//        this.downFileCacheList = sourceList;
//        //先停止下载
//        cacelDownTask();
//        jujleDownFileSize();
//        downFileOneByOne();
//        startToPlayViewActivity("先进行下载，在进行播放操作");
//    }
//
//    /***
//     * 获取当前下载状态
//     * @return
//     */
//    public boolean getDownStatues() {
//        return isDownIng;
//    }
//
//    /**
//     * 一个接一个得下载
//     */
//    private void downFileOneByOne() {
//        if (downFileList == null || downFileList.size() < 1) {
//            startToPlayViewActivity("========全部下载完毕===downTaskListOneByOne");    //准备去播放
//            SystemManagerUtil.syncSdcard();
//            return;
//        }
//        SourceEntity sourceEntity = downFileList.get(0);
//        //加入获取得任务==null.直接跳过当前，去下载下一个
//        if (sourceEntity == null) {
//            goToDownNextFile();
//            return;
//        }
//        String downUrl = sourceEntity.getPath();
//        if (!downUrl.contains("http")) {      //局域网需要拼接网址
//            downUrl = AppInfo.getBaseUrl() + downUrl;
//        }
//        String fileName = sourceEntity.getName();
//        String savePath = AppInfo.BASE_TASK_PATH + "/" + fileName;
//        File taskDir = new File(AppInfo.BASE_TASK_PATH);
//        if (!taskDir.exists()) {
//            taskDir.mkdirs();
//        }
//        LogUtil.task("=========剩余下载得文件数量===" + downFileList.size());
//        if (downTaskrunnable == null) {
//            downTaskrunnable = new DownRunnable();
//        }
//        isDownIng = true;
//        LogUtil.task("=========下载得文件路径===" + downUrl + " / " + savePath);
//        downTaskrunnable.setDownInfo(downUrl, savePath, fileName, new DownStateListener() {
//            @Override
//            public void downStateInfo(DownFileEntity entity) {
//                if (entity == null) {
//                    return;
//                }
//                entity.setTaskId(sourceEntity.getTaskId());
//                int downStatues = entity.getDownState();
//                int progress = entity.getProgress();
//                int downSpeed = entity.getDownSpeed();
//                jujleDownFileLengthToWeb(downSpeed);
//                LogUtil.task("========parserJsonOver=progress===" + progress + " /speed=" + downSpeed + " / " + downStatues);
//                if (downStatues == DownFileEntity.DOWN_STATE_SUCCESS) {
//                    isDownIng = false;
//                    LogUtil.task("========parserJsonOver=progress==下载成功= ");
//                    goToDownNextFile();
//                } else if (downStatues == DownFileEntity.DOWN_STATE_FAIED) {
//                    isDownIng = false;
//                    LogUtil.task("========parserJsonOver=progress==下载失败==" + entity.getDesc());
//                    goToDownNextFile();
//                }
//            }
//        });
//        downTaskrunnable.setIsDelFile(true);
//        downTaskrunnable.setLimitDownSpeed(AppConfig.DOWN_SPEED_LIMIT);
//        AcsService.getInstance().executor(downTaskrunnable);
//    }
//
//    private void goToDownNextFile() {
//        if (downFileList != null && downFileList.size() > 0) {
//            downFileList.remove(0);
//        }
//        downFileOneByOne();
//    }
//
//    //计算下载文件的总大小
//    private void jujleDownFileSize() {
//        if (downFileList == null || downFileList.size() < 1) {
//            return;
//        }
//        downloadAllFileSize = 0;
//        for (int i = 0; i < downFileList.size(); i++) {
//            SourceEntity sourceEntity = downFileList.get(i);
//            long fileSize = sourceEntity.getSize();
//            downloadAllFileSize += fileSize;
//        }
//    }
//
//    /**
//     * 提交下载进度得方法
//     */
//    private void jujleDownFileLengthToWeb(int speed) {
//        try {
//            if (!NetWorkUtils.isNetworkConnected(TaskWorkService.this)) {
//                LogUtil.task("====设备不在线，中止操作====");
//                return;
//            }
//            if (downFileCacheList == null || downFileCacheList.size() < 1) {
//                return;
//            }
//            String taskId = downFileCacheList.get(0).getTaskId();
//            if (taskId == null || taskId.length() < 2) {
//                return;
//            }
//            long hanDownSize = 0;
//            for (SourceEntity sourceEntity : downFileCacheList) {
//                String name = sourceEntity.getName();
//                String savePath = AppInfo.BASE_TASK_PATH + "/" + name;
//                File file = new File(savePath);
//                if (file.exists()) {
//                    long filSize = file.length();
//                    hanDownSize += filSize;
//                }
//            }
//            int progress = (int) (hanDownSize * 100 / downloadAllFileSize);
//            if (progress < 1) {
//                progress = 1;
//            }
//            int randomNum = new Random().nextInt(15);
//            int showDownSpeed = speed - randomNum;
//            if (showDownSpeed < 0) {
//                showDownSpeed = 0;
//            }
//            LogUtil.task("===定时检测进度==" + taskId + " / " + progress + " / " + showDownSpeed + " / " + hanDownSize + " / " + downloadAllFileSize);
//            AcsService.getInstance().updateTaskDoanProgress(CodeUtil.getUniquePsuedoID(),
//                    taskId + "",
//                    showDownSpeed + "",
//                    progress
//            );
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//    /**
//     * 取消下载
//     */
//    private void cacelDownTask() {
//        if (downTaskrunnable != null) {
//            downTaskrunnable.stopDown();
//        }
//    }
//
//
//    /**
//     * 检测完毕，去播放界面
//     */
//    private void startToPlayViewActivity(String tag) {
//        try {
//            LogUtil.task("========全部下载完毕了，进入播放界面=" + tag);
//            cacelDownTask();
//            LogUtil.task("界面在前台，直接发送广播");
//            sendBroadToView(AppInfo.DOWN_TASK_SUCCESS);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//    private void sendBroadToView(String action) {
//        try {
//            Intent intent = new Intent();
//            intent.setAction(action);
//            sendBroadcast(intent);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//    @Override
//    public void onDestroy() {
//        super.onDestroy();
//        if (receiver != null) {
//            unregisterReceiver(receiver);
//        }
//        if (isDownIng) {
//            downTaskrunnable.stopDown();
//        }
//    }
//
//    @Nullable
//    @Override
//    public IBinder onBind(Intent intent) {
//        return null;
//    }
//
//}
