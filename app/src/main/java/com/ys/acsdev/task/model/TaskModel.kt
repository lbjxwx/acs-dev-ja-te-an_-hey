package com.ys.ysim.task.model

import com.ys.acsdev.task.entity.TaskEntity


interface TaskModel {

    /**
     * 获取任务
     */
    fun getTaskFromServer(listener: TaskDealListener)

    /**
     * 聪慧本地数据库中获取数据
     */
    fun getPlayTaskFromDb(listener: TaskDealListener)

    fun checkSourceListIsExict(taskEntity: TaskEntity, listener: TaskDealListener)
}