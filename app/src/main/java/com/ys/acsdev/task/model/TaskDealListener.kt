package com.ys.ysim.task.model

import com.ys.acsdev.task.entity.SourceEntity
import com.ys.acsdev.task.entity.TaskEntity
import java.util.ArrayList

interface TaskDealListener {


    fun checkFileIsFullBackView(isSuccess: Boolean, sourceList: ArrayList<SourceEntity>?, errorDesc: String)

    fun backTaskFromDbToView(isSuccess: Boolean, taskEntity: TaskEntity?, errorDesc: String)

    fun backTaskInfoToView(isSuccess: Boolean, errorDesc: String)

}