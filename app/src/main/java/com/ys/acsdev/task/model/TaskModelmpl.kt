package com.ys.ysim.task.model

import android.util.Log
import com.huiming.common.ext.schedulers
import com.yisheng.facerecog.net.HttpObserver
import com.ys.acsdev.api.ApiHelper
import com.ys.acsdev.commond.AppInfo
import com.ys.acsdev.task.db.DbTaskManagerJava
import com.ys.acsdev.task.entity.SourceEntity
import com.ys.acsdev.task.entity.TaskEntity
import com.ys.acsdev.util.*
import org.json.JSONArray
import org.json.JSONObject
import java.io.File

class TaskModelmpl : TaskModel {

    /**
     * 检测当前任务是否需要下载
     */
    override fun checkSourceListIsExict(taskEntity: TaskEntity, listener: TaskDealListener) {
        if (taskEntity == null) {
            listener.checkFileIsFullBackView(false, null, "传递得任务==null")
            return
        }
        var sourceList = taskEntity.list;
        if (sourceList == null || sourceList.size < 1) {
            listener.checkFileIsFullBackView(false, null, "该任务没有素材,需要检查")
            return
        }
        var needDownFile = ArrayList<SourceEntity>()
        for (sourEntity: SourceEntity in sourceList) {
            var filePath = sourEntity.path
            var fileLength = sourEntity.size
            var fileName = filePath.substring(filePath.lastIndexOf("/") + 1)
            var fileNameLocal = AppInfo.BASE_TASK_PATH + "/" + fileName

            var isExict = FileUtils.ifFileHasExicTask(fileNameLocal, "" + fileLength);
            if (!isExict) {
                LogUtil.cdl("===========文件需要下载====" + sourEntity.toString())
                needDownFile.add(sourEntity)
            }
        }
        listener.checkFileIsFullBackView(true, needDownFile, "返回下载集合success")
    }

    /**
     * 从数据库里面获取数据
     */
    override fun getPlayTaskFromDb(listener: TaskDealListener) {
        var taskEntityList = DbTaskManagerJava.getAllTaskFromDb()
        if (taskEntityList == null || taskEntityList.size < 1) {
            listener.backTaskFromDbToView(false, null, "当前没有任务")
            return
        }
        var taskEntity = taskEntityList[0]
        if (taskEntity == null) {
            listener.backTaskFromDbToView(false, null, "获取任务信息==NULL")
            return
        }
        var taskId = taskEntity.taskId
        var sourceList = DbTaskManagerJava.getSourceListByTaskId(taskId)
        if (sourceList == null || sourceList.size < 1) {
            listener.backTaskFromDbToView(false, null, "没有相关的素材")
            return
        }
        taskEntity.list = sourceList as ArrayList<SourceEntity>?
        listener.backTaskFromDbToView(true, taskEntity, "获取任务成功")
    }

    /**
     * 获取任务
     */
    override fun getTaskFromServer(listener: TaskDealListener) {
        var sn = CodeUtil.getUniquePsuedoID()
        ApiHelper.findByCommunityId(sn)
            .schedulers().subscribe(object : HttpObserver<String>() {
                override fun onSuccess(json: String?) {
                    LogUtil.task("==========请求成功===" + json);
                    if (json == null || json.length < 5) {
                        listener.backTaskInfoToView(false, "获取任务失败:data=null")
                        return
                    }
                    parsenerTaskJson(json, listener)
                }

                override fun onFailed(code: Int, msg: String) {
                    LogUtil.task("==========请求失败===" + msg);
                    listener.backTaskInfoToView(false, "获取任务失败: " + msg)
                }
            })
    }

    fun parsenerTaskJson(jsonDesc: String, listener: TaskDealListener) {
        try {
            var jsonObject = JSONObject(jsonDesc)
            var code = jsonObject.getInt("code")
            var msg = jsonObject.getString("msg")
            if (code != 0) {
                DbTaskManagerJava.deleteAllTaskInfo()
                listener.backTaskInfoToView(false, "Get Task Failed : " + msg)
                return
            }
            var data = jsonObject.getString("data")
            if (data == null || data.length < 5) {
                DbTaskManagerJava.deleteAllTaskInfo()
                listener.backTaskInfoToView(false, "Current No Task")
                return
            }
            DbTaskManagerJava.deleteAllTaskInfo()
            var jsonData = JSONObject(data)
            LogUtil.task("=============" + jsonData);
            var taskId = jsonData.getString("id")
            var taskName = jsonData.getString("taskName")
            var screenType = jsonData.getString("screenType")
            var sourceList = jsonData.getString("sourceList")
            var taskEntity = TaskEntity(taskId, taskName, screenType)
            var isSave = DbTaskManagerJava.saveTaskInfoToDb(taskEntity)
            LogUtil.task("====任务保存状态===" + isSave)
            if (isSave) {
                parsenerSourceEntity(taskId, sourceList, listener)
            } else {
                DbTaskManagerJava.deleTaskInfoByTask(taskEntity)
                listener.backTaskInfoToView(false, "保存任务失败，清理数据")
            }
        } catch (excepyion: Exception) {
            excepyion.printStackTrace()
        }
    }

    fun parsenerSourceEntity(taskId: String, sourceList: String?, listener: TaskDealListener) {
        if (sourceList == null || sourceList.length < 5) {
            listener.backTaskInfoToView(false, "没有素材需要下载")
            return
        }
        var jsonArray = JSONArray(sourceList);
        var taskNum = jsonArray.length();
        if (taskNum < 1) {
            listener.backTaskInfoToView(false, "没有素材需要下载")
            return
        }
        for (i in 0 until jsonArray.length()) {
            var jsonData: JSONObject = jsonArray.getJSONObject(i)
            var sourceId = jsonData.getString("id")
            var type = jsonData.getString("type")
            var name = jsonData.getString("name")
            var size = jsonData.getLong("size")
            var path = jsonData.getString("path")
            if (path.isNullOrEmpty() || "null" == path) {
                continue
            }
            var sourceEntity = SourceEntity(taskId, sourceId, type, name, size, path)
            var isSave = DbTaskManagerJava.saveSourceInfoToDb(sourceEntity)
//            LogUtil.db("====节目保存状态===" + isSave + " / " + sourceEntity.toString())
        }
        listener.backTaskInfoToView(true, "返回任务成功")
    }
}