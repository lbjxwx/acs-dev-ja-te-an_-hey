package com.ys.acsdev.task.view;

import android.widget.AbsoluteLayout;

public interface TaskPlayView {

    AbsoluteLayout getAbView();

    void showToastView(String taost);

}
