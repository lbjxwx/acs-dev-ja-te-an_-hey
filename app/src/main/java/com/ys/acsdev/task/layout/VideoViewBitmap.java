package com.ys.acsdev.task.layout;

import android.content.Context;
import android.graphics.SurfaceTexture;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.widget.RelativeLayout;

import com.ys.acsdev.R;

import java.util.ArrayList;
import java.util.List;

/**
 * 视频播放控件
 */
public class VideoViewBitmap extends RelativeLayout implements
        TextureView.SurfaceTextureListener {

    View view;
    private MediaPlayer mMediaPlayer;
    private Surface surface;
    private TextureView textureView;
    VideoPlayListener listener;
    List<MediAddEntity> playList = new ArrayList<MediAddEntity>();
    int currentPlayIndex = 0;
    Context context;

    /**
     * 直接播放，从0开始
     *
     * @param playUrlList
     */
    public void setPlayList(List<MediAddEntity> playUrlList) {
        setPlayListPosition(playUrlList, 0);
    }

    /**
     * 携带位置，从位置处开始播放
     *
     * @param playUrlList
     * @param playPosition
     */
    public void setPlayListPosition(List<MediAddEntity> playUrlList, int playPosition) {
        currentPlayIndex = playPosition;
        this.playList = playUrlList;
        if (playUrlList.size() < 1) {
            return;
        }
        final String currentPlayUrl = playList.get(currentPlayIndex).getUrl();
        startToPlay(currentPlayUrl);
    }

    public void setVideoPlayListener(VideoPlayListener listener) {
        this.listener = listener;
    }

    public VideoViewBitmap(Context context) {
        this(context, null);
    }

    public VideoViewBitmap(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public VideoViewBitmap(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        view = View.inflate(context, R.layout.view_video, null);
        initView(view);
        addView(view);
    }

    RelativeLayout rela_no_data;

    TaskPlayStateListener taskPlayStateListener;

    public void setVideoClickListen(TaskPlayStateListener taskPlayStateListener) {
        this.taskPlayStateListener = taskPlayStateListener;
    }

    private void initView(View view) {
        rela_no_data = (RelativeLayout) view.findViewById(R.id.rela_no_data);
        textureView = (TextureView) view.findViewById(R.id.textureview);
        textureView.setSurfaceTextureListener(this);//设置监听函数  重写4个方法
        textureView.setOnTouchListener(onTouchListener);
    }


    float downX, upX;
    private OnTouchListener onTouchListener = new OnTouchListener() {

        @Override
        public boolean onTouch(View v, MotionEvent motionEvent) {
            int action = motionEvent.getAction();
            switch (action) {
                case MotionEvent.ACTION_DOWN:
                    downX = motionEvent.getX();
                    break;
                case MotionEvent.ACTION_UP:
                    upX = motionEvent.getX();
                    if (downX - upX > 200) { //向左滑动
                        playNextVideo();
                    } else if (upX - downX > 200) { //向右滑动
                        playProVideo();
                    } else { //执行点击事件

                    }
                    break;
            }
            return true;
        }
    };


    public void startToPlay(String playUrl) {
        rela_no_data.setVisibility(View.GONE);
        try {
            if (mMediaPlayer == null) {
                mMediaPlayer = new MediaPlayer();
            }
            if (mMediaPlayer.isPlaying()) {
                mMediaPlayer.stop();
            }
            mMediaPlayer.reset();
            mMediaPlayer.setDataSource(playUrl);
            mMediaPlayer.setSurface(surface);
            mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            mMediaPlayer.setVolume(1.0f, 1.0f);
            mMediaPlayer.setOnPreparedListener(onPreparedListener);
            mMediaPlayer.setOnCompletionListener(onCompletionListener);
            mMediaPlayer.setOnErrorListener(onErrorListener);
            mMediaPlayer.prepareAsync();
        } catch (Exception e) {
//            if (listener != null) {
//                listener.playError(e.toString() + "/这里不影响播放");
//            }
//            errorToPlayNextVideo();
        }
    }

    private OnCompletionListener onCompletionListener = new OnCompletionListener() {

        @Override
        public void onCompletion(MediaPlayer mp) {
            onCompletionInfo();
        }
    };

    public void onCompletionInfo() {
        try {
            if (listener != null) {
                listener.playCompletionSplash(currentPlayIndex, playTimeCurrent);  //用来给Splach界面回调的
                if (currentPlayIndex == (playList.size() - 1) || currentPlayIndex > (playList.size() - 1)) {
                    listener.playCompletion("全部播放结束了，这里回调");

                }
            }
            playNextVideo();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    OnPreparedListener onPreparedListener = new OnPreparedListener() {
        @Override
        public void onPrepared(MediaPlayer mediaPlayer) {
            try {
                playTimeCurrent = (mediaPlayer.getDuration()) / 1000;
            } catch (Exception e) {
                e.printStackTrace();
            }
            mediaPlayer.start();
        }
    };

    /**
     * 播放异常回调
     */
    MediaPlayer.OnErrorListener onErrorListener = new MediaPlayer.OnErrorListener() {
        @Override
        public boolean onError(MediaPlayer mp, int what, int extra) {
            if (listener != null) {
                listener.playError("播放异常");
            }
            return false;
        }
    };

    //清理缓存得View
    public void removeCacheView() {
        try {
            if (mMediaPlayer != null) {
                mMediaPlayer.release();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void clearMemory() {
        try {
            if (mMediaPlayer == null) {
                return;
            }
            if (mMediaPlayer.isPlaying()) {
                mMediaPlayer.stop();
            }
            mMediaPlayer.release();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onSurfaceTextureAvailable(SurfaceTexture surfaceTexture, int i, int i1) {
        surface = new Surface(surfaceTexture);
        if (listener != null) {
            listener.initOver();
        }
    }

    @Override
    public void onSurfaceTextureSizeChanged(SurfaceTexture surfaceTexture, int i, int i1) {
    }

    @Override
    public boolean onSurfaceTextureDestroyed(SurfaceTexture surfaceTexture) {
        surface = null;
        return true;
    }

    @Override
    public void onSurfaceTextureUpdated(SurfaceTexture surfaceTexture) {
    }

    int playTimeCurrent = 15;

    /***
     * 播放下一个
     */
    private void playNextVideo() {
        try {
            if (playList == null || playList.size() < 1) {
                listener.reStartPlayProgram("播放下一个,出错了，这里执行播放完成,跳过这里");
                return;
            }
            currentPlayIndex++;
            if (currentPlayIndex > (playList.size() - 1)) {
                currentPlayIndex = 0;
            }
            String playUrl = playList.get(currentPlayIndex).getUrl();
            startToPlay(playUrl);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 播放上一个视频
     */
    private void playProVideo() {
        try {
            if (playList == null || playList.size() < 1) {
                listener.reStartPlayProgram("播放上一个,出错了，这里执行播放完成,跳过这里");
                return;
            }
            currentPlayIndex--;
            if (currentPlayIndex < 0) {
                currentPlayIndex = playList.size();
            }
            String playUrl = playList.get(currentPlayIndex).getUrl();
            startToPlay(playUrl);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}


