package com.ys.acsdev.task.layout;

import android.content.Context;
import android.view.View;

import com.ys.acsdev.R;
import com.ys.acsdev.util.LogUtil;

import java.util.List;

/***
 * 播放视频资源
 */
public class ViewVideoGenertrator extends Generator {

    VideoViewBitmap videoView = null;
    List<MediAddEntity> videoLists = null;
    View view;
    String screenPosition;
    String taskId;

    /***
     *
     * @param context
     * @param x
     * 起点坐标
     * @param y
     * 起点坐标
     * @param width
     * 控件宽度
     * @param height
     * 控件高度
     * @param videoLists
     * 素材集合
     */
    public ViewVideoGenertrator(Context context, int x, int y, int width, int height, List<MediAddEntity> videoLists, String screenPosition) {
        super(context, x, y, width, height);
        this.videoLists = videoLists;
        this.screenPosition = screenPosition;
        view = View.inflate(context, R.layout.view_video_play_new, null);
    }

    @Override
    public void updateView(Object object) {
        LogUtil.cdl("=====view初始化===视频初始化====");
        initView(view);
    }

    /***
     * 播放完毕
     */
    @Override
    public void playComplet() {
        if (listener != null) {
            listener.playComplete();
        }
    }

    int playTimeUpdate = 15;

    public void toUpdatePlayNum(int position) {
        LogUtil.e("===============PlayStateChangeListener: toUpdatePlayNum / " + (listener == null), "");
        if (listener == null) {
            return;
        }

        taskId = videoLists.get(position).taskId;
    }

    private void initView(View view) {
        videoView = (VideoViewBitmap) view.findViewById(R.id.video_view);
        videoView.setVideoClickListen(listener);
        videoView.setVideoPlayListener(new VideoPlayListener() {

            @Override
            public void initOver() {
                videoView.setPlayList(videoLists);
            }

            @Override
            public void playCompletion(String tag) {
                LogUtil.cdl("====视频列表播放完毕了==这里回调====" + tag);
                playComplet();
            }

            @Override
            public void playCompletionSplash(int position, int playTime) {  //每次播放完毕都会调用这里
                playTimeUpdate = playTime;
                toUpdatePlayNum(position);
            }

            @Override
            public void playError(String errorDesc) {
                LogUtil.cdl("error==startToPlay==" + errorDesc);
//                ErrorToastView.getInstance().Toast(context, errorDesc);
            }

            @Override
            public void reStartPlayProgram(String errorDesc) {
                LogUtil.cdl("error==reStartPlayProgram==" + errorDesc);
                if (listener != null) {
                    listener.reStartPlayProgram(errorDesc);
                }
            }
        });
    }

    @Override
    public View getView() {
        return view;
    }

    @Override
    public void clearMemory(String tag) {
        if (videoView != null) {
            videoView.clearMemory();
        }
    }
}
