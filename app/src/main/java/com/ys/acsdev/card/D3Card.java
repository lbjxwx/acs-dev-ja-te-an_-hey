package com.ys.acsdev.card;

import com.kongqw.serialportlibrary.SerialPortManager;
import com.kongqw.serialportlibrary.listener.OnSerialPortDataListener;
import com.ys.acsdev.util.ToolUtil;

import java.io.File;
import java.util.Arrays;

// Created by kermitye on 2020/7/13 18:45
public class D3Card extends CardSuper {

    String mDev = "/dev/ttyS3";
    int mRare = 9600;
    SerialPortManager mSerialPort;
    boolean mIsOpen;


    public void init(CardResultListener listener) {
        setListener(listener);
        init();
    }

    @Override
    public void init() {
        if (mSerialPort == null) {
            mSerialPort = new SerialPortManager();
            //添加串口数据通信监听
            mSerialPort.setOnSerialPortDataListener(new OnSerialPortDataListener() {

                @Override
                public void onDataReceived(byte[] bytes) {
                    parseData(bytes);
                }

                @Override
                public void onDataSent(byte[] bytes) {

                }
            });
        }
        if (mIsOpen)
            return;
        mIsOpen = mSerialPort.openSerialPort(new File(mDev), mRare);
        if (!mIsOpen) {
//            LogUtil.saveLogToLocal("初始化刷卡失败");
            log("=====初始化失败");
        } else {
            log("初始化成功");
//            setData();
        }
    }


    private void parseData(byte[] data) {
        log("===刷卡数据: " + ToolUtil.bytesToHex(data));
        if (data == null || data.length < 9 || (data[0] & 0xFF) != 0x02) {
            return;
        }

        if (data.length != 9 && data.length != 10) {
            if (mListener != null)
                mListener.onCardResult(-1, "data parse error");
            return;
        }
        byte[] cardData;
        if (data.length == 9)
            cardData = Arrays.copyOfRange(data, 3, 3 + 4);
        else
            cardData = Arrays.copyOfRange(data, 4, 4 + 4);
        String cardNo = ToolUtil.hexToDecimal(cardData);
        log("====读取卡号: " + cardNo);
        if (mListener != null)
            mListener.onCardResult(0, cardNo);
    }


    @Override
    public void destory() {
        mListener = null;
        mIsOpen = false;
        if (mSerialPort != null) {
            mSerialPort.closeSerialPort();
            mSerialPort = null;
        }
    }
}
