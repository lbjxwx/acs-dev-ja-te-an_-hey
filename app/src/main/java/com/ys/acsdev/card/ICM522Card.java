package com.ys.acsdev.card;

import android.text.TextUtils;

import com.kongqw.serialportlibrary.SerialPortManager;
import com.kongqw.serialportlibrary.listener.OnSerialPortDataListener;
import com.ys.acsdev.util.CodeUtil;
import com.ys.acsdev.util.LogUtil;
import com.ys.acsdev.util.ToolUtils;

import java.io.File;
import java.util.Arrays;

// Created by kermitye on 2020/6/11 16:48
public class ICM522Card extends CardSuper {

    String mDev = "/dev/ttyS3";
    int mRare = 9600;
    SerialPortManager mSerialPort;
    boolean mIsOpen;


    public void init(CardResultListener listener) {
        setListener(listener);
        init();
    }

    @Override
    public void init() {
        if (mSerialPort == null) {
            mSerialPort = new SerialPortManager();
            //添加串口数据通信监听
            mSerialPort.setOnSerialPortDataListener(new OnSerialPortDataListener() {

                @Override
                public void onDataReceived(byte[] bytes) {
                    parseData(bytes);
                }

                @Override
                public void onDataSent(byte[] bytes) {

                }
            });
        }
        if (mIsOpen)
            return;
        mIsOpen = mSerialPort.openSerialPort(new File(mDev), mRare);
        if (!mIsOpen) {
            LogUtil.saveLogToLocal("初始化刷卡失败");
            log("=====初始化失败");
        } else {
            log("初始化成功");
//            setData();
        }
    }


    private void setData() {
        //设置自动读卡号
        byte[] data = {(byte) 0x00, (byte) 0x00, (byte) 0x03, (byte) 0x0C, (byte) 0x00 };
        byte sum = checkSum(data, 0, data.length);
        byte[] finalData = {(byte) 0x00, (byte) 0x00, (byte) 0x03, (byte) 0x0C, (byte) 0x00, sum};
        mSerialPort.sendBytes(finalData);
    }

    private void parseData(byte[] data) {
        log("===刷卡数据: " + ToolUtils.toHexString(data));
        if (data == null || data.length < 5)
            return;

        byte[] cardData = Arrays.copyOfRange(data, 0, 4);
        log("===读取到的16进制卡号: " + ToolUtils.toHexString(cardData));
        String cardNo = CodeUtil.decode(cardData);
        log("====转换出来的卡号: " + cardNo);
        if (!TextUtils.isEmpty(cardNo)) {
            if (mListener != null)
                mListener.onCardResult(0, cardNo);
        } else {
            if (mListener != null)
                mListener.onCardResult(1, "刷卡失败，解析数据错误");
        }
    }

    //计算校验码函数
    private byte checkSum(byte[] data, int offset, int length)
    {
        byte temp = 0;
        for (int i = offset; i < length + offset; i++)
        {
            temp ^= data[i];
        }
        return temp;
    }


    @Override
    public void destory() {
        mListener = null;
        mIsOpen = false;
        if (mSerialPort != null) {
            mSerialPort.closeSerialPort();
            mSerialPort = null;
        }
    }


}
