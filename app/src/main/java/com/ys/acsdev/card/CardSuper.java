package com.ys.acsdev.card;

import androidx.annotation.Nullable;

import com.ys.acsdev.util.LogUtil;

public abstract class CardSuper {
    protected CardResultListener mListener;

    public void setListener(@Nullable CardResultListener listener) {
        this.mListener = listener;
    }

    abstract public void init();

    abstract public void destory();

    protected void log(String msg) {
        LogUtil.e(msg, "CardSuper");
    }
}
