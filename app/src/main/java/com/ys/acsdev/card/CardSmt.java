package com.ys.acsdev.card;

import com.kongqw.serialportlibrary.SerialPortManager;
import com.kongqw.serialportlibrary.listener.OnSerialPortDataListener;
import com.ys.acsdev.util.ToolUtil;

import java.io.File;
import java.util.Arrays;

public class CardSmt extends CardSuper {

    String mDev = "/dev/ttyS3";
    int mRare = 9600;
    SerialPortManager mSerialPort;
    boolean mIsOpen;

    public CardSmt(String path) {
        this.mDev = path;
    }

    public void init(CardResultListener listener) {
        setListener(listener);
        init();
    }

    @Override
    public void init() {
        if (mSerialPort == null) {
            mSerialPort = new SerialPortManager();
            //添加串口数据通信监听
            mSerialPort.setOnSerialPortDataListener(new OnSerialPortDataListener() {

                @Override
                public void onDataReceived(byte[] bytes) {
                    parseData(bytes);
                }

                @Override
                public void onDataSent(byte[] bytes) {

                }
            });
        }
        if (mIsOpen)
            return;
        mIsOpen = mSerialPort.openSerialPort(new File(mDev), mRare);
        if (!mIsOpen) {
            log("=====初始化失败");
        } else {
            log("初始化成功");
        }
    }

    private void parseData(byte[] data) {
        if (data == null || data.length < 4) {
            if (mListener != null) {
                mListener.onCardResult(-1, "data parse error");
            }
            return;
        }

        byte[] cardData = Arrays.copyOfRange(data, 1, 1 + 4);
        log("===刷卡数据000: " + ToolUtil.hexToDecimal(cardData));
        String cardNum = ToolUtil.hexToDecimal(cardData);
        if (mListener != null) {
            mListener.onCardResult(0, cardNum);
        }
    }

    @Override
    public void destory() {
        mListener = null;
        mIsOpen = false;
        if (mSerialPort != null) {
            mSerialPort.closeSerialPort();
            mSerialPort = null;
        }
    }
}
