package com.ys.acsdev.card;

import com.kongqw.serialportlibrary.SerialPortManager;
import com.kongqw.serialportlibrary.listener.OnSerialPortDataListener;
import com.ys.acsdev.util.ToolUtil;

import java.io.File;
import java.util.Arrays;

// Created by kermitye on 2020/7/11 14:29

public class H02DCard extends CardSuper {
    public static String PATH_TTYS1 = "/dev/ttyS1";
    public static String PATH_TTYS2 = "/dev/ttyS2";
    public static String PATH_TTYS3 = "/dev/ttyS3";
    public static String PATH_TTYS4 = "/dev/ttyS4";

    String mDev = PATH_TTYS3;
    int mRare = 9600;
    SerialPortManager mSerialPort;
    boolean mIsOpen;

    public H02DCard() {
    }

    public H02DCard(String path) {
        log("======H02DCard========" + path);
        this.mDev = path;
    }

    public void init(CardResultListener listener) {
        setListener(listener);
        init();
    }

    @Override
    public void init() {
        if (mSerialPort == null) {
            mSerialPort = new SerialPortManager();
            //添加串口数据通信监听
            mSerialPort.setOnSerialPortDataListener(new OnSerialPortDataListener() {

                @Override
                public void onDataReceived(byte[] bytes) {
                    parseData(bytes);
                }

                @Override
                public void onDataSent(byte[] bytes) {

                }
            });
        }
        if (mIsOpen)
            return;
        mIsOpen = mSerialPort.openSerialPort(new File(mDev), mRare);
        if (!mIsOpen) {
//            LogUtil.saveLogToLocal("初始化刷卡失败");
            log("=====初始化失败");
        } else {
            log("初始化成功");
//            setData();
        }
    }


    private void parseData(byte[] data) {
        log("===刷卡数据: " + ToolUtil.bytesToHex(data));
        //会偶尔莫名出现020a0200000000000803数据，所以将数据为10的屏蔽掉
        if (data == null || data.length < 9 || (data[0] & 0xFF) != 0x02)
            return;
        if (!check(Arrays.copyOfRange(data, 1, data.length - 2), data[data.length - 2])) {
            log("检验未通过");
            return;
        }
        if (data.length != 9 && data.length != 10) {
            if (mListener != null)
                mListener.onCardResult(-1, "data parse error");
            return;
        }

        byte[] cardData;
        if (data.length == 9)
            cardData = Arrays.copyOfRange(data, 3, 3 + 4);
        else
            cardData = Arrays.copyOfRange(data, 4, 4 + 4);
        String cardNo = ToolUtil.hexToDecimal(cardData);
        log("====读取卡号: " + cardNo);
        if (mListener != null)
            mListener.onCardResult(0, cardNo);
    }

    private boolean check(byte[] data, byte source) {
        byte result = data[0];
        for (int i = 1; i < data.length; i++) {
            result ^= data[i];
        }
        return result == source;
    }


    @Override
    public void destory() {
        mListener = null;
        mIsOpen = false;
        if (mSerialPort != null) {
            mSerialPort.closeSerialPort();
            mSerialPort = null;
        }
    }
}
