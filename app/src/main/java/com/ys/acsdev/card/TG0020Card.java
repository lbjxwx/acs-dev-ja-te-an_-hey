//package com.ys.acsdev.card;
//
//import android.text.TextUtils;
//
//import com.kongqw.serialportlibrary.SerialPortManager;
//import com.kongqw.serialportlibrary.listener.OnSerialPortDataListener;
//import com.ys.acsdev.util.CodeUtil;
//import com.ys.acsdev.util.LogUtil;
//import com.ys.acsdev.util.SpUtils;
//import com.ys.acsdev.util.ToolUtils;
//
//import java.io.File;
//import java.util.Arrays;
//
//// Created by kermitye on 2020/6/11 16:48
//public class TG0020Card extends CardSuper {
//
//    String mDev = "/dev/ttyS3";
//    int mRare = 9600;
//    SerialPortManager mSerialPort;
//    boolean mIsOpen;
//
//
//    public void init(CardResultListener listener) {
//        setListener(listener);
//        init();
//        int ttysSave = SpUtils.getCW_CARD_TTYS();
//        switch (ttysSave) {
//            case 0:
//                mDev = "/dev/ttyS0";
//                break;
//            case 1:
//                mDev = "/dev/ttyS1";
//                break;
//            case 2:
//                mDev = "/dev/ttyS2";
//                break;
//            case 3:
//                mDev = "/dev/ttyS3";
//                break;
//            case 4:
//                mDev = "/dev/ttyS4";
//                break;
//        }
//    }
//
//    @Override
//    public void init() {
//        if (mSerialPort == null) {
//            mSerialPort = new SerialPortManager();
//            //添加串口数据通信监听
//            mSerialPort.setOnSerialPortDataListener(new OnSerialPortDataListener() {
//
//                @Override
//                public void onDataReceived(byte[] bytes) {
//                    parseData(bytes);
//                }
//
//                @Override
//                public void onDataSent(byte[] bytes) {
//
//                }
//            });
//        }
//        if (mIsOpen)
//            return;
//        mIsOpen = mSerialPort.openSerialPort(new File(mDev), mRare);
//        if (!mIsOpen) {
//            LogUtil.saveLogToLocal("初始化刷卡失败");
//            log("=====初始化失败");
//        } else {
//            log("初始化成功");
////            setData();
//        }
//    }
//
//
//    private void parseData(byte[] data) {
//        log("===刷卡数据: " + ToolUtils.toHexString(data));
//        if (data == null || data.length < 4) {
//            if (mListener != null)
//                mListener.onCardResult(2, "read card error: Invalid data");
//            return;
//        }
//
//        log("===读取到的16进制卡号: " + ToolUtils.toHexString(data));
//        String cardNo = "";
//        try {
//            String hexValue = ToolUtils.toHexString(data).replace(" ", "").toUpperCase();
//            cardNo = Long.parseLong(hexValue, 16) + "";
//            log("===格式后的卡号数据: " + hexValue + " / " + cardNo);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        //            val result = CodeUtil.decode(context)
//////            val hexValue = ToolUtils.toHexString(context)//.replace(" ", "").toUpperCase()
//////            val result = java.lang.Long.parseLong(hexValue, 16)
//
//
//        log("====转换出来的卡号: " + cardNo);
//        if (!TextUtils.isEmpty(cardNo)) {
//            if (mListener != null)
//                mListener.onCardResult(0, cardNo);
//        } else {
//            if (mListener != null)
//                mListener.onCardResult(1, "read card error: Failed to parse data");
//        }
//    }
//
//    //计算校验码函数
//    private byte checkSum(byte[] data, int offset, int length) {
//        byte temp = 0;
//        for (int i = offset; i < length + offset; i++) {
//            temp ^= data[i];
//        }
//        return temp;
//    }
//
//
//    @Override
//    public void destory() {
//        mListener = null;
//        if (mSerialPort != null) {
//            mSerialPort.closeSerialPort();
//            mSerialPort = null;
//        }
//    }
//
//
//}
