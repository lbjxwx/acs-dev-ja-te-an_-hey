package com.ys.acsdev.card;

// Created by kermitye on 2020/6/11 16:36
public interface CardResultListener {
    void onCardResult(int code, String cardNo);
}
