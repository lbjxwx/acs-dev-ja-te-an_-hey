package com.ys.acsdev.card;

import android.text.TextUtils;

import com.kongqw.serialportlibrary.SerialPortManager;
import com.kongqw.serialportlibrary.listener.OnSerialPortDataListener;
import com.ys.acsdev.helper.SoundHelper;
import com.ys.acsdev.util.CodeUtil;
import com.ys.acsdev.util.LogUtil;
import com.ys.acsdev.util.ToolUtils;

import java.io.File;
import java.util.Arrays;

// Created by kermitye on 2020/6/15 13:59
// 天河时代刷卡
public class DL2425Card extends CardSuper {

    String mDev = "/dev/ttyS3";
    int mRare = 115200;
    SerialPortManager mSerialPort;
    boolean mIsOpen;


    public void init(CardResultListener listener) {
        setListener(listener);
        init();
    }

    @Override
    public void init() {
        if (mSerialPort == null) {
            mSerialPort = new SerialPortManager();
            //添加串口数据通信监听
            mSerialPort.setOnSerialPortDataListener(new OnSerialPortDataListener() {

                @Override
                public void onDataReceived(byte[] bytes) {
                    parseData(bytes);
                }

                @Override
                public void onDataSent(byte[] bytes) {


                }
            });
        }
        if (mIsOpen)
            return;
        mIsOpen = mSerialPort.openSerialPort(new File(mDev), mRare);
        if (!mIsOpen) {
            LogUtil.saveLogToLocal("初始化刷卡失败");
            log("=====初始化失败");
        } else {
            log("初始化成功");
        }
    }

    private void parseData(byte[] data) {
        log("===刷卡数据: " + ToolUtils.toHexString(data));
        SoundHelper.playCard();
        if (data == null || data.length < 10 || (data[0] & 0xFF) != 0x91 || (data[1] & 0xFF) != 0x91) {
            if (mListener != null)
                mListener.onCardResult(1, "刷卡失败，解析数据错误");
            return;
        }
        byte[] cardData = Arrays.copyOfRange(data, 4, 8);
        log("========cardData: " + ToolUtils.toHexString(cardData));
        String cardNo = CodeUtil.decode(cardData);
        log("======cardNo: $cardNo");
        if (!TextUtils.isEmpty(cardNo)) {
            if (mListener != null)
                mListener.onCardResult(0, cardNo);
        } else {
            if (mListener != null)
                mListener.onCardResult(2, "解析数据错误");
        }
    }

    //计算校验码函数
    private byte checkSum(byte[] data, int offset, int length) {
        byte temp = 0;
        for (int i = offset; i < length + offset; i++) {
            temp ^= data[i];
        }
        return temp;
    }


    @Override
    public void destory() {
        mListener = null;
        if (mSerialPort != null) {
            mSerialPort.closeSerialPort();
            mSerialPort = null;
        }
    }
}
