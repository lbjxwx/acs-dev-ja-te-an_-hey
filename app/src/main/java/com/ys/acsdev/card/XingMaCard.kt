package com.ys.acsdev.card

import android.text.TextUtils
import com.kongqw.serialportlibrary.SerialPortManager
import com.kongqw.serialportlibrary.listener.OnSerialPortDataListener
import com.ys.acsdev.util.LogUtil.saveLogToLocal
import java.io.File
import java.lang.StringBuilder

// Created by kermitye on 2020/6/11 17:24
class XingMaCard: CardSuper() {
    var mDev = "/dev/ttyS3"
    var mRare = 9600
    var mSerialPort: SerialPortManager? = null
    var mIsOpen = false

    val DEVICE_TTYS1 = "/dev/ttyS1"
    val DEVICE_TTYS2 = "/dev/ttyS2"
    val DEVICE_TTYS3 = "/dev/ttyS3"
    val DEVICE_TTYS4 = "/dev/ttyS4"



    fun init(listener: CardResultListener?) {
        setListener(listener)
        init()
    }

    override fun init() {
        if (mSerialPort == null) {
            mSerialPort = SerialPortManager()
            //添加串口数据通信监听
            mSerialPort!!.setOnSerialPortDataListener(object : OnSerialPortDataListener {
                override fun onDataReceived(bytes: ByteArray) {
                    parseData(bytes)
                }
                override fun onDataSent(bytes: ByteArray) {}
            })
        }
        if (mIsOpen)
            return
        mIsOpen = mSerialPort!!.openSerialPort(File(mDev), mRare)
        if (!mIsOpen) {
            saveLogToLocal("初始化刷卡失败")
            log("=====初始化失败")
        } else {
            log("初始化成功")
        }
    }

    private fun parseData(data: ByteArray?) {
        if (data == null) return
        if (data != null && data.size >= 10) {
            val sb = StringBuilder()
            for (i in 0 until 10) {
                val char = data[i].toChar()
//                log("======char: ${char}")
                sb.append(char)
            }
//            val cardNo = CodeUtil.decode(data)
            val cardNo = sb.toString()
            log("======cardNo: $cardNo")
            if (cardNo.isNotEmpty()) {
                mListener?.onCardResult(0, cardNo)
            } else {
                mListener?.onCardResult(3, "刷卡失败，解析数据错误")
            }
        } else {
            mListener?.onCardResult(2, "刷卡失败，解析数据错误")
        }
    }

    override fun destory() {
        mListener = null
        if (mSerialPort != null) {
            mSerialPort!!.closeSerialPort()
            mSerialPort = null
        }
    }
}