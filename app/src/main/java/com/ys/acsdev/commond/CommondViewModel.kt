package com.ys.acsdev.commond

import androidx.lifecycle.MutableLiveData

// Created by kermitye on 2020/5/27 16:13
// 全局ViewModel
class CommondViewModel {
    companion object {
        @JvmStatic
        val instance: CommondViewModel by lazy { CommondViewModel() }
    }

    //人脸识别初始化状态
    val mFaceInitState = MutableLiveData<Boolean>()

    //网络状态
    val mNetState = MutableLiveData<Boolean>(false)

    //服务器连接状态
    val mServerState = MutableLiveData<Boolean>(false)

}