package com.ys.acsdev.commond;

import android.os.Environment;
import android.util.Log;

import com.ys.acsdev.api.AppConfig;
import com.ys.acsdev.util.CodeUtil;
import com.ys.acsdev.util.SpUtils;

import org.jetbrains.annotations.NotNull;

public class AppInfo {

    public static final String IP_DEFAULT_URL = "47.107.50.81";
    public static final String SERVER_PORT = "8085";

    /**
     * 用来缓存当前保存得人员数量
     */
    public static int CURRENT_SAVE_PERSON_NUM = 0;

    //是否连接服务器
    public static boolean isConnectServer = false;
    //是否重新生成访客二维码
    public static boolean resetVisitorQr = true;

    //网络模式
    public static final int MODEL_NET = 0;
    //单机模式
    public static final int MODEL_STAND_ALONE = 1;

    public static final String USB_APK_NAME = "ysFace.apk";
    //{"company":"花果山","department":"水帘洞","deviceName":"001","logoName":"ysFace.png"}
    public static final String USB_APK_MODIFY_IP = "ysFace.txt";

    public static final String USB_APK_ADD_ASK_FORBIDEN = "ysFace-ask.txt";
    //USB 单机任务
    public static final String SINGLE_TASK_DIR = "eface-media";
    public static final String SINGLE_FACE_PATH = "eface-register";
    //开机动画
    public static final String BOOTANIMAL = "bootanimation.zip";
    //是否同步服务器时间了
    public static boolean isUpdateServerTime = false;

    //USB 人脸注册
    public static final String FACE_REGISTER_DIR = "eface-register";

    /**
     * 单机模式的存储地址
     *
     * @return
     */
    public static String TASK_SINGLE_PATH() {
        return BASE_LOCAL_PATH + "/single";
    }

    public static String getBaseUrl() {
        String ipAddress = SpUtils.getmServerIp();
        String port = SpUtils.getmServerPort();
        String baseUrl = "http://" + ipAddress + ":" + port + "/";
        return baseUrl;
    }

    public static String getBaseCommUrl() {
        String defaultPath = "cloudIntercom/";
        return getBaseUrl() + defaultPath;


    }

    /***
     * 获取注册接口
     * @return
     */
    public static String getRegisterEquipment() {
        return getBaseCommUrl() + "api/registerEquipment";
    }

    /****
     * 疫情新增访客
     * @return
     */
    public static String addEpidemicVisitorInfo() {
        return getBaseCommUrl() + "api/addEpidemicVisitorInfo";
    }

    /***
     * 二维码生成的信息
     * @return
     * //urlForst = "https://efaceapp.esoncloud.com/#/visitor/";
     * //urlForst = "https://efaceapp.esoncloud.com/#/otherVisitor/";
     * urlForst = baseUrl + "start/#/visitor/" + CodeUtil.getUniquePsuedoID();
     *
     * https://pulsetec.ai/#/accessRecord
     */
    @NotNull
    public static String getQrCodeUrl() {
        String baseUrl = getBaseUrl();
        if (baseUrl.contains("47.107.50.81")) {
            baseUrl = "https://eface.esoncloud.com/";
        } else if (baseUrl.contains("35.231.13.47")) {
            baseUrl = "https://pulsetec.ai/";
        }
        String urlForst = "";
        boolean isOpenAskQuestion = SpUtils.getShowAskErCode();
        if (isOpenAskQuestion) {
            //问卷调查
            if (baseUrl.contains("eface.esoncloud.com")) {
                urlForst = baseUrl + "#/questionnaire/" + CodeUtil.getUniquePsuedoID();
            } else {
                urlForst = baseUrl + "start/#/questionnaire/" + CodeUtil.getUniquePsuedoID();
            }
            return urlForst;
        }
        if (baseUrl.contains("eface.esoncloud.com")) {
            urlForst = baseUrl + "#/visitor/" + CodeUtil.getUniquePsuedoID();
        } else {
            urlForst = baseUrl + "start/#/visitor/" + CodeUtil.getUniquePsuedoID();
        }
        Log.e("cdl", "=urlForst=" + urlForst);
        return urlForst;
    }


    //检查软件更新
    public static String getUpdateAppUrl() {
//        return getBaseCommUrl() + "api/selectApkDateByApKFileId";
        return getBaseCommUrl() + "api/selectUpgradeApkInfo";
    }

    /***
     * 获取设备旷视授权状态
     */
    public static String getKungShiAuth() {
        return "http://47.107.50.81:8080/etvauth/authapi/getKungShiAuth";
    }

    /***
     * 旷视激活
     * @return
     */
    public static String activeKuangShiAuth() {
        return "http://47.107.50.81:8080/etvauth/authapi/activeKuangShiAuth";
    }

    /***
     * 更新设备信息
     * @return
     */
    public static String getUpdateEquipmentBySn() {
        return getBaseCommUrl() + "api/updateEquipmentBySn";
    }

    /***
     * 获取门禁的通行记录
     * @return
     */
    public static String getSelectAccessRecordBySn() {
//192.168.1.22:8898/cloudIntercom/api/selectAccessRecordBySn?sn=301F9A80C630
        return getBaseCommUrl() + "api/selectAccessRecordBySn";
    }


    //添加广告记录
    public static String insertTaskStatistics() {
        return getBaseCommUrl() + "api/insertTaskStatistics";
    }

    /***
     * 获取提交通行记录
     * @return
     */
    public static String getUpdateReocrdInfo() {
        return getBaseCommUrl() + "api/insertAccessRecord";
    }

    /***
     * 获取设备需要显示得logo
     */
    public static String getSelectLogoDataBySn() {
        return getBaseCommUrl() + "api/selectLogoDataBySn";
    }

    /***
     * 查询设备是否注册
     * @return
     */
    public static String getSelectGateEquipBySn() {
        //sn
        return getBaseCommUrl() + "api/selectGateEquipBySn";
    }

    /***
     * 获取访客信息
     * @return
     */
    public static String getSelectVisitorUrl() {
        return getBaseCommUrl() + "api/selectVisitor";
    }

    /***
     * 获取人员信息
     * @return
     */
    public static String selectBindPersonBySn() {
        return getBaseCommUrl() + "api/selectBindPersonBySn";
//        http://139.196.252.129:39018/cloudIntercom/api/selectBindPersonBySn
    }

    /***
     * 获取增量查询得接口
     * @return
     */
    public static String getBindPersonBySnUrl() {
//        http://192.168.1.76:8085/cloudIntercom/api/getBindPersonBySn
        return getBaseCommUrl() + "api/getBindPersonBySn";
    }


    /***
     * 提交设备信息给服务器
     * @return
     */
    public static String getUpdateDevToAuthorServer() {
        return "http://47.107.50.81:8080/etvauth/authapi/putSellBoardInfo";
    }

    /***
     * 添加刷卡通行记录
     * OSS提交通行记录信息
     * @return
     */
    public static String putAccessCardRecord() {
        return getBaseCommUrl() + "api/putAccessRecord";
    }

    /***
     * 添加人员信息
     * @return
     */
    public static String insertPersonData() {
//        http://ip:port/cloudIntercom/api/insertPersonData
        return getBaseCommUrl() + "api/insertPersonData";
    }

    /***
     * 获取当前时间
     * @return
     */
    public static String getCurrentSystemTimeFromWeb() {
//        http://47.107.50.81:8085/cloudIntercom/api/getCurrentSystemTime
        return getBaseCommUrl() + "api/getCurrentSystemTime";
    }

    /**
     * 提交APK下载进度
     *
     * @return
     */
    public static String getUpdateAppDownStatues() {
        return getBaseCommUrl() + "api/upgradeApkUpgradeProgress";
    }


    /***
     * 获取设备配置信息
     * @return
     */
    public static String getSelectConfigDataBySn() {
        return getBaseCommUrl() + "api/selectConfigDataBySn";
//        http://192.168.1.20:8085/cloudIntercom/api/selectConfigDataBySn?sn=301F9A8107A2
    }

    /***
     * 获取人员信息黑名单
     * @return
     */
    public static String getBlackList() {
        return getBaseCommUrl() + "api/getBlackList";
    }

    /***
     * 提交下载任务得进度
     * @return
     */
    public static String getUpdateTaskProgress() {
//        @Field("sn") sn: String,
//                @Field("taskId") taskId: String,
//                @Field("downloadSpeed") downloadSpeed: String,
//                @Field("downloadProgress") downloadProgress: String
        return getBaseCommUrl() + "api/updateTaskProgress";
    }

    /***
     * 提交日志到服务器
     * @return
     */
    public static String getUploadEquipmentLog() {
        return getBaseCommUrl() + "api/uploadEquipmentLog";
    }

    //=================通行方式==================
    public static final String PASSTYPE_CARD = "1"; //刷卡
    public static final String PASSTYPE_FACE = "2"; //刷脸
    public static final String PASSTYPE_WEB = "7";  //平台远程
    public static final String PASSTYPE_TEMP_PWD = "8"; //临时密码
    public static final String PASSTYPE_UNKNOW = "9";//陌生人
    public static final String PASSTYPE_VISITOR = "10";//访客
    public static final String PASSTYPE_IDCARD = "11";//刷身份证

    public static final String DEFAULT_PHONE = "666666";
    public static String authorities = "com.ys.acsdevs.fileprovider";


    public static final String TEMP_TYPE_NOT_OPEN = "Not Open";
    public static final String TEMP_TYPE_LOW_TEMP = "Low Temp";

    public static final String TEMP_TYPE_NORMAL = "0";  //体温正常
    public static final String TEMP_TYPE_HEIGHT = "1";  //高温异常

    public static final String MAST_WAER_DEFAULT = "0";  //未佩戴口罩
    public static final String MAST_WAER_RIGHT = "1";  //佩戴口罩

    //人员管理
    public static final int FACE_REGISTER_PERSON = 1;
    //访客
    public static final int FACE_REGISTER_VISITOR = 2;
    //黑名单
    public static final int FACE_REGISTER_BLACK_LIST = 3;
    //获取人员全部信息，用来数据库标号
    public static final int FACE_REGISTER_PERSON_ALL = -1;

    //=================服务器指令================================================
    //同步人脸信息
    public static final int MESSAGE_SERVER_UPDATE_FACE = 5008;
    //同步人脸信息
    public static final int MESSAGE_SERVER_ADD_PERSON = 5028;
    //同步访客信息
    public static final int MESSAGE_SERVER_ADD_VISITOR = 5029;
    public static final int MESSAGE_SERVER_DOOR_OPEN = 5026;

    //修改设备分组
    public static final int MESSAGE_MODIFY_DEV_GROUP = 5006;
    //更新设备信息(门禁信息修改)
    public static final int MESSAGE_SERVER_UPDATE_DEVICE = 5007;
    //删除设备
    public static final int MESSAGE_SERVER_DEL_DEVICE = 5022;
    // 添加门禁机
    public static final int MESSAGE_SERVER_REGISTER_DEV = 5013;
    //重启设备
    public static final int MESSAGE_SERVER_REDEV = 5014;
    //重启应用
    public static final int MESSAGE_SERVER_REAPP = 5015;
    //调节音量
    public static final int MESSAGE_SERVER_SOUND = 5016;
    //获取监控截图
    public static final int SERVER_MONITOR = 5017;
    //更新LOGO
    public static final int MESSAGE_SERVER_LOGO = 6009;
    //检查更新
    public static final int MESSAGE_SERVER_CHECK_UPDATE = 5025;
    //上传工作日志
    public static final int MESSAGE_UPDATE_RUN_LOG = 5030;
    //检查广告
    public static final int MESSAGE_SERVER_AD = 5020;
    //分配设备
    public static final int MESSAGE_REPLACE_DEV = 5031;
    //设备设置参数同步
    public static final int MESSAGE_UPDATE_SETTING_DEV = 5032;

    //==============================================================================================
    public static final String BASE_PATH_INNER = Environment.getExternalStorageDirectory().getPath();
    public static final String BASE_LOCAL_PATH = BASE_PATH_INNER + "/face";
    public static final String BASE_APK_PATH = BASE_LOCAL_PATH + "/apk";
    public static final String BASE_IMAGE_PATH = BASE_LOCAL_PATH + "/image";
    public static final String BASE_VIDEO_PATH = BASE_LOCAL_PATH + "/video";
    public static final String BASE_CRASH_LOG = BASE_LOCAL_PATH + "/crashlog";

    public static final String PATH_ANDROID = BASE_PATH_INNER + "/Android";
    public static final String LOG_PATH = PATH_ANDROID + "/log";
    //缓存U盘路径的
    public static String U_PAN_PATH_CACHE = "";
    public static final String BASE_PATH_CACHE = BASE_LOCAL_PATH + "/cache";
    public static final String LOGO_PATH = BASE_LOCAL_PATH + "/logo.png";
    public static final String BASE_TASK_PATH = BASE_LOCAL_PATH + "/task";
    public static final String PASS_RECORD_PATH = BASE_LOCAL_PATH + "/passRecord";

    //同行记录保存路径
//    public static final String ACCESSrECORD_INFO_PATH = PASS_RECORD_PATH + "/passRecord.xls";

    //截图的保存位置
    public static final String CAPTURE_IMAGE_PATH = BASE_PATH_INNER + "/capture.jpg";
    public static final String CAPTURE_IMAGE_LOCAL = BASE_LOCAL_PATH + "/local";
    //绑定二维码的存储位置
    public static final String QR_BIND_PATH = BASE_PATH_CACHE + "/bindCode.jpg";
    //访客二维码
    public static final String QR_VISITOR_PATH = BASE_LOCAL_PATH + "/visotor.png";
    public static final String QR_VISITOR_DOWN_PATH = BASE_LOCAL_PATH + "/visotor_web.png";
    //人脸识别默认得图片保存位置
    public static final String DEFAULT_FACE_IMAGE_PATH = AppInfo.BASE_IMAGE_PATH + "/face_default.png";

    public static final String YS_CAMERA_ADD = "com.ys.camera_add";
    public static final String YS_CAMERA_REMOVE = "com.ys.camera_remove";

    public static String TAG_UPDATE = "TAG_UPDATE";  //截图上传
    public static String TAG_SHOW = "TAG_SHOW";      //页面加载
    public static final String CAPTURE_IMAGE_RECEIVE = "CAPTURE_IMAGE_RECEIVE";  //发送截图的广播
    public static final String SEND_IMAGE_CAPTURE_SUCCESS = "SEND_IMAGE_CAPTURE_SUCCESS";  //截图成功，返回的广播

    public static final String MODIFY_APP_PACKAGE_CHOOICE = "MODIFY_APP_PACKAGE_CHOOICE";  //修改是否选择本地APP
    public static final String CHANGE_GUARDIAN_STATUES = "CHANGE_GUARDIAN_STATUES"; //ETV发过来的广播修改守护进程的状态
    public static final String MODIFY_GUARDIAN_TIME = "MODIFY_GUARDIAN_TIME"; //ETV发过来的广播修改守护进程的守护时间
    public static final String MODIFY_GUARDIAN_PACKAGENAME = "MODIFY_GUARDIAN_PACKAGENAME"; //修改守护进程包名
    public static final String START_PROJECTOR_GUARDIAN_TIMER = "START_PROJECTOR_GUARDIAN_TIMER"; //通知守护进程，从现在开始计时
    public static final String RECEIVE_BROAD_CAST_LIVE = "RECEIVE_BROAD_CAST_LIVE"; //守护进程发通知过来
    public static final String SEND_BROAD_CAST_LIVE = "SEND_BROAD_CAST_LIVE"; //告诉守护进程自己还活着

    public static final String MODIFY_START_DELAY_TIME = "MODIFY_START_DELAY_TIME";  //修改延时启动得时间
    public static final String MODIFY_START_DELAY_ANIMAL = "MODIFY_START_DELAY_ANIMAL";  //修改延时启动得动画


    // 停止请求或者下载人脸图片信息
    public static final String STOP_REQUEST_DOWN_FACE_IMAGE = "STOP_REQUEST_DOWN_FACE_IMAGE";
    //SERVICE 接受广播，开始请求
    public static final String RECEIVE_MESSAGE_TO_REQUEST_TASK = "RECEIVE_MESSAGE_TO_REQUEST_TASK";
    public static final String DOWN_TASK_SUCCESS = "com.etv.config.DOWN_TASK_SUCCESS";
    //修改底部信息，底部信息需要更新
    public static final String UPDATE_MAIN_BOTTOM_VIEW = "com.etv.config.UPDATE_MAIN_BOTTOM_VIEW";


    public static final String CURRENT_NOT_RIGHT_TIME = "Current Is Not Right Time!";

    //检查主界面得logo
    public static final String CHECK_MAIN_VIEW_LOGO = "CHECK_MAIN_VIEW_LOGO";
    //上传工作日志到服务器
    public static final String UPDATE_LOCAL_RUN_LOG_TO_WEB = "UPDATE_LOCAL_RUN_LOG_TO_WEB";
    //开始检测人脸请求--下载--注册
    public static final String START_CHECK_FACE_DOWN_REGISTER = "START_CHECK_FACE_DOWN_REGISTER";
    //检测APP升级
    public static final String BROAD_CHECK_APP_UPDATE = "BROAD_CHECK_APP_UPDATE";


    //socket 心跳消息
    public static final int SOCKET_HANDLER_MESSAGE_HEART = 245;
    //心跳时间
    public static final long SOCKET_HEART_TIME_DISTANCE = 25 * 1000;
    //用来判断当前设备是否注册程序启动，只检查一次
    public static boolean ifDevRegister = false;
    //正常心跳
    public static final int HEART_STATUES_DEFAULT = 1;
    //从其他界面来的
    public static final int HEART_STATUES_FROM_OTHER_ACTIVITY = -1;
//    //APP检测升级
//    public static final int MESSAGE_CHECK_APP_UPDATE = 246;

    //是否可以检测定时任务
    public static boolean startCheckTaskTag = false;
}
