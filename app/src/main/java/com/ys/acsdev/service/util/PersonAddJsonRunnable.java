package com.ys.acsdev.service.util;

import android.os.Handler;

import com.ys.acsdev.bean.PersonBean;
import com.ys.acsdev.commond.AppInfo;
import com.ys.acsdev.db.dao.FaceInfoJavaDao;
import com.ys.acsdev.db.dao.PersonDao;
import com.ys.acsdev.service.view.FaceCheckView;
import com.ys.acsdev.util.FileUtils;
import com.ys.acsdev.util.LogUtil;
import com.ys.acsdev.util.SimpleDateUtil;
import com.ys.acsdev.util.SpUtils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class PersonAddJsonRunnable implements Runnable {
    String dataJson;
    FaceCheckView faceCheckView;
    private Handler handler = new Handler();
    List<PersonBean> listPerson = new ArrayList<PersonBean>();
    int personTag;

    public PersonAddJsonRunnable(String dataJson, FaceCheckView faceCheckView, int personTag) {
        this.personTag = personTag;
        this.dataJson = dataJson;
        this.faceCheckView = faceCheckView;
        SpUtils.setFaceSyncTime(SimpleDateUtil.formatTaskTimeShow(System.currentTimeMillis()));
    }

    @Override
    public void run() {
        parsenerFaceDate();
    }

    /****
     * 解析
     */
    private void parsenerFaceDate() {
        listPerson.clear();
        LogUtil.persoon("parsenerFaceDate开始解析==000");
        if (dataJson == null || dataJson.length() < 5) {
            backRequestStatuesInfo(faceCheckView, null, "parsener faceJson :Data ==null");
            return;
        }
        try {
            JSONObject jsonData = new JSONObject(dataJson);
            if (dataJson.contains("allUserList")) {
                String allUserList = jsonData.getString("allUserList");
                personAllUserListInfo(allUserList);
            }
            if (dataJson.contains("updateUserList")) {
                String updateUserList = jsonData.getString("updateUserList");
                personAllUserListInfo(updateUserList);
            }
            if (dataJson.contains("removeUserList")) {
                String removeUserList = jsonData.getString("removeUserList");
                personRemoveData(removeUserList);
            }
            backRequestStatuesInfo(faceCheckView, listPerson, "解析数据完毕，返回数据");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /***
     * 解析新增以及更新得数据
     * @param allUserList
     */
    private void personAllUserListInfo(String allUserList) {
        if (allUserList == null || allUserList.length() < 5) {
            return;
        }
        try {
            JSONArray jsonArray = new JSONArray(allUserList);
            int personNum = jsonArray.length();
            if (personNum < 1) {
                return;
            }
            for (int i = 0; i < personNum; i++) {
                JSONObject jsonPerson = jsonArray.getJSONObject(i);
                String idCard = "";
                String photoSize = "";
                String cardNum = "";
                int type = AppInfo.FACE_REGISTER_PERSON;
                if (jsonPerson.toString().contains("type")) {
                    type = jsonPerson.getInt("type");
                }
                String syncTime = SimpleDateUtil.formatTaskTimeShow(System.currentTimeMillis());
                String perId = jsonPerson.getString("perId");
                String name = jsonPerson.getString("name");
                String photoUrl = "";
                if (jsonPerson.toString().contains("photoUrl")) {
                    photoUrl = jsonPerson.getString("photoUrl");
                }
                if (photoUrl.contains("\\")) {
                    photoUrl.replace("\\", "");
                }
                if (jsonPerson.toString().contains("photoSize")) {
                    photoSize = jsonPerson.getString("photoSize");
                }
                if (jsonPerson.toString().contains("idCard")) {
                    idCard = jsonPerson.optString("idCard");
                }
                String phone = "";
                if (jsonPerson.toString().contains("phone")) {
                    phone = jsonPerson.getString("phone");
                }
                if (jsonPerson.toString().contains("cardNum")) {
                    cardNum = jsonPerson.getString("cardNum");
                }
                if (jsonPerson.toString().contains("validateTime")) {
                    syncTime = jsonPerson.getString("validateTime");
                }
                if (personTag == AppInfo.FACE_REGISTER_PERSON_ALL) {
                    //解析全部数据
                    PersonBean personBean = new PersonBean(type, perId, cardNum, name, phone, photoUrl, syncTime, "Waiting...", photoSize, idCard);
                    //直接在解析得时候，保存到数据库，后边直接拉取数据数据
                    boolean isSave = PersonDao.insertPerson(personBean);
                    listPerson.add(personBean);
                    LogUtil.persoon("开始解析-所有得数据==" + personBean.toString() + "\n 保存状态 = " + isSave);
                } else {
                    //只解析相应类型得数据
                    if (personTag == type) {
                        PersonBean personBean = new PersonBean(type, perId, cardNum, name, phone, photoUrl, syncTime, "Waiting...", photoSize, idCard);
                        //直接在解析得时候，保存到数据库，后边直接拉取数据数据
                        boolean isSave = PersonDao.insertPerson(personBean);
                        listPerson.add(personBean);
                        LogUtil.persoon("开始解析-类型数据==" + personBean.toString() + "\n 保存状态 = " + isSave);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void backRequestStatuesInfo(FaceCheckView faceCheckView, List<PersonBean> list, String errorDesc) {
        if (faceCheckView == null) {
            return;
        }
        handler.post(new Runnable() {
            @Override
            public void run() {
                faceCheckView.backFaceListInfo(list, errorDesc);
            }
        });
    }


    /***
     * 解析需要删除得数据
     * @param removeUserList
     */
    private void personRemoveData(String removeUserList) {
        LogUtil.persoon("personRemoveData==" + removeUserList);
        if (removeUserList == null || removeUserList.length() < 5) {
            return;
        }
        try {
            JSONArray jsonArray = new JSONArray(removeUserList);
            int personNum = jsonArray.length();
            LogUtil.persoon("parsenerFaceDate需要移除得数据==000" + personNum);
            if (personNum < 1) {
                return;
            }
            for (int i = 0; i < personNum; i++) {
                String perId = jsonArray.get(i).toString();
                deletePersonFeromDb(perId);
                LogUtil.persoon("parsenerFaceDate需要移除得数据==perId" + perId);
            }
        } catch (Exception e) {
            LogUtil.persoon("==解析异常===" + e.toString());
            e.printStackTrace();
        }
    }

    private void deletePersonFeromDb(String perId) {
        try {
            PersonDao.delPersonByPerId(perId);
            boolean isDel = FaceInfoJavaDao.deleteById(perId, "增量查询数据移除人员信息:" + perId);
            LogUtil.persoon("====删除人员信息状态==" + isDel);
            String faceImagePath = AppInfo.BASE_IMAGE_PATH + "/" + perId + ".jpg";
            FileUtils.deleteDirOrFile(faceImagePath, "增量查询，多余得人脸图片");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
