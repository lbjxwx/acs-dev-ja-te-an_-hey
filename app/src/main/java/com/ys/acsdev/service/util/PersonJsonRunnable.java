package com.ys.acsdev.service.util;

import android.os.Handler;

import com.ys.acsdev.bean.PersonBean;
import com.ys.acsdev.commond.AppInfo;
import com.ys.acsdev.db.dao.PersonDao;
import com.ys.acsdev.service.view.FaceCheckView;
import com.ys.acsdev.util.LogUtil;
import com.ys.acsdev.util.SimpleDateUtil;
import com.ys.acsdev.util.SpUtils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class PersonJsonRunnable implements Runnable {
    String data;
    FaceCheckView faceCheckView;
    private Handler handler = new Handler();

    public PersonJsonRunnable(String data, FaceCheckView faceCheckView) {
        this.data = data;
        this.faceCheckView = faceCheckView;
        SpUtils.setFaceSyncTime(SimpleDateUtil.formatTaskTimeShow(System.currentTimeMillis()));
    }

    @Override
    public void run() {
        parsenerFaceDate();
    }

    /****
     * 解析
     */
    private void parsenerFaceDate() {
        LogUtil.persoon("parsenerFaceDate开始解析==000");
        if (data == null || data.length() < 5) {
            backRequestStatuesInfo(faceCheckView, null, "parsener faceJson :Data ==null");
            return;
        }
        LogUtil.persoon("parsenerFaceDate开始解析==" + data);
        try {
            JSONArray jsonArray = new JSONArray(data);
            int personNum = jsonArray.length();
            if (personNum < 1) {
                backRequestStatuesInfo(faceCheckView, null, "no face info list");
                return;
            }
            List<PersonBean> listPerson = new ArrayList<PersonBean>();
            for (int i = 0; i < personNum; i++) {
                JSONObject jsonPerson = jsonArray.getJSONObject(i);
                String idCard = "";
                String photoSize = "";
                String cardNum = "";
                int type = AppInfo.FACE_REGISTER_PERSON;
                if (jsonPerson.toString().contains("type")) {
                    type = jsonPerson.getInt("type");
                }
                String syncTime = SimpleDateUtil.formatTaskTimeShow(System.currentTimeMillis());
                String perId = jsonPerson.getString("perId");
                String name = jsonPerson.getString("name");
                String photoUrl = "";
                if (jsonPerson.toString().contains("photoUrl")) {
                    photoUrl = jsonPerson.getString("photoUrl");
                }
                if (photoUrl.contains("\\")) {
                    photoUrl.replace("\\", "");
                }
                if (jsonPerson.toString().contains("photoSize")) {
                    photoSize = jsonPerson.getString("photoSize");
                }
                if (jsonPerson.toString().contains("idCard")) {
                    idCard = jsonPerson.optString("idCard");
                }
                String phone = "";
                if (jsonPerson.toString().contains("phone")) {
                    phone = jsonPerson.getString("phone");
                }
                if (jsonPerson.toString().contains("cardNum")) {
                    cardNum = jsonPerson.getString("cardNum");
                }
                if (jsonPerson.toString().contains("validateTime")) {
                    syncTime = jsonPerson.getString("validateTime");
                    LogUtil.persoon("parsenerFaceDate开始解析syncTime==" + syncTime);
                }
                PersonBean personBean = new PersonBean(type, perId, cardNum, name, phone, photoUrl, syncTime, "Waiting...", photoSize, idCard);
                //直接在解析得时候，保存到数据库，后边直接拉取数据数据
                PersonDao.insertPerson(personBean);
                listPerson.add(personBean);
//                LogUtil.persoon("parsenerFaceDate开始解析==" + personBean.toString());
            }
            backRequestStatuesInfo(faceCheckView, listPerson, "get face list success");
        } catch (Exception e) {
            LogUtil.persoon("====解析异常=00=" + e.toString());
            e.printStackTrace();
        }
    }

    private void backRequestStatuesInfo(FaceCheckView faceCheckView, List<PersonBean> list, String errorDesc) {
        if (faceCheckView == null) {
            return;
        }
        LogUtil.persoon("backRequestStatuesInfo==" + errorDesc);
        if (list != null && list.size() > 0) {
            LogUtil.persoon("backRequestStatuesInfo=list=" + list.size());
        }
        handler.post(new Runnable() {
            @Override
            public void run() {
                faceCheckView.backFaceListInfo(list, errorDesc);
            }
        });
    }

}
