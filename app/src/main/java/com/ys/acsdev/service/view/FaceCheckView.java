package com.ys.acsdev.service.view;

import com.ys.acsdev.bean.FaceInfo;
import com.ys.acsdev.bean.PersonBean;

import java.util.List;

public interface FaceCheckView {

    /***
     * 人脸注册完毕，回调
     */
    void registerFaceInfoCompaintBack();

    /***
     * 解析数据
     * 返回得人脸信息列表
     * @param listPerson
     * @param errorDesc
     */
    void backFaceListInfo(List<PersonBean> listPerson, String errorDesc);

    void backRequestFaceInfo(boolean isStatues, String errorDesc, int personTag);

    /***
     * 下载文件状态
     * @param isDown
     * @param errorDesc
     */
    void downFaceFileBack(boolean isDown, List<FaceInfo> data, String errorDesc, String printTag);

}
