package com.ys.acsdev.service.view;

public interface AcsUpdateView {

    /***
     * 上传失败
     * @param desc
     */
    void updateFailed(String desc);

    /***
     * 上传的进度
     * @param lastNum
     */
    void updateLocalAtttendPersent(int lastNum);

    /***
     *上传完成
     */
    void updateLocalAtttendSuccess();


}
