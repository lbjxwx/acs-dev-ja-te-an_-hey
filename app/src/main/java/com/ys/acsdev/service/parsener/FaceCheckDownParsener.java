package com.ys.acsdev.service.parsener;

import android.content.Context;

import com.ys.acsdev.bean.FaceInfo;
import com.ys.acsdev.bean.PersonBean;
import com.ys.acsdev.service.model.FaceCheckDownModel;
import com.ys.acsdev.service.model.FaceCheckDownModelmpl;
import com.ys.acsdev.service.view.FaceCheckView;

import java.util.List;

public class FaceCheckDownParsener {

    Context context;
    FaceCheckDownModel faceCheckDownModel;
    FaceCheckView faceCheckView;

    public FaceCheckDownParsener(Context context, FaceCheckView faceCheckView) {
        this.context = context;
        this.faceCheckView = faceCheckView;
        faceCheckDownModel = new FaceCheckDownModelmpl();
    }

    /***
     * 获取人脸信息
     */
    public void getPersonFromWeb(String printTag) {
        faceCheckDownModel.getPersonFromWeb(printTag, faceCheckView);
    }

    /***
     * 获取增量查询得人员信息
     *1    人员管理
     *2    访客
     *3    黑名单
     *-1   所有得信息
     */
    public void getBindPersonBySn(int psrsonTag) {
        faceCheckDownModel.getBindPersonBySn(psrsonTag, faceCheckView);
    }

    /***
     * 检查下载
     * @param needRegisterData
     */
    public void startToCheckDownFile(List<PersonBean> needRegisterData) {
        faceCheckDownModel.startToCheckDownFile(needRegisterData, faceCheckView);
    }

    /***
     * 注册人员信息
     * @param data
     */
    public void registeLocalFace(List<FaceInfo> data) {
        faceCheckDownModel.registeLocalFace(data, faceCheckView);
    }

    /****
     * 停止所有得请求，下载活动
     */
    public void stopCheckFaceStatues() {
        faceCheckDownModel.stopAllFaceInfo();
    }


}
