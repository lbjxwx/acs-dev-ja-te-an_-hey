package com.ys.acsdev.service.view;

public interface AcsUpdateImageListener {

    /***
     * 上传的进度
     * @param lastNum
     */
    void updateLocalAtttendPersent(int lastNum);

    /***
     *上传完成
     */
    void updateLocalAtttendSuccess();

}
