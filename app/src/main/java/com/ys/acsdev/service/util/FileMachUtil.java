package com.ys.acsdev.service.util;

import com.ys.acsdev.bean.PersonBean;
import com.ys.acsdev.commond.AppInfo;
import com.ys.acsdev.util.FileUtils;

import java.util.ArrayList;
import java.util.List;

public class FileMachUtil {

    /***
     * 判断列表文件是否需要下载
     * @param needRegisterData
     * @return
     */
    public static List<PersonBean> needDownFile(List<PersonBean> needRegisterData) {
        if (needRegisterData == null || needRegisterData.size() < 1) {
            return null;
        }
        List<PersonBean> personBeanList = new ArrayList<PersonBean>();
        for (int i = 0; i < needRegisterData.size(); i++) {
            PersonBean personBean = needRegisterData.get(i);
//            LogUtil.persoon("=====需要下载的文件的个数===111===" + personBean.toString());
            String perId = personBean.getPerId();
            String savePath = AppInfo.BASE_IMAGE_PATH + "/" + perId + ".jpg";
            String fileLength = personBean.getPhotoSize();
            boolean isFileExit = FileUtils.ifFaceFileHasExict(savePath, fileLength);
            if (!isFileExit) {
                personBeanList.add(personBean);
            }
        }
        return personBeanList;
    }

}

