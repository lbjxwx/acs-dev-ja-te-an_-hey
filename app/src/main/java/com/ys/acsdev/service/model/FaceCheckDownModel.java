package com.ys.acsdev.service.model;

import com.ys.acsdev.bean.FaceInfo;
import com.ys.acsdev.bean.PersonBean;
import com.ys.acsdev.service.view.FaceCheckView;

import java.util.List;

public interface FaceCheckDownModel {

    void stopAllFaceInfo();

    /***
     * 增量查询接口
     *1    人员管理
     *2    访客
     *3    黑名单
     *-1   所有得信息
     * @param faceCheckView
     */
    void getBindPersonBySn(int psrsonTag, FaceCheckView faceCheckView);

    /****
     * 获取人员信息
     * @param printTag
     */
    void getPersonFromWeb(String printTag, FaceCheckView faceCheckView);

    /***
     * 开始下载人员信息
     * @param needRegisterData
     * @param faceCheckView
     */
    void startToCheckDownFile(List<PersonBean> needRegisterData, FaceCheckView faceCheckView);

    /***
     * 注册人脸信息
     * @param data
     */
    void registeLocalFace(List<FaceInfo> data, FaceCheckView faceCheckView);
}
