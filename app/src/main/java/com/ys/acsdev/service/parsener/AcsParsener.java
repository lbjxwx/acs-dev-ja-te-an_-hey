package com.ys.acsdev.service.parsener;

import android.content.Context;
import android.util.Log;

import com.ys.acsdev.api.AppConfig;
import com.ys.acsdev.bean.LocalARBean;
import com.ys.acsdev.bean.PersonBean;
import com.ys.acsdev.commond.AppInfo;
import com.ys.acsdev.data.MySDCard;
import com.ys.acsdev.db.DataRepository;
import com.ys.acsdev.db.dao.LocalARDao;
import com.ys.acsdev.service.model.AcsModel;
import com.ys.acsdev.service.model.AcsModelmpl;
import com.ys.acsdev.service.view.AcsUpdateImageListener;
import com.ys.acsdev.service.view.AcsUpdateView;
import com.ys.acsdev.service.view.AcsView;
import com.ys.acsdev.util.FileUtils;
import com.ys.acsdev.util.LogUtil;
import com.ys.acsdev.util.NetWorkUtils;
import com.ys.acsdev.util.SimpleDateUtil;
import com.ys.acsdev.util.SpUtils;
import com.ys.acsdev.util.TimerUtil;

import java.io.File;

public class AcsParsener {

    Context context;
    AcsModel acsModel;

    public AcsParsener(Context context) {
        this.context = context;
        initAcsModel();
    }

    private void initAcsModel() {
        acsModel = new AcsModelmpl();
    }


    //插入通行记录
    public void insertAccessFaceRecordServiceParsener(
            Context context,
            PersonBean personBean,
            String passType,
            String validCode,
            String accessFilePath,
            String temperatureUpdate, String wearMask, String idCardNum, String personName, String qrCodeType) {
        LogUtil.update("=====insertAccessFaceRecordServiceParsener=====" + wearMask);
        if (context == null) {
            saveAccessInfoToLocalDb(personBean, passType, validCode, accessFilePath, temperatureUpdate, wearMask, idCardNum, personName, qrCodeType);
            return;
        }
        //单机模式，优先执行
        if (SpUtils.getWorkModel() == AppInfo.MODEL_STAND_ALONE) {
            if (personBean != null) {
                LogUtil.update("=====保存数据到本地=====" + personBean.toString());
            } else {
                LogUtil.update("=====保存数据到本地==null===");
            }
            saveAccessInfoToLocalDb(personBean, passType, validCode, accessFilePath, temperatureUpdate, wearMask, idCardNum, personName, qrCodeType);
            return;
        }
        if (!NetWorkUtils.isNetworkConnected(context)) {
            LogUtil.update("=====保存数据到本地==NetWorkUtils===> ");
            saveAccessInfoToLocalDb(personBean, passType, validCode, accessFilePath, temperatureUpdate, wearMask, idCardNum, personName, qrCodeType);
            return;
        }
        //没有链接上服务器
        if (!AppInfo.isConnectServer) {
            LogUtil.update("=====保存数据到本地==AppInfo.isConnectServer===");
            saveAccessInfoToLocalDb(personBean, passType, validCode, accessFilePath, temperatureUpdate, wearMask, idCardNum, personName, qrCodeType);
            return;
        }
        acsModel.insertAccessFaceRecordService(personBean, passType, validCode, accessFilePath, temperatureUpdate, wearMask, idCardNum, personName, qrCodeType);
    }


    public void saveAccessInfoToLocalDb(PersonBean personEntity, String passType, String validCode, String accessFilePath,
                                        String temperature, String wearMask, String idCardNum, String personName, String qrCodeType) {
        Log.e("update", "==========保存通行记录到离线记录");
        File accessFile = new File(accessFilePath);
        String deviceId = SpUtils.getDeviceBeanId();
        if (deviceId == null || deviceId.length() < 1) {
            deviceId = "";
        }
        String recordTime = SimpleDateUtil.formatTaskTimeShow(System.currentTimeMillis());
        String phone = AppInfo.DEFAULT_PHONE;
        String name = "";
        String cardNum = "";
        if (personEntity != null) {
            phone = personEntity.getPhone();
            if (phone == null || phone.length() < 2) {
                phone = AppInfo.DEFAULT_PHONE;
            }
            name = personEntity.getName();
            if (name == null || name.length() < 2) {
                name = "No Name";
            }
            Log.e("update", "=======000====保存服本地数据库状态==" + name);
            cardNum = personEntity.getCardNum();
        }

        String filePathSave = "";
        if (accessFile.exists()) {
            filePathSave = accessFile.getAbsolutePath();
        }
        //这里需要计算剩余内存，用剩余内存，减去 文件大小内存
        File fileSave = new File(filePathSave);
        boolean isSaveFaceImage = SpUtils.getUploadFace();
        if (!isSaveFaceImage) {
            //不保存人脸图片，这里执行删除
            if (fileSave != null && fileSave.exists()) {
                fileSave.delete();
            }
        }
        if (idCardNum != null && idCardNum.length() > 5) {
            cardNum = idCardNum;
        }
        if (name == null || name.length() < 2) {
            if (personName != null && personName.length() > 1) {
                name = personName;
            }
        }
        Log.e("update", "===========保存服本地数据库状态==" + name + " / " + cardNum);
        LocalARBean localBean = new LocalARBean(recordTime, passType, phone, deviceId, validCode, filePathSave, temperature, name, cardNum, wearMask, qrCodeType);
        boolean isSave = LocalARDao.saveLocalBean(localBean);
        Log.e("update", "===========保存服本地数据库状态==" + isSave);
        return;
    }

    /***
     * 插入同行记录， 刷卡刷身份证，都走这里
     * @param person
     * @param passType
     * @param perId
     */
    public void insertAccessCardRecordService(PersonBean person, String passType, String perId, String cardCode) {
        acsModel.insertAccessCardRecordService(person, passType, perId, cardCode);
    }

    /***
     * 夜间本地数据到服务器
     */
    public void updateFaceLocalInfoToWeb(boolean jumpTime, AcsUpdateView acsUpdateView) {
        if (SpUtils.getWorkModel() == AppInfo.MODEL_STAND_ALONE) {
            updateFailed(acsUpdateView, "Single Model,Please Check!");
            return;
        }
        if (!AppInfo.isConnectServer) {
            updateFailed(acsUpdateView, "Current Not Connect Server !");
            logLocalSttendInfo("======开始同步本地数据=======未连接服务器=");
            return;
        }
        boolean isRightTime = TimerUtil.isUpdateAcrodToTime();
        logLocalSttendInfo("======开始同步本地数据=======时间是否合格=" + isRightTime);
        if (!isRightTime) {
            updateFailed(acsUpdateView, "Current Is Not Right Time!");
            return;
        }
        if (isUpdateFaceImage) {
            logLocalSttendInfo("======当前正在同步，中断操作");
            updateFailed(acsUpdateView, "Current Updateing...");
            return;
        }
        isUpdateFaceImage = true;
        if (jumpTime) {
            logLocalSttendInfo("手动点击，直接同步");
            updateFaceLocalInfoToWebNoTime(acsUpdateView);
            return;
        }
        //软件自动同步信息
        int currenTime = SimpleDateUtil.getCurrentHourMin();
        //每10分钟上传一次
        if (currenTime % 10 != 0) {
            updateFailed(acsUpdateView, "时间不合法，中断上传");
            logLocalSttendInfo("======开始同步本地数据=======时间不合格=");
            return;
        }
        updateFaceLocalInfoToWebNoTime(acsUpdateView);
    }

    /***
     * 直接同步数据-上传到服务器
     */
    private void updateFaceLocalInfoToWebNoTime(AcsUpdateView acsUpdateView) {
        logLocalSttendInfo("======开始同步本地数据=======111=");
        FileUtils.delOldSaveLog();
        acsModel.syncAccessRecordLocal(new AcsUpdateImageListener() {
            @Override
            public void updateLocalAtttendPersent(int lastNum) {
                updateFilePersent(acsUpdateView, lastNum);
            }

            @Override
            public void updateLocalAtttendSuccess() {
                updateFileSuccess(acsUpdateView);
            }
        });
        DataRepository.getInstance().syncAttendRecord();
    }


    private static boolean isUpdateFaceImage = false;

    /***
     * 上传失败回调
     * @param acsUpdateView
     * @param desc
     */
    private void updateFailed(AcsUpdateView acsUpdateView, String desc) {
        isUpdateFaceImage = false;
        if (acsUpdateView == null) {
            return;
        }
        acsUpdateView.updateFailed(desc);
    }

    /***
     * 上传进度
     * @param acsUpdateView
     * @param lastNum
     */
    private void updateFilePersent(AcsUpdateView acsUpdateView, int lastNum) {
        if (lastNum == 0) {
            isUpdateFaceImage = false;
        } else {
            isUpdateFaceImage = true;
        }
        if (acsUpdateView == null) {
            return;
        }
        acsUpdateView.updateLocalAtttendPersent(lastNum);
    }

    /***
     * 上传全部完成
     * @param acsUpdateView
     */
    private void updateFileSuccess(AcsUpdateView acsUpdateView) {
        isUpdateFaceImage = false;
        if (acsUpdateView == null) {
            return;
        }
        acsUpdateView.updateLocalAtttendSuccess();
    }


    private void logLocalSttendInfo(String desc) {
        LogUtil.persoon("同步本地的通行记录信息: " + desc);
    }

    public void getMainViewLogo() {
        if (!NetWorkUtils.isNetworkConnected(context)) {
            LogUtil.cdl("======没有网络，不请求logo==");
            return;
        }
        if (!AppInfo.isConnectServer) {
            LogUtil.cdl("======服务器未连接，不请求logo==");
            return;
        }
        acsModel.getMainViewLogo(context);
    }

    /***
     * 上传日志到入服务器
     */
    public void uploadRunLogToServer() {
        acsModel.uploadRunLogToServer();
    }

    public void updateTaskDoanProgress(String uniquePsuedoID, String taskId, String speed, int progress) {
        acsModel.updateTaskDoanProgress(uniquePsuedoID, taskId, speed, progress);
    }

    /***
     * 提交设备信息到统计服务器
     */
    public void upodateDevInfoToAuthorServer(String version) {
        acsModel.upodateDevInfoToAuthorServer(version);
    }

    public void checkAppUpdate() {
        acsModel.checkAppUpdate(context, null);
    }

    public void stopDownApk() {
        acsModel.stopDownApk();
    }

    /***
     * 获取当前的系统时间
     */
    public void getCurrentSystemTime(AcsView acsView) {
        acsModel.getCurrentSystemTime(context, acsView);
    }
}
