package com.ys.acsdev.service.view;

import com.ys.acsdev.bean.DeviceBean;

import java.util.List;

public interface SocketServerView {

    void getDeviceInfoStatues(boolean isTrue, int code, List<DeviceBean> lisst, String errorDesc);
}
