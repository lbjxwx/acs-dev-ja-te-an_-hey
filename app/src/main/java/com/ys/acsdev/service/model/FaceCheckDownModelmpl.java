package com.ys.acsdev.service.model;

import com.google.gson.Gson;
import com.yisheng.facerecog.net.HttpObserver;
import com.ys.acsdev.api.ApiHelper;
import com.ys.acsdev.api.AppConfig;
import com.ys.acsdev.bean.FaceInfo;
import com.ys.acsdev.bean.PersonBean;
import com.ys.acsdev.bean.RegisterError;
import com.ys.acsdev.bean.RegisterRequestBody;
import com.ys.acsdev.commond.AppInfo;
import com.ys.acsdev.listener.FaceRegisterListener;
import com.ys.acsdev.runnable.down.DownFileEntity;
import com.ys.acsdev.runnable.down.DownRunnable;
import com.ys.acsdev.runnable.down.DownStateListener;
import com.ys.acsdev.runnable.face.FaceRegisterRunnable;
import com.ys.acsdev.service.AcsService;
import com.ys.acsdev.service.FaceCheckDownService;
import com.ys.acsdev.service.util.FileMachUtil;
import com.ys.acsdev.service.view.FaceCheckView;
import com.ys.acsdev.util.CodeUtil;
import com.ys.acsdev.util.FileUtils;
import com.ys.acsdev.util.LogUtil;
import com.ys.acsdev.util.SimpleDateUtil;
import com.ys.acsdev.util.SpUtils;
import com.ys.acsdev.util.custom.YktValidate;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import okhttp3.Call;

public class FaceCheckDownModelmpl implements FaceCheckDownModel {

    /***
     * 人员增量查询
     * @param faceCheckView
     */
    @Override
    public void getBindPersonBySn(int personTag, FaceCheckView faceCheckView) {
        FaceCheckDownService.ISFACECHECKRUNNORMAL = true;
        String requestUrl = AppInfo.getBindPersonBySnUrl();
        if (requestUrl == null || requestUrl.length() < 5) {
            backRequestStatuesInfo(faceCheckView, false, "url is null or Error", personTag);
            return;
        }
        //设置开始请求得时间
        SpUtils.setStartTime(System.currentTimeMillis());
        String devCode = CodeUtil.getUniquePsuedoID();
        //时间往前推2分钟 。防止
        long lastUpdateTime = SpUtils.getUpdatePersonTime() - (2 * 1000);
        LogUtil.persoon("===增量查询人员信息，时间====" + lastUpdateTime + " / " + requestUrl + " / " + devCode);
        Map<String, String> params = new LinkedHashMap<String, String>();
//        http://192.168.1.76:8085/cloudIntercom/api/getBindPersonBySn?sn=301F9A8277BE&personType=0&time=1608966000049
        params.put("sn", devCode);
        params.put("time", lastUpdateTime + "");
        params.put("personType", "0");  // 人员类型筛选：0=加载所有人员，1=员工，2=访客，3=黑名单
        OkHttpUtils
                .post()
                .url(requestUrl)
                .params(params)
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, String e, int id) {
                        LogUtil.persoon("=====请求人脸信息失败====" + e);
                        backRequestStatuesInfo(faceCheckView, false, "request failed:check netWork", personTag);
                    }

                    @Override
                    public void onResponse(String json, int id) {
                        if (json == null || json.length() < 4) {
                            backRequestStatuesInfo(faceCheckView, false, "json==null", personTag);
                            return;
                        }
                        LogUtil.persoon("=====请求人脸信息成功====" + json);
                        try {
                            JSONObject jsonObject = new JSONObject(json);
                            int code = jsonObject.getInt("code");
                            String msg = jsonObject.getString("msg");
                            if (code != 0) {
                                backRequestStatuesInfo(faceCheckView, false, msg, personTag);
                                return;
                            }
                            String data = jsonObject.getString("data");
                            if (data == null || data.length() < 5) {
                                backRequestStatuesInfo(faceCheckView, true, "", personTag);
                                return;
                            }
                            backRequestStatuesInfo(faceCheckView, true, data, personTag);
                        } catch (Exception e) {
                            LogUtil.persoon("=====请求人脸信息成功=解析异常===" + e.toString());
                            e.printStackTrace();
                        }
                    }
                });
    }

    /***
     * 请求人脸信息
     * @param printTag
     * @param faceCheckView
     */
    @Override
    public void getPersonFromWeb(String printTag, FaceCheckView faceCheckView) {
        FaceCheckDownService.ISFACECHECKRUNNORMAL = true;
//        http://139.196.252.129:39018/cloudIntercom/api/selectBindPersonBySn?sn=301F9A68F92D
        String requestUrl = AppInfo.selectBindPersonBySn();
        if (requestUrl == null || requestUrl.length() < 5) {
            backRequestStatuesInfo(faceCheckView, false, "url is null or Error", AppInfo.FACE_REGISTER_PERSON_ALL);
            return;
        }
        String devCode = CodeUtil.getUniquePsuedoID();
        Map<String, String> params = new LinkedHashMap<String, String>();
        params.put("sn", devCode);
        LogUtil.persoon("=====请求人脸信息URK====" + requestUrl + "?sn=" + devCode);

        OkHttpUtils
                .post()
                .url(requestUrl)
                .params(params)
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, String e, int id) {
                        LogUtil.persoon("=====请求人脸信息失败====" + e.toString());
                        backRequestStatuesInfo(faceCheckView, false, "request failed:check netWork", AppInfo.FACE_REGISTER_PERSON_ALL);
                    }

                    @Override
                    public void onResponse(String json, int id) {
                        if (json == null || json.length() < 4) {
                            backRequestStatuesInfo(faceCheckView, false, "json==null", AppInfo.FACE_REGISTER_PERSON_ALL);
                            return;
                        }
                        LogUtil.persoon("=====请求人脸信息成功====" + json);
                        try {
                            JSONObject jsonObject = new JSONObject(json);
                            int code = jsonObject.getInt("code");
                            String msg = jsonObject.getString("msg");
                            if (code != 0) {
                                backRequestStatuesInfo(faceCheckView, false, msg, AppInfo.FACE_REGISTER_PERSON_ALL);
                                return;
                            }
                            String data = jsonObject.getString("data");
                            if (data == null || data.length() < 5) {
                                backRequestStatuesInfo(faceCheckView, true, "", AppInfo.FACE_REGISTER_PERSON_ALL);
                                return;
                            }
                            backRequestStatuesInfo(faceCheckView, true, data, AppInfo.FACE_REGISTER_PERSON_ALL);
                        } catch (Exception e) {
                            LogUtil.persoon("=====请求人脸信息成功=解析异常===" + e.toString());
                            e.printStackTrace();
                        }
                    }
                });
    }

    private void backRequestStatuesInfo(FaceCheckView faceCheckView, boolean isTrue, String errorDesc, int personTag) {
        if (faceCheckView == null) {
            return;
        }
        faceCheckView.backRequestFaceInfo(isTrue, errorDesc, personTag);
    }

    @Override
    public void stopAllFaceInfo() {
        FaceCheckDownService.setFaceCurrentStatues(FaceCheckDownService.FACE_STATUES_DEFAULT, "中断操作，回复初始化地位");
        FaceCheckDownService.ISFACECHECKRUNNORMAL = false;
    }

    //用来封装需要下载得轮询列表
    List<PersonBean> faceDownList = new ArrayList<PersonBean>();
    //用来封装需要返回得数据
    List<PersonBean> faceBackList = new ArrayList<PersonBean>();

    /***
     * 开始检查下载文件
     * @param needRegisterData
     * @param faceCheckView
     */
    @Override
    public void startToCheckDownFile(List<PersonBean> needRegisterData, FaceCheckView faceCheckView) {
        FaceCheckDownService.getInstance().setFaceCurrentStatues(FaceCheckDownService.FACE_STATUES_DOWNFILE, "准备下载");
        if (faceDownList != null && faceDownList.size() > 0) {
            faceDownList.clear();
        }
        faceBackList = needRegisterData;
        //过滤已经下载得文件
        faceDownList = FileMachUtil.needDownFile(needRegisterData);
        startToDownFileOneByOne(faceCheckView);
    }

    DownRunnable downRunnable = null;

    private void startToDownFileOneByOne(FaceCheckView faceCheckView) {
        if (!FaceCheckDownService.ISFACECHECKRUNNORMAL) {
            LogUtil.persoon("=====用户中止行为====");
            return;
        }
        if (faceDownList == null || faceDownList.size() < 1) {
            backRegisterFaceInfoToMainView(faceCheckView);
            return;
        }
        LogUtil.persoon("===剩余下载得文件个数====" + faceDownList.size(), true);
        PersonBean personBean = faceDownList.get(0);
        //如果数据==null直接去下载下一个
        if (personBean == null) {
            faceDownList.remove(0);
            startToDownFileOneByOne(faceCheckView);
            return;
        }
        String downPath = personBean.getPhotoUrl();
        LogUtil.persoon("===剩余下载得文件个数====" + faceDownList.size() + " / " + downPath);
        //下载地址异常，直接跳过
        if (downPath == null || downPath.length() < 5) {
            faceDownList.remove(0);
            startToDownFileOneByOne(faceCheckView);
            return;
        }
        //过滤掉非图片得文件，或者没有文件得文件
        if (!downPath.endsWith("jpg") && !downPath.endsWith("png")) {
            faceDownList.remove(0);
            startToDownFileOneByOne(faceCheckView);
            return;
        }
        //局域网需要拼接网址
        if (!downPath.contains("http")) {
            downPath = AppInfo.getBaseUrl() + downPath;
        }
        if (downRunnable != null) {
            downRunnable.stopDown();
        }
        String fileSaveName = personBean.getPerId() + ".jpg";
        String savePath = AppInfo.BASE_IMAGE_PATH + "/" + fileSaveName;
        if (downRunnable == null) {
            downRunnable = new DownRunnable();
        }
        LogUtil.persoon("===剩余下载得文件个数=下载路径===" + downPath + " / " + savePath);
        downRunnable.setDownInfo(downPath, savePath, fileSaveName, new DownStateListener() {
            @Override
            public void downStateInfo(DownFileEntity entity) {
                int downStatues = entity.getDownState();
                int progress = entity.getProgress();
                int downSpeed = entity.getDownSpeed();
                LogUtil.down("========下载状态=progress===" + progress + " /speed= " + downSpeed);
                if (downStatues == DownFileEntity.DOWN_STATE_SUCCESS) {
                    //下载成功,直接下载下一个
                    if (faceDownList != null && faceDownList.size() > 0) {
                        faceDownList.remove(0);
                    }
                    startToDownFileOneByOne(faceCheckView);

                } else if (downStatues == DownFileEntity.DOWN_STATE_FAIED) {
                    //下载失败，去下载下一个，并且删除下载失败的文件
                    if (faceDownList != null && faceDownList.size() > 0) {
                        faceDownList.remove(0);
                    }
                    startToDownFileOneByOne(faceCheckView);
                } else if (downStatues == DownFileEntity.DOWN_STATE_PROGRESS) {
                }
            }
        });
        downRunnable.setIsDelFile(true);
        downRunnable.setLimitDownSpeed(AppConfig.DOWN_SPEED_LIMIT);
        AcsService.getInstance().executor(downRunnable);
    }

    /**
     * 下载全部完成了，这里返回下载成功得信息给界面，用来注册
     *
     * @param faceCheckView
     */
    private void backRegisterFaceInfoToMainView(FaceCheckView faceCheckView) {
        if (faceCheckView == null) {
            return;
        }
        if (faceBackList == null || faceBackList.size() < 1) {
            faceCheckView.downFaceFileBack(false, null, "没有需要下载得文件", "==faceBackList==null");
            return;
        }
        List<FaceInfo> faceInfos = new ArrayList<FaceInfo>();
        for (PersonBean personBean : faceBackList) {
            String name = personBean.getName();
            String faceToken = "";
            String personId = personBean.getPerId();
            String path = AppInfo.BASE_IMAGE_PATH + "/" + personBean.getPerId() + ".jpg";
            String faceSize = personBean.getPhotoSize();
            int type = personBean.getType();
            boolean isFileExict = FileUtils.ifFaceFileHasExict(path, faceSize);
            LogUtil.persoon("=========判断文件是否存在====" + isFileExict + " / " + personBean.toString());
            String photoUrl = personBean.getPhotoUrl();
            if (isFileExict) {
                FaceInfo faceInfo = new FaceInfo(name, faceToken, personId, path, type, photoUrl, photoUrl);
                faceInfos.add(faceInfo);
            }
        }
        faceCheckView.downFaceFileBack(true, faceInfos, "下载完成", "====下载完成==界面返回==");
    }

    List<FaceInfo> faceInfosLocal = null;
    ArrayList<RegisterError> registerErrors = new ArrayList<>();

    @Override
    public void registeLocalFace(List<FaceInfo> data, FaceCheckView faceCheckView) {
        if (data == null || data.size() < 1) {
            return;
        }
        faceInfosLocal = data;
        registerErrors.clear();
        FaceCheckDownService.getInstance().setFaceCurrentStatues(FaceCheckDownService.FACE_STATUES_FACE_REGISTER, "准备注册");
        registerFaceOneByOne(faceCheckView);
    }

    FaceRegisterRunnable faceRegisterRunnable = null;

    private void registerFaceOneByOne(FaceCheckView faceCheckView) {
        if (faceInfosLocal == null || faceInfosLocal.size() < 1) {
            if (AppConfig.APP_PARSENE_TYPE == AppConfig.PARSENE_ADD_TYPE) {
                SpUtils.setUpdatePersonTime(SpUtils.getStartTime(), "注册完毕，修改下一次得请求时间戳");
            }
            //这里表示注册完毕了，去执行下一步的操作
            LogUtil.persoon("=======全部注册完毕了====registerFaceOneByOne");
            if (faceCheckView != null) {
                faceCheckView.registerFaceInfoCompaintBack();
            }
            if (registerErrors != null && registerErrors.size() > 0) {
                LogUtil.persoon("==========putFaceRegFail: " + registerErrors.size());
                RegisterRequestBody requestBody = new RegisterRequestBody(CodeUtil.getUniquePsuedoID(), registerErrors.toArray(new RegisterError[registerErrors.size()]));
                String json = new Gson().toJson(requestBody);
                LogUtil.persoon("==========requestBody: " + json);
                ApiHelper.putFaceRegFail(json).subscribeOn(Schedulers.io()).observeOn(Schedulers.io()).subscribe(new HttpObserver<String>() {
                    @Override
                    public void onSuccess(@Nullable String s) {
                        LogUtil.persoon("==========putFaceRegFail result: " + s);
                    }

                    @Override
                    public void onFailed(int code, @NotNull String msg) {
                        LogUtil.persoon("==========putFaceRegFail failed: " + code + " / " + msg);
                    }
                });
            }
            return;
        }
        LogUtil.persoon("=======准备注册，剩余注册的数量====" + faceInfosLocal.size());
        FaceInfo info = faceInfosLocal.get(0);
        File localFile = new File(AppInfo.BASE_IMAGE_PATH + "/" + info.getPersonId() + ".jpg");
        if (!localFile.exists()) {
            gotoRegisteNextFaceInfo(faceCheckView);
            return;
        }
        if (faceRegisterRunnable == null) {
            faceRegisterRunnable = new FaceRegisterRunnable();
        }
        LogUtil.persoon("===registerFaceOneByOne=开始注册==registerTag = " + " / " + info.toString());
        faceRegisterRunnable.setRegiFaceInfo(info, localFile, new FaceRegisterListener() {

            @Override
            public void registerFaceStatues(boolean isTrue, int code, String msg) {
                if (!isTrue) {
                    LogUtil.persoon("注册失败: " + code + " /msg =" + msg);
                    registerErrors.add(new RegisterError(info.getPersonId(), msg));
                }
                gotoRegisteNextFaceInfo(faceCheckView);
            }
        });
        AcsService.getInstance().executor(faceRegisterRunnable);
    }

    private void gotoRegisteNextFaceInfo(FaceCheckView faceCheckView) {
        if (!FaceCheckDownService.ISFACECHECKRUNNORMAL) {
            LogUtil.persoon("=====用户中止行为====");
            return;
        }
        if (faceInfosLocal == null || faceInfosLocal.size() < 1) {
            if (AppConfig.APP_PARSENE_TYPE == AppConfig.PARSENE_ADD_TYPE) {
                SpUtils.setUpdatePersonTime(SpUtils.getStartTime(), "注册完毕，写入下一次得请求时间戳");
            }
            //这里表示注册完毕了，去执行下一步的操作
            LogUtil.persoon("=======全部注册完毕了====gotoRegisteNextFaceInfo");
            return;
        }
        faceInfosLocal.remove(0);
        registerFaceOneByOne(faceCheckView);
    }

}
