package com.ys.acsdev.service;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;

import com.ys.acsdev.api.AppConfig;
import com.ys.acsdev.bean.FaceInfo;
import com.ys.acsdev.bean.PersonBean;
import com.ys.acsdev.commond.AppInfo;
import com.ys.acsdev.db.dao.FaceInfoJavaDao;
import com.ys.acsdev.db.dao.PersonDao;
import com.ys.acsdev.listener.FaceSyncInfoBackListener;
import com.ys.acsdev.runnable.face.FaceManagerUtil;
import com.ys.acsdev.runnable.face.FaceSyncLocalRunnable;
import com.ys.acsdev.service.parsener.FaceCheckDownParsener;
import com.ys.acsdev.service.util.PersonAddJsonRunnable;
import com.ys.acsdev.service.util.PersonJsonRunnable;
import com.ys.acsdev.service.view.FaceCheckView;
import com.ys.acsdev.util.LogUtil;
import com.ys.acsdev.util.NetWorkUtils;
import com.ys.acsdev.util.SpUtils;

import java.util.List;

/***
 * 用来检测人员信息以及下载，注册相关动作
 */
public class FaceCheckDownService extends Service implements FaceCheckView {

    public static FaceCheckDownService mInstance;

    public static FaceCheckDownService getInstance() {
        if (mInstance == null) {
            synchronized (FaceCheckDownService.class) {
                if (mInstance == null) {
                    mInstance = new FaceCheckDownService();
                }
            }
        }
        return mInstance;
    }

    /***
     * 1    人员管理
     * 2    访客
     * 3    黑名单
     * -1   所有得信息
     */
    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(AppInfo.START_CHECK_FACE_DOWN_REGISTER)) {
                int psrsonTag = intent.getIntExtra(AppInfo.START_CHECK_FACE_DOWN_REGISTER, AppInfo.FACE_REGISTER_PERSON_ALL);
                LogUtil.persoon("service接收到同步人员得广播==" + psrsonTag);
                getPersonFromWeb(psrsonTag, "接受到广播，开始操作,请求员工信息");
            } else if (action.equals(AppInfo.STOP_REQUEST_DOWN_FACE_IMAGE)) {
                //停止下载人员信息
                stopDownOrRegisterFaceInfo();
            }
        }
    };

    public void stopDownOrRegisterFaceInfo() {
        faceCheckDownParsener.stopCheckFaceStatues();
    }

    public static final int FACE_STATUES_DEFAULT = -1;
    public static final int FACE_STATUES_REQUEST = 1;  //请求状态
    public static final int FACE_STATUES_MATH_LOCAL = 2;  //检查差异性状态
    public static final int FACE_STATUES_DOWNFILE = 3;  //下载状态
    public static final int FACE_STATUES_FACE_REGISTER = 4;  //注册状态
    //获取当前得状态
    public static int faceCurrentStatues = FACE_STATUES_DEFAULT;
    /****
     * 用来运行或者停止当前行为得
     */
    public static boolean ISFACECHECKRUNNORMAL = false;

    /****
     * 设置当前的状态
     * @param faceStatues
     */
    public static void setFaceCurrentStatues(int faceStatues, String printTag) {
        faceCurrentStatues = faceStatues;
        LogUtil.persoon("====当前的状态===" + faceStatues + " / " + printTag);
    }

    public int getFaceCurrentStatues() {
        String currentShow = "";
        switch (faceCurrentStatues) {
            case FACE_STATUES_DEFAULT:
                currentShow = "";
                break;
            case FACE_STATUES_REQUEST:  //请求状态
                currentShow = "请求状态";
                break;
            case FACE_STATUES_MATH_LOCAL:  //检查差异性状态
                currentShow = "检查差异性状态";
                break;
            case FACE_STATUES_DOWNFILE:  //下载状态
                currentShow = "下载状态";
                break;
            case FACE_STATUES_FACE_REGISTER:  //注册状态
                currentShow = "注册状态";
                break;
        }
        LogUtil.persoon("====当前的状态===" + currentShow);
        return faceCurrentStatues;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        initOther();
        initReceiver();
    }

    FaceCheckDownParsener faceCheckDownParsener;

    private void initOther() {
        if (faceCheckDownParsener == null) {
            faceCheckDownParsener = new FaceCheckDownParsener(FaceCheckDownService.this, this);
        }
    }

    @Override
    public void registerFaceInfoCompaintBack() {
        setFaceCurrentStatues(FACE_STATUES_DEFAULT, "注册完毕,回复状态");
        LogUtil.persoon("====人员注册完毕tag==");
    }

    /***
     * 获取人员信息
     * @param printTag
     * //1    人员管理
     * //2    访客
     * //3    黑名单
     * //-1   所有得信息
     */
    private void getPersonFromWeb(int psrsonTag, String printTag) {
        if (SpUtils.getWorkModel() == AppInfo.MODEL_STAND_ALONE) {
            return;
        }
        if (getFaceCurrentStatues() != FACE_STATUES_DEFAULT) {
            LogUtil.persoon("=当前的状态==请求人脸信息,状态不对，中断操作===");
            return;
        }
        setFaceCurrentStatues(FACE_STATUES_DEFAULT, "getPersonFromWeb");
        LogUtil.persoon("请求人脸信息===" + printTag);
        if (!NetWorkUtils.isNetworkConnected(FaceCheckDownService.this)) {
            LogUtil.persoon("没有网络，不去更新人脸信息");
            return;
        }
        if (!AppInfo.isConnectServer) {
            LogUtil.persoon("没有网络，没有连接到服务器");
            return;
        }
        setFaceCurrentStatues(FACE_STATUES_REQUEST, "getPersonFromWeb");
        initOther();
        if (AppConfig.APP_PARSENE_TYPE == AppConfig.PARSENE_ALL_TYPE) {
            //全量查询
            faceCheckDownParsener.getPersonFromWeb(printTag);
        } else {
            //增量查询
            faceCheckDownParsener.getBindPersonBySn(psrsonTag);
        }
    }

    /***
     * 返回请求人员信息
     * @param isStatues
     * @param data
     * @param personTag
     * 解析类型
     */
    @Override
    public void backRequestFaceInfo(boolean isStatues, String data, int personTag) {
        setFaceCurrentStatues(FACE_STATUES_DEFAULT, "请求人员信息返回来了");
        if (!isStatues) {
            return;
        }
        setFaceCurrentStatues(FACE_STATUES_REQUEST, "准备解析，默认请求状态");
        Runnable runnable = null;
        if (AppConfig.APP_PARSENE_TYPE == AppConfig.PARSENE_ALL_TYPE) {
            //全量查询
            runnable = new PersonJsonRunnable(data, this);
        } else {
            //增量查询
            runnable = new PersonAddJsonRunnable(data, this, personTag);
        }
        AcsService.getInstance().executor(runnable);
    }

    /***
     * 请求人脸信息，返回得人员列表
     * @param listPerson
     * @param errorDesc
     */
    @Override
    public void backFaceListInfo(List<PersonBean> listPerson, String errorDesc) {
        setFaceCurrentStatues(FACE_STATUES_DEFAULT, "请求人脸信息，返回得人员列表");
        LogUtil.persoon("===返回得人脸列表信息=人员=" + errorDesc);
        if (AppConfig.APP_PARSENE_TYPE == AppConfig.PARSENE_ALL_TYPE) {
            //全量查询
            if (listPerson == null || listPerson.size() < 1) {
                deleteLocalFaceInfo();
                return;
            }
            LogUtil.persoon("===返回得人脸列表信息，全量查询==" + listPerson.size());
            startRegister(listPerson);
        } else {
            //增量查询
            if (listPerson == null || listPerson.size() < 1) {
                SpUtils.setUpdatePersonTime(SpUtils.getStartTime(), "解析完成，没有需要同步得数据，更新时间");
                return;
            }
            LogUtil.persoon("===返回得人脸列表信息,增量查询==" + listPerson.size());
            startRegisterAddPerson(listPerson);
        }
    }

    /***
     * 增量查询得注册逻辑
     * 1：便利数据，删除本地现有得数据，
     * 2：更新得数据，全部重新下载，注册
     * @param listPerson
     */
    private void startRegisterAddPerson(List<PersonBean> listPerson) {
        setFaceCurrentStatues(FACE_STATUES_MATH_LOCAL, "增量查询，开始检查差异性");
        for (PersonBean personBean : listPerson) {
            String personId = personBean.getPerId();
            deletePersonFeromDb(personBean);
        }
        setFaceCurrentStatues(FACE_STATUES_DEFAULT, "增量查询，不需要检查差异性");
        initOther();
        faceCheckDownParsener.startToCheckDownFile(listPerson);
    }

    private void deletePersonFeromDb(PersonBean personBean) {
        if (personBean == null) {
            return;
        }
        try {
            boolean isSave = PersonDao.insertPerson(personBean);
            LogUtil.persoon("====更新已经有得信息==" + isSave);
            String perId = personBean.getPerId();
            boolean isDel = FaceInfoJavaDao.deleteById(perId, "增量查询数据移除人员信息:" + perId);
            LogUtil.persoon("====删除人员信息状态==" + isDel);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //==================================================================================================================

    /*****
     * 开始注册员工
     * 同步差异化数据
     * @param datas
     */
    private void startRegister(List<PersonBean> datas) {
        setFaceCurrentStatues(FACE_STATUES_MATH_LOCAL, "开始检查差异性");
        FaceSyncLocalRunnable runnable = new FaceSyncLocalRunnable(datas, new FaceSyncInfoBackListener() {
            @Override
            public void backFaceNeedDownList(boolean isTrue, List<PersonBean> needRegisterData, String errorDesc) {
                setFaceCurrentStatues(FACE_STATUES_DEFAULT, "差异性检查完毕");
                if (!isTrue) {
                    LogUtil.persoon("=====同步数据= ==" + errorDesc);
                    return;
                }
                if (needRegisterData == null || needRegisterData.size() < 1) {
                    LogUtil.persoon("=====同步数据= 需要下载的文件的个数==0==");
                    return;
                }
                LogUtil.persoon("=====同步数据= 需要下载的文件的个数====" + needRegisterData.size());
                initOther();
                faceCheckDownParsener.startToCheckDownFile(needRegisterData);
            }

        });
        AcsService.getInstance().executor(runnable);
    }

    /***
     * 下载状态返回
     * @param isDown
     * @param errorDesc
     */
    @Override
    public void downFaceFileBack(boolean isDown, List<FaceInfo> data, String errorDesc, String tag) {
        LogUtil.persoon("====需要注册得信息===downFaceFileBack ==" + isDown + "/ " + errorDesc + " / " + tag);
        setFaceCurrentStatues(FACE_STATUES_DEFAULT, "下载完毕，准备去注册");
        if (data == null || data.size() < 1) {
            LogUtil.persoon("====需要注册得信息===data == null");
            return;
        }
        LogUtil.persoon("====需要注册得信息===开始注册===" + data.size());
        registerFace(data);
    }

    //人脸注册,下载完后调用
    private void registerFace(List<FaceInfo> data) {
        if (data == null || data.size() < 1) {
            return;
        }
        for (int i = 0; i < data.size(); i++) {
            LogUtil.persoon("====需要注册得信息===遍历查询===" + data.get(i).toString());
        }
        initOther();
        faceCheckDownParsener.registeLocalFace(data);
    }

//=============================下面是:祖传代码，不出问题就不用改的================================================================================

    /***
     * 获取服务器没有数据，清理本地数据
     */
    private void deleteLocalFaceInfo() {
        FaceManagerUtil.delAllLocalFaceInfo(new FaceManagerUtil.FaceManagerUtilListener() {
            @Override
            public void delPersonInfoBack() {

            }
        });
    }

    private void initReceiver() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(AppInfo.START_CHECK_FACE_DOWN_REGISTER);
        intentFilter.addAction(AppInfo.STOP_REQUEST_DOWN_FACE_IMAGE);  //停止请求或者下载人脸信息
        registerReceiver(receiver, intentFilter);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (receiver != null) {
            unregisterReceiver(receiver);
        }
    }
}
