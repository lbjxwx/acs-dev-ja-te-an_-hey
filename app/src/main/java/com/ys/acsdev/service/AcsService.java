package com.ys.acsdev.service;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.util.Log;

import com.card.CardInfo;
import com.card.OnCardListener;
import com.device.CardManager;
import com.hwangjr.rxbus.RxBus;
import com.nlp.cloundcbsappdemo.model.MessageEvent;
import com.ys.acsdev.MyApp;
import com.ys.acsdev.R;
import com.ys.acsdev.bean.PersonBean;
import com.ys.acsdev.commond.AppInfo;
import com.ys.acsdev.commond.CommondViewModel;
import com.ys.acsdev.db.DbTempCompensate;
import com.ys.acsdev.db.dao.FaceInfoJavaDao;
import com.ys.acsdev.db.dao.PersonDao;
import com.ys.acsdev.helper.CheckCameraHelper;
import com.ys.acsdev.helper.PlayLocalMusicUtil;
import com.ys.acsdev.helper.SoundHelper;
import com.ys.acsdev.light.LightManager;
import com.ys.acsdev.manager.DoorManager;
import com.ys.acsdev.manager.TempManager;
import com.ys.acsdev.manager.TtsManager;
import com.ys.acsdev.poweronoff.db.DbTimerLocalUtil;
import com.ys.acsdev.poweronoff.db.TimeLocalEntity;
import com.ys.acsdev.poweronoff.util.system.PowerOnOffUtil;
import com.ys.acsdev.service.parsener.AcsParsener;
import com.ys.acsdev.service.view.AcsUpdateView;
import com.ys.acsdev.service.view.AcsView;
import com.ys.acsdev.ui.sdcheck.SdCheckActivity;
import com.ys.acsdev.util.CodeUtil;
import com.ys.acsdev.util.FileUtils;
import com.ys.acsdev.util.LogUtil;
import com.ys.acsdev.util.NetWorkUtils;
import com.ys.acsdev.util.SimpleDateUtil;
import com.ys.acsdev.util.SpUtils;
import com.ys.acsdev.util.SystemManagerUtil;
import com.ys.acsdev.util.guardian.LeaderBarUtil;
import com.ys.acsdev.view.MyToastView;
import com.ys.facepasslib.FaceManager;
import com.ys.rkapi.Utils.TimeUtils;
import com.ys.zkidrlib.ZkidrManager;

import org.litepal.crud.callback.FindMultiCallback;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class AcsService extends Service implements AcsView {

    private static final int MSG_USB_IN = 1;
    private int CPU_COUNT = Runtime.getRuntime().availableProcessors();
    ExecutorService executor = Executors.newFixedThreadPool(CPU_COUNT * 2);
    private boolean mReadStart = false;

    public static AcsService mInstance;
    private static String cardInitMsg;

    public static AcsService getInstance() {
        if (mInstance == null) {
            synchronized (AcsService.class) {
                if (mInstance == null) {
                    mInstance = new AcsService();
                }
            }
        }
        return mInstance;
    }

    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(AppInfo.YS_CAMERA_ADD)) {
                LogUtil.e("=====接收到摄像头添加的广播", "CheckCamera");
                CheckCameraHelper.getInstance().rebootApp();
            } else if (action.equals(AppInfo.YS_CAMERA_REMOVE)) {
                LogUtil.e("=====接收到摄像头掉的广播", "CheckCamera");
                if (mReadStart) {
                    return;
                }
                mReadStart = true;
                CheckCameraHelper.getInstance().resetCamera();
            } else if (action.equals(AppInfo.RECEIVE_BROAD_CAST_LIVE)) {
                //守护进程广播
                updateGuardianMineAlive();
            } else if (action.equals(Intent.ACTION_TIME_TICK)) {
                //更新人脸信息广播
                updateFaceInfoToLocalFromWeb();
                //上传本地的遗留通行记录信息
                updateFaceLocalInfoToWeb();
                //更新温度补偿
                DbTempCompensate.getCurrentTempAdd();
                //更新访客库记录
                updateVisitorDbInfo();
                //更新本地时间
                updateTimeFromWeb();
            } else if (action.equals(Intent.ACTION_MEDIA_MOUNTED)) {
                //插入SD卡USB
                LogUtil.cdl("=========接收到U盘的插入广播", true);
                try {
                    String sdUsbPath = intent.getData().getPath();
                    if (sdUsbPath == null || sdUsbPath.length() < 3) {
                        showRToast(getString(R.string.sd_in_no_path));
                        LogUtil.e("cdl", "======fileName=====sdUsbPath==null=");
                        return;
                    }
                    AppInfo.U_PAN_PATH_CACHE = sdUsbPath;
                    LogUtil.e("==fileName=====sdUsbPath=" + sdUsbPath);
                    Message message = new Message();
                    message.what = MSG_USB_IN;
                    message.obj = sdUsbPath;
                    handler.sendMessageDelayed(message, 2000);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (action.equals(Intent.ACTION_MEDIA_EJECT)) {
                AppInfo.U_PAN_PATH_CACHE = "";
                //拔出来.SD usb
                LogUtil.cdl("=========接收到U盘的拔出广播", true);
            } else if (action.equals(ConnectivityManager.CONNECTIVITY_ACTION)) {
                //网络变化广播
                dealNetChange(context, intent);
            } else if (action.equals(AppInfo.CHECK_MAIN_VIEW_LOGO)) {
                //同步logo
                initOther();
                acsParsener.getMainViewLogo();
            } else if (action.equals(AppInfo.UPDATE_LOCAL_RUN_LOG_TO_WEB)) {
                //上传日志
                initOther();
                acsParsener.uploadRunLogToServer();
            } else if (action.equals(AppInfo.BROAD_CHECK_APP_UPDATE)) {
                checkAppUpdate();
            }
        }
    };

    @Override
    public void onCreate() {
        super.onCreate();
        Log.e("cdl", "=============card  oncreate");
        mInstance = this;
        handler.sendEmptyMessageDelayed(MESSAGE_UPDATE_TIME, 120 * 1000);
        SoundHelper.init();
        initCard();
        initReceiver();
        initTemp();
        initDoorManager();
    }

    private void initDoorManager() {
        DoorManager.getInstance().init();
    }

    private void initOther() {
        if (acsParsener == null) {
            acsParsener = new AcsParsener(AcsService.this);
        }
    }


    public static final int NOT_PLAY_LOCAL_MEDIA = -1;
    PlayLocalMusicUtil playLocalMusicUtil;

    public void startSpeakTts(String ttsMessage, int resourceId, String tag) {
        boolean isOpenTTS = SpUtils.getOpenTTSVoice();
        if (!isOpenTTS) {
            return;
        }
        if (resourceId == NOT_PLAY_LOCAL_MEDIA) {
            TtsManager.getInstance().speakText(ttsMessage);
            return;
        }
        if (playLocalMusicUtil == null) {
            playLocalMusicUtil = new PlayLocalMusicUtil(AcsService.this);
        }
        playLocalMusicUtil.startPlayRawMusic(resourceId);
    }

    //初始化测温模块
    private void initTemp() {
        TempManager.getInstance().init(SpUtils.getmTempType());
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        initVoice();
        return super.onStartCommand(intent, flags, startId);
    }

    private static final int MESSAGE_UPDATE_TIME = 563;

    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            handler.removeMessages(msg.what);
            switch (msg.what) {
                case MESSAGE_UPDATE_TIME:
                    //更新本地时间完成
                    LogUtil.cdl("==updateTomeFromWeb==" + SimpleDateUtil.formatCurrentTimeMin());
                    checkPowerOnOffTime();
                    break;
                case MSG_USB_IN:
                    String usbPath = (String) msg.obj;
                    if (usbPath == null || usbPath.length() < 3) {
                        return;
                    }
                    String apkPath = FileUtils.updateFileIsExict(AcsService.this, usbPath);
                    LogUtil.cdl("=======U盘文件====" + apkPath);
                    if (apkPath == null || apkPath.length() < 5) {
                        LogUtil.sdcard("U盘检测，升级文件不存在", true);
                        return;
                    }
                    Intent intent = new Intent(AcsService.this, SdCheckActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra(SdCheckActivity.TAG_CHECK_URL, apkPath);
                    startActivity(intent);
                    break;
            }
        }
    };

    /**
     * 上传未连服务器，
     * 保存本地的数据
     */
    private void updateFaceLocalInfoToWeb() {
        initOther();
        acsParsener.updateFaceLocalInfoToWeb(false, new AcsUpdateView() {
            @Override
            public void updateFailed(String desc) {

            }

            @Override
            public void updateLocalAtttendPersent(int lastNum) {

            }

            @Override
            public void updateLocalAtttendSuccess() {

            }
        });
    }

    AcsParsener acsParsener;

    /***
     * 插入人脸识别通行记录
     * @param personBean
     * @param passType
     * @param validCode
     * @param accessFilePath
     * @param temperature
     */
    public void insertAccessFaceRecordService(
            Context context,
            PersonBean personBean,
            String passType,
            String validCode,
            String accessFilePath,
            String temperature,
            String wearMask, String qrCodeType) {
        insertAccessFaceRecordService(context, personBean, passType, validCode, accessFilePath,
                temperature, wearMask, null, null, qrCodeType);
    }

    /***
     * 携带身份证信息提交
     * @param context
     * @param personBean
     * @param passType
     * @param validCode
     * @param accessFilePath
     * @param temperature
     * @param wearMask
     */
    public void insertAccessFaceRecordService(
            Context context,
            PersonBean personBean,
            String passType,
            String validCode,
            String accessFilePath,
            String temperature,
            String wearMask, String idCardNum, String personName, String qrCodeType) {
        try {
            boolean isUpdateFace = SpUtils.getUploadFace();
            if (!isUpdateFace) {
                //写一张默认得图片到本地，防止上传图片是null
                FileUtils.writeDefaultFaceImageToLocal(AcsService.this);
            }
            LogUtil.update("=====insertAccessFaceRecordService=====" + wearMask);
            initOther();
            acsParsener.insertAccessFaceRecordServiceParsener(context, personBean, passType, validCode,
                    accessFilePath, temperature, wearMask, idCardNum, personName, qrCodeType);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /***
     * 插入刷卡的通行记录
     * recordTime  记录时间
     * temperature
     * isFever
     */
    public void insertAccessCardRecordService(PersonBean person, String passType, String perId, String cardCode) {
        initOther();
        handler.post(new Runnable() {
            @Override
            public void run() {
                acsParsener.insertAccessCardRecordService(person, passType, perId, cardCode);
            }
        });
    }

    public void executor(Runnable runnable) {
        executor.execute(runnable);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    /***
     * 每隔10分钟同步人脸数据
     * 定时重启设备
     */
    private void updateFaceInfoToLocalFromWeb() {
        int currenTime = SimpleDateUtil.getCurrentHourMin();
        if (currenTime % 20 == 0) {
            LogUtil.persoon("====每隔十分钟同步人脸数据===" + currenTime);
            Intent intent = new Intent(AppInfo.START_CHECK_FACE_DOWN_REGISTER);
            intent.putExtra(AppInfo.START_CHECK_FACE_DOWN_REGISTER, AppInfo.FACE_REGISTER_PERSON_ALL);
            sendBroadcast(intent);
        }
    }


    /***
     * 初始化刷卡
     */
    //DkManager dkManager = null;
    private CardManager mReadCardManager;

    private void initCard() {
        if (!SpUtils.getInputCardEnable()) {
            LogUtil.cardManager("=======card  开关=");
            return;
        }
        if (SpUtils.getCardComType() == 0) {
            LogUtil.cardManager("=======初始化中控====");
            initCardZogCung();
            return;
        }
        LogUtil.cardManager("======初始化中控==DkManager===");
        String devPath = SpUtils.getCardTtysChooice();

        mReadCardManager = CardManager.getInstance();
        boolean carCardEnable  =SpUtils.getCarCardEnable();
        mReadCardManager.initCard(carCardEnable,devPath, new OnCardListener() {
            @Override
            public void onStart() {

            }

            @Override
            public void onRetry() {

            }

            @Override
            public void onCardLeave() {

            }

            @Override
            public void onCardInfo(CardInfo cardData) {
                parseIDCardResult(cardData);
            }

            @Override
            public void onCardFailure(String errorMsg) {
                RxBus.get().post(errorMsg);
            }

            @Override
            public void onInitError(String error) {
                cardInitMsg = error;
            }
        });

    }

    public static String getCardInitMsg() {
        String msg = cardInitMsg;
        cardInitMsg = null;
        return msg;
    }

    private void parseIDCardResult(CardInfo result) {
        LogUtil.e("=========onIDCardCallBack: getCompareEnable = " + SpUtils.getCompareEnable());
        if (faceCompaire || faceHealthyCompaire) {
            RxBus.get().post(result);
            return;
        }
        LogUtil.e("=========onIDCardCallBack: type = " + result.type + " /idCardNo =  " + result.cardNo);
        if (result.cardNo == null) {
            startSpeakTts(getString(R.string.invalid_card), "无效卡");
            showRToast(getString(R.string.invalid_card) + result.cardNo);
            return;
        }
        PersonBean person = PersonDao.getPersonByIdCard(result.cardNo);
        StringBuilder sb = new StringBuilder();
        if (person == null) {
            startSpeakTts(getString(R.string.card_unbind_person), "该卡未绑定");
            sb.append(getString(R.string.card_unbind_person) + "\n");
            sb.append(getString(R.string.tv_name) + ":" + result.name + "\n");
            sb.append(getString(R.string.tv_id_card) + ":" + result. cardNo + "\n");
            //sb.append(getString(R.string.tv_birthday) + ":" + result.Born + "\n");
            //sb.append(getString(R.string.tv_address) + ":" + result.Address);
            showRToast(sb.toString());
            return;
        }
        DoorManager.getInstance(). open("initCard==person=null");
        LogUtil.e("=========cardResult: " + person.toString());

        startSpeakTts(getString(R.string.read_card_success), "刷卡成功");
        sb.append(getString(R.string.read_card_success) + "\n");
        sb.append(getString(R.string.tv_name) + ":" + result.name + "\n");
        sb.append(getString(R.string.tv_id_card) + ":" + result.cardNo + "\n");
        //sb.append(getString(R.string.tv_birthday) + ":" + result.Born + "\n");
        //sb.append(getString(R.string.tv_address) + ":" + result.Address);

        showRToast(sb.toString());
        LogUtil.e("=========cardResult: idCardNo==" + result.cardNo);

        insertAccessCardRecordService(person, AppInfo.PASSTYPE_CARD, person.getPerId(), result.cardNo);
    }

    boolean faceCompaire = SpUtils.getCompareEnable();
    boolean faceHealthyCompaire = SpUtils.getHealthyCompareEnable();

    /***
     * 初始化中创刷卡器
     */
    private void initCardZogCung() {
        String devPath = SpUtils.getCardTtysChooice();
        ZkidrManager.getInstance().init(MyApp.getInstance(), devPath);
        ZkidrManager.getInstance().setZkidrListener(result -> {
            LogUtil.cardManager("=========onIDCardCallBack: type = " + result.type + " /idCardNo =  " + result.idCardNo);
            if (result.idCardNo == null || result.idCardNo.length() < 2) {
                startSpeakTts(getString(R.string.invalid_card), "无效卡");
                showRToast(getString(R.string.invalid_card) + result.idCardNo);
                return;
            }
            if (faceCompaire || faceHealthyCompaire) {
                RxBus.get().post(result);
                return;
            }

            PersonBean person = PersonDao.getPersonByIdCard(result.idCardNo);
            StringBuilder sb = new StringBuilder();
            if (person == null) {
                startSpeakTts(getString(R.string.card_unbind_person), "该卡未绑定");
                sb.append(getString(R.string.card_unbind_person) + "\n");
                sb.append(getString(R.string.tv_name) + ":" + result.name + "\n");
                sb.append(getString(R.string.tv_id_card) + ":" + result.idCardNo + "\n");
                sb.append(getString(R.string.tv_birthday) + ":" + result.birth + "\n");
                sb.append(getString(R.string.tv_address) + ":" + result.address);
                showRToast(sb.toString());
                return;
            }
            DoorManager.getInstance().open("initCard==person=null");
            LogUtil.cardManager("=========cardResult: " + person.toString());
            startSpeakTts(getString(R.string.read_card_success), "刷卡成功");
            sb.append(getString(R.string.read_card_success) + "\n");
            sb.append(getString(R.string.tv_name) + ":" + result.name + "\n");
            sb.append(getString(R.string.tv_id_card) + ":" + result.idCardNo + "\n");
            sb.append(getString(R.string.tv_birthday) + ":" + result.birth + "\n");
            sb.append(getString(R.string.tv_address) + ":" + result.address);
            showRToast(sb.toString());
            LogUtil.cardManager("=========cardResult: idCardNo==" + result.idCardNo);
            insertAccessCardRecordService(person, AppInfo.PASSTYPE_CARD, person.getPerId(), result.idCardNo);
        });
    }


    @Override
    public void onDestroy() {
        if (SpUtils.getCardComType() == 0) {
            ZkidrManager.getInstance().setZkidrListener(null);
            ZkidrManager.getInstance().destory();
        } else {
            if (mReadCardManager != null) {
                mReadCardManager.release();
            }
        }
        super.onDestroy();
        mReadStart = false;
        handler.removeMessages(MSG_USB_IN);
        TempManager.getInstance().destory();
        TtsManager.getInstance().destory();
        DoorManager.getInstance().destory();
        FaceManager.getInstance().destory();
        LightManager.getInstance().destory();
        if (receiver != null) {
            unregisterReceiver(receiver);
        }
        stopDownApkFile();
    }

    private void showRToast(String toast) {
        //这个提示是用来界面提醒的，这李不提醒。
        if (toast.contains(AppInfo.CURRENT_NOT_RIGHT_TIME)) {
            return;
        }
        try {
            handler.post(new Runnable() {
                @Override
                public void run() {
                    MyToastView.getInstance().Toast(AcsService.this, toast);
                }
            });
        } catch (Exception e) {

            e.printStackTrace();
        }
    }

    /***
     * 提交下载进度到服务器
     * @param uniquePsuedoID
     * @param progress
     */
    public void updateTaskDoanProgress(String uniquePsuedoID, String taskId, String speed, int progress) {
        initOther();
        acsParsener.updateTaskDoanProgress(uniquePsuedoID, taskId, speed, progress);
    }


    public void checkAppUpdate() {
        if (SpUtils.getWorkModel() == AppInfo.MODEL_STAND_ALONE) {
            return;
        }
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                initOther();
                acsParsener.checkAppUpdate();
            }
        }, 5 * 1000);
    }

    public void stopDownApkFile() {
        if (SpUtils.getWorkModel() == AppInfo.MODEL_STAND_ALONE)
            return;
        initOther();
        acsParsener.stopDownApk();
    }

    //=====稳定代码，基本不会修改==========================================================================================
    // =========================================================================================================
    // ==============================================================================================================

    //初始化语音TTS
    private void initVoice() {
        TtsManager.getInstance().init();
    }

    /***
     * 开始说话
     * 程序所有得说话，都调用这里
     * @param ttsMessage
     */
    public void startSpeakTts(String ttsMessage, String tag) {
        startSpeakTts(ttsMessage, NOT_PLAY_LOCAL_MEDIA, tag);
    }

    /***
     * 处理网络变化
     * @param context
     * @param intent
     */
    private void dealNetChange(Context context, Intent intent) {
        NetworkInfo info = intent.getParcelableExtra(ConnectivityManager.EXTRA_NETWORK_INFO);
        if (info == null) {
            LogUtil.cdl("====获取网络对象===null。检测不熬网络变化了");
            return;
        }
        if (NetworkInfo.State.CONNECTED == info.getState() && info.isAvailable()) {
            int netType = info.getType();
            if (netType == ConnectivityManager.TYPE_WIFI || netType == ConnectivityManager.TYPE_MOBILE) {
                LogUtil.cdl("===NetBroadCastReciver网络连接", true);
                netChangeStatues(true);
            } else if (netType == ConnectivityManager.TYPE_ETHERNET) {
                LogUtil.cdl("===NetBroadCastReciver网络连接", true);
                netChangeStatues(true);
            }
        } else {
            LogUtil.cdl("===NetBroadCastReciver网络断开", true);
            netChangeStatues(false);

        }
    }

    private void netChangeStatues(boolean isTrue) {
        CommondViewModel.getInstance().getMNetState().postValue(isTrue);
        if (isTrue) {
            LogUtil.cdl("====网络变化====网络已连接", true);
            SocketService.getInstance().connLineSocket("网络状态发生改变,重连");
            String clVersion = CodeUtil.getSystCodeVersion(AcsService.this);
            updateDevInfoToAuthorServer(clVersion);
        }
    }

    /***
     * 提交设备信息到统计服务器
     */
    public void updateDevInfoToAuthorServer(String version) {
        if (SpUtils.getWorkModel() == AppInfo.MODEL_STAND_ALONE)
            return;
        initOther();
        acsParsener.upodateDevInfoToAuthorServer(version);
    }

    /***
     * 检查定时开关机
     */
    PowerOnOffUtil powerOnOffUtil;

    private void checkPowerOnOffTime() {
        try {
            if (powerOnOffUtil == null) {
                powerOnOffUtil = new PowerOnOffUtil(AcsService.this);
            }
            List<TimeLocalEntity> timerList = DbTimerLocalUtil.queryTimerList();
            if (timerList == null || timerList.size() < 1) {
                powerOnOffUtil.clearPowerOnOffTime();
                return;
            }
            powerOnOffUtil.changePowerOnOffByWorkModel();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /***
     * 更新访客信息，删除已经过期的访客
     */
    private void updateVisitorDbInfo() {
        int currentTime = SimpleDateUtil.getCurrentHourMin();
        if (currentTime % 600 != 0) {
            return;
        }
        PersonDao.getAllPersonByListener(AppInfo.FACE_REGISTER_PERSON_ALL, new FindMultiCallback<PersonBean>() {

            @Override
            public void onFinish(List<PersonBean> list) {
                if (list == null || list.size() < 1) {
                    return;
                }
                for (int i = 0; i < list.size(); i++) {
                    PersonBean personBean = list.get(i);
                    int type = personBean.getType();
                    if (type == AppInfo.FACE_REGISTER_VISITOR) {
                        String validTime = personBean.getSyncTime();
                        long endTime = SimpleDateUtil.StringToLongTime(validTime);
                        long currentTime = System.currentTimeMillis();
                        if (currentTime > endTime) {
                            FaceInfoJavaDao.deleteById(personBean.getPerId(), "定时同步访客信息，删除已经过期得访客");
                        }
                    }
                }
            }
        });
    }

    /***
     * 通知守护进程自己还活着
     */
    private void updateGuardianMineAlive() {
        try {
            boolean isLive = LeaderBarUtil.isAppRunBackground(AcsService.this);
            Intent intentSend = new Intent();
            intentSend.setAction(AppInfo.SEND_BROAD_CAST_LIVE);
            intentSend.putExtra(AppInfo.SEND_BROAD_CAST_LIVE, isLive);
            sendBroadcast(intentSend);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

//    //对外发送测温记录广播，名字及温度
//    public void sendTempRecord(String name, String temp) {
//        try {
//            String action = "com.ys.acesdevs.temperature";
//            Intent intent = new Intent(action);
//            intent.putExtra("name", name);
//            intent.putExtra("temperature", temp);
//            sendBroadcast(intent);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }

    private void initReceiver() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(AppInfo.YS_CAMERA_ADD);  //摄像头添加
        filter.addAction(AppInfo.YS_CAMERA_REMOVE); //摄像头移除
        filter.addAction(Intent.ACTION_TIME_TICK); //时间变化
        filter.addAction(AppInfo.RECEIVE_BROAD_CAST_LIVE); //守护进程
        filter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);   //网络状态改变
        filter.addAction(AppInfo.CHECK_MAIN_VIEW_LOGO);    //更新logo
        filter.addAction(AppInfo.UPDATE_LOCAL_RUN_LOG_TO_WEB);  //上传日志到服务器
        filter.addAction(AppInfo.BROAD_CHECK_APP_UPDATE);  //升级APK
//        filter.addAction("com.ys.eface.faceInfo");  //升级APK
        registerReceiver(receiver, filter);

        IntentFilter filterSd = new IntentFilter();
        filterSd.addAction(Intent.ACTION_MEDIA_MOUNTED); //插入SD卡USB
        filterSd.addAction(Intent.ACTION_MEDIA_EJECT); //拔出来SD卡usb
        filterSd.addDataScheme("file");
        registerReceiver(receiver, filterSd);
    }

    private void updateTimeFromWeb() {
        int currentTime = SimpleDateUtil.getCurrentHourMin();
        if (currentTime == 235) {
            LogUtil.cdl("===updateTomeFromWeb====准备重启设备==");
            SystemManagerUtil.rebootDev(AcsService.this);
        }
        if (SpUtils.getWorkModel() == AppInfo.MODEL_STAND_ALONE) {
            LogUtil.cdl("===updateTomeFromWeb====单机模式。不同步==");
            return;
        }
        String url = AppInfo.getCurrentSystemTimeFromWeb();
        if (url == null || url.length() < 10) {
            LogUtil.cdl("===updateTomeFromWeb====网址不合法==");
            return;
        }
        if (AppInfo.isUpdateServerTime) {
            LogUtil.cdl("===updateTomeFromWeb====已经同步过==");
            return;
        }
        if (!NetWorkUtils.isNetworkConnected(AcsService.this)) {
            LogUtil.cdl("===updateTomeFromWeb======");
            return;
        }
        LogUtil.cdl("===updateTomeFromWeb======");
        acsParsener.getCurrentSystemTime(new AcsView() {
            @Override
            public void backUpdateTime(boolean isTrue, long currentTime) {
                if (!isTrue) {
                    return;
                }
                LogUtil.cdl("===updateTomeFromWeb===backUpdateTime==000=" + currentTime);
                String times = SimpleDateUtil.formatBigString(currentTime);
                LogUtil.cdl("===updateTomeFromWeb===backUpdateTime==111=" + times);
                try {
                    int year = Integer.parseInt(times.substring(0, 4));
                    int month = Integer.parseInt(times.substring(4, 6));
                    int day = Integer.parseInt(times.substring(6, 8));
                    int hour = Integer.parseInt(times.substring(8, 10));
                    int minute = Integer.parseInt(times.substring(10, 12));
                    sendMyBroadcastWithLongExtra("com.ys.update_time", "current_time", TimeUtils.getTimeMills(year, month, day, hour, minute, 0));
                    AppInfo.isUpdateServerTime = true;
                    LogUtil.cdl("===updateTomeFromWeb===backUpdateTime==222=");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void backUpdateTime(boolean isTrue, long currentTime) {

    }

    private void sendMyBroadcastWithLongExtra(String action, String key, long value) {
        try {
            Intent intent = new Intent();
            intent.setAction(action);
            intent.putExtra(key, value);
            sendBroadcast(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
