package com.ys.acsdev.service.model;

import android.content.Context;

import com.ys.acsdev.bean.PersonBean;
import com.ys.acsdev.listener.DownApkStatesListener;
import com.ys.acsdev.service.view.AcsUpdateImageListener;
import com.ys.acsdev.service.view.AcsView;

public interface AcsModel {

    /***
     * 获取当前的系统时间
     * @param context
     * @param acsView
     */
    void getCurrentSystemTime(Context context, AcsView acsView);

    void checkAppUpdate(Context context, DownApkStatesListener downApkStatesListener);


    /***
     * 提交服务器到统计服务器
     */
    void upodateDevInfoToAuthorServer(String clVersion);

    /***
     * 插入门禁记录
     * @param personBean
     * @param passType
     * @param validCode
     * @param accessFilePath
     * @param temperature
     */
    void insertAccessFaceRecordService(PersonBean personBean,
                                       String passType,
                                       String validCode,
                                       String accessFilePath,
                                       String temperature, String wearMask, String cardNum, String personName, String qrCodeType);

    /****
     * 同行记录
     * OSS.刷卡，远程开门都走这里
     * @param person
     * @param passType
     * @param perId
     */
    void insertAccessCardRecordService(PersonBean person, String passType, String perId, String cardCode);

    /***
     * 同步本地信息到服务器
     */
    void syncAccessRecordLocal(AcsUpdateImageListener listener);

    void getMainViewLogo(Context context);

    //上传工作日志到服务器
    void uploadRunLogToServer();

    void updateTaskDoanProgress(String uniquePsuedoID, String taskId, String speed, int progress);

    void stopDownApk();


    /***
     * 上传文件到OSS服务器
     * @param personBean
     * @param passType
     * @param validCode
     * @param accessFilePath
     * @param temperatureUpdate
     * @param tag
     */
//    void updateLoadPathToOSS(PersonBean personBean,
//                             String passType,
//                             String validCode,
//                             String accessFilePath,
//                             String temperatureUpdate, String tag);

    /****
     * 提交同行记录到服务器记录
     * @param personBean
     * @param passType
     * @param validCode
     * @param temperature
     * @param downPath
     * @param fileLength
     */
//    void insertFaceToServerByOssInfo(PersonBean personBean, String passType, String validCode, String temperature, String downPath, long fileLength);
}
