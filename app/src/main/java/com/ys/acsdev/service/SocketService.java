package com.ys.acsdev.service;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;

import com.hwangjr.rxbus.RxBus;
import com.ys.acsdev.MyApp;
import com.ys.acsdev.R;
import com.ys.acsdev.api.AppConfig;
import com.ys.acsdev.attendance.dao.ClockInDao;
import com.ys.acsdev.bean.DeviceBean;
import com.ys.acsdev.bean.LinkServerEvent;
import com.ys.acsdev.commond.AppInfo;
import com.ys.acsdev.commond.CommondViewModel;
import com.ys.acsdev.db.dao.FaceInfoJavaDao;
import com.ys.acsdev.db.dao.LocalARDao;
import com.ys.acsdev.db.dao.PersonDao;
import com.ys.acsdev.helper.TcpHelperNew;
import com.ys.acsdev.listener.SocketMessageListener;
import com.ys.acsdev.manager.DoorManager;
import com.ys.acsdev.service.parsener.SocketServerParsener;
import com.ys.acsdev.service.view.SocketServerView;
import com.ys.acsdev.setting.parsener.SettingMenuParsener;
import com.ys.acsdev.setting.view.SettingMenuView;
import com.ys.acsdev.socket.OnSocketClientListener;
import com.ys.acsdev.util.Biantai;
import com.ys.acsdev.util.CodeUtil;
import com.ys.acsdev.util.FileUtils;
import com.ys.acsdev.util.LogUtil;
import com.ys.acsdev.util.NetWorkUtils;
import com.ys.acsdev.util.SpUtils;
import com.ys.acsdev.util.SystemManagerUtil;
import com.ys.acsdev.util.VoiceManager;
import com.ys.acsdev.view.MyToastView;
import com.ys.facepasslib.FaceInitListener;
import com.ys.facepasslib.FaceManager;

import org.jetbrains.annotations.NotNull;
import org.json.JSONObject;

import java.util.List;

public class SocketService extends Service implements SocketServerView {

    public static SocketService mInstance;

    public static SocketService getInstance() {
        if (mInstance == null) {
            synchronized (SocketService.class) {
                if (mInstance == null) {
                    mInstance = new SocketService();
                }
            }
        }
        return mInstance;
    }

    private static final int MESSAGE_UPDATE_DEV_INFO_FROM_WEB = 5689;
    TcpHelperNew mTcpHelper = null;
    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case MESSAGE_UPDATE_DEV_INFO_FROM_WEB:
                    syncDevInfoFromWeb();
                    break;
                case AppInfo.SOCKET_HANDLER_MESSAGE_HEART:
                    startHeartbeat(AppInfo.HEART_STATUES_DEFAULT);
                    break;
                case MSG_CHECK_HEARDBEAT:
                    removeMessages(MSG_CHECK_HEARDBEAT);
                    if (mAlive) {
                        LogUtil.message("===开始心跳==002===设备当前在线，不用重连", true);
                        return;
                    }
                    connLineSocket("没有收到心跳回应，这里准备去连接");
                    break;
            }
        }
    };
    boolean mIsAuthing = false;

    @Override
    public void onCreate() {
        super.onCreate();
        //单机模式
        if (SpUtils.getWorkModel() == AppInfo.MODEL_STAND_ALONE) {
            if (SpUtils.getmFaceEnable() && SpUtils.getmInitFace() && !mIsAuthing) {
                initFaceSdk();
            }
            return;
        }
        initOther();
        startHeartbeat(AppInfo.HEART_STATUES_DEFAULT);
        //客户定制，不用我们的 socket ,这里就不用心跳机制了
        //mHandler.sendEmptyMessageDelayed(AppInfo.SOCKET_HANDLER_MESSAGE_HEART, AppInfo.SOCKET_HEART_TIME_DISTANCE);
    }

    SocketServerParsener serverParsener;

    private void initOther() {
        if (mTcpHelper == null) {
            mTcpHelper = new TcpHelperNew();
        }
        if (serverParsener == null) {
            serverParsener = new SocketServerParsener(this);
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        mInstance = this;
        initListener();
        return super.onStartCommand(intent, flags, startId);
    }

    private void checkRegister(String printTag) {
        String serverIp = SpUtils.getmServerIp();
        String swrverPort = SpUtils.getmServerPort();
        if (serverIp == null || serverIp.length() < 3) {
            LogUtil.message("==serverIp==null=");
            if (mTcpListener != null) {
                mTcpListener.onError(2, "IP Error");
            }
            return;
        }
        if (swrverPort == null || swrverPort.length() < 3) {
            if (mTcpListener != null) {
                mTcpListener.onError(2, "PORT Error");
            }
            LogUtil.message("==swrverPort==null=");
            return;
        }
        LogUtil.message("=======查询设备注册信息==" + printTag);
        if (SpUtils.getmFaceEnable() && SpUtils.getmInitFace() && !mIsAuthing) {
            initFaceSdk();
        }
        if (!NetWorkUtils.isNetworkConnected(SocketService.this)) {
            LogUtil.message("=======SocketService: 无网络");
            if (mTcpListener != null) {
                mTcpListener.onError(2, getString(R.string.network_anomaly));
            }
            return;
        }
        initOther();
        serverParsener.getDevInfoFromWeb();
    }

    @Override
    public void getDeviceInfoStatues(boolean isTrue, int code, List<DeviceBean> lisst, String errorDesc) {
        LogUtil.message("===获取的设备信息==" + isTrue + " / " + errorDesc);
        if (!isTrue) {
            LogUtil.register("网络请求失败:" + errorDesc);
            if (AppInfo.isConnectServer) {
                disConnected();
            }
            mTcpListener.onError(3, "Request Failed :" + errorDesc);
            return;
        }

        if (lisst == null || lisst.size() < 1) {
            cleanLocalData();
            if (AppInfo.isConnectServer) {
                LogUtil.message("==================获取设备信息为空： 断开服务器连接", true);
                disConnected();
            }
            AppInfo.ifDevRegister = false;
            mTcpListener.onError(3, getString(R.string.device_not_registered));
            updateMainLogoView();
            return;
        }
        AppInfo.ifDevRegister = true;
        SpUtils.setDeviceInfo(lisst.get(0), "SocketService--getDeviceInfoStatues==default");
        sendBroadReceiverView(new Intent(AppInfo.UPDATE_MAIN_BOTTOM_VIEW));
        if (SpUtils.getmFaceEnable() && !SpUtils.getmInitFace() && !mIsAuthing) {
            initFaceSdk();
        }
        if (!AppInfo.isConnectServer) {
            connLineSocket("设备检查注册成功，这里去连接TCP服务器");
        }
        updateMainLogoView();
    }

    private void initFaceSdk() {
        LogUtil.e("=======initFaceSdk");
        initPassFace();
    }

    private void initPassFace() {
        int mFaceRotation = 0;
        switch (SpUtils.getmFaceRotate()) {
            case 0:
                mFaceRotation = 0;
                break;
            case 1:
                mFaceRotation = 90;
                break;
            case 2:
                mFaceRotation = 180;
                break;
            default:
                mFaceRotation = 270;
                break;
        }
        FaceManager.getInstance().setMFaceRotation(mFaceRotation);
        int mFaceMinThreshold = 130;
        switch (SpUtils.getmFaceLen()) {
            case 0:
                mFaceMinThreshold = 150;
                break;
            case 2:
                mFaceMinThreshold = 100;
                break;
            case 3:
                mFaceMinThreshold = 50;
                break;
            case 4:
                mFaceMinThreshold = 25;
                break;
            default:
                mFaceMinThreshold = 130;
                break;
        }
        FaceManager.getInstance().setMFaceMinThreshold(mFaceMinThreshold);
        FaceManager.getInstance().setMMaskEnable(SpUtils.getmMaskEnable());

        FaceManager.getInstance().setMIrLivessEnable(SpUtils.getAutoSwitchCamera());
        FaceManager.getInstance().setMSingleLivessEnable(SpUtils.getSingleLivingEnable());
        //CameraProvider.getCamerasCount(this) > 1
        FaceManager.getInstance().setMLivenessThreshold(SpUtils.getLivenessThreshold());
        FaceManager.getInstance().setMSearchThreshold(SpUtils.getmSearchThreshold());
        String faceAppCode = "naP4K1Tk";
        LogUtil.message("开始旷视人脸识别初始化", true);
        FaceManager.getInstance().init(MyApp.getInstance(), faceAppCode, new FaceInitListener() {
            @Override
            public void onSuccess() {
                mIsAuthing = true;
                LogUtil.message("人脸识别初始化成功", true);
                LogUtil.e("=======initFaceSdk: success");
                SpUtils.setmInitFace(true);
//                sendRxBusFaceInitEvent(true)
                CommondViewModel.getInstance().getMFaceInitState().postValue(true);
                getFreshPersonInfo("初始化旷视人脸完成，获取人员信息", UPDATE_INIT_FACE);
            }

            @Override
            public void onError(int code, @NotNull String msg) {
                mIsAuthing = false;
                LogUtil.message("人脸识别初始化失败: " + code + " / " + msg, true);
                showLongToast(getString(R.string.face_init_failed_msg, code + "", msg));
//                sendRxBusFaceInitEvent(false)
                CommondViewModel.getInstance().getMFaceInitState().postValue(false);
            }
        });
    }


    private int UPDATE_INIT_FACE = 0;  //人脸初始化
    private int UPDATE_CONNECT_SUCCESS = 1;  //服务器链接
    private int UPDATE_RECEIVER_ORDER = 2;  //接受到指令

    /***
     * 获取人员信息
     * @param printTag
     * @param tag
     */
    public void getFreshPersonInfo(String printTag, int tag) {
        LogUtil.persoon("========同步人脸信息===" + printTag);
        if (SpUtils.getWorkModel() == AppInfo.MODEL_STAND_ALONE) {
            return;
        }
        if (!AppInfo.isConnectServer) {
            return;
        }
        int delayTime = 5000;
        if (tag == UPDATE_INIT_FACE ||
                tag == UPDATE_CONNECT_SUCCESS) {
            delayTime = 5000;
        } else if (tag == UPDATE_RECEIVER_ORDER) {
            delayTime = 500;
        }
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(AppInfo.START_CHECK_FACE_DOWN_REGISTER);
                intent.putExtra(AppInfo.START_CHECK_FACE_DOWN_REGISTER, AppInfo.FACE_REGISTER_PERSON_ALL);
                sendBroadReceiverView(intent);
            }
        }, delayTime);
    }

    private void cancelHeartbeat() {
        mHandler.removeMessages(AppInfo.SOCKET_HANDLER_MESSAGE_HEART);
    }

    /**
     * 开始发送心跳
     * 心跳机制是定时的，其他网络状态不发心跳
     * 1  表示默认心跳
     * -1 表示手动链接
     */
    public void startHeartbeat(int heartTag) {
        LogUtil.message("===开始心跳==" + heartTag, true);
        mHandler.removeMessages(AppInfo.SOCKET_HANDLER_MESSAGE_HEART);
        //客户不用我们的长链接，这里的心跳机制，直接注释掉，后边再说
//        mHandler.sendEmptyMessageDelayed(
//                AppInfo.SOCKET_HANDLER_MESSAGE_HEART,
//                AppInfo.SOCKET_HEART_TIME_DISTANCE
//        );
        if (Biantai.checkHeartTime()) {
            //拒绝高频操作
            LogUtil.message("===开始心跳==高频操作，中断操作", true);
            return;
        }
        if (heartTag == AppInfo.HEART_STATUES_FROM_OTHER_ACTIVITY) {  //手动链接服务器界面
            AppInfo.ifDevRegister = false;
            if (mTcpHelper != null) {
                mTcpHelper.dealDisOnlineDev("手动断开设备，重连服务器");
            }
            checkRegister("手动断开设备，重连服务器");
            return;
        }
        //判断设备是否注册
        boolean isDevRegister = AppInfo.ifDevRegister;
        if (!isDevRegister) {
            //没有检查是否注册，这里取注册
            LogUtil.message("===设备未检查注册，这里去检查注册");
            checkRegister("正常心跳，去检查注册情况");
            return;
        }
        LogUtil.message("===开始心跳==001===", true);
        mHandler.removeMessages(MSG_CHECK_HEARDBEAT);
        mHandler.sendEmptyMessageDelayed(MSG_CHECK_HEARDBEAT, HEARDBEAT_TIMEOUT);
        mAlive = false;
//        if (!isConnected()) {
//            //未连接
//            connLineSocket("发送消息前检测到未连接则去重连");
//        }
        if (mTcpHelper != null) {
            mTcpHelper.sendMsg(HEART_SEND_TO_WEB);
        }
    }

    private static final int MSG_CHECK_HEARDBEAT = 1;
    String HEART_SEND_TO_WEB = "{\"code\":\"6002\",\"msg\":" + CodeUtil.getUniquePsuedoID() + "}";
    long HEARDBEAT_TIMEOUT = 10 * 1000L;
    boolean mAlive = false;

    OnSocketClientListener mTcpListener = new OnSocketClientListener() {
        @Override
        public void onRecpData(@NotNull String msg) {
            LogUtil.e("=======SocketService onRecpData:" + msg);
        }

        @Override
        public void heartStatues(boolean isReceiveMessage) {
            mAlive = isReceiveMessage;
        }

        @Override
        public void onConnecting() {
            sendRxBusMessageToMainView(
                    TcpHelperNew.SERVER_CODE_CONNECTING,
                    getString(R.string.connecting)
            );
            LogUtil.message("正在连接服务器", true);
        }


        @Override
        public void onConnected() {
            //重置心跳时间
            LogUtil.message("服务器已连接", true);
            getFreshPersonInfo("服务器连接成功,刷新一次啊数据", UPDATE_CONNECT_SUCCESS);
            //获取广告信息
            Intent intent = new Intent();
            intent.setAction(AppInfo.RECEIVE_MESSAGE_TO_REQUEST_TASK);
            intent.putExtra(AppInfo.RECEIVE_MESSAGE_TO_REQUEST_TASK, "Socket 服务器连接成功");
            sendBroadReceiverView(intent);
            updateMainLogoView();
            sendRxBusMessageToMainView(
                    TcpHelperNew.SERVER_CODE_CONNECTED,
                    getString(R.string.connected)
            );
            //服务器连接后5秒去检查更新
            checkAppUpdateInfo();
            CommondViewModel.getInstance().getMServerState().postValue(true);
        }

        @Override
        public void onDisConnected(String msg) {
            LogUtil.message("服务器断开连接: $msg", true);
            sendRxBusMessageToMainView(
                    TcpHelperNew.SERVER_CODE_DISCONNECT,
                    getString(R.string.disconnect)
            );
            CommondViewModel.getInstance().getMServerState().postValue(false);
        }

        @Override
        public void onError(int code, String msg) {
            LogUtil.message("服务器连接失败: " + code + " / " + msg, true);
            sendRxBusMessageToMainView(code, msg);
            CommondViewModel.getInstance().getMServerState().postValue(false);
        }
    };

    /***
     * 发通知给界面
     */
    private void sendRxBusMessageToMainView(int serverCode, String descView) {
        RxBus.get().post(new LinkServerEvent(serverCode, descView));
    }

    private void initListener() {
        initOther();
        mTcpHelper.init(mTcpListener);
    }

    /***
     * 更新 Logo 信息
     * 延迟3秒更新Logo，
     * 给3秒时间系统操作其他得内容，这个不着急
     */
    private void updateMainLogoView() {
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                sendBroadReceiverView(new Intent(AppInfo.CHECK_MAIN_VIEW_LOGO));
            }
        }, 3000);
    }

    public void connLineSocket(String printTag) {
        LogUtil.message("===connLineSocket====" + printTag);
        String serverIp = SpUtils.getmServerIp();
        String swrverPort = SpUtils.getmServerPort();
        if (serverIp == null || serverIp.length() < 3) {
            LogUtil.message("===connLineSocket====IP ERROR");
            mTcpListener.onError(2, "IP Error");
            return;
        }
        if (swrverPort == null || swrverPort.length() < 3) {
            LogUtil.message("===connLineSocket====PORT ERROR");
            mTcpListener.onError(2, "PORT Error");
            return;
        }
        initOther();
        if (mTcpHelper != null) {
            mTcpHelper.setReceiverOrderMsgCallBack(socketMessageListener);
        }
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                if (mTcpHelper != null) {
                    mTcpHelper.lineSocketWeb(printTag);
                }

            }
        };
        AcsService.getInstance().executor(runnable);
    }

    SocketMessageListener socketMessageListener = new SocketMessageListener() {
        @Override
        public void messageOrderBack(int code, JSONObject jsonObj) {
            LogUtil.message("====service  接收到消息" + code);
            switch (code) {
                case AppInfo.MESSAGE_SERVER_ADD_PERSON:
                case AppInfo.MESSAGE_SERVER_UPDATE_FACE:
                case AppInfo.MESSAGE_SERVER_ADD_VISITOR:  //更新人脸信息
                    getFreshPersonInfo("=====接受到指令，开始同步信息", UPDATE_RECEIVER_ORDER);
                    break;
                case AppInfo.MESSAGE_MODIFY_DEV_GROUP:   //修改设备分组
                case AppInfo.MESSAGE_SERVER_UPDATE_DEVICE: //更新设备信息
                case AppInfo.MESSAGE_SERVER_DEL_DEVICE: //删除设备信息
                    checkRegister("更新设备信息");
                    getFreshPersonInfo("=====接受到修改设备的指令，开始同步人脸信息", UPDATE_RECEIVER_ORDER);
                    break;
                case AppInfo.MESSAGE_SERVER_REGISTER_DEV:    // 添加门禁机
//                RxBus.get().post(ServerOrderEvent(AppInfo.SERVER_REGISTER_DEV, ""))
                    checkRegister("添加门禁设备到服务器");
                    break;
                case AppInfo.MESSAGE_SERVER_REDEV:
//                SystemManagerUtil.rebootDev(this)
                    sendBroadReceiverView(new Intent("android.intent.action.reboot"));
                    break;
                case AppInfo.MESSAGE_SERVER_REAPP:   //重启APP
                    SystemManagerUtil.rebootApp(SocketService.this);
                    break;
                case AppInfo.MESSAGE_SERVER_SOUND:  //修改音量
                    int volume = jsonObj.optInt("volume", 50);
                    VoiceManager.changeMusicVoice(SocketService.this, volume);
                    break;
                case AppInfo.MESSAGE_SERVER_CHECK_UPDATE:
                    checkAppUpdateInfo();
                    break;
                case AppInfo.MESSAGE_SERVER_LOGO:  //修稿logo
                    updateMainLogoView();
                    break;
                case AppInfo.MESSAGE_UPDATE_RUN_LOG:   //上传工作日志
                    sendBroadReceiverView(new Intent(AppInfo.UPDATE_LOCAL_RUN_LOG_TO_WEB));
                    break;
                case AppInfo.MESSAGE_SERVER_AD:  //跟小滚广告
                    Intent intent = new Intent();
                    intent.setAction(AppInfo.RECEIVE_MESSAGE_TO_REQUEST_TASK);
                    intent.putExtra(AppInfo.RECEIVE_MESSAGE_TO_REQUEST_TASK, "Socket：接收到指令，准备请求任务");
                    sendBroadReceiverView(intent);
                    break;
                case AppInfo.MESSAGE_SERVER_DOOR_OPEN:
                    DoorManager.getInstance().open("===server online open door");
                    break;
                case AppInfo.MESSAGE_REPLACE_DEV:
                    delLocalAcrrodInfo();
                    break;
                case AppInfo.MESSAGE_UPDATE_SETTING_DEV:
                    mHandler.sendEmptyMessage(MESSAGE_UPDATE_DEV_INFO_FROM_WEB);
                    break;
            }
        }
    };

    /***
     * 同步服务器信息
     */
    SettingMenuParsener settingMenuParsener;

    private void syncDevInfoFromWeb() {
        try {
            if (settingMenuParsener == null) {
                settingMenuParsener = new SettingMenuParsener(SocketService.this, new SettingMenuView() {

                    @Override
                    public void showWaitDialog(boolean isShow) {

                    }

                    @Override
                    public void syncDevInfoFromWeb(boolean isTrue, String errorDesc) {

                    }
                });
            }
            settingMenuParsener.sycnConfig();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void delLocalAcrrodInfo() {
        AcsService.getInstance().executor(() -> {
            //删除离线通行记录
            String cachePath = AppInfo.BASE_PATH_CACHE;
            FileUtils.deleteDirOrFile(cachePath, "分配设备清理离线记录");
            LocalARDao.delAll();
//        删除离线考勤记录
            ClockInDao.delAlCockIn();
        });
    }

    private void checkAppUpdateInfo() {
        Intent intent = new Intent();
        intent.setAction(AppInfo.BROAD_CHECK_APP_UPDATE);
        sendBroadReceiverView(intent);
    }

    private void sendBroadReceiverView(Intent intent) {
        try {
            sendBroadcast(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showLongToast(String msg) {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                MyToastView.getInstance().Toast(SocketService.this, msg, 3000);
            }
        });
    }

    private void cleanLocalData() {
        SpUtils.setDeviceInfo(null, "==SocketService--清除本地信息===");
        sendBroadReceiverView(new Intent(AppInfo.UPDATE_MAIN_BOTTOM_VIEW));
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                FaceInfoJavaDao.deleteAllFaceInfo();
                PersonDao.deleteAllPerson();
            }
        };
        AcsService.getInstance().executor(runnable);
    }


    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    /**
     * 断开连接
     */
    private void disConnected() {
        LogUtil.e("=======SocketService:调用断开连接");
        if (mTcpHelper != null) {
            mTcpHelper.destory();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        cancelHeartbeat();
        disConnected();
    }

}
