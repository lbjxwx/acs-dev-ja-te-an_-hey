package com.ys.acsdev.service.model;

import android.content.Context;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;

import com.etv.down.XutilDownFileEntity;
import com.etv.down.XutilDownStateListener;
import com.ys.acsdev.util.down.DownImageManager;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;
import com.hwangjr.rxbus.RxBus;
import com.ys.acsdev.api.AppConfig;
import com.ys.acsdev.bean.LocalARBean;
import com.ys.acsdev.bean.LogoChangeEvent;
import com.ys.acsdev.bean.PersonBean;
import com.ys.acsdev.commond.AppInfo;
import com.ys.acsdev.db.dao.LocalARDao;
import com.ys.acsdev.listener.DownApkStatesListener;
import com.ys.acsdev.runnable.down.DownFileEntity;
import com.ys.acsdev.runnable.down.DownRunnable;
import com.ys.acsdev.runnable.down.DownStateListener;
import com.ys.acsdev.runnable.upload.UpdateDoorInfoRunnable;
import com.ys.acsdev.runnable.upload.UpdateFileRunnable;
import com.ys.acsdev.runnable.upload.UpdateImageListener;
import com.ys.acsdev.service.AcsService;
import com.ys.acsdev.service.view.AcsUpdateImageListener;
import com.ys.acsdev.service.view.AcsView;
import com.ys.acsdev.util.APKUtil;
import com.ys.acsdev.util.CodeUtil;
import com.ys.acsdev.util.FileUtils;
import com.ys.acsdev.util.LogUtil;
import com.ys.acsdev.util.SimpleDateUtil;
import com.ys.acsdev.util.SpUtils;
import com.ys.acsdev.util.custom.YktValidate;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import okhttp3.Call;

public class AcsModelmpl implements AcsModel {

    DownApkStatesListener downApkStatesListener;

    /****
     * 获取当前的时间
     * @param context
     */
    @Override
    public void getCurrentSystemTime(Context context, AcsView acsView) {
        String requestUrl = AppInfo.getCurrentSystemTimeFromWeb();
        OkHttpUtils
                .get()
                .url(requestUrl)
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, String e, int id) {

                        LogUtil.cdl("===获取当前的时间=error==" + e.toString());
                        acsView.backUpdateTime(false, -1);
                    }

                    @Override
                    public void onResponse(String response, int id) {
                        LogUtil.cdl("===获取当前的时间=success==" + response);
                        if (response == null || response.length() < 5) {
                            acsView.backUpdateTime(false, -1);
                            return;
                        }
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            int code = jsonObject.getInt("code");
                            if (code != 0) {
                                return;
                            }
                            long data = jsonObject.getLong("data");
                            acsView.backUpdateTime(true, data);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
    }

    @Override
    public void checkAppUpdate(Context context, DownApkStatesListener listener) {
        if (context == null) {
            return;
        }
        downApkStatesListener = listener;
        String requestUrl = AppInfo.getUpdateAppUrl();
        OkHttpUtils
                .post()
                .url(requestUrl)
                .addParams("sn", CodeUtil.getUniquePsuedoID())
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, String e, int id) {
                        if (downApkStatesListener != null) {
                            downApkStatesListener.requestBack(e.toString());
                        }
                        LogUtil.update("=======升级信息=errorrDesc==" + e.toString());
                    }

                    @Override
                    public void onResponse(String response, int id) {
                        if (response == null || response.length() < 5) {
                            if (downApkStatesListener != null) {
                                downApkStatesListener.requestBack("json==null");
                            }
                            return;
                        }
                        LogUtil.update("=======升级信息===" + response);
                        parsenerApkInfoJson(context, response);
                    }
                });
    }

//    /***
//     * 上传文件到OSS服务器
//     * @param personBean
//     * @param passType
//     * @param validCode
//     * @param accessFilePath
//     * @param temperatureUpdate
//     * @param tag
//     */
//    BatchUploadRunnable batchUploadRunnable;
//
//    @Override
//    public void updateLoadPathToOSS(PersonBean personBean, String passType, String validCode, String accessFilePath, String temperatureUpdate, String tag) {
//        if (batchUploadRunnable == null) {
//            batchUploadRunnable = new BatchUploadRunnable();
//        }
//        OSS oss = MyApp.getInstance().getOssClient();
//        batchUploadRunnable.setUpdateFaceInfo(oss, accessFilePath, new OssUpdateListener() {
//            @Override
//            public void backUpdateStatues(int statues, int progress, String desc, long fileLength) {
//                if (statues != DownFileEntity.DOWN_STATE_SUCCESS) {
//                    return;
//                }
//                String downPath = OssConfig.ossDownUrl + OssConfig.attentRecorderPath + desc;
//                Log.e("CDL", "==downPath==" + downPath + " / " + fileLength);
//                insertFaceToServerByOssInfo(personBean, passType, validCode, temperatureUpdate, downPath, fileLength);
//            }
//        });
//        AcsService.getInstance().executor(batchUploadRunnable);
//    }

    private void parsenerApkInfoJson(Context context, String json) {
        try {
            JSONObject jsonObject = new JSONObject(json);
            int code = jsonObject.getInt("code");
            String message = jsonObject.getString("msg");
            if (code != 0) {
                LogUtil.update("升级请求 code = " + code + " /message = " + message, true);
                if (downApkStatesListener != null) {
                    downApkStatesListener.requestBack(message);
                }
                return;
            }
            String data = jsonObject.getString("data");
            if (data == null || data.length() < 5) {
                LogUtil.update("升级请求-没有升级信息", true);
                if (downApkStatesListener != null) {
                    downApkStatesListener.requestBack("No Update Info");
                }
                return;
            }
            JSONObject jsonData = new JSONObject(data);
            int serverCode = Integer.parseInt(jsonData.getString("apkCode"));
//            String apkPath = jsonData.getString("apkFilePath");
            String apkPath = jsonData.getString("apkPath");
            int localCode = APKUtil.getVersionCode(context);
            if (serverCode < localCode || serverCode == localCode) {
                LogUtil.update("已是最新版本,不需要升级", true);
                updateAppDownStartues(100, 0, 2);
                if (downApkStatesListener != null) {
                    downApkStatesListener.requestBack("Current App No Need Update");
                }
                return;
            }
            String apkSize = "";
            if (jsonData.toString().contains("apkSize")) {
                apkSize = jsonData.getString("apkSize");
            }
            if (TextUtils.isEmpty(apkPath)) {
                LogUtil.update("=====aPK下载地址==null", true);
                if (downApkStatesListener != null) {
                    downApkStatesListener.requestBack("Apk Path Is Null");
                }
                return;
            }
            if (!apkPath.startsWith("http")) {
                apkPath = AppInfo.getBaseUrl() + apkPath;
            }
            LogUtil.update("=====update: 检测到新版本，aPK下载地址==" + apkPath, true);
            startDownApp(apkPath, apkSize);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    DownRunnable downApkRunnable;

    @Override
    public void stopDownApk() {
        if (downApkRunnable == null) {
            return;
        }
        downApkRunnable.stopDown();
    }


    private void startDownApp(String downUrl, String apkSize) {
        stopDownApk();
        try {
            lastProgress = 0;
            FileUtils.creatPathNotExcit();
            String currentDate = SimpleDateUtil.getCurrentTimelONG();
            String saveName = currentDate + ".apk";
            String savePath = AppInfo.BASE_APK_PATH + "/" + saveName;
            File apkDir = new File(savePath);
            if (!apkDir.exists()) {
                apkDir.createNewFile();
            }
            downApkRunnable = new DownRunnable(downUrl, savePath, saveName, new DownStateListener() {

                @Override
                public void downStateInfo(DownFileEntity entity) {
                    if (entity == null) {
                        return;
                    }
                    if (downApkStatesListener != null) {
                        downApkStatesListener.downStateInfo(entity);
                    }
                    int downStatues = entity.getDownState();
                    int downProgress = entity.getProgress();
                    int downSpeed = entity.getDownSpeed();
                    LogUtil.update("下载升级文件:" + downStatues + " / " + downProgress + " /speed=" + downSpeed, true);
                    if (downStatues == DownFileEntity.DOWN_STATE_SUCCESS) {
                        updateAppDownStartues(100, 0, 2);
                        String savePath = entity.getSavePath();
                        indtallApk(savePath, apkSize);
                        stopDownApk();
                    } else if (downStatues == DownFileEntity.DOWN_STATE_FAIED) {
                        updateAppDownStartues(-1, 0, 3);
                        stopDownApk();
                    } else if (downStatues == DownFileEntity.DOWN_STATE_START) {
                        updateAppDownStartues(0, 0, 1);
                    } else if (downStatues == DownFileEntity.DOWN_STATE_PROGRESS) {
                        updateAppDownStartues(downProgress, downSpeed, 1);
                    }
                }
            });
            downApkRunnable.setLimitDownSpeed(AppConfig.DOWN_SPEED_LIMIT);
            AcsService.getInstance().executor(downApkRunnable);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void indtallApk(String savePath, String apkSize) {
        try {
            File file = new File(savePath);
            if (!file.exists()) {
                LogUtil.update("==安装文件==下载文件不存在===");
                return;
            }
            long fileLength = file.length();
            long apkSizeLong = fileLength;
            LogUtil.update("==安装文件==fileLength===" + fileLength);
            if (apkSize != null && apkSize.length() > 3) {
                apkSizeLong = Long.parseLong(apkSize);
            }
            LogUtil.update("==安装文件==fileLength===" + fileLength + " / apkSizeLong=" + apkSizeLong);
            if (Math.abs(fileLength - apkSizeLong) > 10 * 1024) {
                LogUtil.update("==安装文件==文件相差太多，不安装===");
                return;
            }
            LogUtil.update("==安装文件==去安装apk===");
            APKUtil.installApk(savePath);
        } catch (Exception e) {
            LogUtil.update("==安装文件=====" + e.toString());
            e.printStackTrace();
        }
    }

    static int lastProgress = 0;

    private void updateAppDownStartues(int percent, int speed, int statues) {
        if (percent < lastProgress || percent == lastProgress) {
            return;
        }
        speed = speed + new Random().nextInt(50);
        String requestUrl = AppInfo.getUpdateAppDownStatues();
        String sn = CodeUtil.getUniquePsuedoID();
        OkHttpUtils
                .post()
                .url(requestUrl)
                .addParams("sn", sn)
                .addParams("percent", percent + "")
                .addParams("statues", statues + "")
                .addParams("speed", speed + "")
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, String e, int id) {
                        LogUtil.update("=======提交上传进度===" + e.toString());
                    }

                    @Override
                    public void onResponse(String response, int id) {
                        LogUtil.update("=======提交上传进度===" + response);
                    }
                });
        lastProgress = percent;
    }

    //================================================================================

    @Override
    public void uploadRunLogToServer() {
        if (SpUtils.getWorkModel() == AppInfo.MODEL_STAND_ALONE) {
            return;
        }
        long currentTime = SimpleDateUtil.getCurrentDateLong();
        String faceRunLogPath = AppInfo.LOG_PATH + "/" + currentTime + ".txt";
        File file = new File(faceRunLogPath);
        if (!file.exists()) {
            LogUtil.update("上传得日志文件不存在");
            return;
        }
        String requestUrl = AppInfo.getUploadEquipmentLog();
        UpdateFileRunnable updateFileRunnable = new UpdateFileRunnable(requestUrl, faceRunLogPath);
        AcsService.getInstance().executor(updateFileRunnable);
    }


    @Override
    public void updateTaskDoanProgress(String sn, String taskId, String speed, int progress) {
        String requestUrl = AppInfo.getUpdateTaskProgress();
        OkHttpUtils
                .post()
                .url(requestUrl)
                .addParams("sn", sn)
                .addParams("taskId", taskId)
                .addParams("downloadSpeed", speed)
                .addParams("downloadProgress", progress + "")
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, String e, int id) {
                        LogUtil.task("=======提交上传进度===" + e.toString());
                    }

                    @Override
                    public void onResponse(String response, int id) {
                        LogUtil.task("=======提交上传进度===" + response);
                    }
                });
    }

    Context contextDown;

    @Override
    public void getMainViewLogo(Context context) {
        if (SpUtils.getWorkModel() == AppInfo.MODEL_STAND_ALONE) {
            return;
        }
        this.contextDown = context;
        String requestUrl = AppInfo.getSelectLogoDataBySn();
        String sn = CodeUtil.getUniquePsuedoID();
        OkHttpUtils
                .post()
                .url(requestUrl)
                .addParams("sn", sn)
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, String e, int id) {
                        LogUtil.logo("=====获取logo error====" + e.toString());
                    }

                    @Override
                    public void onResponse(String json, int id) {
                        if (json == null || json.length() < 4) {
                            return;
                        }
                        LogUtil.logo("=====获取logo success====" + json, true);
                        try {
                            JSONObject jsonObject = new JSONObject(json);
                            int code = jsonObject.getInt("code");
                            if (code != 0) {
                                return;
                            }
                            String data = jsonObject.getString("data");
                            if (data == null || data.length() < 3) {
                                return;
                            }
                            JSONArray jsonArrayData = new JSONArray(data);
                            int arrayNul = jsonArrayData.length();
                            if (arrayNul < 1) {
                                delFileLogoOrVisitor(AppInfo.LOGO_PATH);
                                delFileLogoOrVisitor(AppInfo.QR_VISITOR_DOWN_PATH);
                                return;
                            }
                            JSONObject jsonData = jsonArrayData.getJSONObject(0);
                            String logoPath = "";
                            String logoSize = "";
                            if (jsonData.toString().contains("logoPath")) {
                                logoPath = jsonData.getString("logoPath");
                            }
                            if (jsonData.toString().contains("logoSize")) {
                                logoSize = jsonData.getString("logoSize");
                            }
                            LogUtil.logo("===logo=info=" + logoPath + " / " + logoSize);
                            if (logoPath != null && logoPath.length() > 5) {
                                startDownLogoFile(logoPath, logoSize, AppInfo.LOGO_PATH, "logo");
                            } else {
                                delFileLogoOrVisitor(AppInfo.LOGO_PATH);
                            }
                            String qrcodeUrl = "";
                            if (jsonData.toString().contains("qrcodeUrl")) {
                                qrcodeUrl = jsonData.getString("qrcodeUrl");
                            }
                            String qrcodeSize = "";
                            if (jsonData.toString().contains("qrcodeSize")) {
                                qrcodeSize = jsonData.getString("qrcodeSize");
                            }
                            LogUtil.logo("====访客二维码===" + qrcodeUrl + " / " + qrcodeSize);
                            if (qrcodeUrl != null && qrcodeUrl.length() > 5) {
                                startDownLogoFile(qrcodeUrl, qrcodeSize, AppInfo.QR_VISITOR_DOWN_PATH, "qrcode");
                            } else {
                                delFileLogoOrVisitor(AppInfo.QR_VISITOR_DOWN_PATH);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
    }

    private void delFileLogoOrVisitor(String filePath) {
        File logoFile = new File(filePath);
        if (logoFile.exists()) {
            LogUtil.logo("删除以前下载的LOGO或者访客文件：" + filePath, true);
            logoFile.delete();
            RxBus.get().post(new LogoChangeEvent());
        }
    }

    private void startDownLogoFile(String logoPathDown, String logoSize, String savePath, String tag) {
        FileUtils.creatPathNotExcit();
        if (logoPathDown == null || logoPathDown.length() < 3) {
            FileUtils.deleteDirOrFile(savePath, tag + "检测没有需要下载,直接删除本地文件");
            return;
        }
        boolean isNeedDownLogo = jujleLogoFileExict(savePath, logoSize);
        if (!isNeedDownLogo) {
            LogUtil.logo(tag + "=====文件不需要重新下载====");
            return;
        }
        LogUtil.logo(tag + "==== 文件需要重新下载====" + logoSize + " / " + savePath);
        String logoPath = logoPathDown;
        if (!logoPathDown.contains("http")) {
            logoPath = AppInfo.getBaseUrl() + logoPathDown;
        }

//        XutilDownRunnable runnable = new XutilDownRunnable();
//        runnable.setDownInfo(logoPath,savePath,new XutilDownStateListener(){
//
//            @Override
//            public void downStateInfo(XutilDownFileEntity entity) {
//                int statues = entity.getDownState();
//                if (statues==XutilDownFileEntity.DOWN_STATE_SUCCESS){
//                    RxBus.get().post(new LogoChangeEvent());
//                }
//                LogUtil.logo(tag + "下载状态: " + statues);
//            }
//        });
//        AcsService.getInstance().executor(runnable);


        DownImageManager downImageManager = new DownImageManager(contextDown);
        downImageManager.downImageWithGlide(logoPath, savePath, new XutilDownStateListener() {

            @Override
            public void downStateInfo(XutilDownFileEntity entity) {
                if (entity == null) {
                    LogUtil.logo("下载失败: entity==null");
                    return;
                }
                int statues = entity.getDownState();
                if (statues == XutilDownFileEntity.DOWN_STATE_SUCCESS) {
                    RxBus.get().post(new LogoChangeEvent());
                }
                LogUtil.logo(tag + "下载状态222: " + entity.toString());
            }
        });
    }

    /***
     * 判断logo文件是否需要下载
     * @param savePath
     * @param logoSize
     * @return
     */
    private boolean jujleLogoFileExict(String savePath, String logoSize) {
        //没有文件大小，是需要下载的，是老版本
        if (logoSize == null || logoSize.length() < 3) {
            return true;
        }
        try {
            long fileSize = Long.parseLong(logoSize);
            File file = new File(savePath);
            if (!file.exists()) {
                file.createNewFile();
                return true;
            }
            long fileSaveSize = file.length();
            if (Math.abs(fileSize - fileSaveSize) > 2 * 1024) {
                return true;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    /***
     * 提交刷卡通行记录
     * @param person
     * @param passType
     * @param perId
     */
    @Override
    public void insertAccessCardRecordService(PersonBean person, String passType, String perId, String cardCode) {
        String phone = person.getPhone();
        String requestUtl = AppInfo.putAccessCardRecord();
        String recordTime = SimpleDateUtil.formatTaskTimeShow(System.currentTimeMillis());
        String deviceId = SpUtils.getDeviceBeanId();
        Map<String, String> params = new LinkedHashMap<String, String>();
        params.put("recordTime", recordTime + "");
        params.put("passType", passType + "");
        params.put("gateEquipId", deviceId + "");
        params.put("phone", phone + "");
        params.put("validCode", cardCode + "");
        params.put("temperature", "No Open");
        params.put("isFever", "0");

        OkHttpUtils
                .post()
                .url(requestUtl)
                .params(params)
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, String e, int id) {
                        LogUtil.update("=====上传刷卡效果===failed=" + e.toString());
                    }

                    @Override
                    public void onResponse(String response, int id) {
                        LogUtil.update("=====上传刷卡效果===success=" + response);
                    }
                });
    }

    /***
     * 插入通行记录
     * @param passType
     * @param validCode
     * @param accessFilePath
     * @param temperature
     */
    @Override
    public void insertAccessFaceRecordService(PersonBean personEntity, String passType, String validCode, String accessFilePath,
                                              String temperature, String wearMask, String idCardNum, String personName, String qrCodeType) {
        Log.e("update", "==========保存通行记录==insertAccessFaceRecordService==");
        File accessFile = new File(accessFilePath);
        String deviceId = SpUtils.getDeviceBeanId();
        if (deviceId == null || deviceId.length() < 1) {
            deviceId = "";
        }
        String recordTime = SimpleDateUtil.formatTaskTimeShow(System.currentTimeMillis());
        String phone = AppInfo.DEFAULT_PHONE;
        String name = "";
        String cardNum = "";
        if (personEntity != null) {
            phone = personEntity.getPhone();
            if (phone == null || phone.length() < 2) {
                phone = AppInfo.DEFAULT_PHONE;
            }
            name = personEntity.getName();
            if (name == null || name.length() < 2) {
                name = "No Name";
            }
            cardNum = personEntity.getCardNum();
        }
        if (idCardNum != null && idCardNum.length() > 5) {
            cardNum = idCardNum;
        }
        if (personName != null && personName.length() > 1) {
            name = personName;
        }
        updateAccessRecord(
                accessFile,
                recordTime,
                phone,
                validCode,
                temperature,
                accessFilePath, passType, wearMask, cardNum, name, qrCodeType);
    }

    private void updateAccessRecord(File accessFile, String recordTime, String phone, String validCode, String temperature,
                                    String accessFilePath, String passType, String wearMask, String IdcardNum, String personName, String qrCodeType) {
        updateAccessRecordFile(
                recordTime,
                phone,
                validCode,
                temperature,
                accessFilePath,
                passType,
                wearMask,
                IdcardNum,
                personName,
                qrCodeType,
                new UpdateImageListener() {
                    @Override
                    public void updateImageFailed(String errorDesc) {
                        if (accessFile != null) {
                            accessFile.delete();
                        }
                    }

                    @Override
                    public void updateImageProgress(int progress) {

                    }

                    @Override
                    public void updateImageSuccess(String desc) {
//                  Log.e("update", "===========文件上传返回结果===success==" + desc);
                        if (accessFile != null) {
                            accessFile.delete();
                        }
                    }
                }
        );
    }

    UpdateDoorInfoRunnable updateDoorInfoRunnable;

    public void updateAccessRecordFile(
            String recordTime,
            String phone,
            String validCode,
            String temperature,
            String accessFilePath,
            String passType,
            String wearMask,
            String IdcardNum,
            String personName,
            String qrCodeType,
            UpdateImageListener listener) {

        String requestUtl = AppInfo.getUpdateReocrdInfo();
        if (requestUtl == null || requestUtl.length() < 5) {
            LogUtil.update("=========提交通行记录==网址异常，请检查=");
            return;
        }
        LogUtil.update("=========提交通行记录==xxxxx=" + temperature + " / " + wearMask);
        if (updateDoorInfoRunnable == null) {
            updateDoorInfoRunnable = new UpdateDoorInfoRunnable();
        }
        updateDoorInfoRunnable.setUpdateDoorInfo(
                recordTime,
                phone,
                validCode,
                temperature,
                accessFilePath,
                passType,
                wearMask,
                IdcardNum,
                personName,
                qrCodeType,
                listener
        );
        AcsService.getInstance().executor(updateDoorInfoRunnable);
    }

    @Override
    public void syncAccessRecordLocal(AcsUpdateImageListener acsUpdateImageListener) {
        data.clear();
        data = LocalARDao.getAll();
        if (data == null || data.size() < 1) {
            acsUpdateImageListener.updateLocalAtttendSuccess();
            return;
        }
        LogUtil.persoon("开始同步离线通行记录:" + data.size(), true);
        uploadAccessRecord(acsUpdateImageListener);
    }

    //用来封装上传的数据
    List<LocalARBean> data = new ArrayList<LocalARBean>();

    private void uploadAccessRecordNext(AcsUpdateImageListener listener) {
        if (data == null || data.size() < 1) {
            listener.updateLocalAtttendSuccess();
            Log.e("update", "=========文件上传全部完成=");
            return;
        }
        data.remove(0);
        Log.e("update", "=========去上传下一个=" + data.size());
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                uploadAccessRecord(listener);
            }
        }, 26);
    }

    private Handler handler = new Handler();

    /**
     * 递归上传
     */
    private void uploadAccessRecord(AcsUpdateImageListener acsUpdateImageListener) {
        if (data == null || data.size() < 1) {
            Log.e("update", "=========文件上传全部完成=");
            acsUpdateImageListener.updateLocalAtttendSuccess();
            return;
        }
        try {
            acsUpdateImageListener.updateLocalAtttendPersent(data.size());
            LocalARBean localBean = data.get(0);
            if (localBean == null) {
                uploadAccessRecordNext(acsUpdateImageListener);
                return;
            }
            String filePath = localBean.getFilePath();
            File accessFile = new File(filePath);
            if (!accessFile.exists()) {
                LocalARDao.delLocalBeanBy(localBean);
                uploadAccessRecordNext(acsUpdateImageListener);
                return;
            }
            updateAccessRecordFile(
                    localBean.getRecordTime(),
                    localBean.getPhone(),
                    localBean.getValidCode(),
                    localBean.getTemperature(),
                    localBean.getFilePath(),
                    localBean.getPassType(),
                    localBean.getWearMask(),
                    localBean.getCardNum(),
                    localBean.getName(),
                    localBean.getQrCodeType(),
                    new UpdateImageListener() {

                        @Override
                        public void updateImageFailed(String errorDesc) {
                            uploadAccessRecordNext(acsUpdateImageListener);
                        }

                        @Override
                        public void updateImageProgress(int progress) {

                        }

                        @Override
                        public void updateImageSuccess(String desc) {
                            if (localBean != null) {
                                localBean.delete();
                            }
                            if (accessFile != null) {
                                accessFile.delete();
                            }
                            uploadAccessRecordNext(acsUpdateImageListener);
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void upodateDevInfoToAuthorServer(String clVersion) {
        String requestUrl = AppInfo.getUpdateDevToAuthorServer();
        String mac = CodeUtil.getUniquePsuedoID();
        String address = "No Address";
        String getmLongitude = "No Longitude";
        String getmLatitude = "No Latitude";

        OkHttpUtils
                .post()
                .url(requestUrl)
                .addParams("mac", mac)
                .addParams("address", address)
                .addParams("version", clVersion)
                .addParams("longitude", getmLongitude)
                .addParams("latitude", getmLatitude)
                .addParams("useApp", "EFACE")
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, String e, int id) {
                        LogUtil.http("提交设备信息Author failed=" + e.toString());
                    }

                    @Override
                    public void onResponse(String response, int id) {
                        LogUtil.http("提交设备信息Author success=" + response);
                    }
                });
    }
}
