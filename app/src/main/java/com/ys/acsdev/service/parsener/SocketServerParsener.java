package com.ys.acsdev.service.parsener;

import com.ys.acsdev.bean.DeviceBean;
import com.ys.acsdev.commond.AppInfo;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;
import com.ys.acsdev.service.view.SocketServerView;
import com.ys.acsdev.util.CodeUtil;
import com.ys.acsdev.util.LogUtil;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;

public class SocketServerParsener {


    SocketServerView socketServerView;

    public SocketServerParsener(SocketServerView socketServerView) {
        this.socketServerView = socketServerView;
    }

    public void getDevInfoFromWeb() {
        String requestUrl = AppInfo.getSelectGateEquipBySn();
        OkHttpUtils
                .post()
                .url(requestUrl)
                .addParams("sn", CodeUtil.getUniquePsuedoID())
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, String e, int id) {
                        LogUtil.cdl("====获取设备信息=error==" + e.toString());
                        backDeviceInfoStatues(false, -1, null, e.toString());
                    }

                    @Override
                    public void onResponse(String json, int id) {
                        LogUtil.cdl("====获取设备信息===" + json);
                        if (json == null || json.length() < 5) {
                            backDeviceInfoStatues(true, -1, null, "json==null");
                            return;
                        }
                        try {
                            JSONObject jsonObject = new JSONObject(json);
                            int code = jsonObject.getInt("code");
                            String message = jsonObject.getString("msg");
                            if (code != 0) {
                                backDeviceInfoStatues(true, code, null, message);
                                return;
                            }
                            String data = jsonObject.getString("data");
                            parsenerJsonDate(data);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
    }

    private void backDeviceInfoStatues(boolean b, int i, List<DeviceBean> deviceBeanList, String s) {
        if (socketServerView == null) {
            return;
        }
        socketServerView.getDeviceInfoStatues(b, i, deviceBeanList, s);
    }

    private void parsenerJsonDate(String data) {
        if (data == null || data.length() < 5) {
            backDeviceInfoStatues(true, 0, null, "Data is null");
            return;
        }
        try {
            JSONArray jsonArray = new JSONArray(data);
            int count = jsonArray.length();
            if (count < 1) {
                backDeviceInfoStatues(true, 0, null, "no deviceInfo");
                return;
            }
            List<DeviceBean> deviceBeanList = new ArrayList<DeviceBean>();
            for (int i = 0; i < count; i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                String id = jsonObject.getString("id");
                String imUserId = jsonObject.getString("createUserId");
                String equipName = jsonObject.getString("equipName");
                String password = jsonObject.getString("password");
                String appId = jsonObject.getString("appId");
                String deptId = jsonObject.getString("deptId");
                String deptName = jsonObject.getString("deptName");
                String companyName = jsonObject.getString("companyName");
                DeviceBean deviceBean = new DeviceBean(id, equipName, password, imUserId, appId, deptName, companyName, deptId);
                deviceBeanList.add(deviceBean);
            }
            backDeviceInfoStatues(true, 0, deviceBeanList, "data==null");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
