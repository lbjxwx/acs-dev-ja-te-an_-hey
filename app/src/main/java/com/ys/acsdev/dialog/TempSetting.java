package com.ys.acsdev.dialog;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.ImageView;

import androidx.viewbinding.ViewBinding;

import com.ys.acsdev.R;
import com.ys.acsdev.bean.BeanDefaultEntity;
import com.ys.acsdev.bean.RedioEntity;
import com.ys.acsdev.databinding.DialogTempSettingBinding;
import com.ys.acsdev.dialog.base.BaseDialog;
import com.ys.acsdev.listener.MoreButtonListener;
import com.ys.acsdev.listener.MoreButtonToggleListener;
import com.ys.acsdev.listener.RadioChooiceListener;
import com.ys.acsdev.manager.TempManager;
import com.ys.acsdev.setting.TempCalibraIrActivity;
import com.ys.acsdev.setting.TempSettingListActivity;
import com.ys.acsdev.setting.TempSettingsActivity;
import com.ys.acsdev.setting.entity.TempModelEntity;
import com.ys.acsdev.setting.util.TempModelUtil;
import com.ys.acsdev.util.SpUtils;
import com.ys.acsdev.view.MoreButton;
import com.ys.acsdev.view.MoreButtonToggle;
import com.ys.acsdev.view.MyToastView;
import com.ys.acsdev.view.dialog.EditTextDialog;
import com.ys.acsdev.view.dialog.RadioListDialog;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class TempSetting extends BaseDialog {

    private DialogTempSettingBinding mBindView;
    private MoreButtonToggleListener toggleListener;
    private MoreButtonListener moreListener;
    
    private List<String> mDevicePaths = new ArrayList<>();
    private Activity activity;

    public TempSetting(Context context) {
        super(context);
        activity = (Activity) context;
    }

    @Override
    protected ViewBinding getBinding() {
        mBindView = DialogTempSettingBinding.inflate(getInflater());
        return mBindView;
    }

    @Override
    protected void initView() {
        initDevPaths();
        toggleListener = new MoreButtonToggleListener() {
            @Override
            public void switchToggleView(View view, boolean isChooice) {
                switch (view.getId()) {
                    case R.id.btn_switch:
                        if (isChooice) {
                            SpUtils.setmTempType(TempModelEntity.IIC_MLX_90621_BAB);
                        } else {
                            SpUtils.setmTempType(TempModelEntity.CLOSE);
                        }
                        break;
                    case R.id.more_height_speak:
                        SpUtils.setPlayWearMedia(isChooice);
                        initData();
                        break;
                }
            }
        };
        moreListener = new MoreButtonListener() {
            @Override
            public void clickView(View view) {
                switch (view.getId()) {
                    case R.id.more_hight_wear:
                        showTempHeightDialog();
                        break;
                    case R.id.mBtnTempType:
                        showChhoiceTempType();
                        break;
                    case R.id.mBtnTempUnit:
                        showChooiceTempUnitDialog();
                        break;
                    case R.id.mBtnTempDev:
                        showTempDevDialog();
                        break;
                    case R.id.mBtnTempRare:
                        showTemoRareDialog();
                        break;
                    case R.id.mBtnTempLow:
                        showTempLowDialog();
                        break;
                }
            }
        };
        mBindView.btnSwitch.setOnMoretListener(toggleListener);
        //高温预警功能
        mBindView.moreHightWear.setOnMoretListener(moreListener);
        //硬件类型
        mBindView.mBtnTempType.setOnMoretListener(moreListener);
        mBindView.mBtnTempUnit.setOnMoretListener(moreListener);
        mBindView.mBtnTempDev.setOnMoretListener(moreListener);
        //波特率选择
        mBindView.mBtnTempRare.setOnMoretListener(moreListener);
        //设置有效温度
        mBindView.mBtnTempLow.setOnMoretListener(moreListener);
        mBindView.moreHeightSpeak.setOnMoretListener(toggleListener);

        mBindView.mBtnSave.setOnClickListener(v -> dismiss());
    }

    @Override
    protected void onStart() {
        initData();
    }

    private void initDevPaths() {
        File dev = new File("/dev");
        File[] files = dev.listFiles();
        int i;
        for (i = 0; i < files.length; i++) {
            if (files[i].getAbsolutePath().startsWith("/dev/tty")) {
                mDevicePaths.add(files[i].getAbsolutePath());
            }
        }
    }


    /***
     * 显示高温预警得温度弹窗
     */
    private void showTempHeightDialog() {
        float tempSave = SpUtils.getTempHeightWearNew();
        EditTextDialog editTextDialog = new EditTextDialog(activity);
        editTextDialog.setOnDialogClickListener(new EditTextDialog.EditTextDialogListener() {
            @Override
            public void commit(String content) {
                if (content == null || content.length() < 2) {
                    showToast(getContext().getString(R.string.effective_value));
                    return;
                }
                float tempFloat = 20.0f;
                try {
                    tempFloat = Float.parseFloat(content);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (tempFloat < 37.0) {
                    showToast(getContext().getString(R.string.effective_value));
                    return;
                }
                SpUtils.setTempHeightWearNew(tempFloat);
                showToast(getContext().getString(R.string.set_success));
                initData();
            }

            @Override
            public void hiddleTestClick() {

            }
        });
        editTextDialog.show(
                getContext().getString(R.string.temp_height_wear),
                tempSave + "",
                getContext().getString(R.string.submit),
                getContext().getString(R.string.cacel)
        );
    }


    private void initData() {
        mBindView.btnSwitch.setChcekcAble(SpUtils.getmTempType() != TempModelEntity.CLOSE);
        mBindView.moreSpeakTemp.setChcekcAble(SpUtils.getSpeakTempNum());
        mBindView.moreHeightSpeak.setChcekcAble(SpUtils.getPlayWearMedia());
        mBindView.moreHightWear.setRigt(SpUtils.getTempHeightWearNew() + "");
        mBindView.mBtnTempLow.setRigt(SpUtils.getmTempLow() + "");

        String tempUnit = "";
        if (SpUtils.getTempUnit() == 0) {
            tempUnit = getContext().getString(R.string.centigrade);
        } else {
            tempUnit = getContext().getString(R.string.fahrenhite);
        }
        mBindView.mBtnTempUnit.setRigt(tempUnit);
        String tempTye = TempModelUtil.getCurrentTempType(getContext());
        mBindView.mBtnTempType.setRigt(tempTye);
        updateTempView(SpUtils.getmTempType());
        mBindView.mBtnTempDev.setRigt(SpUtils.getTempDevPath());
        String tempRare = TempModelUtil.getmCurrenTempRare();
        mBindView.mBtnTempRare.setRigt(tempRare);
    }

    /***
     * 温度设置界面的显示与隐藏
     */
    private void updateTempView(int chooicePosition) {
        //mBindView.llInfo.setVisibility(chooicePosition == 0 ? View.GONE : View.VISIBLE);
    }

    private void showTempDevDialog() {
//        List<BeanDefaultEntity> beanDefaultEntityList = TempModelUtil.getDevTTYSList();
        RadioListDialog radioListDialog = new RadioListDialog(getContext());
        List<RedioEntity> listShow = new ArrayList<RedioEntity>();
        for (String devPath : mDevicePaths) {
            listShow.add(new RedioEntity(devPath));
        }
//        for (BeanDefaultEntity beanDefaultEntity : beanDefaultEntityList) {
//            listShow.add(new RedioEntity(beanDefaultEntity.getDefaultDesc()));
//        }
        int selected = mDevicePaths.indexOf(SpUtils.getTempDevPath());
        String showDesc = getContext().getString(R.string.temp_type_dev);
        radioListDialog.show(
                getContext().getString(R.string.serial_port_selection),
                listShow,
                selected,
                showDesc
        );
        radioListDialog.setRadioChooiceListener((redioEntity, chooicePosition) -> {
            SpUtils.setTempDevPath(mDevicePaths.get(chooicePosition));
//            SpUtils.setmTempDev(chooicePosition);
            initData();
        });
    }

    private void showTemoRareDialog() {
        RadioListDialog radioListDialog = new RadioListDialog(getContext());
        List<BeanDefaultEntity> entityList = TempModelUtil.getmTempRareList();
        List<RedioEntity> listShow = new ArrayList<RedioEntity>();
        for (BeanDefaultEntity beanDefaultEntity : entityList) {
            listShow.add(new RedioEntity(beanDefaultEntity.getDefaultDesc()));
        }
        String showDesc = getContext().getString(R.string.temp_type_dev);
        radioListDialog.show(
                getContext().getString(R.string.baud),
                listShow,
                SpUtils.getmTempRare(),
                showDesc
        );
        radioListDialog.setRadioChooiceListener(new RadioChooiceListener() {
            @Override
            public void backChooiceInfo(RedioEntity redioEntity, int chooicePosition) {
                SpUtils.setmTempRare(chooicePosition);
                initData();
            }
        });
    }

    private void showTempLowDialog() {
        EditTextDialog editTextDialog = new EditTextDialog(activity);
        editTextDialog.setOnDialogClickListener(new EditTextDialog.EditTextDialogListener() {
            @Override
            public void commit(String content) {
                if (content == null || content.length() < 2) {
                    showToast(getContext().getString(R.string.set_failed));
                    return;
                }
                float floatNum = 30.0f;
                try {
                    floatNum = Float.parseFloat(content);
                    SpUtils.setmTempLow(floatNum);
                    showToast(getContext().getString(R.string.set_success));
                    initData();
                } catch (Exception e) {
                    showToast(getContext().getString(R.string.set_failed));
                    e.printStackTrace();
                }
            }

            @Override
            public void hiddleTestClick() {

            }
        });
        String lowTemp = SpUtils.getmTempLow() + "";
        editTextDialog.show(
                getContext().getString(R.string.tv_temp_low),
                lowTemp,
                getContext().getString(R.string.submit),
                "");
    }

    private void showChhoiceTempType() {
        List<TempModelEntity> tempTypeList = TempModelUtil.getTempListByUser(getContext());
        RadioListDialog radioListType = new RadioListDialog(getContext());
        List<RedioEntity> listShow = new ArrayList<RedioEntity>();
        int checkIndex = 0;
        for (int i = 0; i < tempTypeList.size(); i++) {
            listShow.add(new RedioEntity(tempTypeList.get(i).getTempName()));
            if (tempTypeList.get(i).getTempTyle() == SpUtils.getmTempType())
                checkIndex = i;
        }
        String showDesc = getContext().getString(R.string.temp_type_dev);
        radioListType.show(
                getContext().getString(R.string.hardware_type),
                listShow,
                checkIndex,
                showDesc);
        radioListType.setRadioChooiceListener(new RadioChooiceListener() {
            @Override
            public void backChooiceInfo(RedioEntity redioEntity, int chooicePosition) {
                TempModelEntity tempModelEntity = tempTypeList.get(chooicePosition);
                SpUtils.setmTempType(tempModelEntity.getTempTyle());
                initData();
                if (SpUtils.getmTempType() == TempModelEntity.CLOSE) {
                    TempManager.getInstance().destory();
                    dismiss();
                } else {
                    updateTempView(tempModelEntity.getTempTyle());
                }
            }
        });
    }

    private void showChooiceTempUnitDialog() {
        RadioListDialog radioListDialogTemp = new RadioListDialog(getContext());
        List<RedioEntity> listShowTemp = new ArrayList<RedioEntity>();
        listShowTemp.add(new RedioEntity(getContext().getString(R.string.centigrade)));
        listShowTemp.add(new RedioEntity(getContext().getString(R.string.fahrenhite)));
        radioListDialogTemp.show(
                getContext().getString(R.string.tv_temp_unit),
                listShowTemp,
                SpUtils.getTempUnit(),
                ""
        );
        radioListDialogTemp.setRadioChooiceListener(new RadioChooiceListener() {
            @Override
            public void backChooiceInfo(RedioEntity redioEntity, int chooicePosition) {
                SpUtils.setTempUnit(chooicePosition);
                changeWearHeightWear();
                showToast(getContext().getString(R.string.set_success));
                initData();
            }
        });
    }

    private void changeWearHeightWear() {
        float wearHeightTemp = SpUtils.getTempHeightWearNew();
        float modifyWearHeightTemp = wearHeightTemp;
        int tempType = SpUtils.getTempUnit();
        if (tempType == 0) {
            //摄氏度
            if (wearHeightTemp > 50) {
                modifyWearHeightTemp = (wearHeightTemp - 32) / 1.8f;
//                temp = temp * 1.8f + 32
            }
        } else if (tempType == 1) {
            //华式摄氏度
            if (wearHeightTemp < 50) {
                modifyWearHeightTemp = wearHeightTemp * 1.8f + 32;
            }
        }
        SpUtils.setTempHeightWearNew(modifyWearHeightTemp);
    }

    public void showToast(String msg) {
        MyToastView.getInstance().Toast(getContext(), msg);
    }

}
