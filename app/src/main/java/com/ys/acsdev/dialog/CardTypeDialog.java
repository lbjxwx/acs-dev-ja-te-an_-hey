package com.ys.acsdev.dialog;

import android.content.Context;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.viewbinding.ViewBinding;

import com.ys.acsdev.R;
import com.ys.acsdev.adapter.BaseViewHolder;
import com.ys.acsdev.adapter.QuickAdapter;
import com.ys.acsdev.bean.CheckEntry;
import com.ys.acsdev.databinding.DialogCardTypeBinding;
import com.ys.acsdev.dialog.base.BaseDialog;
import com.ys.acsdev.util.SpUtils;

import java.util.ArrayList;
import java.util.List;

public class CardTypeDialog extends BaseDialog {

    private DialogCardTypeBinding mBindView;
    private QuickAdapter<CheckEntry> adapter;

    public CardTypeDialog(Context context) {
        super(context);
    }

    @Override
    protected ViewBinding getBinding() {
        mBindView = DialogCardTypeBinding.inflate(getInflater());
        return mBindView;
    }

    @Override
    protected void initView() {
        adapter = new QuickAdapter<CheckEntry>(R.layout.item_check_box) {
            @Override
            protected void convert(BaseViewHolder holder, CheckEntry item) {
                holder.setText(R.id.tv_name, item.name);
                if (item.check) {
                    holder.setBackgroundResource(R.id.iv_check, R.mipmap.radio_chooice);
                } else {
                    holder.setBackgroundResource(R.id.iv_check, R.mipmap.radio_default);
                }
            }
        };
        adapter.setOnItemClickListener((baseAdapter, view, position) -> {
            CheckEntry item = adapter.getItem(position);
            item.check = !item.check;
            adapter.notifyDataChanged();
            cardTypeChange();
        });
        mBindView.rvList.setAdapter(adapter);
        mBindView.rvList.setLayoutManager(new LinearLayoutManager(getContext()));

        mBindView.btnCancel.setOnClickListener(v -> dismiss());
        mBindView.btnYes.setOnClickListener(v -> {
            cardTypeChange();
            dismiss();
        });
    }

    @Override
    protected void onStart() {
        String types = SpUtils.getRongCardType();
        if (types == null) {
            types = "";
        }
        List<CheckEntry> data = new ArrayList<>();
        data.add(new CheckEntry("0100", "普通卡", types.contains("0100")));
        data.add(new CheckEntry("0300","学生卡", types.contains("0300")));
        data.add(new CheckEntry("0600","成人卡", types.contains("0600")));
        data.add(new CheckEntry("0800","敬老卡", types.contains("0800")));
        adapter.setNewData(data);
    }

    private void cardTypeChange() {
        StringBuilder builder = new StringBuilder();
        for(CheckEntry entry : adapter.getData()) {
            if (entry.check) {
                builder.append(entry.type);
                builder.append(',');
            }
        }
        if (builder.length() > 0) {
            builder.deleteCharAt(builder.length() - 1);
        }
        SpUtils.setRongCardType(builder.toString());
    }

}
