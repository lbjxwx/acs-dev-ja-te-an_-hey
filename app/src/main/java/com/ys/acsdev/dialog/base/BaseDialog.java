package com.ys.acsdev.dialog.base;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Window;
import android.view.WindowManager;

import androidx.viewbinding.ViewBinding;

import com.ys.acsdev.R;


public abstract class BaseDialog implements IDialog {

    private final Dialog mDialog;
    private WindowParams windowParams;
    private final ViewBinding mBinding;

    public BaseDialog(Context context) {
        this(context, R.style.panel_dialog);
    }

    public BaseDialog(Context context, int themeResId) {
        mDialog = new Dialog(context, themeResId) {
            @Override
            protected void onStart() {
                super.onStart();
                Window window = getWindow();
                WindowManager.LayoutParams lp = window.getAttributes();
                if (windowParams != null) {
                    windowParams.pasteParams(lp);
                    window.setAttributes(lp);
                } else {
                    windowParams = new WindowParams();
                }
                windowParams.copyParams(lp);
                BaseDialog.this.onStart();
            }

            @Override
            protected void onCreate(Bundle savedInstanceState) {
                super.onCreate(savedInstanceState);
                setContentView(mBinding.getRoot());
                initView();

            }

            @Override
            public void dismiss() {
                super.dismiss();
            }
        };

        mBinding = getBinding();
    }

    protected abstract ViewBinding getBinding();

    protected abstract void initView();

    protected void onStart() {

    }

    @Override
    public void show() {
        mDialog.show();
    }

    @Override
    public void dismiss() {
        if (mDialog.isShowing()) {
            mDialog.dismiss();
        }
    }

    public LayoutInflater getInflater() {
        return LayoutInflater.from(mDialog.getContext());
    }

    public void setCancelable(boolean flag) {
        mDialog.setCancelable(flag);
    }

    public Dialog getDialog() {
        return mDialog;
    }

    public Window getWindow() {
        return mDialog.getWindow();
    }

    public Context getContext() {
        return mDialog.getContext();
    }

    public void setWindowParams(WindowParams params) {
        windowParams = params;
    }

    public void setOnKeyListener(DialogInterface.OnKeyListener listener) {
        mDialog.setOnKeyListener(listener);
    }

    public WindowParams getWindowParams() {
        if (windowParams == null) {
            windowParams = new WindowParams();
        }
        return windowParams;
    }
}
