package com.ys.acsdev.dialog.base;

public interface IDialog {
    void show();
    void dismiss();
}
