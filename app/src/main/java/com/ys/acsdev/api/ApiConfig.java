package com.ys.acsdev.api;

public class ApiConfig {

    //健康码请求接口
    public static final String HEALTHY_REQUEST_URL = "https://o.cworker.cn";

    /**
     * 获取登录接口
     *
     * @return
     */
    public static String getLoginUrl() {
        return HEALTHY_REQUEST_URL + "/connect/token";
    }
}
