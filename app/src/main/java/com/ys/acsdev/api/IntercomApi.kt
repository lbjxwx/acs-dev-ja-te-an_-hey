package com.ys.acsdev.api

import com.huiming.common.ext.BaseResp
import com.ys.acsdev.attendance.bean.EmpAttenInfoEntity
import com.ys.acsdev.attendance.bean.EmptyScheduling
import com.ys.acsdev.attendance.bean.FixScheduleEntity
import com.ys.acsdev.bean.*
import io.reactivex.Observable
import okhttp3.MultipartBody
import retrofit2.http.*

interface IntercomApi {

    companion object {
        const val BASE_PATH: String = "/cloudIntercom/"

        //运行日志上传
        const val UPDATE_RUN_LOG_PATH = BASE_PATH + "api/uploadEquipmentLog"
    }

    @FormUrlEncoded
    @POST("${BASE_PATH}api/putFaceRegFail") //Post请求发送数据
    fun putFaceRegFail(@Field("failRegInfo") failRegInfo: String): Observable<BaseResp<String>>

    /**
     * 获取当前任务信息
     */

    @FormUrlEncoded
    @POST("${BASE_PATH}api/getAdvertTask")
    fun findByCommunityId(
        @Field("sn") sn: String
    ): Observable<String>

//    //查询门禁记录
//    @FormUrlEncoded
//    @POST("${BASE_PATH}api/selectAccessRecordBySn")
//    fun getAccessRecord(
//        @Field("sn") sn: String
//    ): Observable<BaseResp<List<AccessRecordBean>>>

    @GET("${BASE_PATH}api/selectBindPersonBySn")
    fun getAllPerson(@Query("sn") sn: String): Observable<BaseResp<List<PersonBean>>>

    @POST("${BASE_PATH}api/uploadFingerprint")
    @Multipart
    fun uploadFI(
        @Part("psId") psId: String,
        @Part file: MultipartBody.Part?
    ): Observable<BaseResp<Any>>

    /**
     * 无考勤打卡
     */
    @FormUrlEncoded
    @POST("${BASE_PATH}api/freeSign")
    fun freeSignIn(@FieldMap params: Map<String, String>): Observable<BaseResp<Any>>

}