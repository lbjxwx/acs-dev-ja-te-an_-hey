package com.ys.acsdev.api;

public class AppConfig {

    /**
     * 打包打印机版本检查两个地方：
     * 1.gpprintlib下的gradle,将其注释
     * 2.temperaturelib,将串口引入的方式改变一下
     * 此二者不兼容
     */
    public static final int APP_CUSTOM_DEFAULT = 0;           //默认
    public static final int APP_CUSTOM_JTA_DEFAULT = 10;      //佳特安，刷卡
    public static final int APP_CUSTOM_JTA_ID_CARD = 1001;    //佳特安，刷身份证 + 健康码人证比对
    public static final int APP_CUSTOM_JTA_HID = 1002;        //佳特安，HID- 刷卡程序
    public static final int APP_CUSTOM_TEST = 1003;
    //用来区分客户需求
    public static final int APP_CUSTOM_TYPE = APP_CUSTOM_DEFAULT;

    public static final int PARSENE_ALL_TYPE = 0;  //全量查询
    public static final int PARSENE_ADD_TYPE = 1;  //增量查询
    public static final int APP_PARSENE_TYPE = PARSENE_ALL_TYPE;

    //用来测试限速下载  单位KB/S
    public static final int DOWN_SPEED_LIMIT = 3000;
    //守护进程的包名===========================================================================================
    public static final String GUARDIAN_PACKAGE_NAME = "com.guardian";
    //守护app文件大小
    public static final long GUARDIAN_APK_LENGTH_71 = 1633238;
    public static final String GUARDIAN_APP_NAME = "guardian.apk";
    public static final int GUARDIAN_VERSION = 51;
}
