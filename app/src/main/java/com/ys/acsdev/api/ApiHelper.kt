package com.ys.acsdev.api

import androidx.lifecycle.LifecycleOwner
import com.huiming.common.ext.RxExt
import com.huiming.common.ext.bindLife
import com.huiming.common.ext.schedulers
import com.huiming.common.net.RetrofitHelper
import com.ys.acsdev.bean.*
import com.ys.acsdev.commond.AppInfo

object ApiHelper {

    var mApiServer: IntercomApi? = null

    @JvmStatic
    fun putFaceRegFail(body: String) =
        createApi().putFaceRegFail(body).compose(RxExt.handleResult<String>())

    /**
     * 获取当前小区得广告信息
     */
    @JvmStatic
    fun findByCommunityId(sn: String) = createApi()
        .findByCommunityId(sn)


    @JvmStatic
    fun freeSignIn(params: Map<String, String>, lifecycleOwner: LifecycleOwner? = null) =
        createApi().freeSignIn(params).compose(RxExt.handleResult<Any>()).let {
            if (lifecycleOwner != null)
                it.bindLife(lifecycleOwner)
            else
                it
        }.schedulers()

    @JvmStatic
    fun getAllPerson(sn: String) =
        createApi().getAllPerson(sn).compose(RxExt.handleResult<List<PersonBean>>())

    fun createApi(isReset: Boolean = false): IntercomApi {
        if (mApiServer == null || isReset) {
            mApiServer = RetrofitHelper.createApi(IntercomApi::class.java, AppInfo.getBaseUrl())
        }
        return mApiServer!!
    }
}