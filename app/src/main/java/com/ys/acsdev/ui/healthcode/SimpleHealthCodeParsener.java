package com.ys.acsdev.ui.healthcode;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.signature.ObjectKey;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;
import com.ys.acsdev.R;
import com.ys.acsdev.api.AppConfig;
import com.ys.acsdev.bean.DeviceBean;
import com.ys.acsdev.commond.AppInfo;
import com.ys.acsdev.db.DbTempCompensate;
import com.ys.acsdev.manager.ImageManager;
import com.ys.acsdev.service.AcsService;
import com.ys.acsdev.service.SocketService;
import com.ys.acsdev.ui.face.FaceViewModel;
import com.ys.acsdev.ui.face.view.FacePassView;
import com.ys.acsdev.util.CodeUtil;
import com.ys.acsdev.util.LogUtil;
import com.ys.acsdev.util.NetWorkUtils;
import com.ys.acsdev.util.SimpleDateUtil;
import com.ys.acsdev.util.SpUtils;
import com.ys.qrcodelib.QRCodeUtil;

import java.io.File;

import okhttp3.Call;

public class SimpleHealthCodeParsener {

    Context context;
    FacePassView facePassView;
    SimpleHealthCodeModel mVm;
    public long mSleepTime = 30 * 1000;

    public SimpleHealthCodeParsener(Context context, FacePassView facePassView, SimpleHealthCodeModel viewModel) {
        this.context = context;
        this.facePassView = facePassView;
        this.mVm = viewModel;
        getView();
    }

    ImageView mIvNet, mIvServer, mIvLogo;
    TextView mTvVersion, mTvTime, mTvCompany, mTvDep, mTvDevName, tv_dev_mac;
    ImageView mDevQrCode;

    private void getView() {
        mIvNet = facePassView.getIvNet();
        mIvServer = facePassView.getIvServer();
        mTvVersion = facePassView.getTvAppVersion();
        mTvTime = facePassView.getTvShowTime();
        mTvCompany = facePassView.getTvCompany();
        mTvDep = facePassView.getTvDep();
        mTvDevName = facePassView.getTvDevName();
        tv_dev_mac = facePassView.getTvMac();
        mIvLogo = facePassView.getIvLogo();
        mDevQrCode = facePassView.getQrBindImageView();
    }

    private static final int MSG_SLEEP = 2;
    private static final int SHOW_DEFAULT_IMAGE_LOGO = 5621;
    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            removeMessages(msg.what);
            switch (msg.what) {
                case SHOW_DEFAULT_IMAGE_LOGO:
                    loadMainLogo();
                    break;
                case MSG_SLEEP:
                    LogUtil.e("========onHasFaceChange： handler showSleepView");
                    if (mVm != null) {
                        mVm.getMSleepState().postValue(true);
                    }
                    break;
            }
        }
    };

    /***
     * 跟新设备信息到服务器
     */
    public void updateDevInfoToWeb() {
        if (!NetWorkUtils.isNetworkConnected(context)) {
            return;
        }
        String ipAddress = NetWorkUtils.getIPAddress(context);
        String sn = CodeUtil.getUniquePsuedoID();
        String requestUrl = AppInfo.getUpdateEquipmentBySn();
        String softVersion = CodeUtil.getVersionAll(context);
        OkHttpUtils
                .post()
                .url(requestUrl)
                .addParams("sn", sn)
                .addParams("ip", ipAddress + "")
                .addParams("softVersion", softVersion + "")
                .addParams("faceState", "1")
                .addParams("snapState", "1")
                .addParams("camerasNumber", "1")
                .addParams("keypadState", "1")
                .addParams("newworkType", "1")
                .addParams("longitude", "")
                .addParams("latitude", "")
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, String e, int id) {
                        LogUtil.cdl("====提交设备信息==onError==" + e.toString());
                    }

                    @Override
                    public void onResponse(String response, int id) {
                        LogUtil.cdl("====提交设备信息==success==" + response);
                    }
                });
    }

    /***
     * 显示休眠界面
     */
    public void showSleepView() {
        LogUtil.e("========onHasFaceChange： parsener showSleepView");
        handler.removeMessages(MSG_SLEEP);
        handler.sendEmptyMessageDelayed(MSG_SLEEP, mSleepTime);
    }

    public void hiddleSleepView() {
        handler.removeMessages(MSG_SLEEP);
        if (mVm != null) {
            mVm.getMSleepState().postValue(false);
        }
    }

    /***
     * 跟新状态信息
     */
    public void updateTopViewTime() {
        boolean netAvailable = NetWorkUtils.isNetworkConnected(context);
        if (mIvNet != null) {
            mIvNet.setImageResource(netAvailable ? R.mipmap.net_line : R.mipmap.net_disline);
        }
        if (mTvTime != null) {
//            String showTime = SimpleDateUtil.formatNowTime();
            String showTime = SimpleDateUtil.getCurrentTimeJSTHm();
            LogUtil.cdl("=======显示得温度===" + showTime);
            mTvTime.setText(showTime + "");
        }

        String appVersion = CodeUtil.getVersionAll(context);
        if (mTvVersion != null) {
            mTvVersion.setText(appVersion);
        }
        if (mIvServer != null) {
            if (SocketService.getInstance() != null && AppInfo.isConnectServer) {
                mIvServer.setImageResource(R.mipmap.ic_online);
            } else {
                mIvServer.setImageResource(R.mipmap.ic_offline);
            }
        }
        loadMainLogo();
        //跟新温度补偿值
        DbTempCompensate.getCurrentTempAdd();
        //每个小时跟新信息到服务器一次
        long currentTime = SimpleDateUtil.getCurrentHourMin();
        if (currentTime % 100 == 0) {
            updateDevInfoToWeb();
        }
        //跟新恭喜相关得信息
        updateCompanyDepartment();
    }

    public void updateCompanyDepartment() {
        DeviceBean deviceInfo = SpUtils.getDeviceInfo();
        String devNmae = CodeUtil.getUniquePsuedoID();
        String companyForst = context.getString(R.string.company);  //默认机构
        String companyName = context.getString(R.string.unknown);
        String deptNameForst = context.getString(R.string.department);//默认分组
        String deptName = context.getString(R.string.unknown);
        if (deviceInfo != null) {
            companyName = deviceInfo.getCompanyName();
            deptName = deviceInfo.getDeptName();
            devNmae = deviceInfo.getEquipName();
        }
        if (mTvCompany != null) {
            mTvCompany.setText(companyForst + companyName);
        }
        if (mTvDep != null) {
            mTvDep.setText(deptNameForst + deptName);
        }
        if (mTvDevName != null) {
            mTvDevName.setText(context.getString(R.string.dev_name) + devNmae);
        }
        if (tv_dev_mac != null) {
            tv_dev_mac.setText("Mac: " + CodeUtil.getUniquePsuedoID());
        }
    }

    public void updateLeftLogoImage(String imagePath) {
        if (imagePath == null || imagePath.length() < 5) {
            loadMainLogo();
            return;
        }
        int imageType = SpUtils.getShowImageType();
        if (imageType == 0) {
            loadMainLogo();
        } else {
            loadMainFaceView(imagePath);
        }
    }

    private void loadMainFaceView(String imagePath) {
        if (mIvLogo == null) {
            return;
        }
        try {
            Glide.with(context).clear(mIvLogo);  //防止缓存，不刷新
            ImageManager.displayImagePathNoCache(context, imagePath, mIvLogo);
            handler.removeMessages(SHOW_DEFAULT_IMAGE_LOGO);
            handler.sendEmptyMessageDelayed(SHOW_DEFAULT_IMAGE_LOGO, 2000);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void loadMainLogo() {
        LogUtil.e("========loadMainLogo=====");
        if (mIvLogo == null) {
            return;
        }
        try {
            int defaultLogo = R.mipmap.app_icon;
            File fileShow = new File(AppInfo.LOGO_PATH);
            Glide.with(context).clear(mIvLogo);  //防止缓存，不刷新
            Glide.with(context)
                    .load(fileShow)
                    .apply(new RequestOptions()
                            .error(defaultLogo)
                            .skipMemoryCache(true)    //禁止Glide内存缓存
                            .diskCacheStrategy(DiskCacheStrategy.NONE)    //不缓存资源
                            .signature(new ObjectKey(System.currentTimeMillis()))
                    ).into(mIvLogo);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onStopParsener() {
        handler.removeMessages(MSG_SLEEP);
    }

    public void updateBindQrCode() {
        if (mDevQrCode == null) {
            return;
        }
        String devData = "{\"type\":\"addDev\",\"mac\": \"" + CodeUtil.getUniquePsuedoID() + "\"}";
        String qrCodePath = AppInfo.QR_BIND_PATH;
        QRCodeUtil runnable = new QRCodeUtil(devData, qrCodePath, new QRCodeUtil.ErCodeBackListener() {

            @Override
            public void createErCodeState(String errorDes, boolean isCreate, String path) {
                if (!isCreate) {
                    return;
                }
                ImageManager.displayImagePath(path, mDevQrCode);
            }
        });
        AcsService.getInstance().executor(runnable);
    }

}
