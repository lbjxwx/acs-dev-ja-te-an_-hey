package com.ys.acsdev.ui.compare;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ImageFormat;
import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.YuvImage;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AbsoluteLayout;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.card.CardInfo;
import com.hwangjr.rxbus.RxBus;
import com.hwangjr.rxbus.annotation.Subscribe;
import com.hwangjr.rxbus.thread.EventThread;
import com.ys.acsdev.R;
import com.ys.acsdev.api.AppConfig;
import com.ys.acsdev.bean.LinkServerEvent;
import com.ys.acsdev.bean.LogoChangeEvent;
import com.ys.acsdev.bean.MinFaceData;
import com.ys.acsdev.bean.PersonBean;
import com.ys.acsdev.bean.TipsInfo;
import com.ys.acsdev.commond.AppInfo;
import com.ys.acsdev.commond.CommondViewModel;
import com.ys.acsdev.db.DataRepository;
import com.ys.acsdev.helper.SoundHelper;
import com.ys.acsdev.light.LightManager;
import com.ys.acsdev.listener.QrDismissListener;
import com.ys.acsdev.manager.DoorManager;
import com.ys.acsdev.manager.TempManager;
import com.ys.acsdev.manager.TtsManager;
import com.ys.acsdev.runnable.upload.RoateImageListener;
import com.ys.acsdev.service.AcsService;
import com.ys.acsdev.setting.SettingMenuActivity;
import com.ys.acsdev.setting.entity.TempModelEntity;
import com.ys.acsdev.task.parsener.TaskPlayerParsener;
import com.ys.acsdev.task.view.TaskPlayView;
import com.ys.acsdev.ui.face.FaceViewModel;
import com.ys.acsdev.ui.face.view.FacePassView;
import com.ys.acsdev.util.ActivityCollector;
import com.ys.acsdev.util.CameraProvider;
import com.ys.acsdev.util.LogUtil;
import com.ys.acsdev.util.SpUtils;
import com.ys.acsdev.util.ToolUtils;
import com.ys.acsdev.util.ViewSizeChange;
import com.ys.acsdev.view.MyToastView;
import com.ys.acsdev.view.dialog.EditTextDialog;
import com.ys.acsdev.view.dialog.QrBottomDialog;
import com.ys.facepasslib.BaseLivenessActivity;
import com.ys.facepasslib.FaceManager;
import com.ys.facepasslib.camera.CameraPreview;
import com.ys.facepasslib.listener.FaceRecDisListener;
import com.ys.facepasslib.view.FaceCircleViewNew;
import com.ys.zkidrlib.ZkidCardResult;

import org.jetbrains.annotations.NotNull;

import java.io.ByteArrayOutputStream;
import java.util.List;

import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import mcv.facepass.types.FacePassCompareResult;
import mcv.facepass.types.FacePassImage;

public class FaceCompareActivity extends BaseLivenessActivity implements FacePassView, TaskPlayView, View.OnClickListener {

    //陌生人警告间隔
    private static final int MSG_RESET_IMAGE = 1;
    private static final String TAG_RESET_TIP = "TAG_RESET_TIP";
    private static final String TAG_RESET_FACE = "TAG_RESET_FACE";
    private static long mKeyActionTime = 0L;
    private int mSaveRotate = 0;

    FaceCompareModel mVM;
    FaceCompareParsener facePassParsener = null;
    TaskPlayerParsener taskPlayerParsener = null;

    Animation mScalAnim;
    private boolean mScalAniming = false;
    Bitmap cameraBitmap;

    private void initOther() {
        if (mVM == null) {
            mVM = ViewModelProviders.of(this).get(FaceCompareModel.class);
        }
        if (mScalAnim == null) {
            mScalAnim = AnimationUtils.loadAnimation(this, R.anim.tip_scal);
            mScalAnim.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {
                    mScalAniming = true;
                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    mScalAniming = false;
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
        }
    }

    private static final int CLOSE_WHITE_LIGHT_MESSAGE = 56845;
    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case CLOSE_WHITE_LIGHT_MESSAGE:
                    removeMessages(CLOSE_WHITE_LIGHT_MESSAGE);
                    LightManager.getInstance().closeWhiteLight();
                    break;
                case MSG_RESET_IMAGE:
                    removeMessages(MSG_RESET_IMAGE);
                    break;
            }
        }
    };

    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            LogUtil.task("==============监听到广播====" + action);
            if (action.equals(Intent.ACTION_TIME_TICK)) {
                updateTopViewTime();
                if (taskPlayerParsener != null) {
                    taskPlayerParsener.updateSleepTvOnTime();
                }
            } else if (action.equals(AppInfo.DOWN_TASK_SUCCESS)) {
                if (mFlSleep.getVisibility() == View.VISIBLE) {
                    taskPlayerParsener.getTaskFromDb();
                }
            } else if (action.equals(AppInfo.UPDATE_MAIN_BOTTOM_VIEW)) {
                if (facePassParsener != null) {
                    facePassParsener.updateCompanyDepartment();
                }
            }
        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_face_compare);

        initOther();
        initView();
        initReceiver();
        initFaceConfig();
        //用来关闭所有界面的，这里不要注释
        ActivityCollector.addActivity(this);
    }

    CameraPreview mCv;
    ImageView mIvLogo, mBtnSettings;
    View view_click;
    FaceCircleViewNew mFrv;
    TextView mTvTemp, mTvInfo;

    TextView mTvVersion, mTvTime, mTvCompany, mTvDep, mTvDevName, tv_dev_mac;
    ImageView mIvServer, mIvNet;
    FrameLayout mFlSleep;
    AbsoluteLayout mAdView;
    CameraPreview mIRCv;
    ImageView mIvCheck, mDevQrCode, mIvCali;
    FrameLayout mFlNoInit;
    LinearLayout mLlInfo;
    ImageView iv_temp_show;

    LinearLayout mLlCompare;
    TextView tv_compare_tips;
    ImageView mIvCompareCamera;
    ImageView mIvCompareIdcard;
    boolean mIsCompare;
    boolean mIsCompareing;

    private void initView() {
        mLlCompare = findViewById(R.id.ll_compare);
        tv_compare_tips = findViewById(R.id.tv_compare_tips);
        mIvCompareCamera = findViewById(R.id.iv_compare_camera);
        mIvCompareIdcard = findViewById(R.id.iv_compare_idcard);
        iv_temp_show = (ImageView) findViewById(R.id.iv_temp_show);
        mBtnSettings = (ImageView) findViewById(R.id.mBtnSettings);
        mBtnSettings.setOnClickListener(this);
        mLlInfo = (LinearLayout) findViewById(R.id.mLlInfo);
        mFlNoInit = (FrameLayout) findViewById(R.id.mFlNoInit);
        mDevQrCode = findViewById(R.id.dev_qrcode);
        mIRCv = (CameraPreview) findViewById(R.id.mIRCv);
        mIvCheck = (ImageView) findViewById(R.id.mIvCheck);
        mIvCheck.setImageResource(R.mipmap.face_result);
        if (!SpUtils.getShowFaceRectView()) {
            mIvCheck.setVisibility(View.GONE);
        }
        mIvCali = findViewById(R.id.iv_cali);
        mAdView = (AbsoluteLayout) findViewById(R.id.mAdView);
        mFlSleep = (FrameLayout) findViewById(R.id.mFlSleep);
        mIvServer = (ImageView) findViewById(R.id.mIvServer);
        mIvNet = (ImageView) findViewById(R.id.mIvNet);
        mTvVersion = (TextView) findViewById(R.id.mTvVersion);
        mTvTime = (TextView) findViewById(R.id.mTvTime);
        mTvCompany = (TextView) findViewById(R.id.mTvCompany);
        mTvDep = (TextView) findViewById(R.id.mTvDep);
        mTvDevName = (TextView) findViewById(R.id.mTvDevName);
        tv_dev_mac = (TextView) findViewById(R.id.tv_dev_mac);
        mTvInfo = (TextView) findViewById(R.id.mTvInfo);
        mTvTemp = (TextView) findViewById(R.id.mTvTemp);
        mCv = (CameraPreview) findViewById(R.id.mCv);
        ViewSizeChange.setCameraPassFaceSize(mCv);
        mIvLogo = (ImageView) findViewById(R.id.mIvLogo);
        //添加手机触摸，更新logo得功能
        mIvLogo.setOnClickListener(this);
        view_click = (View) findViewById(R.id.view_click);
        view_click.setOnClickListener(this);
        mFrv = (FaceCircleViewNew) findViewById(R.id.mFrv);
        facePassParsener = new FaceCompareParsener(FaceCompareActivity.this, this, mVM);
        facePassParsener.updateBindQrCode();
        initObserve();
        taskPlayerParsener = new TaskPlayerParsener(FaceCompareActivity.this, this);
    }


    //初始化监听
    private void initObserve() {
        CommondViewModel.getInstance().getMFaceInitState().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean it) {
                if (it) {
                    mFlNoInit.setVisibility(View.GONE);
                } else {
                    mFlNoInit.setVisibility(View.VISIBLE);
                }
            }
        });

        CommondViewModel.getInstance().getMNetState().observe(FaceCompareActivity.this, new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean aBoolean) {
                updateTopViewTime();
            }
        });
        mVM.getMSleepState().observe(FaceCompareActivity.this, new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean it) {
                LogUtil.e("=======mSleepState change: " + it);
                mFlSleep.setVisibility(it ? View.VISIBLE : View.GONE);
                if (it) {
                    Intent intent = new Intent();
                    intent.setAction(AppInfo.RECEIVE_MESSAGE_TO_REQUEST_TASK);
                    intent.putExtra(AppInfo.RECEIVE_MESSAGE_TO_REQUEST_TASK, "旷视：initObserve");
                    sendBroadcast(intent);
                } else {
                    taskPlayerParsener.clearTaskView();
                }
            }
        });

        mVM.getMTipsInfo().observe(FaceCompareActivity.this, new Observer<TipsInfo>() {
            @Override
            public void onChanged(TipsInfo it) {
                LogUtil.e("=======changeTip: " + it.toString());
                mTvTemp.setText(it.getTemp());
                mTvTemp.setTextColor(getResources().getColor(it.getTempColor()));
                mTvInfo.setText(it.getText());
                mTvInfo.setBackgroundResource(it.getBgColor());
                if (!mScalAniming)
                    mTvInfo.startAnimation(mScalAnim);
            }
        });

        mVM.getMLazyRestFace().observe(FaceCompareActivity.this, it -> lazyResetFace(it));
        mVM.getMLazyRestTip().observe(FaceCompareActivity.this, aLong -> lazyResetTip(aLong));
        mVM.getMCompare().observe(FaceCompareActivity.this, it -> mIsCompare = true);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.view_click:
                facePassParsener.hiddleSleepView();
                if (!getMHasPerson())
                    facePassParsener.showSleepView();
                break;
            case R.id.mBtnSettings:
                goSettings();
                break;
            case R.id.mIvLogo:
                sendBroadcast(new Intent(AppInfo.CHECK_MAIN_VIEW_LOGO));
                break;
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        updateInfoShow();
        mVM.setMProcessing(false);
        facePassParsener.mSleepTime = SpUtils.getSleepTime() * 1000;
        mVM.setMTempTime(SpUtils.getmTempTime());
        mVM.setMTempEnable(SpUtils.getmTempType() != TempModelEntity.CLOSE);
        mVM.setMAttendEnable(SpUtils.getAttendEnable());
        mVM.setMSimpleModel(SpUtils.getSimpleModel());
        setMOnlyCheck(true);
        setMFaceRectMir(SpUtils.getmFaceRect());
        mKeyActionTime = System.currentTimeMillis();
        setMMaskEnable(SpUtils.getmMaskEnable());
        RxBus.get().register(this);
        resetTip();
        resetFace();
    }


    private void initHasPerson() {
        if (getMHasPerson()) {
            facePassParsener.hiddleSleepView();
            return;
        }
        hideCompare();
        facePassParsener.showSleepView();
    }

    @Override
    protected void onResume() {
        super.onResume();
        initHasPerson();
        updateTopViewTime();
        if (facePassParsener != null) {
            facePassParsener.updateDevInfoToWeb();
            facePassParsener.updateLeftLogoImage(null);
        }
        updateSaveRotate();
        updateDevInfo();
    }

    private void updateSaveRotate() {
        if (SpUtils.getSaveRotate() == 0) {
            mSaveRotate = getMRectRotation();
        } else {
            switch (SpUtils.getSaveRotate()) {
                case 1:
                    mSaveRotate = 0;
                    break;
                case 2:
                    mSaveRotate = 90;
                    break;
                case 3:
                    mSaveRotate = 180;
                    break;
                default:
                    mSaveRotate = 270;
                    break;
            }
        }
        mVM.setMSaveRotate(mSaveRotate);
    }


    @Override
    protected void onPause() {
        super.onPause();
    }

    private void updateDevInfo() {
        boolean isShowTemp = SpUtils.getShowTempIcon();
        iv_temp_show.setVisibility(isShowTemp ? View.VISIBLE : View.GONE);
        mBtnSettings.setBackgroundResource(R.mipmap.icon_setting);
    }

    @Override
    protected void onStop() {
        super.onStop();
        facePassParsener.onStopParsener();
        taskPlayerParsener.clearTaskView();
        setMHasPerson(false);
        if (mHandler != null) {
            mHandler.removeMessages(MSG_RESET_IMAGE);
        }
        TtsManager.getInstance().setSpeechListener(null);
        try {
            stopLazy(TAG_RESET_TIP);
            RxBus.get().unregister(this);
            TempManager.getInstance().setTempCallBack(null);
            LightManager.getInstance().closeAllLight();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            if (receiver != null) {
                unregisterReceiver(receiver);
            }
            if (mVM != null) {
                mVM.destory();
            }
            ActivityCollector.removeActivity(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void lazyResetTip(long time) {
        mVM.delayed(time, TAG_RESET_TIP, new Function0<Unit>() {
            @Override
            public Unit invoke() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        resetTip();
                    }
                });
                return null;
            }
        });
    }

    private void resetTip() {
        LogUtil.e("=======changeTip: resetTip");
        stopLazy(TAG_RESET_FACE);
        mVM.setMProcessing(false);
        mVM.changeTipsInfo(getString(R.string.stand_in_box), R.drawable.bg_tip_blue, "", R.color.green);
        LogUtil.e("=====faceCenter onHasFaceChange resetTip");
        resetFace();
    }

    private void lazyResetFace(long time) {
        stopLazy(TAG_RESET_FACE);
        mVM.delayed(time, TAG_RESET_FACE, new Function0<Unit>() {
            @Override
            public Unit invoke() {
                mVM.setMProcessing(false);
                resetFace();
                return null;
            }
        });
    }

    private void stopLazy(String tag) {
        if (mVM != null) {
            mVM.stopDelayed(tag);
        }
    }

    public void showToastView(String message) {
        MyToastView.getInstance().Toast(this, message);
    }

    private void goSettings() {
        EditTextDialog editTextDialog = new EditTextDialog(this, true);
        editTextDialog.setOnDialogClickListener(new EditTextDialog.EditTextDialogListener() {
            @Override
            public void commit(String it) {
                if (it.isEmpty()) {
                    showToastView(getString(R.string.pwd_is_null));
                    return;
                }
                String password = SpUtils.getSettingPassword();
                //这里内置两个密码，默认123456，778899是给售后用的。请须知
                if (it.equals("778899") || it.equals(password)) {
                    startActivity(new Intent(FaceCompareActivity.this, SettingMenuActivity.class));
                } else {
                    showToastView(getString(R.string.pwd_failed_retry));
                }
            }

            @Override
            public void hiddleTestClick() {
                startActivity(new Intent(FaceCompareActivity.this, SettingMenuActivity.class));
            }
        });

        editTextDialog.show(
                getString(R.string.pwd_verif), "", getString(R.string.confirm), getString(
                        R.string.input_verif_pwd
                )
        );
    }

    @Subscribe(thread = EventThread.MAIN_THREAD)
    public void logoChange(LogoChangeEvent event) {
        LogUtil.cdl("====主界面更新logo====");
        if (facePassParsener != null) {
            facePassParsener.updateLeftLogoImage(null);
        }
    }

    /**
     * Socket服务器连接状态变更
     */
    @Subscribe(thread = EventThread.MAIN_THREAD)
    public void linkServerEvent(LinkServerEvent event) {
        updateTopViewTime();
        facePassParsener.updateDevInfoToWeb();
    }

    private void updateTopViewTime() {
        facePassParsener.updateTopViewTime();
    }

    private void updateInfoShow() {
        mLlInfo.setVisibility(SpUtils.getInfoEnable() ? View.VISIBLE : View.INVISIBLE);
        mIvNet.setVisibility(SpUtils.getHideWifiIcon() ? View.GONE : View.VISIBLE);
        mIvServer.setVisibility(SpUtils.getHideWifiIcon() ? View.GONE : View.VISIBLE);
    }

    private void initFaceConfig() {
        int cameraCount = CameraProvider.getCamerasCount(this);
        LogUtil.e("========获取相机个数: " + cameraCount);
        int currentWidth = SpUtils.getResolutionWidth();
        int currentHeight = SpUtils.getResolutionHeight();
        if (currentWidth == 0 || currentHeight == 0) {
            setMCustomSize(null);
        } else {
            setMCustomSize(new Point(currentWidth, currentHeight));
        }
        //选择是单目还是双目
        setMIsDoubleCamera(SpUtils.getAutoSwitchCamera());
        int prewRoatation = 0;
        switch (SpUtils.getPrewRotate()) {
            case 0:
                prewRoatation = 0;
                break;
            case 1:
                prewRoatation = 90;
                break;
            case 2:
                prewRoatation = 180;
                break;
            default:
                prewRoatation = 270;
                break;
        }
        setMPrewRotation(prewRoatation);

        int rectRoatation = 0;
        switch (SpUtils.getRectRotate()) {
            case 1:
                rectRoatation = 0;
                break;
            case 2:
                rectRoatation = 90;
                break;
            case 3:
                rectRoatation = 180;
                break;
            case 4:
                rectRoatation = 270;
                break;
            default:
                rectRoatation = FaceManager.getInstance().getMFaceRotation();
                break;
        }
        setMRectRotation(rectRoatation);
        setMIsFront(SpUtils.getCameraFacing());
        mFrv.setScreenWidth(SpUtils.getScreenwidth(), SpUtils.getScreenheight(), SpUtils.getShowFaceRectView());

        mFrv.setFaceRectDisListener(new FaceRecDisListener() {
            @Override
            public void rectDistance(float distance, boolean isCenter, float rectWidth, Point headPoint) {
                mVM.setMRectDistance((int) distance);
                TempManager.getInstance().mRectDistance = (int) distance;
                TempManager.getInstance().headPoint = headPoint;
            }
        });
    }

    //==============不涉及到业务逻辑的代码===========================================================================================================
    private void initReceiver() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(Intent.ACTION_TIME_TICK);      //时间变化
        filter.addAction(AppInfo.DOWN_TASK_SUCCESS); //广告下发
        filter.addAction(AppInfo.UPDATE_MAIN_BOTTOM_VIEW);
        registerReceiver(receiver, filter);
    }

    @Override
    public ImageView getQrBindImageView() {
        return mDevQrCode;
    }

    @Override
    public TextView getTvAppVersion() {
        return mTvVersion;
    }

    @Override
    public TextView getTvShowTime() {
        return mTvTime;
    }

    @Override
    public TextView getTvCompany() {
        return mTvCompany;
    }

    @Override
    public TextView getTvDep() {
        return mTvDep;
    }

    @Override
    public AbsoluteLayout getAbView() {
        return mAdView;
    }


    @Override
    public TextView getTvDevName() {
        return mTvDevName;
    }

    @Override
    public TextView getTvMac() {
        return tv_dev_mac;
    }

    @Override
    public ImageView getIvLogo() {
        return mIvLogo;
    }

    @Override
    public ImageView getIvServer() {
        return mIvServer;
    }

    @Override
    public ImageView getIvNet() {
        return mIvNet;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            facePassParsener.hiddleSleepView();
            goSettings();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @NotNull
    @Override
    public CameraPreview getCameraView() {
        return mCv;
    }

    @Override
    public CameraPreview getIrCameraView() {
        return mIRCv;
    }

    @Override
    public FaceCircleViewNew getFaceRectView() {
        return mFrv;
    }

    /**
     * [faceToken] 人脸识别结果
     * [noMask] 未佩戴口罩为true,否则为false
     */
    @Override
    public void recognizeResult(@NotNull String faceToken, boolean noMask, FacePassImage image) {
        LogUtil.e("========recognizeResult： " + faceToken + " / " + mVM.getMProcessing());
        if (!getMHasPerson()) {
            LogUtil.e("========recognizeResult： no has person");
            resetFace();
            return;
        }
        mVM.handlerFaceResult(faceToken, noMask, image == null ? null : new MinFaceData(image.image, image.width, image.height, image.facePassImageRotation));
    }


    @Override
    public void onHasFaceChange(boolean hasFace) {
        if (!SpUtils.getmFaceEnable()) {
            return;
        }
        if (hasFace) {
//            mVM.setMProcessing(false);
            mVM.setMStrangerTime(0);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    facePassParsener.hiddleSleepView();
                    mIvCheck.setImageResource(R.mipmap.face_result);
                }
            });
            LightManager.getInstance().openWhiteLight();
            return;
        }
        hideCompare();
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mIvCheck.setImageResource(R.mipmap.face_check);
            }
        });
        mVM.removeDelayed(FaceViewModel.TAG_HANDLER_FACE);
        facePassParsener.showSleepView();
        lazyResetTip(800);
    }

    //动态获取相机分辨率
    @Override
    public void onSupportedPreviewSizes(@NotNull List<Point> previewsSizes) {
        super.onSupportedPreviewSizes(previewsSizes);
        DataRepository.getInstance().setMSupportedPreview(previewsSizes);
    }

    @Override
    public void cameraPreview(byte[] nv21Data, int width, int height, int rotation) {
        if (nv21Data == null)
            return;
        if (mIsCompare && mLlCompare.getVisibility() == View.GONE) {
            mIsCompare = false;
            cameraBitmap = null;
            AcsService.getInstance().executor(() -> {
                try {
                    YuvImage img = new YuvImage(
                            nv21Data,
                            ImageFormat.NV21,
                            width,
                            height,
                            null
                    );
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    img.compressToJpeg(new Rect(0, 0, width, height), 8, stream);
                    cameraBitmap = BitmapFactory.decodeByteArray(stream.toByteArray(), 0, stream.size());
                    Matrix mat = new Matrix();
                    mat.postRotate(mSaveRotate);
                    cameraBitmap = Bitmap.createBitmap(cameraBitmap, 0, 0, cameraBitmap.getWidth(), cameraBitmap.getHeight(), mat, true);
                    stream.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                runOnUiThread(() -> {
                    showCompare(cameraBitmap, nv21Data, width, height, mSaveRotate);
                });
            });
        }
        if (!mVM.getMIsAccessRecord()) {
            return;
        }
        roatetranBitmapAccessFaceRecord(nv21Data, width, height, mSaveRotate);
    }

    private void roatetranBitmapAccessFaceRecord(byte[] nv21Data, int width, int height, int rotation) {
        PersonBean person = mVM.getMAccessPerson();
        mVM.setMIsAccessRecord(false);
        float tempUpdate = mVM.getTempFloat();
        String wearMask = mVM.getStatuesWearMask();
        ToolUtils.roatetranBitmap(nv21Data, width, height, mSaveRotate, new RoateImageListener() {

            @Override
            public void backImageListener(boolean isSuccess, String imagePath) {
                if (!isSuccess) {
                    return;
                }
                if (imagePath == null || imagePath.length() < 2) {
                    return;
                }
                if (idcardNo == null || idcardNo.length() < 3) {
                    idcardNo = "000";
                }
                if (personName == null || personName.length() < 3) {
                    personName = getString(R.string.stranger);
                }
                facePassParsener.updateLeftLogoImage(imagePath);
                mVM.saveAccessFaceRecord(
                        FaceCompareActivity.this,
                        imagePath,
                        person,
                        tempUpdate,
                        wearMask,
                        idcardNo,
                        personName);

            }
        });
    }

    private String idcardNo = "";  //缓存的身份证号码
    private String personName;  //缓存的人员姓名

    @Subscribe(thread = EventThread.MAIN_THREAD)
    public void idCardCallBack(CardInfo result) {
        SoundHelper.playCard();
        if (mLlCompare.getVisibility() == View.GONE || mIsCompareing) {
            return;
        }
        if (result == null || result.photo == null || cameraBitmap == null) {
            updatePersonTextInfo("获取错误，请重试", getResources().getColor(R.color.red));
            mIsCompareing = false;
            return;
        }
        mIsCompareing = true;
        updatePersonTextInfo("开始比对", getResources().getColor(R.color.grey));
        mIvCompareIdcard.setImageBitmap(result.photo);
        //开始比对
        LogUtil.e("=======compare: 开始比对");
        FacePassCompareResult compareResult = FaceManager.getInstance().singleComparison(cameraBitmap, result.photo);
        if (compareResult == null) {
            updatePersonTextInfo("对比异常，请重试", getResources().getColor(R.color.red));
            showToastView("对比异常，请重试");
            LogUtil.e("=======compare: 对比异常，请重试");
            mIsCompareing = false;
            return;
        }
        if (compareResult.result != 0) {
            updatePersonTextInfo("对比错误，请重试: " + compareResult.result, getResources().getColor(R.color.red));
            showToastView("对比错误，请重试: " + compareResult.result);
            LogUtil.e("=======compare: 对比错误，请重试: " + compareResult.result);
            mIsCompareing = false;
            return;
        }
        //比对分数阀值
        if (compareResult.score >= 60) {
            DoorManager.getInstance().open("人证比对通过，开门");
            if (mVM.getMTempEnable()) {
                updatePersonTextInfo(result.name + ", 验证通过   " + mVM.getMTempShow(), getResources().getColor(R.color.green));
            } else {
                updatePersonTextInfo(result.name + ", 验证通过", getResources().getColor(R.color.green));
            }
            LogUtil.e("=======compare: 验证通过: " + compareResult.score);
            AcsService.getInstance().startSpeakTts(result.name + ",验证通过," + mVM.getMTempShow(), "");
            idcardNo = result.cardNo;
            personName = result.name;
            mVM.setMIsAccessRecord(true);
        } else {
            mIsCompareing = false;
            updatePersonTextInfo(result.name + ", 验证失败", getResources().getColor(R.color.red));
            LogUtil.e("=======compare: 验证失败: " + compareResult.score);
            AcsService.getInstance().startSpeakTts(result.name + ",验证失败", "");
        }
    }

    /**
     * 刷身份证回调
     */
    @Subscribe(thread = EventThread.MAIN_THREAD)
    public void idCardCallBack(ZkidCardResult result) {
        SoundHelper.playCard();
        if (mLlCompare.getVisibility() == View.GONE || mIsCompareing) {
            return;
        }
        if (result == null || result.photo == null || cameraBitmap == null) {
            updatePersonTextInfo("获取错误，请重试", getResources().getColor(R.color.red));
            mIsCompareing = false;
            return;
        }
        mIsCompareing = true;
        updatePersonTextInfo("开始比对", getResources().getColor(R.color.grey));
        mIvCompareIdcard.setImageBitmap(result.photo);
        //开始比对
        LogUtil.e("=======compare: 开始比对");
        FacePassCompareResult compareResult = FaceManager.getInstance().singleComparison(cameraBitmap, result.photo);
        if (compareResult == null) {
            updatePersonTextInfo("对比异常，请重试", getResources().getColor(R.color.red));
            showToastView("对比异常，请重试");
            LogUtil.e("=======compare: 对比异常，请重试");
            mIsCompareing = false;
            return;
        }
        if (compareResult.result != 0) {
            updatePersonTextInfo("对比错误，请重试: " + compareResult.result, getResources().getColor(R.color.red));
            showToastView("对比错误，请重试: " + compareResult.result);
            LogUtil.e("=======compare: 对比错误，请重试: " + compareResult.result);
            mIsCompareing = false;
            return;
        }
        //比对分数阀值
        if (compareResult.score >= 60) {
            DoorManager.getInstance().open("人证比对通过，开门");
            if (mVM.getMTempEnable()) {
                updatePersonTextInfo(result.name + ", 验证通过   " + mVM.getMTempShow(), getResources().getColor(R.color.green));
            } else {
                updatePersonTextInfo(result.name + ", 验证通过", getResources().getColor(R.color.green));
            }
            LogUtil.e("=======compare: 验证通过: " + compareResult.score);
            AcsService.getInstance().startSpeakTts(result.name + ",验证通过," + mVM.getMTempShow(), "");
            idcardNo = result.idCardNo;
            personName = result.name;
            mVM.setMIsAccessRecord(true);
            // mVM.setMAccessPerson();
        } else {
            mIsCompareing = false;
            updatePersonTextInfo(result.name + ", 验证失败", getResources().getColor(R.color.red));
            LogUtil.e("=======compare: 验证失败: " + compareResult.score);
            AcsService.getInstance().startSpeakTts(result.name + ",验证失败", "");
        }
    }

    //显示人证比对
    public void showCompare(Bitmap cameraBitmap, byte[] nv21Data, int width, int height, int rotation) {
        if (cameraBitmap == null) {
            return;
        }
        roatetranBitmapAccessFaceRecord(nv21Data, width, height, mSaveRotate);
        mLlCompare.setVisibility(View.VISIBLE);
        mIvCompareCamera.setImageBitmap(cameraBitmap);
        mIvCompareIdcard.setImageResource(R.mipmap.id_person_defult);
        String tempShow = mVM.getMTempShow();
        String showTitle = tempShow + "\n" + getString(R.string.please_use_idcard);
        String ttsTitle = getString(R.string.temp_normal) + tempShow + "," + getString(R.string.please_use_idcard);
        if (mVM.getMNoMask()) {
            showTitle += "\n" + getString(R.string.no_mask);
            ttsTitle += "," + getString(R.string.speak_no_mask);
        }

        int isTempOpen = SpUtils.getmTempType();
        if (isTempOpen == TempModelEntity.CLOSE) {
            showTitle = getString(R.string.please_use_idcard);
            ttsTitle = getString(R.string.please_use_idcard);
        }
        LightManager.getInstance().openGreenLight();
        updatePersonTextInfo(showTitle, R.color.grey);
        AcsService.getInstance().startSpeakTts(ttsTitle, "");
    }

    private void updatePersonTextInfo(String descShow, int color) {
        tv_compare_tips.setText(descShow);
        tv_compare_tips.setTextColor(color);
    }

    //隐藏人证比对
    public void hideCompare() {
        idcardNo = "";  //缓存的身份证号码
        personName = "";  //缓存的人员姓名
        mLlCompare.setVisibility(View.GONE);
        mIvCompareCamera.setImageResource(R.mipmap.id_person_defult);
        mIvCompareIdcard.setImageResource(R.mipmap.id_person_defult);
        cameraBitmap = null;
        mIsCompareing = false;
        resetTip();
    }
}
