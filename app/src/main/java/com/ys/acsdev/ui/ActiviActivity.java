package com.ys.acsdev.ui;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;
import com.ys.acsdev.R;
import com.ys.acsdev.commond.AppInfo;
import com.ys.acsdev.util.CodeUtil;
import com.ys.acsdev.util.NetWorkUtils;
import com.ys.acsdev.util.SpUtils;

import org.json.JSONObject;

import okhttp3.Call;

/***
 * 快速激活
 */
public class ActiviActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_activi);
        initView();
    }

    TextView tv_dev_statues, tv_dev_activi;
    Button btn_register, btn_activi;

    private void initView() {
        tv_dev_statues = (TextView) findViewById(R.id.tv_dev_statues);
        btn_register = (Button) findViewById(R.id.btn_register);
        tv_dev_activi = (TextView) findViewById(R.id.tv_dev_activi);
        btn_activi = (Button) findViewById(R.id.btn_activi);

        btn_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String userName = SpUtils.getUserName();
                if (userName == null || userName.length() < 3) {
                    tv_dev_statues.setText("UserName is Null ,Please Registe");
                    return;
                }
                registerEquipmentToServer("47.107.50.81", "8085", userName);
            }
        });
        btn_activi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activeKuangShiAuth();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        getDevInfoFromWeb();
    }

    private void activeKuangShiAuth() {
        if (!NetWorkUtils.isNetworkConnected(ActiviActivity.this)) {
            tv_dev_activi.setText("NetWork Error, Please Check ");
            return;
        }
        String appid = SpUtils.getAppid();
        if (appid == null || appid.length() < 3) {
            tv_dev_activi.setText("APPID == NULL");
            return;
        }
        String requestUrl = AppInfo.activeKuangShiAuth();
        String getImgVersion = CodeUtil.getImgVersion();
        final String mac = CodeUtil.getUniquePsuedoID();
        Log.e("CDL", "====获取授权信息= error==" + mac + " / " + getImgVersion + "\n " + requestUrl);
        OkHttpUtils
                .post()
                .url(requestUrl)
                .addParams("appId", appid)
                .addParams("mac", mac)
                .addParams("systemVersion", getImgVersion)
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, String e, int id) {
                        Log.e("CDL", "====激活设备失败= error==" + e);
                        tv_dev_activi.setText("激活设备失败: " + e);
                    }

                    @Override
                    public void onResponse(String json, int id) {
                        if (json == null || json.length() < 5) {
                            showToast("激活设备失败 : json==null");
                            return;
                        }
                        Log.e("CDL", "====激活设备成功===" + json);
                        try {
                            JSONObject jsonObject = new JSONObject(json);
                            int code = jsonObject.getInt("code");
                            String msg = jsonObject.getString("msg");
                            if (code != 0) {
                                tv_dev_activi.setText("激活设备失败: " + msg);
                                return;
                            }
                            tv_dev_activi.setText("激活设备成功");
                            getDevActivityStatues();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
    }

    /***
     * 获取设备授权状态
     */
    private void getDevActivityStatues() {
        if (!NetWorkUtils.isNetworkConnected(ActiviActivity.this)) {
            tv_dev_activi.setText("NetWork Error, Please Check ");
            return;
        }
        String appid = SpUtils.getAppid();
        if (appid == null || appid.length() < 3) {
            tv_dev_activi.setText("APPID == NULL");
            return;
        }
        String requestUrl = AppInfo.getKungShiAuth();
        String getImgVersion = CodeUtil.getImgVersion();
        final String mac = CodeUtil.getUniquePsuedoID();
        Log.e("CDL", "====获取授权信息= error==" + mac + " / " + getImgVersion + "\n " + requestUrl);
        OkHttpUtils
                .post()
                .url(requestUrl)
                .addParams("appId", appid)
                .addParams("mac", mac)
                .addParams("systemVersion", getImgVersion)
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, String e, int id) {
                        Log.e("CDL", "====获取授权信息= error==" + e);
                        tv_dev_activi.setText("获取授权失败: " + e);
                    }

                    @Override
                    public void onResponse(String json, int id) {
                        if (json == null || json.length() < 5) {
                            showToast("获取授权失败 : json==null");
                            return;
                        }
                        Log.e("CDL", "====获取授权信息===" + json);
                        try {
                            JSONObject jsonObject = new JSONObject(json);
                            int code = jsonObject.getInt("code");
                            String msg = jsonObject.getString("msg");
                            String avtivity_statues = "";
                            switch (code) {
                                case 0:
                                    avtivity_statues = "已经激活";
                                    break;
                                case 1:
                                    avtivity_statues = "注册失败或获取失败";
                                    break;
                                case 201:
                                    avtivity_statues = "201获取失败";
                                    break;
                                case 202:
                                    avtivity_statues = "202授权点不足";
                                    break;
                                case 204:
                                    avtivity_statues = "204未激活";
                                    break;
                                default:
                                    avtivity_statues = msg;
                                    break;
                            }
                            tv_dev_activi.setText("授权状态: " + avtivity_statues);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
    }

    /***
     * 注册设备道服务器
     * @param ipAddress
     * @param port
     * @param userName
     */
    public void registerEquipmentToServer(String ipAddress, String port, String userName) {
        if (!NetWorkUtils.isNetworkConnected(ActiviActivity.this)) {
            tv_dev_statues.setText("NetWork Error, Please Check ");
            return;
        }
        String requestUrl = AppInfo.getRegisterEquipment();
        String softVersion = "";
        String sn = CodeUtil.getUniquePsuedoID();
        String equipName = sn;
        String ip = "";
        String longitude = "0";
        String latitude = "0";
        String userAccount = userName;

        OkHttpUtils
                .post()
                .url(requestUrl)
                .addParams("equipName", equipName)
                .addParams("softVersion", softVersion + "")
                .addParams("sn", sn + "")
                .addParams("ip", ip)
                .addParams("longitude", longitude)
                .addParams("latitude", latitude)
                .addParams("userAccount", userAccount)
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, String e, int id) {
                        tv_dev_statues.setText("注册失败: " + e);
                    }

                    @Override
                    public void onResponse(String json, int id) {
                        if (json == null || json.length() < 5) {
                            showToast("Register Fialed : json==null");
                            return;
                        }
                        Log.e("CDL", "====response===" + json);
                        try {
                            JSONObject jsonObject = new JSONObject(json);
                            int code = jsonObject.getInt("code");
                            String msg = jsonObject.getString("msg");
                            if (code != 0) {
                                tv_dev_statues.setText("注册失败: " + msg);
                                return;
                            }
                            tv_dev_statues.setText("注册成功: " + msg);
                            getDevInfoFromWeb();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
    }

    /***
     * 获取设备信息
     */
    private void getDevInfoFromWeb() {
        if (!NetWorkUtils.isNetworkConnected(ActiviActivity.this)) {
            tv_dev_statues.setText("NetWork Error, Please Check ");
            return;
        }
        String code = CodeUtil.getUniquePsuedoID();
        if (code == null || code.length() < 2) {
            tv_dev_statues.setText("注册状态: MAC IS NULL");
            return;
        }
        String requestUrl = AppInfo.getSelectGateEquipBySn();
        requestUrl = requestUrl + "?sn=" + code;
        Log.e("CDL", "====response===" + requestUrl);
        OkHttpUtils
                .get()
                .url(requestUrl)
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, String e, int id) {
                        Log.e("CDL", "====获取设备信息ERROR===" + e);
                        tv_dev_statues.setText("获取设备信息ERROR: " + e);
                    }

                    @Override
                    public void onResponse(String response, int id) {
                        if (response == null || response.length() < 5) {
                            tv_dev_statues.setText("获取设备信息ERROR: response==null");
                            return;
                        }
                        Log.e("CDL", "====response===" + response);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            int code = jsonObject.getInt("code");
                            String msg = jsonObject.getString("msg");
                            if (code != 0) {
                                tv_dev_statues.setText("获取设备信息ERROR:  " + msg);
                                return;
                            }
                            String data = jsonObject.getString("data");
                            if (data == null || data.length() < 5) {
                                tv_dev_statues.setText("未注册:  Data == null");
                                return;
                            }
                            data = data.substring(data.indexOf("{"), data.lastIndexOf("}") + 1);
                            Log.e("CDL", "====response===" + data);
                            JSONObject jsonData = new JSONObject(data);
                            String appId = jsonData.getString("appId");
                            SpUtils.setAppid(appId);
                            if (appId == null || appId.length() < 2) {
                                tv_dev_statues.setText("注册状态: 已经注册 APPID==NULL");
                            } else {
                                tv_dev_statues.setText("注册状态: 已经注册");
                                getDevActivityStatues();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
    }


}
