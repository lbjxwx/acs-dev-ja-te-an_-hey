package com.ys.acsdev.ui

import android.os.Bundle
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.ys.acsdev.R
import com.ys.acsdev.adapter.AccessRecordNewAdapter
import com.ys.acsdev.bean.AccessRecordBean
import com.ys.acsdev.commond.AppInfo
import com.ys.acsdev.listener.AccessParsenerJsonListener
import com.ys.acsdev.manager.ImageManager
import com.ys.acsdev.service.AcsService
import com.ys.acsdev.setting.AccessOffRecordActivity
import com.ys.acsdev.setting.util.AccessParsenJsonRunnable
import com.ys.acsdev.util.CodeUtil
import com.ys.acsdev.util.LogUtil
import com.ys.acsdev.util.NetWorkUtils
import com.zhy.http.okhttp.OkHttpUtils
import com.zhy.http.okhttp.callback.StringCallback
import kotlinx.android.synthetic.main.activity_access_record.*
import okhttp3.Call
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.toast

class AccessRecordActivity : BaseActivity() {

    var mData = arrayListOf<AccessRecordBean>()
    val mAdapter by lazy { AccessRecordNewAdapter(this, mData) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_access_record)
        initView()
    }

    override fun onStart() {
        super.onStart()
        initData()
    }

    override fun onStop() {
        ImageManager.clearCache()
        super.onStop()
    }

    fun initView() {
        mIvBack.setOnClickListener { onBackPressed() }
        mRvRecord.layoutManager = LinearLayoutManager(this)
        mRvRecord.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))
        mRvRecord.adapter = mAdapter
        tv_offonline.setOnClickListener {
            startActivity<AccessOffRecordActivity>()
        }
    }

    fun initData() {
        if (!NetWorkUtils.isNetworkConnected(this)) {
            showToast(getString(R.string.no_network))
            return
        }
        if (!AppInfo.isConnectServer) {
            showToast(getString(R.string.dev_not_online))
            return
        }
        showWait(getString(R.string.getting_data))
        mData.clear()
        mAdapter.setDataList(mData);

        var requestUrl = AppInfo.getSelectAccessRecordBySn();
        OkHttpUtils
            .post()
            .url(requestUrl)
            .addParams("sn", CodeUtil.getUniquePsuedoID())
            .build()
            .execute(object : StringCallback() {
                override fun onResponse(response: String?, id: Int) {
                    LogUtil.attendance("=========获取的通行记录===" + response)
                    hideWait()
                    parsenerJsonInfo(response);
                }

                override fun onError(call: Call?, e: String?, id: Int) {
                    hideWait()
                    toast(e.toString())
                }
            });
    }

    private fun parsenerJsonInfo(response: String?) {
        var runnable: AccessParsenJsonRunnable =
            AccessParsenJsonRunnable(response, object : AccessParsenerJsonListener {
                override fun backJsonInfo(
                    isTrue: Boolean,
                    accessRecordBeanList: MutableList<AccessRecordBean>?,
                    errorDesc: String?
                ) {
                    if (!isTrue) {
                        toast(errorDesc + "")
                        return
                    }
                    if (accessRecordBeanList == null || accessRecordBeanList.size < 1) {
                        toast(getString(R.string.no_record))
                        return
                    }
                    mData.addAll(accessRecordBeanList)
                    mAdapter.setDataList(mData)
                }
            })
        AcsService.getInstance().executor(runnable)
    }

}