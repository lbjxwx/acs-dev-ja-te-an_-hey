//package com.ys.acsdev.ui.healthcodeBack.healthcode;
//
//import android.content.BroadcastReceiver;
//import android.content.Context;
//import android.content.Intent;
//import android.content.IntentFilter;
//import android.graphics.Bitmap;
//import android.graphics.BitmapFactory;
//import android.graphics.ImageFormat;
//import android.graphics.Matrix;
//import android.graphics.Point;
//import android.graphics.Rect;
//import android.graphics.YuvImage;
//import android.os.Bundle;
//import android.os.Handler;
//import android.os.Looper;
//import android.os.Message;
//import android.text.TextUtils;
//import android.util.Log;
//import android.view.KeyEvent;
//import android.view.View;
//import android.view.WindowManager;
//import android.view.animation.Animation;
//import android.view.animation.AnimationUtils;
//import android.widget.AbsoluteLayout;
//import android.widget.Button;
//import android.widget.FrameLayout;
//import android.widget.ImageView;
//import android.widget.LinearLayout;
//import android.widget.TextView;
//
//import androidx.annotation.Nullable;
//import androidx.lifecycle.Observer;
//import androidx.lifecycle.ViewModelProviders;
//
//import com.alibaba.fastjson.JSON;
//import com.alibaba.fastjson.JSONObject;
//import com.alibaba.fastjson.TypeReference;
//import com.card.CardHelper;
//import com.card.CardInfo;
//import com.card.Const;
//import com.card.OnParseCallback;
//import com.cn.sdk.demo.ConfigInfo;
//import com.cn.sdk.demo.MqttHelper;
//import com.cn.sdk.demo.OnMqttListener;
//import com.cn.sdk.demo.Resp;
//import com.device.PageTool;
//import com.etv.util.xutil.HttpUtils;
//import com.etv.util.xutil.exception.HttpException;
//import com.etv.util.xutil.http.ResponseInfo;
//import com.etv.util.xutil.http.callback.RequestCallBack;
//import com.hwangjr.rxbus.RxBus;
//import com.hwangjr.rxbus.annotation.Subscribe;
//import com.hwangjr.rxbus.thread.EventThread;
//import com.nlp.cloundcbsappdemo.listener.HealthyMessageListener;
//import com.nlp.cloundcbsappdemo.model.HealthyPersonEntity;
//import com.nlp.cloundcbsappdemo.model.MessageEvent;
//import com.nlp.cloundcbsappdemo.util.MessageDealUtil;
//import com.nlp.cloundcbsappdemo.viewmodel.MainViewModel;
//import com.yemjs.sdk.common.CommonUtil;
//import com.ys.acsdev.BuildConfig;
//import com.ys.acsdev.R;
//import com.ys.acsdev.bean.LinkServerEvent;
//import com.ys.acsdev.bean.MinFaceData;
//import com.ys.acsdev.bean.Upgrade;
//import com.ys.acsdev.commond.AppInfo;
//import com.ys.acsdev.commond.CommondViewModel;
//import com.ys.acsdev.db.DataRepository;
//import com.ys.acsdev.helper.SoundHelper;
//import com.ys.acsdev.light.LightManager;
//import com.ys.acsdev.listener.QrDismissListener;
//import com.ys.acsdev.manager.TempManager;
//import com.ys.acsdev.manager.TtsManager;
//import com.ys.acsdev.runnable.upload.RoateImageListener;
//import com.ys.acsdev.service.AcsService;
//import com.ys.acsdev.setting.ConfigSettingActivity;
//import com.ys.acsdev.setting.entity.TempModelEntity;
//import com.ys.acsdev.task.parsener.TaskPlayerParsener;
//import com.ys.acsdev.task.view.TaskPlayView;
//import com.ys.acsdev.ui.face.FaceViewModel;
//import com.ys.acsdev.ui.face.view.FacePassView;
//import com.ys.acsdev.ui.healthcode.SimpleHealthIdModel;
//import com.ys.acsdev.util.APKUtil;
//import com.ys.acsdev.util.ActivityCollector;
//import com.ys.acsdev.util.CameraProvider;
//import com.ys.acsdev.util.FileUtils;
//import com.ys.acsdev.util.LogUtil;
//import com.ys.acsdev.util.NetWorkUtils;
//import com.ys.acsdev.util.SpUtils;
//import com.ys.acsdev.util.SystemManagerUtil;
//import com.ys.acsdev.util.ToolUtils;
//import com.ys.acsdev.util.ViewSizeChange;
//import com.ys.acsdev.util.VoiceManager;
//import com.ys.acsdev.view.MyToastView;
//import com.ys.acsdev.view.WaitDialogUtil;
//import com.ys.acsdev.view.dialog.EditTextDialog;
//import com.ys.acsdev.view.dialog.OridinryDialog;
//import com.ys.acsdev.view.dialog.OridinryDialogClick;
//import com.ys.acsdev.view.dialog.QrBottomDialog;
//import com.ys.facepasslib.BaseLivenessActivity;
//import com.ys.facepasslib.FaceManager;
//import com.ys.facepasslib.camera.CameraPreview;
//import com.ys.facepasslib.listener.FaceRecDisListener;
//import com.ys.facepasslib.view.FaceCircleViewNew;
//import com.ys.rkapi.MyManager;
//import com.ys.rkapi.Utils.StorageUtils;
//
//import org.greenrobot.eventbus.EventBus;
//import org.greenrobot.eventbus.ThreadMode;
//import org.jetbrains.annotations.NotNull;
//
//import java.io.ByteArrayOutputStream;
//import java.io.File;
//import java.math.RoundingMode;
//import java.text.NumberFormat;
//import java.util.List;
//
//import cn.anicert.polymerizeDevice.DeviceManager;
//import cn.anicert.polymerizeDevice.event.CtidQRCodeScannedEvent;
//import io.reactivex.Observable;
//import io.reactivex.disposables.Disposable;
//import io.reactivex.schedulers.Schedulers;
//import kotlin.Unit;
//import kotlin.jvm.functions.Function0;
//import mcv.facepass.types.FacePassCompareResult;
//import mcv.facepass.types.FacePassImage;
//
///***
// * 更新内容
// * 暂停读取UseAXManger.wait(context);
// * 继续读取 UseAXManger.continueRed(context);
// * 关闭扫码头 UseAXManger.closeDev(context);
// *
// */
//public class SimpleHealthIdActivity_old extends BaseLivenessActivity implements FacePassView, TaskPlayView,
//        View.OnClickListener {
//
//    //陌生人警告间隔
//    private static final int MSG_RESET_IMAGE = 1;
//
//    private static final String TAG_RESET_TIP = "TAG_RESET_TIP";
//    private static final String TAG_RESET_FACE = "TAG_RESET_FACE";
//    private static long mKeyActionTime = 0L;
//    private int mSaveRotate = 0;
//    private boolean mQrcodeDevIsInit; //扫码头是否初始化
//
//    SimpleHealthIdModel mVM;
//    SimpleHealthIdParsener facePassParsener = null;
//    TaskPlayerParsener taskPlayerParsener = null;
//
//    Animation mScalAnim;
//    QrBottomDialog mQrDialog;
//    private boolean mScalAniming = false;
//    String qrCodeType = "0";
//    private String verfyAddr; // 定位的地址
//    private String mLocation = " ";
//
//    private CardInfo mCardInfo;
//    private String idcardName = "";
//    private String idcardNo = "";
//    private CardHelper mCardHelper;
//    private MqttHelper mqttHelper;
//    private ConfigInfo mConfigInfo;
//    private Button buttonTest;
//
//    private int failCount;
//    private Runnable closeResultRunnable;
//    private NumberFormat numberFormat = NumberFormat.getInstance();
//
//    enum VERIFY_TYPE            //核验类型
//    {
//        VERIFY_TYPE_NONE,       //未知
//        VERIFY_TYPE_CODE,       //健康码核验
//        VERIFY_TYPE_ID_CARD,    //身份证核验
//        VERIFY_TYPE_RC_CARD     //蓉城通核验
//    }
//
//    //过闸方式
//    private String transMethod = "";
//    VERIFY_TYPE verify_type = VERIFY_TYPE.VERIFY_TYPE_NONE;
//
//    //花花来了
//    private void initOther() {
//        if (mVM == null) {
//            mVM = ViewModelProviders.of(this).get(SimpleHealthIdModel.class);
//        }
//        if (mScalAnim == null) {
//            mScalAnim = AnimationUtils.loadAnimation(this, R.anim.tip_scal);
//            mScalAnim.setAnimationListener(new Animation.AnimationListener() {
//                @Override
//                public void onAnimationStart(Animation animation) {
//                    mScalAniming = true;
//                }
//
//                @Override
//                public void onAnimationEnd(Animation animation) {
//                    mScalAniming = false;
//                }
//
//                @Override
//                public void onAnimationRepeat(Animation animation) {
//
//                }
//            });
//        }
//        if (mQrDialog == null) {
//            mQrDialog = new QrBottomDialog(SimpleHealthIdActivity_old.this);
//        }
//        mQrDialog.setOnDismissListener(new QrDismissListener() {
//
//            @Override
//            public void onDismiss() {
//                resetTip();
//            }
//        });
//    }
//
//    private static final int CLOSE_WHITE_LIGHT_MESSAGE = 56845;
//    private Handler mHandler = new Handler(Looper.getMainLooper()) {
//        @Override
//        public void handleMessage(Message msg) {
//            super.handleMessage(msg);
//            switch (msg.what) {
//                case CLOSE_WHITE_LIGHT_MESSAGE:
//                    removeMessages(CLOSE_WHITE_LIGHT_MESSAGE);
//                    LightManager.getInstance().closeWhiteLight();
//                    break;
//                case MSG_RESET_IMAGE:
//                    removeMessages(MSG_RESET_IMAGE);
////                    mIvPerson.setImageResource(R.mipmap.house_icon)
//                    break;
//            }
//        }
//    };
//
//    private BroadcastReceiver receiver = new BroadcastReceiver() {
//        @Override
//        public void onReceive(Context context, Intent intent) {
//            String action = intent.getAction();
//            LogUtil.task("==============监听到广播====" + action);
//            if (action.equals(Intent.ACTION_TIME_TICK)) {
//                updateTopViewTime();
//                if (taskPlayerParsener != null) {
//                    taskPlayerParsener.updateSleepTvOnTime();
//                }
//            } else if (action.equals(AppInfo.DOWN_TASK_SUCCESS)) {
//                if (mFlSleep.getVisibility() == View.VISIBLE) {
//                    taskPlayerParsener.getTaskFromDb();
//                }
//            }
//        }
//    };
//
//    @Override
//    protected void onCreate(@Nullable Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
//                WindowManager.LayoutParams.FLAG_FULLSCREEN);
//        setContentView(R.layout.activity_simple_healthcode3);
//        LogUtil.task("==============onCreate===00001212=");
//        EventBus.getDefault().register(this);
//        initOther();
//        initView();
//        initReceiver();
//        initFaceConfig();
//        initMqtt();
//        ActivityCollector.addActivity(this);
//        initHealthCode();
//        mCardHelper = new CardHelper();
//        initLocation();
//
//        closeResultRunnable = () -> {
//            llCodeResult.setVisibility(View.GONE);
//        };
//        SpUtils.setMqttSn(SpUtils.getMqttSn());
//
//        mHandler.postDelayed(()-> {
//            String msg = AcsService.getCardInitMsg();
//            if (msg != null) {
//                OridinryDialog dialog = new OridinryDialog(this);
//                dialog.show(String.format("卡设备初始化失败: %s", msg), getString(R.string.confirm), getString(R.string.cancel));
//            }
//        }, 1000);
//    }
//
//    private void initLocation() {
////        TencentPosition position = new TencentPosition(getApplication(), location -> {
////            if (location.getAddress() != null) {
////                verfyAddr = location.getAddress();
////                mLocation = String.format("%s,%s", location.getLongitude(), location.getLatitude());
////            }
////        });
////        position.getPosition();
//        //SpUtils.setDeviceEnable(true);
//    }
//
//    private void initMqtt() {
//        mqttHelper = new MqttHelper();
//        mqttHelper.setOnMqttListener(new OnMqttListener() {
//            @Override
//            public void onConnect(boolean isConnect) {
//                System.out.println("mqtt  onconnect  " + isConnect);
//            }
//
//            @Override
//            public void onDevConfig(int ret, String retMsg) {
//                try {
//                    Resp<ConfigInfo> resp = JSON.parseObject(retMsg, new TypeReference<Resp<ConfigInfo>>() {}.getType());
//                    if (ret == 0) {
//                        mConfigInfo = resp.data;
//                        SpUtils.setVerifyAddr(mConfigInfo.verifyAddress);
//                        SpUtils.setDevConfigInfo(JSON.toJSONString(mConfigInfo));
//                        SpUtils.setTempHeightWearNew(mConfigInfo.highTemperature);
//                        SpUtils.setmTempLow(mConfigInfo.lowTemperature);
//                        SpUtils.setmTempType(mConfigInfo.isTemperatureCheck == 1 ? TempModelEntity.IIC_MLX_90621_BAB : TempModelEntity.CLOSE);
//                        mVM.setMTempEnable(mConfigInfo.isTemperatureCheck == 1);
//                        SpUtils.setMaskEnable((mConfigInfo.faceMaskType == 0 || mConfigInfo.faceMaskType == 1));
//                        SpUtils.setMaskIntercept(mConfigInfo.faceMaskType == 0);
//                        if (mConfigInfo.ctidAuthUrl != null && mConfigInfo.ctidAuthUrl.length() > 3) {
//                            SpUtils.setRongCardUrl(mConfigInfo.ctidAuthUrl);
//                        }
//                        if (mConfigInfo.gbUrl != null && mConfigInfo.gbUrl.length() > 3) {
//                            SpUtils.setGKUrl(mConfigInfo.gbUrl);
//                        }
//                        if (mConfigInfo.mztUrl != null && mConfigInfo.mztUrl.length() > 3) {
//                            SpUtils.setMZTUrl(mConfigInfo.mztUrl);
//                        }
//
//                        SpUtils.setFaceAndCardEnable(mConfigInfo.isCtidCheck == 1);
//                        SpUtils.setRongCardType(mConfigInfo.getRctCardType());
//                        SpUtils.setDataReplyEnable(mConfigInfo.reportType > 0);
//                        SpUtils.setWhiteLightEnable(mConfigInfo.isLignting == 1);
//                        if (mConfigInfo.isLignting == 1) {
//                            LightManager.getInstance().openWhiteLight();
//                        } else {
//                            LightManager.getInstance().closeWhiteLight();
//                        }
//                        setMMaskEnable(SpUtils.getmMaskEnable());
//                        if (mConfigInfo.healthCodeWay == 0) {
//                            SpUtils.setHealthPlatform(1);
//                        } else {
//                            SpUtils.setHealthPlatform(0);
//                        }
//                        runOnUiThread(() -> updateTopViewTime());
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//
//            @Override
//            public void onReceiveMsg(String topic, String msg) {
//                LogUtil.cdl("onReceiveMsg  " + topic + "  " + msg);
//                if (topic.contains("DEVICE_INFO")) {
//                    ConfigInfo devConfig = SpUtils.getDevConfigInfo();
//                    mqttHelper.sendDeviceInfo(SpUtils.getDeviceEnable(), StorageUtils.getRealSizeOfNand(), BuildConfig.VERSION_CODE, getString(R.string.app_name), devConfig);
//                } else if (topic.contains("DEVICE_CONFIGURATION")) {
//                    //配置信息下发指令下发
//                    String version = "V1.0";
//                    try {
//                        version = JSON.parseObject(msg).getString("version");
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                    mqttHelper.getDeviceConfig(version);
//                } else if (topic.contains("ENABLE_APPLICATION")) {
//                    //设备启用指令下发
//                    SpUtils.setDeviceEnable(true);
//                } else if (topic.contains("DISABLE_APPLICATION")) {
//                    //设备禁用指令下发
//                    SpUtils.setDeviceEnable(false);
//                } else if (topic.contains("RESTART_APPLICATION")) {
//                    LogUtil.cdl("重启指令： 打开上拉，下拉菜单栏");
//                    //重启应用
//                    SystemManagerUtil.rebootApp(getApplication());
//                } else if (topic.contains("RESTART_SYSTEM")) {
//                    MyManager manager = MyManager.getInstance(getApplication());
//                    manager.setSlideShowNotificationBar(true); //打开下拉菜单
//                    manager.setSlideShowNavBar(true);  //打开滑出底部菜单
//                    //重启系统
//                    SystemManagerUtil.rebootDev(getApplication());
//                } else if (topic.contains("UPGRADE_SYSTEM")) {
//                    //应用升级
//                    try {
//                        Upgrade upgrade = JSON.parseObject(msg, Upgrade.class);
//                        if (upgrade != null && upgrade.updateApkMD5 != null && upgrade.updateURL != null) {
//                            System.out.println("update apk----  " + upgrade.updateVersion + " | " + upgrade.updateURL);
//                            String updateApkMD5 = upgrade.updateApkMD5;
//                            int vcode = Integer.parseInt(upgrade.updateVersion);
//                            if (vcode >= BuildConfig.VERSION_CODE) {
//                                String savePath = getExternalCacheDir() + "/update.apk";
//                                new HttpUtils().download(upgrade.updateURL, savePath, new RequestCallBack<File>() {
//
//                                    @Override
//                                    public void onLoading(long total, long current, boolean isUploading) {
//                                        Log.e("update apk", "---->downloading  " + current + "/" + total);
//                                    }
//
//                                    @Override
//                                    public void onSuccess(ResponseInfo<File> responseInfo) {
//                                        Disposable d = Observable.just(responseInfo.result)
//                                                .subscribeOn(Schedulers.io())
//                                                .subscribe(file -> {
//                                                    String fileMd5 = FileUtils.getFileMD5(file);
//                                                    System.out.println("update apk----  md51: " + updateApkMD5 + "  md52: " + fileMd5);
//                                                    if (updateApkMD5.equalsIgnoreCase(fileMd5)) {
//                                                        APKUtil.installApk(file.getAbsolutePath());
//                                                    }
//                                                }, Throwable::printStackTrace);
//                                    }
//
//                                    @Override
//                                    public void onFailure(HttpException error, String msg) {
//                                        System.out.println("update apk----  error: " + msg + " / " + error.getMessage());
//                                    }
//                                });
//                            }
//                        }
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                }
//            }
//        });
//        mqttHelper.init(getApplication(), SpUtils.getMqttSn());
//    }
//
//    CameraPreview mCv;
//    View view_click;
//    FaceCircleViewNew mFrv;
//    TextView mTvInfo;
//    TextView mTvVersion, mTvTime;
//    FrameLayout mFlSleep;
//    AbsoluteLayout mAdView;
//    CameraPreview mIRCv;
//    ImageView mDevQrCode;
//    FrameLayout mFlNoInit;
//    View view_setting;
//    WaitDialogUtil waitDialogUtil;
//
//    TextView tvCodeStatus, tvTempShow, tvHealthy, tvPersonName;
//    LinearLayout llCodeResult;
//    TextView tv_net, tv_temp, tv_person_card, tvCodeType, tv_accord;
//
//    ImageView voice_reduce, voice_add, ivCodeStatus;
//
//    private void initView() {
//        buttonTest = findViewById(R.id.bt_test);
//        buttonTest.setOnClickListener(v -> {
//            /*if (buttonTest.getText().equals("禁用设备")) {
//                buttonTest.setText("启用设备");
//                mqttHelper.onMqttListener.onReceiveMsg("DISABLE_APPLICATION/FZ000XDL22020003;qos:0;retained:", "dsfasdfasdfasdf");
//            } else {
//                buttonTest.setText("禁用设备");
//                mqttHelper.onMqttListener.onReceiveMsg("ENABLE_APPLICATION/dsfsdfasfas", "dfdsafasdfs");
//            }*/
//            /*Upgrade upgrade = new Upgrade();
//            upgrade.updateApkMD5= "fd311d25edadf3ed29c5d13cf6ccb68b";
//            upgrade.updateURL = "http://etvh5.esoncloud.com/ysFace_V3.3.7.5_81.apk";
//            upgrade.updateVersion = "81";
//            mqttHelper.onMqttListener.onReceiveMsg("UPGRADE_SYSTEM", JSON.toJSONString(upgrade));*/
//        });
//
//        voice_reduce = (ImageView) findViewById(R.id.voice_reduce);
//        voice_reduce.setOnClickListener(this);
//        voice_add = (ImageView) findViewById(R.id.voice_add);
//        voice_reduce.setOnClickListener(this);
//
//        tv_net = (TextView) findViewById(R.id.tv_net);
//        tv_temp = (TextView) findViewById(R.id.tv_temp);
//        tv_person_card = (TextView) findViewById(R.id.tv_person_card);
//        tvCodeType = (TextView) findViewById(R.id.tv_code_type);
//        tv_accord = (TextView) findViewById(R.id.tv_accord);
//
//
//        llCodeResult = (LinearLayout) findViewById(R.id.fl_code_result);
//        //tv_temp_healthy = (TextView) findViewById(R.id.tv_temp_healthy);
//        tvPersonName = findViewById(R.id.tv_person_name);
//        ivCodeStatus = findViewById(R.id.iv_code_status);
//        tvCodeStatus = findViewById(R.id.tv_code_status);
//        tvTempShow = findViewById(R.id.tv_temp_show);
//        tvHealthy = findViewById(R.id.tv_healthy);
//        //tv_result_healthy = (TextView) findViewById(R.id.tv_result_healthy);
//
//        view_setting = (View) findViewById(R.id.view_setting);
//        view_setting.setOnLongClickListener(new View.OnLongClickListener() {
//            @Override
//            public boolean onLongClick(View view) {
//                goSettings();
//                return true;
//            }
//        });
//        mFlNoInit = (FrameLayout) findViewById(R.id.mFlNoInit);
//        mDevQrCode = findViewById(R.id.dev_qrcode);
//        mIRCv = (CameraPreview) findViewById(R.id.mIRCv);
//
//        mAdView = (AbsoluteLayout) findViewById(R.id.mAdView);
//        mFlSleep = (FrameLayout) findViewById(R.id.mFlSleep);
//        mTvVersion = (TextView) findViewById(R.id.mTvVersion);
//        mTvTime = (TextView) findViewById(R.id.mTvTime);
//        mTvInfo = (TextView) findViewById(R.id.mTvInfo);
//        mCv = (CameraPreview) findViewById(R.id.mCv);
//        ViewSizeChange.setCameraPassFaceSize(mCv);
//        view_click = (View) findViewById(R.id.view_click);
//        view_click.setOnClickListener(this);
//        mFrv = (FaceCircleViewNew) findViewById(R.id.mFrv);
//        waitDialogUtil = new WaitDialogUtil(SimpleHealthIdActivity_old.this);
//
//        facePassParsener = new SimpleHealthIdParsener(SimpleHealthIdActivity_old.this, this, mVM);
//        facePassParsener.updateBindQrCode();
//        initObserve();
//        taskPlayerParsener = new TaskPlayerParsener(SimpleHealthIdActivity_old.this, this);
//
//    }
//
//    //初始化监听
//    private void initObserve() {
//        CommondViewModel.getInstance().getMFaceInitState().observe(this, new Observer<Boolean>() {
//            @Override
//            public void onChanged(Boolean it) {
//                if (it) {
//                    mFlNoInit.setVisibility(View.GONE);
//                } else {
//                    mFlNoInit.setVisibility(View.VISIBLE);
//                }
//            }
//        });
//
//        CommondViewModel.getInstance().getMNetState().observe(SimpleHealthIdActivity_old.this, new Observer<Boolean>() {
//            @Override
//            public void onChanged(Boolean aBoolean) {
//                updateTopViewTime();
//            }
//        });
//        mVM.getMSleepState().observe(SimpleHealthIdActivity_old.this, new Observer<Boolean>() {
//            @Override
//            public void onChanged(Boolean it) {
//                LogUtil.e("=======mSleepState change: " + it);
//                mFlSleep.setVisibility(it ? View.VISIBLE : View.GONE);
//                if (it) {
//                    if (mQrDialog != null) {
//                        mQrDialog.dismissQrDialog();
//                    }
//                    taskPlayerParsener.getTaskFromDb();
//                } else {
//                    taskPlayerParsener.clearTaskView();
//                }
//            }
//        });
//
//        mVM.getMTipsInfo().observe(SimpleHealthIdActivity_old.this, it -> {
//            LogUtil.e("=======changeTip: " + it.toString());
//            //温度显示这里标识
//            mTvInfo.setText(it.getText());
//            //暂时注释
////            mTvInfo.setBackgroundResource(it.getBgColor());
//            mTvInfo.setTextColor(getResources().getColor(R.color.white));
//            if (!mScalAniming) {
//                mTvInfo.startAnimation(mScalAnim);
//            }
//        });
//
//        mVM.getMLazyRestFace().observe(SimpleHealthIdActivity_old.this, it -> lazyResetFace(it));
//
//        mVM.getMLazyRestTip().observe(SimpleHealthIdActivity_old.this, aLong -> lazyResetTip(aLong));
//
//        mVM.getMQrCodeState().observe(SimpleHealthIdActivity_old.this, aLong -> mQrDialog.showByText());
//
//        mVM.getMCheckHealthCode().observe(SimpleHealthIdActivity_old.this, aLong -> {
//            showHealthyTipsCode(aLong);
//        });
//    }
//
//    @Override
//    public void onClick(View view) {
//        switch (view.getId()) {
//            case R.id.voice_reduce:
//                VoiceManager.redusMoreVoiceNum(SimpleHealthIdActivity_old.this);
//                break;
//            case R.id.voice_add:
//                VoiceManager.addMoreVoiceNum(SimpleHealthIdActivity_old.this);
//                break;
//            case R.id.view_click:
//                facePassParsener.hiddleSleepView();
//                if (!getMHasPerson())
//                    facePassParsener.showSleepView();
//                break;
//        }
//    }
//
//    @Override
//    protected void onStart() {
//        super.onStart();
//        mVM.setMProcessing(false);
//        facePassParsener.mSleepTime = SpUtils.getSleepTime() * 1000;
//        mVM.setMTempTime(SpUtils.getmTempTime());
//        mVM.setMTempEnable(SpUtils.getmTempType() != TempModelEntity.CLOSE);
//        if (SpUtils.getQrCodeEnable() || SpUtils.getShowAskErCode()) {
//            mVM.setMQrCodeEnable(true);
//        } else {
//            mVM.setMQrCodeEnable(false);
//        }
//        mVM.setMAttendEnable(SpUtils.getAttendEnable());
//        setMOnlyCheck(true);
//        setMFaceRectMir(SpUtils.getmFaceRect());
//        mKeyActionTime = System.currentTimeMillis();
//        setMMaskEnable(SpUtils.getmMaskEnable());
//        RxBus.get().register(this);
//        resetTip();
//        resetFace();
//    }
//
//    private void initHasPerson() {
//        if (getMHasPerson()) {
//            facePassParsener.hiddleSleepView();
//            return;
//        }
//        hideCheckCode();
//        qrCodeType = "0";
//        facePassParsener.showSleepView();
//    }
//
//    @Override
//    protected void onResume() {
//        super.onResume();
//        initHasPerson();
//        updateTopViewTime();
//        if (facePassParsener != null) {
//            facePassParsener.updateDevInfoToWeb();
//        }
//        updateSaveRotate();
//    }
//
//    private void updateSaveRotate() {
//        if (SpUtils.getSaveRotate() == 0) {
//            mSaveRotate = getMRectRotation();
//        } else {
//            switch (SpUtils.getSaveRotate()) {
//                case 1:
//                    mSaveRotate = 0;
//                    break;
//                case 2:
//                    mSaveRotate = 90;
//                    break;
//                case 3:
//                    mSaveRotate = 180;
//                    break;
//                default:
//                    mSaveRotate = 270;
//                    break;
//            }
//        }
//        mVM.setMSaveRotate(mSaveRotate);
//    }
//
//    @Override
//    protected void onStop() {
//        super.onStop();
//        showWaitDialog(false);
//        facePassParsener.onStopParsener();
//        taskPlayerParsener.clearTaskView();
//        setMHasPerson(false);
//        if (mHandler != null) {
//            mHandler.removeMessages(MSG_RESET_IMAGE);
//        }
//        TtsManager.getInstance().setSpeechListener(null);
//        try {
//            stopLazy(TAG_RESET_TIP);
//            RxBus.get().unregister(this);
//            TempManager.getInstance().setTempCallBack(null);
//            LightManager.getInstance().closeAllLight();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//    @Override
//    protected void onDestroy() {
//        super.onDestroy();
//        if (viewModel != null) {
//            viewModel.closePort();
//        }
//        if (EventBus.getDefault().isRegistered(this)) {
//            EventBus.getDefault().unregister(this);
//        }
//        try {
//            if (receiver != null) {
//                unregisterReceiver(receiver);
//            }
//            if (mVM != null) {
//                mVM.destory();
//            }
//            ActivityCollector.removeActivity(this);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//    MainViewModel viewModel;
//    String port = "/dev/ttyACM0";
//
//    private void initHealthCode() {
//        viewModel = new MainViewModel(SimpleHealthIdActivity_old.this);
//        viewModel.setMessageListener(messageListener);
//        viewModel.initHealthyCodeManager(port);
//    }
//
//    private void showInitQrDevDialog() {
//        OridinryDialog dialog = new OridinryDialog(SimpleHealthIdActivity_old.this);
//        dialog.setOnDialogClickListener(new OridinryDialogClick() {
//            @Override
//            public void sure() {
//                viewModel.initHealthyCodeManager(port);
//            }
//
//            @Override
//            public void noSure() {
//
//            }
//        });
//        dialog.show("扫码设备初始化失败,是否重新初始化？", getString(R.string.confirm), getString(R.string.cancel));
//    }
//
//    private HealthyMessageListener messageListener = new HealthyMessageListener() {
//        @Override
//        public void initHealthyStatues(boolean isSuccess, int code) {
//            mQrcodeDevIsInit = isSuccess;
//            prependMsg("初始化状态: " + isSuccess + " / " + code + "/ ");
//            if (!isSuccess) {
//                showInitQrDevDialog();
//            }
//        }
//
//        @Override
//        public void backMessageToView(MessageEvent messageEvent) {
//            Log.e("backMessageToView", messageEvent.getMessage() + "  " + messageEvent.getMsgClass() + " / " + Thread.currentThread());
//            switch (messageEvent.getMsgClass()) {
//                case 2:
//                    //服务器有响应，但是数据获取健康信息失败
//                    showWaitDialog(false);
//                    mVM.changeTipsInfo(messageEvent.getMessage(), R.drawable.bg_tip_red, "", R.color.red);
//                    if (messageEvent.getMessage().contains("无效") || messageEvent.getMessage().contains("过期")) {
//                        AcsService.getInstance().startSpeakTts("健康码无效或已过期，请重新出示", "");
//                    }
//                    break;
//                case 5:
//                    //服务器返回错误，获取健康信息失败
//                    failCount++;
//                    showWaitDialog(false);
//                    mVM.changeTipsInfo(messageEvent.getMessage(), R.drawable.bg_tip_red, "", R.color.red);
//                    //showToastView(messageEvent.getMessage());
//                    if (failCount < 2) {
//                        retryRequestHealth();
//                    } else {
//                        failCount = 0;
//                        SpUtils.setHealthPlatform(SpUtils.getHealthPlatform() == 0 ? 1 : 0);
//                        updateTopViewTime();
//                    }
//                    break;
//                case 0: //执行状态
//                    Log.e("执行状态", messageEvent.getMessage());
//                    prependMsg(messageEvent.getMessage());
//                    break;
//                case 1: //展示健康结果
//                    failCount = 0;
//                    showWaitDialog(false);
//                    String msg = messageEvent.getMessage();
////                    prependMsg("核验结果(简版)：" + MessageDealUtil.VerificationResult(msg) + " / " + Thread.currentThread());
//                    //prependMsg("核验结果（较全版）：" + MessageDealUtil.VerificationResultAll(msg).toString());
//                    HealthyPersonEntity healthyPersonEntity = MessageDealUtil.VerificationResultAll(msg);
//                    boolean highTemp = tempUpdate >= SpUtils.getTempHeightWearNew();
//                    tvTempShow.setVisibility(tempUpdate > 0 ? View.VISIBLE : View.GONE);
//                    if (tempUpdate > 0) {
//                        tvTempShow.setText(String.format("体温%s℃ %s", tempUpdate, highTemp ? "异常" : "正常"));
//                    } else {
//                        tvTempShow.setText("未检测到体温或测温已关闭");
//                    }
//                    String hsjcInfo = healthyPersonEntity.getHSJC();
//                    if (TextUtils.isEmpty(hsjcInfo)) {
//                        //"未查询到核酸信息");
//
//                    } else {
//
//                    }
//                    if (TextUtils.isEmpty(healthyPersonEntity.getName())) {
//                        tvPersonName.setVisibility(View.GONE);
//                    } else {
//                        tvPersonName.setText(healthyPersonEntity.getName());
//                        tvPersonName.setVisibility(View.VISIBLE);
//                    }
//                    //tv_name_healthy.setText("姓名: " + healthyPersonEntity.getName());
//                    //tv_idcard_healthy.setText("身份证: " + healthyPersonEntity.getIDNum());
//                    //tv_date_healthy.setText("时间: " + SimpleDateUtil.formatCurrentTimeMin());
//                    switch (healthyPersonEntity.getStatus()) {
//                        case "00":
//                            qrCodeType = "1";
//                            tvCodeStatus.setText("通过");
//                            tvCodeStatus.setTextColor(0xFF0ED145);
//                            ivCodeStatus.setImageResource(R.mipmap.check_yes2);
//                            tvHealthy.setText("健康码正常");
//                            AcsService.getInstance().startSpeakTts("请通过", "");
//                            break;
//                        case "01":
//                            qrCodeType = "2";
//                            tvCodeStatus.setText("禁止通过");
//                            tvCodeStatus.setTextColor(0xFFFFF200);
//                            ivCodeStatus.setImageResource(R.mipmap.check_yes_yellow);
//                            tvHealthy.setText("健康码黄码");
//                            AcsService.getInstance().startSpeakTts("禁止通过", "");
//                            break;
//                        case "10":
//                            qrCodeType = "3";
//                            tvCodeStatus.setText("禁止通过");
//                            tvCodeStatus.setTextColor(0xFFEC1C24);
//                            ivCodeStatus.setImageResource(R.mipmap.check_yes_red);
//                            tvHealthy.setText("健康码红码");
//                            AcsService.getInstance().startSpeakTts("禁止通过", "");
//                            break;
//                        case "100":
//                            //闽政通 非绿码
//                            qrCodeType = "5";
//                            tvCodeStatus.setText("禁止通过");
//                            tvCodeStatus.setTextColor(0xFFFFF200);
//                            ivCodeStatus.setImageResource(R.mipmap.check_yes_yellow);
//                            tvHealthy.setText("非绿码");
//                            AcsService.getInstance().startSpeakTts("禁止通过", "");
//                            break;
//                    }
//                    if ("1".equals(qrCodeType)) {
//                        if (SpUtils.getmMaskEnable() && AppInfo.MAST_WAER_DEFAULT.equals(wearMaskCurrent)) {
//                            mTvInfo.setText("通过，请佩戴口罩");
//                            mTvInfo.setTextColor(getResources().getColor(R.color.red));
//                            AcsService.getInstance().startSpeakTts("通过，请佩戴口罩", "");
//                        }
//                    }
//                    mHandler.removeCallbacks(closeResultRunnable);
//                    llCodeResult.setVisibility(View.VISIBLE);
//                    mHandler.postDelayed(closeResultRunnable, 2000);
//
//                    LogUtil.update("准备上传素材信息==000+" + imagePathUpdate + " / " + tempUpdate + " / " + wearMaskCurrent + " / " + qrCodeType);
//                    String unique = viewModel.getIdentifyData();
//                    if (verify_type == VERIFY_TYPE.VERIFY_TYPE_ID_CARD) {
//                        transMethod = MqttHelper.TMETHOD_IDCARD;
//                    }
//                    if (verify_type == VERIFY_TYPE.VERIFY_TYPE_RC_CARD) {
//                        transMethod = MqttHelper.TMETHOD_ICCARD;
//                    }
//
//                    if (SpUtils.getDataReplyEnable()) {
//                        mVM.saveAccessFaceRecord(
//                                SimpleHealthIdActivity_old.this,
//                                imagePathUpdate,
//                                null,
//                                tempUpdate,
//                                wearMaskCurrent,
//                                qrCodeType);
//
//                        String cardType = "0000";
//                        if (mCardInfo != null && transMethod.equals(MqttHelper.TMETHOD_ICCARD)) {
//                            cardType = mCardInfo.rcType;
//                        }
//                        numberFormat.setMaximumFractionDigits(2);
//                        numberFormat.setRoundingMode(RoundingMode.DOWN);
//                        String tempC = numberFormat.format(mVM.getTempPrimaryFloat());
//                        String address = TextUtils.isEmpty(verfyAddr) ? SpUtils.getVerifyAddr() : verfyAddr;
//                        mqttHelper.savePassRecord("1".equals(qrCodeType), unique, tempC, highTemp, false, transMethod, cardType, address, mLocation);
//                    }
//                    break;
//                case 7:
//                    //请求bsn结束，获取idcard健康报告
//                    viewModel.requestHealthByIdAndName(idcardName, idcardNo);
//                    break;
//            }
//        }
//    };
//
//    //扫码后回调入口
//    @org.greenrobot.eventbus.Subscribe(threadMode = ThreadMode.MAIN)
//    public void onQrCodeScannedEvent(CtidQRCodeScannedEvent event) {
//        if (!SpUtils.getDeviceEnable()) {
//            mVM.changeTipsInfo(getString(R.string.device_disable), R.drawable.bg_tip_red, "", R.color.red);
//            return;
//        }
//        String qrCodeValue = DeviceManager.getInstance().getCtidQRCode().getRawValue();
//        if (TextUtils.isEmpty(qrCodeValue)) {
//            return;
//        }
//        Log.e("qrcode value", qrCodeValue);
//        if (qrCodeValue.startsWith("ZDSQM-")){
//            //闵政通授权码
//            try {
//                String authCode = CommonUtil.getAuthCode(qrCodeValue.substring(6));
//                SpUtils.setMztInvokeCode(authCode);
//                showToastView("授权码配置成功");
//                return;
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
//
//        if (qrCodeValue.startsWith("ZDSNM-")){
//            //mqtt需要的 Sn码
//            try {
//                String sn = CommonUtil.getAuthCode(qrCodeValue.substring(6));
//                SpUtils.setMqttSn(sn);
//                mqttHelper.setMqttSn(sn);
//                showToastView("sn配置成功, 准备重启应用...");
//                mHandler.postDelayed(()-> SystemManagerUtil.rebootApp(getApplication()), 600);
//                return;
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
//
//        if (qrCodeValue.equals("TURN_TO_MZT")) {
//            SpUtils.setHealthPlatform(1);
//            updateTopViewTime();
//            return;
//        }
//
//        if (qrCodeValue.equals("TURN_TO_NL")) {
//            SpUtils.setHealthPlatform(0);
//            updateTopViewTime();
//            return;
//        }
//
//        if (!isShowHealthyDialog && SpUtils.getmTempType() != TempModelEntity.CLOSE) {
//            showToastView("请测温，再扫码");
//            AcsService.getInstance().startSpeakTts("请测温，再扫码", "");
//            return;
//        }
//        if (!NetWorkUtils.isNetworkConnected(SimpleHealthIdActivity_old.this)) {
//            mVM.changeTipsInfo(PageTool.NETWORK_ERROR_MSG, R.drawable.bg_tip_red, "", R.color.red);
//            return;
//        }
//
//        if (qrCodeValue.length() < 200) {
//            showToastView("请出示健康码二维码！");
//            return;
//        }
//        verify_type = VERIFY_TYPE.VERIFY_TYPE_CODE;
//
//        showWaitDialog(true);
//        int type = DeviceManager.getInstance().getQrType(qrCodeValue);
//        if (SpUtils.getHealthPlatform() == 0) {
//            //国康码平台
//            if (type == 1) {
//                transMethod = MqttHelper.TMETHOD_GM;
//                JSONObject checkData = null;
//                try {
//                    checkData = DeviceManager.getInstance().getCheckData();
//                    qrCodeValue = checkData.toString();
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            } else { //其他码
//                transMethod = MqttHelper.TMETHOD_CTID;
//                //Log.i(TAG, "其他码");
//                //viewModel.getHealthReportByCtidQrCode();
//            }
//
//            viewModel.setQrCodeValue(qrCodeValue);
//            /*try {
//                JSONObject jsonObjectCheckData = JSONObject.parseObject(qrCodeValue);
//                viewModel.requestHealthByQrCode(jsonObjectCheckData, String.valueOf(tempUpdate), mLocation);
//            } catch (Exception e) {
//                //非json串，是其他码
//                viewModel.requestQrValidate(qrCodeValue);
//            }*/
//        } else {
//            //闽政通平台
//            transMethod = (type == 1) ? MqttHelper.TMETHOD_GM : MqttHelper.TMETHOD_BMM;
//            viewModel.setQrCodeValue(qrCodeValue);
//            //viewModel.sendPostHealthVerification(qrCodeValue, String.valueOf(tempUpdate));
//        }
//        requestHealthByCode();
//    }
//
//    //界面报文显示
//    private void prependMsg(final String msg) {
//        Log.i("tempHealthy", msg);
//    }
//
//    private void lazyResetTip(long time) {
//        mVM.delayed(time, TAG_RESET_TIP, new Function0<Unit>() {
//            @Override
//            public Unit invoke() {
//                runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        resetTip();
//                    }
//                });
//                return null;
//            }
//        });
//    }
//
//    private void resetTip() {
//        LogUtil.e("=======changeTip: resetTip");
//        stopLazy(TAG_RESET_FACE);
//        mVM.setMProcessing(false);
//        if (SpUtils.getDeviceEnable()) {
//            mVM.changeTipsInfo(getString(R.string.stand_in_box), R.drawable.bg_tip_blue, "", R.color.green);
//        } else {
//            mVM.changeTipsInfo(getString(R.string.device_disable), R.drawable.bg_tip_red, "", R.color.red);
//        }
//        LogUtil.e("=====faceCenter onHasFaceChange resetTip");
//        resetFace();
//    }
//
//    private void lazyResetFace(long time) {
//        stopLazy(TAG_RESET_FACE);
//        mVM.delayed(time, TAG_RESET_FACE, new Function0<Unit>() {
//            @Override
//            public Unit invoke() {
//                mVM.setMProcessing(false);
//                resetFace();
//                return null;
//            }
//        });
//    }
//
//    private void stopLazy(String tag) {
//        if (mVM != null) {
//            mVM.stopDelayed(tag);
//        }
//    }
//
//    public void showToastView(String message) {
//        runOnUiThread(new Runnable() {
//            @Override
//            public void run() {
//                MyToastView.getInstance().Toast(SimpleHealthIdActivity_old.this, message);
//            }
//        });
//
//    }
//
//    private void goSettings() {
//        EditTextDialog editTextDialog = new EditTextDialog(this, true);
//        editTextDialog.setOnDialogClickListener(new EditTextDialog.EditTextDialogListener() {
//            @Override
//            public void commit(String it) {
//                if (it.isEmpty()) {
//                    showToastView(getString(R.string.pwd_is_null));
//                    return;
//                }
//                String password = SpUtils.getSettingPassword();
//                //这里内置两个密码，默认123456，778899是给售后用的。请须知
//                if (it.equals(password)) {
//                    //startActivity(new Intent(SimpleHealthIdActivity.this, SettingMenuActivity.class));
//                    startActivity(new Intent(SimpleHealthIdActivity_old.this, ConfigSettingActivity.class));
//                } else {
//                    showToastView(getString(R.string.pwd_failed_retry));
//                }
//            }
//
//            @Override
//            public void hiddleTestClick() {
//                //startActivity(new Intent(SimpleHealthIdActivity.this, SettingMenuActivity.class));
//                startActivity(new Intent(SimpleHealthIdActivity_old.this, ConfigSettingActivity.class));
//                //mqttHelper.onMqttListener.onReceiveMsg("RESTART_APPLICATION", "dsfsdf");
//            }
//        });
//
//        editTextDialog.show(
//                getString(R.string.pwd_verif), "", getString(R.string.confirm), getString(
//                        R.string.input_verif_pwd
//                )
//        );
//    }
//
//    /**
//     * Socket服务器连接状态变更
//     */
//    @Subscribe(thread = EventThread.MAIN_THREAD)
//    public void linkServerEvent(LinkServerEvent event) {
//        updateTopViewTime();
//        facePassParsener.updateDevInfoToWeb();
//    }
//
//
//    private void updateTopViewTime() {
//        facePassParsener.updateTopViewTime();
//        //更新主界面信息
//        boolean netAvailable = NetWorkUtils.isNetworkConnected(SimpleHealthIdActivity_old.this);
//        tv_net.setText("网络\n" + (netAvailable ? "连接" : "断开"));
//        int tempModel = SpUtils.getmTempType();
//        int defaultType = TempModelEntity.CLOSE;
//        boolean isTempClose = tempModel == defaultType;
//        tv_temp.setText("测温\n" + (isTempClose ? "关闭" : "打开"));
//        tv_person_card.setText("人证\n" + (SpUtils.getFaceAndCardEnable() ? "打开" : "关闭"));
//        tv_accord.setText("记录\n" + (SpUtils.getDataReplyEnable() ? "打开" : "关闭"));
//        tvCodeType.setText("康码\n" + ((SpUtils.getHealthPlatform() == 0) ? "国康" : "八闽"));
//    }
//
//    private void initFaceConfig() {
//        int cameraCount = CameraProvider.getCamerasCount(this);
//        LogUtil.e("========获取相机个数: " + cameraCount);
//        int currentWidth = SpUtils.getResolutionWidth();
//        int currentHeight = SpUtils.getResolutionHeight();
//        if (currentWidth == 0 || currentHeight == 0) {
//            setMCustomSize(null);
//        } else {
//            setMCustomSize(new Point(currentWidth, currentHeight));
//        }
//        //选择是单目还是双目
//        setMIsDoubleCamera(SpUtils.getAutoSwitchCamera());
//        int prewRoatation = 0;
//        switch (SpUtils.getPrewRotate()) {
//            case 0:
//                prewRoatation = 0;
//                break;
//            case 1:
//                prewRoatation = 90;
//                break;
//            case 2:
//                prewRoatation = 180;
//                break;
//            default:
//                prewRoatation = 270;
//                break;
//        }
//        setMPrewRotation(prewRoatation);
//
//        int rectRoatation = 0;
//        switch (SpUtils.getRectRotate()) {
//            case 1:
//                rectRoatation = 0;
//                break;
//            case 2:
//                rectRoatation = 90;
//                break;
//            case 3:
//                rectRoatation = 180;
//                break;
//            case 4:
//                rectRoatation = 270;
//                break;
//            default:
//                rectRoatation = FaceManager.getInstance().getMFaceRotation();
//                break;
//        }
//        setMRectRotation(rectRoatation);
//        setMIsFront(SpUtils.getCameraFacing());
//        mFrv.setScreenWidth(SpUtils.getScreenwidth(), SpUtils.getScreenheight(), SpUtils.getShowFaceRectView());
//
//        mFrv.setFaceRectDisListener(new FaceRecDisListener() {
//            @Override
//            public void rectDistance(float distance, boolean isCenter, float rectWidth, Point headPoint) {
//                mVM.setMRectDistance((int) distance);
//                TempManager.getInstance().mRectDistance = (int) distance;
//                TempManager.getInstance().headPoint = headPoint;
//                mVM.setMIsFaceCenter(true);
//            }
//        });
//    }
//
//    //==============不涉及到业务逻辑的代码===========================================================================================================
//
//    private void initReceiver() {
//        IntentFilter filter = new IntentFilter();
//        filter.addAction(Intent.ACTION_TIME_TICK);      //时间变化
//        filter.addAction(AppInfo.DOWN_TASK_SUCCESS); //广告下发
//        filter.addAction(AppInfo.UPDATE_MAIN_BOTTOM_VIEW);
//        registerReceiver(receiver, filter);
//    }
//
//
//    @Override
//    public ImageView getQrBindImageView() {
//        return mDevQrCode;
//    }
//
//    @Override
//    public TextView getTvAppVersion() {
//        return mTvVersion;
//    }
//
//    @Override
//    public TextView getTvShowTime() {
//        return mTvTime;
//    }
//
//    @Override
//    public TextView getTvCompany() {
//        return null;
//    }
//
//    @Override
//    public TextView getTvDep() {
//        return null;
//    }
//
//    @Override
//    public AbsoluteLayout getAbView() {
//        return mAdView;
//    }
//
//    @Override
//    public TextView getTvDevName() {
//        return null;
//    }
//
//    @Override
//    public TextView getTvMac() {
//        return null;
//    }
//
//    @Override
//    public ImageView getIvLogo() {
//        return null;
//    }
//
//    @Override
//    public ImageView getIvServer() {
//        return null;
//    }
//
//    @Override
//    public ImageView getIvNet() {
//        return null;
//    }
//
//    @Override
//    public boolean onKeyDown(int keyCode, KeyEvent event) {
//        if (keyCode == KeyEvent.KEYCODE_BACK) {
//            facePassParsener.hiddleSleepView();
//            goSettings();
//            return true;
//        }
//        return super.onKeyDown(keyCode, event);
//    }
//
//    @NotNull
//    @Override
//    public CameraPreview getCameraView() {
//        return mCv;
//    }
//
//    @Override
//    public CameraPreview getIrCameraView() {
//        return mIRCv;
//    }
//
//    @Override
//    public FaceCircleViewNew getFaceRectView() {
//        return mFrv;
//    }
//
//
//    @Override
//    public void onHasFaceChange(boolean hasFace) {
//        if (hasFace) {
////            mVM.setMProcessing(false);
//            mVM.setMStrangerTime(0);
//            runOnUiThread(new Runnable() {
//                @Override
//                public void run() {
//                    facePassParsener.hiddleSleepView();
//                }
//            });
//            if (SpUtils.getWhiteLightEnable()) {
//                LightManager.getInstance().openWhiteLight();
//            }
//            return;
//        }
//        authorIDCard = false;
//        isFaceBack = false;
//        isShowHealthyDialog = false;
//        llCodeResult.setVisibility(View.GONE);
//        qrCodeType = "0";
//        hideCheckCode();
//        if (mQrDialog != null) {
//            mQrDialog.dismissQrDialog();
//        }
//        runOnUiThread(new Runnable() {
//            @Override
//            public void run() {
//                showWaitDialog(false);
//            }
//        });
//        mVM.removeDelayed(FaceViewModel.TAG_HANDLER_FACE);
//        facePassParsener.showSleepView();
//        lazyResetTip(800);
//    }
//
//    //动态获取相机分辨率
//    @Override
//    public void onSupportedPreviewSizes(@NotNull List<Point> previewsSizes) {
//        super.onSupportedPreviewSizes(previewsSizes);
//        DataRepository.getInstance().setMSupportedPreview(previewsSizes);
//    }
//
//
//    // 192.168.7.61
//    /**
//     * [faceToken] 人脸识别结果
//     * [noMask] 未佩戴口罩为true,否则为false
//     */
//    @Override
//    public void recognizeResult(@NotNull String faceToken, boolean noMask, FacePassImage image) {
////        true 未佩戴    false 佩戴口罩
//        if (noMask) {
//            wearMaskCurrent = AppInfo.MAST_WAER_DEFAULT;
//        } else {
//            wearMaskCurrent = AppInfo.MAST_WAER_RIGHT;
//        }
//        LogUtil.e("========recognizeResult： " + faceToken + " / " + mVM.getMProcessing() + " /noMask =" + noMask);
//        if (!getMHasPerson()) {
//            LogUtil.e("========recognizeResult：000 no has person");
//            resetFace();
//            return;
//        }
//        LogUtil.e("========recognizeResult： 1111");
//        mVM.handlerFaceResult(faceToken, noMask, image == null ? null : new MinFaceData(image.image, image.width, image.height, image.facePassImageRotation));
//    }
//
//    boolean isFaceBack = false;  //是否有资格获取人脸图片信息
//    float tempUpdate = 0.0f;
//    Bitmap cameraBitmap = null;
//
//    @Override
//    public void cameraPreview(byte[] nv21Data, int width, int height, int rotation) {
//        if (nv21Data == null) {
//            return;
//        }
//        if (!isFaceBack) {
////            LogUtil.cdl("======cameraBitmap=======isFaceBack==false=");
//            return;
//        }
//        cameraBitmap = null;
//        AcsService.getInstance().executor(() -> {
//            try {
//                YuvImage img = new YuvImage(
//                        nv21Data,
//                        ImageFormat.NV21,
//                        width,
//                        height,
//                        null
//                );
//                ByteArrayOutputStream stream = new ByteArrayOutputStream();
//                img.compressToJpeg(new Rect(0, 0, width, height), 8, stream);
//                cameraBitmap = BitmapFactory.decodeByteArray(stream.toByteArray(), 0, stream.size());
//                Matrix mat = new Matrix();
//                mat.postRotate(mSaveRotate);
//                cameraBitmap = Bitmap.createBitmap(cameraBitmap, 0, 0, cameraBitmap.getWidth(), cameraBitmap.getHeight(), mat, true);
//                stream.close();
////                LogUtil.cdl("======cameraBitmap==========" + (cameraBitmap == null));
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        });
//
//        ToolUtils.roatetranBitmap(nv21Data, width, height, mSaveRotate, new RoateImageListener() {
//
//            @Override
//            public void backImageListener(boolean isSuccess, String imagePath) {
//                if (!isSuccess) {
//                    return;
//                }
//                imagePathUpdate = imagePath;
//            }
//        });
//        isFaceBack = false;
//    }
//
//    private void showWaitDialog(boolean isShow) {
//        if (waitDialogUtil == null) {
//            return;
//        }
//        if (isShow) {
//            waitDialogUtil.show("Loading...");
//        } else {
//            waitDialogUtil.dismiss();
//        }
//    }
//
//    boolean isShowHealthyDialog = false;
//    String imagePathUpdate = "";
//    String wearMaskCurrent = AppInfo.MAST_WAER_DEFAULT;
//    boolean authorIDCard = false;   //当前是否允许刷身份证
//
//    /***
//     * 请出示健康码 榕城通卡
//     */
//    private void showHealthyTipsCode(Float temp) {
//        if (!SpUtils.getDeviceEnable()) {
//            mVM.changeTipsInfo(getString(R.string.device_disable), R.drawable.bg_tip_red, "", R.color.red);
//            return;
//        }
//        if (!mQrcodeDevIsInit) {
//            showInitQrDevDialog();
//            return;
//        }
//        isFaceBack = true;
//        //AcsService.getInstance().startSpeakTts("请刷身份证或公交卡或国康码", "");
//        mTvInfo.setText(SpUtils.getTextTip());
//        mTvInfo.setTextColor(getResources().getColor(R.color.white));
//        if (!mScalAniming) {
//            mTvInfo.startAnimation(mScalAnim);
//        }
//        authorIDCard = true;
//        isShowHealthyDialog = true;
//        tempUpdate = temp;
//        //tv_temp_healthy.setText(temp + " ℃");
////        tempUpdate = temp;
////        tv_temp_healthy.setText(temp + " ℃");
////        isFaceBack = true;
////        AcsService.getInstance().startSpeakTts(getString(R.string.please_healthcode), "");
////        isShowHealthyDialog = true;
////        mTvInfo.setText("请出示健康码");
////        mTvInfo.setBackgroundResource(R.drawable.bg_tip_blue);
////        if (!mScalAniming) {
////            mTvInfo.startAnimation(mScalAnim);
////        }
//    }
//
//    private void hideCheckCode() {
//        resetTip();
//    }
//
//    //读卡失败
//    @Subscribe(thread = EventThread.MAIN_THREAD)
//    public void idCardCallBackFail(String errMsg) {
//        mVM.changeTipsInfo(errMsg, R.drawable.bg_tip_red, "", R.color.red);
//    }
//
//    @Subscribe(thread = EventThread.MAIN_THREAD)
//    public void idCardCallBack(CardInfo cardInfo) {
//        mCardInfo = cardInfo;
//        if (!SpUtils.getDeviceEnable()) {
//            mVM.changeTipsInfo(getString(R.string.device_disable), R.drawable.bg_tip_red, "", R.color.red);
//            return;
//        }
//        if (!authorIDCard && SpUtils.getmTempType() != TempModelEntity.CLOSE) {
//            AcsService.getInstance().startSpeakTts("请先测温", "");
//            mVM.changeTipsInfo("请先测温", R.drawable.bg_tip_red, "", R.color.red);
//            return;
//        }
//        SoundHelper.playCard();
//        if (cardInfo.type == CardInfo.TYPE_BUS) {
//            String cardTypes = SpUtils.getRongCardType();
//            if (cardTypes == null || !cardTypes.contains(cardInfo.rcType)) {
//                AcsService.getInstance().startSpeakTts("不支持该类型的卡", "");
//                mVM.changeTipsInfo("不支持该类型的卡", R.drawable.bg_tip_red, "", R.color.red);
//                return;
//            }
//            String address = TextUtils.isEmpty(verfyAddr) ? SpUtils.getVerifyAddr() : verfyAddr;
//            mCardHelper.getIdentityByBusCardId(SpUtils.getRongCardUrl(), cardInfo.cardNo, Const.strSN, address, new OnParseCallback() {
//                @Override
//                public void onParse(String name, String cardNo) {
//                    idcardName = name != null ? name.trim() : "";
//                    idcardNo = cardNo;
//                    verify_type = VERIFY_TYPE.VERIFY_TYPE_RC_CARD;
//                    viewModel.setIdentifyData(idcardName + idcardNo);
//                    requestHealthByIDCard();
//                }
//
//                @Override
//                public void onFailure(String errorMsg) {
//                    mVM.changeTipsInfo(errorMsg, R.drawable.bg_tip_red, "", R.color.red);
//                }
//            });
//            return;
//        }
//
//        if (cardInfo.name != null) {
//            cardInfo.name = cardInfo.name.trim();
//        }
//
//        verify_type = VERIFY_TYPE.VERIFY_TYPE_ID_CARD;
//        if (cameraBitmap == null) {
//            mVM.changeTipsInfo("识别异常，请重试", R.drawable.bg_tip_red, "", R.color.red);
//            return;
//        }
//
//        //不进行认证比对
//        if (!SpUtils.getFaceAndCardEnable()) {
//            idcardName = cardInfo.name;
//            idcardNo = cardInfo.cardNo;
//            viewModel.setIdentifyData(idcardName + idcardNo);
//            requestHealthByIDCard();
//            return;
//        }
//
//        FacePassCompareResult compareResult = FaceManager.getInstance().singleComparison(cameraBitmap, cardInfo.photo);
//        if (compareResult == null) {
//            mVM.changeTipsInfo("人证比对出错", R.drawable.bg_tip_red, "", R.color.red);
//            return;
//        }
//        if (compareResult.result != 0 || compareResult.score < 40) {
//            LogUtil.cdl("====认证比对分数===="+compareResult.score );
//            AcsService.getInstance().startSpeakTts("人脸验证失败", "");
//            mVM.changeTipsInfo("人证对比失败", R.drawable.bg_tip_red, "", R.color.red);
//            return;
//        }
//        /*mTvInfo.setText("请出示健康码");
//        mTvInfo.setBackgroundResource(R.drawable.bg_tip_blue);
//        if (!mScalAniming) {
//            mTvInfo.startAnimation(mScalAnim);
//        }
//        //显示弹窗
//        isShowHealthyDialog = true;
//        AcsService.getInstance().startSpeakTts("请出示健康码", "");*/
//        idcardName = cardInfo.name;
//        idcardNo = cardInfo.cardNo;
//        viewModel.setIdentifyData(idcardName + idcardNo);
//        requestHealthByIDCard();
//    }
//
//    private void requestHealthByCode() {
//        String qrCodeValue = viewModel.getQrCodeValue();
//        if (SpUtils.getHealthPlatform() == 0) {
//            try {
//                String position = TextUtils.isEmpty(verfyAddr) ? SpUtils.getVerifyAddr() : verfyAddr;
//                JSONObject jsonObjectCheckData = JSONObject.parseObject(qrCodeValue);
//                viewModel.requestHealthByQrCode(jsonObjectCheckData, String.valueOf(tempUpdate), position);
//            } catch (Exception e) {
//                //非json串，是其他码
//                viewModel.requestQrValidate(qrCodeValue);
//            }
//        } else {
//            viewModel.sendPostHealthVerification(qrCodeValue, String.valueOf(tempUpdate));
//        }
//    }
//
//    private void requestHealthByIDCard() {
//        if (SpUtils.getHealthPlatform() == 0) {
//            viewModel.requestBsn();
//        } else {
//            viewModel.sendPostHealthVerification(idcardName, idcardNo, tempUpdate + "");
//        }
//    }
//
//    private void retryRequestHealth() {
//        if (verify_type == VERIFY_TYPE.VERIFY_TYPE_CODE) {
//            requestHealthByCode();
//        } else {
//            requestHealthByIDCard();
//        }
//    }
//
//}
