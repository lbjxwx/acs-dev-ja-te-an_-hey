package com.ys.acsdev.ui.sdcheck;

public interface SdCheckView {
    void addInfoToList(String desc);

    void setThreeClose(String desc);

    void updateWriteProgress(int progress);
}
