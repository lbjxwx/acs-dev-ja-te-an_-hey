package com.ys.acsdev.ui.healthcode

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.huiming.common.ext.async
import com.huiming.common.ext.schedulers
import com.yisheng.facerecog.net.HttpObserver
import com.ys.acsdev.MyApp
import com.ys.acsdev.R
import com.ys.acsdev.api.ApiHelper
import com.ys.acsdev.api.AppConfig
import com.ys.acsdev.attendance.ClockInHelper
import com.ys.acsdev.attendance.bean.ClockInEntity
import com.ys.acsdev.attendance.dao.ClockInDao
import com.ys.acsdev.bean.*
import com.ys.acsdev.commond.AppInfo
import com.ys.acsdev.db.dao.FaceInfoJavaDao
import com.ys.acsdev.db.dao.LocalARDao
import com.ys.acsdev.db.dao.PersonDao
import com.ys.acsdev.helper.SoundHelper
import com.ys.acsdev.light.LightManager
import com.ys.acsdev.manager.DoorManager
import com.ys.acsdev.manager.HighTempManager
import com.ys.acsdev.manager.TempManager
import com.ys.acsdev.service.AcsService
import com.ys.acsdev.setting.entity.TempModelEntity
import com.ys.acsdev.ui.face.util.FaceDealUtil
import com.ys.acsdev.util.*
import com.ys.facepasslib.FaceManager
import io.reactivex.Observable
import io.reactivex.Observer
import io.reactivex.disposables.Disposable
import org.jetbrains.anko.doAsync
import java.io.File
import java.math.BigDecimal
import java.util.concurrent.TimeUnit

class SimpleHealthCodeModel : ViewModel() {

    companion object {
        const val TAG_HANDLER_FACE = "TAG_HANDLER_FACE"
        const val TAG_RESET_FACE = "TAG_RESET_FACE"
        const val TAG_RESET_TIP = "TAG_RESET_TIP"
        const val TEMP_TYPE_NORMAL = 1
    }

    //陌生人警告间隔
    private val LAZY_RESET_TIME = 2500L
    val mDelayedObservable: HashMap<String, Disposable> = hashMapOf()

    //休眠状态
    val mSleepState = MutableLiveData(false)

    val mTipsInfo = MutableLiveData<TipsInfo>()
    val mLazyRestFace = MutableLiveData<Long>()
    val mLazyRestTip = MutableLiveData<Long>()
    val mQrCodeState = MutableLiveData<Long>()
    val mCheckHealthCode = MutableLiveData<Float>()

    //是否正在处理
    var mProcessing = false

    //是否是测温模式
    var mTempEnable = false

    //考勤是否打开
    var mAttendEnable = false

    //二维码登记开关
    var mQrCodeEnable = false

    //测温时间间隔
    var mTempTime = 1.0f

    var mNoMask: Boolean = true

    //温度信息
    var mTempShow = ""

    //用来上传的温度值
    var tempNumFloat: Float = 0f;

    //人脸距离
    var mRectDistance = 0

    var mSaveRotate = 0

    //人脸是否在中心
    var mIsFaceCenter = false
    var mStrangerTime: Long = 0L    //陌生人警告时间
    var mIsAccessRecord = false    //添加通行记录
    var mAccessPerson: PersonBean? = null
    var mIsVisitor = false

    /***
     * 返回浮点型的温度
     */
    fun getTempFloat(): Float {
        tempNumFloat = TempDealUtil.getFloatOnePoint(tempNumFloat);
        return tempNumFloat;
    }

    /***
     * 获取当前是否带口罩了
     * true  没有戴口罩
     * false 戴口罩
     */
    fun getStatuesWearMask(): String {
        var isOpenMask = SpUtils.getmMaskEnable();
        if (!isOpenMask) {
            return AppInfo.MAST_WAER_DEFAULT
        }
        if (mNoMask) {
            return AppInfo.MAST_WAER_DEFAULT
        }
        return AppInfo.MAST_WAER_RIGHT
    }

    fun handlerFaceResult(faceToken: String, noMask: Boolean, minFaceData: MinFaceData? = null) {
        if (mProcessing)
            return
        mProcessing = true
        stopLazy(TAG_RESET_TIP)
        stopLazy(TAG_RESET_FACE)
        SoundHelper.stopWarn()
        if (!mIsFaceCenter) {
            changeTipsInfo(getString(R.string.stand_in_box), R.drawable.bg_tip_blue)
            speechMessage(getString(R.string.stand_in_box))
            mLazyRestFace.postValue(LAZY_RESET_TIME)
            return
        }

        //口罩拦截
        if (noMask && SpUtils.getMaskIntercept()) {
            changeTipsInfo(getString(R.string.no_mask), R.drawable.bg_tip_red)
            speechMessage(getString(R.string.speak_no_mask))
            mLazyRestFace.postValue(LAZY_RESET_TIME)
            if (AppConfig.APP_CUSTOM_TYPE == AppConfig.APP_CUSTOM_JTA_DEFAULT ||
                AppConfig.APP_CUSTOM_TYPE == AppConfig.APP_CUSTOM_JTA_ID_CARD ||
                AppConfig.APP_CUSTOM_TYPE == AppConfig.APP_CUSTOM_JTA_HID
            ) {
                playWearVoice()
            }
            return
        }
        //距离太远
        var maxLen = SpUtils.getFaceRectLength()
        var tempModel = SpUtils.getmTempType()
        LogUtil.cdl("识别距离===" + mRectDistance)
        if (mTempEnable && (mRectDistance >= maxLen || mRectDistance < 1)) {
            val tip = getString(R.string.temp_check_near)
            changeTipsInfo(tip, R.drawable.bg_tip_orange)
            speechMessage(getString(R.string.temp_check_near))
            mLazyRestFace.postValue(LAZY_RESET_TIME)
            return
        }
        if (!mTempEnable || mTempTime <= 0f) {
            handlerFaceInfo(faceToken, noMask, minFaceData)
            return
        }
        stopLazy(TAG_RESET_TIP)
        TempManager.getInstance().sendTempData()
        changeTipsInfo(getString(R.string.temperature), R.drawable.bg_tip_orange)
        delayed((mTempTime * 1000).toLong(), TAG_HANDLER_FACE) {
            handlerFaceInfo(faceToken, noMask, minFaceData)
        }
    }

    //处理人脸信息
    fun handlerFaceInfo(faceToken: String, noMask: Boolean, minFaceData: MinFaceData?) {
        log("========detect handlerFaceInfo")
        stopLazy(TAG_RESET_TIP)
        stopLazy(TAG_RESET_FACE)
        if (faceToken.isNullOrEmpty()) {
            unknowFace(noMask, minFaceData)
            return
        }
        log("=====facepass: 识别成功，获取本地数据 / $faceToken")
        val faceInfo = FaceInfoJavaDao.getFaceInfoByToken(faceToken)
        if (faceInfo == null) {
            unknowFace(noMask, minFaceData)
            LogUtil.faceRegist("=====facepass 识别成功 但无本地数据")
            doAsync {
                FaceManager.instance.delFaceByToken(faceToken)
            }
            return
        }
        log("=====facepass 识别成功，本地有数据===" + faceInfo.toString())
        if (faceInfo.type == AppInfo.FACE_REGISTER_PERSON) {
            //员工
            val person = PersonDao.getPersonById(faceInfo.personId)
            if (person == null) {
                unknowFace(noMask, minFaceData)
                LogUtil.faceRegist("=====facepass 识别成功 但无本地人员数据")
                FaceDealUtil.delelePersonFaceByToken(faceToken)
                return
            }
            compairDbInfoToModify(person, faceInfo)
            LogUtil.e("=====facepass 识别成功: $person")
            LogUtil.e("=====facepass 识别成功000: $faceInfo")
            var tipTxt = getString(R.string.hello_open_door_success)
            if (noMask)
                tipTxt = getString(R.string.no_mask) + "\n" + tipTxt
            if (SpUtils.getSpeakName("员工这里000")) {
                tipTxt = faceInfo.name + "\n" + tipTxt
                LogUtil.cdl("===tipTxt===" + tipTxt)
            }
            if (mTempEnable) {
                handlerTemp(
                    tipTxt + "\n",
                    false,
                    noMask,
                    person.name,
                    person.perId,
                    minFaceData,
                    faceToken
                )
            } else if (mAttendEnable) {
                handlerAttend(
                    person.name,
                    person.perId,
                    "",
                    TipsInfo(tipTxt, R.drawable.bg_tip_green),
                    noMask,
                    false
                )
            } else {
                changeTipsInfo(tipTxt, R.drawable.bg_tip_green)
                speechMessage(getString(R.string.hello_open_door_success), noMask, person.name)
                LogUtil.e("=====faceCenter: openGreen")
                LightManager.getInstance().openGreenLight()
                openDoorManager("员工开门", false)
            }
            mNoMask = noMask
            mIsAccessRecord = true
            mAccessPerson = person
            mIsVisitor = false;
        } else if (faceInfo.type == AppInfo.FACE_REGISTER_VISITOR) {
            //访客
            var visitor = PersonDao.getPersonById(faceInfo.personId)
            if (visitor == null) {
                unknowFace(noMask, minFaceData)
                return
            }
            val isAccess = FaceDealUtil.visitorAccess(visitor)
            if (isAccess) {   //已经过期了
                unknowFace(noMask, minFaceData)
                LogUtil.faceRegist("=====facepass 识别成功 但无本地人员数据")
                FaceDealUtil.delelePersonFaceByToken(faceToken)
                return
            }
            LogUtil.e("=====facepass 识别成功访客: $visitor")
            var tipTxt = getString(R.string.hello_open_door_success)
            if (noMask)
                tipTxt = getString(R.string.no_mask) + "\n" + tipTxt
            if (SpUtils.getSpeakName("访客这里000")) {
                tipTxt = visitor.name + "\n" + tipTxt
            }
            compairDbInfoToModify(visitor, faceInfo)
            if (mTempEnable) {
                handlerTemp(
                    tipTxt + "\n",
                    false,
                    noMask,
                    visitor.name,
                    minFaceData = minFaceData,
                    faceToken = faceToken
                )

            } else {
                changeTipsInfo(tipTxt, R.drawable.bg_tip_green)
                speechMessage(getString(R.string.hello_open_door_success), noMask, visitor.name)
                LogUtil.e("=====faceCenter: openGreen")
                LightManager.getInstance().openGreenLight()
                openDoorManager("访客开门", false)
            }
            mNoMask = noMask
            mIsAccessRecord = true
            mAccessPerson = visitor
            mIsVisitor = true
        } else if (faceInfo.type == AppInfo.FACE_REGISTER_BLACK_LIST) {
            var blackName = faceInfo.name;
            changeTipsInfo(
                getString(R.string.black_person) + " : " + blackName,
                R.drawable.bg_tip_red
            )
            speechMessage(getString(R.string.black_person_forbidon))
            LightManager.getInstance().openRedLight()
            playWearVoice()
        }
        mLazyRestFace.postValue(LAZY_RESET_TIME)
    }

    /****
     * 这里是用来比对识别的数据和本地数据的差异性，
     * 如果信息不同步，就修改本地保存的数据
     */
    private fun compairDbInfoToModify(person: PersonBean?, faceInfo: FaceInfo?) {
        if (person == null || faceInfo == null) {
            return
        }
        var personName = person.name
        var faceInfoName = faceInfo.name
        if (!personName.equals(faceInfoName)) {
            LogUtil.cdl("====文件名字不一致====" + personName + " / " + faceInfoName)
            var isModify = FaceInfoJavaDao.modifyFaceName(faceInfo, personName);
            LogUtil.cdl("====文件名字不一致===modify=" + isModify)
        }
    }

    //陌生人处理
    fun unknowFace(noMask: Boolean, minFaceData: MinFaceData?) {
        mStrangerTime = System.currentTimeMillis()
        var tipTxt = ""
        if (mTempEnable && (SpUtils.getRecogShowEnable() || SpUtils.getOnlyCheckEnable())) {
            tipTxt = ""
        } else {
            tipTxt = getString(R.string.recg_failed_stranger)
        }

        if (noMask) {
            tipTxt = getString(R.string.no_mask) + "\n" + tipTxt
        }
        if (mTempEnable) {
            val isReturn =
                handlerTemp(
                    if (tipTxt.isEmpty()) tipTxt else (tipTxt + "\n"),
                    true,
                    noMask,
                    minFaceData = minFaceData,
                    faceToken = null
                )
            if (isReturn)
                return
        } else if (mQrCodeEnable) {
            mQrCodeState.postValue(System.currentTimeMillis())
            return
        } else {
            changeTipsInfo(tipTxt, R.drawable.bg_tip_red)
            speechMessage(getString(R.string.stranger_warning), noMask)
            LightManager.getInstance().openRedLight()
            openDoorManager("识别到陌生人", true)
        }
        mNoMask = noMask
        mIsAccessRecord = true
        mAccessPerson = null
//        mLazyRestTip.postValue(2500)
        mLazyRestFace.postValue(LAZY_RESET_TIME)
        log("========detect unknowFace end")
    }

    //处理打卡
    fun handlerAttend(
        name: String,
        perId: String,
        speakText: String,
        tipInfo: TipsInfo,
        noMask: Boolean,
        warn: Boolean? = null
    ) {
        if (!SpUtils.getAttendEnable()) {
            return
        }
        AcsService.getInstance()
            .executor { insertWorkerToDev(name, perId, speakText, tipInfo, noMask, warn) }
    }

    //处理测温
    fun handlerTemp(
        tipText: String,
        unknowFace: Boolean,
        noMask: Boolean,
        name: String = "",
        perId: String = "",
        minFaceData: MinFaceData?,
        faceToken: String?
    ): Boolean {
        var oldTipText = tipText
        //距离太远
        var maxLen = SpUtils.getFaceRectLength()
        var tempModel = SpUtils.getmTempType()
        LogUtil.cdl("识别距离===" + mRectDistance)
        if (mRectDistance >= maxLen) {
            val tip = oldTipText + getString(R.string.temp_check_near)
            changeTipsInfo(tip, R.drawable.bg_tip_orange)
            speechMessage(getString(R.string.temp_check_near), noMask, name)
            mLazyRestFace.postValue(2000)
            mLazyRestTip.postValue(4 * 1000)
            return false
        }
        //人脸不在中央
        if (!mIsFaceCenter) {
            val tip = oldTipText + getString(R.string.temperature_zone)
            changeTipsInfo(tip, R.drawable.bg_tip_orange)
            speechMessage(getString(R.string.temperature_zone), noMask, name)
            mLazyRestFace.postValue(2000)
            mLazyRestTip.postValue(4 * 1000)
            return false
        }
        var warnTemp: Float = SpUtils.getTempHeightWearNew()
        var lowTemp = SpUtils.getmTempLow()
        var hightTemp = SpUtils.getTempHeightWearNew()
        tempNumFloat = TempManager.getInstance().getTemp(mRectDistance)
        log("=======界面返回来得温度===" + tempNumFloat)
        try {
            val bg = BigDecimal(tempNumFloat.toDouble())
            tempNumFloat = bg.setScale(1, BigDecimal.ROUND_HALF_UP).toFloat()
            log("=======界面返回来得温度===保留小数:" + tempNumFloat)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        //华氏度
        if (SpUtils.getTempUnit() == 1 && tempNumFloat != -99f) {
            tempNumFloat = tempNumFloat * 1.8f + 32
            tempNumFloat = getTempFloat() //保留意一位小数
            lowTemp = lowTemp * 1.8f + 32
            hightTemp = hightTemp * 1.8f + 32
        }
        mTempShow =
            String.format("%.1f", tempNumFloat) + (if (SpUtils.getTempUnit() == 1) "℉" else "℃")
        //未获取到测温数据
        if (tempNumFloat == -99f) {
            val speakText = getString(R.string.temperature_failed)
            val tipInfo = TipsInfo(
                oldTipText + getString(R.string.temperature_failed_check),
                R.drawable.bg_tip_orange
            )
            changeTipsInfo(tipInfo)
            speechMessage(speakText, noMask, name)
            return false
        }
        //获取温度过低，无效温度
        if (tempNumFloat < lowTemp) {
            var speakText = getString(R.string.temperature_zone)
            var text = oldTipText + getString(R.string.temperature_zone)
            val tipInfo = TipsInfo(
                text,
                R.drawable.bg_tip_orange
            )
            changeTipsInfo(tipInfo)
            speechMessage(speakText, noMask, name)
            return false
        }

        //温度过高
        if (tempNumFloat >= hightTemp) {
            var speakText = MyApp.getInstance().getString(
                R.string.high_temperature_speak,
                tempNumFloat.toString()
            )
            var text = oldTipText + MyApp.getInstance().getString(
                R.string.high_temperature,
                mTempShow
            )
            val tipInfo = TipsInfo(text, R.drawable.bg_tip_red, mTempShow, R.color.red)
            if (mAttendEnable && !perId.isNullOrEmpty()) {
                handlerAttend(name, perId, speakText, tipInfo, noMask, true)
                return false
            }
            LogUtil.cdl("======TipsInfo====" + tipInfo.toString())
            changeTipsInfo(tipInfo)
            speechMessage(speakText, noMask, name)
            LightManager.getInstance().openRedLight()
            playWearVoice()
            return false
        }
        //正常温度
        if (tempNumFloat < warnTemp) {
            var speakText = MyApp.getInstance().getString(
                R.string.check_temperature_speak,
                tempNumFloat.toString()
            )
            var text = oldTipText + MyApp.getInstance().getString(
                R.string.check_temperature,
                mTempShow
            )
            if (SpUtils.getHideTempNumberEnable()) {  //隐藏温度
                text = oldTipText + getString(R.string.temp_normal)
                speakText = getString(R.string.speak_temp_normal)
            }
            val tipInfo = TipsInfo(
                text,
                R.drawable.bg_tip_green,
                mTempShow,
                R.color.green
            )
            LogUtil.cdl("==体温正常，这里执行提示操作。健康码==")
            if (SpUtils.getHealthCodeEnable()) {
                mCheckHealthCode.postValue(tempNumFloat)
                return true
            }
            if (mAttendEnable && !perId.isNullOrEmpty()) {
                handlerAttend(name, perId, speakText, tipInfo, noMask, false)
                return false
            }
            changeTipsInfo(tipInfo)
            speechMessage(speakText, noMask, name)
            LogUtil.cdl("==体温正常，这里执行开门步骤==" + unknowFace)
            openDoorManager("体温正常，这里执行开门步骤", unknowFace)
            LightManager.getInstance().openGreenLight()
            if (mQrCodeEnable && unknowFace) {
                mQrCodeState.postValue(System.currentTimeMillis())
                return true
            }

            return false
        }
        var speakText = MyApp.getInstance().getString(
            R.string.high_temperature_speak,
            mTempShow
        )
        var text =
            oldTipText + MyApp.getInstance().getString(R.string.check_temperature, mTempShow)
        if (SpUtils.getHideTempNumberEnable()) { //隐藏温度
            text = oldTipText + getString(R.string.temp_abnormal)
            speakText = getString(R.string.speak_temp_abnormal)
        }
        Log.e("heightTemp", "text = " + text + " \n speakText =" + speakText)
        val tipInfo = TipsInfo(
            text, R.drawable.bg_tip_red, mTempShow, R.color.red
        )
        if (mAttendEnable && !perId.isNullOrEmpty()) {
            handlerAttend(name, perId, speakText, tipInfo, noMask, true)
            return false
        }
        changeTipsInfo(tipInfo)
        speechMessage(speakText, noMask, name)
        LightManager.getInstance().openRedLight()
        playWearVoice();
        return false
    }

    private fun openDoorManager(printTag: String, isStrange: Boolean) {
        LogUtil.door("识别开门逻辑走这个====" + printTag + " / " + isStrange)
        if (!isStrange) {  //非陌生人，直接开门
            LogUtil.door("识别开门逻辑走这个===非陌生人，直接开门=")
            DoorManager.getInstance().open(printTag)
            return
        }
        LogUtil.door("识别开门逻辑走这个===陌生人逻辑处理=")
    }

    /***
     * 获取单机模式的语音文件
     * ispass  体温正常
     * false   体温异常
     */
    private fun getSimpleModelText(isPass: Boolean, printTag: String): String {
        LogUtil.cdl("=========获取精简模式的温度===" + printTag)
        if (isPass) {
            return hanleTempMormalSingleModelText();
        }
        return hanleTempHeightSingleModelText();
    }

    /***
     * 处理高温的精简模式
     */
    private fun hanleTempHeightSingleModelText(): String {
        var text = getString(R.string.abnormal_temp)
        return text
    }

    /***
     * 体温正常的精简模式
     */
    private fun hanleTempMormalSingleModelText(): String {
        var text = getString(R.string.hello_open_door_success)
        return text;
    }

    private fun playWearVoice() {
        var isOpen = SpUtils.getPlayWearMedia()
        if (!isOpen) {
            return
        }
        SoundHelper.playWarn()
    }

    fun updateTip(result: TipResult) {

    }


    //修改提示信息
    fun changeTipsInfo(
        text: String,
        bgColor: Int,
        temp: String = "",
        tempColor: Int = R.color.green
    ) {
        mTipsInfo.postValue(TipsInfo(text, bgColor, temp, tempColor))
    }

    fun changeTipsInfo(tipsInfo: TipsInfo) {
        if (SpUtils.getHideTempNumberEnable()) {
            tipsInfo.temp = ""
        }
        mTipsInfo.postValue(tipsInfo)
    }

    private fun speechMessage(
        message: String,
        noMask: Boolean = false,
        name: String = "",
        rawId: Int = AcsService.NOT_PLAY_LOCAL_MEDIA
    ) {
        var speakTxt = if (noMask) getString(R.string.speak_no_mask) + message else message
        if (SpUtils.getSpeakName("==speechMessage==方法调用")) {
            speakTxt = name + speakTxt
        }
        LogUtil.cdl("=======speakTxt=======" + speakTxt);
        AcsService.getInstance().startSpeakTts(speakTxt, rawId, "未佩戴口罩")
    }

    fun stopLazy(tag: String) {
        stopDelayed(tag)
    }

    fun delayed(time: Long, tag: String, content: () -> Unit) {
        var disposable = mDelayedObservable.get(tag)
        Observable.timer(time, TimeUnit.MILLISECONDS).schedulers()
            .subscribe(object : Observer<Long> {
                override fun onComplete() {
                }

                override fun onSubscribe(d: Disposable) {
                    if (disposable != null) {
                        if (!disposable!!.isDisposed)
                            disposable!!.dispose()
                        disposable = d
                    }
                    disposable = d
                    mDelayedObservable.put(tag, disposable!!)
                }

                override fun onNext(t: Long) {
                    content()
                }

                override fun onError(e: Throwable) {
                }
            })
    }

    fun removeDelayed(tag: String) {
        var disposable = mDelayedObservable.get(tag)
        if (disposable != null) {
            if (!disposable.isDisposed)
                disposable.dispose()
            disposable = null
            mDelayedObservable.remove(tag)
        }
    }

    fun stopDelayed(tag: String) {
        val disposable = mDelayedObservable.get(tag)
        if (disposable != null) {
            if (!disposable.isDisposed)
                disposable.dispose()
        }
    }

    /**
     * 打卡
     * empId: 员工ID
     */
    private fun insertWorkerToDev(
        name: String,
        empId: String,
        speakText: String,
        tipInfo: TipsInfo,
        noMask: Boolean,
        warn: Boolean? = null
    ) {
        if (!SpUtils.getSaveInfoToLocal()) {
            //不保存信息到服务器，本地
            //删除本地保存的缓存信息
            ClockInDao.delAlCockIn();
            return
        }
        val clockInTime = System.currentTimeMillis()
        LogUtil.attendance("====insertWorkerToDev:$name / $empId / $clockInTime")
        val entity = ClockInHelper.getClockInEntity(empId, clockInTime)
        if (entity == null) {
            LogUtil.attendance("====clockIn: 打卡异常")
            tipInfo.text += (if (tipInfo.text.isEmpty()) "" else "\n") + getString(R.string.punch_error)
            changeTipsInfo(tipInfo)
            var speechTextSuccess = speakText + getString(R.string.punch_error);
            speechMessage(speechTextSuccess, noMask, name)
            return
        }
        entity.name = name
        entity.empId = empId
        LogUtil.attendance("====clockIn:${entity.toString()}")
        if (SpUtils.getWorkModel() == AppInfo.MODEL_NET) {
            insertWorkerToWeb(entity)
        } else {
            entity.save()
        }
        var speechTextSuccess = speakText + getString(R.string.punch_success);
        tipInfo.text += (if (tipInfo.text.isEmpty()) "" else "\n") + getString(R.string.punch_success)
        speechTextSuccess = speakText + getString(R.string.punch_success);
        changeTipsInfo(tipInfo)
        speechMessage(speechTextSuccess, noMask, name)
        if (warn == null) {
            return
        }
        if (warn) {
            LightManager.getInstance().openRedLight()
            playWearVoice();
            return
        }
        LogUtil.e("=====faceCenter: openGreen")
        LightManager.getInstance().openGreenLight()
        openDoorManager("打卡开门==insertWorkerToDev", false)
    }

    //上传打卡记录至服务器或保存至本地
    private fun insertWorkerToWeb(entity: ClockInEntity) {
        LogUtil.attendance("提交考勤到服务器")
        val signParm = ClockInHelper.getSignParm(entity)
        ApiHelper.freeSignIn(signParm).async()
            .subscribe(object : HttpObserver<Any?>() {
                override fun onSuccess(o: Any?) {
                    LogUtil.attendance("提交考勤到服务器success")
                }

                override fun onFailed(code: Int, msg: String) {
                    LogUtil.attendance("提交考勤到服务器failed: $code / $msg")
                    entity.save()
                }
            })
    }

    /***
     * 提交通行记录
     */
    fun saveAccessFaceRecord(
        context: Context,
        imagePath: String,
        person: PersonBean?,
        temperatureUpdate: Float,
        wearMask: String, qrCodeType: String
    ) {
        if (!mIsFaceCenter) {
            LogUtil.update("=====人脸不再识别区，中断上传=====")
            return
        }
        if (!SpUtils.getSaveInfoToLocal()) {
            FileUtils.deleteDirOrFile(AppInfo.BASE_PATH_CACHE, "清理离线记录");    //不保存信息到服务器，本地
            LocalARDao.delAll()                                                      //删除本地保存的缓存信息
            return
        }
        var temperature = TempDealUtil.mathTempByUnit(temperatureUpdate)
        var passType = FaceDealUtil.getPassTypeJujle(person);
        LogUtil.update("=====passType=====" + passType + " / " + temperature + " / " + wearMask)
        var fileCheck = File(imagePath)
        LogUtil.update("=====passType==文件是否存在===" + (fileCheck.exists()))
        if (!fileCheck.exists()) {
            return
        }
        AcsService.getInstance().insertAccessFaceRecordService(
            context,
            person,
            passType,
            person?.perId ?: "",
            imagePath,
            temperature,
            wearMask, qrCodeType
        )
    }


    fun getString(resId: Int): String {
        return MyApp.getInstance().getString(resId)
    }

    fun destory() {
        if (mDelayedObservable != null && mDelayedObservable.size > 0) {
            val iterator = mDelayedObservable.iterator()
            while (iterator.hasNext()) {
                val next = iterator.next()
                removeDelayed(next.key)
            }
        }
    }

    fun log(msg: String) {
        LogUtil.e(msg, "FaceViewModel")
    }
}