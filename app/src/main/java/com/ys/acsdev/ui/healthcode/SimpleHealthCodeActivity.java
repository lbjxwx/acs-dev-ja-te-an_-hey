package com.ys.acsdev.ui.healthcode;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Point;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AbsoluteLayout;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.alibaba.fastjson.JSONObject;
import com.bumptech.glide.Glide;
import com.hwangjr.rxbus.RxBus;
import com.hwangjr.rxbus.annotation.Subscribe;
import com.hwangjr.rxbus.thread.EventThread;
import com.nlp.cloundcbsappdemo.listener.HealthyMessageListener;
import com.nlp.cloundcbsappdemo.model.HealthyPersonEntity;
import com.nlp.cloundcbsappdemo.model.MessageEvent;
import com.nlp.cloundcbsappdemo.util.MessageDealUtil;
import com.nlp.cloundcbsappdemo.viewmodel.MainViewModel;
import com.ys.acsdev.R;
import com.ys.acsdev.bean.LinkServerEvent;
import com.ys.acsdev.bean.MinFaceData;
import com.ys.acsdev.commond.AppInfo;
import com.ys.acsdev.commond.CommondViewModel;
import com.ys.acsdev.db.DataRepository;
import com.ys.acsdev.light.LightManager;
import com.ys.acsdev.listener.QrDismissListener;
import com.ys.acsdev.manager.TempManager;
import com.ys.acsdev.manager.TtsManager;
import com.ys.acsdev.runnable.upload.RoateImageListener;
import com.ys.acsdev.service.AcsService;
import com.ys.acsdev.setting.SettingMenuActivity;
import com.ys.acsdev.setting.entity.TempModelEntity;
import com.ys.acsdev.task.parsener.TaskPlayerParsener;
import com.ys.acsdev.task.view.TaskPlayView;
import com.ys.acsdev.ui.face.FaceViewModel;
import com.ys.acsdev.ui.face.view.FacePassView;
import com.ys.acsdev.util.ActivityCollector;
import com.ys.acsdev.util.CameraProvider;
import com.ys.acsdev.util.LogUtil;
import com.ys.acsdev.util.NetWorkUtils;
import com.ys.acsdev.util.SimpleDateUtil;
import com.ys.acsdev.util.SpUtils;
import com.ys.acsdev.util.ToolUtils;
import com.ys.acsdev.util.ViewSizeChange;
import com.ys.acsdev.view.MyToastView;
import com.ys.acsdev.view.WaitDialogUtil;
import com.ys.acsdev.view.dialog.EditTextDialog;
import com.ys.acsdev.view.dialog.QrBottomDialog;
import com.ys.facepasslib.BaseLivenessActivity;
import com.ys.facepasslib.FaceManager;
import com.ys.facepasslib.camera.CameraPreview;
import com.ys.facepasslib.listener.FaceRecDisListener;
import com.ys.facepasslib.view.FaceCircleViewNew;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.ThreadMode;
import org.jetbrains.annotations.NotNull;

import java.util.List;

import cn.anicert.polymerizeDevice.DeviceManager;
import cn.anicert.polymerizeDevice.event.CtidQRCodeScannedEvent;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import mcv.facepass.types.FacePassImage;

/***
 * 更新内容
 * 暂停读取UseAXManger.wait(context);
 * 继续读取 UseAXManger.continueRed(context);
 * 关闭扫码头 UseAXManger.closeDev(context);
 *
 */
public class SimpleHealthCodeActivity extends BaseLivenessActivity implements FacePassView, TaskPlayView,
        View.OnClickListener {

    //陌生人警告间隔
    private static final int MSG_RESET_IMAGE = 1;

    private static final String TAG_RESET_TIP = "TAG_RESET_TIP";
    private static final String TAG_RESET_FACE = "TAG_RESET_FACE";
    private static long mKeyActionTime = 0L;
    private int mSaveRotate = 0;

    SimpleHealthCodeModel mVM;
    SimpleHealthCodeParsener facePassParsener = null;
    TaskPlayerParsener taskPlayerParsener = null;


    Animation mScalAnim;
    QrBottomDialog mQrDialog;
    private boolean mScalAniming = false;
    String qrCodeType = "0";


    private void initOther() {
        if (mVM == null) {
            mVM = ViewModelProviders.of(this).get(SimpleHealthCodeModel.class);
        }
        if (mScalAnim == null) {
            mScalAnim = AnimationUtils.loadAnimation(this, R.anim.tip_scal);
            mScalAnim.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {
                    mScalAniming = true;
                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    mScalAniming = false;
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
        }
        if (mQrDialog == null) {
            mQrDialog = new QrBottomDialog(SimpleHealthCodeActivity.this);
        }
        mQrDialog.setOnDismissListener(new QrDismissListener() {

            @Override
            public void onDismiss() {
                resetTip();
            }
        });
    }

    private static final int CLOSE_WHITE_LIGHT_MESSAGE = 56845;
    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case CLOSE_WHITE_LIGHT_MESSAGE:
                    removeMessages(CLOSE_WHITE_LIGHT_MESSAGE);
                    LightManager.getInstance().closeWhiteLight();
                    break;
                case MSG_RESET_IMAGE:
                    removeMessages(MSG_RESET_IMAGE);
//                    mIvPerson.setImageResource(R.mipmap.house_icon)
                    break;
            }
        }
    };

    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            LogUtil.task("==============监听到广播====" + action);
            if (action.equals(Intent.ACTION_TIME_TICK)) {
                updateTopViewTime();
                if (taskPlayerParsener != null) {
                    taskPlayerParsener.updateSleepTvOnTime();
                }
            } else if (action.equals(AppInfo.DOWN_TASK_SUCCESS)) {
                if (mFlSleep.getVisibility() == View.VISIBLE) {
                    taskPlayerParsener.getTaskFromDb();
                }
            } else if (action.equals(AppInfo.UPDATE_MAIN_BOTTOM_VIEW)) {
                if (facePassParsener != null) {
                    facePassParsener.updateCompanyDepartment();
                }
            }
        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_simple_healthcode);
        EventBus.getDefault().register(this);
        initOther();
        initView();
        initReceiver();
        initFaceConfig();
        ActivityCollector.addActivity(this);
        initHealthCode();
    }

    CameraPreview mCv;
    ImageView mIvLogo, mBtnSettings;
    View view_click;
    FaceCircleViewNew mFrv;
    TextView mTvTemp, mTvInfo;
    TextView mTvVersion, mTvTime, mTvCompany, mTvDep, mTvDevName, tv_dev_mac;
    ImageView mIvServer, mIvNet;
    FrameLayout mFlSleep;
    AbsoluteLayout mAdView;
    CameraPreview mIRCv;
    ImageView mDevQrCode, mIvCali;
    FrameLayout mFlNoInit;
    LinearLayout mLlInfo;
    View view_setting;
    ImageView iv_temp_show;
    WaitDialogUtil waitDialogUtil;

    TextView tv_temp_healthy, tv_name_healthy, tv_idcard_healthy, tv_date_healthy, tv_result_healthy, tv_status_healthy, tv_hsjc_healthy;
    ImageView iv_qr_healthy;
    ImageView iv_face_healthy;
    LinearLayout fl_code_result;

    private void initView() {
        fl_code_result = (LinearLayout) findViewById(R.id.fl_code_result);
        tv_temp_healthy = (TextView) findViewById(R.id.tv_temp_healthy);
        tv_name_healthy = (TextView) findViewById(R.id.tv_name_healthy);
        tv_idcard_healthy = (TextView) findViewById(R.id.tv_idcard_healthy);
        tv_date_healthy = (TextView) findViewById(R.id.tv_date_healthy);
        tv_result_healthy = (TextView) findViewById(R.id.tv_result_healthy);
        iv_qr_healthy = (ImageView) findViewById(R.id.iv_qr_healthy);
        iv_face_healthy = (ImageView) findViewById(R.id.iv_face_healthy);
        tv_status_healthy = (TextView) findViewById(R.id.tv_status_healthy);
        tv_hsjc_healthy = (TextView) findViewById(R.id.tv_hsjc_healthy);

        view_setting = (View) findViewById(R.id.view_setting);
        iv_temp_show = (ImageView) findViewById(R.id.iv_temp_show);
        mBtnSettings = (ImageView) findViewById(R.id.mBtnSettings);
        mBtnSettings.setOnClickListener(this);

        view_setting.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                goSettings();
                return true;
            }
        });
        mLlInfo = (LinearLayout) findViewById(R.id.mLlInfo);
        mFlNoInit = (FrameLayout) findViewById(R.id.mFlNoInit);
        mDevQrCode = findViewById(R.id.dev_qrcode);
        mIRCv = (CameraPreview) findViewById(R.id.mIRCv);

        mIvCali = findViewById(R.id.iv_cali);
        mAdView = (AbsoluteLayout) findViewById(R.id.mAdView);
        mFlSleep = (FrameLayout) findViewById(R.id.mFlSleep);
        mIvServer = (ImageView) findViewById(R.id.mIvServer);
        mIvNet = (ImageView) findViewById(R.id.mIvNet);
        mTvVersion = (TextView) findViewById(R.id.mTvVersion);
        mTvTime = (TextView) findViewById(R.id.mTvTime);
        mTvCompany = (TextView) findViewById(R.id.mTvCompany);
        mTvDep = (TextView) findViewById(R.id.mTvDep);
        mTvDevName = (TextView) findViewById(R.id.mTvDevName);
        tv_dev_mac = (TextView) findViewById(R.id.tv_dev_mac);
        mTvInfo = (TextView) findViewById(R.id.mTvInfo);
        mTvTemp = (TextView) findViewById(R.id.mTvTemp);
        mCv = (CameraPreview) findViewById(R.id.mCv);
        ViewSizeChange.setCameraPassFaceSize(mCv);
        mIvLogo = (ImageView) findViewById(R.id.mIvLogo);
        //添加手机触摸，更新logo得功能
        mIvLogo.setOnClickListener(this);
        view_click = (View) findViewById(R.id.view_click);
        view_click.setOnClickListener(this);
        mFrv = (FaceCircleViewNew) findViewById(R.id.mFrv);
        waitDialogUtil = new WaitDialogUtil(SimpleHealthCodeActivity.this);

        facePassParsener = new SimpleHealthCodeParsener(SimpleHealthCodeActivity.this, this, mVM);
        facePassParsener.updateBindQrCode();
        initObserve();
        taskPlayerParsener = new TaskPlayerParsener(SimpleHealthCodeActivity.this, this);
    }

    //初始化监听
    private void initObserve() {
        CommondViewModel.getInstance().getMFaceInitState().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean it) {
                if (it) {
                    mFlNoInit.setVisibility(View.GONE);
                } else {
                    mFlNoInit.setVisibility(View.VISIBLE);
                }
            }
        });

        CommondViewModel.getInstance().getMNetState().observe(SimpleHealthCodeActivity.this, new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean aBoolean) {
                updateTopViewTime();
            }
        });
        mVM.getMSleepState().observe(SimpleHealthCodeActivity.this, new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean it) {
                LogUtil.e("=======mSleepState change: " + it);
                mFlSleep.setVisibility(it ? View.VISIBLE : View.GONE);
                if (it) {
                    if (mQrDialog != null) {
                        mQrDialog.dismissQrDialog();
                    }
                    Intent intent = new Intent();
                    intent.setAction(AppInfo.RECEIVE_MESSAGE_TO_REQUEST_TASK);
                    intent.putExtra(AppInfo.RECEIVE_MESSAGE_TO_REQUEST_TASK, "旷视：initObserve");
                    sendBroadcast(intent);
                } else {
                    taskPlayerParsener.clearTaskView();
                }
            }
        });

        mVM.getMTipsInfo().observe(SimpleHealthCodeActivity.this, it -> {
            LogUtil.e("=======changeTip: " + it.toString());
            mTvTemp.setText(it.getTemp());
            mTvTemp.setTextColor(getResources().getColor(it.getTempColor()));
            mTvInfo.setText(it.getText());
            mTvInfo.setBackgroundResource(it.getBgColor());
            if (!mScalAniming) {
                mTvInfo.startAnimation(mScalAnim);
            }
        });

        mVM.getMLazyRestFace().observe(SimpleHealthCodeActivity.this, it -> lazyResetFace(it));

        mVM.getMLazyRestTip().observe(SimpleHealthCodeActivity.this, aLong -> lazyResetTip(aLong));

        mVM.getMQrCodeState().observe(SimpleHealthCodeActivity.this, aLong -> mQrDialog.showByText());

        mVM.getMCheckHealthCode().observe(SimpleHealthCodeActivity.this, aLong -> {
            showHealthyTipsCode(aLong);
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.view_click:
                facePassParsener.hiddleSleepView();
                if (!getMHasPerson())
                    facePassParsener.showSleepView();
                break;
            case R.id.mBtnSettings:
                goSettings();
                break;
            case R.id.mIvLogo:
                sendBroadcast(new Intent(AppInfo.CHECK_MAIN_VIEW_LOGO));
                break;
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        updateInfoShow();
        mVM.setMProcessing(false);
        facePassParsener.mSleepTime = SpUtils.getSleepTime() * 1000;
        mVM.setMTempTime(SpUtils.getmTempTime());
        mVM.setMTempEnable(SpUtils.getmTempType() != TempModelEntity.CLOSE);
        if (SpUtils.getQrCodeEnable() || SpUtils.getShowAskErCode()) {
            mVM.setMQrCodeEnable(true);
        } else {
            mVM.setMQrCodeEnable(false);
        }
        mVM.setMAttendEnable(SpUtils.getAttendEnable());
        setMOnlyCheck(true);
        setMFaceRectMir(SpUtils.getmFaceRect());
        mKeyActionTime = System.currentTimeMillis();
        setMMaskEnable(SpUtils.getmMaskEnable());
        RxBus.get().register(this);
        resetTip();
        resetFace();
    }

    private void initHasPerson() {
        if (getMHasPerson()) {
            facePassParsener.hiddleSleepView();
            return;
        }
        hideCheckCode();
        qrCodeType = "0";
        facePassParsener.showSleepView();
    }

    @Override
    protected void onResume() {
        super.onResume();
        initHasPerson();
        updateTopViewTime();
        if (facePassParsener != null) {
            facePassParsener.updateDevInfoToWeb();
            facePassParsener.updateLeftLogoImage(null);
        }
        updateSaveRotate();
        updateDevInfo();
    }

    private void updateSaveRotate() {
        if (SpUtils.getSaveRotate() == 0) {
            mSaveRotate = getMRectRotation();
        } else {
            switch (SpUtils.getSaveRotate()) {
                case 1:
                    mSaveRotate = 0;
                    break;
                case 2:
                    mSaveRotate = 90;
                    break;
                case 3:
                    mSaveRotate = 180;
                    break;
                default:
                    mSaveRotate = 270;
                    break;
            }
        }
        mVM.setMSaveRotate(mSaveRotate);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    private void updateDevInfo() {
        boolean isShowTemp = SpUtils.getShowTempIcon();
        iv_temp_show.setVisibility(isShowTemp ? View.VISIBLE : View.GONE);
        mBtnSettings.setBackgroundResource(R.mipmap.icon_setting);
    }

    @Override
    protected void onStop() {
        super.onStop();
        showWaitDialog(false);
        facePassParsener.onStopParsener();
        taskPlayerParsener.clearTaskView();
        setMHasPerson(false);
        if (mHandler != null) {
            mHandler.removeMessages(MSG_RESET_IMAGE);
        }
        TtsManager.getInstance().setSpeechListener(null);
        try {
            stopLazy(TAG_RESET_TIP);
            RxBus.get().unregister(this);
            TempManager.getInstance().setTempCallBack(null);
            LightManager.getInstance().closeAllLight();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (viewModel != null) {
            viewModel.closePort();
        }
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
        try {
            if (receiver != null) {
                unregisterReceiver(receiver);
            }
            if (mVM != null) {
                mVM.destory();
            }
            ActivityCollector.removeActivity(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    MainViewModel viewModel;
    String port = "/dev/ttyACM0";

    private void initHealthCode() {
        viewModel = new MainViewModel(SimpleHealthCodeActivity.this);
        viewModel.setMessageListener(messageListener);
        viewModel.initHealthyCodeManager(port);
    }

    private HealthyMessageListener messageListener = new HealthyMessageListener() {
        @Override
        public void initHealthyStatues(boolean isSuccess, int code) {
            prependMsg("初始化状态: " + isSuccess + " / " + code + "/ ");
            if (!isSuccess) {
                showToastView("初始化失败：" + code);
            }
        }

        @Override
        public void backMessageToView(MessageEvent messageEvent) {
            Log.e("MAIN mode", messageEvent.getMessage() + "  " + messageEvent.getMsgClass() + " / " + Thread.currentThread());
            switch (messageEvent.getMsgClass()) {
                case 5:
                    showWaitDialog(false);
                    if (messageEvent != null) {
                        showToastView(messageEvent.getMessage());
                    }
                    break;
                case 0: //执行状态
                    Log.e("执行状态", messageEvent.getMessage());
                    prependMsg(messageEvent.getMessage());
                    break;
                case 1: //展示健康结果
                    showWaitDialog(false);
                    fl_code_result.setVisibility(View.VISIBLE);
                    String msg = messageEvent.getMessage();
//                    prependMsg("核验结果(简版)：" + MessageDealUtil.VerificationResult(msg) + " / " + Thread.currentThread());
                    prependMsg("核验结果（较全版）：" + MessageDealUtil.VerificationResultAll(msg).toString());
                    HealthyPersonEntity healthyPersonEntity = MessageDealUtil.VerificationResultAll(msg);
                    if (healthyPersonEntity == null) {
                        showToastView("获取健康码失败！");
                        return;
                    }
                    boolean isSuccess = healthyPersonEntity.isParsenerStatues();
                    if (!isSuccess) {
                        showToastView("获取健康码失败：" + healthyPersonEntity.getErrorDesc());
                        return;
                    }
                    String hsjcInfo = healthyPersonEntity.getHSJC();
                    if (TextUtils.isEmpty(hsjcInfo)) {
                        tv_hsjc_healthy.setText("未查询到核酸信息");
                    } else {
                        tv_hsjc_healthy.setText(hsjcInfo);
                    }
                    tv_name_healthy.setText(healthyPersonEntity.getName());
                    tv_idcard_healthy.setText(healthyPersonEntity.getIDNum());
                    tv_date_healthy.setText(SimpleDateUtil.formatCurrentTimeMin());
                    switch (healthyPersonEntity.getStatus()) {
                        case "00":
                            qrCodeType = "1";
                            iv_qr_healthy.setImageResource(R.mipmap.qrcode_greem);
                            tv_result_healthy.setText("请通行");
                            tv_status_healthy.setText("绿码: 低风险");
                            AcsService.getInstance().startSpeakTts("绿码，请通行", "");
                            break;
                        case "01":
                            qrCodeType = "2";
                            iv_qr_healthy.setImageResource(R.mipmap.qrcode_yellow);
                            tv_result_healthy.setText("请等待");
                            tv_status_healthy.setText("黄码: 中风险");
                            AcsService.getInstance().startSpeakTts("黄码，请等待", "");
                            break;
                        case "10":
                            qrCodeType = "3";
                            iv_qr_healthy.setImageResource(R.mipmap.qrcode_red);
                            tv_result_healthy.setText("禁止通行");
                            tv_status_healthy.setText("红码: 高风险");
                            AcsService.getInstance().startSpeakTts("红码，禁止通行", "");
                            break;
                    }
                    LogUtil.update("准备上传素材信息==000+" + imagePathUpdate + " / " + tempUpdate + " / " + wearMaskCurrent + " / " + qrCodeType);

                    mVM.saveAccessFaceRecord(
                            SimpleHealthCodeActivity.this,
                            imagePathUpdate,
                            null,
                            tempUpdate,
                            wearMaskCurrent,
                            qrCodeType);
                    break;
            }
        }
    };

    //扫码后回调入口
    @org.greenrobot.eventbus.Subscribe(threadMode = ThreadMode.MAIN)
    public void onQrCodeScannedEvent(CtidQRCodeScannedEvent event) {
        if (!isShowHealthyDialog) {
            showToastView("请测温，在扫码");
            AcsService.getInstance().startSpeakTts("请测温，在扫码", "");
            return;
        }
        prependMsg("=====onQrCodeScannedEvent==" + Thread.currentThread());
        if (!NetWorkUtils.isNetworkConnected(SimpleHealthCodeActivity.this)) {
            showToastView("网络异常，请检查!");
            return;
        }
        String qrCodeValue = DeviceManager.getInstance().getCtidQRCode().getRawValue();
        if (TextUtils.isEmpty(qrCodeValue)) {
            return;
        }
        int type = DeviceManager.getInstance().getQrType(qrCodeValue);
        if (type == 1) {
            JSONObject checkData = null;
            try {
                checkData = DeviceManager.getInstance().getCheckData();
            } catch (Exception e) {
                e.printStackTrace();
            }
            showWaitDialog(true);
            viewModel.requestHealthByQrCode(checkData, "36", "深圳市高新中一道");
            return;
        }
        showWaitDialog(true);
        viewModel.requestQrValidate(qrCodeValue);
    }

    //界面报文显示
    private void prependMsg(final String msg) {
        Log.i("tempHealthy", msg);
    }

    private void lazyResetTip(long time) {
        mVM.delayed(time, TAG_RESET_TIP, new Function0<Unit>() {
            @Override
            public Unit invoke() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        resetTip();
                    }
                });
                return null;
            }
        });
    }

    private void resetTip() {
        LogUtil.e("=======changeTip: resetTip");
        stopLazy(TAG_RESET_FACE);
        mVM.setMProcessing(false);
        mVM.changeTipsInfo(getString(R.string.stand_in_box), R.drawable.bg_tip_blue, "", R.color.green);
        LogUtil.e("=====faceCenter onHasFaceChange resetTip");
        resetFace();
    }

    private void lazyResetFace(long time) {
        stopLazy(TAG_RESET_FACE);
        mVM.delayed(time, TAG_RESET_FACE, new Function0<Unit>() {
            @Override
            public Unit invoke() {
                mVM.setMProcessing(false);
                resetFace();
                return null;
            }
        });
    }

    private void stopLazy(String tag) {
        if (mVM != null) {
            mVM.stopDelayed(tag);
        }
    }

    public void showToastView(String message) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                MyToastView.getInstance().Toast(SimpleHealthCodeActivity.this, message);
            }
        });

    }

    private void goSettings() {
        EditTextDialog editTextDialog = new EditTextDialog(this, true);
        editTextDialog.setOnDialogClickListener(new EditTextDialog.EditTextDialogListener() {
            @Override
            public void commit(String it) {
                if (it.isEmpty()) {
                    showToastView(getString(R.string.pwd_is_null));
                    return;
                }
                String password = SpUtils.getSettingPassword();
                //这里内置两个密码，默认123456，778899是给售后用的。请须知
                if (it.equals("778899") || it.equals(password)) {
                    startActivity(new Intent(SimpleHealthCodeActivity.this, SettingMenuActivity.class));
                } else {
                    showToastView(getString(R.string.pwd_failed_retry));
                }
            }

            @Override
            public void hiddleTestClick() {
                startActivity(new Intent(SimpleHealthCodeActivity.this, SettingMenuActivity.class));
            }
        });

        editTextDialog.show(
                getString(R.string.pwd_verif), "", getString(R.string.confirm), getString(
                        R.string.input_verif_pwd
                )
        );
    }

    /**
     * Socket服务器连接状态变更
     */
    @Subscribe(thread = EventThread.MAIN_THREAD)
    public void linkServerEvent(LinkServerEvent event) {
        updateTopViewTime();
        facePassParsener.updateDevInfoToWeb();
    }

    private void updateTopViewTime() {
        facePassParsener.updateTopViewTime();
    }

    private void updateInfoShow() {
        mLlInfo.setVisibility(SpUtils.getInfoEnable() ? View.VISIBLE : View.INVISIBLE);
//        mLlInfo.setVisibility(View.GONE);
        mIvNet.setVisibility(SpUtils.getHideWifiIcon() ? View.GONE : View.VISIBLE);
        mIvServer.setVisibility(SpUtils.getHideWifiIcon() ? View.GONE : View.VISIBLE);
    }

    private void initFaceConfig() {
        int cameraCount = CameraProvider.getCamerasCount(this);
        LogUtil.e("========获取相机个数: " + cameraCount);
        int currentWidth = SpUtils.getResolutionWidth();
        int currentHeight = SpUtils.getResolutionHeight();
        if (currentWidth == 0 || currentHeight == 0) {
            setMCustomSize(null);
        } else {
            setMCustomSize(new Point(currentWidth, currentHeight));
        }
        //选择是单目还是双目
        setMIsDoubleCamera(SpUtils.getAutoSwitchCamera());
        int prewRoatation = 0;
        switch (SpUtils.getPrewRotate()) {
            case 0:
                prewRoatation = 0;
                break;
            case 1:
                prewRoatation = 90;
                break;
            case 2:
                prewRoatation = 180;
                break;
            default:
                prewRoatation = 270;
                break;
        }
        setMPrewRotation(prewRoatation);

        int rectRoatation = 0;
        switch (SpUtils.getRectRotate()) {
            case 1:
                rectRoatation = 0;
                break;
            case 2:
                rectRoatation = 90;
                break;
            case 3:
                rectRoatation = 180;
                break;
            case 4:
                rectRoatation = 270;
                break;
            default:
                rectRoatation = FaceManager.getInstance().getMFaceRotation();
                break;
        }
        setMRectRotation(rectRoatation);
        setMIsFront(SpUtils.getCameraFacing());
        mFrv.setScreenWidth(SpUtils.getScreenwidth(), SpUtils.getScreenheight(), SpUtils.getShowFaceRectView());

        mFrv.setFaceRectDisListener(new FaceRecDisListener() {
            @Override
            public void rectDistance(float distance, boolean isCenter, float rectWidth, Point headPoint) {
                mVM.setMRectDistance((int) distance);
                TempManager.getInstance().mRectDistance = (int) distance;
                TempManager.getInstance().headPoint = headPoint;
                mVM.setMIsFaceCenter(true);
            }
        });
    }


    //==============不涉及到业务逻辑的代码===========================================================================================================

    private void initReceiver() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(Intent.ACTION_TIME_TICK);      //时间变化
        filter.addAction(AppInfo.DOWN_TASK_SUCCESS); //广告下发
        filter.addAction(AppInfo.UPDATE_MAIN_BOTTOM_VIEW);
        registerReceiver(receiver, filter);
    }


    @Override
    public ImageView getQrBindImageView() {
        return mDevQrCode;
    }

    @Override
    public TextView getTvAppVersion() {
        return mTvVersion;
    }

    @Override
    public TextView getTvShowTime() {
        return mTvTime;
    }

    @Override
    public TextView getTvCompany() {
        return mTvCompany;
    }

    @Override
    public TextView getTvDep() {
        return mTvDep;
    }

    @Override
    public AbsoluteLayout getAbView() {
        return mAdView;
    }


    @Override
    public TextView getTvDevName() {
        return mTvDevName;
    }

    @Override
    public TextView getTvMac() {
        return tv_dev_mac;
    }

    @Override
    public ImageView getIvLogo() {
        return mIvLogo;
    }

    @Override
    public ImageView getIvServer() {
        return mIvServer;
    }

    @Override
    public ImageView getIvNet() {
        return mIvNet;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            facePassParsener.hiddleSleepView();
            goSettings();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @NotNull
    @Override
    public CameraPreview getCameraView() {
        return mCv;
    }

    @Override
    public CameraPreview getIrCameraView() {
        return mIRCv;
    }

    @Override
    public FaceCircleViewNew getFaceRectView() {
        return mFrv;
    }


    @Override
    public void onHasFaceChange(boolean hasFace) {
        if (hasFace) {
//            mVM.setMProcessing(false);
            mVM.setMStrangerTime(0);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    facePassParsener.hiddleSleepView();
                }
            });
            LightManager.getInstance().openWhiteLight();
            return;
        }
        isFaceBack = false;
        isShowHealthyDialog = false;
        fl_code_result.setVisibility(View.GONE);
        qrCodeType = "0";
        hideCheckCode();
        if (mQrDialog != null) {
            mQrDialog.dismissQrDialog();
        }
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                showWaitDialog(false);
            }
        });
        mVM.removeDelayed(FaceViewModel.TAG_HANDLER_FACE);
        facePassParsener.showSleepView();
        lazyResetTip(800);
    }


    //动态获取相机分辨率
    @Override
    public void onSupportedPreviewSizes(@NotNull List<Point> previewsSizes) {
        super.onSupportedPreviewSizes(previewsSizes);
        DataRepository.getInstance().setMSupportedPreview(previewsSizes);
    }

    /**
     * [faceToken] 人脸识别结果
     * [noMask] 未佩戴口罩为true,否则为false
     */
    @Override
    public void recognizeResult(@NotNull String faceToken, boolean noMask, FacePassImage image) {
//        true 未佩戴    false 佩戴口罩
        if (noMask) {
            wearMaskCurrent = AppInfo.MAST_WAER_DEFAULT;
        } else {
            wearMaskCurrent = AppInfo.MAST_WAER_RIGHT;
        }
        LogUtil.e("========recognizeResult： " + faceToken + " / " + mVM.getMProcessing() + " /noMask =" + noMask);
        if (!getMHasPerson()) {
            LogUtil.e("========recognizeResult：000 no has person");
            resetFace();
            return;
        }
        LogUtil.e("========recognizeResult： 1111");
        mVM.handlerFaceResult(faceToken, noMask, image == null ? null : new MinFaceData(image.image, image.width, image.height, image.facePassImageRotation));
    }

    boolean isFaceBack = false;
    float tempUpdate = 0.0f;

    @Override
    public void cameraPreview(byte[] nv21Data, int width, int height, int rotation) {
        if (nv21Data == null) {
            return;
        }
        if (!isFaceBack) {
            return;
        }
        ToolUtils.roatetranBitmap(nv21Data, width, height, mSaveRotate, new RoateImageListener() {

            @Override
            public void backImageListener(boolean isSuccess, String imagePath) {
                if (!isSuccess) {
                    return;
                }
                imagePathUpdate = imagePath;
                if (iv_face_healthy != null) {
                    LogUtil.e("=====================>roatetranBitmap bitmap,显示人脸头像---->" + imagePath);
                    Glide.with(SimpleHealthCodeActivity.this).load(imagePath).into(iv_face_healthy);
                }
            }
        });
        isFaceBack = false;
    }

    private void showWaitDialog(boolean isShow) {
        if (waitDialogUtil == null) {
            return;
        }
        if (isShow) {
            waitDialogUtil.show("Loading...");
        } else {
            waitDialogUtil.dismiss();
        }
    }

    boolean isShowHealthyDialog = false;
    String imagePathUpdate = "";
    String wearMaskCurrent = AppInfo.MAST_WAER_DEFAULT;

    /***
     * 请出示健康码
     */
    private void showHealthyTipsCode(Float temp) {
        tempUpdate = temp;
        tv_temp_healthy.setText(temp + " ℃");
        isFaceBack = true;
        AcsService.getInstance().startSpeakTts(getString(R.string.please_healthcode), "");
        isShowHealthyDialog = true;
        mTvInfo.setText("请出示健康码");
        mTvInfo.setBackgroundResource(R.drawable.bg_tip_blue);
        if (!mScalAniming) {
            mTvInfo.startAnimation(mScalAnim);
        }
    }

    private void hideCheckCode() {
        resetTip();
    }

}
