package com.ys.acsdev.ui.healthcodeBack.healthcode;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.signature.ObjectKey;
import com.ys.acsdev.BuildConfig;
import com.ys.acsdev.R;
import com.ys.acsdev.bean.DeviceBean;
import com.ys.acsdev.commond.AppInfo;
import com.ys.acsdev.db.DbTempCompensate;
import com.ys.acsdev.manager.ImageManager;
import com.ys.acsdev.service.AcsService;
import com.ys.acsdev.service.SocketService;
import com.ys.acsdev.ui.face.view.FacePassView;
import com.ys.acsdev.ui.healthcode.SimpleHealthCodeModel;
import com.ys.acsdev.ui.healthcode.SimpleHealthIdModel;
import com.ys.acsdev.util.CodeUtil;
import com.ys.acsdev.util.LogUtil;
import com.ys.acsdev.util.NetWorkUtils;
import com.ys.acsdev.util.SimpleDateUtil;
import com.ys.acsdev.util.SpUtils;
import com.ys.qrcodelib.QRCodeUtil;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import java.io.File;

import okhttp3.Call;

public class SimpleHealthIdParsener {

    Context context;
    FacePassView facePassView;
    SimpleHealthIdModel mVm;
    public long mSleepTime = 30 * 1000;

    public SimpleHealthIdParsener(Context context, FacePassView facePassView, SimpleHealthIdModel viewModel) {
        this.context = context;
        this.facePassView = facePassView;
        this.mVm = viewModel;
        getView();
    }

    TextView mTvVersion, mTvTime;
    ImageView mDevQrCode;

    private void getView() {
        mTvVersion = facePassView.getTvAppVersion();
        mTvTime = facePassView.getTvShowTime();
        mDevQrCode = facePassView.getQrBindImageView();
    }

    private static final int MSG_SLEEP = 2;
    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            removeMessages(msg.what);
            switch (msg.what) {
                case MSG_SLEEP:
                    LogUtil.e("========onHasFaceChange： handler showSleepView");
                    if (mVm != null) {
                        mVm.getMSleepState().postValue(true);
                    }
                    break;
            }
        }
    };

    /***
     * 跟新设备信息到服务器
     */
    public void updateDevInfoToWeb() {
        if (!NetWorkUtils.isNetworkConnected(context)) {
            return;
        }
        String ipAddress = NetWorkUtils.getIPAddress(context);
        String sn = CodeUtil.getUniquePsuedoID();
        String requestUrl = AppInfo.getUpdateEquipmentBySn();
        String softVersion = CodeUtil.getVersionAll(context);
        OkHttpUtils
                .post()
                .url(requestUrl)
                .addParams("sn", sn)
                .addParams("ip", ipAddress + "")
                .addParams("softVersion", softVersion + "")
                .addParams("faceState", "1")
                .addParams("snapState", "1")
                .addParams("camerasNumber", "1")
                .addParams("keypadState", "1")
                .addParams("newworkType", "1")
                .addParams("longitude", "")
                .addParams("latitude", "")
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, String e, int id) {
                        LogUtil.cdl("====提交设备信息==onError==" + e.toString());
                    }

                    @Override
                    public void onResponse(String response, int id) {
                        LogUtil.cdl("====提交设备信息==success==" + response);
                    }
                });
    }

    /***
     * 显示休眠界面
     */
    public void showSleepView() {
        LogUtil.e("========onHasFaceChange： parsener showSleepView");
        handler.removeMessages(MSG_SLEEP);
        handler.sendEmptyMessageDelayed(MSG_SLEEP, mSleepTime);
    }

    public void hiddleSleepView() {
        handler.removeMessages(MSG_SLEEP);
        if (mVm != null) {
            mVm.getMSleepState().postValue(false);
        }
    }

    /***
     * 跟新状态信息
     */
    public void updateTopViewTime() {
        if (mTvTime != null) {
            String showTime = SimpleDateUtil.getCurrentTimeJSTHm();
            LogUtil.cdl("=======显示得温度===" + showTime);
            mTvTime.setText(showTime + "");
        }
        String appVersion = BuildConfig.VERSION_NAME + "_" + BuildConfig.VERSION_CODE;
        if (mTvVersion != null) {
            mTvVersion.setText(appVersion);
        }
        //跟新温度补偿值
        DbTempCompensate.getCurrentTempAdd();
        //每个小时跟新信息到服务器一次
        long currentTime = SimpleDateUtil.getCurrentHourMin();
        if (currentTime % 100 == 0) {
            updateDevInfoToWeb();
        }
    }

    public void onStopParsener() {
        handler.removeMessages(MSG_SLEEP);
    }

    public void updateBindQrCode() {
        if (mDevQrCode == null) {
            return;
        }
        String devData = "{\"type\":\"addDev\",\"mac\": \"" + CodeUtil.getUniquePsuedoID() + "\"}";
        String qrCodePath = AppInfo.QR_BIND_PATH;
        QRCodeUtil runnable = new QRCodeUtil(devData, qrCodePath, new QRCodeUtil.ErCodeBackListener() {

            @Override
            public void createErCodeState(String errorDes, boolean isCreate, String path) {
                if (!isCreate) {
                    return;
                }
                ImageManager.displayImagePath(path, mDevQrCode);
            }
        });
        AcsService.getInstance().executor(runnable);
    }

}
