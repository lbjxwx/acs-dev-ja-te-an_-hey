package com.ys.acsdev.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.WindowManager;

import androidx.appcompat.app.AppCompatActivity;

import com.ys.acsdev.R;
import com.ys.acsdev.db.DbTempCompensate;
import com.ys.acsdev.service.AcsService;
import com.ys.acsdev.service.FaceCheckDownService;
import com.ys.acsdev.util.ActivityCollector;
import com.ys.acsdev.util.SystemManagerUtil;
import com.ys.acsdev.view.MyToastView;
import com.ys.acsdev.view.WaitDialogUtil;
import com.ys.acsdev.view.dialog.OridinryDialog;
import com.ys.acsdev.view.dialog.OridinryDialogClick;


public class BaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        initBase();
    }

    private void initBase() {
        ActivityCollector.addActivity(this);
        DbTempCompensate.getCurrentTempAdd();
    }

    @Override
    protected void onResume() {
        super.onResume();
        startService(new Intent(BaseActivity.this, AcsService.class));
        startService(new Intent(BaseActivity.this, FaceCheckDownService.class));
    }

    /****
     * 重启弹窗
     */
    public void showRebootDialog() {
        OridinryDialog oridinryDialog = new OridinryDialog(BaseActivity.this);
        oridinryDialog.setOnDialogClickListener(new OridinryDialogClick() {
            @Override
            public void sure() {
                SystemManagerUtil.rebootApp(BaseActivity.this);
            }

            @Override
            public void noSure() {

            }
        });
        oridinryDialog.show(getString(R.string.reboot_msg), getString(R.string.reboot), getString(R.string.cancel));
    }

    WaitDialogUtil mWaitDialog;

    public void showToast(String msg) {
        MyToastView.getInstance().Toast(this, msg);
    }

    public void showWait(String msg) {
        if (mWaitDialog == null) {
            mWaitDialog = new WaitDialogUtil(BaseActivity.this);
        }
        mWaitDialog.show(msg);
    }

    public void hideWait() {
        if (mWaitDialog != null) {
            mWaitDialog.dismiss();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ActivityCollector.removeActivity(this);
    }
}
