package com.ys.acsdev.ui.sdcheck;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;

import androidx.appcompat.app.AppCompatActivity;

import com.ys.acsdev.R;
import com.ys.acsdev.commond.AppInfo;
import com.ys.acsdev.util.FileUtils;
import com.ys.acsdev.util.LogUtil;
import com.ys.acsdev.util.SpUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class SdCheckActivity extends AppCompatActivity implements SdCheckView {

    @Override
    protected void onCreate(Bundle paramBundle) {
        super.onCreate(paramBundle);
        setContentView(R.layout.activity_sd_check);
        initView();
        getDataInfo();
    }

    String checkUrl;
    public static final String TAG_CHECK_URL = "TAG_CHECK_URL";
    ListView lv_sd_info;
    SdCheckAdapter adapter;
    List<SdCheckEntity> list = new ArrayList<>();
    ProgressBar pb_show;
    SdCheckParsener sdCheckParsener;

    private void initView() {
        Intent intent = getIntent();
        checkUrl = intent.getStringExtra(TAG_CHECK_URL);
        lv_sd_info = (ListView) findViewById(R.id.lv_sd_info);
        pb_show = (ProgressBar) findViewById(R.id.pb_show);
        adapter = new SdCheckAdapter(SdCheckActivity.this, list);
        lv_sd_info.setAdapter(adapter);
        sdCheckParsener = new SdCheckParsener(SdCheckActivity.this, this);
    }

    private void getDataInfo() {
        if (checkUrl == null || checkUrl.length() < 5) {
            LogUtil.sdcard("升级文件路径不存在", true);
            setThreeClose(getString(R.string.file_path_no_exist));
            return;
        }
        if (checkUrl.contains(".apk")) {
            addInfoToList("User Action: Update YsFace Apk");
            sdCheckParsener.writeApkToLocal(checkUrl);
            return;
        } else if (checkUrl.contains(AppInfo.USB_APK_MODIFY_IP)) {
            //修改当前 IP
            SpUtils.setWokModel(AppInfo.MODEL_STAND_ALONE);
            addInfoToList("User Action: Modify Local Company and Department");
            String jsonText = FileUtils.getTxtInfoFromTxtFile(checkUrl);
            LogUtil.cdl("====jsonText===" + jsonText);
            String basePath = checkUrl.substring(0, checkUrl.lastIndexOf("/"));
            sdCheckParsener.parsenerJsonInfo(jsonText, basePath);
            return;
        } else if (checkUrl.contains(AppInfo.SINGLE_TASK_DIR)) {
            //离线广告信息展示
            addInfoToList("User Action: Offline Single Machine Task");
        } else if (checkUrl.contains(AppInfo.BOOTANIMAL)) {
            addInfoToList("User Action: Ueplace boot animal");
            //替换开机动画
            sdCheckParsener.writeBootAnimalToLocal(checkUrl);
        } else if (checkUrl.contains(AppInfo.FACE_REGISTER_DIR)) {
            LocalUPanRegister localFaceRegister = new LocalUPanRegister(getApplication());
            localFaceRegister.getRegLiveData().observe(SdCheckActivity.this, faceReg -> {
                if (faceReg.state == FaceReg.START) {
                    addInfoToList("开始注册人员: " + faceReg.fileName);
                }
                if (faceReg.state == FaceReg.SUCCESS) {
                    addInfoToList(faceReg.fileName + " 注册成功");
                }
                if (faceReg.state == FaceReg.ERROR) {
                    addInfoToList(faceReg.fileName + " 注册失败");
                }
                if (faceReg.state == FaceReg.COMPLETE) {
                    addInfoToList("所有人员已经注册完成");
                    setThreeClose("");
                }
            });
            LogUtil.sdcard("aaaaaaaaaa--" + checkUrl, true);
            addInfoToList("User Action: register local face for image file");
            localFaceRegister.registerFace(new File(checkUrl));
        }
    }

    @Override
    public void addInfoToList(String info) {
        addInfoToList(info, false);
    }

    public void addInfoToList(String info, boolean isRed) {
        list.add(new SdCheckEntity(info, list.size() + 1 + "", isRed));
        adapter.setList(list);
    }

    @Override
    public void setThreeClose(String desc) {
        addInfoToList(desc + getString(R.string.lazy_close), true);
        handler.sendEmptyMessageDelayed(FINISH_MY_SELF, FINISH_TIME_DISTANCE);
    }

    @Override
    public void updateWriteProgress(int progress) {
        pb_show.setProgress(progress);
    }

    private static final int FINISH_MY_SELF = 89;
    private static final int FINISH_TIME_DISTANCE = 5000;
    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case FINISH_MY_SELF:
                    finish();
                    break;
            }
        }
    };


}
