package com.ys.acsdev.ui;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.tbruyelle.rxpermissions2.RxPermissions;
import com.ys.acsdev.R;
import com.ys.acsdev.util.APKUtil;
import com.ys.acsdev.util.LogUtil;
import com.ys.acsdev.util.RootCmd;
import com.ys.acsdev.util.SpUtils;
import com.ys.acsdev.view.dialog.OridinryDialog;
import com.ys.acsdev.view.dialog.OridinryDialogClick;

public class StartActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
        checkAppIsSafe();

        /*String qrCodeValue = "ZDSQM-WyoAH8PDA5UHUHtdAkDWmSy+S0il5Id21GmQE8F/7Nqz4w/ejM5igHHiFmyoQNFFu/tvGFBQXjxc\n48/G8P3cIDPgjET2IEk3WkPRfSLphH8EtU7FHcD9ddgHjzTrog51";
        try {
            String authCode = CommonUtil.getAuthCode(qrCodeValue.substring(6));
            System.out.println("aaaaaa  " + authCode);
        } catch (Exception e) {
            e.printStackTrace();
        }*/
        /*JSONObject json = new JSONObject();
        json.put("adfd", "adfdf");
        json.put("dsfaadfd", "dsfsdf");
        json.put("devSetting", SpUtils.getDevConfigInfo());
        System.out.println(json.toString());*/
    }

    /***
     * 检查主板得得合法性
     * 不是我们公司得主板，不让使用
     */
    private void checkAppIsSafe() {
        String powerOnOffPackageName = "com.adtv";  //定时开关机
        boolean isInstall = APKUtil.ApkState(StartActivity.this, powerOnOffPackageName);
        if (!isInstall) {
            showCpuErrorDialog();
            return;
        }
        initView();
        checkPermission();
    }

    private void initView() {
//        MyManager.getInstance(this).setSlideShowNavBar(SpUtils.INSTANCE.getMMenuBarEnable());
//        MyManager.getInstance(this).setSlideShowNotificationBar(SpUtils.INSTANCE.getMShowNavEnable());
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private void checkPermission() {
        RxPermissions rxPermissions = new RxPermissions(StartActivity.this);
        rxPermissions.request(Manifest.permission.CAMERA,
                Manifest.permission.READ_PHONE_STATE,
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.ACCESS_NETWORK_STATE,
                Manifest.permission.ACCESS_WIFI_STATE,
                Manifest.permission.CALL_PHONE,
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.CHANGE_NETWORK_STATE)
                .subscribe(aBoolean -> {
                    if (aBoolean) {
                        LogUtil.e("====================startActivity: 已授权限");
                        initData();
                        goToSplashActivity();
                    } else {
                        LogUtil.e("====================startActivity: 未授权限");
                        Toast.makeText(this, R.string.no_permission, Toast.LENGTH_SHORT).show();
                        finish();
                    }
                });
    }


    private void initData() {
        int frontImage = SpUtils.getFrontImage();
        int backImage = SpUtils.getBackImage();
        if (frontImage != 0) {
            RootCmd.setProperty(RootCmd.CAMERA_FONR_MIRROR, frontImage == 1 ? "true" : "false");
        }
        if (backImage != 0) {
            RootCmd.setProperty(RootCmd.CAMERA_BACK_MIRROR, backImage == 1 ? "true" : "false");
        }
    }

    private void goToSplashActivity() {
        LogUtil.cdl("=============软件起来了=======startActivity==", true);
//        startActivity(new Intent(StartActivity.this, HealthyActivity.class));
        startActivity(new Intent(StartActivity.this, SplashActivity.class));
        finish();
    }

    /**
     * 提示用户
     */
    private void showCpuErrorDialog() {
        OridinryDialog oridinryDialog = new OridinryDialog(StartActivity.this);
        oridinryDialog.setOnDialogClickListener(new OridinryDialogClick() {
            @Override
            public void sure() {
                finish();
            }

            @Override
            public void noSure() {
                finish();
            }
        });
        oridinryDialog.show("设备未授权,请联系厂家售后 !", "关闭", "关闭");
    }

}
