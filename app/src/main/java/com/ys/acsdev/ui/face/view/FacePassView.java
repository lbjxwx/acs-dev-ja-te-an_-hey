package com.ys.acsdev.ui.face.view;

import android.widget.ImageView;
import android.widget.TextView;

public interface FacePassView {

    ImageView getQrBindImageView();

    TextView getTvAppVersion();

    TextView getTvShowTime();

    TextView getTvCompany();

    TextView getTvDep();

    TextView getTvDevName();

    TextView getTvMac();

    ImageView getIvLogo();

    ImageView getIvServer();

    ImageView getIvNet();
//    ImageView getSleepLogoView();
//    TextView getTvTip();
}
