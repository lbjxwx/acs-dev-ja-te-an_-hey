package com.ys.acsdev.ui;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ys.acsdev.R;
import com.ys.acsdev.commond.AppInfo;
import com.ys.acsdev.service.AcsService;
import com.ys.acsdev.service.SocketService;
import com.ys.acsdev.ui.compare.FaceCompareActivity;
import com.ys.acsdev.ui.face.FacePassIrJaveActivity;
import com.ys.acsdev.ui.healthcode.SimpleHealthCodeActivity;
import com.ys.acsdev.ui.healthcodeBack.healthcode.SimpleHealthIdActivity;
import com.ys.acsdev.util.APKUtil;
import com.ys.acsdev.util.CodeUtil;
import com.ys.acsdev.util.FileUtils;
import com.ys.acsdev.util.InitInfoUtil;
import com.ys.acsdev.util.LogUtil;
import com.ys.acsdev.util.NetWorkUtils;
import com.ys.acsdev.util.SpUtils;
import com.ys.acsdev.util.guardian.FileRawWriteRunnable;
import com.ys.acsdev.util.guardian.GuardianUtil;
import com.ys.acsdev.util.guardian.InstallGuardianUtil;
import com.ys.acsdev.util.guardian.WriteSdListener;
import com.ys.rkapi.MyManager;

public class SplashActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        init();
    }

    TextView mTvSn;

    private void init() {
        startService(new Intent(SplashActivity.this, SocketService.class));
        startService(new Intent(SplashActivity.this, AcsService.class));
        mTvSn = (TextView) findViewById(R.id.mTvSn);
        mTvSn.setText(getString(R.string.device_no) + CodeUtil.getUniquePsuedoID());
        handler.sendEmptyMessageDelayed(MESSAGE_GO_TO_MAIN, 5 * 1000);
    }

    private static final int MESSAGE_GO_TO_MAIN = 879;
    private static final int MESSAGE_WRITE_VIDEO_TO_LOCAL = 888;
    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case MESSAGE_GO_TO_MAIN:
                    Intent intent = new Intent();
                    if (SpUtils.getHealthCodeEnable()) {
                        //健康码
                        intent.setClass(SplashActivity.this, SimpleHealthCodeActivity.class);
                    } else if (SpUtils.getCompareEnable()) {
                        // 人证比对
                        intent.setClass(SplashActivity.this, FaceCompareActivity.class);
                    } else if (SpUtils.getHealthyCompareEnable()) {
                        // 人证比对+健康码
                        intent.setClass(SplashActivity.this, SimpleHealthIdActivity.class);
                    } else { //常规旷视识别
                        intent.setClass(SplashActivity.this, FacePassIrJaveActivity.class);
                    }
                    System.out.println(intent.getComponent().getClassName());
                    startActivity(intent);
                    SpUtils.setFirstLogin(false);
                    finish();
                    break;
                case MESSAGE_WRITE_VIDEO_TO_LOCAL:
                    writeDefaultSleepFileToLocalPath();
                    break;
            }
        }
    };

    @Override
    protected void onResume() {
        super.onResume();
        InitInfoUtil.initSplashInfo();
        InitInfoUtil.getDefaultInfo(SplashActivity.this);
        installGuardian();
        if (NetWorkUtils.isNetworkConnected(this)) {
            String clVersion = CodeUtil.getSystCodeVersion(this);
            AcsService.getInstance().updateDevInfoToAuthorServer(clVersion);
        }
        MyManager manager = MyManager.getInstance(SplashActivity.this);
        manager.setSlideShowNotificationBar(false); //关闭下拉菜单
        manager.setSlideShowNavBar(false);  //关闭滑出底部菜单

        //这里延迟两秒，用来用来处理屏保视频的
        handler.sendEmptyMessageDelayed(MESSAGE_WRITE_VIDEO_TO_LOCAL, 2 * 1000);
    }

    private void writeDefaultSleepFileToLocalPath() {
        FileUtils.writeDefaultFaceImageToLocal(SplashActivity.this);
    }

    private void installGuardian() {
        InstallGuardianUtil installGuardianUtil = new InstallGuardianUtil(this);
        installGuardianUtil.startInstallGuardian();
        String packageName = "com.guardian";
        boolean isInstall = APKUtil.ApkState(SplashActivity.this, packageName);
        if (!isInstall) {
            return;
        }
        GuardianUtil.modifyGuardianPackageName(SplashActivity.this);
        GuardianUtil.modifyGuardianTime(SplashActivity.this, 60 * 1000);
        GuardianUtil.modifyAppShowChooicePackageButton(SplashActivity.this);
    }

}




