package com.ys.acsdev.ui;

import android.os.Bundle;
import android.view.View;

import com.google.gson.Gson;
import com.yisheng.facerecog.net.HttpObserver;
import com.ys.acsdev.R;
import com.ys.acsdev.api.ApiHelper;
import com.ys.acsdev.bean.RegisterError;
import com.ys.acsdev.bean.RegisterRequestBody;
import com.ys.acsdev.commond.AppInfo;
import com.ys.acsdev.listener.SeekBarBackListener;
import com.ys.acsdev.runnable.FileWriteRunnable;
import com.ys.acsdev.util.CodeUtil;
import com.ys.acsdev.util.LogUtil;
import com.ys.acsdev.util.RootCmd;
import com.ys.acsdev.util.VoiceManager;
import com.ys.acsdev.util.guardian.WriteSdListener;
import com.ys.acsdev.view.dialog.OridinryDialog;
import com.ys.acsdev.view.dialog.OridinryDialogClick;
import com.ys.acsdev.view.dialog.SeekBarDialog;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import io.reactivex.schedulers.Schedulers;

public class TestActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);
        initView();
    }

    private void initView() {
        findViewById(R.id.button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialog();
            }
        });
    }

    ArrayList<RegisterError> registerErrors = new ArrayList<>();

    private void showDialog() {
        registerErrors.add(new RegisterError("1", "1234556"));
        registerErrors.add(new RegisterError("2", "1234556"));
        registerErrors.add(new RegisterError("3", "1234556"));
        registerErrors.add(new RegisterError("4", "1234556"));
        RegisterRequestBody requestBody = new RegisterRequestBody(CodeUtil.getUniquePsuedoID(), registerErrors.toArray(new RegisterError[registerErrors.size()]));
        String json = new Gson().toJson(requestBody);
        LogUtil.persoon("==========requestBody====: " + json);
        ApiHelper.putFaceRegFail(json).subscribeOn(Schedulers.io()).observeOn(Schedulers.io()).subscribe(new HttpObserver<String>() {
            @Override
            public void onSuccess(@Nullable String s) {
                LogUtil.persoon("==========requestBody result: " + s);
            }

            @Override
            public void onFailed(int code, @NotNull String msg) {
                LogUtil.persoon("==========requestBody failed: " + code + " / " + msg);
            }
        });
    }


}
