package com.ys.acsdev.ui;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.ys.acsdev.R;
import com.ys.acsdev.attendance.dao.ClockInDao;
import com.ys.acsdev.attendance.dao.EmpScheDao;
import com.ys.acsdev.commond.AppInfo;
import com.ys.acsdev.db.dao.FaceInfoJavaDao;
import com.ys.acsdev.db.dao.LocalARDao;
import com.ys.acsdev.db.dao.PersonDao;
import com.ys.acsdev.util.FileUtils;
import com.ys.acsdev.util.LogUtil;
import com.ys.acsdev.util.SpUtils;
import com.ys.acsdev.util.SystemManagerUtil;
import com.ys.acsdev.view.dialog.OridinryDialog;
import com.ys.acsdev.view.dialog.OridinryDialogClick;

public class ModelActivity extends BaseActivity implements View.OnClickListener {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_model);
        initView();
        initListener();
    }

    RadioGroup rg_work_model;
    ImageView am_iv_back;
    Button am_btn_confirm;
    TextView am_tv_tips;

    private void initView() {
        am_iv_back = findViewById(R.id.am_iv_back);
        am_iv_back.setOnClickListener(this);
        am_btn_confirm = (Button) findViewById(R.id.am_btn_confirm);
        am_btn_confirm.setOnClickListener(this);
        am_tv_tips = (TextView) findViewById(R.id.am_tv_tips);
        rg_work_model = (RadioGroup) findViewById(R.id.rg_work_model);
    }

    private void initListener() {
        rg_work_model.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int checkId) {
                switch (checkId) {
                    case R.id.am_rb_net:
                        SpUtils.setWokModel(AppInfo.MODEL_NET);
                        break;
                    case R.id.am_rb_standalone:
                        SpUtils.setWokModel(AppInfo.MODEL_STAND_ALONE);
                        break;
                }
            }
        });
    }

    private void updateMainView() {
        am_tv_tips.setVisibility(SpUtils.getmInitFace() ? View.GONE : View.VISIBLE);
        int model = SpUtils.getWorkModel();
        if (model == AppInfo.MODEL_NET) {
            rg_work_model.check(R.id.am_rb_net);
        } else {
            rg_work_model.check(R.id.am_rb_standalone);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.am_btn_confirm:
                showConfirmDialog();
                break;
            case R.id.am_iv_back:
                onBackPressed();
                break;
        }
    }

    private void showConfirmDialog() {
        OridinryDialog oridinryDialog = new OridinryDialog(ModelActivity.this);
        oridinryDialog.setOnDialogClickListener(new OridinryDialogClick() {
            @Override
            public void sure() {
                confirm();
            }

            @Override
            public void noSure() {

            }
        });
        oridinryDialog.show(getString(R.string.change_model_info), getString(R.string.submit), getString(R.string.cacel));
    }

    private void confirm() {
        showWait("");
        new Thread(() -> {
            PersonDao.deleteAllPerson();
            FaceInfoJavaDao.deleteAllFaceInfo();
            LocalARDao.delAll();
            ClockInDao.delAlCockIn();
            EmpScheDao.delAllAttends();
            EmpScheDao.delAllSche();
            EmpScheDao.delAllFixSche();
            FileUtils.deleteDirOrFile(AppInfo.BASE_PATH_CACHE, "切换模式");
            FileUtils.deleteDirOrFile(AppInfo.CAPTURE_IMAGE_LOCAL, "切换模式");
            runOnUiThread(() -> hideWait());
            SystemManagerUtil.rebootApp(this);
        }).start();
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateMainView();
    }

}
