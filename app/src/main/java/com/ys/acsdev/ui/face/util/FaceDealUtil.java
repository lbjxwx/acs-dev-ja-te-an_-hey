package com.ys.acsdev.ui.face.util;

import com.ys.acsdev.api.AppConfig;
import com.ys.acsdev.bean.PersonBean;
import com.ys.acsdev.commond.AppInfo;
import com.ys.acsdev.db.dao.FaceInfoJavaDao;
import com.ys.acsdev.db.dao.PersonDao;
import com.ys.acsdev.service.AcsService;
import com.ys.acsdev.util.SimpleDateUtil;
import com.ys.facepasslib.FaceManager;

public class FaceDealUtil {

    /****
     * 删除人脸信息
     */
    public static void delelePersonFaceByToken(String faceToken) {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                FaceManager.getInstance().delFaceByToken(faceToken);
                FaceInfoJavaDao.deleteByToken(faceToken);
            }
        };
        AcsService.getInstance().executor(runnable);
    }

    /***
     * 判断通行方式类型
     * @param person
     * @return
     */
    public static String getPassTypeJujle(PersonBean person) {
        if (person == null) {
            return AppInfo.PASSTYPE_UNKNOW;
        }
        int personType = person.getType();
        if (personType == AppInfo.FACE_REGISTER_PERSON) {
            return AppInfo.PASSTYPE_FACE;
        }
        if (personType == AppInfo.FACE_REGISTER_VISITOR) {
            return AppInfo.PASSTYPE_VISITOR;
        }
        return AppInfo.PASSTYPE_FACE;
    }

    /**
     * 获取访客的通行权限
     * false  没有过期
     * true   已经过期
     */
    public static boolean visitorAccess(PersonBean visitor) {
        if (visitor == null) {
            return false;
        }
        String expirationTime = visitor.getSyncTime();
        long endTime = SimpleDateUtil.StringToLongTime(expirationTime);
        long currentTime = System.currentTimeMillis();
        if (currentTime < endTime) {
            return false;
        }
        PersonDao.delPersonByPerId(visitor.getPerId());
        return true;
    }


}
