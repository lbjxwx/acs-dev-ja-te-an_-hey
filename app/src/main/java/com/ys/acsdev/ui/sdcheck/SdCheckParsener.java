package com.ys.acsdev.ui.sdcheck;

import android.content.Context;
import android.util.Log;

import com.ys.acsdev.R;
import com.ys.acsdev.bean.DeviceBean;
import com.ys.acsdev.commond.AppInfo;
import com.ys.acsdev.runnable.FileWriteRunnable;
import com.ys.acsdev.service.AcsService;
import com.ys.acsdev.task.entity.MediaBean;
import com.ys.acsdev.util.APKUtil;
import com.ys.acsdev.util.Biantai;
import com.ys.acsdev.util.CodeUtil;
import com.ys.acsdev.util.FileUtils;
import com.ys.acsdev.util.LogUtil;
import com.ys.acsdev.util.SpUtils;
import com.ys.acsdev.util.guardian.WriteSdListener;
import com.ys.entity.FileEntity;
import com.ys.rkapi.MyManager;
import com.ys.util.FileMatch;

import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class SdCheckParsener {

    Context context;
    SdCheckView sdCheckView;
    /**
     * 将文件拷贝到本地路径
     *
     * @param filePth
     */
    int HasCopyNum = 0;
    List<MediaBean> listFile = new ArrayList<MediaBean>();

    public SdCheckParsener(Context context, SdCheckView sdCheckView) {
        this.context = context;
        this.sdCheckView = sdCheckView;
    }

    public void parsenerJsonInfo(String jsonText, String filePath) {
        if (jsonText == null || jsonText.length() < 5) {
            setThreeClose("There is nothing info in USB");
            return;
        }
        try {
            JSONObject jsonObject = new JSONObject(jsonText);
            DeviceBean deviceBean = SpUtils.getDeviceInfo();
            if (deviceBean == null) {
                String id = System.currentTimeMillis() + "";
                String equipName = CodeUtil.getUniquePsuedoID();
                String password = "";
                String imUserId = "";
                String appId = "";
                String deptName = "";
                String companyName = "";
                String deptId = "";
                deviceBean = new DeviceBean(id, equipName, password, imUserId,
                        appId, deptName, companyName, deptId);
            }
            if (jsonObject.toString().contains("company")) {
                String company = jsonObject.getString("company");
                addInfoToList("Modify company Success ");
                deviceBean.setCompanyName(company);
            }
            if (jsonObject.toString().contains("department")) {
                String department = jsonObject.getString("department");
                deviceBean.setDeptName(department);
                addInfoToList("Modify department Success ");
            }
            if (jsonObject.toString().contains("deviceName")) {
                String deviceName = jsonObject.getString("deviceName");
                deviceBean.setEquipName(deviceName);
                addInfoToList("Modify deviceName Success ");
            }
            String logoName = "";
            if (jsonObject.toString().contains("logoName")) {
                logoName = jsonObject.getString("logoName");
            }
            SpUtils.setDeviceInfo(deviceBean, "U盘修改底部显示信息");
            if (logoName != null && logoName.length() > 2) {
                writeLogoFileToLocal(filePath + "/" + logoName);
                return;
            }
            setThreeClose("Modify Main Info Success");
        } catch (Exception e) {
            setThreeClose("Parsener Json Error:" + e.toString());
            e.printStackTrace();
        }
    }

    public void writeLogoFileToLocal(String checkUrl) {

        File fileWrite = new File(checkUrl);
        if (!fileWrite.exists()) {
            setThreeClose("The logo file does not exist :" + checkUrl);
            return;
        }
        FileUtils.creatPathNotExcit();
        String logoSavePath = AppInfo.LOGO_PATH;
        File file = new File(logoSavePath);
        if (file.exists()) {
            file.delete();
        }
        try {
            file.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        addInfoToList(context.getString(R.string.start_write_file));
        FileWriteRunnable runnable = new FileWriteRunnable(checkUrl, logoSavePath, new WriteSdListener() {
            @Override
            public void writrFailed(String rrorDesc) {
                updateWriteProgress(0);
                setThreeClose(context.getString(R.string.copy_file_failed_msg, rrorDesc));
                LogUtil.sdcard("拷贝升级文件失败: " + rrorDesc, true);
            }

            @Override
            public void writeSuccess(String filePath) {
                updateWriteProgress(100);
                setThreeClose(context.getString(R.string.update_data_succeeded));
            }

            @Override
            public void writeProgress(int prgress) {
                updateWriteProgress(prgress);
                LogUtil.e("===========拷贝USB文件进度: " + prgress);
            }
        });
        AcsService.getInstance().executor(runnable);
    }

    public void writeApkToLocal(String checkUrl) {
        FileUtils.deleteDirOrFile(AppInfo.BASE_APK_PATH, "U盘升级APK, 先清理磁盘目录");
        FileUtils.creatPathNotExcit();
        String savePath = AppInfo.BASE_APK_PATH + "/face.apk";
        try {
            LogUtil.sdcard("准备升级APK", true);
            addInfoToList(context.getString(R.string.ready_update));
            File file = new File(checkUrl);
            if (!file.exists()) {
                LogUtil.sdcard("升级文件不存在", true);
                setThreeClose(context.getString(R.string.file_not_exist));
                return;
            }
            File fileApk = new File(savePath);
            if (fileApk.exists()) {
                fileApk.delete();
            }
            fileApk.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        addInfoToList(context.getString(R.string.start_write_file));
        FileWriteRunnable runnable = new FileWriteRunnable(checkUrl, savePath, new WriteSdListener() {
            @Override
            public void writrFailed(String rrorDesc) {
                updateWriteProgress(0);
                setThreeClose(context.getString(R.string.copy_file_failed_msg, rrorDesc));
                LogUtil.sdcard("拷贝升级文件失败: " + rrorDesc, true);
            }

            @Override
            public void writeSuccess(String filePath) {
                updateWriteProgress(100);
                addInfoToList(context.getString(R.string.update_data_succeeded));
                addInfoToList(context.getString(R.string.update_finish));
                LogUtil.sdcard("拷贝升级文件成功: " + filePath, true);
                installSdApk(filePath);
            }

            @Override
            public void writeProgress(int prgress) {
                updateWriteProgress(prgress);
//                addInfoToList(getString(R.string.write_progress, prgress + ""));
                LogUtil.e("===========拷贝USB文件进度: " + prgress);
            }
        });
        AcsService.getInstance().executor(runnable);
    }


    public void writeBootAnimalToLocal(String checkUrl) {
        FileUtils.creatPathNotExcit();
        //保存至根目录下
        String savePath = AppInfo.BASE_PATH_INNER + "/bootanimal.zip";
        try {
            LogUtil.sdcard(context.getString(R.string.ready_copy_bootanimal), true);
            addInfoToList(context.getString(R.string.ready_copy_bootanimal));
            File file = new File(checkUrl);
            if (!file.exists()) {
                LogUtil.sdcard("开机动画文件不存在", true);
                setThreeClose(context.getString(R.string.file_not_exist));
                return;
            }
            File fileApk = new File(savePath);
            if (fileApk.exists()) {
                fileApk.delete();
            }
            fileApk.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        addInfoToList(context.getString(R.string.start_write_file));
        FileWriteRunnable runnable = new FileWriteRunnable(checkUrl, savePath, new WriteSdListener() {
            @Override
            public void writrFailed(String rrorDesc) {
                updateWriteProgress(0);
                setThreeClose(context.getString(R.string.copy_file_failed_msg, rrorDesc));
                LogUtil.sdcard("拷贝升级文件失败: " + rrorDesc, true);
            }

            @Override
            public void writeSuccess(String filePath) {
                updateWriteProgress(100);
                addInfoToList(context.getString(R.string.copy_boot_animal_success));
//                addInfoToList(context.getString(R.string.update_finish));
                LogUtil.sdcard("拷贝升级文件成功: " + filePath, true);

                MyManager.getInstance(context).replaceBootanimation(filePath);
            }

            @Override
            public void writeProgress(int prgress) {
                updateWriteProgress(prgress);
//                addInfoToList(getString(R.string.write_progress, prgress + ""));
                LogUtil.e("===========拷贝USB文件进度: " + prgress);
            }
        });
        AcsService.getInstance().executor(runnable);
    }


    /**
     * 拷贝文件到ETV单机目录下
     *
     * @param filePath
     */
    public void copyDisTaskToLocal(String filePath) {

    }

    private void parpreToWriteFileToSd(List<File> listFileSearch) {
        addInfoToList("Number of files to be copied: " + listFileSearch.size());
        HasCopyNum = listFileSearch.size();
        listFile.clear();
        for (File fileCurrent : listFileSearch) {
            String filePth = fileCurrent.getPath();
            String fileName = fileCurrent.getName();
            LogUtil.zip("====离线文件的路径==" + filePth);
            int fileType = FileMatch.fileMatch(fileName);
            //图片，视频，文档准许拷贝
            if (fileType == FileEntity.STYLE_FILE_IMAGE
                    || fileType == FileEntity.STYLE_FILE_VIDEO) {
                listFile.add(new MediaBean(fileName, filePth, fileType));
            }
        }
        if (listFile == null || listFile.size() < 1) {
            setThreeClose("No Image or Video File  in the directory,Please check");
            return;
        }
        copyFileOneByOne();
    }

    private void copyFileOneByOne() {
        if (listFile == null || listFile.size() < 1) {
            setThreeClose("Cpoy over,Pull out U disk !");
            return;
        }
        String filePath = listFile.get(0).getPath();
        copyFileToLocalFileDir(filePath);
    }

    private void copyFileToLocalFileDir(String filePth) {
        try {
            String savePath = filePth.substring(filePth.indexOf("eface-media/") + 12);
            savePath = AppInfo.TASK_SINGLE_PATH() + "/" + savePath;
            File parentFile = new File(savePath).getParentFile();
            if (!parentFile.exists()) {
                parentFile.mkdirs();
            }
            LogUtil.zip("=======文件写入==真实路径=" + filePth + " / " + savePath);
            FileWriteRunnable runnable = new FileWriteRunnable(filePth, savePath, new WriteSdListener() {

                @Override
                public void writeProgress(int progress) {
                    updateWriteProgress(progress);
                }

                @Override
                public void writeSuccess(String savePath) {
                    sdCheckView.addInfoToList("Write successfully: " + savePath);
                    if (listFile != null && listFile.size() > 0) {
                        listFile.remove(0);
                    }
                    copyFileOneByOne();
                }

                @Override
                public void writrFailed(String errorrDesc) {
                    sdCheckView.addInfoToList("Write failure: " + errorrDesc);
                    if (listFile != null && listFile.size() > 0) {
                        listFile.remove(0);
                    }
                    copyFileOneByOne();
                }
            });
            AcsService.getInstance().executor(runnable);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void installSdApk(String savePath) {
        Log.e("update", "====开始安装APK==" + savePath);
        if (Biantai.isThreeClick()) {
            return;
        }
        try {
            APKUtil apkUtil = new APKUtil(context);
            apkUtil.installApk(savePath);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updateWriteProgress(int progress) {
        if (sdCheckView != null) {
            sdCheckView.updateWriteProgress(progress);
        }
    }

    private void setThreeClose(String desc) {
        if (sdCheckView != null) {
            sdCheckView.setThreeClose(desc);
        }
    }


    private void addInfoToList(String desc) {
        if (sdCheckView != null) {
            sdCheckView.addInfoToList(desc);
        }
    }


}
