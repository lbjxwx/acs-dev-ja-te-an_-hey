package com.ys.acsdev.ui.sdcheck;

public class FaceReg {
    public static final int START = 1;
    public static final int SUCCESS = 2;
    public static final int COMPLETE = 3;
    public static final int ERROR = 4;

    public int state;
    public String fileName;

    public FaceReg() {

    }

    public FaceReg(int state, String fileName) {
        this.state = state;
        this.fileName = fileName;
    }
}
