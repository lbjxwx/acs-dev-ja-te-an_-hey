package com.ys.acsdev.ui.sdcheck;

import android.app.Application;
import android.graphics.Bitmap;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.ys.acsdev.bean.FaceInfo;
import com.ys.acsdev.bean.PersonBean;
import com.ys.acsdev.commond.AppInfo;
import com.ys.acsdev.db.dao.FaceInfoJavaDao;
import com.ys.acsdev.db.dao.PersonDao;
import com.ys.acsdev.util.SimpleDateUtil;
import com.ys.facepasslib.FaceManager;
import com.ys.facepasslib.IRegisterFaceListener;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import mcv.facepass.types.FacePassCompareResult;

/**
 * U盘本地人脸注册
 */
public class LocalUPanRegister extends AndroidViewModel {


    private List<File> faceList = new ArrayList<>();
    private MutableLiveData<FaceReg> liveData = new MutableLiveData<>();
    private RegisterListenerImpl listenerImpl = new RegisterListenerImpl();

    public LocalUPanRegister(@NonNull Application application) {
        super(application);
    }

    public MutableLiveData<FaceReg> getRegLiveData() {
        return liveData;
    }

    public void registerFace(File faceDir) {
        if (faceDir == null) {
            liveData.setValue(new FaceReg(FaceReg.COMPLETE, ""));
            return;
        }
        if (!faceDir.exists() || !faceDir.isDirectory()) {
            liveData.setValue(new FaceReg(FaceReg.COMPLETE, ""));
            return;
        }
        File[] files = faceDir.listFiles();
        if (files == null || files.length < 1) {
            liveData.setValue(new FaceReg(FaceReg.COMPLETE, ""));
            return;
        }
        faceList.clear();
        for(File file : files) {
            if (file.isFile()) {
                faceList.add(file);
            }
        }
        startRegisterFace();
    }

    private void startRegisterFace() {
        if (faceList.size() < 1) {
            liveData.setValue(new FaceReg(FaceReg.COMPLETE, ""));
            return;
        }
        File file = faceList.remove(0);
        listenerImpl.setFaceFile(file);
        liveData.setValue(new FaceReg(FaceReg.START, file.getName()));
        FaceManager.getInstance().registerByFile(file, listenerImpl);
    }

    private String getFileName(File file) {
        String name = file.getName();
        int index = name.lastIndexOf('.');
        return name.substring(0, index);
    }

    private class RegisterListenerImpl implements IRegisterFaceListener {

        private File faceFile;
        Disposable disposable;

        public void setFaceFile(File file) {
            this.faceFile = file;
        }

        @Override
        public void onSuccess(@NonNull String faceToken, @NonNull String featurePath) {
            disposable = Observable.fromCallable(() -> {
                FaceManager faceManager = FaceManager.getInstance();
                int type = AppInfo.FACE_REGISTER_PERSON;
                long time = System.currentTimeMillis();
                String perId = time + "";
                String username = getFileName(faceFile);

                List<FaceInfo> infoList = FaceInfoJavaDao.getFaceInfoByName(username);
                for (FaceInfo faceInfo : infoList) {
                    Bitmap face1 = faceManager.getFaceImage(faceInfo.getFaceToken());
                    if (face1 == null) {
                        //同名且没有人脸图片，删除同名的旧数据
                        PersonDao.delPersonByPerId(faceInfo.getPersonId());
                        FaceInfoJavaDao.deleteByToken(faceInfo.getFaceToken());
                        continue;
                    }
                    Bitmap face2 = faceManager.getFaceImage(faceToken);
                    FacePassCompareResult result = faceManager.compareFace(face1, face2);
                    if (result != null && result.score > 72) {
                        PersonDao.delPersonByPerId(faceInfo.getPersonId());
                        FaceInfoJavaDao.deleteByToken(faceInfo.getFaceToken());
                        faceManager.delFaceByToken(faceInfo.getFaceToken());
                    }
                }

                //因为是U盘文件，所以不能直接获取文件的path
                //两种方案选择，1、把U盘文件复制到设备里面，然后再获取文件的path
                /*File desFile = new File(AppInfo.BASE_IMAGE_PATH + "/" + faceFile.getName());
                int index = 1;
                while (desFile.exists()) {
                    desFile = new File(AppInfo.BASE_IMAGE_PATH + "/" + index + faceFile.getName());
                    index++;
                }
                // U盘人脸图片文件复制到设备里面
                boolean copy = FileUtils.copyFile(faceFile, desFile);
                String path = copy ? desFile.getAbsolutePath() : "";*/

                //第二种方案， 2、直接从人脸库中已经注册成功的人脸，再通过faceToken获取
                String path = faceManager.getFaceImagePath(faceToken);

                String syncTime = SimpleDateUtil.formatTaskTimeShow(time);
                PersonDao.insertPerson(new PersonBean(type, perId, perId, username, perId, path, syncTime, "Dev Add", "", perId));
                FaceInfoJavaDao.insertFaceInfo(new FaceInfo(username, faceToken, perId, path, type, path, path));
                return new FaceReg(FaceReg.SUCCESS, faceFile.getName());
            }).subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                            faceReg -> {
                                liveData.setValue(faceReg);
                                startRegisterFace();
                            },
                            error -> {
                                error.printStackTrace();
                                liveData.setValue(new FaceReg(FaceReg.ERROR, faceFile.getName()));
                                startRegisterFace();
                            });

        }

        @Override
        public void onError(int code, @NonNull String msg) {
            liveData.setValue(new FaceReg(FaceReg.ERROR, faceFile.getName()));
            startRegisterFace();
        }

    }

}
