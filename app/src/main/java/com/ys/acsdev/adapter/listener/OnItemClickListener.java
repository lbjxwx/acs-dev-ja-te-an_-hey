package com.ys.acsdev.adapter.listener;

import android.view.View;

import com.ys.acsdev.adapter.BaseQuickAdapter;


public interface OnItemClickListener {
    void onItemClick(BaseQuickAdapter<?, ?> baseAdapter, View view, int position);
}
