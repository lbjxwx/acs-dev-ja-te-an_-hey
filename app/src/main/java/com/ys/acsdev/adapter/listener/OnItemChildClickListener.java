package com.ys.acsdev.adapter.listener;

import android.view.View;

import com.ys.acsdev.adapter.BaseQuickAdapter;


public interface OnItemChildClickListener {
    void onItemChildClick(BaseQuickAdapter<?,?> baseAdapter, View view, int position);
}
