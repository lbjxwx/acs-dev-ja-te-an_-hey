package com.ys.acsdev.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;


import com.ys.acsdev.R;
import com.ys.acsdev.bean.SocketEntity;
import com.ys.acsdev.listener.ObjectClickListener;

import java.util.List;

public class SocketLineAdapter extends BaseAdapter {

    Context context;
    List<SocketEntity> lists;
    LayoutInflater layoutInflater;

    public void setListContent(List<SocketEntity> lists) {
        this.lists = lists;
        notifyDataSetChanged();
    }

    public SocketLineAdapter(Context context, List<SocketEntity> lists) {
        this.context = context;
        this.lists = lists;
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return lists.size();
    }

    @Override
    public SocketEntity getItem(int position) {
        return lists.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = layoutInflater.inflate(R.layout.dialog_item_socket, null);
            viewHolder.tv_ip = (TextView) convertView.findViewById(R.id.tv_ip);
            viewHolder.tv_port = (TextView) convertView.findViewById(R.id.tv_port);
            viewHolder.tv_username = (TextView) convertView.findViewById(R.id.tv_username);
            viewHolder.lv_del = (ImageView) convertView.findViewById(R.id.lv_del);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        final SocketEntity socketEntity = lists.get(position);
        viewHolder.tv_ip.setText(socketEntity.getIp());
        viewHolder.tv_port.setText(socketEntity.getPort());
        viewHolder.tv_username.setText(socketEntity.getUserName());
        viewHolder.lv_del.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onAdapterClickListener == null) {
                    return;
                }
                onAdapterClickListener.clickSure(socketEntity);
            }
        });
        return convertView;
    }

    class ViewHolder {
        TextView tv_ip;
        TextView tv_port;
        TextView tv_username;
        ImageView lv_del;
    }

    ObjectClickListener onAdapterClickListener;

    public void setOnAdapterClickListener(ObjectClickListener onAdapterClickListener) {
        this.onAdapterClickListener = onAdapterClickListener;
    }


}
