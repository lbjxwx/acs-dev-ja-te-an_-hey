package com.ys.acsdev.adapter.listener;

import android.view.View;

import com.ys.acsdev.adapter.BaseQuickAdapter;


public interface OnItemChildLongClickListener {
    boolean onItemChildLongClick(BaseQuickAdapter<?,?> baseAdapter, View view, int position);
}
