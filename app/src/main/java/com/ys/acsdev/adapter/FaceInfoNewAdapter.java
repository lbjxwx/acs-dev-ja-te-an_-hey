package com.ys.acsdev.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Shader;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.ys.acsdev.R;
import com.ys.acsdev.bean.FaceInfo;
import com.ys.acsdev.bean.PersonBean;
import com.ys.acsdev.commond.AppInfo;
import com.ys.acsdev.db.dao.FaceInfoJavaDao;
import com.ys.acsdev.listener.AdapterItemClickListener;
import com.ys.acsdev.manager.ImageManager;
import com.ys.acsdev.util.SimpleDateUtil;
import com.ys.acsdev.util.SpUtils;
import com.ys.facepasslib.FaceManager;

import java.util.List;

public class FaceInfoNewAdapter extends RecyclerView.Adapter<FaceInfoNewAdapter.ViewHolder> {

    List<PersonBean> mFaceData = null;
    Context context;
    String syncTime = SpUtils.getFaceSyncTime();

    public FaceInfoNewAdapter(Context context, List<PersonBean> mFaceData) {
        this.context = context;
        this.mFaceData = mFaceData;
    }

    public void setList(List<PersonBean> mFaceData) {
        this.mFaceData = mFaceData;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_face_info, parent, false);
        FaceInfoNewAdapter.ViewHolder holder = new FaceInfoNewAdapter.ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        PersonBean personBean = mFaceData.get(position);
        FaceInfo faceInfo = FaceInfoJavaDao.getFaceInfoById(personBean.getPerId());
        boolean isRegisted = faceInfo != null && faceInfo.getFaceToken() != null && !faceInfo.getFaceToken().isEmpty();
        holder.ifi_tv_name.setText(personBean.getName());
        String cardNum = personBean.getCardNum();
        if (cardNum == null || cardNum.length() < 2) {
            cardNum = context.getString(R.string.none);
        }
        holder.ifi_tv_card.setText(cardNum);
        holder.ifi_tv_registed.setText(isRegisted ? context.getString(R.string.registered) : context.getString(R.string.unregistered));
        holder.ifi_tv_registed.setTextColor(isRegisted ? context.getResources().getColor(R.color.green) : context.getResources().getColor(R.color.red));
        holder.ifi_tv_time.setText(syncTime);
        holder.ifi_btn_del.setOnClickListener(view -> {
            if (adapterItemClickListener != null) {
                adapterItemClickListener.adapterItemClick(holder.ifi_btn_del.getId(), personBean, position);
            }
        });
        holder.ifi_tv_registed.setOnClickListener(view -> {
            if (adapterItemClickListener != null) {
                adapterItemClickListener.adapterItemClick(holder.ifi_tv_registed.getId(), personBean, position);
            }
        });

        String url = personBean.getPhotoUrl();
        if (url == null || url.length() < 5) {
            return;
        }
        int workModel = SpUtils.getWorkModel();
        if (workModel == AppInfo.MODEL_STAND_ALONE) {
            //单机模式
            ImageManager.displayImageDefault(url, holder.ifi_iv_face, R.mipmap.face);
            return;
        }
        try {
            if (faceInfo == null) {
                ImageManager.displayImageResource(R.mipmap.face_default, holder.ifi_iv_face);
                return;
            }
            String faceToken = faceInfo.getFaceToken();
            if (faceToken == null || faceToken.length() < 2) {
                ImageManager.displayImageResource(R.mipmap.face_default, holder.ifi_iv_face);
                return;
            }
            Bitmap bitmap = FaceManager.getInstance().getFaceImage(faceToken);
            if (bitmap != null) {
                ImageManager.loadBitmap(context, bitmap, holder.ifi_iv_face);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return mFaceData.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView ifi_tv_name, ifi_tv_card, ifi_tv_registed, ifi_tv_time;
        Button ifi_btn_del;
        ImageView ifi_iv_face;

        public ViewHolder(View itemView) {
            super(itemView);
            ifi_iv_face = (ImageView) itemView.findViewById(R.id.ifi_iv_face);
            ifi_tv_name = (TextView) itemView.findViewById(R.id.ifi_tv_name);
            ifi_tv_card = (TextView) itemView.findViewById(R.id.ifi_tv_card);
            ifi_tv_registed = (TextView) itemView.findViewById(R.id.ifi_tv_registed);
            ifi_tv_time = (TextView) itemView.findViewById(R.id.ifi_tv_time);
            ifi_btn_del = (Button) itemView.findViewById(R.id.ifi_btn_del);
        }
    }

    AdapterItemClickListener adapterItemClickListener;

    public void setOnAdapterOnItemClickListener(AdapterItemClickListener adapterItemClickListener) {
        this.adapterItemClickListener = adapterItemClickListener;
    }

}
