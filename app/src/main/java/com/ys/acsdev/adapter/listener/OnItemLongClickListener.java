package com.ys.acsdev.adapter.listener;

import android.view.View;

import com.ys.acsdev.adapter.BaseQuickAdapter;

public interface OnItemLongClickListener {
    boolean onItemLongClick(BaseQuickAdapter<?, ?> adapter, View view, int position);
}
