package com.ys.acsdev.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ys.acsdev.R;
import com.ys.acsdev.bean.AccessRecordBean;
import com.ys.acsdev.commond.AppInfo;
import com.ys.acsdev.manager.ImageManager;

import java.util.List;

public class AccessRecordNewAdapter extends RecyclerView.Adapter<AccessRecordNewAdapter.ViewHolder> {
    List<AccessRecordBean> accessRecordBeanList = null;
    Context context;

    public AccessRecordNewAdapter(Context context, List<AccessRecordBean> accessRecordBeanList) {
        this.context = context;
        this.accessRecordBeanList = accessRecordBeanList;
    }

    public void setDataList(List<AccessRecordBean> accessRecordBeanList) {
        this.accessRecordBeanList = accessRecordBeanList;
        notifyDataSetChanged();
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_access_record, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        AccessRecordBean accessRecordBean = accessRecordBeanList.get(position);
        String name = accessRecordBean.getName();
        String cardNum = accessRecordBean.getCardNum();
        String temp = accessRecordBean.getTemperature();
        String time = accessRecordBean.getRecordTime();
        if (time == null || time.length() < 3) {
            time = context.getString(R.string.none);
        }
        if (temp == null || temp.length() < 2) {
            temp = context.getString(R.string.none);
        }
        String passType = accessRecordBean.getPassType();
        if (passType.equals(AppInfo.PASSTYPE_UNKNOW)) {
            name = accessRecordBean.getUserName();
            cardNum = accessRecordBean.getIdCardNum();
        }
        if (name == null || name.length() < 1) {
            name = context.getString(R.string.stranger);
        }
        if (cardNum == null || cardNum.length() < 2) {
            cardNum = context.getString(R.string.none);
        }
        if (cardNum != null && cardNum.length() > 13) {
            cardNum = cardNum.substring(0, 6) + "******";
        }
        holder.iar_tv_time.setText(time);
        holder.iar_tv_temp.setText(temp);
        holder.iar_tv_card.setText(cardNum);
        holder.iar_tv_name.setText(name);
        String type = context.getString(R.string.unknown);
        switch (accessRecordBean.getPassType()) {
            case AppInfo.PASSTYPE_FACE:
                type = context.getString(R.string.face_recognition);
                break;
            case AppInfo.PASSTYPE_CARD:
                type = context.getString(R.string.use_card);
                break;
            case AppInfo.PASSTYPE_UNKNOW:
                type = context.getString(R.string.face_recognition);
                break;
            case AppInfo.PASSTYPE_TEMP_PWD:
                type = context.getString(R.string.temporary_password);
                break;
        }
        holder.iar_tv_type.setText(type);
        String url = accessRecordBean.getPhotoUrl();
        if (url == null || url.length() < 4) {
            url = "";
        }
        if (!TextUtils.isEmpty(url) && !url.startsWith("http") && !url.contains("face/cache")) {
            url = AppInfo.getBaseUrl() + url;
        }
        ImageManager.displayImageDefault(url, holder.iar_iv_face, R.mipmap.face);
    }

    @Override
    public int getItemCount() {
        return accessRecordBeanList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView iar_tv_time;
        TextView iar_tv_temp;
        TextView iar_tv_card;
        TextView iar_tv_type;
        ImageView iar_iv_face;
        TextView iar_tv_name;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            iar_tv_time = (TextView) itemView.findViewById(R.id.iar_tv_time);
            iar_tv_temp = (TextView) itemView.findViewById(R.id.iar_tv_temp);
            iar_tv_card = (TextView) itemView.findViewById(R.id.iar_tv_card);
            iar_tv_type = (TextView) itemView.findViewById(R.id.iar_tv_type);
            iar_tv_name = (TextView) itemView.findViewById(R.id.iar_tv_name);
            iar_iv_face = (ImageView) itemView.findViewById(R.id.iar_iv_face);
        }
    }
}
