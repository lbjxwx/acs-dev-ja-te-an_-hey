package com.huiming.huimingstore.ext

import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty

/**
 * Created by kermitye on 2018/11/15 18:40
 */
object DelegatesExt {
    fun <T> notNullSingleValue(): ReadWriteProperty<Any?, T> = NotNullSingleValueVar()
}


class NotNullSingleValueVar<T> : ReadWriteProperty<Any?, T> {

    private var value: T? = null
    //Getter函数 如果已经被初始化，则会返回一个值，否则会抛异常。
    override fun getValue(thisRef: Any?, property: KProperty<*>): T {
        return value ?: throw IllegalStateException("${property.name} not initialized")
    }

    //Setter函数 如果仍然是null，则赋值，否则会抛异常。
    override fun setValue(thisRef: Any?, property: KProperty<*>, value: T) {
        this.value = if (this.value == null) value else throw IllegalStateException("${property.name} not initialized")
    }
}