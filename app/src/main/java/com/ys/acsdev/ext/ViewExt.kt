package com.huiming.huimingstore.ext

import android.text.Html
import android.text.Html.FROM_HTML_MODE_COMPACT
import android.view.View
import android.widget.EditText
import android.widget.TextView


/**
 * Created by kermitye on 2018/11/13 11:06
 */
fun View.setVisible(visible: Boolean, invisible: Boolean = false) {
    this.visibility = if (visible) View.VISIBLE else if (invisible) View.INVISIBLE else View.GONE
}


fun EditText.getValue() = text.toString().trim()

fun EditText.getInt() = if(text.toString().isEmpty()) 0 else text.toString().toInt()

inline fun TextView.setRedStr(normalStr: String, redStr: String) {
    setHtmlStr("${normalStr}<font color='#ff4284'>$redStr</font>")
}

inline fun TextView.setHtmlStr(htmlStr: String) {
    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
        setText(Html.fromHtml(htmlStr, FROM_HTML_MODE_COMPACT))
    } else {
        setText(Html.fromHtml(htmlStr))
    }

}

inline fun TextView.getInt() = if (text.toString().isEmpty()) 0 else text.toString().toInt()



inline fun TextView.setBold(enable: Boolean) {
    paint.isFakeBoldText = enable
}
inline fun TextView.hideDrawable() {
    setCompoundDrawables(null,null,null,null)
}


inline fun TextView.setDrawable(resId: Int) {
    var drawable = context.resources.getDrawable(resId, null)
    drawable.setBounds(0,0,drawable.minimumWidth, drawable.minimumHeight)
    setCompoundDrawables(null, null, drawable, null)
}
