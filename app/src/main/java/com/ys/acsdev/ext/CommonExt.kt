package com.huiming.huimingstore.ext

import android.app.ActivityManager
import android.app.Application
import android.content.ActivityNotFoundException
import android.content.Context
import android.graphics.Paint
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import androidx.appcompat.app.AppCompatActivity
import android.widget.TextView
import com.bumptech.glide.RequestBuilder
import com.bumptech.glide.request.RequestOptions
import com.ys.acsdev.MyApp


/**
 * Created by kermitye on 2018/11/16 10:10
 */
inline fun RequestBuilder<*>.option(placeResourceId: Int = -1, errorResourceId: Int = -1) =
    apply(RequestOptions().let {
        if (placeResourceId != -1)
            it.placeholder(placeResourceId)
        if (errorResourceId != -1)
            it.error(errorResourceId)
        it
    })

inline fun RequestBuilder<*>.override(width: Int = -1, height: Int = -1) =
    apply(RequestOptions().let {
        it.override(width, height)
        it
    })


inline fun String.hidePhone() = this.substring(0, 3) + "****" + this.substring(7, 11)

/**
 * 给TextView添加删除线
 */
fun TextView.strikeout() = apply { paint.flags = Paint.STRIKE_THRU_TEXT_FLAG }

fun Float.money(): String = "¥ ${String.format("%.2f", this)}"
fun Double.money(): String = "¥ ${String.format("%.2f", this)}"
fun Int.money(): String = "¥ ${String.format("%.2f", this)}"
fun String.money(): String = "¥ $this"

fun Long.toTwoNum(): String = run {
    if (this >= 10) {
        return this.toString()
    } else {
        return "0${this}"
    }
}

fun Int.boolean() = this == 1

inline fun Context.isTopActivity(activityName: String?): Boolean {
    if (activityName.isNullOrEmpty())
        return false
    var am = getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
    val cn = am.getRunningTasks(1)
    val taskInfo = cn[0]
    val topActivity = taskInfo.topActivity
    if (topActivity.getPackageName().equals(packageName) &&
        topActivity.getClassName().equals(activityName)
    ) {
        return true;
    }
    return false
}


/**
 * 检查微信是否安装
 */
fun AppCompatActivity.checkWechat(): Boolean {
    val packageManager = this.packageManager// 获取packagemanager
    val pinfo = packageManager.getInstalledPackages(0)// 获取所有已安装程序的包信息
    if (pinfo != null) {
        for (i in pinfo.indices) {
            val pn = pinfo[i].packageName
            if (pn == "com.tencent.mm") {
                return true
            }
        }
    }
    return false
}

fun Application.getExtColor(resId: Int) = resources.getColor(resId)


inline fun androidx.fragment.app.FragmentManager.inTransaction(func: androidx.fragment.app.FragmentTransaction.() -> androidx.fragment.app.FragmentTransaction) =
    beginTransaction().func().commit()

fun AppCompatActivity.addFragment(fragment: androidx.fragment.app.Fragment, frameId: Int) =
    supportFragmentManager.inTransaction { add(frameId, fragment) }

fun AppCompatActivity.replaceFragment(fragment: androidx.fragment.app.Fragment, frameId: Int) =
    supportFragmentManager.inTransaction { replace(frameId, fragment) }


fun getResourceStr(resId: Int): String {
    return MyApp.getInstance().resources.getString(resId)
}

fun getResourceStr(resId: Int, vararg data: Any?): String {
    return String.format(getResourceStr(resId), data)
}
