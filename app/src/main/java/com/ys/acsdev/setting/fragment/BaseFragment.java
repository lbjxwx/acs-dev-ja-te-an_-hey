package com.ys.acsdev.setting.fragment;

import android.app.Fragment;

import com.ys.acsdev.view.MyToastView;

public class BaseFragment extends Fragment {

    public void showToast(String string) {
        MyToastView.getInstance().Toast(getActivity(), string);
    }

}
