package com.ys.acsdev.setting.util;

import android.content.Context;

import com.ys.acsdev.R;
import com.ys.acsdev.api.AppConfig;
import com.ys.acsdev.bean.BeanDefaultEntity;
import com.ys.acsdev.setting.entity.TempModelEntity;
import com.ys.acsdev.util.SpUtils;

import java.util.ArrayList;
import java.util.List;

public class TempModelUtil {

    public static int getCurrentTempType(Context context, int position) {
        List<TempModelEntity> getTympMpdelList = getTympMpdelList(context);
        TempModelEntity tempModelEntity = getTympMpdelList.get(position);
        int backType = tempModelEntity.getTempTyle();
        return backType;
    }

    /***
     * 获取当前测温类型
     * @return
     */
    public static String getCurrentTempType(Context context) {
        String tempTypeBacl = "";
        int tempType = SpUtils.getmTempType();
        List<TempModelEntity> getTympMpdelList = getTympMpdelList(context);
        for (TempModelEntity tempModelEntity : getTympMpdelList) {
            int tempTypeSave = tempModelEntity.getTempTyle();
            if (tempTypeSave == tempType) {
                tempTypeBacl = tempModelEntity.getTempName();
                break;
            }
        }
        return tempTypeBacl;
    }

    public static List<TempModelEntity> getTempListByUser(Context context) {
        List<TempModelEntity> listsBack = new ArrayList<TempModelEntity>();
        //获取当前支持得列表信息
        List<TempModelEntity> lists = getTympMpdelList(context);
        int customCode = AppConfig.APP_CUSTOM_TYPE;
        switch (customCode) {
            case AppConfig.APP_CUSTOM_DEFAULT:
                listsBack.addAll(lists);
                break;
            case AppConfig.APP_CUSTOM_JTA_DEFAULT:
            case AppConfig.APP_CUSTOM_JTA_HID:
            case AppConfig.APP_CUSTOM_JTA_ID_CARD:
                listsBack.add(lists.get(0));
                listsBack.add(lists.get(1));
                listsBack.add(lists.get(5));
                listsBack.add(lists.get(16));
                listsBack.add(lists.get(26));
                break;
            default:
                listsBack.addAll(lists);
                break;
        }
        return listsBack;
    }

    /***
     * 获取所有测温类型
     * 下面得集合顺序
     * 1：不允许删除
     * 2：只允许往后追加
     * 3：当前顺序不允许变
     * @param context
     * @return
     */
    private static List<TempModelEntity> getTympMpdelList(Context context) {
        List<TempModelEntity> lists = new ArrayList<TempModelEntity>();
        lists.add(new TempModelEntity(TempModelEntity.CLOSE, context.getString(R.string.close)));  //0
        lists.add(new TempModelEntity(TempModelEntity.IIC_MLX_90621_BAB, "IIC_MLX_90621_BAB"));  //1
        lists.add(new TempModelEntity(TempModelEntity.IIC_MLX_90621_BAA, "IIC_MLX_90621_BAA"));  //2
        lists.add(new TempModelEntity(TempModelEntity.IIC_MLX_90640_0AA1547511, "IIC_MLX_90640_0AA1547511")); //3
        lists.add(new TempModelEntity(TempModelEntity.IIC_MLX_90614, "IIC_MLX_90614"));  //4
        lists.add(new TempModelEntity(TempModelEntity.IIC_MLX_90641, "IIC_MLX_90641"));   //5
        lists.add(new TempModelEntity(TempModelEntity.SP_MLX_90621_RR, "SP_MLX_90621_RR"));   //6
        lists.add(new TempModelEntity(TempModelEntity.SP_MLX_90621_STM, "SP_MLX_90621_STM"));   //7
        lists.add(new TempModelEntity(TempModelEntity.SP_MLX_90621_TIR_SN, "SP_MLX_90621_TIR_SN"));   //8
        //9 -10 是周蒙模块
        lists.add(new TempModelEntity(TempModelEntity.SP_MLX_90621_BAA_ZM_TXT, "SP_MLX_90621_BAA_ZM_TXT"));   //9
        lists.add(new TempModelEntity(TempModelEntity.SP_MLX_90621_BAA_ZM_HEX, "SP_MLX_90621_BAA_ZM_HEX"));   //10
        //通用
        lists.add(new TempModelEntity(TempModelEntity.SP_MLX_90621_BAA, "SP_MLX_90621_BAA"));   //11
        //12  13 是华一得模块
        lists.add(new TempModelEntity(TempModelEntity.SP_MLX_90621_HY, "SP_MLX_90621_HY"));   //12
        lists.add(new TempModelEntity(TempModelEntity.SP_MLX_90621_HY1, "SP_MLX_90621_HY1"));   //13
        //14 铭鸿宇客户，后边直接删除
        lists.add(new TempModelEntity(TempModelEntity.SP_MHY_10724, "SP_MHY_10724"));   //14
        lists.add(new TempModelEntity(TempModelEntity.SP_XRJ_S60, "SP_XRJ_S60"));   //15
        lists.add(new TempModelEntity(TempModelEntity.SP_HM_32X32, "SP_HM_32X32"));   //16
        lists.add(new TempModelEntity(TempModelEntity.SP_MLX_90641_ESF, "SP_MLX_90641_ESF"));   //17
        //19 今视通模块
        lists.add(new TempModelEntity(TempModelEntity.SP_MLX_90621_JST, "SP_MLX_90621_JST"));   //18
        //奥朗德
        lists.add(new TempModelEntity(TempModelEntity.SP_HM_MT031, "SP_HM_MT031")); //19
        //慧科
        lists.add(new TempModelEntity(TempModelEntity.SP_KM_D801, "SP_KM_D801")); //20
        //星马621
        lists.add(new TempModelEntity(TempModelEntity.SP_OTPA_16, "SP_OTPA_16")); //21
        //天地印象
        lists.add(new TempModelEntity(TempModelEntity.SP_MLX_90641_YNC, "SP_MLX_90641_YNC")); //22
        //华一 欧姆龙
        lists.add(new TempModelEntity(TempModelEntity.SP_OMR_D6T_4X4, "SP_OMR_D6T_4X4")); //23
        //星马 i2c
        lists.add(new TempModelEntity(TempModelEntity.IIC_OTPA_16, "IIC_OTPA_16")); //24
        //90641 stm
        lists.add(new TempModelEntity(TempModelEntity.SP_MLX_90641_STM, "SP_MLX_90641_STM")); //25
        //海曼5.0
        lists.add(new TempModelEntity(TempModelEntity.SP_HM_V11, "SP_HM_V11")); //26  ttys-
        //MC3216
        lists.add(new TempModelEntity(TempModelEntity.USB_OTPA_MC3216, "USB_OTPA_MC3216")); //27  ttys-
        lists.add(new TempModelEntity(TempModelEntity.SP_EXTERNAL_SMALL_PLATE, "SP_EXTERNAL_SMALL_PLATE")); //28  欧姆龙。3和一串口小板
        lists.add(new TempModelEntity(TempModelEntity.SP_HM5_0_STM_YS, "SP_HM5.0_STM_YS")); //29 海曼5.0 亿晟
        lists.add(new TempModelEntity(TempModelEntity.IIC_HPTA_32, "IIC_HM5.0_YS")); //30  ttys-
        return lists;
    }

    public static String getmCurrenTempRare() {
        String ttysBack = "";
        int rare = SpUtils.getmTempRare();
        List<BeanDefaultEntity> entityList = getmTempRareList();
        for (BeanDefaultEntity beanDefaultEntity : entityList) {
            int rareSave = beanDefaultEntity.getDefaultNum();
            if (rare == rareSave) {
                ttysBack = beanDefaultEntity.getDefaultDesc();
                break;
            }
        }
        return ttysBack;
    }

    /***
     * 获取支持的波特率
     * @return
     */
    public static List<BeanDefaultEntity> getmTempRareList() {
        List<BeanDefaultEntity> list = new ArrayList<BeanDefaultEntity>();
        list.add(new BeanDefaultEntity(0, "9600"));
        list.add(new BeanDefaultEntity(1, "19200"));
        list.add(new BeanDefaultEntity(2, "57600"));
        list.add(new BeanDefaultEntity(3, "115200"));
        list.add(new BeanDefaultEntity(4, "230400"));
        list.add(new BeanDefaultEntity(5, "460800"));
        list.add(new BeanDefaultEntity(6, "921600"));
        list.add(new BeanDefaultEntity(7, "1152000"));
        return list;
    }

    /***
     * 获取当前串口显示类型
     * @return
     */
    public static String getCurrentTTYS() {
        String ttysBack = "";
        int ttys = SpUtils.getmTempDev();
        List<BeanDefaultEntity> entityList = getDevTTYSList();
        for (BeanDefaultEntity beanDefaultEntity : entityList) {
            int ttysSave = beanDefaultEntity.getDefaultNum();
            if (ttys == ttysSave) {
                ttysBack = beanDefaultEntity.getDefaultDesc();
                break;
            }
        }
        return ttysBack;
    }

    /***
     * 串口类型
     * @return
     */
    public static List<BeanDefaultEntity> getDevTTYSList() {
        List<BeanDefaultEntity> list = new ArrayList<BeanDefaultEntity>();
        list.add(new BeanDefaultEntity(0, "ttyS0"));
        list.add(new BeanDefaultEntity(1, "ttyS1"));
        list.add(new BeanDefaultEntity(2, "ttyS2"));
        list.add(new BeanDefaultEntity(3, "ttyS3"));
        list.add(new BeanDefaultEntity(4, "ttyS4"));
        return list;
    }


}
