package com.ys.acsdev.setting.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.ys.acsdev.R;
import com.ys.acsdev.bean.LocalARBean;
import com.ys.acsdev.commond.AppInfo;
import com.ys.acsdev.manager.ImageManager;
import com.ys.acsdev.task.banner.GlideImageLoader;
import com.ys.acsdev.util.LogUtil;

import java.util.List;

public class AccessOffRecordAdapter extends BaseAdapter {

    Context context;
    List<LocalARBean> list;
    LayoutInflater layoutInflater;

    public AccessOffRecordAdapter(Context context, List<LocalARBean> list) {
        this.context = context;
        this.list = list;
        layoutInflater = LayoutInflater.from(context);
    }

    public void setInfoList(List<LocalARBean> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public LocalARBean getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = layoutInflater.inflate(R.layout.item_access_record, null);
            viewHolder.iar_tv_name = convertView.findViewById(R.id.iar_tv_name);
            viewHolder.iar_tv_type = convertView.findViewById(R.id.iar_tv_type);
            viewHolder.iar_tv_card = convertView.findViewById(R.id.iar_tv_card);
            viewHolder.iar_tv_temp = convertView.findViewById(R.id.iar_tv_temp);
            viewHolder.iar_tv_time = convertView.findViewById(R.id.iar_tv_time);
            viewHolder.iar_iv_face = convertView.findViewById(R.id.iar_iv_face);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        LocalARBean entity = list.get(position);
        LogUtil.cdl("=======数据加载=-===" + entity.toString());
        String filePath = entity.getFilePath();
        ImageManager.displayImageDefault(filePath, viewHolder.iar_iv_face, R.mipmap.face_default);
        String name = entity.getName();
        if (entity.getName() == null || entity.getName().length() < 2) {
            name = context.getString(R.string.stranger);
        }
        viewHolder.iar_tv_name.setText(name);
        String type = context.getString(R.string.unknown);
        switch (entity.getPassType()) {
            case AppInfo.PASSTYPE_FACE:
                type = context.getString(R.string.face_recognition);
                break;
            case AppInfo.PASSTYPE_CARD:
                type = context.getString(R.string.use_card);
                break;
            case AppInfo.PASSTYPE_UNKNOW:
                type = context.getString(R.string.face_recognition);
                break;
            case AppInfo.PASSTYPE_TEMP_PWD:
                type = context.getString(R.string.temporary_password);
                break;
        }
        viewHolder.iar_tv_type.setText(type);
        String cardNum = entity.getCardNum() == null || entity.getCardNum().isEmpty() ? context.getString(R.string.none) : entity.getCardNum();
        if (cardNum != null && cardNum.length() > 13) {
            cardNum = cardNum.substring(0, 6) + "******";
        }
        viewHolder.iar_tv_card.setText(cardNum);
        String temp = entity.getTemperature() == null || entity.getTemperature().isEmpty() ? context.getString(R.string.none) : entity.getTemperature();
        viewHolder.iar_tv_temp.setText(temp);
        viewHolder.iar_tv_time.setText(entity.getRecordTime());
        return convertView;
    }

    class ViewHolder {
        TextView iar_tv_name, iar_tv_type, iar_tv_card, iar_tv_temp, iar_tv_time;
        ImageView iar_iv_face;
    }

}
