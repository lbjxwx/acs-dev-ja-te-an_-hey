package com.ys.acsdev.setting.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ys.acsdev.R;
import com.ys.acsdev.listener.AdapterItemClickListener;
import com.ys.acsdev.setting.entity.TempAddEntity;

import java.util.List;


/***
 * 温度补偿显示adapter
 */
public class TempAddAdapter extends BaseAdapter {

    List<TempAddEntity> tempAddEntityList = null;
    Context context;
    LayoutInflater layoutInflater;


    public TempAddAdapter(Context context, List<TempAddEntity> tempAddEntityList) {
        this.context = context;
        this.tempAddEntityList = tempAddEntityList;
        layoutInflater = LayoutInflater.from(context);
    }

    public void setTempList(List<TempAddEntity> tempAddEntityList) {
        this.tempAddEntityList = tempAddEntityList;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return tempAddEntityList.size();
    }

    @Override
    public TempAddEntity getItem(int i) {
        return tempAddEntityList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
        ViewHoder viewHoder = null;
        if (convertView == null) {
            viewHoder = new ViewHoder();
            convertView = layoutInflater.inflate(R.layout.item_temp_add, null);
            viewHoder.rela_bgg = convertView.findViewById(R.id.rela_bgg);
            viewHoder.tv_show_num = convertView.findViewById(R.id.tv_show_num);
            viewHoder.tv_start = convertView.findViewById(R.id.tv_start);
            viewHoder.tv_end = convertView.findViewById(R.id.tv_end);
            viewHoder.tv_temp_add = convertView.findViewById(R.id.tv_temp_add);
            viewHoder.btn_del = convertView.findViewById(R.id.btn_del);
            convertView.setTag(viewHoder);
        } else {
            viewHoder = (ViewHoder) convertView.getTag();
        }
        TempAddEntity tempAddEntity = tempAddEntityList.get(i);
        boolean isChooice = tempAddEntity.isChlooice();
        viewHoder.tv_show_num.setTextColor(context.getResources().getColor(R.color.grey));
        viewHoder.tv_start.setTextColor(context.getResources().getColor(R.color.grey));
        viewHoder.tv_end.setTextColor(context.getResources().getColor(R.color.grey));
        viewHoder.tv_temp_add.setTextColor(context.getResources().getColor(R.color.grey));
        viewHoder.rela_bgg.setBackgroundColor(context.getResources().getColor(R.color.white));
        if (isChooice) {
            viewHoder.tv_show_num.setTextColor(context.getResources().getColor(R.color.white));
            viewHoder.tv_start.setTextColor(context.getResources().getColor(R.color.white));
            viewHoder.tv_end.setTextColor(context.getResources().getColor(R.color.white));
            viewHoder.tv_temp_add.setTextColor(context.getResources().getColor(R.color.white));
            viewHoder.rela_bgg.setBackgroundColor(context.getResources().getColor(R.color.white));
            viewHoder.rela_bgg.setBackgroundColor(context.getResources().getColor(R.color.themColor));
        }
        viewHoder.tv_show_num.setText((i + 1) + "");
        viewHoder.tv_start.setText(tempAddEntity.getStartTime() + "");
        viewHoder.tv_end.setText(tempAddEntity.getEndTime() + "");
        viewHoder.tv_temp_add.setText(tempAddEntity.getAddTemp() + "");
        viewHoder.btn_del.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (itemClickListener == null) {
                    return;
                }
                itemClickListener.adapterItemClick(view.getId(), tempAddEntity, i);
            }
        });
        return convertView;
    }

    class ViewHoder {
        LinearLayout rela_bgg;
        Button btn_del;
        TextView tv_show_num;
        TextView tv_start;
        TextView tv_end;
        TextView tv_temp_add;
    }

    AdapterItemClickListener itemClickListener;

    public void setItemClickListener(AdapterItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }
}
