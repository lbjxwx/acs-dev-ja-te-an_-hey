package com.ys.acsdev.setting.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.ys.acsdev.R;
import com.ys.acsdev.setting.entity.SettingEntity;

import java.util.List;

/***
 * 設置界面--adapter
 */
public class SettingMenuAdapter extends BaseAdapter {

    List<SettingEntity> list = null;
    Context context;
    LayoutInflater layoutInflater;

    public SettingMenuAdapter( Context context,List<SettingEntity> list) {
        this.context = context;
        this.list = list;
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        if (list==null||list.size()<1){
            return 0;
        }
        return list.size();
    }

    @Override
    public SettingEntity getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if (convertView==null){
            viewHolder = new ViewHolder();
            convertView = layoutInflater.inflate(R.layout.item_setting_menu,null);
            viewHolder.iv_icon_menu = convertView.findViewById(R.id.iv_icon_menu);
            viewHolder.tv_title = convertView.findViewById(R.id.tv_title);
            convertView.setTag(viewHolder);
        }else {
            viewHolder = (ViewHolder)convertView.getTag();
        }
        viewHolder.iv_icon_menu.setBackgroundResource(list.get(position).getImageId());
        viewHolder.tv_title.setText(list.get(position).getTitle());
        return convertView;
    }

    class  ViewHolder{
        ImageView iv_icon_menu;
        TextView  tv_title;
    }

}
