//package com.ys.acsdev.setting.adapter;
//
//import android.text.TextUtils;
//import android.widget.ImageView;
//
//import androidx.annotation.NonNull;
//import androidx.annotation.Nullable;
//
//import com.chad.library.adapter.base.BaseQuickAdapter;
//import com.chad.library.adapter.base.BaseViewHolder;
//import com.ys.acsdev.R;
//import com.ys.acsdev.api.ApiHelper;
//import com.ys.acsdev.bean.FaceInfo;
//import com.ys.acsdev.bean.VisitorEntity;
//import com.ys.acsdev.db.dao.FaceInfoDao;
//import com.ys.acsdev.manager.ImageManager;
//
//import java.util.List;
//
//public class VisitorInfoAdapter extends BaseQuickAdapter<VisitorEntity, BaseViewHolder> {
//
//    public VisitorInfoAdapter(@Nullable List data) {
//        super(R.layout.item_visitor_info, data);
//    }
//
//    @Override
//    protected void convert(@NonNull BaseViewHolder helper, VisitorEntity item) {
//
//        FaceInfo faceInfo = FaceInfoDao.getInstance().getFaceInfoById(item.getEmpId());
//
//        boolean isRegisted = faceInfo != null && faceInfo.getFaceToken() != null && !faceInfo.getFaceToken().isEmpty();
//        helper.setText(R.id.ivi_tv_name, item.getName());
//        helper.setText(R.id.ivi_tv_registed, isRegisted ? mContext.getString(R.string.registered) : mContext.getString(R.string.unregistered));
//        helper.setTextColor(R.id.ivi_tv_registed, isRegisted ? mContext.getResources().getColor(R.color.green) : mContext.getResources().getColor(R.color.red));
//        helper.setText(R.id.ivi_tv_valid_time, item.getValidTime());
//        String url = item.getFaceFilePath();
//        if (!TextUtils.isEmpty(url) && !url.startsWith("http")) {
//            url = ApiHelper.INSTANCE.getBaseUrl("", "") + url;
//        }
//        ImageManager.INSTANCE.displayImageDefault(url, (ImageView) helper.getView(R.id.ivi_iv_face), R.mipmap.face);
//    }
//}
