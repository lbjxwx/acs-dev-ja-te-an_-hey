package com.ys.acsdev.setting;

import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.speech.tts.Voice;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.ys.acsdev.R;
import com.ys.acsdev.api.AppConfig;
import com.ys.acsdev.commond.AppInfo;
import com.ys.acsdev.listener.MoreButtonListener;
import com.ys.acsdev.listener.MoreButtonToggleListener;
import com.ys.acsdev.runnable.down.DownFileEntity;
import com.ys.acsdev.runnable.down.DownRunnable;
import com.ys.acsdev.runnable.down.DownStateListener;
import com.ys.acsdev.service.AcsService;
import com.ys.acsdev.ui.BaseActivity;
import com.ys.acsdev.util.APKUtil;
import com.ys.acsdev.util.NetWorkUtils;
import com.ys.acsdev.util.SpUtils;
import com.ys.acsdev.view.MoreButton;
import com.ys.acsdev.view.MoreButtonToggle;

import org.jetbrains.annotations.Nullable;

public class VoiceSettingsActivity extends BaseActivity implements View.OnClickListener, MoreButtonListener {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_voice_settings);
        initView();
    }

    ProgressBar mPb;
    ImageView mIvBack;
    TextView tv_down_statues;

    MoreButton more_install_apk_statues, more_tts_chooice, more_tts_setting;
    MoreButtonToggle more_toggle_tts;
    MoreButtonToggle avs_btn_speak_name;

    private void initView() {
        more_toggle_tts = (MoreButtonToggle) findViewById(R.id.more_toggle_tts);
        more_toggle_tts.setOnMoretListener(new MoreButtonToggleListener() {
            @Override
            public void switchToggleView(View view, boolean isChooice) {
                SpUtils.setOpenTTSVoice(isChooice);
                showToast(getString(R.string.set_success));
                initData();
            }
        });

        more_tts_setting = (MoreButton) findViewById(R.id.more_tts_setting);
        more_tts_setting.setOnMoretListener(this);
        more_install_apk_statues = (MoreButton) findViewById(R.id.more_install_apk_statues);
        more_install_apk_statues.setOnMoretListener(this);
        more_tts_chooice = (MoreButton) findViewById(R.id.more_tts_chooice);
        more_tts_chooice.setOnMoretListener(this);
        avs_btn_speak_name = findViewById(R.id.avs_btn_speak_name);
        avs_btn_speak_name.setOnMoretListener(new MoreButtonToggleListener() {
            @Override
            public void switchToggleView(View view, boolean isChooice) {
                SpUtils.setSpeakName(isChooice);
                if (isChooice) {
                    //播报人名，就需要关闭精简模式
                    SpUtils.setSimpleModel(false);
                }
            }
        });
        mPb = findViewById(R.id.avs_pb);
        mPb.setMax(100);
        mIvBack = (ImageView) findViewById(R.id.mIvBack);
        mIvBack.setOnClickListener(this);
        tv_down_statues = (TextView) findViewById(R.id.tv_down_statues);
    }

    @Override
    public void clickView(View view) {
        switch (view.getId()) {
            case R.id.more_tts_setting:
                startActivity(new Intent("com.android.settings.TTS_SETTINGS"));
                break;
            case R.id.more_tts_chooice:
                //语音选择
                Intent intent = new Intent();
                intent.setClassName("com.android.settings", "com.android.settings.Settings$InputMethodAndLanguageSettingsActivity");
                startActivity(intent);
                break;
            case R.id.more_install_apk_statues:
                String googleTssPkg = "com.google.android.tts";
                boolean installed = APKUtil.ApkState(VoiceSettingsActivity.this, googleTssPkg);
                if (installed) {
                    showToast("The software has been installed");
                    return;
                }
                if (isDownStatues) {
                    showToast("Is downloading ...");
                    return;
                }
                downApkFromWeb();
                break;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.mIvBack:
                finish();
                break;
        }
    }

    boolean isDownStatues = false;

    private void downApkFromWeb() {
        if (!NetWorkUtils.isNetworkConnected(VoiceSettingsActivity.this)) {
            showToast("Current Not Net");
            return;
        }
        String fileSaneName = "googleTTs.apk";
        String url = "http://119.23.220.53:8899/resource/apk/google_tts.apk";
//        String url = "http://119.23.220.53:8899/resource/apk/kdxf_tts.apk";
        String savePath = AppInfo.BASE_APK_PATH + "/" + fileSaneName;

        DownRunnable downRunnable = new DownRunnable(url, savePath, fileSaneName, new DownStateListener() {
            @Override
            public void downStateInfo(DownFileEntity entity) {
                if (entity == null) {
                    return;
                }
                int downStatues = entity.getDownState();
                switch (downStatues) {
                    case DownFileEntity.DOWN_STATE_PROGRESS:
                        isDownStatues = true;
                        int progress = entity.getProgress();
                        int speed = entity.getDownSpeed();
                        mPb.setProgress(progress);
                        tv_down_statues.setText(speed + " KB/S" + " / " + progress + "%");
                        break;
                    case DownFileEntity.DOWN_STATE_SUCCESS:
                        isDownStatues = false;
                        String apkPath = entity.getSavePath();
                        installApkToLocal(apkPath);
                        break;
                    case DownFileEntity.DOWN_STATE_FAIED:
                        isDownStatues = false;
                        String ERRORdESC = entity.getDesc();
                        tv_down_statues.setText("Error:" + ERRORdESC);
                        break;
                }
            }
        });
        downRunnable.setIsDelFile(true);
        downRunnable.setLimitDownSpeed(AppConfig.DOWN_SPEED_LIMIT);
        AcsService.getInstance().executor(downRunnable);
    }

    private void installApkToLocal(String url) {
        Log.e("install", "===安装的路径==" + url);
        APKUtil.installApk(url);
    }

    @Override
    protected void onResume() {
        super.onResume();
        initData();
    }

    private void initData() {
        String googleTssPkg = "com.google.android.tts";
        boolean installed = APKUtil.ApkState(VoiceSettingsActivity.this, googleTssPkg);
        if (installed) {
            more_install_apk_statues.setTextTitle(getString(R.string.installted));
        } else {
            more_install_apk_statues.setTextTitle(getString(R.string.no_install));
        }
        more_toggle_tts.setChcekcAble(SpUtils.getOpenTTSVoice());
        avs_btn_speak_name.setChcekcAble(SpUtils.getSpeakName("语音设置调用"));
    }
}
