package com.ys.acsdev.setting.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import com.ys.acsdev.R;
import com.ys.acsdev.api.AppConfig;
import com.ys.acsdev.commond.AppInfo;
import com.ys.acsdev.listener.SeekBarBackListener;
import com.ys.acsdev.poweronoff.PowerOnOffLocalActivity;
import com.ys.acsdev.setting.AboutActivity;
import com.ys.acsdev.setting.AppSettingsActivity;
import com.ys.acsdev.setting.AppUpdateActivity;
import com.ys.acsdev.setting.GeneralSettingsActivity;
import com.ys.acsdev.setting.ModifyPassActivity;
import com.ys.acsdev.setting.SettingMenuActivity;
import com.ys.acsdev.setting.adapter.SettingMenuAdapter;
import com.ys.acsdev.setting.entity.SettingEntity;
import com.ys.acsdev.ui.ModelActivity;
import com.ys.acsdev.ui.SplashActivity;
import com.ys.acsdev.util.LogUtil;
import com.ys.acsdev.util.SpUtils;
import com.ys.acsdev.util.SystemManagerUtil;
import com.ys.acsdev.util.VoiceManager;
import com.ys.acsdev.view.dialog.EditTextDialog;
import com.ys.acsdev.view.dialog.OridinryDialog;
import com.ys.acsdev.view.dialog.OridinryDialogClick;
import com.ys.acsdev.view.dialog.SeekBarDialog;
import com.ys.rkapi.MyManager;

import java.util.ArrayList;
import java.util.List;

public class AppSettingFragment extends BaseFragment implements AdapterView.OnItemClickListener {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = View.inflate(getActivity(), R.layout.fragment_default, null);
        initView(view);
        return view;
    }

    SettingMenuAdapter settingMenuAdapter;
    GridView grid_menu;
    List<SettingEntity> settingEntityList = null;
    SystemManagerUtil systemManagerUtil;

    private void initView(View view) {
        settingEntityList = getSettingData();
        grid_menu = (GridView) view.findViewById(R.id.grid_menu);
        settingMenuAdapter = new SettingMenuAdapter(getActivity(), settingEntityList);
        grid_menu.setAdapter(settingMenuAdapter);
        grid_menu.setOnItemClickListener(this);
        systemManagerUtil = new SystemManagerUtil(getActivity());
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
        int clickId = settingEntityList.get(position).getClickId();
        switch (clickId) {
            case SettingEntity.APP_UPDATE:
                startActivity(new Intent(getActivity(), AppUpdateActivity.class));
                break;
            case SettingEntity.MODIFY_PASSWORD:   //修改密碼
                startActivity(new Intent(getActivity(), ModifyPassActivity.class));
                break;
            case SettingEntity.TEST_CLOCKIN:  //打卡测试
//                startActivity(new Intent(getActivity(), ClockInActivity.class));
                break;
            case SettingEntity.GENERAL_SETTING:  //通用设置
                startActivity(new Intent(getActivity(), GeneralSettingsActivity.class));
                break;
            case SettingEntity.EXIT_APP:
                showExitAppDialog();
                break;
            case SettingEntity.TEST_SETTING:
                startActivity(new Intent(getActivity(), AppSettingsActivity.class));
                break;
            case SettingEntity.MODEL_CHANGE:
                startActivity(new Intent(getActivity(), ModelActivity.class));
                break;
            case SettingEntity.APP_INFO:
                startActivity(new Intent(getActivity(), AboutActivity.class));
                break;
            case SettingEntity.POWER_ON_LAUNCHER:
                startActivity(new Intent(getActivity(), PowerOnOffLocalActivity.class));
                break;
            case SettingEntity.DEV_LIGHT_SETTING://屏幕亮度
                showChangeLightDialog();
                break;
            case SettingEntity.DEV_VOICE_SETTING:  //音量设置
                showChangeVoiceDialog();
                break;
        }
    }

    private void showChangeVoiceDialog() {
        int curentVoice = VoiceManager.getCurrentVoiceNum(getActivity());
        LogUtil.cdl("======backNumInfo==" + curentVoice);
        SeekBarDialog seekBarDialog = new SeekBarDialog(getActivity());
        seekBarDialog.setOnDialogClickListener(new SeekBarBackListener() {
            @Override
            public void backNumInfo(int progress) {
                LogUtil.cdl("======backNumInfo==" + progress);
                VoiceManager.setMediaVoiceNum(getActivity(), progress);
            }
        });
        seekBarDialog.show(getString(R.string.voice_setting), curentVoice, 15);
    }

    /***
     * 修改屏幕亮度
     */
    private void showChangeLightDialog() {
        int curentLight = systemManagerUtil.getSystemBrightness();
        LogUtil.cdl("======backNumInfo==" + curentLight);
        SeekBarDialog seekBarDialog = new SeekBarDialog(getActivity());
        seekBarDialog.setOnDialogClickListener(new SeekBarBackListener() {
            @Override
            public void backNumInfo(int progress) {
                systemManagerUtil.changeAppBrightness(progress);
                LogUtil.cdl("======backNumInfo==" + progress);
            }
        });
        seekBarDialog.show(getString(R.string.light_setting), curentLight, 255);
    }

    /**
     * 客户要的密码验证
     */
    private void showExitPassDialog() {
        EditTextDialog editTextDialog = new EditTextDialog(getActivity());
        editTextDialog.setOnDialogClickListener(new EditTextDialog.EditTextDialogListener() {
            @Override
            public void commit(String content) {
                String savePass = SpUtils.getQinguan_password();
                if (!content.contains(savePass)) {
                    showToast(getString(R.string.pwd_failed_retry));
                    return;
                }
                SettingMenuActivity parentActivity = (SettingMenuActivity) getActivity();
                parentActivity.killMySlef();
            }

            @Override
            public void hiddleTestClick() {

            }
        });
        editTextDialog.show("密码验证", "", "确认", "请输入密码");
    }

    private void showExitAppDialog() {
        OridinryDialog oridinryDialog = new OridinryDialog(getActivity());
        oridinryDialog.setOnDialogClickListener(new OridinryDialogClick() {
            @Override
            public void sure() {
                MyManager manager = MyManager.getInstance(getActivity());
                manager.setSlideShowNotificationBar(true); //关闭下拉菜单
                manager.setSlideShowNavBar(true);  //关闭滑出底部菜单
                SettingMenuActivity parentActivity = (SettingMenuActivity) getActivity();
                parentActivity.killMySlef();
            }

            @Override
            public void noSure() {

            }
        });
        oridinryDialog.show(getString(R.string.exit_app_content), getString(R.string.submit), getString(R.string.cacel));
    }

    private List<SettingEntity> getSettingData() {
        List<SettingEntity> settingEntityList = new ArrayList<SettingEntity>();
        settingEntityList.add(new SettingEntity(SettingEntity.GENERAL_SETTING, getString(R.string.tv_general_setting), R.mipmap.ic_general_setting));
        //单机模式用的太多了，直接开放 。后期优化单机功能
        settingEntityList.add(new SettingEntity(SettingEntity.MODEL_CHANGE, getString(R.string.model_select), R.mipmap.icon_work_model));
        settingEntityList.add(new SettingEntity(SettingEntity.POWER_ON_LAUNCHER, getString(R.string.title_power), R.mipmap.ic_power));
        settingEntityList.add(new SettingEntity(SettingEntity.DEV_LIGHT_SETTING, getString(R.string.light_setting), R.mipmap.icon_light_setting));
        settingEntityList.add(new SettingEntity(SettingEntity.DEV_VOICE_SETTING, getString(R.string.voice_setting), R.mipmap.icon_voice_setting));
        if (SpUtils.getWorkModel() == AppInfo.MODEL_STAND_ALONE) {
            settingEntityList.add(new SettingEntity(SettingEntity.MODIFY_PASSWORD, getString(R.string.change_pwd), R.mipmap.ic_pwd_change));
        }
        settingEntityList.add(new SettingEntity(SettingEntity.TEST_SETTING, getString(R.string.app_test_setting), R.mipmap.ic_light));
        if (SpUtils.getWorkModel() == AppInfo.MODEL_NET) {
            settingEntityList.add(new SettingEntity(SettingEntity.APP_UPDATE, getString(R.string.app_update), R.mipmap.app_update));
        }
        settingEntityList.add(new SettingEntity(SettingEntity.APP_INFO, getString(R.string.device_info), R.mipmap.icon_setting_device));
        settingEntityList.add(new SettingEntity(SettingEntity.EXIT_APP, getString(R.string.exit_app), R.mipmap.ic_exit));
        return settingEntityList;
    }
}
