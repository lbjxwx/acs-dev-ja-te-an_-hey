package com.ys.acsdev.setting;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.ys.acsdev.R;
import com.ys.acsdev.bean.RedioEntity;
import com.ys.acsdev.databinding.ActivityConfigSettingsBinding;
import com.ys.acsdev.dialog.CardTypeDialog;
import com.ys.acsdev.dialog.TempSetting;
import com.ys.acsdev.listener.MoreButtonListener;
import com.ys.acsdev.listener.MoreButtonToggleListener;
import com.ys.acsdev.setting.entity.TempModelEntity;
import com.ys.acsdev.ui.BaseActivity;
import com.ys.acsdev.util.SpUtils;
import com.ys.acsdev.util.SystemManagerUtil;
import com.ys.acsdev.view.dialog.EditTextDialog;
import com.ys.acsdev.view.dialog.OridinryDialog;
import com.ys.acsdev.view.dialog.OridinryDialogClick;
import com.ys.acsdev.view.dialog.RadioListDialog;

import java.util.ArrayList;
import java.util.List;

public class ConfigSettingActivity extends BaseActivity {

    ActivityConfigSettingsBinding mBindView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBindView = ActivityConfigSettingsBinding.inflate(getLayoutInflater());
        setContentView(mBindView.getRoot());
        initView();
    }

    private void initView() {
        //公交卡刷開開關
        mBindView.btnCarCard.setOnMoretListener(new MoreButtonToggleListener() {
            @Override
            public void switchToggleView(View view, boolean isChooice) {
                 SpUtils.setCarCardEnable(isChooice);
                  initData();
            }
        });

        mBindView.btnTest.setOnMoretListener(new MoreButtonToggleListener() {
            @Override
            public void switchToggleView(View view, boolean isChooice) {

            }
        });

        mBindView.mIvBack.setOnClickListener(v -> finish());
        mBindView.viewHidenBtn.setOnLongClickListener(v -> {
            startActivity(new Intent(this, SettingMenuActivity.class));
            return true;
        });
        mBindView.btnTemp.setOnMoretListener(new MoreButtonListener() {
            @Override
            public void clickView(View view) {
                TempSetting tempSetting = new TempSetting(ConfigSettingActivity.this);
                tempSetting.getDialog().setOnDismissListener(dialog -> {
                    mBindView.btnTemp.setRigt(SpUtils.getmTempType() == TempModelEntity.CLOSE ? "已关闭" : "已打开");
                });
                tempSetting.show();
            }
        });
        mBindView.btnLight.setOnMoretListener(new MoreButtonToggleListener() {
            @Override
            public void switchToggleView(View view, boolean isChooice) {
                SpUtils.setWhiteLightEnable(isChooice);
            }
        });
        mBindView.mBtnMaskCheck.setOnMoretListener(new MoreButtonListener() {
            @Override
            public void clickView(View view) {
                RadioListDialog radioListDialog = new RadioListDialog(ConfigSettingActivity.this);
                List<RedioEntity> listShow = new ArrayList<RedioEntity>();
                listShow.add(new RedioEntity("口罩不限制"));
                listShow.add(new RedioEntity("可过闸提醒戴口罩"));
                listShow.add(new RedioEntity("必须戴口罩过闸"));

                int selected = 0;
                if (SpUtils.getmMaskEnable()) {
                    selected = 1;
                    if (SpUtils.getMaskIntercept()) {
                        selected = 2;
                    }
                }
                radioListDialog.show(
                        getString(R.string.check_mask),
                        listShow,
                        selected,
                        ""
                );
                radioListDialog.setRadioChooiceListener((redioEntity, position) -> {
                    SpUtils.setMaskEnable(position > 0);
                    SpUtils.setMaskIntercept(position > 1);
                    initData();
                });
            }
        });

        mBindView.btnDevSn.setOnMoretListener(new MoreButtonListener() {
            @Override
            public void clickView(View view) {
                EditTextDialog editTextDialog = new EditTextDialog(ConfigSettingActivity.this);
                editTextDialog.setOnDialogClickListener(new EditTextDialog.EditTextDialogListener() {
                    @Override
                    public void commit(String content) {
                        if (content.trim().length() < 1) {
                            showToast("设备SN不能为空");
                            return;
                        }
                        SpUtils.setMqttSn(content);
                        showToast(getString(R.string.set_success));
                    }

                    @Override
                    public void hiddleTestClick() {

                    }
                });
                editTextDialog.show("设备SN", SpUtils.getMqttSn(), getString(R.string.confirm), "");
            }
        });

        mBindView.btnFaceCard.setOnMoretListener(new MoreButtonToggleListener() {
            @Override
            public void switchToggleView(View view, boolean isChooice) {
                SpUtils.setFaceAndCardEnable(isChooice);
            }
        });

        mBindView.btnCodeChannel.setOnMoretListener(new MoreButtonListener() {
            @Override
            public void clickView(View view) {
                RadioListDialog radioListDialog = new RadioListDialog(ConfigSettingActivity.this);
                List<RedioEntity> listShow = new ArrayList<RedioEntity>();
                listShow.add(new RedioEntity("闽政通通道"));
                listShow.add(new RedioEntity("国办通道"));

                int selected = 0;
                if (SpUtils.getHealthPlatform() == 0) {
                    selected = 1;
                }
                radioListDialog.show(
                        "核验通道",
                        listShow,
                        selected,
                        ""
                );
                radioListDialog.setRadioChooiceListener((redioEntity, position) -> {
                    SpUtils.setHealthPlatform(position == 0 ? 1 : 0);
                    initData();
                });
            }
        });

        mBindView.btnDataReply.setOnMoretListener(new MoreButtonToggleListener() {
            @Override
            public void switchToggleView(View view, boolean isChooice) {
                SpUtils.setUploadFace(isChooice);
                SpUtils.setDataReplyEnable(isChooice);
                initData();
            }
        });

        mBindView.btnTextTip.setOnMoretListener(new MoreButtonListener() {
            @Override
            public void clickView(View view) {
                EditTextDialog editTextDialog = new EditTextDialog(ConfigSettingActivity.this);
                editTextDialog.setOnDialogClickListener(new EditTextDialog.EditTextDialogListener() {
                    @Override
                    public void commit(String content) {
                        if (content.trim().length() < 1) {
                            showToast("文字提示不能为空");
                            return;
                        }
                        SpUtils.setTextTip(content);
                        showToast(getString(R.string.set_success));
                    }

                    @Override
                    public void hiddleTestClick() {

                    }
                });
                String showContent = SpUtils.getTextTip();
                editTextDialog.show("准备扫码或刷卡前提示", showContent, getString(R.string.confirm), "");
            }
        });

        mBindView.btnRongUrl.setOnMoretListener(new MoreButtonListener() {
            @Override
            public void clickView(View view) {
                EditTextDialog editTextDialog = new EditTextDialog(ConfigSettingActivity.this);
                editTextDialog.setOnDialogClickListener(new EditTextDialog.EditTextDialogListener() {
                    @Override
                    public void commit(String content) {
                        if (content.trim().length() < 1) {
                            showToast("身份证认证接口地址不能为空");
                            return;
                        }
                        if (!content.startsWith("http")) {
                            content = "http://" + content;
                        }
                        SpUtils.setRongCardUrl(content);
                        showToast(getString(R.string.set_success));
                    }

                    @Override
                    public void hiddleTestClick() {

                    }
                });
                String showContent = SpUtils.getRongCardUrl();
                editTextDialog.show("榕城通身份证认证接口地址", showContent, getString(R.string.confirm), "");
            }
        });

        mBindView.btnCardType.setOnMoretListener(new MoreButtonListener() {
            @Override
            public void clickView(View view) {
                CardTypeDialog dialog = new CardTypeDialog(ConfigSettingActivity.this);
                dialog.show();
            }
        });

        mBindView.mBtnSave.setOnClickListener(v -> showRebootDialog());
    }

    @Override
    protected void onStart() {
        super.onStart();
        initData();
    }

    private void initData() {
        mBindView.mBtnMaskCheck.setRigt("口罩不限制");
        if (SpUtils.getmMaskEnable()) {
            mBindView.mBtnMaskCheck.setRigt("可过闸提醒戴口罩");
            if (SpUtils.getMaskIntercept()) {
                mBindView.mBtnMaskCheck.setRigt("必须戴口罩过闸");
            }
        }
        mBindView.btnCarCard.setChcekcAble(SpUtils.getCarCardEnable());
        mBindView.btnLight.setChcekcAble(SpUtils.getWhiteLightEnable());
        mBindView.btnFaceCard.setChcekcAble(SpUtils.getFaceAndCardEnable());
        mBindView.btnCodeChannel.setRigt(SpUtils.getHealthPlatform() == 0 ? "国办通道" : "闽政通通道");
        mBindView.btnDataReply.setChcekcAble(SpUtils.getDataReplyEnable());
        mBindView.btnTemp.setRigt(SpUtils.getmTempType() == TempModelEntity.CLOSE ? "已关闭" : "已打开");

    }

    /****
     * 重启弹窗
     */
    public void showRebootDialog() {
        OridinryDialog oridinryDialog = new OridinryDialog(this);
        oridinryDialog.setOnDialogClickListener(new OridinryDialogClick() {
            @Override
            public void sure() {
                finish();
                SystemManagerUtil.rebootApp(getApplication());
            }

            @Override
            public void noSure() {

            }
        });
        oridinryDialog.show(getString(R.string.reboot_msg), getString(R.string.reboot), getString(R.string.cancel));
    }
}
