package com.ys.acsdev.setting.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import com.ys.acsdev.BuildConfig;
import com.ys.acsdev.R;
import com.ys.acsdev.api.AppConfig;
import com.ys.acsdev.attendance.ClockInActivity;
import com.ys.acsdev.attendance.ClockInRecordActivity;
import com.ys.acsdev.commond.AppInfo;
import com.ys.acsdev.setting.AccessOffRecordActivity;
import com.ys.acsdev.setting.FaceInfoActivity;
import com.ys.acsdev.setting.FaceSettingsActivity;
import com.ys.acsdev.setting.ServerLnkActivity;
import com.ys.acsdev.setting.TempSettingsActivity;
import com.ys.acsdev.setting.VisitorInfoActivity;
import com.ys.acsdev.setting.VoiceSettingsActivity;
import com.ys.acsdev.setting.adapter.SettingMenuAdapter;
import com.ys.acsdev.setting.entity.SettingEntity;
import com.ys.acsdev.ui.AccessRecordActivity;
import com.ys.acsdev.util.LogUtil;
import com.ys.acsdev.util.SpUtils;
import com.ys.acsdev.view.dialog.EditTextDialog;

import java.util.ArrayList;
import java.util.List;

public class DefaultSettingFragment extends BaseFragment implements AdapterView.OnItemClickListener {

    View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = View.inflate(getActivity(), R.layout.fragment_default, null);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        initView();
    }

    SettingMenuAdapter settingMenuAdapter;
    GridView grid_menu;
    List<SettingEntity> settingEntityList = null;

    private void initView() {
        settingEntityList = getSettingData();
        grid_menu = (GridView) view.findViewById(R.id.grid_menu);
        settingMenuAdapter = new SettingMenuAdapter(getActivity(), settingEntityList);
        grid_menu.setAdapter(settingMenuAdapter);
        grid_menu.setOnItemClickListener(this);
    }

    private List<SettingEntity> getSettingData() {
        List<SettingEntity> settingEntityList = new ArrayList<SettingEntity>();
        if (SpUtils.getWorkModel() == AppInfo.MODEL_NET)
            settingEntityList.add(new SettingEntity(SettingEntity.SERVER_LINE, getString(R.string.server_connection), R.mipmap.ic_server));
        settingEntityList.add(new SettingEntity(SettingEntity.FACE_SETTING, getString(R.string.face_set), R.mipmap.ic_face_setting));
//        settingEntityList.add(new SettingEntity(SettingEntity.RED_LIGHT_SETTING, getString(R.string.readline_set), R.mipmap.ic_readline));
        settingEntityList.add(new SettingEntity(SettingEntity.TEMP_CHECK, getString(R.string.temperature_setting), R.mipmap.ic_temperate));
        settingEntityList.add(new SettingEntity(SettingEntity.PERSON_INFO, getString(R.string.person_info), R.mipmap.ic_face_info));
        settingEntityList.add(new SettingEntity(SettingEntity.ACCESS_RECORD, getString(R.string.access_record), R.mipmap.ic_accessrecord));
        if (SpUtils.getAttendEnable()) {
            settingEntityList.add(new SettingEntity(SettingEntity.CLOCKIN_RECORD, getString(R.string.attend_record), R.mipmap.icon_work_acrrod));
        }
        settingEntityList.add(new SettingEntity(SettingEntity.VOICE_SETTING, getString(R.string.voice_settings), R.mipmap.icon_voice));
        if (SpUtils.getWorkModel() == AppInfo.MODEL_NET) {
            settingEntityList.add(new SettingEntity(SettingEntity.VISITOR_INFO, getString(R.string.visitor_info), R.mipmap.icon_face_visitors));
            settingEntityList.add(new SettingEntity(SettingEntity.BLACK_PERSON, getString(R.string.black_person), R.mipmap.icon_black_list));
        }
        if (BuildConfig.DEBUG) {
            settingEntityList.add(new SettingEntity(SettingEntity.TEST_CLOCKIN, "模拟打卡", R.mipmap.icon_work_acrrod));
        }
        return settingEntityList;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
        int clickId = settingEntityList.get(position).getClickId();
        switch (clickId) {
            case SettingEntity.SERVER_LINE:   //鏈接服務器
                startActivity(new Intent(getActivity(), ServerLnkActivity.class));
                break;
            case SettingEntity.FACE_SETTING:  //人臉設置
                startActivity(new Intent(getActivity(), FaceSettingsActivity.class));
                break;
//            case SettingEntity.RED_LIGHT_SETTING: //紅外補光開關設置
//                startActivity(new Intent(getActivity(), ReadLineSettingsActivity.class));
//                break;
            case SettingEntity.TEMP_CHECK:   //溫度檢測設置
                startActivity(new Intent(getActivity(), TempSettingsActivity.class));
                break;
            case SettingEntity.PERSON_INFO:   //人員信息
                LogUtil.cdl("===========点击人员信息====");
                startActivity(new Intent(getActivity(), FaceInfoActivity.class));
                break;
            case SettingEntity.ACCESS_RECORD:   //人員信息
                if (SpUtils.getWorkModel() == AppInfo.MODEL_NET)
                    startActivity(new Intent(getActivity(), AccessRecordActivity.class));
                else
                    startActivity(new Intent(getActivity(), AccessOffRecordActivity.class));
                break;
            case SettingEntity.VOICE_SETTING:   //语音设置
                startActivity(new Intent(getActivity(), VoiceSettingsActivity.class));
                break;
            case SettingEntity.VISITOR_INFO:   //访客信息
                Intent intentVisitor = new Intent();
                intentVisitor.setClass(getActivity(), VisitorInfoActivity.class);
                intentVisitor.putExtra("PersonType", AppInfo.FACE_REGISTER_VISITOR);
                startActivity(intentVisitor);
                break;
            case SettingEntity.BLACK_PERSON:  //黑名单
                Intent intentblack = new Intent();
                intentblack.setClass(getActivity(), VisitorInfoActivity.class);
                intentblack.putExtra("PersonType", AppInfo.FACE_REGISTER_BLACK_LIST);
                startActivity(intentblack);
                break;
            case SettingEntity.CLOCKIN_RECORD:  //打卡记录
                startActivity(new Intent(getActivity(), ClockInRecordActivity.class));
                break;
            case SettingEntity.TEST_CLOCKIN:  //打卡记录
                startActivity(new Intent(getActivity(), ClockInActivity.class));
                break;
        }
    }

    /**
     * 客户要的密码验证
     *
     * @param faceSetting
     */
    private void showPaddwordRecorder(int faceSetting) {
        EditTextDialog editTextDialog = new EditTextDialog(getActivity());
        editTextDialog.setOnDialogClickListener(new EditTextDialog.EditTextDialogListener() {
            @Override
            public void hiddleTestClick() {

            }

            @Override
            public void commit(String content) {
                String savePass = SpUtils.getQinguan_password();
                if (!content.contains(savePass)) {
                    showToast(getString(R.string.pwd_failed_retry));
                    return;
                }
                if (faceSetting == SettingEntity.FACE_SETTING) {
                    startActivity(new Intent(getActivity(), FaceSettingsActivity.class));
                } else if (faceSetting == SettingEntity.TEMP_CHECK) {
                    startActivity(new Intent(getActivity(), TempSettingsActivity.class));
                }
            }
        });
        editTextDialog.show("密码验证", "", "确认", "请输入密码");
    }
}
