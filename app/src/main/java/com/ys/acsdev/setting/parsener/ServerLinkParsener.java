package com.ys.acsdev.setting.parsener;

import android.content.Context;
import android.text.TextUtils;

import com.ys.acsdev.R;
import com.ys.acsdev.bean.DeviceBean;
import com.ys.acsdev.commond.AppInfo;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;
import com.ys.acsdev.util.CodeUtil;
import com.ys.acsdev.util.LogUtil;
import com.ys.acsdev.util.NetWorkUtils;
import com.ys.acsdev.util.SpUtils;

import org.json.JSONObject;

import okhttp3.Call;

public class ServerLinkParsener {

    ServerLinkView serverLinkView;
    Context context;

    public ServerLinkParsener(Context context, ServerLinkView serverLinkView) {
        this.context = context;
        this.serverLinkView = serverLinkView;
    }

    public void registerEquipmentToServer(String ipAddress, String port, String userName) {
        if (!NetWorkUtils.isNetworkConnected(context)) {
            showToast("NetWork Error, Please Check ");
            return;
        }
        boolean jujle = jujleInfoRight(ipAddress, port);
        if (!jujle) {
            return;
        }
        showWaitDialogUtil(true);
        String requestUrl = AppInfo.getRegisterEquipment();
        String softVersion = "";
        String sn = CodeUtil.getUniquePsuedoID();
        String equipName = sn;
        String ip = "";
        String longitude = "0";
        String latitude = "0";
        String userAccount = userName;
        DeviceBean deviceBean = SpUtils.getDeviceInfo();
        if (deviceBean != null) {
            equipName = deviceBean.getEquipName();
        }
        OkHttpUtils
                .post()
                .url(requestUrl)
                .addParams("equipName", equipName)
                .addParams("softVersion", softVersion + "")
                .addParams("sn", sn + "")
                .addParams("ip", ip)
                .addParams("longitude", longitude)
                .addParams("latitude", latitude)
                .addParams("userAccount", userAccount)
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, String e, int id) {
                        LogUtil.cdl("======注册失败=====" + e.toString());
                        if (serverLinkView == null) {
                            return;
                        }
                        serverLinkView.checkRegisterStatues(false, "Register Fialed : " + e.toString());
                    }

                    @Override
                    public void onResponse(String json, int id) {
                        LogUtil.cdl("======注册成功=====" + json);
                        if (json == null || json.length() < 5) {
                            showToast("Register Fialed : json==null");
                            return;
                        }
                        try {
                            JSONObject jsonObject = new JSONObject(json);
                            int code = jsonObject.getInt("code");
                            String msg = jsonObject.getString("msg");
                            if (code != 0) {
                                showToast("Register Frailed: " + msg);
                                serverLinkView.checkRegisterStatues(false, msg);
                                return;
                            }
                            serverLinkView.checkRegisterStatues(true, msg);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
    }

    public boolean jujleInfoRight(String ipAddress, String port) {
        if (TextUtils.isEmpty(ipAddress) || ipAddress.length() < 2) {
            showToast(context.getString(R.string.input_ip_port));
            return false;
        }
        if (TextUtils.isEmpty(port) || port.length() < 2) {
            showToast(context.getString(R.string.input_ip_port));
            return false;
        }
        ipAddress = ipAddress.replace(" ", "").trim();
        SpUtils.setmServerIp(ipAddress);
        SpUtils.setmServerPort(port);
        return true;
    }

    private void showWaitDialogUtil(boolean b) {
        if (serverLinkView == null) {
            return;
        }
        serverLinkView.showWaitDialog(b);
    }

    private void showToast(String desc) {
        if (serverLinkView == null) {
            return;
        }
        serverLinkView.showToastView(desc);
    }

}
