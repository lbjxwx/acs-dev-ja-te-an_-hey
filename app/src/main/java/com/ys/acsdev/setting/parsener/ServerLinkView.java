package com.ys.acsdev.setting.parsener;

public interface ServerLinkView {

    void showToastView(String desc);

    void showWaitDialog(boolean isShow);

    void checkRegisterStatues(boolean isTrue, String errorDesc);

}
