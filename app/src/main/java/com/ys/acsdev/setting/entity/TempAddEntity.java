package com.ys.acsdev.setting.entity;


import org.litepal.crud.LitePalSupport;

public class TempAddEntity extends LitePalSupport {

    String startTime;  //开始时间
    String endTime;    //结束时间
    float addTemp;   //补偿值
    String sateTimeId;  //存储ID
    boolean isChlooice;

    public TempAddEntity(String startTime, String endTime, float addTemp, String sateTimeId) {
        this.startTime = startTime;
        this.endTime = endTime;
        this.addTemp = addTemp;
        this.sateTimeId = sateTimeId;
    }

    public boolean isChlooice() {
        return isChlooice;
    }

    public void setChlooice(boolean chlooice) {
        isChlooice = chlooice;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public float getAddTemp() {
        return addTemp;
    }

    public void setAddTemp(float addTemp) {
        this.addTemp = addTemp;
    }

    public String getSateTimeId() {
        return sateTimeId;
    }

    public void setSateTimeId(String sateTimeId) {
        this.sateTimeId = sateTimeId;
    }

    @Override
    public String toString() {
        return "TempAddEntity{" +
                "startTime=" + startTime +
                ", endTime=" + endTime +
                ", addTemp=" + addTemp +
                ", sateTimeId=" + sateTimeId +
                '}';
    }
}
