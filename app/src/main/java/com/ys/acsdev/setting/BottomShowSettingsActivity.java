package com.ys.acsdev.setting;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.ys.acsdev.R;
import com.ys.acsdev.api.AppConfig;
import com.ys.acsdev.bean.DeviceBean;
import com.ys.acsdev.bean.RedioEntity;
import com.ys.acsdev.listener.MoreButtonListener;
import com.ys.acsdev.listener.MoreButtonToggleListener;
import com.ys.acsdev.listener.RadioChooiceListener;
import com.ys.acsdev.ui.BaseActivity;
import com.ys.acsdev.util.SpUtils;
import com.ys.acsdev.view.MoreButton;
import com.ys.acsdev.view.MoreButtonToggle;
import com.ys.acsdev.view.dialog.ModifyCompanyDialog;
import com.ys.acsdev.view.dialog.RadioListDialog;

import java.util.ArrayList;
import java.util.List;

public class BottomShowSettingsActivity extends BaseActivity implements View.OnClickListener, MoreButtonToggleListener, MoreButtonListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bottom_settings);
        initView();
        updateDateInfo();
    }

    ImageView mIvBack;
    MoreButtonToggle btn_bottom_info_show, btn_show_face_rect, btn_setting_simple_model, btn_hide_wifi, btnFaceBgrect;
    MoreButtonToggle btn_recog_show, btn_simple_model, btn_hide_temp_number, btn_show_ad_info;
    MoreButton more_show_logo_image, more_modify_company;


    @Override
    public void clickView(View view) {
        switch (view.getId()) {
            case R.id.more_show_logo_image:
                showModifyLogoOrImageDialog();
                break;
            case R.id.more_modify_company:
                showModifyCompanyInfoDialog();
                break;
        }
    }

    private void showModifyCompanyInfoDialog() {
        DeviceBean deviceBean = SpUtils.getDeviceInfo();
        String company = "";
        String departMent = "";
        String devNickName = "";
        if (deviceBean != null) {
            company = deviceBean.getCompanyName();
            departMent = deviceBean.getDeptName();
            devNickName = deviceBean.getEquipName();
        }
        ModifyCompanyDialog modifyCompanyDialog = new ModifyCompanyDialog(BottomShowSettingsActivity.this);
        modifyCompanyDialog.show(company, departMent, devNickName);
        modifyCompanyDialog.setModifyCompanyLitener(new ModifyCompanyDialog.ModifyCompanyLitener() {
            @Override
            public void backCompanyInfo(String company, String department, String nickName) {
                deviceBean.setCompanyName(company);
                deviceBean.setDeptName(department);
                deviceBean.setEquipName(nickName);
                SpUtils.setDeviceInfo(deviceBean, "手动输入现实信息");
                showToast(getString(R.string.set_success));
            }
        });
    }

    private void initView() {
        btn_hide_wifi = findViewById(R.id.btn_hide_wifi);
        btn_hide_wifi.setOnMoretListener(this);
        btn_setting_simple_model = (MoreButtonToggle) findViewById(R.id.btn_setting_simple_model);
        btn_setting_simple_model.setOnMoretListener(this);
        more_show_logo_image = (MoreButton) findViewById(R.id.more_show_logo_image);
        more_show_logo_image.setOnMoretListener(this);
        more_modify_company = (MoreButton) findViewById(R.id.more_modify_company);
        more_modify_company.setOnMoretListener(this);
        btn_bottom_info_show = (MoreButtonToggle) findViewById(R.id.btn_bottom_info_show);
        btn_bottom_info_show.setOnMoretListener(this);
        btn_show_ad_info = (MoreButtonToggle) findViewById(R.id.btn_show_ad_info);
        btn_show_ad_info.setOnMoretListener(this);
        btn_show_face_rect = (MoreButtonToggle) findViewById(R.id.btn_show_face_rect);
        btn_show_face_rect.setOnMoretListener(this);
        btnFaceBgrect = findViewById(R.id.btn_face_bgrect);
        btnFaceBgrect.setOnMoretListener(this);
        btn_simple_model = findViewById(R.id.btn_simple_model);
        btn_simple_model.setOnMoretListener(this);
        btn_recog_show = findViewById(R.id.btn_recog_show);
        btn_recog_show.setOnMoretListener(this);
        mIvBack = (ImageView) findViewById(R.id.mIvBack);
        mIvBack.setOnClickListener(this);
        btn_hide_temp_number = findViewById(R.id.btn_hide_temp_number);
        btn_hide_temp_number.setOnMoretListener(this);
        btn_hide_wifi.setVisibility(View.GONE);
    }


    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void switchToggleView(View view, boolean isChooice) {
        switch (view.getId()) {
            case R.id.btn_show_ad_info:
                SpUtils.setShowAdInfo(isChooice);
                break;
            case R.id.btn_setting_simple_model:
                SpUtils.setSettingSimpleModel(isChooice);
                break;
            case R.id.btn_show_face_rect:
                SpUtils.setShowFaceRectView(isChooice);
                break;
            case R.id.btn_face_bgrect:
                SpUtils.setShowFaceBgRectView(isChooice);
                break;
            case R.id.btn_bottom_info_show:  //底部信息显示
                SpUtils.setInfoEnable(isChooice);
                break;
            case R.id.btn_recog_show:  //陌生人只显示测温
                SpUtils.setRecogShowEnable(isChooice);
                break;
            case R.id.btn_simple_model:
                SpUtils.setSimpleModel(isChooice);
                if (isChooice) {
                    //间间模式，不播放人名
                    SpUtils.setSpeakName(false);
                }
                break;
            case R.id.btn_hide_temp_number:
                SpUtils.setHideTempNumberEnable(isChooice);
                break;
            case R.id.btn_hide_wifi:
                SpUtils.setHideWifiIcon(isChooice);
                break;
        }
        updateDateInfo();
    }

    private void updateDateInfo() {
        btn_show_ad_info.setChcekcAble(SpUtils.getShowAdInfo());
        more_show_logo_image.setRigt(SpUtils.getShowImageType() == 0 ? getString(R.string.show_logo) : getString(R.string.show_face));
        btn_simple_model.setChcekcAble(SpUtils.getSimpleModel());
        btn_bottom_info_show.setChcekcAble(SpUtils.getInfoEnable());
        btn_show_face_rect.setChcekcAble(SpUtils.getShowFaceRectView());
        btnFaceBgrect.setChcekcAble(SpUtils.getShowFaceBgRectView());
        btn_recog_show.setChcekcAble(SpUtils.getRecogShowEnable());
        btn_hide_temp_number.setChcekcAble(SpUtils.getHideTempNumberEnable());
        btn_setting_simple_model.setChcekcAble(SpUtils.getSettingSimpleModel());
        btn_hide_wifi.setChcekcAble(SpUtils.getHideWifiIcon());
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.mIvBack:
                finish();
                break;
        }
    }

    private void showModifyLogoOrImageDialog() {
        RadioListDialog radioListDialog = new RadioListDialog(BottomShowSettingsActivity.this);
        List<RedioEntity> listShow = new ArrayList<RedioEntity>();
        listShow.add(new RedioEntity(getString(R.string.show_logo)));
        listShow.add(new RedioEntity(getString(R.string.show_face)));
        int checkPosition = SpUtils.getShowImageType();
        String showDesc = getString(R.string.show_type);
        radioListDialog.show(
                getString(R.string.show_type),
                listShow,
                checkPosition,
                showDesc
        );
        radioListDialog.setRadioChooiceListener(new RadioChooiceListener() {
            @Override
            public void backChooiceInfo(RedioEntity redioEntity, int chooicePosition) {
                SpUtils.setShowImageType(chooicePosition);
                updateDateInfo();
            }
        });
    }
}
