package com.ys.acsdev.setting;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;

import com.ys.acsdev.R;
import com.ys.acsdev.listener.AdapterItemClickListener;
import com.ys.acsdev.setting.adapter.LanguageAdapter;
import com.ys.acsdev.setting.entity.LanguageEntity;
import com.ys.acsdev.ui.BaseActivity;
import com.ys.acsdev.util.language.LanguageChangeUtil;
import com.ys.acsdev.util.SpUtils;
import com.ys.acsdev.view.dialog.OridinryDialog;
import com.ys.acsdev.view.dialog.OridinryDialogClick;

import java.util.List;

public class LanguageChangeActivity extends BaseActivity implements View.OnClickListener, AdapterItemClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_language);
        initView();
    }

    ImageView mIvBack;
    ListView lv_language_content;
    LanguageAdapter adapter;
    LanguageChangeUtil languageChangeUtil;

    private void initView() {
        languageChangeUtil = new LanguageChangeUtil(LanguageChangeActivity.this);
        mIvBack = (ImageView) findViewById(R.id.mIvBack);
        mIvBack.setOnClickListener(this);
        lv_language_content = (ListView) findViewById(R.id.lv_language_content);
        List<LanguageEntity> languageEntityList = languageChangeUtil.getLanguageInfo();
        adapter = new LanguageAdapter(LanguageChangeActivity.this, languageEntityList);
        lv_language_content.setAdapter(adapter);
        adapter.setOnAdapterItemClickListener(this);
    }


    @Override
    public void adapterItemClick(int id, Object object, int clickPosition) {
        if (clickPosition == 2) {
            showToast("当前不支持该语言");
            return;
        }
        int currentPosition = SpUtils.getLanguageDesc();
        if (currentPosition == clickPosition) {
            showToast("请不要重复设置");
            return;
        }
        OridinryDialog oridinryDialog = new OridinryDialog(LanguageChangeActivity.this);
        oridinryDialog.setOnDialogClickListener(new OridinryDialogClick() {
            @Override
            public void sure() {

                SpUtils.setLanguageDesc(clickPosition);
                showToast("设置语言成功");
                languageChangeUtil.changeLanguage();
                adapter.notifyDataSetChanged();
            }

            @Override
            public void noSure() {

            }
        });
        oridinryDialog.show("是否切换当前语言?", "确定", "取消");
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.mIvBack:
                finish();
                break;
        }
    }
}
