package com.ys.acsdev.setting;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.ys.acsdev.R;
import com.ys.acsdev.api.AppConfig;
import com.ys.acsdev.bean.DeviceBean;
import com.ys.acsdev.commond.AppInfo;
import com.ys.acsdev.manager.ImageManager;
import com.ys.acsdev.ui.BaseActivity;
import com.ys.acsdev.util.CodeUtil;
import com.ys.acsdev.util.SpUtils;

import java.io.File;

// Created by kermitye on 2020/6/23 13:52
public class AboutActivity extends BaseActivity {

    TextView mTvName, mTvMac, mTvCompany, mTvDepartment, mLabelCompany, mLabelDep, tv_app_version;
    ImageView mIvLogo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_new);
        initView();
        initData();
    }

    private void initView() {
        tv_app_version = (TextView) findViewById(R.id.tv_app_version);
        mTvName = (TextView) findViewById(R.id.tv_name);
        mTvMac = (TextView) findViewById(R.id.tv_mac);
        mTvCompany = (TextView) findViewById(R.id.tv_company);
        mTvDepartment = (TextView) findViewById(R.id.tv_department);
        mIvLogo = (ImageView) findViewById(R.id.iv_logo);
        mLabelCompany = (TextView) findViewById(R.id.label_company);
        mLabelDep = (TextView) findViewById(R.id.label_department);
        findViewById(R.id.iv_back).setOnClickListener(view -> onBackPressed());
    }

    private void initData() {
        tv_app_version.setText(CodeUtil.getSystCodeVersion(AboutActivity.this));
        mTvMac.setText(CodeUtil.getUniquePsuedoID());
        DeviceBean deviceInfo = SpUtils.getDeviceInfo();
        if (deviceInfo != null) {
            mTvName.setText(deviceInfo.getEquipName());
            mTvCompany.setText(deviceInfo.getCompanyName());
            mTvDepartment.setText(deviceInfo.getDeptName());
        }
        File fileShow = new File(AppInfo.LOGO_PATH);
        int defaultLogo = R.mipmap.app_icon;
        ImageManager.displayImageNoCache(fileShow, mIvLogo, defaultLogo);
    }

}
