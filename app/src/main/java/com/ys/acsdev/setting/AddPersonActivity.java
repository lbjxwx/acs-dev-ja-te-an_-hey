package com.ys.acsdev.setting;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;

import com.ys.acsdev.R;
import com.ys.acsdev.api.AppConfig;
import com.ys.acsdev.bean.DeviceBean;
import com.ys.acsdev.bean.FaceInfo;
import com.ys.acsdev.bean.PersonBean;
import com.ys.acsdev.commond.AppInfo;
import com.ys.acsdev.db.dao.FaceInfoJavaDao;
import com.ys.acsdev.db.dao.PersonDao;
import com.ys.acsdev.helper.SoundHelper;
import com.ys.acsdev.manager.ImageManager;
import com.ys.acsdev.runnable.upload.UpdateAddPersonInfoRunnable;
import com.ys.acsdev.runnable.upload.UpdateImageListener;
import com.ys.acsdev.service.AcsService;
import com.ys.acsdev.ui.BaseActivity;
import com.ys.acsdev.util.FileUtils;
import com.ys.acsdev.util.LogUtil;
import com.ys.acsdev.util.SimpleDateUtil;
import com.ys.acsdev.util.SpUtils;
import com.ys.acsdev.view.dialog.PopBottomDialog;
import com.ys.config.ImageConfig;
import com.ys.facepasslib.FaceManager;
import com.ys.facepasslib.IRegisterFaceListener;
import com.ys.imgcroplib.Crop;
import com.ys.zip.ImageUtil;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.IOException;

public class AddPersonActivity extends BaseActivity implements View.OnClickListener {

    ImageView mIvBack;
    EditText mEtName, mEtPhone;
    RadioGroup mRgSex;
    ImageView mIvFace;
    Button mBtnAdd;
    String mSex = "0";  //默认女
    ImageUtil imageUtil;
    String mImgPath;
    EditText et_card_num;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_person);
        initView();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    private void initView() {
        et_card_num = (EditText) findViewById(R.id.et_card_num);
        mEtName = (EditText) findViewById(R.id.mEtName);
        mEtPhone = (EditText) findViewById(R.id.mEtPhone);
        mRgSex = (RadioGroup) findViewById(R.id.mRgSex);
        mIvFace = (ImageView) findViewById(R.id.mIvFace);
        mBtnAdd = (Button) findViewById(R.id.mBtnAdd);
        mIvBack = (ImageView) findViewById(R.id.mIvBack);
        mBtnAdd.setOnClickListener(this);
        mIvBack.setOnClickListener(this);
        mIvFace.setOnClickListener(this);
        mRgSex.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int viewId) {
                switch (viewId) {
                    case R.id.mRbFemale:
                        mSex = "0";
                        break;
                    case R.id.mRbMale:
                        mSex = "1";
                        break;
                }
                Log.e("haha", "=============" + viewId);
            }
        });
        imageUtil = new ImageUtil(AddPersonActivity.this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.mIvFace:
                pickPhoto();
                break;
            case R.id.mBtnAdd:
                addPerson();
                break;
            case R.id.mIvBack:
                finish();
                break;
        }
    }

    File mTempFile;  //文件上传缓存的文件

    private void addPerson() {
        if (mTempFile == null || !mTempFile.exists()) {
            showToast(getString(R.string.select_face));
            return;
        }
        String cardNum = getCaredNum();
        String username = getUserName();
        String mobilePhonr = getPhoneNum();
        if (username == null || username.length() < 2) {
            showToast(getString(R.string.input_name));
            return;
        }
        if (mobilePhonr == null || mobilePhonr.length() < 6) {
            showToast(getString(R.string.input_phone));
            return;
        }
        if (SpUtils.getWorkModel() == AppInfo.MODEL_STAND_ALONE) {
            localRegister(username, mobilePhonr, getCaredNum());
            return;
        }
        DeviceBean deviceInfo = SpUtils.getDeviceInfo();
        if (deviceInfo == null) {
            showToast(getString(R.string.device_is_null));
            return;
        }
        LogUtil.persoon("====提交人员信息====" + deviceInfo.toString());
        String deviceId = deviceInfo.getDeptId();
        if (deviceId == null || deviceId.length() < 2) {
            showToast(getString(R.string.device_is_null));
            return;
        }
        showWait(getString(R.string.adding));
        String filePathUpdate = mTempFile.getAbsolutePath();
        UpdateAddPersonInfoRunnable runnable = new UpdateAddPersonInfoRunnable(deviceId, username, cardNum, mobilePhonr, mSex, filePathUpdate, getCaredNum(), new UpdateImageListener() {

            @Override
            public void updateImageFailed(String errorDesc) {
                hideWait();
                showToast("Update Error: " + errorDesc);
                LogUtil.update("========上传文件失败====" + errorDesc);
            }

            @Override
            public void updateImageProgress(int progress) {
            }

            @Override
            public void updateImageSuccess(String desc) {
                LogUtil.update("========上传文件成功===" + desc);
                hideWait();
                showToast(getString(R.string.add_success));
                finish();
            }
        });
        AcsService.getInstance().executor(runnable);
    }

    private void pickPhoto() {
        FileUtils.creatPathNotExcit();
        PopBottomDialog popBottomDialog = new PopBottomDialog(this);
        popBottomDialog.setOnBottomClickListener(new PopBottomDialog.PopBottomDialogListener() {
            @Override
            public void clickPosition(int position) {
                switch (position) {
                    case 1:
                        Intent intent = new Intent();
                        intent.setClass(AddPersonActivity.this, CameraPhotoActivity.class);
                        startActivityForResult(intent, ImageConfig.PHOTO_CAMERA);
                        break;
                    case 2:
                        Intent intentLocal = new Intent(Intent.ACTION_GET_CONTENT);
                        intentLocal.addCategory(Intent.CATEGORY_OPENABLE);
                        intentLocal.setType("image/*");
                        startActivityForResult(intentLocal, ImageConfig.PHOTO_LOCAL_CHOOICE);
                        break;
                    case 3:
                        break;
                }
            }
        });
        popBottomDialog.showPopDialog(
                getString(R.string.camera_make_photo),
                getString(R.string.image_local),
                getString(R.string.cacel));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != RESULT_OK) {
            return;
        }
        if (data == null) {
            showToast(getString(R.string.no_chooice_pic));
            return;
        }
        if (requestCode == ImageConfig.PHOTO_CAMERA) {
            String imagePath = AppInfo.CAPTURE_IMAGE_PATH;
            mImgPath = ImageConfig.CAPTURE_IMAGE_CUT_PATH;
            if (SpUtils.getWorkModel() == AppInfo.MODEL_STAND_ALONE)
                mImgPath = AppInfo.CAPTURE_IMAGE_LOCAL + "/" + System.currentTimeMillis() + ".jpg";
            File fileOut = new File(mImgPath);
            if (fileOut.exists()) {
                fileOut.delete();
            }
            try {
                fileOut.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
            Crop.of(Uri.parse(new File(imagePath).toString()), Uri.parse(new File(mImgPath).toString())).asSquare().start(this);
        } else if (requestCode == ImageConfig.PHOTO_LOCAL_CHOOICE) {
            mImgPath = ImageConfig.CAPTURE_IMAGE_CUT_PATH;
            if (SpUtils.getWorkModel() == AppInfo.MODEL_STAND_ALONE) {
                mImgPath = AppInfo.CAPTURE_IMAGE_LOCAL + "/" + System.currentTimeMillis() + ".jpg";
            }
            imageUtil.cutImageLocal(data, mImgPath);
        } else if (requestCode == ImageConfig.IMAGE_CUT_BACK || requestCode == Crop.REQUEST_CROP) {
            File fileOut = new File(mImgPath);
            if (!fileOut.exists()) {
                showToast(getString(R.string.cat_image_failed));
                return;
            }
            ImageManager.displayImagePathNoCache(this, mImgPath, mIvFace);
            mTempFile = new File(mImgPath);
        }
    }

    private String getPhoneNum() {
        return mEtPhone.getText().toString().trim();
    }

    private String getUserName() {
        return mEtName.getText().toString().trim();
    }

    private String getCaredNum() {
        String cardNum = et_card_num.getText().toString().trim();
        return cardNum;
    }

    private void localRegister(String username, String mobilePhonr, String idCard) {
        showWait(getString(R.string.adding));
        String syncTime = SimpleDateUtil.formatTaskTimeShow(System.currentTimeMillis());
        SpUtils.setFaceSyncTime(syncTime);
        new Thread(() -> {
            registerIrPerson(username, mobilePhonr, idCard, syncTime);
        }).start();
    }


    private void registerIrPerson(String username, String mobilePhonr, String idCard, String syncTime) {
        FaceManager.getInstance().registerByFile(mTempFile, new IRegisterFaceListener() {
            @Override
            public void onSuccess(@NotNull String faceToken, @NotNull String featurePath) {
                String perId = System.currentTimeMillis() + "";
                String path = mTempFile.getAbsolutePath();
                PersonDao.insertPerson(new PersonBean(
                        AppInfo.FACE_REGISTER_PERSON,
                        perId,
                        idCard,
                        username,
                        mobilePhonr,
                        path,
                        syncTime,
                        "Dev Add",
                        "", ""));
                FaceInfoJavaDao.insertFaceInfo(new FaceInfo(
                        username, faceToken, perId, path, AppInfo.FACE_REGISTER_PERSON, path, path
                ));
                runOnUiThread(() -> {
                    showToast(getString(R.string.add_success));
                    finish();
                });
            }

            @Override
            public void onError(int code, @NotNull String msg) {
                runOnUiThread(() -> {
                    showToast(getString(R.string.add_failed, code, msg));
                    hideWait();
                });
            }
        });
    }

}
