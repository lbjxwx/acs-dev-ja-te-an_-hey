package com.ys.acsdev.setting;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.ys.acsdev.R;
import com.ys.acsdev.listener.MoreButtonToggleListener;
import com.ys.acsdev.ui.BaseActivity;
import com.ys.acsdev.util.SpUtils;
import com.ys.acsdev.util.SystemManagerUtil;
import com.ys.acsdev.view.MoreButtonToggle;
import com.ys.acsdev.view.dialog.OridinryDialog;
import com.ys.acsdev.view.dialog.OridinryDialogClick;

/***
 * 访客设置
 */
public class VisitorSettingsActivity extends BaseActivity implements MoreButtonToggleListener, View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vositor_settings);
        initView();
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateDateInfo();
    }

    MoreButtonToggle more_btn_ask, more_btn_healthcode, mBtnQrCodeEnable, more_btn_person_id, more_btn_health_id;
    ImageView mIvBack;
    Button btn_submit;

    private void initView() {
        mIvBack = (ImageView) findViewById(R.id.mIvBack);
        mIvBack.setOnClickListener(this);
        more_btn_ask = (MoreButtonToggle) findViewById(R.id.more_btn_ask);
        more_btn_ask.setOnMoretListener(this);
        more_btn_health_id = (MoreButtonToggle) findViewById(R.id.more_btn_health_id);
        more_btn_health_id.setOnMoretListener(this);
        mBtnQrCodeEnable = (MoreButtonToggle) findViewById(R.id.mBtnQrCodeEnable);
        mBtnQrCodeEnable.setOnMoretListener(this);
        more_btn_person_id = (MoreButtonToggle) findViewById(R.id.more_btn_person_id);
        more_btn_person_id.setOnMoretListener(this);
        more_btn_healthcode = findViewById(R.id.more_btn_healthcode);
        more_btn_healthcode.setOnMoretListener(this);
        btn_submit = (Button) findViewById(R.id.btn_submit);
        btn_submit.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.mIvBack:
                onBackPressed();
                break;
            case R.id.btn_submit:
                showRebootAppDialog();
                break;
        }
    }

    @Override
    public void switchToggleView(View view, boolean isChooice) {
        if (isChooice) {
            setVisitorToggleStatuesDefault();
        }
        boolean isCardEnable = SpUtils.getInputCardEnable();
        switch (view.getId()) {
            case R.id.more_btn_health_id:
                if (!isCardEnable) {
                    showToast("刷卡未打开，请打开");
                    return;
                }
                SpUtils.setHealthyCompareEnable(isChooice);
                break;
            case R.id.more_btn_person_id:  //Id person compaire
                if (!isCardEnable) {
                    showToast("刷卡未打开，请打开");
                    return;
                }
                SpUtils.setCompareEnable(isChooice);
                break;
            case R.id.mBtnQrCodeEnable:
                SpUtils.setQrCodeEnable(isChooice);
                break;
            case R.id.more_btn_healthcode:  //验证健康码
                SpUtils.setHealthCodeEnable(isChooice);
                break;
            case R.id.more_btn_ask:
                SpUtils.setShowAskErCode(isChooice);
                break;
        }
        updateDateInfo();
    }

    public void setVisitorToggleStatuesDefault() {
        SpUtils.setShowAskErCode(false);
        SpUtils.setHealthCodeEnable(false);
        SpUtils.setQrCodeEnable(false);
        SpUtils.setCompareEnable(false);
        SpUtils.setHealthyCompareEnable(false);
    }

    private void updateDateInfo() {
        more_btn_health_id.setChcekcAble(SpUtils.getHealthyCompareEnable());
        more_btn_person_id.setChcekcAble(SpUtils.getCompareEnable());
        more_btn_healthcode.setChcekcAble(SpUtils.getHealthCodeEnable());
        more_btn_ask.setChcekcAble(SpUtils.getShowAskErCode());
        mBtnQrCodeEnable.setChcekcAble(SpUtils.getQrCodeEnable());
    }

    /***
     * 重启应用
     */
    private void showRebootAppDialog() {
        OridinryDialog oridinryDialog = new OridinryDialog(VisitorSettingsActivity.this);
        oridinryDialog.setOnDialogClickListener(new OridinryDialogClick() {
            @Override
            public void sure() {
                showToast(getString(R.string.set_success));
                SystemManagerUtil.rebootApp(VisitorSettingsActivity.this);
            }

            @Override
            public void noSure() {

            }
        });
        oridinryDialog.show(
                getString(R.string.reboot_msg),
                getString(R.string.reboot),
                getString(R.string.cancel)
        );
    }
}
