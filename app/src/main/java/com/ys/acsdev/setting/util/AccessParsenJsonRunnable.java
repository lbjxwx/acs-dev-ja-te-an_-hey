package com.ys.acsdev.setting.util;

import android.os.Handler;

import com.ys.acsdev.bean.AccessRecordBean;
import com.ys.acsdev.listener.AccessParsenerJsonListener;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class AccessParsenJsonRunnable implements Runnable {


    String jsonInfo;
    AccessParsenerJsonListener listener;
    private Handler handler = new Handler();

    public AccessParsenJsonRunnable(String jsonInfo,
                                    AccessParsenerJsonListener listener) {
        this.jsonInfo = jsonInfo;
        this.listener = listener;
    }

    @Override
    public void run() {
        parsenerJsonInfo();
    }

    private void parsenerJsonInfo() {
        if (jsonInfo == null || jsonInfo.length() < 5) {
            backJsonListInfo(false, null, " Json is Null");
            return;
        }
        try {
            JSONObject jsonObject = new JSONObject(jsonInfo);
            int code = jsonObject.getInt("code");
            String msg = jsonObject.getString("msg");
            if (code != 0) {
                backJsonListInfo(false, null, msg);
                return;
            }
            String data = jsonObject.getString("data");
            parsenerDataInfo(data);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void parsenerDataInfo(String data) {
        if (data == null || data.length() < 5) {
            backJsonListInfo(false, null, "data is Null");
            return;
        }
        try {
            JSONArray jsonArray = new JSONArray(data);
            int num = jsonArray.length();
            if (num < 1) {
                backJsonListInfo(false, null, "Data Is Null");
                return;
            }
            List<AccessRecordBean> accessRecordBeanList = new ArrayList<AccessRecordBean>();
            for (int i = 0; i < num; i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                String name = jsonObject.getString("name");
                String photoUrl = jsonObject.getString("photoUrl");
                String passType = jsonObject.getString("passType");
                String cardNum = jsonObject.getString("cardNum");
                String temperature = jsonObject.getString("temperature");
                String recordTime = jsonObject.getString("recordTime");
                String wearMask = jsonObject.getString("isMask");
                String userName = "";
                String idCardNum = "";
                if (jsonObject.toString().contains("userName")) {
                    userName = jsonObject.getString("userName");
                }
                if (jsonObject.toString().contains("idCardNum")) {
                    idCardNum = jsonObject.getString("idCardNum");
                }
                AccessRecordBean accessRecordBean = new AccessRecordBean(name, photoUrl, passType, cardNum, temperature, recordTime, wearMask, userName, idCardNum);
                accessRecordBeanList.add(accessRecordBean);
            }
            backJsonListInfo(true, accessRecordBeanList, "Parsener Json Success !");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void backJsonListInfo(boolean istrue, List<AccessRecordBean> accessRecordBeanList, String errorDesc) {
        if (listener == null) {
            return;
        }
        handler.post(new Runnable() {
            @Override
            public void run() {
                listener.backJsonInfo(istrue, accessRecordBeanList, errorDesc);
            }
        });
    }

}
