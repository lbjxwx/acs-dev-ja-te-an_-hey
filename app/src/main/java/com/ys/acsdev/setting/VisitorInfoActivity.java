package com.ys.acsdev.setting;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ys.acsdev.R;
import com.ys.acsdev.api.AppConfig;
import com.ys.acsdev.bean.FaceInfo;
import com.ys.acsdev.bean.PersonBean;
import com.ys.acsdev.commond.AppInfo;
import com.ys.acsdev.commond.CommondViewModel;
import com.ys.acsdev.db.dao.FaceInfoJavaDao;
import com.ys.acsdev.db.dao.PersonDao;
import com.ys.acsdev.manager.ImageManager;
import com.ys.acsdev.runnable.face.FaceManagerUtil;
import com.ys.acsdev.service.AcsService;
import com.ys.acsdev.service.FaceCheckDownService;
import com.ys.acsdev.setting.adapter.VisitorInfoNewAdapter;
import com.ys.acsdev.ui.BaseActivity;
import com.ys.acsdev.util.LogUtil;
import com.ys.acsdev.util.SpUtils;
import com.ys.acsdev.view.dialog.OridinryDialog;
import com.ys.acsdev.view.dialog.OridinryDialogClick;

import org.litepal.crud.callback.FindMultiCallback;

import java.util.ArrayList;
import java.util.List;

/***
 * 访客和黑名单，都共用这个类显示
 */
public class VisitorInfoActivity extends BaseActivity implements View.OnClickListener {

    RecyclerView mRv;
    VisitorInfoNewAdapter mAdapter;
    List<PersonBean> mFaceData = new ArrayList<>();
    ImageView mIvNet, mIvServer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visitor_info);
        initView();
    }

    @Override
    protected void onStop() {
        ImageManager.clearCache();
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    TextView tv_title_face;
    ImageView avi_iv_back;
    Button btn_clear_all;
    Button avi_btn_get_data;
    int personType = AppInfo.FACE_REGISTER_VISITOR;

    private void initView() {
        Intent intent = getIntent();
        personType = intent.getIntExtra("PersonType", AppInfo.FACE_REGISTER_VISITOR);
        mIvNet = findViewById(R.id.avi_iv_net);
        mIvServer = findViewById(R.id.avi_iv_server);
        btn_clear_all = (Button) findViewById(R.id.btn_clear_all);
        btn_clear_all.setOnClickListener(this);
        avi_iv_back = (ImageView) findViewById(R.id.avi_iv_back);
        avi_iv_back.setOnClickListener(this);
        tv_title_face = (TextView) findViewById(R.id.tv_title_face);
        if (personType == AppInfo.FACE_REGISTER_VISITOR) {
            tv_title_face.setText(getString(R.string.visitor_info));
        } else {
            tv_title_face.setText(getString(R.string.black_person));
        }
        mRv = (RecyclerView) findViewById(R.id.avi_rv);
        mRv.setLayoutManager(new LinearLayoutManager(this));
        mRv.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        mAdapter = new VisitorInfoNewAdapter(VisitorInfoActivity.this, mFaceData);
        mRv.setAdapter(mAdapter);
        avi_btn_get_data = (Button) findViewById(R.id.avi_btn_get_data);
        avi_btn_get_data.setOnClickListener(this);
        mIvServer.setImageResource(CommondViewModel.getInstance().getMServerState().getValue() ? R.mipmap.ic_online : R.mipmap.ic_offline);
        mIvNet.setImageResource(CommondViewModel.getInstance().getMNetState().getValue() ? R.mipmap.net_line : R.mipmap.net_disline);
        CommondViewModel.getInstance().getMServerState().observe(this, aBoolean -> {
            mIvServer.setImageResource(aBoolean ? R.mipmap.ic_online : R.mipmap.ic_offline);
        });
        CommondViewModel.getInstance().getMNetState().observe(this, isConnected -> {
            mIvNet.setImageResource(isConnected ? R.mipmap.net_line : R.mipmap.net_disline);
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_clear_all:
                showDelAllDialog();
                break;
            case R.id.avi_iv_back:
                onBackPressed();
                break;
            case R.id.avi_btn_get_data:
                initData();
                Intent intent = new Intent(AppInfo.START_CHECK_FACE_DOWN_REGISTER);
                if (personType == AppInfo.FACE_REGISTER_VISITOR) {
                    intent.putExtra(AppInfo.START_CHECK_FACE_DOWN_REGISTER, AppInfo.FACE_REGISTER_VISITOR);
                } else {
                    intent.putExtra(AppInfo.START_CHECK_FACE_DOWN_REGISTER, AppInfo.FACE_REGISTER_BLACK_LIST);
                }
                sendBroadcast(intent);
                break;
        }
    }

    private void showDelAllDialog() {
        OridinryDialog oridinryDialog = new OridinryDialog(VisitorInfoActivity.this);
        oridinryDialog.setOnDialogClickListener(new OridinryDialogClick() {
            @Override
            public void sure() {
                showWait("Loading...");
                if (AppConfig.APP_PARSENE_TYPE == AppConfig.PARSENE_ADD_TYPE) {
                    SpUtils.setUpdatePersonTime(0, "清空访客/黑名单数据");
                }
                deleteLocalVisitor();
                mHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        hideWait();
                        initData();
                    }
                }, 2000);
            }

            @Override
            public void noSure() {

            }
        });
        oridinryDialog.show(getString(R.string.if_del_info), getString(R.string.submit), getString(R.string.cacel));
    }

    /****
     * 删除本地的访客信息
     */
    public void deleteLocalVisitor() {
        sendBroadcast(new Intent(AppInfo.STOP_REQUEST_DOWN_FACE_IMAGE));
        FaceManagerUtil.deleteLocalFaceInfoByType(personType, new FaceManagerUtil.FaceManagerUtilListener() {
            @Override
            public void delPersonInfoBack() {

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        initData();
    }

    private void updateVisitorStatues() {
        String forstInfo = getString(R.string.visitor_info);
        if (personType == AppInfo.FACE_REGISTER_VISITOR) {
            forstInfo = getString(R.string.visitor_info);
        } else {
            forstInfo = getString(R.string.black_person);
        }

        String content = "";
        switch (FaceCheckDownService.faceCurrentStatues) {
            case FaceCheckDownService.FACE_STATUES_DEFAULT:
                content = getString(R.string.register_over);
                break;
            case FaceCheckDownService.FACE_STATUES_REQUEST:  //请求状态
                content = getString(R.string.register_request);
                break;
            case FaceCheckDownService.FACE_STATUES_MATH_LOCAL:  //检查差异性状态
                content = getString(R.string.register_checking);
                break;
            case FaceCheckDownService.FACE_STATUES_DOWNFILE:  //下载状态
                content = getString(R.string.register_downing);
                break;
            case FaceCheckDownService.FACE_STATUES_FACE_REGISTER:  //注册状态
                content = getString(R.string.register_reging);
                break;
        }
        tv_title_face.setText(forstInfo + ": " + content);

    }

    private void initData() {
        showWait(getString(R.string.getting_data));
        updateVisitorStatues();
        if (mFaceData != null) {
            mFaceData.clear();
        }
        PersonDao.getAllPersonByListener(personType, new FindMultiCallback<PersonBean>() {
            @Override
            public void onFinish(List<PersonBean> list) {
                LogUtil.persoon("==========获取得访客信息===" + personType);
                hideWait();
                if (list == null || list.size() < 1) {
                    mHandler.sendEmptyMessage(UPDATE_LIST_VIEW_NODATA);
                    return;
                }
                for (int i = 0; i < list.size(); i++) {
                    PersonBean personBean = list.get(i);
                    LogUtil.persoon("==========获取得访客信息===" + personBean.toString());
                    mFaceData.add(personBean);
                }
                mHandler.sendEmptyMessage(UPDATE_LIST_VIEW);
            }
        });
    }

    private static final int UPDATE_LIST_VIEW = 9087;
    private static final int UPDATE_LIST_VIEW_NODATA = 9088;
    Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case UPDATE_LIST_VIEW_NODATA:
                    mAdapter.setList(mFaceData);
                    showToast(getString(R.string.no_data));
                    break;
                case UPDATE_LIST_VIEW:
                    showWait("Load Face Statues...");
                    logInfo("先加载数据=====:" + mFaceData.size());
                    mAdapter.setList(mFaceData);
                    updateFaceRegisterInfo();
                    break;
            }
        }
    };

    /***
     * 用来更新人脸注册状态。
     */
    private void updateFaceRegisterInfo() {
        if (mFaceData == null || mFaceData.size() < 1) {
            logInfo("没有数据，，中断");
            hideWait();
            return;
        }
        try {
            Runnable runnable = new Runnable() {
                @Override
                public void run() {
                    for (int i = 0; i < mFaceData.size(); i++) {
                        PersonBean personBean = mFaceData.get(i);
                        if (personBean == null) {
                            continue;
                        }
                        FaceInfo faceInfo = FaceInfoJavaDao.getFaceInfoById(personBean.getPerId());
                        if (faceInfo == null) {
                            personBean.setRegister(false);
                            continue;
                        }
                        boolean isRegisted = faceInfo.getFaceToken() != null && !faceInfo.getFaceToken().isEmpty();
                        personBean.setRegister(isRegisted);
                        logInfo("=====解析注册状态====" + i);
                    }
                    logInfo("=====解析注册状态===over=");

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            logInfo("数据回来了，加载状态");
                            hideWait();
                            mAdapter.setList(mFaceData);
                        }
                    });
                }
            };
            AcsService.getInstance().executor(runnable);
        } catch (Exception e) {
            hideWait();
            logInfo("=====解析注册状态===error=" + e.toString());
            e.printStackTrace();
        }
    }

    private void logInfo(String desc) {
        LogUtil.persoon("==界面显示===" + desc);
    }

}
