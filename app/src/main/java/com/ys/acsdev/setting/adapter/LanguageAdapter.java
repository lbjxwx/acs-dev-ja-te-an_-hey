package com.ys.acsdev.setting.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.ys.acsdev.R;
import com.ys.acsdev.listener.AdapterItemClickListener;
import com.ys.acsdev.setting.entity.LanguageEntity;
import com.ys.acsdev.util.SpUtils;

import java.util.List;

/***
 * 設置界面--adapter
 */
public class LanguageAdapter extends BaseAdapter {

    List<LanguageEntity> list = null;
    Context context;
    LayoutInflater layoutInflater;

    public LanguageAdapter(Context context, List<LanguageEntity> list) {
        this.context = context;
        this.list = list;
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        if (list == null || list.size() < 1) {
            return 0;
        }
        return list.size();
    }

    @Override
    public LanguageEntity getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = layoutInflater.inflate(R.layout.item_language_change, null);
            viewHolder.iv_icon_menu = convertView.findViewById(R.id.iv_icon_menu);
            viewHolder.tv_title = convertView.findViewById(R.id.tv_title);
            viewHolder.btn_check_view = convertView.findViewById(R.id.btn_check_view);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        LanguageEntity languageEntity = list.get(position);
        viewHolder.iv_icon_menu.setBackgroundResource(languageEntity.getCountryImage());
        viewHolder.tv_title.setText(languageEntity.getName());
        int showPosition = SpUtils.getLanguageDesc();
        if (showPosition == position) {
            viewHolder.btn_check_view.setBackgroundResource(R.mipmap.check_yes);
        } else {
            viewHolder.btn_check_view.setBackgroundResource(R.mipmap.check_no);
        }
        viewHolder.btn_check_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (adapterItemClickListener == null) {
                    return;
                }
                adapterItemClickListener.adapterItemClick(view.getId(), languageEntity, position);
            }
        });
        return convertView;
    }

    class ViewHolder {
        ImageView iv_icon_menu;
        TextView tv_title;
        Button btn_check_view;
    }

    AdapterItemClickListener adapterItemClickListener;

    public void setOnAdapterItemClickListener(AdapterItemClickListener adapterItemClickListener) {
        this.adapterItemClickListener = adapterItemClickListener;
    }
}
