package com.ys.acsdev.setting

import android.os.Bundle
import android.util.Log
import android.view.View
import com.huiming.huimingstore.ext.setVisible
import com.ys.acsdev.R
import com.ys.acsdev.api.AppConfig
import com.ys.acsdev.bean.RedioEntity
import com.ys.acsdev.db.DataRepository
import com.ys.acsdev.listener.MoreButtonListener
import com.ys.acsdev.listener.MoreButtonToggleListener
import com.ys.acsdev.listener.RadioChooiceListener
import com.ys.acsdev.ui.BaseActivity
import com.ys.acsdev.util.LogUtil.cdl
import com.ys.acsdev.util.RootCmd
import com.ys.acsdev.util.SpUtils
import com.ys.acsdev.view.dialog.*
import kotlinx.android.synthetic.main.activity_face_settings.*

class FaceSettingsActivity : BaseActivity(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_face_settings)
        initView()
    }

    override fun onResume() {
        super.onResume()
        initData()
    }

    private fun initView() {
        more_pic_quit.setOnMoretListener {
            var radioListDialog = RadioListDialog(this)
            var listShow = ArrayList<RedioEntity>();
            listShow.add(RedioEntity(getString(R.string.pic_quit_low)))
            listShow.add(RedioEntity(getString(R.string.pic_quit_middle)))
            listShow.add(RedioEntity(getString(R.string.pic_quit_height)))
            var checkPosition = SpUtils.getUpdatePicQuity()
            var desc = getString(R.string.pic_quit_title)
            radioListDialog.show(getString(R.string.pic_quit_title), listShow, checkPosition, desc)
            radioListDialog.setRadioChooiceListener(object : RadioChooiceListener {
                override fun backChooiceInfo(redioEntity: RedioEntity?, chooicePosition: Int) {
                    SpUtils.setUpdatePicQuity(chooicePosition)
                    initData()
                }
            })
        }
        mIvBack.setOnClickListener { onBackPressed() }
        mBtnSave.setOnClickListener(this)

        btn_clear_shared.setOnClickListener(object : View.OnClickListener {
            override fun onClick(p0: View?) {
                showClearSharedDialog()
            }
        })
        //活体检测
        more_btn_live_test.setOnMoretListener(object : MoreButtonToggleListener {

            override fun switchToggleView(view: View?, isChooice: Boolean) {
                SpUtils.setmAutoSwitchCamera(isChooice)
                initData()
            }
        })
        mBtnSingleLive.setOnMoretListener { view, isChooice ->
            SpUtils.setSingleLivingEnable(isChooice)
            initData()
        }


        mBtnCheckModel.setOnMoretListener { view, isChooice ->
            SpUtils.setOnlyCheckEnable(isChooice)
            initData()
        }

        //设置人脸识别框镜像显示
        mBtnFaceRect.setOnMoretListener {
            var radioListDialog = RadioListDialog(this)
            var listShow = ArrayList<RedioEntity>();
            listShow.add(RedioEntity(getString(R.string.auto)))
            listShow.add(RedioEntity(getString(R.string.image)))
            listShow.add(RedioEntity(getString(R.string.non_image)))
            var checkPosition = SpUtils.getmFaceRect()
            var desc = getString(R.string.face_rect_position)
            radioListDialog.show(getString(R.string.face_image_set), listShow, checkPosition, desc)
            radioListDialog.setRadioChooiceListener(object : RadioChooiceListener {
                override fun backChooiceInfo(redioEntity: RedioEntity?, chooicePosition: Int) {
                    SpUtils.setmFaceRect(chooicePosition)
                    initData()
                }
            })
        }

        //设置人脸识别框镜像显示
        mBtnResolution.setOnMoretListener {
            var radioListDialog = RadioListDialog(this)
            val currentWidht = SpUtils.getResolutionWidth()
            val currentHeight = SpUtils.getResolutionHeight()
            val showDesc = getString(R.string.preview_resolution)
            var checkPosition = 0

            var listShow = ArrayList<RedioEntity>();
            listShow.add(RedioEntity(getString(R.string.auto)))
            DataRepository.instance.mSupportedPreview.forEachIndexed { index, point ->
                if (point.x == currentWidht && point.y == currentHeight) {
                    checkPosition = index + 1
                }
                listShow.add(RedioEntity("${point.x}x${point.y}"))
            }
            radioListDialog.show(
                getString(R.string.tv_resolution),
                listShow,
                checkPosition,
                showDesc
            )
            radioListDialog.setRadioChooiceListener(object : RadioChooiceListener {
                override fun backChooiceInfo(redioEntity: RedioEntity?, chooicePosition: Int) {
                    if (chooicePosition == 0) {
                        SpUtils.setResolutionWidth(0)
                        SpUtils.setResolutionHeight(0)
                    } else {
                        val point = DataRepository.instance.mSupportedPreview[chooicePosition - 1]
                        Log.e("cdl", "====设置的分辨率==" + point.x + " / " + point.y)
                        SpUtils.setResolutionWidth(point.x)
                        SpUtils.setResolutionHeight(point.y)
                    }
//                    SpUtils.setmResolution(chooicePosition)
                    initData()
                }
            })
        }

        //设置人脸识别框水平镜像显示
        mBtnFaceRectHor.setOnMoretListener {
            var radioListDialog = RadioListDialog(this)
            var listShow = ArrayList<RedioEntity>();
            listShow.add(RedioEntity(getString(R.string.image)))
            listShow.add(RedioEntity(getString(R.string.non_image)))
            var checkPosition = SpUtils.getmFaceRectHor()
            var showDesc = getString(R.string.preview_hro)
            radioListDialog.show(
                getString(R.string.tv_face_rect_h),
                listShow,
                checkPosition,
                showDesc
            )
            radioListDialog.setRadioChooiceListener(object : RadioChooiceListener {
                override fun backChooiceInfo(redioEntity: RedioEntity?, chooicePosition: Int) {
                    SpUtils.setmFaceRectHor(chooicePosition)
                    initData()
                }
            })
        }

        //设置人脸识别框垂直镜像显示
        mBtnFaceRectVer.setOnMoretListener {
            var radioListDialog = RadioListDialog(this)
            var listShow = ArrayList<RedioEntity>();
            listShow.add(RedioEntity(getString(R.string.image)))
            listShow.add(RedioEntity(getString(R.string.non_image)))
            var checkPosition = SpUtils.getmFaceRectVertical()
            var showDesc = getString(R.string.preview_ver)
            radioListDialog.show(
                getString(R.string.tv_face_rect_v),
                listShow,
                checkPosition,
                showDesc
            )
            radioListDialog.setRadioChooiceListener(object : RadioChooiceListener {
                override fun backChooiceInfo(redioEntity: RedioEntity?, chooicePosition: Int) {
                    SpUtils.setmFaceRectVertical(chooicePosition)
                    initData()
                }
            })
        }

        /***
         * 相机方向
         */
        mBtnCameraFacing.setOnMoretListener {
            var radioListDialog = RadioListDialog(this)
            var listShow = ArrayList<RedioEntity>();
            listShow.add(RedioEntity(getString(R.string.front)));
            listShow.add(RedioEntity(getString(R.string.rear)));
            var checkPosition = 0;
            if (SpUtils.getCameraFacing()) {
                checkPosition = 0;
            } else {
                checkPosition = 1;
            }
            var showDesc = getString(R.string.camera_change)
            radioListDialog.show(
                getString(R.string.tv_camera_rotate),
                listShow,
                checkPosition,
                showDesc
            )
            radioListDialog.setRadioChooiceListener(object : RadioChooiceListener {
                override fun backChooiceInfo(redioEntity: RedioEntity?, chooicePosition: Int) {
                    if (chooicePosition == 0) {
                        SpUtils.setCameraFacing(true)
                    } else {
                        SpUtils.setCameraFacing(false)
                    }
                    initData()
                }
            })
        }

        //人脸识别距离
        mBtnFaceLen.setOnMoretListener {
            var radioListDialog = RadioListDialog(this)
            var listShow = ArrayList<RedioEntity>();
            listShow.add(RedioEntity(getString(R.string.len1)));
            listShow.add(RedioEntity(getString(R.string.len5)));
            listShow.add(RedioEntity(getString(R.string.len2)));
            listShow.add(RedioEntity(getString(R.string.len3)));
            listShow.add(RedioEntity(getString(R.string.len4)));
            var checkPosition = SpUtils.getmFaceLen()
            var showDesc = getString(R.string.rect_distance)
            radioListDialog.show(getString(R.string.tv_face_len), listShow, checkPosition, showDesc)
            radioListDialog.setRadioChooiceListener(object : RadioChooiceListener {
                override fun backChooiceInfo(redioEntity: RedioEntity?, chooicePosition: Int) {
                    SpUtils.setFaceLen(chooicePosition)
                    initData()
                }
            })
        }

        //设置预览角度
        mBtnPrewRotate.setOnMoretListener {
            var radioListDialog = RadioListDialog(this)
            var listShow = ArrayList<RedioEntity>();
            listShow.add(RedioEntity(getString(R.string.rotate0)));
            listShow.add(RedioEntity(getString(R.string.rotate90)));
            listShow.add(RedioEntity(getString(R.string.rotate180)));
            listShow.add(RedioEntity(getString(R.string.rotate270)));
            var checkPosition = SpUtils.getPrewRotate()
            var showDesc = getString(R.string.face_pre_roate)
            radioListDialog.show(
                getString(R.string.tv_prew_rotate),
                listShow,
                checkPosition,
                showDesc
            )
            radioListDialog.setRadioChooiceListener(object : RadioChooiceListener {
                override fun backChooiceInfo(redioEntity: RedioEntity?, chooicePosition: Int) {
                    SpUtils.setPrewRotate(chooicePosition)
                    initData()
                }
            })
        }

        //设置识别算法角度-人脸识别角度
        mBtnFaceRotate.setOnMoretListener {
            var radioListDialog = RadioListDialog(this)
            var listShow = ArrayList<RedioEntity>();
            listShow.add(RedioEntity(getString(R.string.rotate0)));
            listShow.add(RedioEntity(getString(R.string.rotate90)));
            listShow.add(RedioEntity(getString(R.string.rotate180)));
            listShow.add(RedioEntity(getString(R.string.rotate270)));
            var checkPosition = SpUtils.getmFaceRotate()
            var showDesc = getString(R.string.face_reg_roate)
            radioListDialog.show(
                getString(R.string.tv_face_rotate),
                listShow,
                checkPosition,
                showDesc
            )
            radioListDialog.setRadioChooiceListener(object : RadioChooiceListener {
                override fun backChooiceInfo(redioEntity: RedioEntity?, chooicePosition: Int) {
                    SpUtils.setFaceRotate(chooicePosition)
                    initData()
                }
            })
        }

        //设置抓拍角度
        mBtnSaveRotate.setOnMoretListener {
            var radioListDialog = RadioListDialog(this)
            var listShow = ArrayList<RedioEntity>();
            listShow.add(RedioEntity(getString(R.string.auto)))
            listShow.add(RedioEntity(getString(R.string.rotate0)))
            listShow.add(RedioEntity(getString(R.string.rotate90)))
            listShow.add(RedioEntity(getString(R.string.rotate180)))
            listShow.add(RedioEntity(getString(R.string.rotate270)))
            var checkPosition = SpUtils.getSaveRotate()
            var showDesc = getString(R.string.arcord_image_roate)
            radioListDialog.show(
                getString(R.string.tv_save_rotate),
                listShow,
                checkPosition,
                showDesc
            )
            radioListDialog.setRadioChooiceListener(object : RadioChooiceListener {
                override fun backChooiceInfo(redioEntity: RedioEntity?, chooicePosition: Int) {
                    SpUtils.setSaveRotate(chooicePosition)
                    initData()
                }
            })
        }

        //设置人脸框角度
        mBtnRectRotate.setOnMoretListener {
            var radioListDialog = RadioListDialog(this)
            var listShow = ArrayList<RedioEntity>();
            listShow.add(RedioEntity(getString(R.string.auto)))
            listShow.add(RedioEntity(getString(R.string.rotate0)))
            listShow.add(RedioEntity(getString(R.string.rotate90)))
            listShow.add(RedioEntity(getString(R.string.rotate180)))
            listShow.add(RedioEntity(getString(R.string.rotate270)))
            var checkPosition = SpUtils.getRectRotate()
            var showDesc = getString(R.string.face_reg_roate)
            radioListDialog.show(
                getString(R.string.tv_rect_rotate),
                listShow,
                checkPosition,
                showDesc
            )
            radioListDialog.setRadioChooiceListener(object : RadioChooiceListener {
                override fun backChooiceInfo(redioEntity: RedioEntity?, chooicePosition: Int) {
                    SpUtils.setRectRotate(chooicePosition)
                    initData()
                }
            })
        }

        mBtnMaskEnable.setOnMoretListener(object : MoreButtonToggleListener {

            override fun switchToggleView(view: View?, isChooice: Boolean) {
                SpUtils.setMaskEnable(isChooice)
                if (!isChooice) {
                    SpUtils.setMaskIntercept(false)
                }
                initData()
            }
        })

        mBtnMaskInterceptEnable.setOnMoretListener(object : MoreButtonToggleListener {

            override fun switchToggleView(view: View?, isChooice: Boolean) {
                SpUtils.setMaskIntercept(isChooice)
                initData()
            }
        })

        //前相机镜像
        more_front_camera.setOnMoretListener {
            var radioListDialog = RadioListDialog(this)
            var listShow = ArrayList<RedioEntity>();
            listShow.add(RedioEntity(getString(R.string.image)));
            listShow.add(RedioEntity(getString(R.string.non_image)));
            var checkPosition = 0;
            var fcamInfo = RootCmd.getProperty(RootCmd.CAMERA_FONR_MIRROR, "1");
            if (fcamInfo.contains("true")) {
                checkPosition = 0;
            } else {
                checkPosition = 1;
            }
            var showDesc = getString(R.string.camera_forst_mirror)
            radioListDialog.show(
                getString(R.string.front_camera_imag),
                listShow,
                checkPosition,
                showDesc
            )
            radioListDialog.setRadioChooiceListener(object : RadioChooiceListener {
                override fun backChooiceInfo(redioEntity: RedioEntity?, chooicePosition: Int) {
                    Log.e("haha", "====返回得位置===" + checkPosition)
                    if (chooicePosition == 0) {
                        RootCmd.setProperty(RootCmd.CAMERA_FONR_MIRROR, "true");
                        SpUtils.setFrontImage(1)
                    } else {
                        RootCmd.setProperty(RootCmd.CAMERA_FONR_MIRROR, "false");
                        SpUtils.setFrontImage(2)
                    }
                    initData();
                }
            })
        }

        //后相机镜像
        more_back_camera.setOnMoretListener {
            var radioListDialog = RadioListDialog(this)
            var listShow = ArrayList<RedioEntity>();
            listShow.add(RedioEntity(getString(R.string.image)));
            listShow.add(RedioEntity(getString(R.string.non_image)));
            var checkPosition = 0;
            var fcamInfo = RootCmd.getProperty(RootCmd.CAMERA_BACK_MIRROR, "1");
            if (fcamInfo.contains("true")) {
                checkPosition = 0;
            } else {
                checkPosition = 1;
            }
            var showDesc = getString(R.string.camera_forst_mirror)
            radioListDialog.show(
                getString(R.string.rear_camera_image),
                listShow,
                checkPosition,
                showDesc
            )
            radioListDialog.setRadioChooiceListener(object : RadioChooiceListener {
                override fun backChooiceInfo(redioEntity: RedioEntity?, chooicePosition: Int) {
                    Log.e("haha", "====返回得位置===" + checkPosition)
                    if (chooicePosition == 0) {
                        SpUtils.setBackImage(1)
                        RootCmd.setProperty(RootCmd.CAMERA_BACK_MIRROR, "true");
                    } else {
                        SpUtils.setBackImage(2)
                        RootCmd.setProperty(RootCmd.CAMERA_BACK_MIRROR, "false");
                    }
                    initData();
                }
            })
        }

        mBtnSearchThreshold.setOnMoretListener {
            var editTextDialog = EditTextDialog(this)
            editTextDialog.setOnDialogClickListener(object : EditTextDialog.EditTextDialogListener {
                override fun commit(content: String?) {
                    if (content == null || content.length < 1) {
                        showToast(getString(R.string.effective_value))
                        return
                    }
                    try {
                        var floatNum = content.toFloat()
                        SpUtils.setSearchThreshold(floatNum)
                        showToast(getString(R.string.set_success))
                        initData()

                    } catch (e: Exception) {
                        showToast(getString(R.string.set_failed))
                        e.printStackTrace()
                    }
                }

                override fun hiddleTestClick() {
                    TODO("Not yet implemented")
                }

            })
            var showContent = SpUtils.getmSearchThreshold().toString()
            editTextDialog.show(
                "",
                showContent,
                getString(R.string.confirm),
                getString(R.string.effective_value)
            )
        }

        mBtnLivenessThreshold.setOnMoretListener {
            var editTextDialog = EditTextDialog(this)
            editTextDialog.setOnDialogClickListener(object : EditTextDialog.EditTextDialogListener {
                override fun commit(content: String?) {
                    if (content == null || content.length < 1) {
                        showToast(getString(R.string.effective_value))
                        return
                    }
                    try {
                        var floatNum = content.toFloat()
                        SpUtils.setLivenessThreshold(floatNum)
                        showToast(getString(R.string.set_success))
                        initData()

                    } catch (e: Exception) {
                        showToast(getString(R.string.set_failed))
                        e.printStackTrace()
                    }
                }

                override fun hiddleTestClick() {
                    TODO("Not yet implemented")
                }
            })
            var showContent = SpUtils.getLivenessThreshold().toString()
            editTextDialog.show(
                "",
                showContent,
                getString(R.string.confirm),
                getString(R.string.effective_value)
            )
        }
    }

    /***
     * 显示清理Sharedperference
     */
    private fun showClearSharedDialog() {
        var oridinryDialog = OridinryDialog(this);
        oridinryDialog.setOnDialogClickListener(object : OridinryDialogClick {
            override fun sure() {
//                SystemManagerUtil.clearSharedInfo()
                SpUtils.setSharedDefaultInfo();
                showRebootDialog();
            }

            override fun noSure() {
            }
        })
        oridinryDialog.show(
            getString(R.string.clear_shared_content),
            getString(R.string.submit),
            getString(R.string.cacel)
        );
    }

    private fun initData() {
        /* if (SpUtils.getCameraFacing()) {//摄像头前置
             more_front_camera.setVisible(true)
             more_back_camera.setVisible(false)
         } else {  //摄像头后置
             more_front_camera.setVisible(false)
             more_back_camera.setVisible(true)
         }*/
        var picQuity = SpUtils.getUpdatePicQuity();
        if (picQuity == 0) {
            more_pic_quit.setRigt(getString(R.string.pic_quit_low))
        } else if (picQuity == 1) {
            more_pic_quit.setRigt(getString(R.string.pic_quit_middle))
        } else if (picQuity == 2) {
            more_pic_quit.setRigt(getString(R.string.pic_quit_height))
        }
        var fcamInfo = RootCmd.getProperty(RootCmd.CAMERA_FONR_MIRROR, "1");
        if (fcamInfo.contains("true")) {
            more_front_camera.setRigt(getString(R.string.image))
        } else {
            more_front_camera.setRigt(getString(R.string.non_image))
        }
        var bcamInfo = RootCmd.getProperty(RootCmd.CAMERA_BACK_MIRROR, "1");
        if (bcamInfo.contains("true")) {
            more_back_camera.setRigt(getString(R.string.image))
        } else {
            more_back_camera.setRigt(getString(R.string.non_image))
        }
        mBtnCameraFacing.setRigt(
            if (SpUtils.getCameraFacing()) getString(R.string.front) else getString(
                R.string.rear
            )
        )
        mBtnFaceRectHor.setRigt(
            if (SpUtils.getmFaceRectHor() == 0) getString(R.string.image) else getString(
                R.string.non_image
            )
        )
        mBtnFaceRectVer.setRigt(
            if (SpUtils.getmFaceRectVertical() == 0) getString(R.string.image) else getString(
                R.string.non_image
            )
        )
        mBtnSearchThreshold.setRigt("" + SpUtils.getmSearchThreshold())
        mBtnLivenessThreshold.setRigt("" + SpUtils.getLivenessThreshold())
        //旷视
        mBtnFaceRectHor.setVisible(false)
        mBtnFaceRectVer.setVisible(false)
        mBtnFaceRect.setVisible(true)
        mBtnRectRotate.setVisible(true)

        mBtnMaskEnable.setChcekcAble(SpUtils.getmMaskEnable())
        mBtnCheckModel.setChcekcAble(SpUtils.getOnlyCheckEnable())
        mBtnMaskInterceptEnable.setChcekcAble(SpUtils.getMaskIntercept())
        mBtnMaskInterceptEnable.setVisible(SpUtils.getmMaskEnable())

//        mBtnFaceLen.setRigt(mFaceLen.toString())
        mBtnFaceLen.setRigt(
            when (SpUtils.getmFaceLen()) {
                0 -> getString(R.string.len1)
                2 -> getString(R.string.len2)
                3 -> getString(R.string.len3)
                4 -> getString(R.string.len4)
                else -> getString(R.string.len5)
            }
        )
        val currentWidht = SpUtils.getResolutionWidth()
        val currentHeight = SpUtils.getResolutionHeight()
        val showDesc =
            if (currentWidht == 0 || currentHeight == 0) getString(R.string.auto) else "${currentWidht}x${currentHeight}"
        mBtnResolution.setRigt(showDesc)
        mBtnFaceRect.setRigt(
            when (SpUtils.getmFaceRect()) {
                0 -> getString(R.string.auto)
                1 -> getString(R.string.image)
                else -> getString(R.string.non_image)
            }
        )
        mBtnPrewRotate.setRigt(
            when (SpUtils.getPrewRotate()) {
                0 -> getString(R.string.rotate0)
                1 -> getString(R.string.rotate90)
                2 -> getString(R.string.rotate180)
                else -> getString(R.string.rotate270)
            }
        )
        mBtnSaveRotate.setRigt(
            when (SpUtils.getSaveRotate()) {
                0 -> getString(R.string.auto)
                1 -> getString(R.string.rotate0)
                2 -> getString(R.string.rotate90)
                3 -> getString(R.string.rotate180)
                else -> getString(R.string.rotate270)
            }
        )
        mBtnRectRotate.setRigt(
            when (SpUtils.getRectRotate()) {
                0 -> getString(R.string.auto)
                1 -> getString(R.string.rotate0)
                2 -> getString(R.string.rotate90)
                3 -> getString(R.string.rotate180)
                else -> getString(R.string.rotate270)
            }
        )
        mBtnFaceRotate.setRigt(
            when (SpUtils.getmFaceRotate()) {
                0 -> getString(R.string.rotate0)
                1 -> getString(R.string.rotate90)
                2 -> getString(R.string.rotate180)
                else -> getString(R.string.rotate270)
            }
        )
        //单目活体开关
        mBtnSingleLive.setChcekcAble(SpUtils.getSingleLivingEnable())
        //活体检测
        more_btn_live_test.setChcekcAble(SpUtils.getAutoSwitchCamera())
        hiddleViewByCustom()
    }

    private fun hiddleViewByCustom() {
        var isSingleModel = !SpUtils.getSettingSimpleModel()
        mBtnCheckModel.setVisible(isSingleModel)  //检测模式
        mBtnCameraFacing.setVisible(isSingleModel)  //前后摄像头切换
        mBtnFaceRotate.setVisible(isSingleModel)  //算法角度
        mBtnPrewRotate.setVisible(isSingleModel)   //预览角度
        mBtnFaceRect.setVisible(isSingleModel)     //人脸框镜像
        mBtnRectRotate.setVisible(isSingleModel)    //人脸框角度
        more_front_camera.setVisible(isSingleModel)
        more_back_camera.setVisible(isSingleModel)
        mBtnSaveRotate.setVisible(isSingleModel)
        mBtnSearchThreshold.setVisible(isSingleModel)
        mBtnLivenessThreshold.setVisible(isSingleModel)
        more_btn_live_test.setVisible(isSingleModel)   //双目活体直接关掉
    }

    override fun onClick(v: View?) {
        if (v == null) {
            return
        }
        when (v.id) {
            R.id.mBtnSave -> {
                showRebootDialog()
            }
        }
    }
}