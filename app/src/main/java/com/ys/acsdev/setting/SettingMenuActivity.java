package com.ys.acsdev.setting;

import android.annotation.SuppressLint;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.ComponentName;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.ys.acsdev.R;
import com.ys.acsdev.commond.AppInfo;
import com.ys.acsdev.light.LightManager;
import com.ys.acsdev.setting.fragment.AppSettingFragment;
import com.ys.acsdev.setting.fragment.DefaultSettingFragment;
import com.ys.acsdev.setting.parsener.SettingMenuParsener;
import com.ys.acsdev.setting.view.SettingMenuView;
import com.ys.acsdev.ui.BaseActivity;
import com.ys.acsdev.util.APKUtil;
import com.ys.acsdev.util.ActivityCollector;
import com.ys.acsdev.util.NetWorkUtils;
import com.ys.acsdev.util.SpUtils;
import com.ys.acsdev.view.dialog.OridinryDialog;
import com.ys.acsdev.view.dialog.OridinryDialogClick;

/***
 * 設置界面-新的
 * cdl
 */
public class SettingMenuActivity extends BaseActivity implements View.OnClickListener, SettingMenuView {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting_menu);
        initView();
        initListener();
    }

    Button btn_exit;
    private FragmentManager fragmentManager;
    Button btn_default_setting;
    Button btn_app_setting;
    Button btn_sync_info;
    TextView tv_ip;
    View view_click;
    SettingMenuParsener settingMenuParsener;

    private void initView() {
        fragmentManager = getFragmentManager();
        view_click = (View) findViewById(R.id.view_click);
        btn_default_setting = (Button) findViewById(R.id.btn_default_setting);
        btn_default_setting.setOnClickListener(this);
        btn_app_setting = (Button) findViewById(R.id.btn_app_setting);
        btn_app_setting.setOnClickListener(this);
        btn_exit = (Button) findViewById(R.id.btn_exit);
        btn_exit.setOnClickListener(this);
        btn_sync_info = (Button) findViewById(R.id.btn_sync_info);
        btn_sync_info.setOnClickListener(this);
        tv_ip = (TextView) findViewById(R.id.tv_ip);
        tv_ip.setText(NetWorkUtils.getIPAddress(SettingMenuActivity.this));
        btn_sync_info.setVisibility(SpUtils.getWorkModel() == AppInfo.MODEL_NET ? View.VISIBLE : View.GONE);
        settingMenuParsener = new SettingMenuParsener(SettingMenuActivity.this, this);
    }

    private void initListener() {
        view_click.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                gotoGuardianApp();
                return true;
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        LightManager.getInstance().closeAllLight();
        setTabSelection(CURRENT_POSITION);
    }

    static int CURRENT_POSITION = 0;
    public static final int FRAGMEN_DEFAULT_SETTING = 0;
    public static final int FRAGMEN_APP_SETTING = 1;
    int colorChooice = 0xff5AA7F5;   //选中蓝色
    int colorWhite = 0xffffffff;  //white
    int colorGrey = 0xff373737;  //grey
    DefaultSettingFragment defaultSettingFragment;
    AppSettingFragment appSettingFragment;


    @SuppressLint("ResourceAsColor")
    public void setTabSelection(int tabSelection) {
        btn_default_setting.setBackgroundColor(colorWhite);
        btn_app_setting.setBackgroundColor(colorWhite);
        btn_default_setting.setTextColor(colorGrey);
        btn_app_setting.setTextColor(colorGrey);
        CURRENT_POSITION = tabSelection;
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        switch (tabSelection) {
            case FRAGMEN_DEFAULT_SETTING:
                btn_default_setting.setBackgroundColor(colorChooice);
                btn_default_setting.setTextColor(colorWhite);
                if (defaultSettingFragment == null) {
                    defaultSettingFragment = new DefaultSettingFragment();
                }
                transaction.replace(R.id.content_layout, defaultSettingFragment);
                break;
            case FRAGMEN_APP_SETTING:
                btn_app_setting.setBackgroundColor(colorChooice);
                btn_app_setting.setTextColor(colorWhite);
                if (appSettingFragment == null) {
                    appSettingFragment = new AppSettingFragment();
                }
                transaction.replace(R.id.content_layout, appSettingFragment);
                break;
        }
        transaction.commit();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_default_setting:
                setTabSelection(FRAGMEN_DEFAULT_SETTING);
                break;
            case R.id.btn_app_setting:
                setTabSelection(FRAGMEN_APP_SETTING);
                break;
            case R.id.btn_sync_info:
                showUpdateInfoFromDialog();
                break;
            case R.id.btn_exit:
                finish();
                break;
        }
    }

    private void showUpdateInfoFromDialog() {
        OridinryDialog dialog = new OridinryDialog(this);
        dialog.setOnDialogClickListener(new OridinryDialogClick() {
            @Override
            public void sure() {
                settingMenuParsener.sycnConfig();
            }

            @Override
            public void noSure() {

            }
        });
        dialog.show(
                getString(R.string.sync_info),
                getString(R.string.confirm),
                getString(R.string.cancel)
        );
    }

    @Override
    public void showWaitDialog(boolean isShow) {
        if (isShow) {
            showWait(getString(R.string.getting_config));
        } else {
            hideWait();
        }
    }

    @Override
    public void syncDevInfoFromWeb(boolean isTrue, String errorDesc) {
        if (isTrue) {
            showRebootDialog();
            return;
        }
        showToast(getString(R.string.get_config_failed_msg) + errorDesc);
    }

    public void killMySlef() {
        ActivityCollector.finishAll();
        System.exit(0);
        finish();
    }

    private void gotoGuardianApp() {
        String packageName = "com.guardian";
        boolean isInstall = APKUtil.ApkState(SettingMenuActivity.this, packageName);
        if (!isInstall) {
            showToast(getString(R.string.no_software_reboot));
            return;
        }
        Intent intent = new Intent();
        ComponentName cmp = new ComponentName("com.guardian", "com.guardian.ui.MainActivity");
        intent.addCategory(Intent.CATEGORY_LAUNCHER);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setComponent(cmp);
        startActivity(intent);
    }


}
