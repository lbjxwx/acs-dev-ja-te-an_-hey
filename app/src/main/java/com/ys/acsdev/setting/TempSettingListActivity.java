package com.ys.acsdev.setting;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.ys.acsdev.R;
import com.ys.acsdev.api.AppConfig;
import com.ys.acsdev.bean.RedioEntity;
import com.ys.acsdev.db.DbTempCompensate;
import com.ys.acsdev.listener.AdapterItemClickListener;
import com.ys.acsdev.listener.RadioChooiceListener;
import com.ys.acsdev.listener.TimeBackListener;
import com.ys.acsdev.setting.adapter.TempAddAdapter;
import com.ys.acsdev.setting.entity.TempAddEntity;
import com.ys.acsdev.ui.BaseActivity;
import com.ys.acsdev.util.SimpleDateUtil;
import com.ys.acsdev.util.TimerUtil;
import com.ys.acsdev.view.MyToastView;
import com.ys.acsdev.view.dialog.OridinryDialog;
import com.ys.acsdev.view.dialog.OridinryDialogClick;
import com.ys.acsdev.view.dialog.RadioListDialog;

import java.util.ArrayList;
import java.util.List;

public class TempSettingListActivity extends BaseActivity implements View.OnClickListener, AdapterItemClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_temp_list);
        intView();
    }

    ListView lv_temp_show;
    TempAddAdapter adapter;
    List<TempAddEntity> tempAddEntityList = new ArrayList<TempAddEntity>();
    Button btn_add_temp;
    RelativeLayout rela_add_temp;
    Button btn_start;
    Button btn_end;
    Button btn_dialog_cacel;
    Button btn_dialog_submit;
    EditText et_tar_num;
    ImageView mIvBack;

    private void intView() {
        mIvBack = (ImageView) findViewById(R.id.mIvBack);
        et_tar_num = (EditText) findViewById(R.id.et_tar_num);
        rela_add_temp = (RelativeLayout) findViewById(R.id.rela_add_temp);
        btn_start = (Button) findViewById(R.id.btn_start);
        btn_end = (Button) findViewById(R.id.btn_end);
        btn_dialog_cacel = (Button) findViewById(R.id.btn_dialog_cacel);
        btn_dialog_submit = (Button) findViewById(R.id.btn_dialog_submit);
        btn_start.setOnClickListener(this);
        btn_end.setOnClickListener(this);
        btn_dialog_cacel.setOnClickListener(this);
        btn_dialog_submit.setOnClickListener(this);

        lv_temp_show = (ListView) findViewById(R.id.lv_temp_show);
        adapter = new TempAddAdapter(TempSettingListActivity.this, tempAddEntityList);
        lv_temp_show.setAdapter(adapter);
        adapter.setItemClickListener(this);
        btn_add_temp = (Button) findViewById(R.id.btn_add_temp);
        btn_add_temp.setOnClickListener(this);
        mIvBack.setOnClickListener(this);
    }

    @Override
    public void adapterItemClick(int id, final Object object, int clickPosition) {
        OridinryDialog oridinryDialog = new OridinryDialog(TempSettingListActivity.this);
        oridinryDialog.setOnDialogClickListener(new OridinryDialogClick() {
            @Override
            public void sure() {
                TempAddEntity tempAddEntity = (TempAddEntity) object;
                String saveId = tempAddEntity.getSateTimeId();
                boolean isDel = DbTempCompensate.deleteTempComById(saveId);
                if (isDel) {
                    getDateFromLocalDb();
                } else {
                    showToastView(getString(R.string.del_failed));
                }
            }

            @Override
            public void noSure() {

            }
        });
        oridinryDialog.show(getString(R.string.if_del_info), getString(R.string.submit), getString(R.string.cacel));
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.mIvBack:
                finish();
                break;
            case R.id.btn_start:
                showStartTimeShowDialog();
                break;
            case R.id.btn_end:
                showEndTimeShowDialog();
                break;
            case R.id.btn_dialog_submit:
                submitInfoToAddView();
                break;
            case R.id.btn_dialog_cacel:
                rela_add_temp.setVisibility(View.GONE);
                btn_start.setText("");
                btn_end.setText("");
                et_tar_num.setText("");
                break;
            case R.id.btn_add_temp:

                rela_add_temp.setVisibility(View.VISIBLE);
                btn_start.setText("6:00");
                btn_end.setText("18:00");
                et_tar_num.setText("");
                startHourMin = "6:00";
                endHourMin = "18:00";
                break;
        }
    }

    private void submitInfoToAddView() {
        boolean isJujle = isInfoJujleRight();
        if (!isJujle) {
            return;
        }
        String targetNum = et_tar_num.getText().toString();
        Intent intent = new Intent();
        intent.setClass(TempSettingListActivity.this, TempCalibraIrActivity.class);
        Log.e("cdl", "=====时间变迁====" + startHourMin + " / " + endHourMin);
        intent.putExtra("targetNum", targetNum);
        intent.putExtra("startHourMin", startHourMin);
        intent.putExtra("endHourMin", endHourMin);
        startActivity(intent);
        rela_add_temp.setVisibility(View.GONE);
    }

    private boolean isInfoJujleRight() {
        String targetNum = et_tar_num.getText().toString();
        if (targetNum == null || targetNum.length() < 2) {
            showToastView(getString(R.string.input_right_temp));
            return false;
        }
        try {
            if (startHourMin == null || startHourMin.length() < 2) {
                showToastView(getString(R.string.input_right_start_time));
                return false;
            }
            if (endHourMin == null || endHourMin.length() < 2) {
                showToastView(getString(R.string.input_right_end_time));
                return false;
            }
            float floatTarNum = Float.parseFloat(targetNum);
            if (floatTarNum < 36) {
                showToastView(getString(R.string.input_right_temp));
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            showToastView(getString(R.string.input_right_temp));
            return false;
        }
        return true;
    }

    private void showToastView(String desc) {
        MyToastView.getInstance().Toast(TempSettingListActivity.this, desc);
    }

    String startHourMin;
    String endHourMin;

    private void showEndTimeShowDialog() {
        List<RedioEntity> listRadio = TimerUtil.getTimeStartInfo();
        int showPosition = 18;
        RadioListDialog radioListDialog = new RadioListDialog(TempSettingListActivity.this);
        radioListDialog.setRadioChooiceListener(new RadioChooiceListener() {
            @Override
            public void backChooiceInfo(RedioEntity redioEntity, int chooicePosition) {
                endHourMin = redioEntity.getRadioText();
                btn_end.setText(endHourMin);
            }
        });
        radioListDialog.show("Time", listRadio, showPosition, "");
    }

    private void showStartTimeShowDialog() {
        List<RedioEntity> listRadio = TimerUtil.getTimeStartInfo();
        int showPosition = 6;
        RadioListDialog radioListDialog = new RadioListDialog(TempSettingListActivity.this);
        radioListDialog.setRadioChooiceListener(new RadioChooiceListener() {
            @Override
            public void backChooiceInfo(RedioEntity redioEntity, int chooicePosition) {
                startHourMin = redioEntity.getRadioText();
                btn_start.setText(startHourMin);
            }
        });
        radioListDialog.show("Time", listRadio, showPosition, "");
    }

    @Override
    protected void onResume() {
        super.onResume();
        getDateFromLocalDb();
    }

    private void getDateFromLocalDb() {
        tempAddEntityList.clear();
        adapter.setTempList(tempAddEntityList);
        tempAddEntityList = DbTempCompensate.queryTempComInfo();
        if (tempAddEntityList == null || tempAddEntityList.size() < 1) {
            return;
        }
        int currentHourMin = SimpleDateUtil.getCurrentHourMin();
        for (int i = 0; i < tempAddEntityList.size(); i++) {
            TempAddEntity tempAddEntity = tempAddEntityList.get(i);
            String startTime = tempAddEntity.getStartTime();
            String endTime = tempAddEntity.getEndTime();
            startTime = startTime.replace(":", "");
            endTime = endTime.replace(":", "");
            int startCom = Integer.parseInt(startTime);
            int endCom = Integer.parseInt(endTime);
            if (startCom < currentHourMin && currentHourMin < endCom) {
                tempAddEntityList.get(i).setChlooice(true);
                break;
            }
        }
        adapter.setTempList(tempAddEntityList);
    }


    @Override
    protected void onStop() {
        super.onStop();
        //更新温度补偿值
        DbTempCompensate.getCurrentTempAdd();
    }

}
