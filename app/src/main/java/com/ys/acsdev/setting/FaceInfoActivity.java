package com.ys.acsdev.setting;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ys.acsdev.R;
import com.ys.acsdev.adapter.FaceInfoNewAdapter;
import com.ys.acsdev.api.AppConfig;
import com.ys.acsdev.bean.FaceInfo;
import com.ys.acsdev.bean.PersonBean;
import com.ys.acsdev.commond.AppInfo;
import com.ys.acsdev.commond.CommondViewModel;
import com.ys.acsdev.db.dao.FaceInfoJavaDao;
import com.ys.acsdev.db.dao.PersonDao;
import com.ys.acsdev.manager.ImageManager;
import com.ys.acsdev.runnable.face.FaceManagerUtil;
import com.ys.acsdev.service.AcsService;
import com.ys.acsdev.service.FaceCheckDownService;
import com.ys.acsdev.ui.BaseActivity;
import com.ys.acsdev.util.FileUtils;
import com.ys.acsdev.util.LogUtil;
import com.ys.acsdev.util.SpUtils;
import com.ys.acsdev.view.dialog.OridinryDialog;
import com.ys.acsdev.view.dialog.OridinryDialogClick;

import org.litepal.crud.callback.FindMultiCallback;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class FaceInfoActivity extends BaseActivity implements View.OnClickListener {

    RecyclerView mRv;
    FaceInfoNewAdapter mAdapter;
    ImageView mIvNet, mIvServer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_face_info);
        initView();
    }

    private void updateLineStatues() {
        MutableLiveData<Boolean> serverState = CommondViewModel.getInstance().getMServerState();
        MutableLiveData<Boolean> netState = CommondViewModel.getInstance().getMNetState();
        mIvServer.setImageResource(serverState != null && serverState.getValue() ? R.mipmap.ic_online : R.mipmap.ic_offline);
        mIvNet.setImageResource(netState != null && netState.getValue() ? R.mipmap.net_line : R.mipmap.net_disline);
        CommondViewModel.getInstance().getMServerState().observe(this, aBoolean -> {
            mIvServer.setImageResource(aBoolean ? R.mipmap.ic_online : R.mipmap.ic_offline);
        });
        CommondViewModel.getInstance().getMNetState().observe(this, isConnected -> {
            mIvNet.setImageResource(isConnected ? R.mipmap.net_line : R.mipmap.net_disline);
        });
    }


    @Override
    protected void onResume() {
        super.onResume();
        getFaceInfo();
    }

    TextView tv_title_face, mTvCount;
    Button btn_register;
    Button btn_unregister;
    Button btn_clear;
    Button btn_add;
    Button btn_get_data;
    ImageView mIvBack;

    private void initView() {
        btn_register = (Button) findViewById(R.id.btn_register);
        btn_register.setOnClickListener(this);
        btn_unregister = (Button) findViewById(R.id.btn_unregister);
        btn_unregister.setOnClickListener(this);

        mIvNet = (ImageView) findViewById(R.id.afi_iv_net);
        mIvServer = (ImageView) findViewById(R.id.afi_iv_server);
        mTvCount = (TextView) findViewById(R.id.afi_tv_count);
        btn_clear = (Button) findViewById(R.id.btn_clear);
        btn_clear.setOnClickListener(this);
        tv_title_face = (TextView) findViewById(R.id.tv_title_face);
        mIvBack = (ImageView) findViewById(R.id.mIvBack);
        mIvBack.setOnClickListener(this);
        mRv = findViewById(R.id.afi_rv);
        mRv.setLayoutManager(new LinearLayoutManager(this));
        mRv.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));

        mAdapter = new FaceInfoNewAdapter(FaceInfoActivity.this, mFaceData);
        mRv.setAdapter(mAdapter);
        btn_add = (Button) findViewById(R.id.btn_add);
        btn_add.setOnClickListener(this);
        btn_get_data = (Button) findViewById(R.id.btn_get_data);
        btn_get_data.setOnClickListener(this);

        mAdapter.setOnAdapterOnItemClickListener((id, object, clickPosition) -> {
            PersonBean selectBean = (PersonBean) object; //获取Item对象
            LogUtil.e("========selectBean:" + selectBean);
            switch (id) {
                case R.id.ifi_btn_del:
                    showDelItemDialog(selectBean);
                    break;
                case R.id.ifi_tv_registed:
                    if (selectBean.isRegister()) {
                        return;
                    }
                    if (selectBean.getRegisterDesc() != "") {
                        showToast(selectBean.getRegisterDesc());
                    }
                    break;
            }
        });
        updateLineStatues();
    }

    /***
     *
     */
    public void showDataByFilter(boolean isFilter) {
        if (mFaceData == null || mFaceData.size() < 1) {
            showToast(getString(R.string.no_data));
            return;
        }
        List<PersonBean> filterList = new ArrayList<PersonBean>();
        for (int i = 0; i < mFaceData.size(); i++) {
            PersonBean personBean = mFaceData.get(i);
            if (personBean.isRegister() == isFilter) {
                filterList.add(personBean);
            }
        }
        mAdapter.setList(filterList);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_register:
                showDataByFilter(true);
                break;
            case R.id.btn_unregister:
                showDataByFilter(false);
                break;
            case R.id.btn_get_data:
                getFaceInfo();
                if (SpUtils.getWorkModel() == AppInfo.MODEL_STAND_ALONE) {
                    return;
                }
                //刷新人员管理得数据
                Intent intent = new Intent(AppInfo.START_CHECK_FACE_DOWN_REGISTER);
                intent.putExtra(AppInfo.START_CHECK_FACE_DOWN_REGISTER, AppInfo.FACE_REGISTER_PERSON);
                sendBroadcast(intent);
                break;
            case R.id.btn_add:
                startActivity(new Intent(FaceInfoActivity.this, AddPersonActivity.class));
                break;
            case R.id.btn_clear:
                showDelAllDialog();
                break;
            case R.id.mIvBack:
                finish();
                break;
        }
    }

    private static final int UPDATE_LIST_VIEW = 9087;
    private static final int UPDATE_LIST_VIEW_NO_DATA = 9088;
    Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case UPDATE_LIST_VIEW_NO_DATA:
                    showToast(getString(R.string.no_data));
                    break;
                case UPDATE_LIST_VIEW:
                    showWait("Load Face Statues...");
                    //先加载列表数据。
                    logInfo("先加载数据");
                    mAdapter.setList(mFaceData);
                    updateFaceRegisterInfo();
                    break;
            }
        }
    };

    private void logInfo(String desc) {
//        LogUtil.persoon("==界面显示===" + desc);
    }

    /***
     * 用来更新人脸注册状态。
     */
    private void updateFaceRegisterInfo() {
        if (mFaceData == null || mFaceData.size() < 1) {
            logInfo("没有数据，，中断");
            hideWait();
            return;
        }
        try {
            Runnable runnable = new Runnable() {
                @Override
                public void run() {
                    for (int i = 0; i < mFaceData.size(); i++) {
                        PersonBean personBean = mFaceData.get(i);
                        if (personBean == null) {
                            continue;
                        }
                        FaceInfo faceInfo = FaceInfoJavaDao.getFaceInfoById(personBean.getPerId());
                        if (faceInfo == null) {
                            personBean.setRegister(false);
                            continue;
                        }
                        boolean isRegisted = faceInfo.getFaceToken() != null && !faceInfo.getFaceToken().isEmpty();
                        personBean.setRegister(isRegisted);
                        logInfo("=====解析注册状态====" + i);
                    }
                    logInfo("=====解析注册状态===over=");

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            logInfo("数据回来了，加载状态");
                            hideWait();
                            mAdapter.setList(mFaceData);
                            String showForst = getString(R.string.registered) + "/" + getString(R.string.unregistered);
                            mTvCount.setText(showForst + " : " + "(" + 0 + "/" + 0 + ")");
                            if (mFaceData == null || mFaceData.size() < 1) {
                                return;
                            }
                            int registed = 0;
                            for (int i = 0; i < mFaceData.size(); i++) {
                                PersonBean personBean = mFaceData.get(i);
                                if (personBean.isRegister()) {
                                    registed = registed + 1;
                                }
                            }
                            mTvCount.setText(showForst + " : " + "(" + registed + " / " + (mFaceData.size() - registed) + ")");
                        }
                    });
                }
            };
            AcsService.getInstance().executor(runnable);
        } catch (Exception e) {
            hideWait();
            logInfo("=====解析注册状态===error=" + e.toString());
            e.printStackTrace();
        }
    }

    List<PersonBean> mFaceData = new ArrayList<>();

    private void getFaceInfo() {
        mFaceData.clear();
        mAdapter.setList(mFaceData);
        showWait(getString(R.string.getting_data));
        PersonDao.getAllPersonByListener(AppInfo.FACE_REGISTER_PERSON, new FindMultiCallback<PersonBean>() {
            @Override
            public void onFinish(List<PersonBean> list) {
                hideWait();
                if (list == null || list.size() < 1) {
                    LogUtil.persoon("=======personBean==null===");
                    mHandler.sendEmptyMessage(UPDATE_LIST_VIEW_NO_DATA);
                    return;
                }
                LogUtil.persoon("=======personBean=====" + list.size());
                for (int i = 0; i < list.size(); i++) {
                    PersonBean personBean = list.get(i);
                    LogUtil.persoon("=======personBean=====" + personBean.toString());
                    mFaceData.add(personBean);
                }
                mHandler.sendEmptyMessage(UPDATE_LIST_VIEW);
            }
        });
        if (SpUtils.getWorkModel() == AppInfo.MODEL_STAND_ALONE) {
            return;
        }
        updateFaceRequestStatues();
    }


    private Handler handler = new Handler();

    /***
     * 获取服务器没有数据，清理本地数据
     */
    private void deleteLocalFaceInfo() {
        sendBroadcast(new Intent(AppInfo.STOP_REQUEST_DOWN_FACE_IMAGE));
        FaceManagerUtil.deleteLocalFaceInfoByType(AppInfo.FACE_REGISTER_PERSON, new FaceManagerUtil.FaceManagerUtilListener() {
            @Override
            public void delPersonInfoBack() {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        mFaceData.clear();
                        mAdapter.setList(mFaceData);
                    }
                });
            }
        });
    }

    /***
     * 更新下载的状态
     */
    private void updateFaceRequestStatues() {
        switch (FaceCheckDownService.faceCurrentStatues) {
            case FaceCheckDownService.FACE_STATUES_DEFAULT:
                tv_title_face.setText(getString(R.string.person_info) + ": " + getString(R.string.register_over));
                break;
            case FaceCheckDownService.FACE_STATUES_REQUEST:  //请求状态
                tv_title_face.setText(getString(R.string.person_info) + ": " + getString(R.string.register_request));
                break;
            case FaceCheckDownService.FACE_STATUES_MATH_LOCAL:  //检查差异性状态
                tv_title_face.setText(getString(R.string.person_info) + ": " + getString(R.string.register_checking));
                break;
            case FaceCheckDownService.FACE_STATUES_DOWNFILE:  //下载状态
                tv_title_face.setText(getString(R.string.person_info) + ": " + getString(R.string.register_downing));
                break;
            case FaceCheckDownService.FACE_STATUES_FACE_REGISTER:  //注册状态
                tv_title_face.setText(getString(R.string.person_info) + ": " + getString(R.string.register_reging));
                break;
        }
    }

    private void showDelItemDialog(PersonBean selectBean) {
        OridinryDialog oridinryDialog = new OridinryDialog(FaceInfoActivity.this);
        oridinryDialog.setOnDialogClickListener(new OridinryDialogClick() {
            @Override
            public void sure() {
                delPersonItemInfo(selectBean);
            }

            @Override
            public void noSure() {

            }
        });
        oridinryDialog.show(getString(R.string.if_del_info), getString(R.string.submit), getString(R.string.cacel));
    }

    private void delPersonItemInfo(PersonBean selectBean) {
        showWait(getString(R.string.deleting));
        if (SpUtils.getWorkModel() == AppInfo.MODEL_STAND_ALONE) {
            PersonDao.delPersonByPerId(selectBean.getPerId());
            FaceInfoJavaDao.deleteById(selectBean.getPerId(), "人员管理--删除单个人员信息");
            getFaceInfo();
            File imageFile = new File(selectBean.getPhotoUrl());
            if (imageFile.exists())
                imageFile.delete();
            showToast(getString(R.string.del_success));
            return;
        }
        showToast(getString(R.string.del_success));
        PersonDao.delPersonByPerId(selectBean.getPerId());
        getFaceInfo();
    }

    private void showDelAllDialog() {
        OridinryDialog oridinryDialog = new OridinryDialog(FaceInfoActivity.this);
        oridinryDialog.setOnDialogClickListener(new OridinryDialogClick() {
            @Override
            public void sure() {
                if (AppConfig.APP_PARSENE_TYPE == AppConfig.PARSENE_ADD_TYPE) {
                    SpUtils.setUpdatePersonTime(0, "清空人员信息");
                }
                String filePath = AppInfo.BASE_IMAGE_PATH;
                FileUtils.deleteDirOrFile(filePath, "手动清理数据库和本地数据");
                deleteLocalFaceInfo();
            }

            @Override
            public void noSure() {

            }
        });
        oridinryDialog.show(getString(R.string.if_del_info), getString(R.string.submit), getString(R.string.cacel));
    }

    @Override
    protected void onStop() {
        ImageManager.clearCache();
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
