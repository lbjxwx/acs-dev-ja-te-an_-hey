//package com.ys.acsdev.setting;
//
//import android.os.Bundle;
//import android.view.View;
//import android.widget.Button;
//import android.widget.EditText;
//
//import com.ys.acsdev.R;
//import com.ys.acsdev.ui.BaseActivity;
//import com.ys.acsdev.util.SpUtils;
//import com.ys.acsdev.view.MyToastView;
//
//public class UrlSettingGXActivity extends BaseActivity {
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_url_setting);
//        initView();
//    }
//
//    EditText et_get_face, et_update_face, et_sysnc_time;
//
//    private void initView() {
//        findViewById(R.id.iv_back).setOnClickListener(view -> onBackPressed());
//        et_get_face = (EditText) findViewById(R.id.et_get_face);
//        et_update_face = (EditText) findViewById(R.id.et_update_face);
//        et_sysnc_time = (EditText) findViewById(R.id.et_sysnc_time);
//        findViewById(R.id.btn_submit).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                sunbmitSpUtilSetting();
//            }
//        });
//    }
//
//    private void sunbmitSpUtilSetting() {
//        String faceinfoUrl = et_get_face.getText().toString().trim();
//        String updateFace = et_update_face.getText().toString().trim();
//        String syncTime = et_sysnc_time.getText().toString().trim();
//        SpUtils.setGetFaceUrl(faceinfoUrl);
//        SpUtils.setUpdateFaceToWeb(updateFace);
//        SpUtils.setSyncTimeUrl(syncTime);
//        MyToastView.getInstance().Toast(UrlSettingGXActivity.this, "设置成功");
//    }
//
//    @Override
//    protected void onResume() {
//        super.onResume();
//        updateFaceInfo();
//    }
//
//    private void updateFaceInfo() {
//        String faceinfoUrl = SpUtils.getGetFaceUrl();
//        String updateFace = SpUtils.getUpdateFaceToWeb();
//        String syncTime = SpUtils.getSyncTimeUrl();
//        et_get_face.setText(faceinfoUrl);
//        et_update_face.setText(updateFace);
//        et_sysnc_time.setText(syncTime);
//    }
//
//}
