package com.ys.acsdev.setting;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.ys.acsdev.R;
import com.ys.acsdev.api.AppConfig;
import com.ys.acsdev.bean.LocalARBean;
import com.ys.acsdev.commond.AppInfo;
import com.ys.acsdev.db.dao.LocalARDao;
import com.ys.acsdev.service.parsener.AcsParsener;
import com.ys.acsdev.service.view.AcsUpdateImageListener;
import com.ys.acsdev.service.view.AcsUpdateView;
import com.ys.acsdev.service.view.AcsView;
import com.ys.acsdev.setting.adapter.AccessOffRecordAdapter;
import com.ys.acsdev.ui.BaseActivity;
import com.ys.acsdev.util.CodeUtil;
import com.ys.acsdev.util.FileUtils;
import com.ys.acsdev.util.LogUtil;
import com.ys.acsdev.util.NetWorkUtils;
import com.ys.acsdev.util.SimpleDateUtil;
import com.ys.acsdev.util.SpUtils;
import com.ys.acsdev.util.ToolUtils;
import com.ys.acsdev.util.file.ExportFileUtil;
import com.ys.acsdev.view.dialog.IntervalDialog;
import com.ys.acsdev.view.dialog.OridinryDialog;
import com.ys.acsdev.view.dialog.OridinryDialogClick;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class AccessOffRecordActivity extends AccessBaseActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_access_off);
        initView();
    }

    ImageView mIvBack;
    TextView tv_delete;
    ListView mRvRecord;
    TextView tv_update_time;
    AccessOffRecordAdapter adapter;
    List<LocalARBean> list = new ArrayList<LocalARBean>();
    Button tv_setting_time;
    Button btn_sync;
    TextView tv_title_attent;
    TextView tv_export;

    private void initView() {
        tv_title_attent = (TextView) findViewById(R.id.tv_title_attent);
        tv_title_attent.setOnClickListener(this);
        tv_setting_time = (Button) findViewById(R.id.tv_setting_time);
        tv_setting_time.setOnClickListener(this);
        tv_update_time = (TextView) findViewById(R.id.tv_update_time);
        mIvBack = (ImageView) findViewById(R.id.mIvBack);
        mIvBack.setOnClickListener(this);

        btn_sync = (Button) findViewById(R.id.btn_sync);
        btn_sync.setOnClickListener(this);

        tv_delete = (TextView) findViewById(R.id.tv_delete);
        tv_delete.setOnClickListener(this);
        mRvRecord = (ListView) findViewById(R.id.mRvRecord);
        adapter = new AccessOffRecordAdapter(AccessOffRecordActivity.this, list);
        mRvRecord.setAdapter(adapter);
        tv_export = findViewById(R.id.tv_export);
        tv_export.setOnClickListener(this);
        LinearLayout llTime = findViewById(R.id.lin_time_setting);
        llTime.setVisibility(SpUtils.getWorkModel() == AppInfo.MODEL_NET ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_export:
                showExportDialog();
                break;
            case R.id.tv_title_attent:
//                addTextInfo();
                break;
            case R.id.btn_sync:
                int workModel = SpUtils.getWorkModel();
                if (workModel == AppInfo.MODEL_STAND_ALONE) {
                    showToast("SINGLE MODEL ，Pleck Check");
                    return;
                }
                if (!NetWorkUtils.isNetworkConnected(AccessOffRecordActivity.this)) {
                    showToast("Net Error ，Pleck Check");
                    return;
                }
                syncAttendToWeb();
                break;
            case R.id.tv_setting_time:
                showInvitDialog();
                break;
            case R.id.tv_delete:
                showDelDialog();
                break;
            case R.id.mIvBack:
                finish();
                break;
        }
    }

    AcsParsener acsParsener;

    private void syncAttendToWeb() {
        LogUtil.update("======syncAttendToWeb=======000=");
        if (acsParsener == null) {
            acsParsener = new AcsParsener(AccessOffRecordActivity.this);
        }
        acsParsener.updateFaceLocalInfoToWeb(true, new AcsUpdateView() {
            @Override
            public void updateFailed(String desc) {
                showToast(desc);
            }

            @Override
            public void updateLocalAtttendPersent(int lastNum) {
                showWait("The remaining: " + lastNum);
            }

            @Override
            public void updateLocalAtttendSuccess() {
                hideWait();
                list.clear();
                adapter.setInfoList(list);
            }
        });
    }

    private void showInvitDialog() {
        IntervalDialog dialog = new IntervalDialog(AccessOffRecordActivity.this);
        dialog.setOnInvitListener(new IntervalDialog.InviTimeBackListener() {
            @Override
            public void backTime(String startTime, String endTime) {
                LogUtil.cdl("==========返回得时间===" + startTime + " / " + endTime);
                SpUtils.setUpdateStartTime(startTime);
                SpUtils.setUpdateEndTime(endTime);
                updateView();
            }
        });
        String startTime = SpUtils.getUpdateStartTime();
        String endTime = SpUtils.getUpdateEndTime();
        dialog.show(startTime, endTime);
    }

    private void showExportDialog() {
        if (list == null || list.size() < 1) {
            showToast(getString(R.string.local_no_access));
            return;
        }
        boolean isUsbExict = checkUsbBindBord();
        if (!isUsbExict) {
            return;
        }
        OridinryDialog oridinryDialog = new OridinryDialog(this);
        oridinryDialog.setOnDialogClickListener(new OridinryDialogClick() {
            @Override
            public void sure() {
                exportAccessToLocal(list);
            }

            @Override
            public void noSure() {

            }
        });
        oridinryDialog.show(
                getString(R.string.save_ar_to_local),
                getString(R.string.confirm),
                getString(R.string.cancel)
        );
    }

    private void showDelDialog() {
        OridinryDialog oridinryDialog = new OridinryDialog(AccessOffRecordActivity.this);
        oridinryDialog.setOnDialogClickListener(new OridinryDialogClick() {
            @Override
            public void sure() {
                delAllInfo();
            }

            @Override
            public void noSure() {

            }
        });
        oridinryDialog.show(getString(R.string.if_del_info), getString(R.string.submit), getString(R.string.cacel));
    }

    private void delAllInfo() {
        if (list == null || list.size() < 1) {
            showToast(getString(R.string.no_data));
            return;
        }
        String cachePath = AppInfo.BASE_PATH_CACHE;
        FileUtils.deleteDirOrFile(cachePath, "清理离线记录");
        LocalARDao.delAll();
        getAccInfoFromLocalDb();
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateView();
        getAccInfoFromLocalDb();
    }

    private void updateView() {
        String startTime = SpUtils.getUpdateStartTime();
        String endTime = SpUtils.getUpdateEndTime();
        tv_update_time.setText(getString(R.string.start_time) + ": " + startTime + "   " + getString(R.string.end_time) + ": " + endTime);
    }

    private void getAccInfoFromLocalDb() {
        list.clear();
        adapter.setInfoList(list);
        showWait("Loading...");
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                list = LocalARDao.getAll();
                if (list == null || list.size() < 1) {
                    handler.sendEmptyMessage(MESSAGE_NO_DATE);
                    return;
                }
                handler.sendEmptyMessage(MESSAGE_TO_UPDATE_VIEW);
            }
        };
        Thread thread = new Thread(runnable);
        thread.start();
    }

    private void addTextInfo() {
        showWait("Loading...");
        new Thread() {
            @Override
            public void run() {
                super.run();
                String codeNum = CodeUtil.getUniquePsuedoID();
                for (int i = 0; i < 6000; i++) {
                    int randomNum = new Random().nextInt(20000);
                    String recordTime = System.currentTimeMillis() + randomNum + "";
                    LocalARBean LocalARBean = new LocalARBean(recordTime, AppInfo.PASSTYPE_CARD, "13999999999", codeNum,
                            "", "", "36.5", "ESON TEST", "123456789", "1", "0");
                    LocalARDao.saveLocalBean(LocalARBean);
                    Message message = new Message();
                    message.what = MESSAGE_UPDATE_ADD_NUM;
                    message.obj = i;
                    handler.sendMessage(message);
                }
                handler.sendEmptyMessage(MESSAGE_ADD_NUM_COMPANY);
            }
        }.start();
    }

    private static final int MESSAGE_NO_DATE = 5645;           //没有数据，刷新界面
    private static final int MESSAGE_TO_UPDATE_VIEW = 5646;     //获取数据成功，刷新界面
    private static final int MESSAGE_UPDATE_ADD_NUM = 5647;    //增加数据-变量
    private static final int MESSAGE_ADD_NUM_COMPANY = 5648;  //添加数据完成，刷新数据
    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case MESSAGE_ADD_NUM_COMPANY:
                    hideWait();
                    getAccInfoFromLocalDb();
                    break;
                case MESSAGE_UPDATE_ADD_NUM:
                    int num = (int) msg.obj;
                    tv_title_attent.setText("AddNum: " + num);
                    break;
                case MESSAGE_TO_UPDATE_VIEW:
                    hideWait();
                    Collections.reverse(list);
                    //list = ToolUtils.sortLocalAR(list);
                    LogUtil.cdl("========list======" + list.size());
                    adapter.setInfoList(list);
                    tv_title_attent.setText(getString(R.string.office_record) + "(" + list.size() + ")");
                    break;
                case MESSAGE_NO_DATE:
                    hideWait();
                    tv_title_attent.setText(getString(R.string.office_record));
                    showToast(getString(R.string.no_data));
                    break;
            }
        }
    };
}
