package com.ys.acsdev.setting.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.ys.acsdev.R;
import com.ys.acsdev.bean.FaceInfo;
import com.ys.acsdev.bean.PersonBean;
import com.ys.acsdev.commond.AppInfo;
import com.ys.acsdev.db.dao.FaceInfoJavaDao;
import com.ys.acsdev.manager.ImageManager;
import com.ys.acsdev.util.LogUtil;

import java.util.ArrayList;
import java.util.List;

public class VisitorInfoNewAdapter extends RecyclerView.Adapter<VisitorInfoNewAdapter.ViewHolder> {

    List<PersonBean> mFaceData = new ArrayList<PersonBean>();
    Context context;

    public VisitorInfoNewAdapter(Context context, List<PersonBean> mFaceData) {
        this.context = context;
        this.mFaceData.clear();
        this.mFaceData.addAll(mFaceData);
    }

    public void setList(List<PersonBean> mFaceData) {
        this.mFaceData.clear();
        this.mFaceData.addAll(mFaceData);
        notifyDataSetChanged();
    }

    @Override
    public VisitorInfoNewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_visitor_info, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        PersonBean visitorEntity = mFaceData.get(position);
        if (visitorEntity == null) {
            return;
        }
        FaceInfo faceInfo = FaceInfoJavaDao.getFaceInfoById(visitorEntity.getPerId());
        boolean isRegisted = faceInfo != null && faceInfo.getFaceToken() != null && !faceInfo.getFaceToken().isEmpty();
        String name = visitorEntity.getName();
        LogUtil.cdl("===adapter===" + name);
        holder.ivi_tv_name.setText(name);
        holder.ivi_tv_registed.setText(isRegisted ? context.getString(R.string.registered) : context.getString(R.string.unregistered));
        holder.ivi_tv_registed.setTextColor(isRegisted ? context.getResources().getColor(R.color.green) : context.getResources().getColor(R.color.red));

        int faceType = visitorEntity.getType();
        if (faceType == AppInfo.FACE_REGISTER_BLACK_LIST) {
            holder.ivi_tv_valid_time.setText("Forbid");
        } else {
            holder.ivi_tv_valid_time.setText(visitorEntity.getSyncTime());
        }
        String url = visitorEntity.getPhotoUrl();
        if (!TextUtils.isEmpty(url) && !url.startsWith("http")) {
            url = AppInfo.getBaseUrl() + url;
        }
        ImageManager.displayImageDefault(url, holder.ivi_iv_face, R.mipmap.face);
    }

    @Override
    public int getItemCount() {
        return mFaceData.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView ivi_iv_face;
        TextView ivi_tv_name, ivi_tv_registed, ivi_tv_valid_time;

        public ViewHolder(View itemView) {
            super(itemView);
            ivi_iv_face = (ImageView) itemView.findViewById(R.id.ivi_iv_face);
            ivi_tv_name = (TextView) itemView.findViewById(R.id.ivi_tv_name);
            ivi_tv_registed = (TextView) itemView.findViewById(R.id.ivi_tv_registed);
            ivi_tv_valid_time = (TextView) itemView.findViewById(R.id.ivi_tv_valid_time);
        }
    }
}