package com.ys.acsdev.setting.view;

public interface SettingMenuView {

    void showWaitDialog(boolean isShow);

    void syncDevInfoFromWeb(boolean isTrue, String errorDesc);

}
