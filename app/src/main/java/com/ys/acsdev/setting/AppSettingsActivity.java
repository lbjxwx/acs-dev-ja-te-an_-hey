package com.ys.acsdev.setting;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.ys.acsdev.R;
import com.ys.acsdev.commond.AppInfo;
import com.ys.acsdev.light.LightManager;
import com.ys.acsdev.listener.MoreButtonToggleListener;
import com.ys.acsdev.manager.DoorManager;
import com.ys.acsdev.ui.BaseActivity;
import com.ys.acsdev.util.SpUtils;
import com.ys.acsdev.view.MoreButtonToggle;

import org.jetbrains.annotations.Nullable;

public class AppSettingsActivity extends BaseActivity implements View.OnClickListener, MoreButtonToggleListener {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_settings);
        initView();
    }

    ImageView mIvBack;
    Button btn_submit;
    MoreButtonToggle more_btn_white;
    MoreButtonToggle more_btn_green;
    MoreButtonToggle more_btn_red;
    MoreButtonToggle more_btn_red_line;
    MoreButtonToggle more_btn_open_door_test;

    private void initView() {
        mIvBack = (ImageView) findViewById(R.id.mIvBack);
        mIvBack.setOnClickListener(this);
        btn_submit = (Button) findViewById(R.id.btn_submit);
        btn_submit.setOnClickListener(this);
        more_btn_open_door_test = (MoreButtonToggle) findViewById(R.id.more_btn_open_door_test);
        more_btn_open_door_test.setOnMoretListener(this);
        more_btn_white = (MoreButtonToggle) findViewById(R.id.more_btn_white);
        more_btn_white.setOnMoretListener(this);
        more_btn_green = (MoreButtonToggle) findViewById(R.id.more_btn_green);
        more_btn_green.setOnMoretListener(this);
        more_btn_red = (MoreButtonToggle) findViewById(R.id.more_btn_red);
        more_btn_red.setOnMoretListener(this);
        more_btn_red_line = (MoreButtonToggle) findViewById(R.id.more_btn_red_line);
        more_btn_red_line.setOnMoretListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_submit:
                //上传工作日志
                sendBroadcast(new Intent(AppInfo.UPDATE_LOCAL_RUN_LOG_TO_WEB));
                break;
            case R.id.mIvBack:
                onBackPressed();
                break;
        }

    }

    @Override
    public void switchToggleView(View view, boolean isChooice) {
        switch (view.getId()) {
            case R.id.more_btn_open_door_test:
                openDoorTest(isChooice);
                break;
            case R.id.more_btn_white:
                if (isChooice) {
                    LightManager.getInstance().openWhiteLight();
                } else {
                    LightManager.getInstance().closeWhiteLight();
                }
                break;
            case R.id.more_btn_green:
                if (isChooice) {
                    LightManager.getInstance().openGreenLight();
                } else {
                    LightManager.getInstance().closeGreenLight();
                }
                break;
            case R.id.more_btn_red:
                if (isChooice) {
                    LightManager.getInstance().openRedLight();
                } else {
                    LightManager.getInstance().closeRedLight();
                }
                break;
            case R.id.more_btn_red_line:
                if (isChooice) {
                    LightManager.getInstance().openRedLineLight();
                } else {
                    LightManager.getInstance().closeRedLineLight();
                }
                break;
        }
    }

    private void openDoorTest(boolean isChooice) {
        boolean isOpenDoor = SpUtils.getDoorEnable();
        if (!isOpenDoor) {
            showToast("Please Open door switch");
            return;
        }
        try {
            if (isChooice) {
                DoorManager.getInstance().open("开门测试==点击开门");
            } else {
                DoorManager.getInstance().close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
