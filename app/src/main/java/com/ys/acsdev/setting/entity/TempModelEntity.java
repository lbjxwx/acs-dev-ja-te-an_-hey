package com.ys.acsdev.setting.entity;

public class TempModelEntity {

    int tempTyle;
    String tempName;

    public static final int CLOSE = 0;  //关
    public static final int IIC_MLX_90621_BAB = 1;  //I2C矩阵(MLX90621_BAB)
    public static final int SP_MLX_90621_RR = 2;  //串口矩阵(MLX90621_RR)
    public static final int SP_MHY_10724 = 3;  //串口矩阵(MHY10724)
    public static final int SP_MLX_90621_STM = 4;  //串口矩阵(MLX90621_YS)
    public static final int IIC_MLX_90640_0AA1547511 = 5;  //I2C矩阵(90640_0AA1547511)
    public static final int IIC_MLX_90614 = 6;  //I2C单点(IMLX90614)
    public static final int SP_XRJ_S60 = 7;  // 串口矩阵(XRJ-S60)
    public static final int SP_HM_32X32 = 8;  //串口矩阵(HM_RB32X3290_A)
    public static final int IIC_MLX_90641 = 9;  //I2C矩阵(90641)
    public static final int SP_MLX_90641_ESF = 10;  //串口矩阵(90641_ESF)
    public static final int SP_MLX_90621_TIR_SN = 11;  //串口矩阵(MLX90621_TIR)
    public static final int SP_MLX_90621_BAA = 12;  //串口矩阵(MLX90621_BAA)
    public static final int SP_MLX_90621_HY = 13;  //串口矩阵(MLX90621_HY)
    public static final int SP_MLX_90621_JST = 14;  //串口矩阵(MLX621_JST)
    public static final int IIC_MLX_90621_BAA = 15;  //I2C矩阵(MLX90621_BAA)
    public static final int SP_MLX_90621_BAA_ZM_TXT = 16;  //串口矩阵(MLX90621_BAA_Single)
    public static final int SP_MLX_90621_BAA_ZM_HEX = 17; //串口矩阵(MLX90621_BAA_周蒙)
    public static final int SP_MLX_90621_HY1 = 18; //串口矩阵(MLX90621_BAA_华一)
    public static final int SP_HM_MT031 = 19;      //串口矩阵(MLX9hm_mt031 奥朗德)
    public static final int SP_KM_D801 = 20; //串口矩阵(km_d801 慧科)
    public static final int SP_OTPA_16 = 21; //串口矩阵(muc 星马)-原 MUC 探头
    public static final int SP_MLX_90641_YNC = 22; //串口矩阵(天地印象 90641 BCB)
    public static final int SP_OMR_D6T_4X4 = 23; //串口矩阵(欧姆龙 华一)
    public static final int IIC_OTPA_16 = 24; //I2C(muc 星马)
    public static final int SP_MLX_90641_STM = 25; //串口矩阵(90641 stm)
    public static final int SP_HM_V11 = 26;       //串口矩阵(hm v11 5.0)
    public static final int USB_OTPA_MC3216 = 27; //串口矩阵(SMLX_MC3216)
    public static final int IIC_HPTA_32 = 28; //i2c矩阵(IIC-HPTA-32x32)
    public static final int SP_EXTERNAL_SMALL_PLATE = 30; //串口矩阵(欧姆龙--公用小板集合
    public static final int SP_HM5_0_STM_YS = 31;    //海曼5.0 亿晟

    public static final int CUSTOM_ALL = -1;

    public TempModelEntity(int tempTyle, String tempName) {
        this.tempTyle = tempTyle;
        this.tempName = tempName;
    }

    public int getTempTyle() {
        return tempTyle;
    }

    public void setTempTyle(int tempTyle) {
        this.tempTyle = tempTyle;
    }

    public String getTempName() {
        return tempName;
    }

    public void setTempName(String tempName) {
        this.tempName = tempName;
    }
}
