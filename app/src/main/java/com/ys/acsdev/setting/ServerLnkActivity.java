package com.ys.acsdev.setting;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hwangjr.rxbus.RxBus;
import com.hwangjr.rxbus.annotation.Subscribe;
import com.hwangjr.rxbus.thread.EventThread;
import com.ys.acsdev.R;
import com.ys.acsdev.api.ApiHelper;
import com.ys.acsdev.api.AppConfig;
import com.ys.acsdev.attendance.dao.ClockInDao;
import com.ys.acsdev.bean.LinkServerEvent;
import com.ys.acsdev.bean.SocketEntity;
import com.ys.acsdev.commond.AppInfo;
import com.ys.acsdev.db.dao.LocalARDao;
import com.ys.acsdev.db.dao.SocketLineDb;
import com.ys.acsdev.helper.TcpHelperNew;
import com.ys.acsdev.manager.ImageManager;
import com.ys.acsdev.service.AcsService;
import com.ys.acsdev.service.SocketService;
import com.ys.acsdev.setting.parsener.ServerLinkParsener;
import com.ys.acsdev.setting.parsener.ServerLinkView;
import com.ys.acsdev.ui.ActiviActivity;
import com.ys.acsdev.ui.BaseActivity;
import com.ys.acsdev.util.CodeUtil;
import com.ys.acsdev.util.FileUtils;
import com.ys.acsdev.util.KeyBoardUtil;
import com.ys.acsdev.util.LogUtil;
import com.ys.acsdev.util.NetWorkUtils;
import com.ys.acsdev.util.SpUtils;
import com.ys.acsdev.view.MyToastView;
import com.ys.acsdev.view.WaitDialogUtil;
import com.ys.acsdev.view.dialog.OridinryDialog;
import com.ys.acsdev.view.dialog.OridinryDialogClick;
import com.ys.acsdev.view.dialog.SocketLogDialog;
import com.ys.qrcodelib.QRCodeUtil;

import org.jetbrains.annotations.Nullable;

import java.util.List;

public class ServerLnkActivity extends BaseActivity implements View.OnClickListener, ServerLinkView {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        RxBus.get().register(ServerLnkActivity.this);
        setContentView(R.layout.activity_link_server);
        initView();
    }

    ImageView mIvBack;
    FrameLayout frame_bgg;
    TextView mTvState;
    EditText mEtIP, mEtPort;
    Button btn_link_server, btn_line_log;
    ServerLinkParsener serverLinkParsener;
    ImageView iv_scan_code;
    Button btn_activit;

    private void initView() {
        btn_activit = (Button) findViewById(R.id.btn_activit);
        btn_activit.setOnClickListener(this);
        frame_bgg = (FrameLayout) findViewById(R.id.frame_bgg);
        frame_bgg.setOnClickListener(this);
        iv_scan_code = (ImageView) findViewById(R.id.iv_scan_code);
        mIvBack = (ImageView) findViewById(R.id.mIvBack);
        mIvBack.setOnClickListener(this);
        mTvState = (TextView) findViewById(R.id.mTvState);
        btn_line_log = (Button) findViewById(R.id.btn_line_log);
        btn_line_log.setOnClickListener(this);
        btn_link_server = (Button) findViewById(R.id.btn_link_server);
        btn_link_server.setOnClickListener(this);
        mEtIP = (EditText) findViewById(R.id.mEtIP);
        mEtPort = (EditText) findViewById(R.id.mEtPort);
        mEtIP.setText(SpUtils.getmServerIp());
        mEtPort.setText(SpUtils.getmServerPort());
        serverLinkParsener = new ServerLinkParsener(ServerLnkActivity.this, this);
        updateLineStatuesView();
    }

    private void updateLineStatuesView() {
        boolean isConnected = AppInfo.isConnectServer;
        if (isConnected) {
            mTvState.setTextColor(getResources().getColor(R.color.black));
            mTvState.setText(getString(R.string.connected));
        } else {
            mTvState.setTextColor(getResources().getColor(R.color.red));
            mTvState.setText(getString(R.string.unconnected));
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_activit:
                startActivity(new Intent(ServerLnkActivity.this, ActiviActivity.class));
                break;
            case R.id.btn_line_log:
                showLogInfoDialog();
                break;
            case R.id.btn_link_server:
                sendBroadcast(new Intent(AppInfo.STOP_REQUEST_DOWN_FACE_IMAGE));
                String ipAddress = getIpAddress();
                String ipSave = SpUtils.getmServerIp();
                if (!ipAddress.equals(ipSave)) {
                    //切换服务器，这里去清理数据
                    showDealLocalInfoDialog();
                    return;
                }
                linkServer();
//                LinkServerEvent(new LinkServerEvent(TcpHelperNew.SERVER_CODE_CONNECTING, "准备注册"));
//                serverLinkParsener.registerEquipmentToServer(getIpAddress(), getServerPort(), getUserName());
                break;
            case R.id.mIvBack:
                onBackPressed();
                break;
            case R.id.frame_bgg:
                KeyBoardUtil.hiddleBord(frame_bgg);
                break;
        }
    }

    @Override
    public void checkRegisterStatues(boolean isTrue, String errorDesc) {
        LogUtil.message("=====注册状态===" + isTrue + " / " + errorDesc);
        showWaitDialog(false);
        showToastView(errorDesc);
        if (!isTrue) {
            return;
        }
        LogUtil.message("=====注册状态==准备去连接服务器=");
        linkServer();
    }

    private void linkServer() {
        String ipAddress = getIpAddress();
        boolean jujle = serverLinkParsener.jujleInfoRight(ipAddress, getServerPort());
        if (!jujle) {
            return;
        }
        //保存信息到数据库，用来快速连接到服务器
        SocketEntity socketEntity = new SocketEntity(SpUtils.getmServerIp(), SpUtils.getmServerPort(), SpUtils.getUserName());
        SocketLineDb.saveSocketLineStateInfo(socketEntity);
        if (!NetWorkUtils.isNetworkConnected(this)) {
            showToast(getString(R.string.network_anomaly));
            return;
        }
        LogUtil.message("开始连接: " + SpUtils.getSettingPassword() + "/" + SpUtils.getmServerPort() + "/" + SpUtils.getUserName(), true);
        mTvState.setText(getString(R.string.connecting));
        showWaitDialog(true);
        //更换服务器IP的时候更新httpClient
        ApiHelper.INSTANCE.createApi(true);
        SocketService.getInstance().startHeartbeat(AppInfo.HEART_STATUES_FROM_OTHER_ACTIVITY);
        handler.sendEmptyMessageDelayed(MESSAGE_CLOSE_DIALOG, 7000);
    }

    private void showDealLocalInfoDialog() {
        OridinryDialog oridinryDialog = new OridinryDialog(ServerLnkActivity.this);
        oridinryDialog.setOnDialogClickListener(new OridinryDialogClick() {
            @Override
            public void sure() {
                if (AppConfig.APP_PARSENE_TYPE == AppConfig.PARSENE_ADD_TYPE) {
                    SpUtils.setUpdatePersonTime(0, "切换服务器，清理本地数据，重新拉取数据");
                }
                delLocalAcrrodInfo();
                AppInfo.resetVisitorQr = true;
                linkServer();
            }

            @Override
            public void noSure() {

            }
        });
        oridinryDialog.show(getString(R.string.change_server_content), getString(R.string.submit), getString(R.string.cacel));
    }

    private void delLocalAcrrodInfo() {
        //删除离线通行记录
        String cachePath = AppInfo.BASE_PATH_CACHE;
        FileUtils.deleteDirOrFile(cachePath, "清理离线记录");
        LocalARDao.delAll();
        //删除离线考勤记录
        ClockInDao.delAlCockIn();
        //删除本地访客图片
        FileUtils.delQrCodePath("切换服务器，删除访客二维码");
    }

    @Subscribe(thread = EventThread.MAIN_THREAD)
    public void LinkServerEvent(LinkServerEvent event) {
        switch (event.getCode()) {
            case TcpHelperNew.SERVER_CODE_CONNECTING:
                mTvState.setText(getString(R.string.connecting));
                break;
            case TcpHelperNew.SERVER_CODE_CONNECTED:
                showWaitDialog(false);
                mTvState.setTextColor(0xff000000);
                mTvState.setText(getString(R.string.connected));
                break;
            case TcpHelperNew.SERVER_CODE_DISCONNECT:
                mTvState.setText(getString(R.string.disconnect));
                showToast("Code => " + event.getCode() + "\nMessage => " + event.getMsg());
                break;
            default:
                mTvState.setTextColor(0xffF0001D);
                showToast("Code => " + event.getCode() + "\nMessage => " + event.getMsg());
                mTvState.setText(getString(R.string.conn_failed));
                break;
        }
    }

    /***
     * 显示连接记录
     */
    private void showLogInfoDialog() {
        List<SocketEntity> list = SocketLineDb.getSocketInfoList();
        if (list == null || list.size() < 1) {
            showToast("No Data");
            return;
        }
        SocketLogDialog socketLogDialog = new SocketLogDialog(this);
        socketLogDialog.setOnDialogClickListener(new SocketLogDialog.SocketClickListener() {
            @Override
            public void clickSocketEntity(SocketEntity socketEntity) {
                if (socketEntity == null) {
                    return;
                }
                showToast("Modify Success!");
                String ip = socketEntity.getIp();
                String port = socketEntity.getPort();
                mEtIP.setText(ip);
                mEtPort.setText(port);
            }

            @Override
            public void delSocketEntity(SocketEntity socketEntity) {
                if (socketEntity == null) {
                    return;
                }
                showToast("Delete Info Success");
                String ip = socketEntity.getIp();
                SocketLineDb.delPowerInfoByIp(ip);
            }
        });
        socketLogDialog.show(list);
    }

    private static final int MESSAGE_CLOSE_DIALOG = 876;
    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case MESSAGE_CLOSE_DIALOG:
                    showWaitDialog(false);
                    updateLineStatuesView();
                    break;
            }
        }
    };


    WaitDialogUtil waitDialogUtil;

    @Override
    public void showToastView(String desc) {
        MyToastView.getInstance().Toast(ServerLnkActivity.this, desc);
    }

    @Override
    public void showWaitDialog(boolean b) {
        if (waitDialogUtil == null) {
            waitDialogUtil = new WaitDialogUtil(ServerLnkActivity.this);
        }
        if (b) {
            waitDialogUtil.show("Loading . . .");
        } else {
            waitDialogUtil.dismiss();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateBindQrCode();
    }

    public void updateBindQrCode() {
        String devData = "{\"type\":\"addDev\",\"mac\": \"" + CodeUtil.getUniquePsuedoID() + "\"}";
        String qrCodePath = AppInfo.QR_BIND_PATH;
        QRCodeUtil runnable = new QRCodeUtil(devData, qrCodePath, new QRCodeUtil.ErCodeBackListener() {

            @Override
            public void createErCodeState(String errorDes, boolean isCreate, String path) {
                if (!isCreate) {
                    return;
                }
                ImageManager.displayImagePath(path, iv_scan_code);
            }
        });
        AcsService.getInstance().executor(runnable);
    }


    @Override
    protected void onDestroy() {
        RxBus.get().unregister(ServerLnkActivity.this);
        super.onDestroy();
        if (handler != null) {
            handler.removeMessages(MESSAGE_CLOSE_DIALOG);
        }
    }

    private String getServerPort() {
        return mEtPort.getText().toString().trim();
    }

    private String getIpAddress() {
        return mEtIP.getText().toString().trim();
    }

}
