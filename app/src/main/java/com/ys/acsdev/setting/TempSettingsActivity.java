package com.ys.acsdev.setting;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.ys.acsdev.R;
import com.ys.acsdev.api.AppConfig;
import com.ys.acsdev.bean.BeanDefaultEntity;
import com.ys.acsdev.bean.RedioEntity;
import com.ys.acsdev.listener.MoreButtonListener;
import com.ys.acsdev.listener.MoreButtonToggleListener;
import com.ys.acsdev.listener.RadioChooiceListener;
import com.ys.acsdev.manager.TempManager;
import com.ys.acsdev.setting.entity.TempModelEntity;
import com.ys.acsdev.setting.util.TempModelUtil;
import com.ys.acsdev.ui.BaseActivity;
import com.ys.acsdev.util.SpUtils;
import com.ys.acsdev.view.MoreButton;
import com.ys.acsdev.view.MoreButtonToggle;
import com.ys.acsdev.view.dialog.EditTextDialog;
import com.ys.acsdev.view.dialog.RadioListDialog;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class TempSettingsActivity extends BaseActivity implements MoreButtonToggleListener, MoreButtonListener {

    Button mBtnSave, mBtnReset;
    MoreButtonToggle more_show_temp, more_height_speak, more_speak_temp;
    ImageView mIvBack;
    MoreButton more_hight_wear, mBtnTempType, mBtnTempUnit,
            mBtnTempDev, mBtnTempRare, mBtnTempLow, mBtnTempTime, more_btn_auto, more_btn_temp_calibration;
    LinearLayout lin_serial, lin_default;
    ArrayList<String> mDevicePaths = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_temp_settings);
        initView();
        initDevPaths();
    }

    @Override
    protected void onResume() {
        super.onResume();
        initData();
    }

    @Override
    public void switchToggleView(View view, boolean isChooice) {
        switch (view.getId()) {
            case R.id.more_speak_temp:
                SpUtils.setSpeakTempNum(isChooice);
                initData();
                break;
            case R.id.more_height_speak:
                SpUtils.setPlayWearMedia(isChooice);
                initData();
                break;
            case R.id.more_show_temp:
                SpUtils.setShowTempIcon(isChooice);
                initData();
                break;
        }
    }

    @Override
    public void clickView(View view) {
        switch (view.getId()) {
            case R.id.more_hight_wear:
                showTempHeightDialog();
                break;
            case R.id.mBtnTempType:
                showChhoiceTempType();
                break;
            case R.id.mBtnTempUnit:
                showChooiceTempUnitDialog();
                break;
            case R.id.mBtnTempDev:
                showTempDevDialog();
                break;
            case R.id.mBtnTempRare:
                showTemoRareDialog();
                break;
            case R.id.mBtnTempLow:
                showTempLowDialog();
                break;
            case R.id.mBtnTempTime:
                showTempTimeDialog();
                break;
            case R.id.more_btn_auto:
                startActivity(new Intent(TempSettingsActivity.this, TempSettingListActivity.class));
                break;
            case R.id.more_btn_temp_calibration:
                jumpTempCalibration();
                break;
        }
    }

    private void initView() {
        lin_serial = (LinearLayout) findViewById(R.id.lin_serial);
        lin_default = (LinearLayout) findViewById(R.id.lin_default);
        mBtnSave = (Button) findViewById(R.id.mBtnSave);
        mBtnReset = findViewById(R.id.mBtnReset);
        mBtnReset.setOnClickListener(view -> {
            SpUtils.setSharedDefaultInfo();
            showRebootDialog();
        });
        mBtnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showRebootDialog();
            }
        });

        more_height_speak = (MoreButtonToggle) findViewById(R.id.more_height_speak);
        more_height_speak.setOnMoretListener(this);
        more_show_temp = (MoreButtonToggle) findViewById(R.id.more_show_temp);
        more_show_temp.setOnMoretListener(this);
        mIvBack = (ImageView) findViewById(R.id.mIvBack);
        mIvBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        //高温预警功能
        more_hight_wear = (MoreButton) findViewById(R.id.more_hight_wear);
        more_hight_wear.setOnMoretListener(this);

        //硬件类型
        mBtnTempType = (MoreButton) findViewById(R.id.mBtnTempType);
        mBtnTempType.setOnMoretListener(this);

        mBtnTempUnit = (MoreButton) findViewById(R.id.mBtnTempUnit);
        mBtnTempUnit.setOnMoretListener(this);

        mBtnTempDev = (MoreButton) findViewById(R.id.mBtnTempDev);
        mBtnTempDev.setOnMoretListener(this);
        //波特率选择
        mBtnTempRare = (MoreButton) findViewById(R.id.mBtnTempRare);
        mBtnTempRare.setOnMoretListener(this);
        //设置有效温度
        mBtnTempLow = (MoreButton) findViewById(R.id.mBtnTempLow);
        mBtnTempLow.setOnMoretListener(this);
        mBtnTempTime = (MoreButton) findViewById(R.id.mBtnTempTime);
        mBtnTempTime.setOnMoretListener(this);

        more_btn_auto = (MoreButton) findViewById(R.id.more_btn_auto);
        more_btn_auto.setOnMoretListener(this);
        more_btn_temp_calibration = (MoreButton) findViewById(R.id.more_btn_temp_calibration);
        more_btn_temp_calibration.setOnMoretListener(this);
        more_speak_temp = (MoreButtonToggle) findViewById(R.id.more_speak_temp);
        more_speak_temp.setOnMoretListener(this);
    }


    private void initDevPaths() {
        File dev = new File("/dev");
        File[] files = dev.listFiles();
        int i;
        for (i = 0; i < files.length; i++) {
            if (files[i].getAbsolutePath().startsWith("/dev/tty")) {
                mDevicePaths.add(files[i].getAbsolutePath());
            }
        }
    }

    //此为调用模块温度校准
    private void jumpTempCalibration() {
        EditTextDialog editTextDialog = new EditTextDialog(TempSettingsActivity.this);
        editTextDialog.setOnDialogClickListener(new EditTextDialog.EditTextDialogListener() {
            @Override
            public void commit(String content) {
                if (content == null || content.length() < 2) {
                    showToast(getString(R.string.input_right_temp));
                    return;
                }
                float tempFloat = 36.5f;
                try {
                    tempFloat = Float.parseFloat(content);
                } catch (Exception e) {
                    showToast(getString(R.string.input_right_temp));
                    e.printStackTrace();
                    return;
                }
                Intent intent = new Intent(TempSettingsActivity.this, TempCalibraIrActivity.class);
                intent.putExtra("type", 1);
                intent.putExtra("targetNum", tempFloat + "");
                startActivity(intent);
            }

            @Override
            public void hiddleTestClick() {

            }
        });
        editTextDialog.show(
                getString(R.string.input_target_temp),
                "36.5",
                getString(R.string.submit),
                getString(R.string.cacel)
        );
    }


    /***
     * 显示高温预警得温度弹窗
     */
    private void showTempHeightDialog() {
        float tempSave = SpUtils.getTempHeightWearNew();
        EditTextDialog editTextDialog = new EditTextDialog(TempSettingsActivity.this);
        editTextDialog.setOnDialogClickListener(new EditTextDialog.EditTextDialogListener() {
            @Override
            public void commit(String content) {
                if (content == null || content.length() < 2) {
                    showToast(getString(R.string.effective_value));
                    return;
                }
                float tempFloat = 20.0f;
                try {
                    tempFloat = Float.parseFloat(content);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (tempFloat < 37.0) {
                    showToast(getString(R.string.effective_value));
                    return;
                }
                SpUtils.setTempHeightWearNew(tempFloat);
                showToast(getString(R.string.set_success));
                initData();
            }

            @Override
            public void hiddleTestClick() {

            }
        });
        editTextDialog.show(
                getString(R.string.temp_height_wear),
                tempSave + "",
                getString(R.string.submit),
                getString(R.string.cacel)
        );
    }

    private void initData() {
        more_speak_temp.setChcekcAble(SpUtils.getSpeakTempNum());
        more_height_speak.setChcekcAble(SpUtils.getPlayWearMedia());
        more_show_temp.setChcekcAble(SpUtils.getShowTempIcon());
        more_hight_wear.setRigt(SpUtils.getTempHeightWearNew() + "");
        more_btn_auto.setRigt(SpUtils.getmTempComp() + "");
        mBtnTempLow.setRigt(SpUtils.getmTempLow() + "");
        mBtnTempTime.setRigt(SpUtils.getmTempTime() + "");
        more_btn_temp_calibration.setVisibility(SpUtils.getmTempType() == TempModelEntity.SP_HM_MT031 ? View.VISIBLE : View.GONE);

        String tempUnit = "";
        if (SpUtils.getTempUnit() == 0) {
            tempUnit = getString(R.string.centigrade);
        } else {
            tempUnit = getString(R.string.fahrenhite);
        }
        mBtnTempUnit.setRigt(tempUnit);
        String tempTye = TempModelUtil.getCurrentTempType(TempSettingsActivity.this);
        mBtnTempType.setRigt(tempTye);
        updateTempView(SpUtils.getmTempType());
//        String tempDev = TempModelUtil.getCurrentTTYS();
        mBtnTempDev.setRigt(SpUtils.getTempDevPath());
        String tempRare = TempModelUtil.getmCurrenTempRare();
        mBtnTempRare.setRigt(tempRare);
        hiddleViewByCustomer();
    }

    private void hiddleViewByCustomer() {
        boolean isSingleModel = SpUtils.getSettingSimpleModel();
        more_show_temp.setVisibility(isSingleModel ? View.GONE : View.VISIBLE);  //显示测温图
    }

    /***
     * 温度设置界面的显示与隐藏
     */
    private void updateTempView(int chooicePosition) {
        if (chooicePosition == 0) {
            lin_serial.setVisibility(View.GONE);
            lin_default.setVisibility(View.GONE);
            return;
        }
        if (chooicePosition == TempModelEntity.IIC_MLX_90621_BAB ||
                chooicePosition == TempModelEntity.IIC_MLX_90640_0AA1547511 ||
                chooicePosition == TempModelEntity.IIC_MLX_90621_BAA ||
                chooicePosition == TempModelEntity.IIC_MLX_90614 ||
                chooicePosition == TempModelEntity.IIC_MLX_90641 ||
                chooicePosition == TempModelEntity.IIC_OTPA_16 ||
                chooicePosition == TempModelEntity.IIC_HPTA_32
        ) {
            lin_serial.setVisibility(View.GONE);
            lin_default.setVisibility(View.VISIBLE);
            return;
        }
        lin_serial.setVisibility(View.VISIBLE);
        lin_default.setVisibility(View.VISIBLE);
    }

    private void showTempDevDialog() {
//        List<BeanDefaultEntity> beanDefaultEntityList = TempModelUtil.getDevTTYSList();
        RadioListDialog radioListDialog = new RadioListDialog(this);
        List<RedioEntity> listShow = new ArrayList<RedioEntity>();
        for (String devPath : mDevicePaths) {
            listShow.add(new RedioEntity(devPath));
        }
//        for (BeanDefaultEntity beanDefaultEntity : beanDefaultEntityList) {
//            listShow.add(new RedioEntity(beanDefaultEntity.getDefaultDesc()));
//        }
        int selected = mDevicePaths.indexOf(SpUtils.getTempDevPath());
        String showDesc = getString(R.string.temp_type_dev);
        radioListDialog.show(
                getString(R.string.serial_port_selection),
                listShow,
                selected,
                showDesc
        );
        radioListDialog.setRadioChooiceListener((redioEntity, chooicePosition) -> {
            SpUtils.setTempDevPath(mDevicePaths.get(chooicePosition));
//            SpUtils.setmTempDev(chooicePosition);
            initData();
        });
    }

    private void showTemoRareDialog() {
        RadioListDialog radioListDialog = new RadioListDialog(this);
        List<BeanDefaultEntity> entityList = TempModelUtil.getmTempRareList();
        List<RedioEntity> listShow = new ArrayList<RedioEntity>();
        for (BeanDefaultEntity beanDefaultEntity : entityList) {
            listShow.add(new RedioEntity(beanDefaultEntity.getDefaultDesc()));
        }
        String showDesc = getString(R.string.temp_type_dev);
        radioListDialog.show(
                getString(R.string.baud),
                listShow,
                SpUtils.getmTempRare(),
                showDesc
        );
        radioListDialog.setRadioChooiceListener(new RadioChooiceListener() {
            @Override
            public void backChooiceInfo(RedioEntity redioEntity, int chooicePosition) {
                SpUtils.setmTempRare(chooicePosition);
                initData();
            }
        });
    }

    private void showTempLowDialog() {
        EditTextDialog editTextDialog = new EditTextDialog(this);
        editTextDialog.setOnDialogClickListener(new EditTextDialog.EditTextDialogListener() {
            @Override
            public void commit(String content) {
                if (content == null || content.length() < 2) {
                    showToast(getString(R.string.set_failed));
                    return;
                }
                float floatNum = 30.0f;
                try {
                    floatNum = Float.parseFloat(content);
                    SpUtils.setmTempLow(floatNum);
                    showToast(getString(R.string.set_success));
                    initData();
                } catch (Exception e) {
                    showToast(getString(R.string.set_failed));
                    e.printStackTrace();
                }
            }

            @Override
            public void hiddleTestClick() {

            }
        });
        String lowTemp = SpUtils.getmTempLow() + "";
        editTextDialog.show(
                getString(R.string.tv_temp_low),
                lowTemp,
                getString(R.string.submit),
                "");
    }

    private void showChhoiceTempType() {
        List<TempModelEntity> tempTypeList = TempModelUtil.getTempListByUser(TempSettingsActivity.this);
        RadioListDialog radioListType = new RadioListDialog(this);
        List<RedioEntity> listShow = new ArrayList<RedioEntity>();
        int checkIndex = 0;
        for (int i = 0; i < tempTypeList.size(); i++) {
            listShow.add(new RedioEntity(tempTypeList.get(i).getTempName()));
            if (tempTypeList.get(i).getTempTyle() == SpUtils.getmTempType())
                checkIndex = i;
        }
        String showDesc = getString(R.string.temp_type_dev);
        radioListType.show(
                getString(R.string.hardware_type),
                listShow,
                checkIndex,
                showDesc);
        radioListType.setRadioChooiceListener(new RadioChooiceListener() {
            @Override
            public void backChooiceInfo(RedioEntity redioEntity, int chooicePosition) {
                TempModelEntity tempModelEntity = tempTypeList.get(chooicePosition);
                SpUtils.setmTempType(tempModelEntity.getTempTyle());
                initData();
                if (SpUtils.getmTempType() == TempModelEntity.CLOSE) {
                    TempManager.getInstance().destory();
                }
                updateTempView(tempModelEntity.getTempTyle());
            }
        });
    }

    private void showTempTimeDialog() {
        EditTextDialog editTextDialog = new EditTextDialog(this);
        editTextDialog.setOnDialogClickListener(new EditTextDialog.EditTextDialogListener() {
            @Override
            public void commit(String content) {
                if (content == null || content.length() < 1) {
                    showToast(getString(R.string.temp_time_set));
                    return;
                }
                try {
                    float floatNum = Float.parseFloat(content);
                    if (floatNum > 5) {
                        showToast(getString(R.string.temp_time_set));
                        return;
                    }
                    if (floatNum < 0) {
                        showToast(getString(R.string.input_succ_info));
                        return;
                    }
                    SpUtils.setmTempTime(floatNum);
                    showToast(getString(R.string.set_success));
                    initData();
                } catch (Exception e) {
                    showToast(getString(R.string.set_failed));
                    e.printStackTrace();
                }
            }

            @Override
            public void hiddleTestClick() {

            }
        });
        String showContent = SpUtils.getmTempTime() + "";
        editTextDialog.show("", showContent, getString(R.string.confirm), "0~5");
    }

    private void showChooiceTempUnitDialog() {
        RadioListDialog radioListDialogTemp = new RadioListDialog(this);
        List<RedioEntity> listShowTemp = new ArrayList<RedioEntity>();
        listShowTemp.add(new RedioEntity(getString(R.string.centigrade)));
        listShowTemp.add(new RedioEntity(getString(R.string.fahrenhite)));
        radioListDialogTemp.show(
                getString(R.string.tv_temp_unit),
                listShowTemp,
                SpUtils.getTempUnit(),
                ""
        );
        radioListDialogTemp.setRadioChooiceListener(new RadioChooiceListener() {
            @Override
            public void backChooiceInfo(RedioEntity redioEntity, int chooicePosition) {
                SpUtils.setTempUnit(chooicePosition);
                changeWearHeightWear();
                showToast(getString(R.string.set_success));
                initData();
            }
        });
    }

    private void changeWearHeightWear() {
        float wearHeightTemp = SpUtils.getTempHeightWearNew();
        float modifyWearHeightTemp = wearHeightTemp;
        int tempType = SpUtils.getTempUnit();
        if (tempType == 0) {
            //摄氏度
            if (wearHeightTemp > 50) {
                modifyWearHeightTemp = (wearHeightTemp - 32) / 1.8f;
//                temp = temp * 1.8f + 32
            }
        } else if (tempType == 1) {
            //华式摄氏度
            if (wearHeightTemp < 50) {
                modifyWearHeightTemp = wearHeightTemp * 1.8f + 32;
            }
        }
        SpUtils.setTempHeightWearNew(modifyWearHeightTemp);
    }

}
