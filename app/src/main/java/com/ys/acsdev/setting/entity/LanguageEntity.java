package com.ys.acsdev.setting.entity;

public class LanguageEntity {

    int countryImage;
    String name;
    String languageType;
    public static final String LANGUAGE_TYPE_CHAINA = "zh";
    public static final String LANGUAGE_TYPE_ENGLISH = "en";
    public static final String LANGUAGE_TYPE_ITALIAN = "ita";

    public LanguageEntity(int countryImage, String name, String languageType) {
        this.countryImage = countryImage;
        this.name = name;
        this.languageType = languageType;
    }

    public String getLanguageType() {
        return languageType;
    }

    public void setLanguageType(String languageType) {
        this.languageType = languageType;
    }

    public int getCountryImage() {
        return countryImage;
    }

    public void setCountryImage(int countryImage) {
        this.countryImage = countryImage;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    @Override
    public String toString() {
        return "LanguageEntity{" +
                "countryImage=" + countryImage +
                ", name='" + name + '\'' +
                '}';
    }
}
