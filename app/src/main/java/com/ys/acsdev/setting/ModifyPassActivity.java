package com.ys.acsdev.setting;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.ys.acsdev.R;
import com.ys.acsdev.ui.BaseActivity;
import com.ys.acsdev.util.SpUtils;

public class ModifyPassActivity extends BaseActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modify_password);
        initView();
    }

    ImageView mIvBack;
    EditText et_old_pass,et_new_pass,et_new_pass_again;
    Button btn_submit;
    private void initView() {
          mIvBack = (ImageView)findViewById(R.id.mIvBack);
        mIvBack.setOnClickListener(this);
        et_old_pass = (EditText)findViewById(R.id.et_old_pass);
        et_new_pass = (EditText)findViewById(R.id.et_new_pass);
        et_new_pass_again = (EditText)findViewById(R.id.et_new_pass_again);
        btn_submit = (Button)findViewById(R.id.btn_submit);
        btn_submit.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_submit:
              boolean isJujleRigh = jujleEditPassword();
              if (isJujleRigh){
                  String newPassword = getNewPassword();
                  SpUtils.setSettingPassword(newPassword );
                  showToast(getString(R.string.save_success));
                  finish();
              }
                break;
            case R.id.mIvBack:
                finish();
                break;
        }
    }

    /**
     * 判断输入的数据是否正常
     * @return
     */
    private boolean jujleEditPassword() {
        boolean backStaues = false;
        String oldPassWordSave = SpUtils.getSettingPassword();
        String oldPass = getOldPassword();
        if (oldPass==null||oldPass.length()<2){
            showToast(getString(R.string.input_old_pwd));
            return false;
        }
        String newPasword = getNewPassword();
        String newPassAgain = getNewAgainPassword();
        if (newPasword==null||newPasword.length()<2){
            showToast(getString(R.string.input_new_pwd));
            return false;
        }
        if (newPassAgain==null||newPassAgain.length()<2){
            showToast(getString(R.string.confirm_new_pwd));
            return false;
        }
        if (!oldPass.contains(oldPassWordSave)){
            showToast(getString(R.string.old_pwd_failed));
            return false;
        }
        if (newPasword.equals(newPassAgain)){
            return true;
        }
        showToast(getString(R.string.pwd_not_match));
        return false;
    }


    private String getOldPassword(){
        return et_old_pass.getText().toString().trim();
    }

    private String getNewPassword(){
        return et_new_pass.getText().toString().trim();
    }

    private String getNewAgainPassword(){
        return et_new_pass_again.getText().toString().trim();
    }
}
