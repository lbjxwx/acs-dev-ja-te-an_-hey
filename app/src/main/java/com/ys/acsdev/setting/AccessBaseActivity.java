package com.ys.acsdev.setting;

import android.os.Bundle;

import com.ys.acsdev.R;
import com.ys.acsdev.bean.LocalARBean;
import com.ys.acsdev.commond.AppInfo;
import com.ys.acsdev.commond.CommondCallBack;
import com.ys.acsdev.data.MySDCard;
import com.ys.acsdev.runnable.FileWriteRunnable;
import com.ys.acsdev.service.AcsService;
import com.ys.acsdev.ui.BaseActivity;
import com.ys.acsdev.util.FileUtils;
import com.ys.acsdev.util.LogUtil;
import com.ys.acsdev.util.file.ExportFileUtil;
import com.ys.acsdev.util.guardian.WriteSdListener;
import com.ys.acsdev.view.dialog.OridinryDialog;
import com.ys.acsdev.view.dialog.OridinryDialogClick;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/***
 * 通行记录，用来操作导出通行记录得
 */
public class AccessBaseActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        checkUsbPath();
    }

    /***
     * 检查U盘路径
     */
    private void checkUsbPath() {
        MySDCard mySDCard = new MySDCard(AccessBaseActivity.this);
        List<String> usbList = mySDCard.getUsbPath();
        if (usbList == null || usbList.size() < 1) {
            AppInfo.U_PAN_PATH_CACHE = "";
            return;
        }
        AppInfo.U_PAN_PATH_CACHE = usbList.get(0).toString();
    }

    public boolean checkUsbBindBord() {
        exportPath = AppInfo.U_PAN_PATH_CACHE;
        if (exportPath == null || exportPath.length() < 3) {
            showToast(getString(R.string.no_usb_insert));
            return false;
        }
        exportPath = exportPath + "/passRecord";
        File file = new File(exportPath);
        if (!file.exists()) {
            boolean isCreate = file.mkdir();
            LogUtil.export("===Upan文件夹创建状态==" + isCreate);
        }
        return true;
    }

    ExportFileUtil exportFileUtil;
    String exportPath = "";

    /***
     * 导出记录到本地
     * 先把文件导出到U盘
     */
    public void exportAccessToLocal(List<LocalARBean> list) {
        File file = new File(exportPath);
        if (!file.exists()) {
            showToast(getString(R.string.file_not_exist));
            return;
        }
        showWait(getString(R.string.loding_eaport));
        FileUtils.deleteDirOrFile(AppInfo.PASS_RECORD_PATH, "导出，先清理一次上一次的记录");
        if (exportFileUtil == null) {
            exportFileUtil = new ExportFileUtil(AccessBaseActivity.this);
        }
        String saveAllPath = exportPath + "/passRecord.xls";
        exportFileUtil.writExcelSd(true, list, saveAllPath, new CommondCallBack() {

            @Override
            public void onCommondCallBack(int code, String msg) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        showToast(msg);
                        hideWait();
                        if (code == 0) {  //写入成功
                            writeFileToUPan();
                        }
                    }
                });
            }
        });
    }

    /***
     * 再把文件拷贝到U盘目录下面
     */
    private void writeFileToUPan() {
//        String filePath = AppInfo.BASE_PATH_CACHE;
//        File file = new File(filePath);
//        if (!file.exists()) {
//            LogUtil.export("====writeFileToUPan===文件不存在");
//            showToast(getString(R.string.file_not_exist));
//            return;
//        }
//        GetFileFromPathForRunnable runnable = new GetFileFromPathForRunnable(filePath, new GetFileFromPathForRunnable.QueryFileFromPathListener() {
//            @Override
//            public void backFileList(boolean isSuccess, List<File> listFileSearch, String errorDesc) {
//                if (!isSuccess) {
//                    showToast(getString(R.string.file_not_exist));
//                    LogUtil.export("====writeFileToUPan===获取文件失败：" + errorDesc);
//                    return;
//                }
//                if (listFileSearch == null || listFileSearch.size() < 1) {
//                    showToast(getString(R.string.file_not_exist));
//                    LogUtil.export("====writeFileToUPan===listFileSearch==0 ");
//                    return;
//                }
//                fileListWrite = listFileSearch;
//                fileAllLength = fileListWrite.size();
//                showUpdateDialog();
//                writeFileToUPanOneByOne(true);
//            }
//        });
//        AcsService.getInstance().executor(runnable);
    }


    private void updateDialogContent(int lastLength, int AllLength) {
        if (oridinryDialog != null) {
            oridinryDialog.updateShowContent(getString(R.string.last_num) + ": " + lastLength +
                    "\n" + getString(R.string.lall_num) + ": " + AllLength);
        }
    }

    int fileAllLength = 0;
    List<File> fileListWrite = new ArrayList<File>();

    private void writeNextFile(boolean isWriteSuccess) {
        if (fileListWrite == null || fileListWrite.size() < 1) {
            writeFileOver();
            return;
        }
        fileListWrite.remove(0);
        writeFileToUPanOneByOne(isWriteSuccess);
    }

    FileWriteRunnable fileWriteRunnable;

    private void writeFileToUPanOneByOne(boolean isWriteSuccess) {
        if (exportPath == null || exportPath.length() < 5) {
            showToast(getString(R.string.u_path_check_error));
            LogUtil.export("====writeFileToUPan====usbPath=null=");
            return;
        }
        LogUtil.cdl("====writeFileToUPan====usbPath==" + exportPath);
        try {
            File fileUpan = new File(exportPath);
            if (!fileUpan.exists()) {
                showToast(getString(R.string.file_not_exist));
                LogUtil.export("====writeFileToUPan====U盘文件不存在");
                return;
            }
            if (fileListWrite == null || fileListWrite.size() < 1) {
                writeFileOver();
                return;
            }
            LogUtil.export("====writeFileToUPan====写入情况==剩余/总数==" + fileListWrite.size() + " / " + fileAllLength + " / " + isWriteSuccess);
            updateDialogContent(fileListWrite.size(), fileAllLength);
            File file = fileListWrite.get(0);
            String filePath = file.getPath();
            String fileName = file.getName();
            String saveAllPath = exportPath + "/" + fileName;
            LogUtil.export("====writeFileToUPan===saveAllPath==" + filePath + " / " + saveAllPath);
            if (fileWriteRunnable == null) {
                fileWriteRunnable = new FileWriteRunnable();
            }
            fileWriteRunnable.setWriteFileInfo(filePath, saveAllPath, new WriteSdListener() {
                @Override
                public void writeProgress(int progress) {

                }

                @Override
                public void writeSuccess(String savePath) {
                    FileUtils.deleteDirOrFile(filePath, "文件导出，导出成功，删除文件");
                    writeNextFile(true);
                }

                @Override
                public void writrFailed(String errorrDesc) {
                    writeNextFile(false);
                }
            });
            AcsService.getInstance().executor(fileWriteRunnable);
        } catch (Exception e) {
            LogUtil.export("====writeFileToUPan====写入异常==" + e.toString());
            e.printStackTrace();
        }
    }

    private void writeFileOver() {
        if (fileListWrite != null && fileListWrite.size() > 0) {
            fileListWrite.clear();
        }
        if (oridinryDialog != null) {
            oridinryDialog.dissmiss();
        }
        showToast(getString(R.string.write_compant));
        LogUtil.export("====writeFileToUPan===writeFileOver==");
    }


    OridinryDialog oridinryDialog;

    private void showUpdateDialog() {
        if (oridinryDialog == null) {
            oridinryDialog = new OridinryDialog(AccessBaseActivity.this);
        }
        oridinryDialog.show("", getString(R.string.cacel), getString(R.string.cacel));
        updateDialogContent(fileAllLength, fileAllLength);
        oridinryDialog.setOnDialogClickListener(new OridinryDialogClick() {
            @Override
            public void sure() {
                writeFileOver();
            }

            @Override
            public void noSure() {
                writeFileOver();
            }
        });
    }


}
