package com.ys.acsdev.setting;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.ys.acsdev.R;
import com.ys.acsdev.commond.AppInfo;
import com.ys.acsdev.listener.DownApkStatesListener;
import com.ys.acsdev.runnable.down.DownFileEntity;
import com.ys.acsdev.service.AcsService;
import com.ys.acsdev.service.model.AcsModel;
import com.ys.acsdev.service.model.AcsModelmpl;
import com.ys.acsdev.ui.BaseActivity;
import com.ys.acsdev.util.APKUtil;
import com.ys.acsdev.util.LogUtil;


public class AppUpdateActivity extends BaseActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_app_update);
        initView();
    }

    ImageView mIvBack;
    TextView tv_statues;
    ProgressBar pro_down;
    Button btn_check_app;

    private void initView() {
        mIvBack = (ImageView) findViewById(R.id.mIvBack);
        mIvBack.setOnClickListener(this);
        btn_check_app = (Button) findViewById(R.id.btn_check_app);
        btn_check_app.setOnClickListener(this);
        tv_statues = (TextView) findViewById(R.id.tv_statues);
        pro_down = (ProgressBar) findViewById(R.id.pro_down);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_check_app:
                checkAppStatues();
                break;
            case R.id.mIvBack:
                finish();
                break;
        }
    }

    AcsModel acsModel;

    private void checkAppStatues() {
        AcsService.getInstance().stopDownApkFile();
        if (acsModel == null) {
            acsModel = new AcsModelmpl();
        }
        acsModel.checkAppUpdate(AppUpdateActivity.this, new DownApkStatesListener() {

            @Override
            public void requestBack(String requestDesc) {
                tv_statues.setText(requestDesc + "");
            }

            @Override
            public void downStateInfo(DownFileEntity entity) {
                if (entity == null) {
                    return;
                }
                int downStatues = entity.getDownState();
                int downProgress = entity.getProgress();
                int downSpeed = entity.getDownSpeed();
                String desc = entity.getDesc();
                pro_down.setProgress(downProgress);
                LogUtil.update("下载升级文件:" + downStatues + " / " + downProgress + " /speed=" + downSpeed, true);
                if (downStatues == DownFileEntity.DOWN_STATE_SUCCESS) {
                    tv_statues.setText("Down Success");
                } else if (downStatues == DownFileEntity.DOWN_STATE_FAIED) {
                    tv_statues.setText("Down Failed: " + desc);
                } else if (downStatues == DownFileEntity.DOWN_STATE_START) {
                    tv_statues.setText("Down Start: " + desc);
                } else if (downStatues == DownFileEntity.DOWN_STATE_PROGRESS) {
                    tv_statues.setText(downProgress + "% / " + downSpeed + " KB");
                }
            }
        });
    }

}
