package com.ys.acsdev.setting;

import android.app.Activity
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.view.WindowManager
import com.huiming.huimingstore.ext.setVisible
import com.ys.acsdev.R
import com.ys.acsdev.commond.AppInfo
import com.ys.acsdev.util.APKUtil
import com.ys.acsdev.util.ActivityCollector
import com.ys.acsdev.util.LogUtil
import com.ys.acsdev.util.SpUtils
import com.ys.acsdev.view.MyToastView
import com.ys.facepasslib.BaseFaceActivity
import com.ys.facepasslib.FaceManager
import com.ys.facepasslib.camera.CameraPreview
import com.ys.facepasslib.view.FaceCircleViewNew
import kotlinx.android.synthetic.main.activity_camera.*
import mcv.facepass.types.FacePassImage
import java.io.File

class CameraPhotoActivity : BaseFaceActivity() {

    val receiver = object : BroadcastReceiver() {
        override fun onReceive(contxt: Context?, intent: Intent?) {

            when (intent?.action) {
                AppInfo.SEND_IMAGE_CAPTURE_SUCCESS -> {
                    showBtnAndBackView(true)
                    var file = File(AppInfo.CAPTURE_IMAGE_PATH)
                    if (!file.exists()) {
                        showToastView(getString(R.string.capture_failed))
                        return
                    }
                    //数据是使用Intent返回
                    //数据是使用Intent返回
                    val intentBack = Intent()
                    setResult(Activity.RESULT_OK, intentBack)
                    finish()
                }
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        setContentView(R.layout.activity_camera)
        initView()
        initReceiver();
        initFaceConfig()
        //用来关闭所有界面的，这里不要注释
        ActivityCollector.addActivity(this)
    }

    var handler: Handler = Handler()

    private fun initReceiver() {
        var intentFilter = IntentFilter();
        intentFilter.addAction(AppInfo.SEND_IMAGE_CAPTURE_SUCCESS);
        registerReceiver(receiver, intentFilter)
    }

    override fun onStart() {
        super.onStart()
        //控制人脸框是否镜像
        mFaceRectMir = SpUtils.getmFaceRect()
        mMaskEnable = SpUtils.getmMaskEnable()
        resetFace()
    }

    override fun onStop() {
        mHasPerson = false
        super.onStop()
    }

    override fun onDestroy() {
        super.onDestroy()
        ActivityCollector.removeActivity(this)
        if (receiver != null) {
            unregisterReceiver(receiver)
        }
    }

    fun initView() {
        btn_start_temp.setOnClickListener(object : View.OnClickListener {
            override fun onClick(p0: View?) {
                captureImage();
            }
        })
        mIvBack.setOnClickListener(object : View.OnClickListener {
            override fun onClick(p0: View?) {
                finish()
            }
        })
    }

    private fun captureImage() {
        var guardianName = "com.guardian";
        var isInstall = APKUtil.ApkState(this, guardianName);
        if (!isInstall) {
            showToastView(getString(R.string.guardian_no_install))
            return
        }
        showBtnAndBackView(false);
        handler.postDelayed(object : Runnable {
            override fun run() {
                val intent = Intent()
                val screenWidth: Int = SpUtils.getScreenwidth()
                val screenHeight: Int = SpUtils.getScreenheight()
                intent.putExtra("screenWidth", screenWidth)
                intent.putExtra("screenHeight", screenHeight)
                intent.putExtra("tag", AppInfo.TAG_UPDATE)
                intent.action = AppInfo.CAPTURE_IMAGE_RECEIVE
                sendBroadcast(intent)
            }
        }, 300);
    }

    private fun showBtnAndBackView(showStues: Boolean) {
        btn_start_temp.setVisible(showStues)
        mIvBack.setVisible(showStues)
    }

    private fun showToastView(desc: String) {
        MyToastView.getInstance().Toast(this, desc);
    }

    fun initFaceConfig() {
        mPrewRotation = when (SpUtils.getPrewRotate()) {
            0 -> 0
            1 -> 90
            2 -> 180
            else -> 270
        }

        mRectRotation = when (SpUtils.getRectRotate()) {
            1 -> 0
            2 -> 90
            3 -> 180
            4 -> 270
            else -> FaceManager.instance.mFaceRotation
        }
        mIsFront = SpUtils.getCameraFacing()
        mFrv.setScreenWidth(0, 0, SpUtils.getShowFaceRectView())
    }

    override fun onHasFaceChange(hasFace: Boolean) {
        LogUtil.e("=======onHasFaceChange: $hasFace")
    }

    override fun getCameraView(): CameraPreview = mCv

    override fun getFaceRectView(): FaceCircleViewNew = mFrv
    override fun recognizeResult(faceToken: String, noMask: Boolean, image: FacePassImage?) {
        LogUtil.e("========recognizeResult： $faceToken")
    }


    override fun cameraPreview(nv21: kotlin.ByteArray, width: Int, height: Int, rotation: Int) {
    }
}