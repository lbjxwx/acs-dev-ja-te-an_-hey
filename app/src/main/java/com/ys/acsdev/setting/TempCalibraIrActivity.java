package com.ys.acsdev.setting;

import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProviders;

import com.ys.acsdev.BuildConfig;
import com.ys.acsdev.R;
import com.ys.acsdev.api.AppConfig;
import com.ys.acsdev.db.DbTempCompensate;
import com.ys.acsdev.manager.TempManager;
import com.ys.acsdev.manager.TtsManager;
import com.ys.acsdev.service.AcsService;
import com.ys.acsdev.setting.entity.TempAddEntity;
import com.ys.acsdev.setting.entity.TempModelEntity;
import com.ys.acsdev.ui.face.FaceViewModel;
import com.ys.acsdev.util.ActivityCollector;
import com.ys.acsdev.util.Biantai;
import com.ys.acsdev.util.CameraProvider;
import com.ys.acsdev.util.LogUtil;
import com.ys.acsdev.util.SimpleDateUtil;
import com.ys.acsdev.util.SpUtils;
import com.ys.acsdev.util.ViewSizeChange;
import com.ys.acsdev.view.MyToastView;
import com.ys.facepasslib.BaseLivenessActivity;
import com.ys.facepasslib.FaceManager;
import com.ys.facepasslib.camera.CameraPreview;
import com.ys.facepasslib.view.FaceCircleViewNew;
import com.ys.temperaturelib.temperature.TemperatureEntity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import mcv.facepass.types.FacePassImage;

public class TempCalibraIrActivity extends BaseLivenessActivity implements View.OnClickListener {

    private static final String TAG_RESET_TIP = "TAG_RESET_TIP";
    private static final String TAG_RESET_FACE = "TAG_RESET_FACE";
    FaceViewModel mVM;
    //0: 校准温度补偿 1:模块自动校准
    int mType = 0;

    private void initOther() {
        if (mVM == null) {
            mVM = ViewModelProviders.of(this).get(FaceViewModel.class);
        }
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_doorway_temp);
        initOther();
        initView();
        initFaceConfig();
        //用来关闭所有界面的，这里不要注释
        ActivityCollector.addActivity(this);
    }

    CameraPreview mCv;
    FaceCircleViewNew mFrv;
    TextView mTvTemp, mTvInfo;
    CameraPreview mIRCv;
    ImageView mIvBack;
    Button btn_start_temp;
    boolean isCheckTemp = false;
    TextView tv_temp_show, tv_info;

    String startHourMin;
    String endHourMin;
    String mTargetTemp;

    private void initView() {
        writeDistanceInfoToLocal("\n\n\n\n");
        Intent intent = getIntent();
        mType = intent.getIntExtra("type", 0);
        mTargetTemp = intent.getStringExtra("targetNum");
        if (mType == 0) {
            startHourMin = intent.getStringExtra("startHourMin");
            endHourMin = intent.getStringExtra("endHourMin");
        }
        btn_start_temp = (Button) findViewById(R.id.btn_start_temp);
        btn_start_temp.setOnClickListener(this);
        mIvBack = (ImageView) findViewById(R.id.mIvBack);
        mIvBack.setOnClickListener(this);
        mIRCv = (CameraPreview) findViewById(R.id.mIRCv);
        tv_temp_show = (TextView) findViewById(R.id.tv_temp_show);
        mTvInfo = (TextView) findViewById(R.id.mTvInfo);
        mTvTemp = (TextView) findViewById(R.id.mTvTemp);
        mCv = (CameraPreview) findViewById(R.id.mCv);
        ViewSizeChange.setCameraPassFaceSize(mCv);
        mFrv = (FaceCircleViewNew) findViewById(R.id.mFrv);
        tv_info = findViewById(R.id.tv_info);
        if (mType == 0) {
            mFrv.setFaceRectDisListener((distanceNum, isCenter, rectWidth, headPoint) -> {
                TempManager.getInstance().mRectDistance = (int) distanceNum;
                TempManager.getInstance().headPoint = headPoint;
                String showDesc = "";
                if (distanceNum > 60) {
                    showDesc = getString(R.string.temp_check_near);
                    updateShowTips(showDesc);
                    startToSpeak(showDesc, "温度检测，请靠近");
                    cacelTempTime();
                    return;
                }
                if (!isCenter) {
                    showDesc = getString(R.string.temperature_zone);
                    updateShowTips(showDesc);
                    startToSpeak(showDesc, getString(R.string.temperature_zone));
                    cacelTempTime();
                    return;
                }
                if (isCheckTemp) {
                } else {
                    updateShowTips(getString(R.string.click_start_button));
                }
            });
        }
        if (BuildConfig.DEBUG) {
            tv_info.setVisibility(View.VISIBLE);
        }
    }

    private void startToSpeak(String speakDesc, String tag) {
        boolean isSpeak = TtsManager.getInstance().isSpeaking();
        if (isSpeak) {
            return;
        }
        LogUtil.cdl("====当前状态====" + isSpeak);
        AcsService.getInstance().startSpeakTts(speakDesc, tag);
    }


    private void updateShowTips(String showDesc) {
        mTvInfo.setText(showDesc);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_start_temp:
                startTempCheckTimer();
                break;
            case R.id.mIvBack:
                finish();
                break;
        }
    }

    private void writeDistanceInfoToLocal(String desc) {
        LogUtil.saveLogToLocalSingle(desc);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mVM.setMProcessing(false);
        initTemp();
        setMFaceRectMir(SpUtils.getmFaceRect());
        mVM.setMTempTime(SpUtils.getmTempTime());
        mVM.setMTempEnable(SpUtils.getmTempType() != TempModelEntity.CLOSE);
        resetTip();
        resetFace();
    }

    @Override
    protected void onStop() {
        super.onStop();
        try {
            stopLazy(TAG_RESET_TIP);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initTemp() {
        TempManager.getInstance().setTempCallBack(new TempManager.TempCallBack() {
            @Override
            public void onTempResult(TemperatureEntity entity, int rectDistance) {
                if (BuildConfig.DEBUG) {
                    Message msg = Message.obtain();
                    msg.what = TEMP_INFO;
                    msg.obj = "Temp ：" + entity.source + "\nDistance：" + rectDistance + "\n补偿值: " + (entity.temperatue - entity.source);
                    mHandler.sendMessage(msg);
                }
                LogUtil.temperature("=========界面返回得文图===" + entity.temperatue + " / " + entity.ta);
                if (isCheckTemp) {
                    if (mType != 0)
                        return;
                    if (SpUtils.getTempUnit() == 1) {
                        float showTempNum = (entity.temperatue * 1.8f) + 32;
                        floatList.add(showTempNum);
                    } else {
                        floatList.add(entity.temperatue);
                    }
                }
                runOnUiThread(() -> {
//                        temp = temp * 1.8f + 32
                    if (SpUtils.getTempUnit() == 1) {
                        float showTempNum = (entity.temperatue * 1.8f) + 32;
                        tv_temp_show.setText(showTempNum + "");
                    } else {
                        tv_temp_show.setText(entity.temperatue + "");
                    }
                });
            }

            @Override
            public void onCalibrationComplete() {
                isCheckTemp = false;
                runOnUiThread(() -> updateShowTips(getString(R.string.calibra_complete)));
            }
        });
    }

    private void resetTip() {
        stopLazy(TAG_RESET_FACE);
        mVM.setMProcessing(false);
        resetFace();
    }

    private void stopLazy(String tag) {
        if (mVM != null) {
            mVM.stopDelayed(tag);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            if (mVM != null) {
                mVM.destory();
            }
            cacelTempTime();
            ActivityCollector.removeActivity(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //==============不涉及到业务逻辑的代码===========================================================================================================

    private static final int TIME_REDUICE_DEFAULT = 10;
    private int time_reduice_ten = TIME_REDUICE_DEFAULT;
    private List<Float> floatList = new ArrayList<Float>();

    private void startTempCheckTimer() {
        isCheckTemp = true;
        if (mType != 0) {
            float floatTempTar = 36.5f;
            try {
                floatTempTar = Float.parseFloat(mTargetTemp);
            } catch (Exception e) {
                e.printStackTrace();
            }
            TempManager.getInstance().calibration(floatTempTar);
            updateShowTips(getString(R.string.wait_calibration));
            tv_temp_show.setText("");
            return;
        }

        btn_start_temp.setVisibility(View.GONE);
        time_reduice_ten = TIME_REDUICE_DEFAULT;
        floatList.clear();
        new Thread() {
            @Override
            public void run() {
                super.run();
                while (isCheckTemp) {
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    mHandler.sendEmptyMessage(TIME_REDUICE);
                }
            }
        }.start();
    }

    private static final int TIME_REDUICE = 564;
    private static final int TEMP_INFO = 102;

    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            switch (msg.what) {
                case TEMP_INFO:
                    tv_info.setText((String) msg.obj);
                    break;
                case TIME_REDUICE:
                    time_reduice_ten--;
                    LogUtil.cdl("=========倒计时====" + time_reduice_ten);
                    if (time_reduice_ten < 1) {
                        checkOverTemp();
                        return;
                    }
                    updateShowTips(getString(R.string.last_time_rediuce) + time_reduice_ten);
                    break;
            }
        }
    };

    private void checkOverTemp() {
        if (Biantai.isTwoClick()) {
            return;
        }
        cacelTempTime();
        updateShowTips("Success");
        if (floatList == null || floatList.size() < 5) {
            showToastView(getString(R.string.get_temp_error));
            startToSpeak(getString(R.string.get_temp_error), "获取测温数据失败");
            return;
        }
        float max1 = Collections.max(floatList);
        floatList.remove(max1);
        float max2 = Collections.max(floatList);
        floatList.remove(max2);
        float min1 = Collections.min(floatList);
        floatList.remove(min1);
        float min2 = Collections.min(floatList);
        floatList.remove(min2);
        float sumTemp = 0;
        for (float tempShow : floatList) {
            sumTemp += tempShow;
        }
        float averageNum = sumTemp / floatList.size();
        float floatTempTar = 0;
        try {
            floatTempTar = Float.parseFloat(mTargetTemp);
        } catch (Exception e) {
            e.printStackTrace();
        }
//        float tempCompCache = averageNum - floatTempTar;
        float tempCompCache = floatTempTar - averageNum;
        LogUtil.cdl("=====checkOverTemp==000===" + averageNum + " /设定得温度== " + floatTempTar);

        //保留小数点后两位
        float tempCompSave = (float) (Math.round(tempCompCache * 100)) / 100;

//        LogUtil.cdl("=====checkOverTemp==3333===" + tempCompSave);
        String saveId = SimpleDateUtil.getCurrentTimelONG();
        TempAddEntity tempAddEntity = new TempAddEntity(startHourMin, endHourMin, tempCompSave, saveId);
        DbTempCompensate.addTempToLocal(tempAddEntity);
        showToastView(getString(R.string.calibra_complete));
        startToSpeak(getString(R.string.calibra_complete), "校准完成");
        finish();
    }


    /***
     * 取消测温
     */
    private void cacelTempTime() {
        isCheckTemp = false;
        btn_start_temp.setVisibility(View.VISIBLE);
    }


    public void showToastView(String message) {
        MyToastView.getInstance().Toast(this, message);
    }

    private void initFaceConfig() {
        int cameraCount = CameraProvider.getCamerasCount(this);
        LogUtil.e("========获取相机个数: " + cameraCount);
        int currentWidth = SpUtils.getResolutionWidth();
        int currentHeight = SpUtils.getResolutionHeight();
        if (currentWidth == 0 || currentHeight == 0) {
            setMCustomSize(null);
        } else {
            setMCustomSize(new Point(currentWidth, currentHeight));
        }
        //选择是单目还是双目
        setMIsDoubleCamera(SpUtils.getAutoSwitchCamera());
        int prewRoatation = 0;
        switch (SpUtils.getPrewRotate()) {
            case 0:
                prewRoatation = 0;
                break;
            case 1:
                prewRoatation = 90;
                break;
            case 2:
                prewRoatation = 180;
                break;
            default:
                prewRoatation = 270;
                break;
        }
        setMPrewRotation(prewRoatation);

        int rectRoatation = 0;
        switch (SpUtils.getRectRotate()) {
            case 1:
                rectRoatation = 0;
                break;
            case 2:
                rectRoatation = 90;
                break;
            case 3:
                rectRoatation = 180;
                break;
            case 4:
                rectRoatation = 270;
                break;
            default:
                rectRoatation = FaceManager.getInstance().getMFaceRotation();
                break;
        }
        setMRectRotation(rectRoatation);
        setMIsFront(SpUtils.getCameraFacing());
        mFrv.setScreenWidth(
                SpUtils.getScreenwidth(),
                SpUtils.getScreenheight(), SpUtils.getShowFaceRectView()
        );
    }


    @Override
    public CameraPreview getCameraView() {
        return mCv;
    }


    @Override
    public CameraPreview getIrCameraView() {
        return mIRCv;
    }


    @Override
    public FaceCircleViewNew getFaceRectView() {
        return mFrv;
    }

    /**
     * [faceToken] 人脸识别结果
     * [noMask] 未佩戴口罩为true,否则为false
     */
    @Override
    public void recognizeResult(String faceToken, boolean noMask, FacePassImage image) {
        LogUtil.e("========recognizeResult： $faceToken");
    }

    @Override
    public void onHasFaceChange(boolean hasFace) {
        if (!hasFace) {
            cacelTempTime();
        }
    }


    @Override
    public void cameraPreview(byte[] nv21Data, int width, int height, int rotation) {
    }
}
