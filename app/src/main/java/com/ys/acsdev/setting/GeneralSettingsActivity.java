package com.ys.acsdev.setting;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.ys.acsdev.R;
import com.ys.acsdev.bean.RedioEntity;
import com.ys.acsdev.listener.MoreButtonListener;
import com.ys.acsdev.listener.MoreButtonToggleListener;
import com.ys.acsdev.ui.BaseActivity;
import com.ys.acsdev.util.SpUtils;
import com.ys.acsdev.util.SystemManagerUtil;
import com.ys.acsdev.view.MoreButton;
import com.ys.acsdev.view.MoreButtonToggle;
import com.ys.acsdev.view.dialog.EditTextDialog;
import com.ys.acsdev.view.dialog.OridinryDialog;
import com.ys.acsdev.view.dialog.OridinryDialogClick;
import com.ys.acsdev.view.dialog.RadioListDialog;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class GeneralSettingsActivity extends BaseActivity implements MoreButtonToggleListener,
        View.OnClickListener, MoreButtonListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_general_settings);
        initView();
        updateDateInfo();
        initTtysList();
    }

    private void initTtysList() {
        File dev = new File("/dev");
        File[] files = dev.listFiles();
        int i;
        for (i = 0; i < files.length; i++) {
            if (files[i].getAbsolutePath().startsWith("/dev/tty")) {
                mDevicePaths.add(files[i].getAbsolutePath());
            }
        }
    }


    ArrayList<String> mDevicePaths = new ArrayList<>();
    MoreButtonToggle mBtnAttendEnable, mBtnReadCard, mBtnDoorEnable, btn_upload_face;
    private MoreButtonToggle face_info_no_save;
    Button btn_submit;
    MoreButton mBtnInfoEnable, btn_sleep_time, btn_close_door, more_btn_visitor_menu, more_btn_card_ttys, more_btn_card_type;


    @Override
    public void switchToggleView(View view, boolean isChooice) {
        switch (view.getId()) {
            case R.id.face_info_no_save:
                SpUtils.setSaveInfoToLocal(isChooice);
                updateDateInfo();
                break;
            case R.id.mBtnAttendEnable:
                SpUtils.setmAttendEnable(isChooice);
                updateDateInfo();
                break;
            case R.id.mBtnReadCard:
                SpUtils.setInputCardEnable(isChooice);
                updateDateInfo();
                break;
            case R.id.mBtnDoorEnable:
                SpUtils.setDoorEnable(isChooice);
                updateDateInfo();
                break;
            case R.id.btn_upload_face:
                SpUtils.setUploadFace(isChooice);
                updateDateInfo();
                break;
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_setting_default:
                //设置复位
                showBackSettingDialog();
                break;
            case R.id.mIvBack:
                onBackPressed();
                break;
            case R.id.btn_submit:
                showRebootAppDialog();
                break;
        }
    }

    private void showBackSettingDialog() {
        OridinryDialog oridinryDialog = new OridinryDialog(this);
        oridinryDialog.setOnDialogClickListener(new OridinryDialogClick() {
            @Override
            public void sure() {
                SpUtils.setSharedDefaultInfo();
                showRebootAppDialog();
            }

            @Override
            public void noSure() {

            }
        });
        oridinryDialog.show(
                getString(R.string.clear_shared_content),
                getString(R.string.submit),
                getString(R.string.cacel)
        );
    }

    ImageView mIvBack;
    Button btn_setting_default;

    @Override
    public void clickView(View view) {
        switch (view.getId()) {
            case R.id.more_btn_card_type:
                showCardTypeChooiceDialog();
                break;
            case R.id.more_btn_card_ttys:
                showCardTtysChooice();
                break;
            case R.id.more_btn_visitor_menu: //访客设置
                startActivity(new Intent(GeneralSettingsActivity.this, VisitorSettingsActivity.class));
                break;
            case R.id.mBtnInfoEnable:
                startActivity(new Intent(GeneralSettingsActivity.this, BottomShowSettingsActivity.class));
                break;
            case R.id.btn_sleep_time:
                showSleepTimeDialog();
                break;
            case R.id.btn_close_door:
                showDoorTimeDialog();
                break;
        }
    }

    /***
     * 刷卡型号
     */
    private void showCardTypeChooiceDialog() {
        RadioListDialog radioListDialog = new RadioListDialog(this);
        List<RedioEntity> listShow = new ArrayList<RedioEntity>();
        listShow.add(new RedioEntity("中控"));
        listShow.add(new RedioEntity("德科"));
        int selected = SpUtils.getCardComType();
        String showDesc = "";
        radioListDialog.show(
                "刷卡型号",
                listShow,
                selected,
                showDesc
        );
        radioListDialog.setRadioChooiceListener((redioEntity, chooicePosition) -> {
            SpUtils.setCardComType(chooicePosition);
            updateDateInfo();
        });

    }


    private void showCardTtysChooice() {
        RadioListDialog radioListDialog = new RadioListDialog(this);
        List<RedioEntity> listShow = new ArrayList<RedioEntity>();
        for (String devPath : mDevicePaths) {
            listShow.add(new RedioEntity(devPath));
        }
        int selected = mDevicePaths.indexOf(SpUtils.getCardTtysChooice());
        String showDesc = "";
        radioListDialog.show(
                getString(R.string.serial_port_selection),
                listShow,
                selected,
                showDesc
        );
        radioListDialog.setRadioChooiceListener((redioEntity, chooicePosition) -> {
            SpUtils.setCardTtysChooice(mDevicePaths.get(chooicePosition));
            updateDateInfo();
        });
    }

    private void initView() {
        more_btn_card_type = findViewById(R.id.more_btn_card_type);
        more_btn_card_type.setOnMoretListener(this);

        more_btn_card_ttys = findViewById(R.id.more_btn_card_ttys);
        more_btn_card_ttys.setOnMoretListener(this);
        more_btn_visitor_menu = findViewById(R.id.more_btn_visitor_menu);
        more_btn_visitor_menu.setOnMoretListener(this);
        btn_setting_default = (Button) findViewById(R.id.btn_setting_default);
        btn_setting_default.setOnClickListener(this);
        mIvBack = (ImageView) findViewById(R.id.mIvBack);
        mIvBack.setOnClickListener(this);
        face_info_no_save = (MoreButtonToggle) findViewById(R.id.face_info_no_save);
        face_info_no_save.setOnMoretListener(this);
        mBtnInfoEnable = (MoreButton) findViewById(R.id.mBtnInfoEnable);
        mBtnInfoEnable.setOnMoretListener(this);
        mBtnAttendEnable = (MoreButtonToggle) findViewById(R.id.mBtnAttendEnable);
        mBtnAttendEnable.setOnMoretListener(this);
        mBtnReadCard = (MoreButtonToggle) findViewById(R.id.mBtnReadCard);
        mBtnReadCard.setOnMoretListener(this);
        mBtnDoorEnable = (MoreButtonToggle) findViewById(R.id.mBtnDoorEnable);
        mBtnDoorEnable.setOnMoretListener(this);
        btn_submit = (Button) findViewById(R.id.btn_submit);
        btn_submit.setOnClickListener(this);
        btn_sleep_time = findViewById(R.id.btn_sleep_time);
        btn_sleep_time.setOnMoretListener(this);
        btn_close_door = findViewById(R.id.btn_close_door);
        btn_close_door.setOnMoretListener(this);

        btn_upload_face = findViewById(R.id.btn_upload_face);
        btn_upload_face.setOnMoretListener(this);
    }


    /***
     * 重启应用
     */
    private void showRebootAppDialog() {
        OridinryDialog oridinryDialog = new OridinryDialog(GeneralSettingsActivity.this);
        oridinryDialog.setOnDialogClickListener(new OridinryDialogClick() {
            @Override
            public void sure() {
                showToast(getString(R.string.set_success));
                SystemManagerUtil.rebootApp(GeneralSettingsActivity.this);
            }

            @Override
            public void noSure() {

            }
        });
        oridinryDialog.show(
                getString(R.string.reboot_msg),
                getString(R.string.reboot),
                getString(R.string.cancel)
        );
    }

    private void showSleepTimeDialog() {
        EditTextDialog editTextDialog = new EditTextDialog(this);
        editTextDialog.setOnDialogClickListener(new EditTextDialog.EditTextDialogListener() {
            @Override
            public void commit(String content) {
                if (content == null || content.length() < 1) {
                    showToast(getString(R.string.set_failed));
                    return;
                }
                try {
                    int sleepTime = Integer.parseInt(content);
                    if (sleepTime < 3) {
                        showToast("Time must be greater than 3");
                        return;
                    }
                    SpUtils.setSleepTime(sleepTime);
                    showToast(getString(R.string.set_success));
                    updateDateInfo();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void hiddleTestClick() {

            }
        });
        String showContent = SpUtils.getSleepTime() + "";
        editTextDialog.show(getString(R.string.sleep_time), showContent, getString(R.string.confirm), "");
    }


    private void showDoorTimeDialog() {
        EditTextDialog editTextDialog = new EditTextDialog(this);
        editTextDialog.setOnDialogClickListener(new EditTextDialog.EditTextDialogListener() {
            @Override
            public void commit(String content) {
                if (content == null || content.length() < 1) {
                    showToast(getString(R.string.input_succ_info));
                    return;
                }
                try {
                    float valueTime = Float.parseFloat(content);
                    if (valueTime < 1) {
                        showToast(getString(R.string.input_succ_info));
                        return;
                    }
                    SpUtils.setCloseDoorTime(valueTime);
                    showToast(getString(R.string.set_success));
                    updateDateInfo();
                } catch (Exception e) {
                    e.printStackTrace();
                    showToast(getString(R.string.set_failed));
                }
            }

            @Override
            public void hiddleTestClick() {

            }
        });
        String showContent = SpUtils.getCloseDoorTime() + "";
        editTextDialog.show(getString(R.string.close_door_time), showContent, getString(R.string.confirm), "");
    }


    private void updateDateInfo() {
        more_btn_card_type.setRigt(SpUtils.getCardComType() == 0 ? "中创" : "德科");
        more_btn_card_ttys.setRigt(SpUtils.getCardTtysChooice());
        btn_sleep_time.setRigt(SpUtils.getSleepTime() + "");
        btn_close_door.setRigt(SpUtils.getCloseDoorTime() + "");
        face_info_no_save.setChcekcAble(SpUtils.getSaveInfoToLocal());
        mBtnAttendEnable.setChcekcAble(SpUtils.getAttendEnable());
        boolean cardEnable = SpUtils.getInputCardEnable();
        mBtnReadCard.setChcekcAble(cardEnable);
        more_btn_card_ttys.setVisibility(cardEnable ? View.VISIBLE : View.GONE);
        more_btn_card_type.setVisibility(cardEnable ? View.VISIBLE : View.GONE);
        mBtnDoorEnable.setChcekcAble(SpUtils.getDoorEnable());
        btn_upload_face.setChcekcAble(SpUtils.getUploadFace());
    }


}
