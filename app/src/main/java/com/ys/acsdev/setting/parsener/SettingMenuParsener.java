package com.ys.acsdev.setting.parsener;

import android.content.Context;
import android.util.Log;

import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;
import com.ys.acsdev.R;
import com.ys.acsdev.api.AppConfig;
import com.ys.acsdev.bean.FaceConfig;
import com.ys.acsdev.bean.GuardConfig;
import com.ys.acsdev.bean.TemperatureConfig;
import com.ys.acsdev.commond.AppInfo;
import com.ys.acsdev.setting.view.SettingMenuView;
import com.ys.acsdev.util.CodeUtil;
import com.ys.acsdev.util.NetWorkUtils;
import com.ys.acsdev.util.SpUtils;

import org.json.JSONArray;
import org.json.JSONObject;

import okhttp3.Call;

public class SettingMenuParsener {


    Context context;
    SettingMenuView settingMenuView;

    public SettingMenuParsener(Context context, SettingMenuView settingMenuView) {
        this.context = context;
        this.settingMenuView = settingMenuView;
    }

    public void sycnConfig() {
        if (SpUtils.getWorkModel() == AppInfo.MODEL_STAND_ALONE) {
            settingMenuView.syncDevInfoFromWeb(false, "It is currently in stand-alone mode");
            return;
        }
        if (!NetWorkUtils.isNetworkConnected(context)) {
            settingMenuView.syncDevInfoFromWeb(false, "Net Error , please Check");
            return;
        }
        settingMenuView.showWaitDialog(true);
        String requestUrl = AppInfo.getSelectConfigDataBySn();
        OkHttpUtils
                .post()
                .url(requestUrl)
                .addParams("sn", CodeUtil.getUniquePsuedoID())
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, String e, int id) {
                        logInfo("====获取设备配置信息= error==" + e);
                        settingMenuView.showWaitDialog(false);
                        settingMenuView.syncDevInfoFromWeb(false, context.getString(R.string.get_config_failed_msg) + " : " + e);
                    }

                    @Override
                    public void onResponse(String json, int id) {
                        settingMenuView.showWaitDialog(false);
                        if (json == null || json.length() < 5) {
                            settingMenuView.syncDevInfoFromWeb(false, "json==null");
                            return;
                        }
                        logInfo("====获取设备配置信息===" + json);
                        try {
                            JSONObject jsonObject = new JSONObject(json);
                            int code = jsonObject.getInt("code");
                            String msg = jsonObject.getString("msg");
                            if (code != 0) {
                                settingMenuView.syncDevInfoFromWeb(false, msg);
                                return;
                            }
                            String data = jsonObject.getString("data");
                            parsenerDate(data);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
    }


    private void parsenerDate(String data) {
        if (data == null || data.length() < 5) {
            settingMenuView.syncDevInfoFromWeb(false, "data==null");
            return;
        }
        settingMenuView.syncDevInfoFromWeb(true, "Setting Success");
        try {
            JSONObject jsonObject = new JSONObject(data);
            String face = jsonObject.getString("face");
            String guard = jsonObject.getString("guard");
            logInfo("guard===" + guard);
            String temperature = jsonObject.getString("temperature");
            FaceConfig faceConfig = parsenerFaceInfo(face);
            setFaceInfo(faceConfig);
            TemperatureConfig temperatureConfig = parsenerTempConfig(temperature);
            setTempInfoConfig(temperatureConfig);
            GuardConfig guardConfig = parsenerGuardianInfo(guard);
            setGuardianAndPassword(guardConfig);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private GuardConfig parsenerGuardianInfo(String guard) {
        GuardConfig guardConfig = null;
        if (guard == null || guard.length() < 5) {
            return null;
        }
        try {
            logInfo("==========解析密码===" + guard);
            JSONObject jsonObject = new JSONObject(guard);
            String pwd = jsonObject.getString("pwd");
            int isSaveInfoLocal = 1;
            if (jsonObject.toString().contains("isSaveInfoLocal")) {
                isSaveInfoLocal = jsonObject.getInt("isSaveInfoLocal");
            }
            logInfo("==========解析密码=111==" + pwd + " / " + isSaveInfoLocal);
            guardConfig = new GuardConfig(pwd, isSaveInfoLocal);
        } catch (Exception e) {
            logInfo("==========解析密码===" + guard);
            e.printStackTrace();
        }
        return guardConfig;
    }

    private TemperatureConfig parsenerTempConfig(String temperature) {
        TemperatureConfig temperatureConfig = null;
        if (temperature == null || temperature.length() < 5) {
            return null;
        }
        try {
            JSONArray jsonArray = new JSONArray(temperature);
            int num = jsonArray.length();
            if (num < 1) {
                return null;
            }
            JSONObject jsonObject = jsonArray.getJSONObject(0);
            String temperatureBtr = jsonObject.getString("temperatureBtr");
            String temperatureCompensation = jsonObject.getString("temperatureCompensation");
            String temperatureIntervalTime = jsonObject.getString("temperatureIntervalTime");
            String temperatureSmp = jsonObject.getString("temperatureSmp");
            String thermometricPeriphera = jsonObject.getString("thermometricPeriphera");
            String temperatureAlarm = jsonObject.getString("temperatureAlarm");
            int temperatureUnit = 0;
            if (jsonObject.toString().contains("temperatureUnit")) {
                temperatureUnit = jsonObject.getInt("temperatureUnit");
            }
            temperatureConfig = new TemperatureConfig(temperatureBtr, temperatureCompensation, temperatureIntervalTime,
                    temperatureSmp, thermometricPeriphera, temperatureAlarm, temperatureUnit);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return temperatureConfig;
    }

    private FaceConfig parsenerFaceInfo(String face) {
        FaceConfig faceConfig = null;
        if (face == null || face.length() < 5) {
            return null;
        }
        try {
            JSONArray jsonArray = new JSONArray(face);
            int num = jsonArray.length();
            if (num < 1) {
                return null;
            }
            JSONObject jsonObject = jsonArray.getJSONObject(0);
            String algorithmAngle = jsonObject.getString("algorithmAngle");
            String cameraDirection = jsonObject.getString("cameraDirection");
            String faceAngle = jsonObject.getString("faceAngle");
            String faceImage = jsonObject.getString("faceImage");
            String frontCameraImage = jsonObject.getString("frontCameraImage");
            String maskMonitoring = jsonObject.getString("maskMonitoring");
            String previewAngle = jsonObject.getString("previewAngle");
            String rearCameraImage = jsonObject.getString("rearCameraImage");
            String recognitionDistance = jsonObject.getString("recognitionDistance");
            String snapAngle = jsonObject.getString("snapAngle");
            int masksIntercept = 0;
            if (jsonObject.toString().contains("masksIntercept")) {
                masksIntercept = jsonObject.getInt("masksIntercept");
            }
            faceConfig = new FaceConfig(algorithmAngle, cameraDirection, faceAngle, faceImage, frontCameraImage,
                    maskMonitoring, previewAngle, rearCameraImage, recognitionDistance, snapAngle, masksIntercept);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return faceConfig;
    }

    private void logInfo(String desc) {
        Log.e("setting", "=====" + desc);
    }

    /****
     * 设置温度
     */
    private void setTempInfoConfig(TemperatureConfig temperatureConfig) {
        if (temperatureConfig == null) {
            return;
        }
        logInfo("温度====" + temperatureConfig.toString());
        int defaultType = SpUtils.getmTempType();
        String recognitionDistance = temperatureConfig.getThermometricPeriphera();
        String device = temperatureConfig.getTemperatureSmp();
        int serRoiDefault = SpUtils.getmTempDev();
        String rare = temperatureConfig.getTemperatureBtr();
        int rareNum = SpUtils.getmTempRare();
        String timeDistance = temperatureConfig.getTemperatureIntervalTime();
        float timeNumDefault = SpUtils.getmTempTime();
        //发热温度
        String temperatureAlarm = temperatureConfig.getTemperatureAlarm();
        float tempWear = SpUtils.getTempHeightWearNew();
        try {
            defaultType = Integer.parseInt(recognitionDistance);
            serRoiDefault = Integer.parseInt(device);
            rareNum = Integer.parseInt(rare);
            timeNumDefault = Float.parseFloat(timeDistance);
            tempWear = Float.parseFloat(temperatureAlarm);
        } catch (Exception e) {
            e.printStackTrace();
        }
        logInfo("===温度类型==" + defaultType);
        SpUtils.setmTempType(defaultType);
        logInfo("===串口==" + serRoiDefault);
        SpUtils.setmTempDev(serRoiDefault);
        logInfo("===测温波特率==" + rareNum);
        SpUtils.setmTempRare(rareNum);
        logInfo("===测温间隔时间==" + timeNumDefault);
        SpUtils.setmTempTime(timeNumDefault);
        logInfo("===发热温度==" + tempWear);
        SpUtils.setTempHeightWearNew(tempWear);
        int temperatureUnit = temperatureConfig.getTemperatureUnit();
        SpUtils.setTempUnit(temperatureUnit);
    }

    /***
     * 设置人脸相关属性
     */
    private void setFaceInfo(FaceConfig faceConfig) {
        if (faceConfig == null) {
            logInfo("人脸设置==faceConfig==null=");
            return;
        }
        logInfo("人脸设置==faceConfig===" + faceConfig.toString());
        try {
            boolean maskEnable = faceConfig.getMaskMonitoring().contains("1") ? true : false;
            logInfo("口罩模式===" + maskEnable);
            SpUtils.setMaskEnable(maskEnable);

            //1：后   0 ：前
            boolean cameraFacing = faceConfig.getCameraDirection().contains("1") ? false : true;
            logInfo("相机方向===" + (cameraFacing ? "前置" : "后置"));
            SpUtils.setCameraFacing(cameraFacing);

            String recognitionDistance = faceConfig.getRecognitionDistance();
            int recogLength = SpUtils.getmFaceLen();

            String algorithmAngle = faceConfig.getAlgorithmAngle();
            int algoThAngle = SpUtils.getmFaceRotate();

            String previewAngle = faceConfig.getPreviewAngle();
            int prewAngleNum = SpUtils.getPrewRotate();

            String saveRotate = faceConfig.getSnapAngle();
            int saveRoateDefault = SpUtils.getSaveRotate();
            try {
                recogLength = Integer.parseInt(recognitionDistance);
                algoThAngle = Integer.parseInt(algorithmAngle);
                prewAngleNum = Integer.parseInt(previewAngle);
                saveRoateDefault = Integer.parseInt(saveRotate);
            } catch (Exception e) {
                e.printStackTrace();
            }
            logInfo("相机抓拍角度===" + saveRoateDefault);
            SpUtils.setSaveRotate(saveRoateDefault);
            logInfo("识别距离===" + recognitionDistance);
            SpUtils.setFaceLen(recogLength);
            logInfo("算法角度===" + recognitionDistance);
            SpUtils.setFaceRotate(algoThAngle);
            //这里要添加 虹软的算法
            logInfo("预览角度===" + previewAngle);
            SpUtils.setPrewRotate(prewAngleNum);

            int defaultFaceRect = SpUtils.getmFaceRect();
            int faceRect = faceConfig.getFaceImage() == null || faceConfig.getFaceImage().isEmpty() ? defaultFaceRect : Integer.parseInt(faceConfig.getFaceImage());
            logInfo("人脸框镜像===" + faceRect);
            SpUtils.setmFaceRect(faceRect);

            int rectRoateDefault = SpUtils.getRectRotate();
            int rectRotate = faceConfig.getFaceAngle() == null || faceConfig.getFaceAngle().isEmpty() ? rectRoateDefault : Integer.parseInt(faceConfig.getFaceAngle());
            logInfo("人脸框角度===" + rectRotate);
            SpUtils.setRectRotate(rectRotate);
            int masksIntercept = faceConfig.getMasksIntercept(); //1 拦截  0 不拦截
            SpUtils.setMaskIntercept(masksIntercept == 1 ? true : false);

//            前后摄像头镜像设置
//            int frontCameraImage = faceConfig.getFrontCameraImage() == null || faceConfig.getFrontCameraImage().isEmpty() ? 0 : Integer.parseInt(faceConfig.getFrontCameraImage());
//            if (frontCameraImage == 1) {
//                RootCmd.setProperty(RootCmd.CAMERA_FONR_MIRROR, "true");
//            } else {
//                RootCmd.setProperty(RootCmd.CAMERA_FONR_MIRROR, "false");
//            }
//            int backCameraImage = faceConfig.getRearCameraImage() == null || faceConfig.getRearCameraImage().isEmpty() ? 1 : Integer.parseInt(faceConfig.getRearCameraImage());
//            if (backCameraImage == 1) {
//                RootCmd.setProperty(RootCmd.CAMERA_BACK_MIRROR, "true");
//            } else {
//                RootCmd.setProperty(RootCmd.CAMERA_BACK_MIRROR, "false");
//            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /***
     * 设置守护进程以及退出密码
     */
    private void setGuardianAndPassword(GuardConfig guardConfig) {
        if (guardConfig == null) {
            return;
        }
        String pwd = guardConfig.getPwd();
        if (pwd == null || pwd.length() < 3) {
            pwd = "123456";
        }
        SpUtils.setSettingPassword(pwd);
        int isSaveInfoLocal = guardConfig.getIsSaveInfoLocal();
        SpUtils.setSaveInfoToLocal(isSaveInfoLocal == 1 ? true : false);
    }

}
