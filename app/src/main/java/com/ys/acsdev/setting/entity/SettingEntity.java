package com.ys.acsdev.setting.entity;

import com.ys.acsdev.R;

public class SettingEntity {

    String title;
    int imageId;
    int clickId;
    //    public static final int RED_LIGHT_SETTING = 2;  //红外补光
    public static final int SERVER_LINE = 0;        //服务器连接
    public static final int FACE_SETTING = 1;       //人臉設置
    public static final int TEMP_CHECK = 2;         //体温设置
    public static final int PERSON_INFO = 3;        //人员信息
    public static final int POWER_ON_LAUNCHER = 4;  //开机启动
    public static final int MODIFY_PASSWORD = 5;    //修改密码
    public static final int ACCESS_RECORD = 6;      //人员信息
    public static final int TEST_CLOCKIN = 7;        //模拟打卡
    public static final int CLOCKIN_RECORD = 8;        //打卡记录
    public static final int EXIT_APP = 9;        //退出APP
    public static final int GENERAL_SETTING = 10;        //通用设置
    public static final int VOICE_SETTING = 11;        //语音设置
    public static final int TEST_SETTING = 12;        //灯光测试设置
    public static final int APP_UPDATE = 13;        //灯光测试设置
    public static final int VISITOR_INFO = 14;        //访客信息
    public static final int MODEL_CHANGE = 15;  //切换模式
    public static final int APP_INFO = 16;  //设备信息
    public static final int DEV_LIGHT_SETTING = 17;  //亮度设定
    public static final int DEV_VOICE_SETTING = 18;  //音量设置
    public static final int BLACK_PERSON = 20;  //黑名单
    public static final int URL_SETTING_GX = 21;  //国兴，网页设置




    public SettingEntity(int clickId, String title, int imageId) {
        this.clickId = clickId;
        this.title = title;
        this.imageId = imageId;
    }

    public int getClickId() {
        return clickId;
    }

    public void setClickId(int clickId) {
        this.clickId = clickId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getImageId() {
        return imageId;
    }

    public void setImageId(int imageId) {
        this.imageId = imageId;
    }
}
