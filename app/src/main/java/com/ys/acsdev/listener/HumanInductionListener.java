package com.ys.acsdev.listener;

public interface HumanInductionListener {
    void onHasHuman(boolean hasHuman);

    /**
     * 初始化状态反馈
     *
     * @param isTrue
     * @param desc
     */
    void initStatues(boolean isTrue, String desc);
}
