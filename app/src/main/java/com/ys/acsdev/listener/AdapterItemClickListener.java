package com.ys.acsdev.listener;

public interface AdapterItemClickListener {

    void adapterItemClick(int id, Object object, int clickPosition);

}
