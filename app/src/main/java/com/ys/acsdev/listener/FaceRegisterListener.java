package com.ys.acsdev.listener;


/***
 * 人脸注册
 */
public interface FaceRegisterListener {

    void registerFaceStatues(boolean isTrue, int code, String msg);

}
