package com.ys.acsdev.listener;

import android.view.View;

public interface MoreButtonToggleListener {
    void switchToggleView(View view, boolean isChooice);
}