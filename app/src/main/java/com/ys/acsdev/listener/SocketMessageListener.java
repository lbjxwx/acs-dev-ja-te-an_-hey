package com.ys.acsdev.listener;

import org.json.JSONObject;

public interface SocketMessageListener {

    void messageOrderBack(int code, JSONObject obj);

}
