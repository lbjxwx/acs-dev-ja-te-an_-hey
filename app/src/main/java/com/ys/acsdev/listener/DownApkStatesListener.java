package com.ys.acsdev.listener;

import com.ys.acsdev.runnable.down.DownFileEntity;

public interface DownApkStatesListener {

    void requestBack(String requestDesc);

    void downStateInfo(DownFileEntity entity);
}