package com.ys.acsdev.listener;

import com.ys.acsdev.bean.RedioEntity;

public interface RadioChooiceListener {

    void backChooiceInfo(RedioEntity redioEntity, int chooicePosition);

}
