package com.ys.acsdev.listener;

public interface QrDismissListener {
    void onDismiss();
}