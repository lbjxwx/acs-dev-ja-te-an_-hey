package com.ys.acsdev.listener;

import com.ys.acsdev.bean.PersonBean;

import java.util.List;

public interface FaceSyncInfoBackListener {
    /***
     * 返回需要注册的人员
     * @param needRegisterData
     */
    void backFaceNeedDownList(boolean isTrue, List<PersonBean> needRegisterData, String errorDesc);
}