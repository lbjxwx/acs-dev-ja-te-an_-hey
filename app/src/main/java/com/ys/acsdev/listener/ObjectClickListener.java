package com.ys.acsdev.listener;

public interface ObjectClickListener {
    void clickSure(Object object);
}
