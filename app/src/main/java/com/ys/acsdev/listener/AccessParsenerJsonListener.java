package com.ys.acsdev.listener;

import com.ys.acsdev.bean.AccessRecordBean;

import java.util.List;

/***
 * 解析通行记录的回调
 */
public interface AccessParsenerJsonListener {

    void backJsonInfo(boolean isTrue, List<AccessRecordBean> accessRecordBeanList, String errorDesc);
}
