package com.ys.acsdev.listener;

import android.view.View;

public interface MoreButtonListener {
    void clickView(View view);
}