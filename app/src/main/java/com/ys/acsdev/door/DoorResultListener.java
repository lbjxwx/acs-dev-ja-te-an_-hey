package com.ys.acsdev.door;

// Created by kermitye on 2020/5/14 9:27
public interface DoorResultListener {
    void onDoorResult(boolean success);
}
