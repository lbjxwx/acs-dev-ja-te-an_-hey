package com.ys.acsdev.door;

import android.os.Handler;
import android.os.Message;

import com.ys.acsdev.MyApp;
import com.ys.acsdev.util.LogUtil;
import com.ys.acsdev.util.yface.GPIOManager;

/**
 * ===================================================================================
 * 继电器方式开门
 * ===================================================================================
 */
public class RelayDoor extends BaseDoor {

    private RelayDoor() {
    }

    private static class SingletonHolder {
        public static final RelayDoor INSTANCE = new RelayDoor();
    }

    public static RelayDoor getInstance() {
        return SingletonHolder.INSTANCE;
    }


    private static final int MSG_CLOSE = 0x01;

    Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case MSG_CLOSE:
                    close();
                    break;
            }
        }
    };
    DoorResultListener mListener;

    public void init() {
        GPIOManager.getInstance(MyApp.getInstance());
    }

    public void open() {
        log("=======RelayDoor======开门");
        GPIOManager.getInstance(MyApp.getInstance()).pullUpRelay();
//        if (mListener != null)
//            mListener.onDoorResult(success);
        if (mCloseTime > 0) {
            mHandler.sendEmptyMessageDelayed(MSG_CLOSE, (long) (mCloseTime * 1000));
        }
    }

    @Override
    public void close() {
        log("=======DoorManager=====000=关门");
        GPIOManager.getInstance(MyApp.getInstance()).pullDownRelay();
    }

    public boolean checkState() {
        String value = GPIOManager.getInstance(MyApp.getInstance()).getRelayStatus().trim();
        log("===============当前门的状：" + value);
        return value == "1";
    }


    public void setDoorResultListener(DoorResultListener listener) {
        this.mListener = listener;
    }


    public void destory() {
        mListener = null;
        mHandler.removeMessages(MSG_CLOSE);
    }

    private void log(String msg) {
        LogUtil.e(msg, "RelayDoor");
    }

}
