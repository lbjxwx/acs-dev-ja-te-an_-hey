package com.ys.acsdev.door;

public abstract class BaseDoor {
    protected float mCloseTime;

    public abstract void init();

    public abstract void open();

    public abstract void close();

    public abstract void destory();


    public void setCloseTime(float closeTime) {
        this.mCloseTime = closeTime;
    }
}
