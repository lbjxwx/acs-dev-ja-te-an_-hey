package com.ys.acsdev.door;

import android.os.Handler;
import android.os.Message;

import com.kongqw.serialportlibrary.SerialPortManager;
import com.kongqw.serialportlibrary.listener.OnSerialPortDataListener;
import com.ys.acsdev.util.LogUtil;

import java.io.File;

public class SerialportDoor extends BaseDoor {

    private SerialportDoor() {
    }

    private static class SingletonHolder {
        public static final SerialportDoor INSTANCE = new SerialportDoor();
    }

    public static SerialportDoor getInstance() {
        return SingletonHolder.INSTANCE;
    }

    private static final int MSG_CLOSE = 0x01;

    DoorResultListener mListener;

    private String mDeviceType = "/dev/ttyS3";
    private int mBaudrate = 9600;  //波率
    SerialPortManager mSerialPort;

    private static final byte[] DATA_OPEN = new byte[]{(byte) 0xAA, 0x02, 0x00, 0x00, 0x55};


    Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case MSG_CLOSE:
                    close();
                    break;
            }
        }
    };


    public void init() {
        if (mSerialPort == null) {
            mSerialPort = new SerialPortManager();
            //添加串口数据通信监听
            mSerialPort.setOnSerialPortDataListener(new OnSerialPortDataListener() {

                @Override
                public void onDataReceived(byte[] bytes) {

                }

                @Override
                public void onDataSent(byte[] bytes) {

                }
            });
        }
        boolean isOpen = mSerialPort.openSerialPort(new File(mDeviceType), mBaudrate);
        if (!isOpen) {
            LogUtil.saveLogToLocal("初始化开门失败");
            log("=====初始化失败");
        } else {
            log("初始化成功");
        }
    }


    public void open() {
        if (mSerialPort == null)
            return;
        mSerialPort.sendBytes(DATA_OPEN);
        if (mCloseTime > 0) {
            mHandler.sendEmptyMessageDelayed(MSG_CLOSE, (long) (mCloseTime * 1000));
        }
    }


    //暂时不需要关门
    public void close() {

    }

    public void destory() {
        mListener = null;
        mHandler.removeMessages(MSG_CLOSE);
        if (mSerialPort != null) {
            mSerialPort.closeSerialPort();
        }
    }


    public void setDoorResultListener(DoorResultListener listener) {
        this.mListener = listener;
    }


    private void log(String msg) {
        LogUtil.e(msg, "SerialportDoor");
    }

}
