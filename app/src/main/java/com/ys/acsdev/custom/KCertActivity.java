package com.ys.acsdev.custom;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.ys.acsdev.R;
import com.ys.acsdev.ui.BaseActivity;
import com.ys.acsdev.ui.SplashActivity;
import com.ys.acsdev.util.SpUtils;

public class KCertActivity extends BaseActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kcview);
        initView();
    }

    EditText et_current_pwd;
    EditText et_new_pwd;
    Button btn_submit;
    ImageView iv_back;
    LinearLayout lin_modify, lin_to_main;
    EditText et_to_main;
    Button btn_submit_main;

    private void initView() {
        lin_modify = (LinearLayout) findViewById(R.id.lin_modify);
        lin_to_main = (LinearLayout) findViewById(R.id.lin_to_main);
        et_to_main = (EditText) findViewById(R.id.et_to_main);
        et_current_pwd = (EditText) findViewById(R.id.et_current_pwd);
        et_new_pwd = (EditText) findViewById(R.id.et_new_pwd);
        btn_submit = (Button) findViewById(R.id.btn_submit);
        btn_submit.setOnClickListener(this);
        btn_submit_main = (Button) findViewById(R.id.btn_submit_main);
        btn_submit_main.setOnClickListener(this);
        iv_back = (ImageView) findViewById(R.id.iv_back);
        iv_back.setOnClickListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateViewEdit();
    }

    private void updateViewEdit() {
        boolean isFirstLogin = SpUtils.isFirstLogin();
        if (isFirstLogin) {
            lin_modify.setVisibility(View.VISIBLE);
            lin_to_main.setVisibility(View.GONE);
        } else {
            lin_modify.setVisibility(View.GONE);
            lin_to_main.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_submit_main:
                String loginPwd = getLoginPwd();
                if (loginPwd == null || loginPwd.length() < 3) {
                    showToast("Pwd is null");
                    return;
                }
                String currentPwd = SpUtils.getSettingPassword();
                if (!loginPwd.equals(currentPwd)) {
                    showToast("Pwd is Wrong");
                    return;
                }


                startToSplash();
                break;
            case R.id.iv_back:
                finish();
                break;
            case R.id.btn_submit:
                comfimPwd();
                break;
        }
    }

    private void comfimPwd() {
        String currentPwd = getCurrentPwd();
        String newPwd = getNewPwd();
        boolean isJujle = JujleInfoSuccess(currentPwd, newPwd);
        if (!isJujle) {
            return;
        }
        SpUtils.setSettingPassword(newPwd);
        startToSplash();
    }

    private void startToSplash() {
        Intent intent = new Intent(KCertActivity.this, SplashActivity.class);
        startActivity(intent);
        finish();
    }

    private boolean JujleInfoSuccess(String currentPwd, String newPwd) {
        if (currentPwd == null || currentPwd.length() < 3) {
            showToast("Current Pwd is null");
            return false;
        }
        if (newPwd == null || newPwd.length() < 3) {
            showToast("New Pwd is null");
            return false;
        }
        String password = SpUtils.getSettingPassword();
        if (!currentPwd.equals(password)) {
            showToast("Current Pwd is Wrong");
            return false;
        }
        if (newPwd.equals(currentPwd)) {
            showToast("The modified password does not match the original password");
            return false;
        }
        return true;
    }

    private String getLoginPwd() {
        return et_to_main.getText().toString().trim();
    }

    private String getCurrentPwd() {
        return et_current_pwd.getText().toString().trim();
    }

    private String getNewPwd() {
        return et_new_pwd.getText().toString().trim();
    }
}
