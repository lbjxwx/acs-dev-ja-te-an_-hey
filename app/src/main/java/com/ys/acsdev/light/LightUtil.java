package com.ys.acsdev.light;

import com.ys.acsdev.util.CpuModel;

/**
 * 人脸识别
 * 灯光控制
 */
public class LightUtil {

    //绿灯得路径
    public static String LIGHT_GREEN_PATH = "/sys/class/custom_class/custom_dev/green_led";
    private static String LIGHT_GREEN_PATH_3399 = "sys/devices/platform/misc_power_en/g_led";
    //白灯得路径
    private static String LIGHT_WHITE_PATH = "/sys/class/custom_class/custom_dev/white_led";
    private static String LIGHT_WHITE_PATH_3399 = "/sys/devices/platform/misc_power_en/w_led";
    //红色灯得路径
    private static String LIGHT_RED_PATH = "/sys/class/custom_class/custom_dev/red_led";
    private static String LIGHT_RED_PATH_3399 = "sys/devices/platform/misc_power_en/r_led";
    //红外灯开关
    private static String LIGHT_RED_LINE_PATH = "/sys/class/custom_class/custom_dev/ir";
    private static String LIGHT_RED_LINE_PATH_3399 = "sys/devices/platform/misc_power_en/ir_led";

    private LightHelper mRedHelper;
    private LightHelper mGreenHelper;
    private LightHelper mWhiteHelper;
    private LightHelper mRedLineHelper;

    private LightUtil() {
        mRedHelper = new LightHelper(LIGHT_RED_PATH());
        mGreenHelper = new LightHelper(LIGHT_GREEN_PATH());
        mWhiteHelper = new LightHelper(LIGHT_WHITE_PATH());
        mRedLineHelper = new LightHelper(LIGHT_RED_LINE_PATH());
    }

    private static class SingletonHolder {
        public static final LightUtil INSTANCE = new LightUtil();
    }

    public static LightUtil getInstance() {
        return SingletonHolder.INSTANCE;
    }


    public boolean getWhiteState() {
        return "1".equals(mWhiteHelper.read());
    }

    public boolean getGreenState() {
        return "1".equals(mGreenHelper.read());
    }

    public boolean getRedState() {
        return "1".equals(mRedHelper.read());
    }

    public void openWhite() {
        mWhiteHelper.write("1");
    }

    public void closeWhite() {
        mWhiteHelper.write("0");
    }

    public void openGreen() {
        mGreenHelper.write("1");
    }

    public void closeGreen() {
        mGreenHelper.write("0");
    }

    public void openRed() {
        mRedHelper.write("1");
    }

    public void closeRed() {
        mRedHelper.write("0");
    }


    public void openRedLine() {
        mRedLineHelper.write("1");
    }

    public void closeRedLine() {
        mRedLineHelper.write("0");
    }


    public void release() {
        mRedHelper.release();
        mGreenHelper.release();
        mWhiteHelper.release();
        mRedLineHelper.release();
    }


    private String LIGHT_RED_LINE_PATH() {
        String cpuModel = CpuModel.getMobileType();
        if (cpuModel.contains("rk3399")) {
            return LIGHT_RED_LINE_PATH_3399;
        }
        return LIGHT_RED_LINE_PATH;
    }

    //绿灯得路径
    private String LIGHT_GREEN_PATH() {
        String cpuModel = CpuModel.getMobileType();
        if (cpuModel.contains("rk3399")) {
            return LIGHT_GREEN_PATH_3399;
        }
        return LIGHT_GREEN_PATH;
    }


    //白灯得路径
    private String LIGHT_WHITE_PATH() {
        String cpuModel = CpuModel.getMobileType();
        if (cpuModel.contains("rk3399")) {
            return LIGHT_WHITE_PATH_3399;
        }
        return LIGHT_WHITE_PATH;
    }

    //红色灯得路径
    private String LIGHT_RED_PATH() {
        String cpuModel = CpuModel.getMobileType();
        if (cpuModel.contains("rk3399")) {
            return LIGHT_RED_PATH_3399;
        }
        return LIGHT_RED_PATH;
    }

}
