package com.ys.acsdev.light;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;

import com.ys.acsdev.R;
import com.ys.acsdev.util.SimpleDateUtil;
import com.ys.acsdev.util.SpUtils;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.TimeUnit;

// Created by kermitye on 2020/8/1 8:50
public class LightManager {

    java.util.concurrent.ExecutorService executorService = new java.util.concurrent.ThreadPoolExecutor(
            1, 3,
            0, TimeUnit.SECONDS,
            new ArrayBlockingQueue(10)  // 使用有界队列，避免OOM
    );

    private Handler mHandler = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            closeRedLight();
            closeGreenLight();
            closeWhiteLight();
        }
    };

    private LightManager() {
    }

    private static class SingletonHolder {
        public static final LightManager INSTANCE = new LightManager();
    }

    public static LightManager getInstance() {
        return SingletonHolder.INSTANCE;
    }


    public void openWhiteLight() {
        executorService.execute(() -> {
            closeGreenLight();
            closeRedLight();
            LightUtil.getInstance().openWhite();
            delayCloseLight();
        });

    }

    public void closeWhiteLight() {
        LightUtil.getInstance().closeWhite();
    }

    /**
     * 打开绿色的灯
     */
    public void openGreenLight() {
        executorService.execute(() -> {
            closeRedLight();
            closeWhiteLight();
            LightUtil.getInstance().openGreen();
            delayCloseLight();
        });
    }

    public void closeGreenLight() {
        LightUtil.getInstance().closeGreen();
    }

    public void openRedLight() {
        executorService.execute(() -> {
            closeGreenLight();
            closeWhiteLight();
            LightUtil.getInstance().openRed();
            delayCloseLight();
        });
    }

    public void closeRedLight() {
        LightUtil.getInstance().closeRed();
    }

    /**
     * 开红外线灯光
     */
    public void openRedLineLight() {
        executorService.execute(() -> {
            LightUtil.getInstance().openRedLine();
        });
    }

    public static void closeRedLineLight() {
        LightUtil.getInstance().closeRedLine();
    }


    public void delayCloseLight() {
        mHandler.removeMessages(0);
        mHandler.sendEmptyMessageDelayed(0, 3000);
    }

    public static void logInfo(String logMessage) {
//        LogUtil.e(logMessage, "lightUtil");
    }


    public void destory() {
        logInfo("=======LightUtil: destory");
        mHandler.removeMessages(0);
        closeAllLight();
        LightUtil.getInstance().release();
    }

    public void closeAllLight() {
        logInfo("=======LightUtil: closeAllLight");
        closeRedLight();
        closeRedLineLight();
        closeGreenLight();
        closeWhiteLight();
    }

}
