package com.ys.acsdev.light;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

// Created by kermitye on 2020/7/31 21:18
public class LightHelper {

    private File mFile;
    private FileOutputStream fileWriteStream;
    private FileInputStream fileInputStream;

    public LightHelper(String path) {
        try {
            mFile = new File(path);
            if (!mFile.exists()) {
                mFile = null;
                return;
            }
            fileWriteStream = new FileOutputStream(mFile);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void write(String context) {
        if (fileWriteStream == null)
            return;
        try {
            fileWriteStream.write(context.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    //读取会不停的新建对象谨慎使用
    public String read() {
        if (mFile == null)
            return "";
        close();
        String result = "";
        try {
            fileInputStream = new FileInputStream(mFile);
            byte[] data = new byte[1];
            fileInputStream.read(data);
            result = new String(data);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            close();
        }
        return result;
    }

    private void close() {
        if (fileInputStream != null) {
            try {
                fileInputStream.close();
                fileInputStream = null;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    public void release() {
        if (fileWriteStream != null) {
            try {
                fileWriteStream.close();
                fileWriteStream = null;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        close();
    }
}
