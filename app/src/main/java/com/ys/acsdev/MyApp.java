package com.ys.acsdev;

import android.app.Application;
import android.content.SharedPreferences;


import com.ys.acsdev.util.CrashExceptionHandler;
import com.ys.acsdev.util.RootCmd;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.https.HttpsUtils;

import org.litepal.LitePal;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class MyApp extends Application {

    public static MyApp Instance;
    private static SharedPreferences mSharedPreferences;
    public static String USER_INFO = "EFace_Sp";
//    OSS oss;

    public static MyApp getInstance() {
        return Instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mSharedPreferences = getSharedPreferences(USER_INFO, 0);
        Instance = this;
        initOther();
    }

    private void initOther() {
        LitePal.initialize(this); //数据库初始化
//        Bugly.init(getApplicationContext(), "a2eeac3d90", false);
        RootCmd.setProperty("persist.cam.reset", "true");
        RootCmd.setProperty("persist.sys.usbcamera.status", "");
        CrashExceptionHandler crashExceptionHandler = CrashExceptionHandler.getCrashInstance();
        crashExceptionHandler.init();
        initOkHttps();
    }

    /***
     * cdl
     * OKhttp改成单例模式，不用重复初始化，
     * 占用服务器多个端口得问题
     */
    private void initOkHttps() {
        //HTTPS相关的访问
        HttpsUtils.SSLParams sslParams = HttpsUtils.getSslSocketFactory(null, null, null);
        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .connectTimeout(10000L, TimeUnit.MILLISECONDS)
                .readTimeout(10000L, TimeUnit.MILLISECONDS)
                .sslSocketFactory(sslParams.sSLSocketFactory, sslParams.trustManager)
                .addInterceptor(new Interceptor() {
                    @Override
                    public Response intercept(Chain chain) throws IOException {
                        Request request = chain.request()
                                .newBuilder()
                                .header("Accept-Language", "en-US")
                                .build();
                        return chain.proceed(request);
                    }
                })
                .build();
        OkHttpUtils.initClient(okHttpClient);
    }

    public String getResourceStr(int resId) {
        return getResources().getString(resId);
    }

    public void clearAllSharedInfo() {
        mSharedPreferences.edit().clear().commit();
    }

    public void saveData(String key, Object data) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        try {
            if (data instanceof Integer) {
                editor.putInt(key, (Integer) data);
            } else if (data instanceof Boolean) {
                editor.putBoolean(key, (Boolean) data);
            } else if (data instanceof String) {
                editor.putString(key, (String) data);
            } else if (data instanceof Float) {
                editor.putFloat(key, (Float) data);
            } else if (data instanceof Long) {
                editor.putLong(key, (Long) data);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        boolean isSave = editor.commit();
    }

    public Object getData(String key, Object defaultObject) {
        try {
            if (defaultObject instanceof String) {
                return mSharedPreferences.getString(key, (String) defaultObject);
            } else if (defaultObject instanceof Integer) {
                return mSharedPreferences.getInt(key, (Integer) defaultObject);
            } else if (defaultObject instanceof Boolean) {
                return mSharedPreferences.getBoolean(key, (Boolean) defaultObject);
            } else if (defaultObject instanceof Float) {
                return mSharedPreferences.getFloat(key, (Float) defaultObject);
            } else if (defaultObject instanceof Long) {
                return mSharedPreferences.getLong(key, (Long) defaultObject);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


}
