package com.ys.acsdev.runnable.upload;

import android.os.Build;

import com.etv.util.xutil.HttpUtils;
import com.etv.util.xutil.exception.HttpException;
import com.etv.util.xutil.http.RequestParams;
import com.etv.util.xutil.http.ResponseInfo;
import com.etv.util.xutil.http.callback.RequestCallBack;
import com.etv.util.xutil.http.client.multipart.MIME;
import com.etv.util.xutil.http.client.util.HttpMethod;
import com.ys.acsdev.api.AppConfig;
import com.ys.acsdev.commond.AppInfo;
import com.ys.acsdev.util.CodeUtil;
import com.ys.acsdev.util.LogUtil;
import com.ys.acsdev.util.SpUtils;
import com.ys.acsdev.util.TempDealUtil;
import com.ys.acsdev.util.custom.YktValidate;

import org.json.JSONObject;

import java.io.File;

import okhttp3.internal.http2.Header;

/***
 * 提交通行记录
 */
public class UpdateDoorInfoRunnable implements Runnable {

    private static HttpUtils httpUtils;

    String filePath;
    UpdateImageListener listener;
    String recordTime;
    String phone;
    String validCode;
    String temperature;
    String passType;
    String wearMask;
    String idCardNum;
    String personName;
    String qrCodeType;

    public UpdateDoorInfoRunnable() {
        if (httpUtils == null) {
            httpUtils = new HttpUtils(50000);
        }
    }

    public void setUpdateDoorInfo(String recordTime, String phone,
                                  String validCode, String temperature,
                                  String filePath, String passType, String wearMask,
                                  String idCardNum, String personName, String qrCodeType, UpdateImageListener listener) {
        this.recordTime = recordTime;
        this.phone = phone;
        this.validCode = validCode;
        this.temperature = temperature;
        this.filePath = filePath;
        this.passType = passType;
        this.wearMask = wearMask;
        this.idCardNum = idCardNum;
        this.personName = personName;
        this.qrCodeType = qrCodeType;  //0空白 1绿 2黄 3红
        this.listener = listener;
    }

    @Override
    public void run() {
        if (temperature.contains(AppInfo.TEMP_TYPE_LOW_TEMP)) {
            //低温不上传
            return;
        }
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.O_MR1) {
            //9.0系统
//            startToDowmFile();
        } else {
            photoUpload();
        }
    }

    public void photoUpload() {
        String doorOpen = SpUtils.getDoorEnable() ? "1" : "0";
        LogUtil.update("=======photoUpload");
        String devCode = CodeUtil.getUniquePsuedoID();
        RequestParams params = new RequestParams();
        String token = recordTime + "";
        String singCode = "";
        long currentTime = System.currentTimeMillis();
        LogUtil.update("=======temperature==上传=" + temperature);
        String isFever = TempDealUtil.getTempIsFever(temperature);
        String tempUpdate = TempDealUtil.getUpdateTempNum(temperature);
        LogUtil.update("=======temperature==发烧=" + isFever + " / " + tempUpdate + " / wearMask=" + wearMask);
        params.addHeader("token", token);
        params.addBodyParameter("recordTime", recordTime);
        params.addBodyParameter("passType", passType);
        params.addBodyParameter("phone", phone);
        params.addBodyParameter("validCode", validCode);
        params.addBodyParameter("isFever", isFever + "");
        params.addBodyParameter("temperature", tempUpdate);
        params.addBodyParameter("isOpenDoor", doorOpen);    //0未开门 1开门
        if (passType.contains(AppInfo.PASSTYPE_UNKNOW)) {
            params.addBodyParameter("userName", personName);
            params.addBodyParameter("idCardNum", idCardNum);
        }
        if (SpUtils.getUploadFace()) {
            //上传人脸图片
            File file = new File(filePath);
            params.addBodyParameter("files", file, MIME.ENC_BINARY);
        } else {
            //上传默认得图片
            File file = new File(AppInfo.DEFAULT_FACE_IMAGE_PATH);
            if (file.exists()) {
                params.addBodyParameter("files", file, MIME.ENC_BINARY);
            }
        }
        params.addBodyParameter("qrCodeType", qrCodeType);
        params.addBodyParameter("isMask", wearMask);
        params.addBodyParameter("sn", devCode);
        LogUtil.update("=====fineName = " + devCode +
                " \n recordTime" + recordTime +
                "\n validCode= " + validCode +
                "\n filePath=" + filePath +
                "\n passType=" + passType +
                "\nisFever =  " + isFever +
                "\n temperature =" + temperature +
                "\ncurrentTime=" + currentTime +
                "\nwearMask=" + wearMask +
                "\ndevCode=" + devCode +
                "\nqrCodeType=" + qrCodeType +
                "\nsingCode= " + singCode);
        String requestUtl = AppInfo.getUpdateReocrdInfo();
        LogUtil.update("文件上传====" + requestUtl);
        httpUtils.send(HttpMethod.POST, requestUtl, params,
                new RequestCallBack() {
                    @Override
                    public void onStart() {
                        super.onStart();
                    }

                    @Override
                    public void onLoading(long total, long current,
                                          boolean isUploading) {
                        super.onLoading(total, current, isUploading);
                        if (total < 1) {
                            total = 1;
                        }
                        if (current < 1) {
                            current = 1;
                        }
                        int progress = (int) (current * 100 / total);
                        if (listener == null) {
                            return;
                        }
                        listener.updateImageProgress(progress);
                    }

                    @Override
                    public void onSuccess(ResponseInfo arg0) {
                        try {
                            String jsonBack = arg0.result.toString();
                            if (jsonBack.length() < 5) {
                                if (listener != null) {
                                    listener.updateImageFailed("上传成功:Json==null");
                                }
                                return;
                            }
                            LogUtil.update("上传成功==000=" + jsonBack);
                            JSONObject jsonObject = new JSONObject(jsonBack);
                            int code = jsonObject.getInt("code");
                            String msg = jsonObject.getString("msg");
                            if (code != 0) {
                                if (listener != null) {
                                    listener.updateImageFailed("Failed:" + msg);
                                }
                                return;
                            }
                            if (listener != null) {
                                listener.updateImageSuccess("Success :" + msg);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(HttpException arg0, String arg1) {
                        LogUtil.update("上传通行记录失败==" + arg0.toString() + " : " + arg1);
                        if (listener != null) {
                            listener.updateImageFailed("Failed:" + arg0.toString());
                        }
                    }
                });
    }

    private void logMath(String s) {
        LogUtil.e("temoMath", s);
    }
}
