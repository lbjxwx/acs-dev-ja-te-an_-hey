package com.ys.acsdev.runnable.down;

import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.etv.util.xutil.HttpUtils;
import com.etv.util.xutil.exception.HttpException;
import com.etv.util.xutil.http.HttpHandler;
import com.etv.util.xutil.http.ResponseInfo;
import com.etv.util.xutil.http.callback.RequestCallBack;
import com.ys.acsdev.poweronoff.util.MyLog;
import com.ys.acsdev.util.CodeUtil;
import com.ys.acsdev.util.FileUtils;
import com.ys.acsdev.util.LogUtil;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.FileCallBack;

import java.io.File;

import okhttp3.Call;
import okhttp3.Request;

public class DownRunnable implements Runnable {

    private HttpHandler<File> httHhandler = null;
    String downUrl, fileName;
    String saveUrl;
    DownStateListener listener;
    HttpUtils httpUtils;
    long downProNum = 0;  //上一次下载量，用来判断下载速度
    boolean isFalse = false;
    int LimitDownSpeed = -1;


    public void setLimitDownSpeed(int LimitDownSpeed) {
        this.LimitDownSpeed = LimitDownSpeed;
    }

    public void setIsDelFile(boolean isDelFile) {
        this.isFalse = isDelFile;
    }

    public DownRunnable() {
    }

    public DownRunnable(String downUrl, String saveUrl, String fileName, DownStateListener listener) {
        this.downUrl = downUrl;
        this.fileName = fileName;
        this.saveUrl = saveUrl;
        this.listener = listener;
        httHhandler = null;
        FileUtils.creatPathNotExcit();
    }

    public void setDownInfo(String downUrl, String saveUrl, String fileName, DownStateListener listener) {
        this.downUrl = downUrl;
        this.saveUrl = saveUrl;
        this.fileName = fileName;
        this.listener = listener;
        httHhandler = null;
        FileUtils.creatPathNotExcit();
    }

    @Override
    public void run() {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.O_MR1) {
            //9.0系统
            startToDowmFile();
        } else {
            startToDownDefault();
        }
    }

    private void startToDownDefault() {
        //第一个参数:下载地址
        //第二个参数:文件存储路径
        //第三个参数:是否断点续传
        //第四个参数:是否重命名
        //第五个参数:请求回调
        try {
            if (httpUtils == null) {
                httpUtils = new HttpUtils();
            }
            File fileDown = new File(saveUrl);
            //如果不需要断点续传，这里可以删除
            if (isFalse) {
                if (fileDown.exists()) {
                    LogUtil.down("======文件存在，删除文件");
                    fileDown.delete();
                }
            }
            fileDown.createNewFile();
            httpUtils.configRequestThreadPoolSize(3);//设置由几条线程进行下载
            httpUtils.setLimitDownSpeed(LimitDownSpeed);
            httHhandler = httpUtils.download(downUrl, saveUrl, true, true, new RequestCallBack<File>() {
                @Override
                public void onStart() {
                    super.onStart();
                    LogUtil.down("======开始下载：" + downUrl);
                    downProNum = 0;
                    backState("开始下载", DownFileEntity.DOWN_STATE_START, 0, downUrl, saveUrl, 1000);
                }

                @Override
                public void onLoading(long total, long current, boolean isUploading) {
                    super.onLoading(total, current, isUploading);
                    int progress = (int) (current * 100 / total);
                    int speed = (int) ((current - downProNum) / 1024);
//                    LogUtil.down("======下载进度==progress=" + progress + "   current=" + current + "/" + total + "    /speed = " + speed + " / " + saveUrl);
                    backState("下载中", DownFileEntity.DOWN_STATE_PROGRESS, progress, downUrl, saveUrl, speed);
                    downProNum = current;
                }

                @Override
                public void onSuccess(ResponseInfo<File> responseInfo) {
                    LogUtil.down("===下载成功===" + saveUrl);
                    if (httHhandler != null) {
                        httHhandler.cancel();
                    }
                    backState("下载成功", DownFileEntity.DOWN_STATE_SUCCESS, 100, downUrl, saveUrl, 0);
                }

                @Override
                public void onFailure(HttpException e, String s) {
                    LogUtil.down("===下载失败==" + s + "   /" + e.toString());
                    if (httHhandler != null) {
                        httHhandler.cancel();
                    }
                    backState(s, DownFileEntity.DOWN_STATE_FAIED, 0, downUrl, saveUrl, -1);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            Log.i("down", "下载异常==" + e.toString());
        }
    }

    private void startToDowmFile() {
        OkHttpUtils
                .get()
                .url(downUrl)
                .build()
                .execute(new FileCallBack(saveUrl, fileName) {

                    @Override
                    public void onBefore(Request request, int id) {
                        LogUtil.down("======开始下载：" + downUrl);
                        downProNum = 0;
                        backState("开始下载", DownFileEntity.DOWN_STATE_START, 0, downUrl, saveUrl, 1000);
                    }

                    @Override
                    public void inProgress(int progress, long total, int id) {
                        int speed = 1000;
//                    LogUtil.down("======下载进度==progress=" + progress + "   current=" + current + "/" + total + "    /speed = " + speed + " / " + saveUrl);
                        backState("下载中", DownFileEntity.DOWN_STATE_PROGRESS, progress, downUrl, saveUrl, speed);
                    }

                    @Override
                    public void onError(Call call, String errorMessage, int id) {
                        LogUtil.down("===下载失败==" + errorMessage);
                        if (httHhandler != null) {
                            httHhandler.cancel();
                        }
                        backState(errorMessage, DownFileEntity.DOWN_STATE_FAIED, 0, downUrl, saveUrl, -1);
                    }

                    @Override
                    public void onResponse(File file, int id) {
                        if (httHhandler != null) {
                            httHhandler.cancel();
                        }
                        backState("下载成功", DownFileEntity.DOWN_STATE_SUCCESS, 100, downUrl, saveUrl, 0);
                    }
                });
    }

    public void stopDown() {
        try {
            if (httHhandler == null) {
                return;
            }
            httHhandler.cancel();
        } catch (Exception e) {
            LogUtil.cdl("=====准备停止下载,执行异常==" + e.toString(), true);
            e.printStackTrace();
        }
    }

    private Handler handler = new Handler(Looper.getMainLooper());

    private void backState(final String state, final int downState, final int progress,
                           final String downUrl, final String saveUrl, final int speed) {
        handler.post(new Runnable() {
            @Override
            public void run() {
                DownFileEntity entity = new DownFileEntity();
                entity.setDesc(state);
                entity.setDownState(downState);
                entity.setDownSpeed(speed);
                entity.setProgress(progress);
                entity.setDownPath(downUrl);
                entity.setSavePath(saveUrl);
                listener.downStateInfo(entity);
            }
        });

    }


}
