package com.ys.acsdev.runnable.down;

/**
 * 下载状态回调
 */

public interface DownStateListener {

    void downStateInfo(DownFileEntity entity);

}
