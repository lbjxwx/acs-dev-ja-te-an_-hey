package com.ys.acsdev.runnable;

import android.graphics.Bitmap;
import android.os.Handler;


import com.ys.acsdev.commond.AppInfo;
import com.ys.acsdev.listener.WriteBitmapToLocalListener;
import com.ys.zip.CompressImageListener;
import com.ys.zip.CompressImageUtil;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class BitmapWriteLocalRunnable implements Runnable {

    Bitmap bmp;
    String saveZipPath;
    WriteBitmapToLocalListener listener;
    private Handler handler = new Handler();

    public BitmapWriteLocalRunnable(Bitmap bmp, String saveZipPath, WriteBitmapToLocalListener listener) {
        this.bmp = bmp;
        this.saveZipPath = saveZipPath;
        this.listener = listener;
    }

//    val tempFile = FileUtil.createTmpFile(MyApp.instance, AppInfo.BASE_PATH_CACHE)

    @Override
    public void run() {
        saveImageToGallery();
    }

    public void saveImageToGallery() {
        if (bmp == null) {
            backImageInfo(false, "bmp==null");
            return;
        }
        FileOutputStream fos = null;
        try {
            String imagePath = AppInfo.BASE_PATH_CACHE + "/cache.jpg";
            File file = new File(imagePath);
            if (file.exists()) {
                file.delete();
            }
            file.createNewFile();
            fos = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.JPEG, 20, fos);
            fos.flush();
            zipImage(imagePath);
        } catch (Exception e) {
            backImageInfo(false, e.toString());
            e.printStackTrace();
        } finally {
            try {
                if (fos != null) {
                    fos.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void zipImage(String savePath) {
        CompressImageUtil.getInstance().compressPic(savePath, saveZipPath, new CompressImageListener() {
            @Override
            public void backErrorDesc(String desc) {
                backImageInfo(false, desc);
            }

            @Override
            public void backImageSuccess(String oldPath, String imagePath) {
                backImageInfo(true, imagePath);
            }
        });
    }

    public void backImageInfo(boolean isTrue, String desc) {
        if (listener == null) {
            return;
        }
        if (handler == null) {
            return;
        }
        handler.post(new Runnable() {
            @Override
            public void run() {
                if (listener != null) {
                    listener.writeStatues(isTrue, desc);
                }
            }
        });
    }

}
