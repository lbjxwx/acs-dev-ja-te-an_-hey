package com.ys.acsdev.runnable.upload;

import com.etv.util.xutil.HttpUtils;
import com.etv.util.xutil.exception.HttpException;
import com.etv.util.xutil.http.RequestParams;
import com.etv.util.xutil.http.ResponseInfo;
import com.etv.util.xutil.http.callback.RequestCallBack;
import com.etv.util.xutil.http.client.multipart.MIME;
import com.etv.util.xutil.http.client.util.HttpMethod;
import com.ys.acsdev.commond.AppInfo;
import com.ys.acsdev.util.CodeUtil;
import com.ys.acsdev.util.LogUtil;

import org.json.JSONObject;

import java.io.File;

public class UpdateVisotorRunnable implements Runnable {

    String name;
    String phone;
    int isSymptom;
    int isAbroad;
    int isSeparate;
    File file;
    int sex;
    UpdateImageListener listener;

    public UpdateVisotorRunnable(String name, String phone, int isSymptom, int isAbroad, int isSeparate, File file, int sex, UpdateImageListener listener) {
        this.name = name;
        this.phone = phone;
        this.isSymptom = isSymptom;
        this.isAbroad = isAbroad;
        this.isSeparate = isSeparate;
        this.file = file;
        this.sex = sex;
        this.listener = listener;
    }

    @Override
    public void run() {
        if (!file.exists()) {
            listener.updateImageFailed("File is not exists");
            return;
        }
        HttpUtils utils = new HttpUtils(50000); // 设置连接超时
        String sn = CodeUtil.getUniquePsuedoID();
        RequestParams params = new RequestParams();
        String token = System.currentTimeMillis() + "";
        params.addHeader("token", token);
        params.addBodyParameter("name", name);
        params.addBodyParameter("phone", phone);
        params.addBodyParameter("sex", sex + "");
        params.addBodyParameter("isSymptom", isSymptom + "");
        params.addBodyParameter("isAbroad", isAbroad + "");
        params.addBodyParameter("isSeparate", isSeparate + "");
        params.addBodyParameter("mac", CodeUtil.getUniquePsuedoID());
        params.addBodyParameter("files", file, MIME.ENC_BINARY);
        String requestUtl = AppInfo.addEpidemicVisitorInfo();
        LogUtil.update("===上传成功==提交新增人员000=" + isSymptom + " / " + isAbroad + " / " + isSeparate);
        utils.send(HttpMethod.POST, requestUtl, params,
                new RequestCallBack() {

                    @Override
                    public void onStart() {
                        super.onStart();
                    }

                    @Override
                    public void onLoading(long total, long current,
                                          boolean isUploading) {
                        super.onLoading(total, current, isUploading);
                        LogUtil.update("===上传进度===" + current + " / " + total);
                        if (total < 1) {
                            total = 1;
                        }
                        if (current < 1) {
                            current = 1;
                        }
                        int progress = (int) (current * 100 / total);
                        if (listener == null) {
                            return;
                        }
                        listener.updateImageProgress(progress);
                    }

                    @Override
                    public void onSuccess(ResponseInfo arg0) {
                        String result = arg0.result.toString();
                        LogUtil.update("===上传成功==提交新增人员000=" + result);
                        if (result != null && !result.isEmpty()) {
                            try {
                                JSONObject jsonObject = new JSONObject(result);
                                int code = jsonObject.optInt("code");
                                String msg = jsonObject.optString("msg");
                                LogUtil.update("==提交新增人员000=" + code + ":" + msg);
                                if (code != 0) {
                                    listener.updateImageFailed(msg);
                                    return;
                                } else {
                                    listener.updateImageSuccess("Success: " + msg);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                                return;
                            }
                        }
                    }

                    @Override
                    public void onFailure(HttpException arg0, String arg1) {
                        LogUtil.update("==提交新增人员000=" + arg0 + ":" + arg1);
                        listener.updateImageFailed("Failed :" + arg0.toString());
                    }
                });


    }
}
