package com.ys.acsdev.runnable.upload;

import com.etv.util.xutil.HttpUtils;
import com.etv.util.xutil.exception.HttpException;
import com.etv.util.xutil.http.RequestParams;
import com.etv.util.xutil.http.ResponseInfo;
import com.etv.util.xutil.http.callback.RequestCallBack;
import com.etv.util.xutil.http.client.multipart.MIME;
import com.etv.util.xutil.http.client.util.HttpMethod;
import com.ys.acsdev.commond.AppInfo;
import com.ys.acsdev.util.CodeUtil;
import com.ys.acsdev.util.LogUtil;

import org.json.JSONObject;

import java.io.File;

/***
 * 提交通行记录
 */
public class UpdateAddPersonInfoRunnable implements Runnable {

    String requestUtl = AppInfo.insertPersonData();
    UpdateImageListener listener;
    String deviceId;
    String mEtName;
    String phoneNum;
    String sex;
    String filePath;
    String cardNum;
    String idCard;


    public UpdateAddPersonInfoRunnable(String deviceId, String mEtName, String cardNum, String phoneNum,
                                       String sex, String filePath, String idCard, UpdateImageListener listener) {
        this.deviceId = deviceId;
        this.mEtName = mEtName;
        this.cardNum = cardNum;
        this.phoneNum = phoneNum;
        this.sex = sex;
        this.filePath = filePath;
        this.listener = listener;
        this.idCard = idCard;
    }

    @Override
    public void run() {
        photoUpload();
    }

    public void photoUpload() {
        File file = new File(filePath);
        if (!file.exists()) {
            listener.updateImageFailed("上传文件不存在");
            return;
        }
        HttpUtils utils = new HttpUtils(50000); // 设置连接超时
        String sn = CodeUtil.getUniquePsuedoID();
        RequestParams params = new RequestParams();
        String token = System.currentTimeMillis() + "";
        params.addHeader("token", token);
        params.addBodyParameter("deptId", deviceId);
        params.addBodyParameter("name", mEtName);
        params.addBodyParameter("phone", phoneNum);
        params.addBodyParameter("sex", sex + "");
        params.addBodyParameter("cardNum", cardNum + "");
        params.addBodyParameter("sn", sn + "");
        params.addBodyParameter("idCard", idCard);
        params.addBodyParameter("files", file, MIME.ENC_BINARY);
        utils.send(HttpMethod.POST, requestUtl, params,
                new RequestCallBack() {

                    @Override
                    public void onStart() {
                        super.onStart();
                    }

                    @Override
                    public void onLoading(long total, long current,
                                          boolean isUploading) {
                        super.onLoading(total, current, isUploading);
                        LogUtil.update("===上传进度===" + current + " / " + total);
                        if (total < 1) {
                            total = 1;
                        }
                        if (current < 1) {
                            current = 1;
                        }
                        int progress = (int) (current * 100 / total);
                        if (listener == null) {
                            return;
                        }
                        listener.updateImageProgress(progress);
                    }

                    @Override
                    public void onSuccess(ResponseInfo arg0) {
                        String result = arg0.result.toString();
                        LogUtil.update("===上传成功==提交新增人员000=" + result);
                        if (listener == null) {
                            return;
                        }
                        if (result != null && !result.isEmpty()) {
                            try {
                                JSONObject jsonObject = new JSONObject(result);
                                int code = jsonObject.optInt("code");
                                String msg = jsonObject.optString("msg");
                                if (code != 0) {
                                    LogUtil.update("==提交新增人员000=" + code + ":" + msg);
                                    if (listener != null) {
                                        listener.updateImageFailed(msg);
                                    }
                                    return;
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                                return;
                            }
                        }
                        listener.updateImageSuccess("上传成功");
                    }

                    @Override
                    public void onFailure(HttpException arg0, String arg1) {
                        LogUtil.update("==提交新增人员000=" + arg0 + ":" + arg1);
                        if (listener == null) {
                            return;
                        }
                        listener.updateImageFailed("Update Failed :" + arg0.toString());
                    }
                });
    }

}
