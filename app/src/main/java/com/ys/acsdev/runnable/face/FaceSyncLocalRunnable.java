package com.ys.acsdev.runnable.face;

import android.os.Handler;

import com.ys.acsdev.api.AppConfig;
import com.ys.acsdev.bean.FaceInfo;
import com.ys.acsdev.bean.PersonBean;
import com.ys.acsdev.db.dao.FaceInfoJavaDao;
import com.ys.acsdev.db.dao.PersonDao;
import com.ys.acsdev.listener.FaceSyncInfoBackListener;
import com.ys.acsdev.util.LogUtil;
import com.ys.facepasslib.FaceManager;

import java.util.ArrayList;
import java.util.List;

/***
 * 人员同步本地
 */
public class FaceSyncLocalRunnable implements Runnable {
    List<PersonBean> datas;
    private Handler handler = new Handler();
    FaceSyncInfoBackListener listener;

    public FaceSyncLocalRunnable(List<PersonBean> datas, FaceSyncInfoBackListener listener) {
        this.datas = datas;
        this.listener = listener;
    }

    @Override
    public void run() {
        SyncLocalPersonBeanInfo();
    }

    /***
     * 同步显示得数据库
     */
    private void SyncLocalPersonBeanInfo() {
        if (datas == null || datas.size() < 1) {
            backFaceList(false, null, "DATA ==NULL");
            return;
        }
        log("开始同步本地人员信息===000");
        //同步本地人员信息,添加或删除，每次获取信息得时候重新添加，不用同步
        PersonDao.insertAllSyncPerson(datas, new PersonDao.PersonDaoListener() {
            @Override
            public void syncFaceInfoCompany(boolean isTrue, String errorDesc) {
                log("=====同步本地人员信息数据=====" + isTrue + " / " + errorDesc);
                syncFaceImageInfoDb(datas);
            }
        });
    }

    private void log(String desc) {
//        LogUtil.persoon("===FaceSyncLocalRunnable==" + desc);
    }

    //同步本地人脸信息,删除服务器中已删除的人脸信息
    private void syncFaceImageInfoDb(List<PersonBean> datas) {
        log("====同步人脸图片信息=");
        FaceInfoJavaDao.syncFaceByPseron(datas, new FaceInfoJavaDao.FaceInfoDaoListener() {
            @Override
            public void syncFaceInfoCompany(boolean isTrue, String errorDesc) {
                log("=====同步本地人脸图片数据=====" + isTrue + " / " + errorDesc);
                jujleNeedDownFileList();
            }
        });
    }

    private void jujleNeedDownFileList() {
        log("====获取需要下载得图片====");
        //获取需要注册的人员
        List<PersonBean> needRegisterData = getNeedRegisterData();
        //开始下载
        if (needRegisterData == null || needRegisterData.size() < 1) {
            log("====获取需要下载得图片==null或者 00 ==");
            backFaceList(false, null, "没有需要下载得文件");
            return;
        }
        log("====获取需要下载得图片====" + needRegisterData.size());
        backFaceList(true, needRegisterData, "解析匹配数据成功");
    }

    private void backFaceList(boolean ieTrue, List<PersonBean> needRegisterData, String errorDesc) {
        if (listener == null) {
            return;
        }
        if (handler == null) {
            return;
        }
        handler.post(new Runnable() {
            @Override
            public void run() {
                log("====返回同步得信息====" + ieTrue + " / " + errorDesc);
                listener.backFaceNeedDownList(ieTrue, needRegisterData, errorDesc);
            }
        });
    }

    /**
     * 获取需要注册的员工数据
     *
     * @return
     */
    private List<PersonBean> getNeedRegisterData() {
        List<PersonBean> allPerson = PersonDao.getAllPerson();
        if (allPerson == null || allPerson.size() < 1) {
            return null;
        }
        List<PersonBean> result = new ArrayList<>();
        for (PersonBean person : allPerson) {
            FaceInfo faceEntity = FaceInfoJavaDao.getFaceInfoById(person.getPerId());
            if (faceEntity != null) {
                String perId = person.getPerId();
                String savePathName = faceEntity.getPath();
                String personUrl = person.getPhotoUrl();
                String faceEntityUrl = faceEntity.getDownFileUrl();
                if (savePathName.contains(perId) && personUrl.equals(faceEntityUrl)) {
                    log("=====facepass service: $name 人脸已注册并且未修改==" + personUrl);
                    continue;
                }
            }
            //人脸未注册或已修改，删除已注册的人脸信息
            if (faceEntity != null) {
                FaceManager.getInstance().delFaceByToken(faceEntity.getFaceToken());
            }
            String photouRL = person.getPhotoUrl();
            if (photouRL == null || photouRL.isEmpty() || photouRL.contains("null")) {
                log("=====facepass service: 人脸路径为空 $name / $path");
                continue;
            }
            result.add(person);
        }
        return result;
    }
}
