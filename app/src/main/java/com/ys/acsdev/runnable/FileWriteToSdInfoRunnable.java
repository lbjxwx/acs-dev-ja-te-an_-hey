package com.ys.acsdev.runnable;

import android.text.TextUtils;

import com.ys.acsdev.util.FileUtils;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;

/**
 * 将log信息写入到SD设备中
 */
public class FileWriteToSdInfoRunnable implements Runnable {

    String devInfo;
    String filePath;

    public FileWriteToSdInfoRunnable(String devInfo, String filePath) {
        this.devInfo = devInfo;
        this.filePath = filePath;
    }

    @Override
    public void run() {
        try {
            FileUtils.creatPathNotExcit();
            if (devInfo.length() < 5 || TextUtils.isEmpty(devInfo)) {
                return;
            }
            File file = new File(filePath);
            if (!file.exists()) {
                file.createNewFile();
            }
            devInfo = "\n" + devInfo;
            RandomAccessFile raf = new RandomAccessFile(file, "rwd");
            raf.seek(file.length());
            raf.write(devInfo.getBytes());
            raf.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
