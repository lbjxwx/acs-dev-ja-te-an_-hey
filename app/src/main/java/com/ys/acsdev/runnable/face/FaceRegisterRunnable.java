package com.ys.acsdev.runnable.face;

import android.os.Handler;

import com.ys.acsdev.api.AppConfig;
import com.ys.acsdev.bean.FaceInfo;
import com.ys.acsdev.bean.PersonBean;
import com.ys.acsdev.db.dao.FaceInfoJavaDao;
import com.ys.acsdev.db.dao.PersonDao;
import com.ys.acsdev.listener.FaceRegisterListener;
import com.ys.acsdev.util.FileUtils;
import com.ys.acsdev.util.LogUtil;
import com.ys.facepasslib.FaceManager;
import com.ys.facepasslib.IRegisterFaceListener;

import org.jetbrains.annotations.NotNull;

import java.io.File;

/***
 * 人脸注册线程
 */
public class FaceRegisterRunnable implements Runnable {
    FaceInfo info;
    File imageFile;
    FaceRegisterListener listener;

    public void setRegiFaceInfo(FaceInfo info, File imageFile, FaceRegisterListener listener) {
        this.info = info;
        this.imageFile = imageFile;
        this.listener = listener;
    }

    @Override
    public void run() {
        registerFaceByFacePass();
    }

    private Handler handler = new Handler();

    private void backRegisterStatues(boolean b, int code, String faceToken) {
        if (listener == null) {
            return;
        }
        handler.post(new Runnable() {
            @Override
            public void run() {
                listener.registerFaceStatues(b, code, faceToken);
            }
        });
    }

    //旷视人脸注册
    private void registerFaceByFacePass() {
        final FaceInfo faceInfo = info;
        FaceManager.getInstance().registerByFile(imageFile, new IRegisterFaceListener() {
            @Override
            public void onSuccess(@NotNull String faceToken, @NotNull String featurePath) {
                if (imageFile.exists()) {
                    imageFile.delete();
                }
                faceInfo.setFaceToken(faceToken);
                FileUtils.deleteDirOrFile(imageFile, "人脸注册成功，删除本地保存的图片");
                LogUtil.persoon("人脸注册成功: " + faceInfo.getName() + "  :  " + faceInfo.toString(), true);
                FaceInfoJavaDao.insertFaceInfo(faceInfo);
                backRegisterStatues(true, 0, faceToken);
            }

            @Override
            public void onError(int code, @NotNull String msg) {
                backRegisterStatues(false, code, msg);
                FileUtils.deleteDirOrFile(info.getPath(), "===人脸失败，质量问题");
                LogUtil.persoon("=====人脸注册失败 " + info.getName() + " / " + msg + " /path = " + info.getPath());
                PersonBean person = PersonDao.getPersonById(info.getPersonId());
                if (person != null) {
                    person.setRegisterDesc(msg);
                    boolean isSave = PersonDao.insertPerson(person);
                    LogUtil.persoon("=====onError=====" + isSave);
                }
            }
        });
    }


}
