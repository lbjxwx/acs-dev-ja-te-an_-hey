package com.ys.acsdev.runnable;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ImageFormat;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.YuvImage;
import android.os.Handler;
import android.os.Looper;

import com.ys.acsdev.commond.AppInfo;
import com.ys.acsdev.runnable.upload.RoateImageListener;
import com.ys.acsdev.util.FileUtils;
import com.ys.acsdev.util.LogUtil;
import com.ys.acsdev.util.SpUtils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class ImageRoateRunnable implements Runnable {

    byte[] previewData;
    int width;
    int height;
    int rotate;
    RoateImageListener listener;
    private Handler handler = new Handler(Looper.getMainLooper());
    int pucChangeSize = 1;

    public ImageRoateRunnable(byte[] previewData, int width, int height, int rotate, RoateImageListener listener) {
        this.previewData = previewData;
        this.width = width;
        this.height = height;
        this.rotate = rotate;
        this.listener = listener;
        int Picquity = SpUtils.getUpdatePicQuity(); //图片保存的分辨率
        if (Picquity == 0) {
            //  * 0：低分辨率
            pucChangeSize = 1;
        } else if (Picquity == 1) {
            // * 1：中分辨率
            pucChangeSize = 3;
        } else if (Picquity == 2) {
            // * 2：高分辨率
            pucChangeSize = 5;
        } else {
            pucChangeSize = 1;
        }
    }

    @Override
    public void run() {
        try {
            YuvImage img = new YuvImage(
                    previewData,
                    ImageFormat.NV21,
                    width,
                    height,
                    null
            );
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            img.compressToJpeg(new Rect(0, 0, width, height), 8 * pucChangeSize, stream);
            Bitmap bitmap = BitmapFactory.decodeByteArray(stream.toByteArray(), 0, stream.size());
            Matrix mat = new Matrix();
            mat.postRotate(rotate);
            bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), mat, true);
            stream.close();
            LogUtil.update("=====pecFace tranBitmap success==: " + (bitmap == null));
            saveImageToGallery(bitmap);
        } catch (Exception e) {
            LogUtil.update("=====pecFace: tranBitmap error ");
            backImageListener(false, e.toString());
            e.printStackTrace();
        }
    }

    public void saveImageToGallery(Bitmap bitmap) {
        FileUtils.creatPathNotExcit();
        if (bitmap == null) {
            backImageListener(false, "bmp==null");
            return;
        }
        FileOutputStream fos = null;
        try {
            String imagePath = AppInfo.BASE_PATH_CACHE + "/" + System.currentTimeMillis() + ".jpg";
            File file = new File(imagePath);
            if (file.exists()) {
                file.delete();
            }
            file.createNewFile();
            fos = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 15 * pucChangeSize, fos);
            fos.flush();
            backImageListener(true, imagePath);
        } catch (Exception e) {
            backImageListener(false, e.toString());
            e.printStackTrace();
        } finally {
            try {
                if (fos != null) {
                    fos.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void backImageListener(boolean isSuccess, String imagePath) {
        if (listener == null) {
            return;
        }
        handler.post(new Runnable() {
            @Override
            public void run() {
                listener.backImageListener(isSuccess, imagePath);
            }
        });
    }
}
