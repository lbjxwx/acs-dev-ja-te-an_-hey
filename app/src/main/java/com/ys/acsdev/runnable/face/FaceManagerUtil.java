package com.ys.acsdev.runnable.face;


import com.ys.acsdev.api.AppConfig;
import com.ys.acsdev.bean.FaceInfo;
import com.ys.acsdev.commond.AppInfo;
import com.ys.acsdev.db.dao.FaceInfoJavaDao;
import com.ys.acsdev.db.dao.PersonDao;
import com.ys.acsdev.service.AcsService;
import com.ys.acsdev.util.LogUtil;
import com.ys.facepasslib.FaceManager;

import java.util.List;

/****
 * 用来管理人脸信息管理
 * 批量操作
 */
public class FaceManagerUtil {


    public FaceManagerUtil() {

    }

    /***
     * 清空本地所有得数据
     */
    public static void delAllLocalFaceInfo(FaceManagerUtilListener listener) {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                LogUtil.persoon("=====facepass service: person is null");
                PersonDao.deleteAllPerson();
                List<FaceInfo> faceInfos = FaceInfoJavaDao.getAllFaceInfo();
                if (faceInfos == null || faceInfos.size() < 1) {
                    listener.delPersonInfoBack();
                    return;
                }
                for (int i = 0; i < faceInfos.size(); i++) {
                    FaceInfo faceInfo = faceInfos.get(i);
                    FaceManager.getInstance().delFaceByToken(faceInfo.getFaceToken());
                }
                FaceInfoJavaDao.delAllFaceInfoByType(-1, "service比对所有的数据，清空数据");
                listener.delPersonInfoBack();
                LogUtil.persoon("====同步人员信息完成====清除所有得人员信息===");
            }
        };
        AcsService.getInstance().executor(runnable);
    }


    /***
     * 清除员工所有数据
     *     //人员管理
     *     public static final int FACE_REGISTER_PERSON = 1;
     *     //访客
     *     public static final int FACE_REGISTER_VISITOR = 2;
     *     //黑名单
     *     public static final int FACE_REGISTER_BLACK_LIST = 3;
     */
    public static void deleteLocalFaceInfoByType(int type, FaceManagerUtilListener listener) {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                PersonDao.deleteAllPerson();
                List<FaceInfo> faceInfos = FaceInfoJavaDao.getAllByType(type);
                if (faceInfos == null || faceInfos.size() < 1) {
                    listener.delPersonInfoBack();
                    return;
                }
                for (int i = 0; i < faceInfos.size(); i++) {
                    FaceInfo faceInfo = faceInfos.get(i);
                    FaceManager.getInstance().delFaceByToken(faceInfo.getFaceToken());
                }
                FaceInfoJavaDao.delAllFaceInfoByType(type, "人员管理界面，删除人员信息");
                listener.delPersonInfoBack();
            }
        };
        AcsService.getInstance().executor(runnable);
    }


    public interface FaceManagerUtilListener {
        //删除人员信息
        void delPersonInfoBack();
    }

}
