package com.ys.acsdev.runnable.upload;

import com.etv.util.xutil.HttpUtils;
import com.etv.util.xutil.exception.HttpException;
import com.etv.util.xutil.http.RequestParams;
import com.etv.util.xutil.http.ResponseInfo;
import com.etv.util.xutil.http.callback.RequestCallBack;
import com.etv.util.xutil.http.client.multipart.MIME;
import com.etv.util.xutil.http.client.util.HttpMethod;
import com.ys.acsdev.util.CodeUtil;
import com.ys.acsdev.util.LogUtil;
import com.ys.acsdev.util.SimpleDateUtil;

import java.io.File;

/***
 * 文件上传线程
 */
public class UpdateFileRunnable implements Runnable {

    String requestUtl;
    String filePath;
    UpdateImageListener listener;

    public UpdateFileRunnable(String requestUtl, String filePath) {
        this.requestUtl = requestUtl;
        this.filePath = filePath;
    }

    public UpdateFileRunnable(String requestUtl, String filePath, UpdateImageListener listener) {
        this.requestUtl = requestUtl;
        this.filePath = filePath;
        this.listener = listener;
    }

    @Override
    public void run() {
        photoUpload();
    }

    public void photoUpload() {
        HttpUtils utils = new HttpUtils(50000); // 设置连接超时
        String fileName = CodeUtil.getUniquePsuedoID();
        LogUtil.update("=====fineName 文件上传= " + fileName);
        RequestParams params = new RequestParams();
        String token = SimpleDateUtil.formatBig(System.currentTimeMillis()) + "";
        params.addHeader("token", token);
        params.addBodyParameter("sn", fileName);
        File file = new File(filePath);
        params.addBodyParameter("files", file, MIME.ENC_BINARY);
        utils.send(HttpMethod.POST, requestUtl, params,
                new RequestCallBack() {

                    @Override
                    public void onStart() {
                        super.onStart();
                    }

                    @Override
                    public void onLoading(long total, long current,
                                          boolean isUploading) {
                        super.onLoading(total, current, isUploading);
//                        LogUtil.update("===上传进度===" + current + " / " + total);
                        if (total < 1) {
                            total = 1;
                        }
                        if (current < 1) {
                            current = 1;
                        }
                        int progress = (int) (current * 100 / total);
                        if (listener == null) {
                            return;
                        }
                        listener.updateImageProgress(progress);
                    }

                    @Override
                    public void onSuccess(ResponseInfo arg0) {
                        LogUtil.update("上传成功==000=" + arg0.result.toString());
                        if (listener == null) {
                            return;
                        }
                        listener.updateImageSuccess("上传成功");
                    }

                    @Override
                    public void onFailure(HttpException arg0, String arg1) {
                        LogUtil.update("上传日志失败==" + arg0 + ":" + arg1);
                        if (listener == null) {
                            return;
                        }
                        listener.updateImageSuccess("Update Failed :" + arg0.toString());
                    }
                });
    }

}
