package com.ys.acsdev.db.dao;

import com.ys.acsdev.bean.PersonBean;
import com.ys.acsdev.commond.AppInfo;
import com.ys.acsdev.service.AcsService;
import com.ys.acsdev.util.LogUtil;

import org.litepal.LitePal;
import org.litepal.crud.callback.FindMultiCallback;

import java.util.ArrayList;
import java.util.List;

public class PersonDao {

    public static PersonBean getPersonById(String perId) {
        List<PersonBean> beanList = LitePal.where("perId=?", perId).find(PersonBean.class);
        if (beanList == null || beanList.size() < 1) {
            return null;
        }
        return beanList.get(0);
    }

    public static PersonBean getPersonByCard(String cardNum) {
        List<PersonBean> beanList = LitePal.where("cardNum=?", cardNum).find(PersonBean.class);
        if (beanList == null || beanList.size() < 1) {
            return null;
        }
        return beanList.get(0);
    }

    public static PersonBean getPersonByIdCard(String idCardNum) {
        List<PersonBean> beanList = LitePal.where("idCard=?", idCardNum).find(PersonBean.class);
        if (beanList == null || beanList.size() < 1) {
            return null;
        }
        return beanList.get(0);
    }

    public static List<PersonBean> getAllPerson() {
        return LitePal.findAll(PersonBean.class);
    }

    /***
     * 根据类型筛数据
     *
     *     -1    FACE_REGISTER_PERSON_ALL 获取全部信息
     *     //人员管理
     *     public static final int FACE_REGISTER_PERSON = 1;
     *     //访客
     *     public static final int FACE_REGISTER_VISITOR = 2;
     *     //黑名单
     *     public static final int FACE_REGISTER_BLACK_LIST = 3;
     * @param callBack
     */
    public static void getAllPersonByListener(int personType, FindMultiCallback<PersonBean> callBack) {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                List<PersonBean> beanList = null;
                if (personType == AppInfo.FACE_REGISTER_PERSON_ALL) {
                    beanList = getAllPerson();
                } else {
                    beanList = LitePal.where("type=?", personType + "").find(PersonBean.class);
                }
                if (callBack != null) {
                    callBack.onFinish(beanList);
                }
            }
        };
        AcsService.getInstance().executor(runnable);
//        LitePal.findAllAsync(PersonBean.class).listen(callBack);
    }

    public static boolean insertPerson(PersonBean person) {
        if (person == null) {
            return false;
        }
        boolean isSave = person.saveOrUpdate("perId=?", person.getPerId());
        return isSave;
    }

    public static void insertAllSyncPerson(List<PersonBean> datas, PersonDaoListener listener) {
        if (datas == null || datas.size() < 1) {
            listener.syncFaceInfoCompany(false, "原始数据为null");
            return;
        }
        List<String> newData = new ArrayList<String>();
        for (PersonBean personBean : datas) {
            newData.add(personBean.getPerId());
        }
        syncFaceData(newData, listener);
    }

    //同步数据，删除垃圾数据
    public static void syncFaceData(List<String> newData, PersonDaoListener listener) {
        try {
            //如果没有新数据，
            List<PersonBean> personBeanList = getAllPerson();
            if (personBeanList == null || personBeanList.size() < 1) {
                listener.syncFaceInfoCompany(false, "本地数据为null");
                return;
            }
            //旧数据与新数据差集，找出需要删除的数据=======================================
            List<String> oldList = new ArrayList<String>();
            for (int i = 0; i < personBeanList.size(); i++) {
                PersonBean personBean = personBeanList.get(i);
                oldList.add(personBean.getPerId());
            }
            oldList.removeAll(newData);
            if (oldList == null || oldList.size() < 1) {
                listener.syncFaceInfoCompany(true, "比对完成,数据一摸一样");
                return;
            }
            for (int i = 0; i < oldList.size(); i++) {
                String oldString = oldList.get(i);
                delPersonByPerId(oldString);
            }
            listener.syncFaceInfoCompany(true, "比对完成,删除数据成功");
//            for (int i = 0; i < personBeanList.size(); i++) {
//                boolean isDateExict = false;
//                PersonBean personBean = personBeanList.get(i);
//                String oldString = personBean.getPerId();
//                for (int j = 0; j < newData.size(); j++) {
//                    String newString = newData.get(j);
//                    if (newString.equals(oldString)) {
//                        isDateExict = true;
//                        continue;
//                    }
//                }
//                if (!isDateExict) { //如果人员信息存在就不操作，如果
//                    delPersonByPerId(oldString);
//                }
//            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static boolean delPersonByPerId(String perId) {
        LogUtil.persoon("删除数据库里面得数据==" + perId);
        try {
            int isDelNul = LitePal.deleteAll(PersonBean.class, "perId=?", perId);
            if (isDelNul > 0) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public static void deleteAllPerson() {
        LogUtil.persoon("删除所有数据库里面得数据==");
        try {
            LitePal.deleteAll(PersonBean.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public interface PersonDaoListener {
        void syncFaceInfoCompany(boolean isTrue, String errorDesc);
    }

}
