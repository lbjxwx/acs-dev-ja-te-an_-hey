package com.ys.acsdev.db.dao;

import android.content.ContentValues;
import android.util.Log;

import com.ys.acsdev.bean.SocketEntity;
import com.ys.acsdev.util.LogUtil;

import org.litepal.LitePal;

import java.util.List;


/***
 * 服务器连接记录数表
 */
public class SocketLineDb {

    /***
     * 保存控件数据库
     * @param entity
     * @return
     */
    public static boolean saveSocketLineStateInfo(SocketEntity entity) {
        if (entity == null) {
            return false;
        }
        String ip = entity.getIp();
        try {
            List<SocketEntity> cpList = LitePal.where("ip=?", ip).find(SocketEntity.class);
            if (cpList == null || cpList.size() < 1) {
                LogUtil.cdl("===0000====没有数据，添加到数据库");
                return addSocketInfoToDb(entity);
            } else {
                return modifySocketLineInfo(entity);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        LogUtil.cdl("===0000====不知道为什么");
        return false;
    }

    private static boolean modifySocketLineInfo(SocketEntity entity) {
        if (entity == null) {
            return false;
        }
        String ip = entity.getIp();
        String port = entity.getPort();
        String userName = entity.getUserName();
        ContentValues values = new ContentValues();
        values.put("port", port);
        values.put("userName", userName);
        int modifyNum = LitePal.updateAll(SocketEntity.class, values, "ip=?", ip);
        if (modifyNum > 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 保存数据到数据库
     *
     * @param entity
     * @return
     */
    private static boolean addSocketInfoToDb(SocketEntity entity) {
        boolean isSave = false;
        if (entity == null) {
            return isSave;
        }
        try {
            isSave = entity.save();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return isSave;
    }

    public static boolean delPowerInfoByIp(String ip) {
        try {
            int delNum = LitePal.deleteAll(SocketEntity.class, "ip = ?", ip);
            if (delNum < 1) {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }

    public static List<SocketEntity> getSocketInfoList() {
        List<SocketEntity> txtList = null;
        try {
            txtList = LitePal.findAll(SocketEntity.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return txtList;
    }

    public static void clearAllData() {
        LitePal.deleteAll(SocketEntity.class);
    }

}
