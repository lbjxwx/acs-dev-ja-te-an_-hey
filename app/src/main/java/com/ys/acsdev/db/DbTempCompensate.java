package com.ys.acsdev.db;


import android.content.ContentValues;

import com.ys.acsdev.setting.entity.TempAddEntity;
import com.ys.acsdev.util.Biantai;
import com.ys.acsdev.util.LogUtil;
import com.ys.acsdev.util.SimpleDateUtil;
import com.ys.acsdev.util.SpUtils;

import org.litepal.LitePal;

import java.util.List;

/****
 * 温度补偿数据库工具类
 */
public class DbTempCompensate {

    /***
     * 获取当前补偿值
     * 每分钟更新一次
     * @return
     */
    public static void getCurrentTempAdd() {
        if (Biantai.isOneClick()) {
            return;
        }
        List<TempAddEntity> tempAddEntities = queryTempComInfo();
        if (tempAddEntities == null || tempAddEntities.size() < 1) {
            LogUtil.cdl("====更新温度补偿值===0");
            SpUtils.setmTempComp(0);
            return;
        }
        float backNum = 0;
        for (int i = 0; i < tempAddEntities.size(); i++) {
            TempAddEntity tempAddEntity = tempAddEntities.get(i);
            String sateTimeId = tempAddEntity.getSateTimeId();
            String startTime = tempAddEntity.getStartTime();
            String endTime = tempAddEntity.getEndTime();
            if (startTime == null || !startTime.contains(":")) {
                deleteTempComById(sateTimeId);
                continue;
            }
            if (endTime == null || !endTime.contains(":")) {
                deleteTempComById(sateTimeId);
                continue;
            }
            startTime = startTime.replace(":", "");
            endTime = endTime.replace(":", "");
            int startCom = Integer.parseInt(startTime);
            int endCom = Integer.parseInt(endTime);
            int currentHourMin = SimpleDateUtil.getCurrentHourMin();
            if (startCom < currentHourMin && currentHourMin < endCom) {
                backNum = tempAddEntity.getAddTemp();
                break;
            }
        }
        LogUtil.cdl("====更新温度补偿值===" + backNum);
        SpUtils.setmTempComp(backNum);
    }

    /***
     * 查询列表信息
     * @return
     */
    public static List<TempAddEntity> queryTempComInfo() {
        List<TempAddEntity> txtList = null;
        try {
            txtList = LitePal.findAll(TempAddEntity.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return txtList;
    }

    /***
     * 修改-补偿值
     * @param entity
     * @return
     */
    public static boolean modifyTempComById(TempAddEntity entity) {
        if (entity == null) {
            return false;
        }
        try {
            String sateTimeId = entity.getSateTimeId();
            String startTime = entity.getStartTime();
            String endTime = entity.getEndTime();
            float addTemp = entity.getAddTemp();
            ContentValues values = new ContentValues();
            values.put("startTime", startTime);
            values.put("endTime", endTime);
            values.put("addTemp", addTemp);
            int modifyNum = LitePal.updateAll(TempAddEntity.class, values, "sateTimeId=?", sateTimeId);
            if (modifyNum > 0) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    /***
     * 删除数据库信息
     * @param sateTimeId
     * @return
     */
    public static boolean deleteTempComById(String sateTimeId) {
        try {
            int delNum = LitePal.deleteAll(TempAddEntity.class, "sateTimeId = ?", sateTimeId + "");
            if (delNum < 1) {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }

    /***
     * 添加数据到数据库
     * @param tempAddEntity
     * @return
     */
    public static boolean addTempToLocal(TempAddEntity tempAddEntity) {
        boolean isSave = false;
        if (tempAddEntity == null) {
            return false;
        }
        try {
            isSave = tempAddEntity.save();
        } catch (Exception e) {
            e.printStackTrace();
        }
        LogUtil.db("=====保存=====" + isSave);
        return isSave;
    }

}
