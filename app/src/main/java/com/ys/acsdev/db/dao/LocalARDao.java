package com.ys.acsdev.db.dao;

import com.ys.acsdev.bean.LocalARBean;
import com.ys.acsdev.commond.AppInfo;
import com.ys.acsdev.util.LogUtil;

import org.litepal.LitePal;

import java.io.File;
import java.util.List;


/***
 * 局域网离线同行记录--数据库操作类
 * 通行数据保存表
 */
public class LocalARDao {

    /***
     * 保存数据
     * @param localARBean
     * @return
     */
    public static boolean saveLocalBean(LocalARBean localARBean) {
        if (localARBean == null) {
            LogUtil.db("=====数据库==保存数据库===拦截=");
            return false;
        }
        int maxNum = 20000;
        int currentNum = AppInfo.CURRENT_SAVE_PERSON_NUM;
        LogUtil.db("=====数据库数据====" + currentNum);
        if (currentNum > maxNum) {
            LogUtil.db("=====数据库已经满了，准备删除数据====");
            LocalARBean firstInfo = LitePal.findFirst(LocalARBean.class);
            if (firstInfo == null) {
                LogUtil.db("====数据库已经满了=获取第一条数据失败====");
                return false;
            }
            boolean isDel = delLocalBeanBy(firstInfo);
            LogUtil.db("===数据库已经满了==删除第一条数据状态====" + isDel);
            if (isDel) {
                LogUtil.db("====数据库已经满了=准备添加新数据====");
                AppInfo.CURRENT_SAVE_PERSON_NUM = AppInfo.CURRENT_SAVE_PERSON_NUM + 1;
                return localARBean.save();
            }
            return false;
        } else {
            LogUtil.db("=====数据库保存的数量====" + currentNum);
            AppInfo.CURRENT_SAVE_PERSON_NUM = AppInfo.CURRENT_SAVE_PERSON_NUM + 1;
            return localARBean.save();
        }
    }

    /***
     * 删除数据
     * @param localARBean
     * @return
     */
    public static boolean delLocalBeanBy(LocalARBean localARBean) {
        if (localARBean == null) {
            return false;
        }
        String recordTime = localARBean.getRecordTime();
        String filePath = localARBean.getFilePath();
        try {
            int delNum = LitePal.deleteAll(LocalARBean.class, "recordTime = ?", recordTime);
            AppInfo.CURRENT_SAVE_PERSON_NUM = AppInfo.CURRENT_SAVE_PERSON_NUM - 1;
            if (delNum < 1) {
                return false;
            }
            File fileDel = new File(filePath);
            if (fileDel.exists()) {
                boolean isDel = fileDel.delete();
                LogUtil.db("=========删除文件夹状态===" + isDel);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }

    public static List<LocalARBean> getAll() {
        return LitePal.findAll(LocalARBean.class);
    }

    public static void delAll() {
        LitePal.deleteAll(LocalARBean.class);
        AppInfo.CURRENT_SAVE_PERSON_NUM = 0;
    }

}
