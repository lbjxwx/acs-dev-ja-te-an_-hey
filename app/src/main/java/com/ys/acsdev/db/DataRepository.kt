package com.ys.acsdev.db

import android.graphics.Point
import com.huiming.common.ext.async
import com.hwangjr.rxbus.RxBus
import com.yisheng.facerecog.net.HttpObserver
import com.ys.acsdev.api.ApiHelper
import com.ys.acsdev.attendance.ClockInHelper
import com.ys.acsdev.attendance.bean.ClockInEntity
import com.ys.acsdev.attendance.dao.ClockInDao
import com.ys.acsdev.bean.SyncAttendEvent
import com.ys.acsdev.service.AcsService
import com.ys.acsdev.util.LogUtil

class DataRepository {

    companion object {
        @JvmStatic
        val instance: DataRepository by lazy { DataRepository() }
    }

    var mSupportedPreview: MutableList<Point> = arrayListOf()
    private var mLocalAttendData = arrayListOf<ClockInEntity>()
    var mSuccessCount = 0
    var mFailedCount = 0


    /***
     * 同步考勤
     */
    fun syncAttendRecord() {
        AcsService.getInstance()?.executor {
            mSuccessCount = 0
            mFailedCount = 0
            mLocalAttendData.clear()
            val data = ClockInDao.getAllClockIn()
            mLocalAttendData.addAll(data)
            updateNextRecorder()
        }
    }

    fun updateNextRecorder() {
        if (mLocalAttendData == null || mLocalAttendData.size < 1) {
            RxBus.get().post(SyncAttendEvent(mSuccessCount, mFailedCount))
            return
        }
        val entity = mLocalAttendData.get(0)
        uploadAttendRecordOnByOne(entity)
    }

    fun uploadAttendRecordOnByOne(entity: ClockInEntity) {
        if (entity == null) {
            updateNextRecorder();
            return
        }
        val signParm = ClockInHelper.getSignParm(entity)
        ApiHelper.freeSignIn(signParm).async()
            .subscribe(object : HttpObserver<Any?>() {
                override fun onSuccess(o: Any?) {
                    mSuccessCount++
                    LogUtil.attendance("同步考勤success")
                    ClockInDao.deleteClock(entity)
                    mLocalAttendData.remove(entity)
                    updateNextRecorder()
                }

                override fun onFailed(code: Int, msg: String) {
                    mFailedCount++
                    LogUtil.attendance("同步考勤failed=" + msg)
                    mLocalAttendData.remove(entity)
                    updateNextRecorder()
                }
            })
    }


    private fun log(msg: String) {
        LogUtil.e(msg, "DataRepository")
    }

}