//package com.ys.acsdev.db.dao;
//
//import android.content.ContentValues;
//
//import com.ys.acsdev.bean.AskForbidenEntity;
//import com.ys.acsdev.util.LogUtil;
//
//import org.litepal.LitePal;
//
//import java.util.List;
//
//
//public class DbAskForbiden {
//
//    /***
//     * 保存控件数据库
//     * @param entity
//     * @return
//     */
//    public static boolean saveSocketLineStateInfo(AskForbidenEntity entity) {
//        if (entity == null) {
//            return false;
//        }
//        String saveTime = entity.getSaveTime();
//        try {
//            List<AskForbidenEntity> cpList = LitePal.where("saveTime=?", saveTime).find(AskForbidenEntity.class);
//            if (cpList == null || cpList.size() < 1) {
//                LogUtil.cdl("===0000====没有数据，添加到数据库");
//                return addAskForbidenInfoDb(entity);
//            } else {
//                return modifyAskForBiDenInfo(entity);
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        LogUtil.cdl("===0000====不知道为什么");
//        return false;
//    }
//
//    private static boolean modifyAskForBiDenInfo(AskForbidenEntity entity) {
//        if (entity == null) {
//            return false;
//        }
//        String saveTime = entity.getSaveTime();
//        String requestTxt = entity.getRequestTxt();
//        int answerTxt = entity.getAnswerTxt();
//
//        ContentValues values = new ContentValues();
//        values.put("requestTxt", requestTxt);
//        values.put("answerTxt", answerTxt);
//        int modifyNum = LitePal.updateAll(AskForbidenEntity.class, values, "saveTime=?", saveTime);
//        if (modifyNum > 0) {
//            return true;
//        } else {
//            return false;
//        }
//    }
//
//    /**
//     * 保存数据到数据库
//     *
//     * @param entity
//     * @return
//     */
//    private static boolean addAskForbidenInfoDb(AskForbidenEntity entity) {
//        boolean isSave = false;
//        if (entity == null) {
//            return isSave;
//        }
//        try {
//            isSave = entity.save();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return isSave;
//    }
//
//    public static boolean delPowerInfoByIp(String saveTime) {
//        try {
//            int delNum = LitePal.deleteAll(AskForbidenEntity.class, "saveTime = ?", saveTime);
//            if (delNum < 1) {
//                return false;
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return true;
//    }
//
//    public static List<AskForbidenEntity> getAskForbidenInfoList() {
//        List<AskForbidenEntity> txtList = null;
//        try {
//            txtList = LitePal.findAll(AskForbidenEntity.class);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return txtList;
//    }
//
//    public static void clearAllData() {
//        LitePal.deleteAll(AskForbidenEntity.class);
//    }
//
//}
