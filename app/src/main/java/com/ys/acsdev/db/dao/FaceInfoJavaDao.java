package com.ys.acsdev.db.dao;

import android.content.ContentValues;

import com.ys.acsdev.api.AppConfig;
import com.ys.acsdev.bean.FaceInfo;
import com.ys.acsdev.bean.PersonBean;
import com.ys.acsdev.commond.AppInfo;
import com.ys.acsdev.util.LogUtil;
import com.ys.facepasslib.FaceManager;

import org.litepal.LitePal;

import java.util.ArrayList;
import java.util.List;

/***
 * 人脸
 */
public class FaceInfoJavaDao {

    public static FaceInfo getFaceInfoByToken(String token) {
        List<FaceInfo> faceInfoList = LitePal.where("faceToken=?", token + "").find(FaceInfo.class);
        if (faceInfoList == null || faceInfoList.size() < 1) {
            return null;
        }
        return faceInfoList.get(0);
    }

    public static FaceInfo getFaceInfoById(String id) {
        List<FaceInfo> faceInfoList = LitePal.where("personId=?", id + "").find(FaceInfo.class);
        if (faceInfoList == null || faceInfoList.size() < 1) {
            return null;
        }
        return faceInfoList.get(0);
    }

    public static List<FaceInfo> getFaceInfoByName(String name) {
        return LitePal.where("name=?", name).find(FaceInfo.class);
    }

    /****
     *     //人员管理
     *     public static final int FACE_REGISTER_PERSON = 1;
     *     //访客
     *     public static final int FACE_REGISTER_VISITOR = 2;
     *     //黑名单
     *     public static final int FACE_REGISTER_BLACK_LIST = 3;
     * @param type
     * @return
     */
    public static List<FaceInfo> getAllByType(int type) {
        if (type == -1) {
            return getAllFaceInfo();
        }
        List<FaceInfo> faceInfoList = LitePal.where("type=?", type + "").find(FaceInfo.class);
        if (faceInfoList == null || faceInfoList.size() < 1) {
            return null;
        }
        return faceInfoList;
    }

    public static List<FaceInfo> getAllFaceInfo() {
        return LitePal.findAll(FaceInfo.class);
    }

    public static void insertFaceInfo(FaceInfo faceInfo) {
        if (faceInfo == null) {
            return;
        }
        LogUtil.db("==========注册成功保存到数据库====" + faceInfo.toString());
        faceInfo.saveOrUpdate("personId=?", faceInfo.getPersonId());
    }

    public static boolean modifyFaceName(FaceInfo faceInfo, String name) {
        if (faceInfo == null) {
            return false;
        }
        String personId = faceInfo.getPersonId();
        ContentValues values = new ContentValues();
        values.put("name", name);
        int modifyNum = LitePal.updateAll(FaceInfo.class, values, "personId=?", personId);
        if (modifyNum > 0) {
            return true;
        } else {
            return false;
        }


    }


    /***
     * 同步人员信息
     * @param newData
     */
    public static void syncFaceByPseron(List<PersonBean> newData, FaceInfoDaoListener listener) {
        if (newData == null || newData.size() < 1) {
            listener.syncFaceInfoCompany(false, "原始数据==null");
            return;
        }
        List<String> dataString = new ArrayList<String>();
        for (PersonBean personBean : newData) {
            dataString.add(personBean.getPerId());
        }
        syncFaceInFoLocal(dataString, listener);
    }

    //同步人脸库数据，删除垃圾数据
    public static void syncFaceInFoLocal(List<String> newData, FaceInfoDaoListener listener) {
        List<FaceInfo> faceInfoList = getAllByType(-1);
        if (faceInfoList == null || faceInfoList.size() < 1) {
            listener.syncFaceInfoCompany(false, "原始数据==null");
            return;
        }
        //旧数据与新数据差集，找出需要删除的数据=======================================
        List<String> oldList = new ArrayList<String>();
        for (int i = 0; i < faceInfoList.size(); i++) {
            oldList.add(faceInfoList.get(i).getPersonId());
        }
        oldList.removeAll(newData);
        if (oldList == null || oldList.size() < 1) {
            listener.syncFaceInfoCompany(true, "比对完成,数据一摸一样");
            return;
        }
        for (int i = 0; i < oldList.size(); i++) {
            String personId = oldList.get(i);
            FaceInfo faceInfo = FaceInfoJavaDao.getFaceInfoById(personId);
            FaceManager.getInstance().delFaceByToken(faceInfo.getFaceToken());
            deleteById(personId, "同步人脸信息删除多余得人脸");
        }
        listener.syncFaceInfoCompany(true, "比对完成,删除数据成功");
    }

    public static boolean deleteById(String id, String tag) {
        int delNum = LitePal.deleteAll(FaceInfo.class, "personId = ?", id);
        LogUtil.db("==========根据ID删除人脸信息====" + delNum + " / " + tag);
        if (delNum > 0) {
            return true;
        } else {
            return false;
        }
    }

    public static void deleteByToken(String token) {
        int delNum = LitePal.deleteAll(FaceInfo.class, "faceToken = ?", token);
        LogUtil.db("==========根据FaceToken删除人脸信息====" + delNum);
    }

    public static void deleteAllFaceInfo() {
        delAllFaceByToken(getAllFaceInfo());
        LitePal.deleteAll(FaceInfo.class);
    }

    //删除本地人脸token
    public static void delAllFaceByToken(List<FaceInfo> faceList) {
        if (faceList == null || faceList.size() < 1) {
            return;
        }
        for (FaceInfo faceInfo : faceList) {
            FaceManager.getInstance().delFaceByToken(faceInfo.getFaceToken());
        }
    }

    /****
     * 根据类型来删除人员信息
     * @param type
     *     //人员管理
     *     public static final int FACE_REGISTER_PERSON = 1;
     *     //访客
     *     public static final int FACE_REGISTER_VISITOR = 2;
     *     //黑名单
     *     public static final int FACE_REGISTER_BLACK_LIST = 3;
     */
    public static void delAllFaceInfoByType(int type, String tag) {
        if (type < AppInfo.FACE_REGISTER_PERSON) {
            int delNum = LitePal.deleteAll(FaceInfo.class);
            LogUtil.db("==========根据type删除人脸信息====" + delNum + " / " + tag);
            return;
        }
        int delNum = LitePal.deleteAll(FaceInfo.class, "type = ?", type + "");
        LogUtil.db("==========根据type删除人脸信息====" + delNum + " / " + tag);
    }


    public interface FaceInfoDaoListener {
        void syncFaceInfoCompany(boolean isTrue, String errorDesc);
    }


}
