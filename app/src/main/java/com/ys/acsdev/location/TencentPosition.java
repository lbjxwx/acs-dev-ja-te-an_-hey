package com.ys.acsdev.location;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.widget.Toast;

import com.device.PageTool;
import com.tencent.map.geolocation.TencentLocation;
import com.tencent.map.geolocation.TencentLocationListener;
import com.tencent.map.geolocation.TencentLocationManager;
import com.tencent.map.geolocation.TencentLocationRequest;


public class TencentPosition implements TencentLocationListener {

    //定位管理
    private TencentLocationManager mLocationManager;
    //定位请求
    private TencentLocationRequest locationRequest;

    public static final String TAG = "TencentPosition";

    private Context context;
    private OnLocationListener listener;

    public TencentPosition(Context context, OnLocationListener listener) {
        this.context = context;
        this.listener = listener;

        //实例化
        initLocation();

    }

    public void getPosition()
    {
        mLocationManager.requestSingleFreshLocation(locationRequest,this, Looper.getMainLooper());
    }

    /**
     * 初始化定位信息
     */
    private void initLocation() {
        //获取TencentLocationManager实例
        mLocationManager = TencentLocationManager.getInstance(context);
        //获取定位请求TencentLocationRequest 实例
        locationRequest = TencentLocationRequest.create();
        //设置定位时间间隔，10s
        locationRequest.setInterval(1000);
        //位置信息的详细程度 REQUEST_LEVEL_ADMIN_AREA表示获取经纬度，位置所处的中国大陆行政区划
        locationRequest.setRequestLevel(TencentLocationRequest.REQUEST_LEVEL_ADMIN_AREA);
        //是否允许使用GPS
        locationRequest.setAllowGPS(true);
        //是否需要获取传感器方向，提高室内定位的精确度
        locationRequest.setAllowDirection(true);
        //是否需要开启室内定位
        locationRequest.setIndoorLocationMode(true);
    }

    /**
     * 接收定位结果
     */
    @Override
    public void onLocationChanged(TencentLocation location, int error, String reason) {
        if (listener != null) {
            listener.onLocationChanged(location);
        }
        System.out.println("location " + error + " | " + reason);
        mLocationManager.removeUpdates(this);
    }

    /**
     * 用于接收GPS、WiFi、Cell状态码
     */
    @Override
    public void onStatusUpdate(String name, int status, String desc) {
        Log.d(TAG, "name：" + name + " desc：" + statusUpdate(name, status));
    }

    /**
     * Toast提示
     *
     * @param msg 内容
     */
    private void showMsg(String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }

    /**
     * 定位状态判断
     *
     * @param name   GPS、WiFi、Cell
     * @param status 状态码
     * @return
     */
    private String statusUpdate(String name, int status) {
        if ("gps".equals(name)) {
            switch (status) {
                case 0:
                    return "GPS开关关闭";
                case 1:
                    return "GPS开关打开";
                case 3:
                    return "GPS可用，代表GPS开关打开，且搜星定位成功";
                case 4:
                    return "GPS不可用";
                default:
                    return "";
            }
        } else if ("wifi".equals(name)) {
            switch (status) {
                case 0:
                    return "Wi-Fi开关关闭";
                case 1:
                    return "Wi-Fi开关打开";
                case 2:
                    return "权限被禁止，禁用当前应用的 ACCESS_COARSE_LOCATION 等定位权限";
                case 5:
                    return "位置信息开关关闭，在android M系统中，此时禁止进行Wi-Fi扫描";
                default:
                    return "";
            }
        } else if ("cell".equals(name)) {
            switch (status) {
                case 0:
                    return "cell 模块关闭";
                case 1:
                    return "cell 模块开启";
                case 2:
                    return "定位权限被禁止，位置权限被拒绝通常发生在禁用当前应用的 ACCESS_COARSE_LOCATION 等定位权限";
                default:
                    return "";
            }
        }
        return "";
    }
}