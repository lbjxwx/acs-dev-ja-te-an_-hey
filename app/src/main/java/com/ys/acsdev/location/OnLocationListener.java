package com.ys.acsdev.location;

import com.tencent.map.geolocation.TencentLocation;

public interface OnLocationListener {
    void onLocationChanged(TencentLocation location);
}
