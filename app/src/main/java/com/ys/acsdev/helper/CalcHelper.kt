package com.ys.acsdev.helper

// Created by kermitye on 2020/10/20 15:05
object CalcHelper {

    @JvmStatic
    fun avg(data: List<Float>): Float {
        return data.average().toFloat()
    }


    @JvmStatic
    fun calcCaliAvg(data: List<Float>): Float {
        return data.map { 36.5 - it }.average().toFloat()
    }

}