package com.ys.acsdev.helper;

import android.os.Handler;
import android.os.Message;

import com.ys.acsdev.MyApp;
import com.ys.acsdev.util.GpioUtils;
import com.ys.acsdev.util.LogUtil;
import com.ys.acsdev.util.SystemManagerUtil;

// Created by kermitye on 2019/12/24 20:57
public class CheckCameraHelper {
    private static final int gpIO = 14;

    private static final int MSG_HIGHT = 0;
    private static final int MSG_LOW = 1;
    private static final int MSG_REBOOT = 2;

    private CheckCameraHelper() {
    }

    private static class SingletonHoler {
        public static final CheckCameraHelper INSTANCE = new CheckCameraHelper();
    }

    public static CheckCameraHelper getInstance() {
        return SingletonHoler.INSTANCE;
    }

    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case MSG_HIGHT:
                    LogUtil.e("================拉高", "CheckCamera");
                    setValue("1");
//                    sendEmptyMessageDelayed(MSG_REBOOT, 5 * 1000);
                    break;
                case MSG_LOW:
                    LogUtil.e("================拉低", "CheckCamera");
                    setValue("0");
                    sendEmptyMessageDelayed(MSG_HIGHT, 5*1000);
                    break;
                case MSG_REBOOT:
                    LogUtil.e("================重启", "CheckCamera");
                    destory();
                    SystemManagerUtil.rebootApp(MyApp.getInstance());
                    break;
            }
        }
    };

    public void resetCamera() {
        mHandler.sendEmptyMessage(MSG_LOW);
    }

    public void rebootApp() {
        mHandler.removeMessages(MSG_REBOOT);
        mHandler.sendEmptyMessageDelayed(MSG_REBOOT, 5 * 1000);
    }

    public void destory() {
        mHandler.removeMessages(MSG_LOW);
        mHandler.removeMessages(MSG_HIGHT);
        mHandler.removeMessages(MSG_REBOOT);
    }

    private boolean setValue(String value) {
        LogUtil.e("===============setvalue: " + value, "CheckCameraHelper");
        return GpioUtils.writeGpioValue(gpIO, value);
    }
}
