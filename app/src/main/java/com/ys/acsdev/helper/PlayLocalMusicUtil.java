package com.ys.acsdev.helper;

import android.content.Context;
import android.media.MediaPlayer;

/***
 * 播放提示音工具类
 */
public class PlayLocalMusicUtil {

    Context context;

    public PlayLocalMusicUtil(Context context) {
        this.context = context;
    }

    MediaPlayer mediaPlayer;

    public void startPlayRawMusic(int rawId) {
        if (context == null) {
            return;
        }
        try {
            stopPlayMusic();
            if (mediaPlayer != null) {
                mediaPlayer.reset();//进行重置
            }
            mediaPlayer = MediaPlayer.create(context, rawId);
            mediaPlayer.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void stopPlayMusic() {
        try {
            if (mediaPlayer == null) {
                return;
            }
            if (mediaPlayer.isPlaying()) {
                mediaPlayer.stop();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
