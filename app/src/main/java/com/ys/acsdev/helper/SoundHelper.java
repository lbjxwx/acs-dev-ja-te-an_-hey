package com.ys.acsdev.helper;

import android.media.SoundPool;

import com.ys.acsdev.MyApp;
import com.ys.acsdev.R;

public class SoundHelper {

    static SoundPool soundPool;
    public static int mWarnSound = -1;
    public static int mCardSound = -1;

    public static int mStream = 0;

    public static void init() {
        if (soundPool == null) {
            soundPool = new SoundPool.Builder().setMaxStreams(10).build();
        }
        if (mWarnSound == -1) {
            mWarnSound = soundPool.load(MyApp.getInstance(), R.raw.black_alerm, 1);
        }
        if (mCardSound == -1) {
            mCardSound = soundPool.load(MyApp.getInstance(), R.raw.card, 1);
        }
    }

    public static void playWarn() {
        init();
        mStream = soundPool.play(mWarnSound, 1f, 1f, 1, 0, 1f);
    }

    public static void playCard() {
        try {
            init();
            soundPool.play(mCardSound, 1f, 1f, 1, 0, 1f);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void stopWarn() {
        if (soundPool != null) {
            soundPool.stop(mStream);
        }
    }

    public static void destory() {
        if (soundPool != null) {
            soundPool.release();
        }
    }

}
