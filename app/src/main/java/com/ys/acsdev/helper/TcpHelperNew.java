package com.ys.acsdev.helper;

import com.ys.acsdev.MyApp;
import com.ys.acsdev.commond.AppInfo;
import com.ys.acsdev.listener.SocketMessageListener;
import com.ys.acsdev.socket.OnSocketClientListener;
import com.ys.acsdev.socket.SiteWebsocket;
import com.ys.acsdev.socket.SocketWebListener;
import com.ys.acsdev.util.CodeUtil;
import com.ys.acsdev.util.LogUtil;
import com.ys.acsdev.util.NetWorkUtils;
import com.ys.acsdev.util.SpUtils;

import org.java_websocket.enums.ReadyState;
import org.json.JSONObject;


public class TcpHelperNew implements SocketWebListener {

    public TcpHelperNew() {

    }

    public static final int SERVER_CODE_CONNECTED = 800;
    public static final int SERVER_CODE_DISCONNECT = 801;
    public static final int SERVER_CODE_CONNECTING = 802;
    public static final int MESSAGE_TYPE_ORDER_WEB = 1000;
    //服务器特殊指令
    public static final int MESSAGE_TYPE_SOCKET_DEV = 2000;
    //设备端用来检查心跳得回收消息
    public static final int MSG_CODE_HEART = 4002;

    //    boolean mConnnected = false;
    OnSocketClientListener mListener = null;
    SiteWebsocket mSocket = null;
    String CLOSE_MSG = "   {\"code\":\"2004\",\"msg\":" + CodeUtil.getUniquePsuedoID() + "}";

    public void init(OnSocketClientListener listener) {
        this.mListener = listener;
    }

    public void lineSocketWeb(String printTag) {
        LogUtil.message("=======准备连接TCP==" + printTag);
        if (!NetWorkUtils.isNetworkConnected(MyApp.getInstance())) {
            LogUtil.message("=======网络异常，请检查网络是否连接");
            if (mListener != null) {
                mListener.onError(2, "Network error");
            }
            return;
        }
        if (mSocket == null) {
            LogUtil.message("=======SocketService==初始化==SiteWebsocket");
            mSocket = new SiteWebsocket(this, getUrl());
        }
        if (mListener != null) {
            mListener.onConnecting();
        }
        try {
            ReadyState readyState = mSocket.getReadyState();
            LogUtil.message("---连接服务器状态---" + readyState + " =非空判斷==" + (mSocket == null));
            if (readyState.equals(ReadyState.NOT_YET_CONNECTED)) {
                LogUtil.message("---第一次i链接服务器---");
                if (mSocket != null) {
                    mSocket.connect();
                }
                return;
            }
            LogUtil.message("---重连服务器---");
            if (mSocket != null) {
                mSocket.reconnect();
            }
        } catch (Exception e) {
            e.printStackTrace();
            if (mListener != null) {
                mListener.onError(1, "Line Failed：" + e.toString());
            }
        }
    }


    /***
     * 服务器连接状态
     * @param state
     * @param desc
     */
    @Override
    public void socketState(int state, String desc) {
        if (state == SocketWebListener.SOCKET_CLOSE || state == SocketWebListener.SOCKET_ERROR) {
            LogUtil.message("=======socket断开了=" + state + " /desc = " + desc, true);
            dealDisOnlineDev("socket断开了");
        } else if (state == SocketWebListener.SOCKET_OPEN) {
            LogUtil.message("======socket连接上了");
            AppInfo.isConnectServer = true;
            if (mListener != null) {
                mListener.onConnected();
            }
        }
    }


    /***
     * 断开重连
     */
    private void dissOrReconnect() {
        try {
            LogUtil.message("执行断开重连的方法", true);
            dealDisOnlineDev("断开重连");
            if (mSocket != null) {
                mSocket = null;
            }
            lineSocketWeb("dissOrReconnect");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /***
     * 注销登陆
     * 2：取消hanlder活动
     * 3:下线
     */
    public void dealDisOnlineDev(String s) {
        LogUtil.message("中断和服务器得连接===" + s, true);
        try {
            if (mListener != null) {
                mListener.onDisConnected("No connection");
            }
            AppInfo.isConnectServer = false;
            if (mSocket != null) {
                mSocket.closeLineState();
                mSocket = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 接收到消息
     * 回调运行在子线程中
     */
    @Override
    public void receiverMessage(String message) {
        LogUtil.message("接收到服务器消息: " + message, true);
        if (message == null || !message.contains("{")) {
            return;
        }
        try {
            JSONObject jsonObj = new JSONObject(message);
            int type = jsonObj.getInt("type");
            int code = jsonObj.getInt("code");
            switch (type) {
                case MESSAGE_TYPE_ORDER_WEB:
                    handlerOrderMsg(code, jsonObj); //操控指令
                    break;
                case MESSAGE_TYPE_SOCKET_DEV:
                    handlerSocketMsg(code); //特殊指令
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 处理特殊指令
     */
    private void handlerSocketMsg(int code) {
        switch (code) {
            case MSG_CODE_HEART:
                LogUtil.message("=======收到心跳消息=============");
                if (mListener != null) {
                    mListener.heartStatues(true);
                }
                break;
        }
    }

    SocketMessageListener socketMessageListener;

    public void setReceiverOrderMsgCallBack(SocketMessageListener socketMessageListener) {
        this.socketMessageListener = socketMessageListener;
    }


    /**
     * 处理操控指令
     */
    private void handlerOrderMsg(int code, JSONObject obj) {
        if (socketMessageListener != null) {
            socketMessageListener.messageOrderBack(code, obj);
        }
    }

    /**
     * 发送消息
     */
    public void sendMsg(String msg) {
        try {
            if (mSocket == null) {
                return;
            }
            if (!AppInfo.isConnectServer) {
                return;
            }
            LogUtil.message("发消息给服务器: " + msg, true);
            mSocket.send(msg);
        } catch (Exception e) {
            LogUtil.e("=======SocketService:发消息给服务器error===$e");
            e.printStackTrace();
        }
    }

    public void destory() {
        dealDisOnlineDev("service onDestroy");
        AppInfo.isConnectServer = false;
        if (mSocket == null) {
            return;
        }
        sendMsg(CLOSE_MSG);
        mSocket.close();
        mSocket = null;
    }

    private String getUrl() {
        String devId = CodeUtil.getUniquePsuedoID();
        String mHost = SpUtils.getmServerIp();
        String mPort = SpUtils.getmServerPort();
        String address = "ws://" + mHost + ":" + mPort + "/cloudIntercom/linksocket?" + devId;
        return address;
    }

}
