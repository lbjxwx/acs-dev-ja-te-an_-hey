package com.ys.acsdev.socket;

import java.nio.ByteBuffer;

public interface SocketWebListener {
    int SOCKET_OPEN = 1;
    int SOCKET_INIT = 0;
    int SOCKET_CLOSE = -1;
    int SOCKET_ERROR = -2;

    void socketState(int state, String desc);

    void receiverMessage(String message);

//    void receiverMessage(ByteBuffer message);
}