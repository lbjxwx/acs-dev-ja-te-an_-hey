package com.ys.acsdev.socket

abstract class OnSocketClientListener {
    open fun onConnecting() {}
    open fun onConnected() {}
    open fun onDisConnected(msg: String) {}
    open fun onError(code: Int, msg: String) {}
    open fun heartStatues(isReceiveMessage: Boolean) {}
    abstract fun onRecpData(msg: String)

}