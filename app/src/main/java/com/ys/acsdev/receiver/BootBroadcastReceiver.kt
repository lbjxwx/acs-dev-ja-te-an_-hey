package com.ys.acsdev.receiver

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Handler
import com.ys.acsdev.ui.StartActivity
import com.ys.acsdev.util.LogUtil

/***
 * 开机启动不用这里，直接通过守护进程拉起来，
 * 守护进程可以设置
 */
class BootBroadcastReceiver : BroadcastReceiver() {

    private val ACTION_BOOT = "android.intent.action.BOOT_COMPLETED"

    override fun onReceive(context: Context?, intent: Intent?) {
        val action = intent?.getAction() ?: ""
        if (action == ACTION_BOOT) {
            LogUtil.e("接收到开机广播", "BroadReceiver", true)
            Handler().postDelayed({
                val intent1 = Intent(context, StartActivity::class.java)
                intent1.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                context?.startActivity(intent1)
            }, 8 * 1000)
        }
    }
}