package com.ys.acsdev.bean;

import org.litepal.crud.LitePalSupport;

/**
 * 链接记录
 */
public class SocketEntity extends LitePalSupport {

    String ip;
    String port;
    String userName;

    public SocketEntity(String ip, String port, String userName) {
        this.ip = ip;
        this.port = port;
        this.userName = userName;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @Override
    public String toString() {
        return "SocketEntity{" +
                "ip='" + ip + '\'' +
                ", port='" + port + '\'' +
                ", userName='" + userName + '\'' +
                '}';
    }
}
