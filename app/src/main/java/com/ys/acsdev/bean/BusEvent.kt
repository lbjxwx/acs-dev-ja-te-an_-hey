package com.ys.acsdev.bean

/*
* Created by kermitye on 2019/5/16 15:54
*/
data class LinkServerEvent(var code: Int, var msg: String)

data class CallEvent(var code: Int, var msg: String)

class LogoChangeEvent()

class SyncAttendEvent(var success: Int, var failed: Int)