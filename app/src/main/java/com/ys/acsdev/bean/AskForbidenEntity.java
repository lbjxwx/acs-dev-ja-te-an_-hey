//package com.ys.acsdev.bean;
//
//import org.litepal.crud.LitePalSupport;
//
//public class AskForbidenEntity extends LitePalSupport {
//
//    String saveTime;
//    String requestTxt;
//    int answerTxt;  //1 确认   2：否
//
//    public AskForbidenEntity(String saveTime, String requestTxt, int answerTxt) {
//        this.saveTime = saveTime;
//        this.requestTxt = requestTxt;
//        this.answerTxt = answerTxt;
//    }
//
//    public String getSaveTime() {
//        return saveTime;
//    }
//
//    public void setSaveTime(String saveTime) {
//        this.saveTime = saveTime;
//    }
//
//    public String getRequestTxt() {
//        return requestTxt;
//    }
//
//    public void setRequestTxt(String requestTxt) {
//        this.requestTxt = requestTxt;
//    }
//
//    public int getAnswerTxt() {
//        return answerTxt;
//    }
//
//    public void setAnswerTxt(int answerTxt) {
//        this.answerTxt = answerTxt;
//    }
//
//    @Override
//    public String toString() {
//        return "AskForbidenEntity{" +
//                "saveTime='" + saveTime + '\'' +
//                ", requestTxt='" + requestTxt + '\'' +
//                ", answerTxt=" + answerTxt +
//                '}';
//    }
//}
