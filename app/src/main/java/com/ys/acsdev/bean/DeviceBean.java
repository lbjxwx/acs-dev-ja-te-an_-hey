package com.ys.acsdev.bean;

public class DeviceBean {

    String id;
    String equipName;
    String password;
    String imUserId;
    String appId;
    String deptName;
    String companyName;
    String deptId;

    public DeviceBean(String id, String equipName, String password, String imUserId, String appId, String deptName, String companyName, String deptId) {
        this.id = id;
        this.equipName = equipName;
        this.password = password;
        this.imUserId = imUserId;
        this.appId = appId;
        this.deptName = deptName;
        this.companyName = companyName;
        this.deptId = deptId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEquipName() {
        return equipName;
    }

    public void setEquipName(String equipName) {
        this.equipName = equipName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getImUserId() {
        return imUserId;
    }

    public void setImUserId(String imUserId) {
        this.imUserId = imUserId;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getDeptId() {
        return deptId;
    }

    public void setDeptId(String deptId) {
        this.deptId = deptId;
    }
}
