package com.ys.acsdev.bean;

public class BeanDefaultEntity {

    int defaultNum;
    String defaultDesc;

    public BeanDefaultEntity(int defaultNum, String defaultDesc) {
        this.defaultNum = defaultNum;
        this.defaultDesc = defaultDesc;
    }

    public int getDefaultNum() {
        return defaultNum;
    }

    public void setDefaultNum(int defaultNum) {
        this.defaultNum = defaultNum;
    }

    public String getDefaultDesc() {
        return defaultDesc;
    }

    public void setDefaultDesc(String defaultDesc) {
        this.defaultDesc = defaultDesc;
    }
}
