package com.ys.acsdev.bean;

// Created by kermitye on 2020/7/1 14:03
public class RegisterError {
    public String personId;
    public String failReason;

    public RegisterError(String id, String msg) {
        this.personId = id;
        this.failReason = msg;
    }
}
