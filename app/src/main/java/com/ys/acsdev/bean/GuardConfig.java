package com.ys.acsdev.bean;

public class GuardConfig {
    String pwd;
    //是否保存信息到本地
    int isSaveInfoLocal;  //1:保存  0 不保存

    public GuardConfig(String pwd, int isSaveInfoLocal) {
        this.pwd = pwd;
        this.isSaveInfoLocal = isSaveInfoLocal;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }


    public int getIsSaveInfoLocal() {
        return isSaveInfoLocal;
    }

    public void setIsSaveInfoLocal(int isSaveInfoLocal) {
        this.isSaveInfoLocal = isSaveInfoLocal;
    }
}
