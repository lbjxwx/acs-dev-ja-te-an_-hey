package com.ys.acsdev.bean;

public class FaceConfig {

    String algorithmAngle;
    String cameraDirection;
    String faceAngle;
    String faceImage;
    String frontCameraImage;
    String maskMonitoring;
    String previewAngle;
    String rearCameraImage;
    String recognitionDistance;
    String snapAngle;
    int masksIntercept;  //1 拦截  0：不拦截

    public FaceConfig(String algorithmAngle, String cameraDirection, String faceAngle, String faceImage,
                      String frontCameraImage, String maskMonitoring, String previewAngle, String rearCameraImage,
                      String recognitionDistance, String snapAngle, int masksIntercept) {
        this.algorithmAngle = algorithmAngle;
        this.cameraDirection = cameraDirection;
        this.faceAngle = faceAngle;
        this.faceImage = faceImage;
        this.frontCameraImage = frontCameraImage;
        this.maskMonitoring = maskMonitoring;
        this.previewAngle = previewAngle;
        this.rearCameraImage = rearCameraImage;
        this.recognitionDistance = recognitionDistance;
        this.snapAngle = snapAngle;
        this.masksIntercept = masksIntercept;
    }

    public int getMasksIntercept() {
        return masksIntercept;
    }

    public void setMasksIntercept(int masksIntercept) {
        this.masksIntercept = masksIntercept;
    }

    public String getAlgorithmAngle() {
        return algorithmAngle;
    }

    public void setAlgorithmAngle(String algorithmAngle) {
        this.algorithmAngle = algorithmAngle;
    }

    public String getCameraDirection() {
        return cameraDirection;
    }

    public void setCameraDirection(String cameraDirection) {
        this.cameraDirection = cameraDirection;
    }

    public String getFaceAngle() {
        return faceAngle;
    }

    public void setFaceAngle(String faceAngle) {
        this.faceAngle = faceAngle;
    }

    public String getFaceImage() {
        return faceImage;
    }

    public void setFaceImage(String faceImage) {
        this.faceImage = faceImage;
    }

    public String getFrontCameraImage() {
        return frontCameraImage;
    }

    public void setFrontCameraImage(String frontCameraImage) {
        this.frontCameraImage = frontCameraImage;
    }

    public String getMaskMonitoring() {
        return maskMonitoring;
    }

    public void setMaskMonitoring(String maskMonitoring) {
        this.maskMonitoring = maskMonitoring;
    }

    public String getPreviewAngle() {
        return previewAngle;
    }

    public void setPreviewAngle(String previewAngle) {
        this.previewAngle = previewAngle;
    }

    public String getRearCameraImage() {
        return rearCameraImage;
    }

    public void setRearCameraImage(String rearCameraImage) {
        this.rearCameraImage = rearCameraImage;
    }

    public String getRecognitionDistance() {
        return recognitionDistance;
    }

    public void setRecognitionDistance(String recognitionDistance) {
        this.recognitionDistance = recognitionDistance;
    }

    public String getSnapAngle() {
        return snapAngle;
    }

    public void setSnapAngle(String snapAngle) {
        this.snapAngle = snapAngle;
    }

    @Override
    public String toString() {
        return "FaceConfig{" +
                "algorithmAngle='" + algorithmAngle + '\'' +
                ", cameraDirection='" + cameraDirection + '\'' +
                ", faceAngle='" + faceAngle + '\'' +
                ", faceImage='" + faceImage + '\'' +
                ", frontCameraImage='" + frontCameraImage + '\'' +
                ", maskMonitoring='" + maskMonitoring + '\'' +
                ", previewAngle='" + previewAngle + '\'' +
                ", rearCameraImage='" + rearCameraImage + '\'' +
                ", recognitionDistance='" + recognitionDistance + '\'' +
                ", snapAngle='" + snapAngle + '\'' +
                '}';
    }
}
