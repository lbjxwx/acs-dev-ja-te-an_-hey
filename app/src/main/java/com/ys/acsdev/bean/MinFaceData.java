package com.ys.acsdev.bean;

// Created by kermitye on 2020/6/30 17:16
public class MinFaceData {
    public byte[] data;
    public int width;
    public int height;
    public int rotate;

    public MinFaceData(byte[] data, int width, int height, int rotate) {
        this.data = data;
        this.width = width;
        this.height = height;
        this.rotate = rotate;
    }
}
