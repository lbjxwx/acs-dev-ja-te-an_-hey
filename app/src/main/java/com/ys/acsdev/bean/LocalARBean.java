package com.ys.acsdev.bean;

import org.litepal.crud.LitePalSupport;


/***
 * 离线通行记录--实体类
 */
public class LocalARBean extends LitePalSupport {

    String recordTime;
    String passType = "2";
    String phone;
    String sn;
    String validCode;
    String filePath;
    String temperature;
    String name;
    String cardNum;
    String wearMask;
    String qrCodeType;

    public LocalARBean(String recordTime, String passType, String phone, String sn, String validCode,
                       String filePath, String temperature, String name, String cardNum, String wearMask, String qrCodeType) {
        this.recordTime = recordTime;
        this.passType = passType;
        this.phone = phone;
        this.sn = sn;
        this.validCode = validCode;
        this.filePath = filePath;
        this.temperature = temperature;
        this.name = name;
        this.cardNum = cardNum;
        this.wearMask = wearMask;
        this.qrCodeType = qrCodeType;
    }

    public String getQrCodeType() {
        return qrCodeType;
    }

    public void setQrCodeType(String qrCodeType) {
        this.qrCodeType = qrCodeType;
    }

    public String getWearMask() {
        return wearMask;
    }

    public void setWearMask(String wearMask) {
        this.wearMask = wearMask;
    }

    public String getRecordTime() {
        return recordTime;
    }

    public void setRecordTime(String recordTime) {
        this.recordTime = recordTime;
    }

    public String getPassType() {
        return passType;
    }

    public void setPassType(String passType) {
        this.passType = passType;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getSn() {
        return sn;
    }

    public void setSn(String sn) {
        this.sn = sn;
    }

    public String getValidCode() {
        return validCode;
    }

    public void setValidCode(String validCode) {
        this.validCode = validCode;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getTemperature() {
        return temperature;
    }

    public void setTemperature(String temperature) {
        this.temperature = temperature;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCardNum() {
        return cardNum;
    }

    public void setCardNum(String cardNum) {
        this.cardNum = cardNum;
    }

    @Override
    public String toString() {
        return "LocalARBean{" +
                "recordTime='" + recordTime + '\'' +
                ", passType='" + passType + '\'' +
                ", phone='" + phone + '\'' +
                ", sn='" + sn + '\'' +
                ", validCode='" + validCode + '\'' +
                ", filePath='" + filePath + '\'' +
                ", temperature='" + temperature + '\'' +
                ", name='" + name + '\'' +
                ", cardNum='" + cardNum + '\'' +
                ", isWearMask='" + wearMask + '\'' +
                '}';
    }
}
