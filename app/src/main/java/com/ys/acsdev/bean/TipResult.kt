package com.ys.acsdev.bean

// Created by kermitye on 2020/9/10 11:11
data class TipResult(
    var name: String = "",
    var noMask: Boolean = false,
    var tempType: Int = 0,
    var tempData: String = "",
    var punchState: Boolean = false
)