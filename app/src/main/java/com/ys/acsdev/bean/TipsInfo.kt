package com.ys.acsdev.bean

import com.ys.acsdev.R

// Created by kermitye on 2020/5/27 18:55
data class TipsInfo(var text: String, var bgColor: Int, var temp: String = "", var tempColor: Int = R.color.green)