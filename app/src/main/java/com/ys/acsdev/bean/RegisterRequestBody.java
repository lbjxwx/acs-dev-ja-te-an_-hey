package com.ys.acsdev.bean;

import java.util.List;

// Created by kermitye on 2020/7/1 14:02
public class RegisterRequestBody {
    public String sn;
    public RegisterError[] faceInfos;

    public RegisterRequestBody() {
    }

    public RegisterRequestBody(String sn, RegisterError[] data) {
        this.sn = sn;
        this.faceInfos = data;
    }
}
