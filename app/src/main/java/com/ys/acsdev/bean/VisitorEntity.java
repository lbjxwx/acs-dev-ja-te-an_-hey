//package com.ys.acsdev.bean;
//
//import org.litepal.crud.LitePalSupport;
//
//public class VisitorEntity extends LitePalSupport {
//
//    String empId;
//    String name;
//    String faceFilePath;
//    String validTime;
//    String desc;   //注册成功之后，添加一个备注
//    String fileSize;
//
//    public VisitorEntity(String empId, String name,
//                         String faceFilePath, String validTime, String fileSize) {
//        this.empId = empId;
//        this.name = name;
//        this.faceFilePath = faceFilePath;
//        this.validTime = validTime;
//        this.fileSize = fileSize;
//    }
//
//    public String getFileSize() {
//        return fileSize;
//    }
//
//    public void setFileSize(String fileSize) {
//        this.fileSize = fileSize;
//    }
//
//    public String getEmpId() {
//        return empId;
//    }
//
//    public void setEmpId(String empId) {
//        this.empId = empId;
//    }
//
//    public String getName() {
//        return name;
//    }
//
//    public void setName(String name) {
//        this.name = name;
//    }
//
//    public String getFaceFilePath() {
//        return faceFilePath;
//    }
//
//    public void setFaceFilePath(String faceFilePath) {
//        this.faceFilePath = faceFilePath;
//    }
//
//    public String getValidTime() {
//        return validTime;
//    }
//
//    public void setValidTime(String validTime) {
//        this.validTime = validTime;
//    }
//
//    public String getVisitorDesc() {
//        return desc;
//    }
//
//    public void setVisitorDesc(String desc) {
//        this.desc = desc;
//    }
//
//    @Override
//    public String toString() {
//        return "VisitorEntity{" +
//                "empId='" + empId + '\'' +
//                ", name='" + name + '\'' +
//                ", faceFilePath='" + faceFilePath + '\'' +
//                ", validTime='" + validTime + '\'' +
//                ", desc='" + desc + '\'' +
//                ", fileSize='" + fileSize + '\'' +
//                '}';
//    }
//}
