package com.ys.acsdev.bean;

import org.litepal.crud.LitePalSupport;

public class FaceInfo extends LitePalSupport {

    String name;
    String faceToken;
    String personId;
    String path;
    int type = 1;  //1 是员工  2：访客  3:黑名单
    String photoUrl;   //保存在本地的路径
    String downFileUrl;


    public FaceInfo(String name, String faceToken, String personId, String path, int type, String photoUrl, String downFileUrl) {
        this.name = name;
        this.faceToken = faceToken;
        this.personId = personId;
        this.path = path;
        this.type = type;
        this.photoUrl = photoUrl;
        this.downFileUrl = downFileUrl;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public String getDownFileUrl() {
        return downFileUrl;
    }

    public void setDownFileUrl(String downFileUrl) {
        this.downFileUrl = downFileUrl;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFaceToken() {
        return faceToken;
    }

    public void setFaceToken(String faceToken) {
        this.faceToken = faceToken;
    }

    public String getPersonId() {
        return personId;
    }

    public void setPersonId(String personId) {
        this.personId = personId;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "FaceInfo{" +
                "name='" + name + '\'' +
                ", faceToken='" + faceToken + '\'' +
                ", personId='" + personId + '\'' +
                ", path='" + path + '\'' +
                ", type=" + type +
                '}';
    }
}
