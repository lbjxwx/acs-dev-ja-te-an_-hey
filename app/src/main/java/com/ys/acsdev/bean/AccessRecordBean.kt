package com.ys.acsdev.bean

// Created by kermitye on 2020/3/14 15:42
data class AccessRecordBean(
    var name: String? = "",
    var photoUrl: String? = "",
    var passType: String = "",
    var cardNum: String? = "",
    var temperature: String? = "",
    var recordTime: String? = "",
    var wearMask: String? = "0",
    var userName: String? = "",
    var idCardNum: String? = ""
)