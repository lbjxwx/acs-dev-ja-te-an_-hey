package com.ys.acsdev.bean;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.Nullable;

import org.litepal.crud.LitePalSupport;

public class PersonBean extends LitePalSupport implements Parcelable {

    int type;       //人员类型 1：人员  2：访客  3：黑名单
    String perId;      //	户主id
    String cardNum;   //	卡号
    String name;           //	姓名
    String phone;      //	手机
    String photoUrl;    //	人脸路径
    String syncTime;   //同步时间
    String desc;    //注册信息
    String photoSize;  //文件得大小
    String idCard; //身份证号
    boolean isRegister;  //是否注册
    float backTemp;


    public PersonBean(int type, String perId, String cardNum, String name, String phone, String photoUrl, String syncTime, String desc, String photoSize, String idCard) {
        this.type = type;
        this.perId = perId;
        this.cardNum = cardNum;
        this.name = name;
        this.phone = phone;
        this.photoUrl = photoUrl;
        this.syncTime = syncTime;
        this.desc = desc;
        this.photoSize = photoSize;
        this.idCard = idCard;
    }

    public float getBackTemp() {
        return backTemp;
    }

    public void setBackTemp(float backTemp) {
        this.backTemp = backTemp;
    }

    public PersonBean() {
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

    public boolean isRegister() {
        return isRegister;
    }

    public void setRegister(boolean register) {
        isRegister = register;
    }

    public String getPhotoSize() {
        return photoSize;
    }

    public void setPhotoSize(String photoSize) {
        this.photoSize = photoSize;
    }

    public String getRegisterDesc() {
        return desc;
    }

    public void setRegisterDesc(String desc) {
        this.desc = desc;
    }

    public String getPerId() {
        return perId;
    }

    public void setPerId(String perId) {
        this.perId = perId;
    }

    public String getCardNum() {
        return cardNum;
    }

    public void setCardNum(String cardNum) {
        this.cardNum = cardNum;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public String getSyncTime() {
        return syncTime;
    }

    public void setSyncTime(String syncTime) {
        this.syncTime = syncTime;
    }


    @Override
    public boolean equals(@Nullable Object obj) {
        if (obj instanceof PersonBean) {
            return this.perId.equals(((PersonBean) obj).perId);
        } else {
            return super.equals(obj);
        }
    }

    @Override
    public String toString() {
        return "PersonBean{" +
                "type='" + type + '\'' +
                "perId='" + perId + '\'' +
                ", cardNum='" + cardNum + '\'' +
                ", name='" + name + '\'' +
                ", phone='" + phone + '\'' +
                ", photoUrl='" + photoUrl + '\'' +
                ", syncTime='" + syncTime + '\'' +
                ", desc='" + desc + '\'' +
                ", photoSize='" + photoSize + '\'' +
                ", isRegister=" + isRegister +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.perId);
        dest.writeString(this.cardNum);
        dest.writeString(this.name);
        dest.writeString(this.phone);
        dest.writeString(this.photoUrl);
        dest.writeString(this.syncTime);
        dest.writeString(this.desc);
        dest.writeString(this.photoSize);
        dest.writeString(this.idCard);
    }

    protected PersonBean(Parcel in) {
        this.perId = in.readString();
        this.cardNum = in.readString();
        this.name = in.readString();
        this.phone = in.readString();
        this.photoUrl = in.readString();
        this.syncTime = in.readString();
        this.desc = in.readString();
        this.photoSize = in.readString();
        this.idCard = in.readString();
    }

    public static final Creator<PersonBean> CREATOR = new Creator<PersonBean>() {
        @Override
        public PersonBean createFromParcel(Parcel source) {
            return new PersonBean(source);
        }

        @Override
        public PersonBean[] newArray(int size) {
            return new PersonBean[size];
        }
    };
}