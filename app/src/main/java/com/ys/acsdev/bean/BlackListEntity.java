//package com.ys.acsdev.bean;
//
//import org.litepal.crud.LitePalSupport;
//
///***
// * 黑名单实体类
// */
//public class BlackListEntity extends LitePalSupport {
//
//    String perId;
//    String fileSize;
//    String blackPersonUrl;
//    String name;
//    String syncTime;   //同步时间
//
//    public BlackListEntity(String perId, String fileSize, String blackPersonUrl, String name) {
//        this.perId = perId;
//        this.fileSize = fileSize;
//        this.blackPersonUrl = blackPersonUrl;
//        this.name = name;
//    }
//
//    public String getSyncTime() {
//        return syncTime;
//    }
//
//    public void setSyncTime(String syncTime) {
//        this.syncTime = syncTime;
//    }
//
//    public String getPersonId() {
//        return perId;
//    }
//
//    public void setPersonId(String perId) {
//        this.perId = perId;
//    }
//
//    public String getFileSize() {
//        return fileSize;
//    }
//
//    public void setFileSize(String fileSize) {
//        this.fileSize = fileSize;
//    }
//
//    public String getBlackPersonUrl() {
//        return blackPersonUrl;
//    }
//
//    public void setBlackPersonUrl(String blackPersonUrl) {
//        this.blackPersonUrl = blackPersonUrl;
//    }
//
//    public String getName() {
//        return name;
//    }
//
//    public void setName(String name) {
//        this.name = name;
//    }
//
//    @Override
//    public String toString() {
//        return "BlackListEntity{" +
//                "fileSize='" + fileSize + '\'' +
//                ", blackPersonUrl='" + blackPersonUrl + '\'' +
//                ", name='" + name + '\'' +
//                '}';
//    }
//}
