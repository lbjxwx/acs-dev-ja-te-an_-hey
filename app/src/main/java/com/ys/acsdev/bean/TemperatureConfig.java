package com.ys.acsdev.bean;

//temperatureAlarm 发热温度

public class TemperatureConfig {
    String temperatureBtr;
    String temperatureCompensation;
    String temperatureIntervalTime;
    String temperatureSmp;
    String thermometricPeriphera;
    String temperatureAlarm;
    int temperatureUnit;  //0 摄氏度   1：华氏度

    public TemperatureConfig(String temperatureBtr, String temperatureCompensation, String temperatureIntervalTime,
                             String temperatureSmp, String thermometricPeriphera, String temperatureAlarm, int temperatureUnit) {
        this.temperatureBtr = temperatureBtr;
        this.temperatureCompensation = temperatureCompensation;
        this.temperatureIntervalTime = temperatureIntervalTime;
        this.temperatureSmp = temperatureSmp;
        this.thermometricPeriphera = thermometricPeriphera;
        this.temperatureAlarm = temperatureAlarm;
        this.temperatureUnit = temperatureUnit;
    }

    public int getTemperatureUnit() {
        return temperatureUnit;
    }

    public void setTemperatureUnit(int temperatureUnit) {
        this.temperatureUnit = temperatureUnit;
    }

    public String getTemperatureBtr() {
        return temperatureBtr;
    }

    public void setTemperatureBtr(String temperatureBtr) {
        this.temperatureBtr = temperatureBtr;
    }

    public String getTemperatureCompensation() {
        return temperatureCompensation;
    }

    public void setTemperatureCompensation(String temperatureCompensation) {
        this.temperatureCompensation = temperatureCompensation;
    }

    public String getTemperatureIntervalTime() {
        return temperatureIntervalTime;
    }

    public void setTemperatureIntervalTime(String temperatureIntervalTime) {
        this.temperatureIntervalTime = temperatureIntervalTime;
    }

    public String getTemperatureSmp() {
        return temperatureSmp;
    }

    public void setTemperatureSmp(String temperatureSmp) {
        this.temperatureSmp = temperatureSmp;
    }

    public String getThermometricPeriphera() {
        return thermometricPeriphera;
    }

    public void setThermometricPeriphera(String thermometricPeriphera) {
        this.thermometricPeriphera = thermometricPeriphera;
    }

    public String getTemperatureAlarm() {
        return temperatureAlarm;
    }

    public void setTemperatureAlarm(String temperatureAlarm) {
        this.temperatureAlarm = temperatureAlarm;
    }

    @Override
    public String toString() {
        return "TemperatureConfig{" +
                "temperatureBtr='" + temperatureBtr + '\'' +
                ", temperatureCompensation='" + temperatureCompensation + '\'' +
                ", temperatureIntervalTime='" + temperatureIntervalTime + '\'' +
                ", temperatureSmp='" + temperatureSmp + '\'' +
                ", thermometricPeriphera='" + thermometricPeriphera + '\'' +
                ", temperatureAlarm='" + temperatureAlarm + '\'' +
                '}';
    }
}
