package com.ys.acsdev.bean;

public class CheckEntry {
    public String type;
    public String name;
    public boolean check;

    public CheckEntry(String type, String name, boolean check) {
        this.type = type;
        this.name = name;
        this.check = check;
    }
}
