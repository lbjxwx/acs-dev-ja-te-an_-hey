//package com.ys.acsdev.protocol
//
//import com.huiming.huimingstore.ext.getResourceStr
//import com.kongqw.serialportlibrary.SerialPortManager
//import com.kongqw.serialportlibrary.listener.OnSerialPortDataListener
//import com.ys.acsdev.R
//import com.ys.acsdev.util.CodeUtil
//import com.ys.acsdev.util.LogUtil
//import com.ys.acsdev.util.ToolUtils
//import java.io.File
//import java.lang.Exception
//
//class KeyCardManager {
//
//    companion object {
//        val instance: KeyCardManager by lazy { KeyCardManager() }
//        val TYPE_ERROR = -1
//        val TYPE_KEY = 1
//        val TYPE_CARD = 2
//    }
//
//    val DEVICE_TTYS1 = "/dev/ttyS1"
//    val DEVICE_TTYS2 = "/dev/ttyS2"
//    val DEVICE_TTYS3 = "/dev/ttyS3"
//    val DEVICE_TTYS4 = "/dev/ttyS4"
//
//    val mSerialPort by lazy { SerialPortManager() }
//    val mBaudrate = 9600    //波率
//    var mDeviceType = DEVICE_TTYS3  //串口
//
//    var mlistener: KeyCardListener? = null
//    var mIsOpen = false
//
//
//    fun initKeyCardManager(callBack: ((success: Boolean, msg: String) -> Unit)? = null) {
//        //添加串口数据通信监听
//        mSerialPort.setOnSerialPortDataListener(object : OnSerialPortDataListener {
//            override fun onDataReceived(bytes: ByteArray?) {
//                if (bytes == null)
//                    return
//                log("=======接收到串口数据: ${ToolUtils.toHexString(bytes)}")
//                //接收到数据
//                //解析数据
//                if (bytes != null)
//                    parseData(bytes)
//            }
//
//            override fun onDataSent(bytes: ByteArray?) {
//                //发送数据
//                log("=======发送数据: ${ToolUtils.toHexString(bytes)}")
//            }
//        })
//        mIsOpen = mSerialPort.openSerialPort(File(mDeviceType), mBaudrate)
//        if (!mIsOpen) {
//            LogUtil.cardManager("====erro: 刷卡功能初始化失败", true)
//            callBack?.let { it(false, getResourceStr(R.string.read_card_init_failed)) }
////            mOpenListener?.onFailed(-2, "打开串口失败")
//        } else {
//            LogUtil.cardManager("刷卡功能初始化成功", true)
//            callBack?.let { it(true, getResourceStr(R.string.read_card_init_success)) }
//        }
//    }
//
//    fun setResultListener(listener: KeyCardListener?) {
//        mlistener = listener
//    }
//
//    fun parseData(data: ByteArray) {
//        if (data.size < 8 || data[0].toInt() != 2) {
//            //数据格式错误
//            mlistener?.onKeyCardResult("非法数据", TYPE_ERROR)
//            return
//        }
//        val type = data[1].toInt()
//        when (type) {
//            1 -> parseKey(data) //按键
//            2 -> parseCard(data)    //刷卡
//        }
//    }
//
//    fun parseKey(data: ByteArray) {
//        try {
//            val content = data[6].toInt()
//            log("==========按键： $content ${mlistener == null}")
//            mlistener?.onKeyCardResult(content.toString(), TYPE_KEY)
//        } catch (e: Exception) {
//            e.printStackTrace()
//        }
//    }
//
//    fun parseCard(data: ByteArray) {
//        try {
//            val context = data.copyOfRange(2, 2 + 4)
//            val result = CodeUtil.decode(context)
////            val hexValue = ToolUtils.toHexString(context)//.replace(" ", "").toUpperCase()
////            val result = java.lang.Long.parseLong(hexValue, 16)
//            log("==========刷卡： $result")
//            mlistener?.onKeyCardResult(result, TYPE_CARD)
//        } catch (e: Exception) {
//            e.printStackTrace()
//        }
//    }
//
//    fun destory() {
//        mlistener = null
//        mSerialPort.closeSerialPort()
//    }
//
//    fun log(msg: String) {
//        LogUtil.e(msg, "CardManager")
//    }
//
//}
//
//interface KeyCardListener {
//    fun onKeyCardResult(result: String, type: Int)
//}