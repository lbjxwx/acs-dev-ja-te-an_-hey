package com.ys.acsdev.poweronoff.util;

import android.annotation.SuppressLint;


import com.ys.acsdev.poweronoff.entity.TimeEntity;

import java.text.SimpleDateFormat;
import java.util.Date;

@SuppressLint({"SimpleDateFormat"})
public class SimpleDateUtil {

    /***
     * 定时开关机用来解析时间的
     * @param timeLong
     * @return
     */
    public static TimeEntity getFormatLongTime(long timeLong) {
        String time = String.valueOf(timeLong);
        int year = Integer.parseInt(time.substring(0, 4));
        int month = Integer.parseInt(time.substring(4, 6));
        int day = Integer.parseInt(time.substring(6, 8));
        int hour = Integer.parseInt(time.substring(8, 10));
        int min = Integer.parseInt(time.substring(10, 12));
        TimeEntity entity = new TimeEntity(year, month, day, hour, min);
        return entity;
    }


    /**
     * 将long转化成时间戳
     *
     * @param time
     * @return
     */
    public static long StringToLongTimePower(String time) {
        long timeBack = -1;
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmss");
        try {
            Date dateStart = format.parse(time);
            timeBack = dateStart.getTime();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return timeBack;
    }


    public static String formatCurrentTime() {
        long time = System.currentTimeMillis();
        return new SimpleDateFormat("yyyy-MM-dd HH:mm").format(Long.valueOf(time));
    }


    /***
     * 将时间戳转换成时间数据
     * @param paramLong
     * @return
     */
    public static long formatBig(long paramLong) {
        return Long.parseLong(new SimpleDateFormat("yyyyMMddHHmmss").format(Long.valueOf(paramLong)));
    }

    public static long getCurrentTimelONG() {
        long time = System.currentTimeMillis();
        String timeBack = new SimpleDateFormat("yyyyMMddHHmmss").format(Long.valueOf(time));
        long timeBackLong = Long.parseLong(timeBack);
        return timeBackLong;
    }

}
