package com.ys.acsdev.poweronoff;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;

import com.ys.acsdev.R;
import com.ys.acsdev.poweronoff.adapter.PowerSaveAdapter;
import com.ys.acsdev.poweronoff.db.PowerOnOffLogDb;
import com.ys.acsdev.poweronoff.entity.PoOnOffLogEntity;
import com.ys.acsdev.ui.BaseActivity;
import com.ys.acsdev.view.MyToastView;

import java.util.ArrayList;
import java.util.List;

public class PowerInOffLogActivity extends BaseActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle paramBundle) {
        super.onCreate(paramBundle);
        setContentView(R.layout.activity_hiddle_view);
        initView();
    }

    ListView list_info;
    List<PoOnOffLogEntity> list_log = new ArrayList<>();
    PowerSaveAdapter adapter;
    private Button btn_clear;
    ImageView mIvBack;

    private void initView() {
        list_info = (ListView) findViewById(R.id.list_hiddle);
        adapter = new PowerSaveAdapter(this, list_log);
        list_info.setAdapter(adapter);
        btn_clear = (Button) findViewById(R.id.btn_clear);
        btn_clear.setOnClickListener(this);
        mIvBack = (ImageView) findViewById(R.id.mIvBack);
        mIvBack.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.mIvBack:
                finish();
                break;
            case R.id.btn_clear:
                PowerOnOffLogDb.clearAllData();
                getDataInfo();
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        getDataInfo();
    }

    private void getDataInfo() {
        list_log.clear();
        list_log.add(new PoOnOffLogEntity(getString(R.string.off_time), getString(R.string.open_time), getString(R.string.save_time)));
        list_log = PowerOnOffLogDb.getPowerInfoList();
        adapter.setList(list_log);
        if (list_log.size() < 1) {
            MyToastView.getInstance().Toast(PowerInOffLogActivity.this, getString(R.string.no_data));
        }
    }
}
