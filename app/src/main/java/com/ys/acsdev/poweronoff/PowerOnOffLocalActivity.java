package com.ys.acsdev.poweronoff;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.ys.acsdev.R;
import com.ys.acsdev.commond.AppInfo;
import com.ys.acsdev.poweronoff.adapter.PowerOnOffLocalAdapter;
import com.ys.acsdev.poweronoff.db.DbTimerLocalUtil;
import com.ys.acsdev.poweronoff.db.TimeLocalEntity;
import com.ys.acsdev.poweronoff.util.system.PowerOnOffUtil;
import com.ys.acsdev.ui.BaseActivity;
import com.ys.acsdev.view.MyToastView;
import com.ys.acsdev.view.WaitDialogUtil;
import com.ys.acsdev.view.dialog.OridinryDialog;
import com.ys.acsdev.view.dialog.OridinryDialogClick;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/***
 * 单机版本的定时开关机
 */
public class PowerOnOffLocalActivity extends BaseActivity implements View.OnClickListener,
        AdapterView.OnItemClickListener, AdapterView.OnItemLongClickListener {

    ListView lv_power;
    TextView tv_show_time;
    PowerOnOffLocalAdapter adapter;
    LinearLayout iv_no_data;
    List<TimeLocalEntity> list = new ArrayList<TimeLocalEntity>();
    PowerOnOffUtil powerOnOffUtil;
    private Handler handler = new Handler();

    @Override
    protected void onCreate(Bundle paramBundle) {
        super.onCreate(paramBundle);
        setContentView(R.layout.fragment_poweronoff_local);
        initView();
        getLocalDbData();
    }

    TextView tv_mon, tv_tue, tv_wed, tv_thu, tv_fri, tv_sat, tv_sun;
    Button btn_add_time, btn_clear_time, btn_show_log;
    OridinryDialog oridinryDialog;
    WaitDialogUtil waitDialogUtil;
    ImageView mIvBack;

    private void initView() {
        AppInfo.startCheckTaskTag = false;
        waitDialogUtil = new WaitDialogUtil(PowerOnOffLocalActivity.this);
        powerOnOffUtil = new PowerOnOffUtil(PowerOnOffLocalActivity.this);
        mIvBack = (ImageView) findViewById(R.id.mIvBack);
        mIvBack.setOnClickListener(this);

        tv_mon = (TextView) findViewById(R.id.tv_mon);
        tv_tue = (TextView) findViewById(R.id.tv_tue);
        tv_wed = (TextView) findViewById(R.id.tv_wed);
        tv_thu = (TextView) findViewById(R.id.tv_thu);
        tv_fri = (TextView) findViewById(R.id.tv_fri);
        tv_sat = (TextView) findViewById(R.id.tv_sat);
        tv_sun = (TextView) findViewById(R.id.tv_sun);

        btn_show_log = (Button) findViewById(R.id.btn_show_log);
        btn_show_log.setOnClickListener(this);
        btn_clear_time = (Button) findViewById(R.id.btn_clear_time);
        btn_clear_time.setOnClickListener(this);
        btn_add_time = (Button) findViewById(R.id.btn_add_time);
        btn_add_time.setOnClickListener(this);
        tv_show_time = (TextView) findViewById(R.id.tv_show_time);
        iv_no_data = (LinearLayout) findViewById(R.id.iv_no_data);
        lv_power = (ListView) findViewById(R.id.lv_power);
        adapter = new PowerOnOffLocalAdapter(PowerOnOffLocalActivity.this, list);
        lv_power.setAdapter(adapter);
        lv_power.setOnItemClickListener(this);
        lv_power.setOnItemLongClickListener(this);
    }

    /***
     * 获取数据库保存的定时开关时间
     */
    private void getLocalDbData() {
        try {
            list.clear();
            waitDialogUtil.show("loading...");
            list = DbTimerLocalUtil.queryTimerList();
            if (list == null || list.size() < 1) {
                waitDialogUtil.dismiss();
                iv_no_data.setVisibility(View.VISIBLE);
                powerOnOffUtil.clearPowerOnOffTime();
                MyToastView.getInstance().Toast(PowerOnOffLocalActivity.this, getString(R.string.no_power_data));
                getCurrentDate();
                return;
            }
            iv_no_data.setVisibility(View.GONE);
            adapter.setList(list);
            getCurrentDate();
            powerOnOffUtil.changePowerOnOffByWorkModel();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    waitDialogUtil.dismiss();
                    getCurrentDate();
                }
            }, 1000);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_show_log:
                startActivity(new Intent(PowerOnOffLocalActivity.this, PowerInOffLogActivity.class));
                break;
            case R.id.mIvBack:
                finish();
                break;
            case R.id.btn_clear_time:
                DbTimerLocalUtil.clearTimeDb();
                getLocalDbData();
                break;
            case R.id.btn_add_time:
                Intent intent = new Intent(PowerOnOffLocalActivity.this, TimerChangeActivity.class);
                intent.putExtra(TimerChangeActivity.ORDER_STRING, TimerChangeActivity.ORDER_ADD);
                PowerOnOffLocalActivity.this.startActivity(intent);
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        getLocalDbData();
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
        TimeLocalEntity entity = list.get(position);
        final long id = entity.getId();
        if (oridinryDialog == null) {
            oridinryDialog = new OridinryDialog(PowerOnOffLocalActivity.this);
        }
        oridinryDialog.show(getString(R.string.change_power_info), getString(R.string.confirm), getString(R.string.cancel));
        oridinryDialog.setOnDialogClickListener(new OridinryDialogClick() {
            @Override
            public void sure() {  //修改
                Intent intent = new Intent(PowerOnOffLocalActivity.this, TimerChangeActivity.class);
                intent.putExtra(TimerChangeActivity.ORDER_STRING, TimerChangeActivity.ORDER_MODIFY);
                intent.putExtra(TimerChangeActivity.TIMER_ID, id);
                PowerOnOffLocalActivity.this.startActivity(intent);
            }

            @Override
            public void noSure() {  //删除

            }
        });
    }

    private void showToastView(String s) {
        MyToastView.getInstance().Toast(PowerOnOffLocalActivity.this, s);
    }

    /***
     * 更新界面，今天星期几和今天的定时开关机时间
     */
    private void getCurrentDate() {
        Calendar calendar = Calendar.getInstance();
        int currentDayWeek = calendar.get(Calendar.DAY_OF_WEEK);  //今天的工作星期
        switch (currentDayWeek) {
            case 1: //7
                tv_sun.setTextColor(getResources().getColor(R.color.white));
                tv_sun.setBackgroundResource(R.drawable.terminal_setting);
                break;
            case 2: //1
                tv_mon.setBackgroundResource(R.drawable.terminal_setting);
                tv_mon.setTextColor(getResources().getColor(R.color.white));
                break;
            case 3: //2
                tv_tue.setBackgroundResource(R.drawable.terminal_setting);
                tv_tue.setTextColor(getResources().getColor(R.color.white));
                break;
            case 4: //3
                tv_wed.setBackgroundResource(R.drawable.terminal_setting);
                tv_wed.setTextColor(getResources().getColor(R.color.white));
                break;
            case 5: //4
                tv_thu.setBackgroundResource(R.drawable.terminal_setting);
                tv_thu.setTextColor(getResources().getColor(R.color.white));
                break;
            case 6: //5
                tv_fri.setBackgroundResource(R.drawable.terminal_setting);
                tv_fri.setTextColor(getResources().getColor(R.color.white));
                break;
            case 7: //6
                tv_sat.setBackgroundResource(R.drawable.terminal_setting);
                tv_sat.setTextColor(getResources().getColor(R.color.white));
                break;

        }
        //=======================================================================
        //更新一下下一次的定时开关机
        String onTime = PowerOnOffUtil.getPowerOnTime(PowerOnOffLocalActivity.this);
        String offTime = PowerOnOffUtil.getPowerOffTime(PowerOnOffLocalActivity.this);
        if (onTime.length() < 2 || offTime.length() < 2) {
            tv_show_time.setText(getString(R.string.off_time) + offTime + "\n" + getString(R.string.open_time) + onTime);
        } else {
            String year = onTime.substring(0, 4);
            String month = onTime.substring(4, 6);
            String day = onTime.substring(6, 8);
            String hour = onTime.substring(8, 10);
            String min = onTime.substring(10, 12);

            String offyear = offTime.substring(0, 4);
            String offmonth = offTime.substring(4, 6);
            String offday = offTime.substring(6, 8);
            String offhour = offTime.substring(8, 10);
            String offmin = offTime.substring(10, 12);
            String onTimeShow = year + "/" + month + "/" + day + "  " + hour + ":" + min + ":00";
            String offTimeShow = offyear + "/" + offmonth + "/" + offday + "  " + offhour + ":" + offmin + ":00";
            tv_show_time.setText(getString(R.string.off_time) + offTimeShow + "\n" + getString(R.string.open_time) + onTimeShow);
        }
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> adapterView, View view, int position, long l) {
        TimeLocalEntity entity = list.get(position);
        final long id = entity.getId();
        if (oridinryDialog == null) {
            oridinryDialog = new OridinryDialog(PowerOnOffLocalActivity.this);
        }
        oridinryDialog.show(getString(R.string.del_power_info), getString(R.string.tv_del), getString(R.string.cancel));
        oridinryDialog.setOnDialogClickListener(new OridinryDialogClick() {
            @Override
            public void sure() {  //删除
                boolean isDel = DbTimerLocalUtil.delTimerById(id);
                showToastView(getString(R.string.del_data_info) + (getString(isDel ? R.string.punch_success_jdq : R.string.failed)));
                getLocalDbData();
            }

            @Override
            public void noSure() {  //取消

            }
        });
        return true;
    }

//    public void onDestroy() {
//        super.onDestroy();
//        if (receiver != null) {
//            PowerOnOffLocalActivity.this.unregisterReceiver(receiver);
//        }
//    }

}
