package com.ys.acsdev.poweronoff.util.system;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import java.util.Arrays;

public class PowerOnOffManager {

    private static PowerOnOffManager powerOnOffManager;
    private Context mContext;

    private PowerOnOffManager(Context context) {
        this.mContext = context;
    }

    public static synchronized PowerOnOffManager getInstance(Context context) {
        if (powerOnOffManager == null) {
            powerOnOffManager = new PowerOnOffManager(context);
        }

        return powerOnOffManager;
    }

    public void shutdown() {
        Intent intent = new Intent();
        intent.setAction("android.intent.action.shutdown");
        this.mContext.sendBroadcast(intent);
    }

    public void clearPowerOnOffTime() {
        Intent intent = new Intent("android.intent.ClearOnOffTime");
        this.mContext.sendBroadcast(intent);
    }

    public String getPowerOnTime() {
        return Utils.getValueFromProp("persist.sys.powerontime");
    }

    public String getPowerOffTime() {
        return Utils.getValueFromProp("persist.sys.powerofftime");
    }

    public static String getLastestPowerOnTime() {
        return Utils.getValueFromProp("persist.sys.powerontimeper");
    }

    public static String getLastestPowerOffTime() {
        return Utils.getValueFromProp("persist.sys.powerofftimeper");
    }

    public void setPowerOnOff(int[] powerOnTime, int[] powerOffTime) {
        Intent intent = new Intent("android.intent.action.setpoweronoff");
        intent.putExtra("timeon", powerOnTime);
        intent.putExtra("timeoff", powerOffTime);
        intent.putExtra("enable", true);
        this.mContext.sendBroadcast(intent);
        Log.d("PowerOnOffManager", "poweron:" + Arrays.toString(powerOnTime) + "/ poweroff:" + Arrays.toString(powerOffTime));
    }

    public void cancelPowerOnOff(int[] powerOnTime, int[] powerOffTime) {
        Intent intent = new Intent("android.intent.action.setpoweronoff");
        intent.putExtra("timeon", powerOnTime);
        intent.putExtra("timeoff", powerOffTime);
        intent.putExtra("enable", false);
        this.mContext.sendBroadcast(intent);
        Log.d("PowerOnOffManager", "poweron:" + Arrays.toString(powerOnTime) + "/ poweroff:" + Arrays.toString(powerOffTime));
    }

    public String getVersion() {
        return Utils.getValueFromProp("persist.sys.poweronoffversion");
    }
}
