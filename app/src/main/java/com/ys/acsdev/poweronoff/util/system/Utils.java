package com.ys.acsdev.poweronoff.util.system;


import java.lang.reflect.Method;

public class Utils {

    public Utils() {
    }

    public static String getValueFromProp(String key) {
        String value = "";

        try {
            Class classType = Class.forName("android.os.SystemProperties");
            Method getMethod = classType.getDeclaredMethod("get", new Class[]{String.class});
            value = (String) getMethod.invoke(classType, new Object[]{key});
        } catch (Exception var4) {
        }

        return value;
    }
}
