package com.ys.acsdev.poweronoff.util;

import java.io.File;
import java.io.FileInputStream;

public class FileUtil {
    public static final String TAG = "FileUtil";

    //删除目录或者文件
    public static boolean deleteDirOrFile(String Path) {
        return deleteDirOrFile(new File(Path));
    }

    public static boolean deleteDirOrFile(File file) {
        if (file.isFile()) {
            file.delete();
            return false;
        }
        if (file.isDirectory()) {
            File[] childFile = file.listFiles();
            if (childFile == null || childFile.length == 0) {
                file.delete();
                return false;
            }
            for (File f : childFile) {
                deleteDirOrFile(f);
            }
            file.delete();
        }
//        FileUtil.creatPathNotExcit();
        return false;
    }
}
