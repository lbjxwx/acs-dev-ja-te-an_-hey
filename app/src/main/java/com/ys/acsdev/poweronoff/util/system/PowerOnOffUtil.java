package com.ys.acsdev.poweronoff.util.system;

import android.content.Context;


import com.ys.acsdev.poweronoff.db.DbTimerLocalUtil;
import com.ys.acsdev.poweronoff.db.PowerOnOffLogDb;
import com.ys.acsdev.poweronoff.db.TimeLocalEntity;
import com.ys.acsdev.poweronoff.entity.PoOnOffLogEntity;
import com.ys.acsdev.poweronoff.entity.ScheduleRecord;
import com.ys.acsdev.poweronoff.entity.TimeEntity;
import com.ys.acsdev.poweronoff.entity.TimedTaskListEntity;
import com.ys.acsdev.poweronoff.entity.TimerDbEntity;
import com.ys.acsdev.poweronoff.util.DbTimerUtil;
import com.ys.acsdev.poweronoff.util.MyLog;
import com.ys.acsdev.poweronoff.util.SimpleDateUtil;

import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

/**
 * 定时开关机管理类
 * 业务逻辑如下
 * 1：查询网络服务器用户设定的定时开关机时间
 * 网络查询失败，也从本地去获取
 * 2：保存用户设定的到本地数据库
 * 3：获取本地数据库数据
 * 4：去设定定时开关机时间
 */
public class PowerOnOffUtil {

    Context context;
    private Calendar c;
    PowerOnOffManager manager;

    public PowerOnOffUtil(Context context) {
        this.context = context;
        c = Calendar.getInstance();
        manager = PowerOnOffManager.getInstance(context);
        c.setTimeZone(TimeZone.getTimeZone("GMT+8:00"));
    }


    /***
     * 保存数据到本地数据库
     * @param lists
     */
    public void savePowerOnOffTime(List<TimedTaskListEntity> lists) {
        listPowerOnOff.clear();
        DbTimerUtil.clearTimeDb();
        if (lists.size() < 1) {
            clearPowerOnOffTime();
            MyLog.powerOnOff("获取的定时开关机 < 1 ,用户删除定时开关机任务，终止操作");
            return;
        }
        for (int i = 0; i < lists.size(); i++) {
            TimedTaskListEntity entity = lists.get(i);
            String powerOnTime = entity.getTtOnTime();
            String powerOffTime = entity.getTtOffTime();
            String ttMon = entity.getTtMon();
            String ttTue = entity.getTtTue();
            String ttWed = entity.getTtWed();
            String ttThu = entity.getTtThu();
            String ttFri = entity.getTtFri();
            String ttSat = entity.getTtSat();
            String ttSun = entity.getTtSun();
            TimerDbEntity timerDbEntity = new TimerDbEntity(powerOnTime, powerOffTime, ttMon, ttTue, ttWed, ttThu, ttFri, ttSat, ttSun);
            DbTimerUtil.addTimerDb(timerDbEntity);
        }
        getPowerOnOffFromDb();
    }

    /***
     * 从数据库中获取数据
     */
    public void getPowerOnOffFromDb() {
        MyLog.powerOnOff("======冲本地数据库中查询数据");
        List<TimerDbEntity> lists = DbTimerUtil.queryTimerList();
        if (lists == null || lists.size() < 1) {
            clearPowerOnOffTime();
            return;
        }
        List<TimedTaskListEntity> entntiList = new ArrayList<TimedTaskListEntity>();
        for (int i = 0; i < lists.size(); i++) {
            TimerDbEntity entity = lists.get(i);
            TimedTaskListEntity timerTaskListEntity = new TimedTaskListEntity();
            timerTaskListEntity.setTtOnTime(entity.getTtOnTime());
            timerTaskListEntity.setTtOffTime(entity.getTtOffTime());
            timerTaskListEntity.setTtMon(entity.getTtMon());
            timerTaskListEntity.setTtTue(entity.getTtTue());
            timerTaskListEntity.setTtWed(entity.getTtWed());
            timerTaskListEntity.setTtThu(entity.getTtThu());
            timerTaskListEntity.setTtFri(entity.getTtFri());
            timerTaskListEntity.setTtSat(entity.getTtSat());
            timerTaskListEntity.setTtSun(entity.getTtSun());
            entntiList.add(timerTaskListEntity);
        }
        setPowerOnOffTime(entntiList);
    }

    /***
     * 根据工作模式来设定定时开关机
     */
    public void changePowerOnOffByWorkModel() {
        clearPowerOnOffTime();
        List<TimeLocalEntity> lists = DbTimerLocalUtil.queryTimerList();
        setLocalTimer(lists);
    }

    /***
     * 设置单机定时开关机
     * @param lists
     */
    private void setLocalTimer(List<TimeLocalEntity> lists) {
        if (lists == null || lists.size() < 1) {
            clearPowerOnOffTime();
            return;
        }
        List<TimedTaskListEntity> timeListSet = new ArrayList<>();
        for (int i = 0; i < lists.size(); i++) {
            TimeLocalEntity timerDbTentity = lists.get(i);
            String starttTime = timerDbTentity.getTtOnTime();
            String enTime = timerDbTentity.getTtOffTime();
            String mon = timerDbTentity.getTtMon();
            String tue = timerDbTentity.getTtTue();
            String wed = timerDbTentity.getTtWed();
            String thu = timerDbTentity.getTtThu();
            String fri = timerDbTentity.getTtFri();
            String sta = timerDbTentity.getTtSat();
            String sun = timerDbTentity.getTtSun();
            TimedTaskListEntity entity = new TimedTaskListEntity("-1", starttTime, enTime, mon, tue, wed, thu, fri, sta, sun);
            timeListSet.add(entity);
        }
        setPowerOnOffTime(timeListSet);
    }

    /**
     * 设置系统定时开关机
     * 亿晟定时开关机规律
     * 1：分组智能设定一组
     * 2：采取获取当前最近的一组，设定，
     * 3：下次开机重新获取离下次最近的一组
     *
     * @param lists
     */
    public void setPowerOnOffTime(List<TimedTaskListEntity> lists) {
        try {
            listPowerOnOff.clear();
            if (lists.size() < 1) {
                clearPowerOnOffTime();    //===清理定时开关机====
                MyLog.powerOnOff("获取的定时开关机 < 1 ,用户删除定时开关机任务，终止操作");
                return;
            }
            for (int i = 0; i < lists.size(); i++) {
                int dayOfWeek = -1;
                TimedTaskListEntity entity = lists.get(i);
                String powerOnTime = entity.getTtOnTime();
                String powerOffTime = entity.getTtOffTime();
                if (powerOnTime.length() < 1 || powerOffTime.length() < 1) { //如果本次的数据为null，跳过本次循环
                    continue;
                }
                MyLog.powerOnOff("=====获取的开机关机时间===" + powerOnTime + " / " + powerOffTime);
                int onHour = Integer.parseInt(powerOnTime.substring(0, powerOnTime.indexOf(":")));
                int onMin = Integer.parseInt(powerOnTime.substring(powerOnTime.indexOf(":") + 1, powerOnTime.length()));
                int offHour = Integer.parseInt(powerOffTime.substring(0, powerOffTime.indexOf(":")));
                int offMin = Integer.parseInt(powerOffTime.substring(powerOffTime.indexOf(":") + 1, powerOffTime.length()));
//            MyLog.powerOnOff("=====获取的开关机机小时==" + onHour + " / " + onMin + "  /关机时间==" + offHour + " / " + offMin);
                boolean ttMon = Boolean.parseBoolean(entity.getTtMon());
                if (ttMon) {
                    dayOfWeek = 2;
                    saveTimerTask(dayOfWeek, onHour, onMin, offHour, offMin);
                }
                boolean ttTue = Boolean.parseBoolean(entity.getTtTue());
                if (ttTue) {
                    dayOfWeek = 3;
                    saveTimerTask(dayOfWeek, onHour, onMin, offHour, offMin);
                }
                boolean ttWed = Boolean.parseBoolean(entity.getTtWed());
                if (ttWed) {
                    dayOfWeek = 4;
                    saveTimerTask(dayOfWeek, onHour, onMin, offHour, offMin);
                }
                boolean ttThu = Boolean.parseBoolean(entity.getTtThu());
                if (ttThu) {
                    dayOfWeek = 5;
                    saveTimerTask(dayOfWeek, onHour, onMin, offHour, offMin);
                }
                boolean ttFri = Boolean.parseBoolean(entity.getTtFri());
                if (ttFri) {
                    dayOfWeek = 6;
                    saveTimerTask(dayOfWeek, onHour, onMin, offHour, offMin);
                }
                boolean ttSat = Boolean.parseBoolean(entity.getTtSat());
                if (ttSat) {
                    dayOfWeek = 7;
                    saveTimerTask(dayOfWeek, onHour, onMin, offHour, offMin);
                }
                boolean ttSun = Boolean.parseBoolean(entity.getTtSun());
                if (ttSun) {
                    dayOfWeek = 1;
                    saveTimerTask(dayOfWeek, onHour, onMin, offHour, offMin);
                }
            }

            long powerOffTime = TSchedule.getLastPowerOnOffTime(listPowerOnOff, false);
            long powerOnTime = TSchedule.getLastPowerOnOffTime(listPowerOnOff, true);
            MyLog.powerOnOff("===获取的关机时间===" + powerOffTime + " /最早的开机时间==" + powerOnTime);
            if (powerOffTime < 0 && powerOnTime < 0) {   //这里可能获取时间异常了 ，不设置定时开关机
                MyLog.powerOnOff("===获取的开关机时间异常了，这里清理定时开关机");
                clearPowerOnOffTime();
                return;
            }
            if (powerOffTime > 0 && powerOnTime < 0) {
                //这里表示设置的开关时间已经超过一周了，这里需要做一个预判。第二天这个时候开机i一次，然后重新获取
                powerOnTime = TSchedule.jujleNoOnTime(powerOffTime);
            }
            MyLog.powerOnOff("=====最终的时间===" + powerOnTime + " / " + powerOffTime);
            long currentTimeSimple = SimpleDateUtil.getCurrentTimelONG(); //獲取當前的時間
            //如果下一次的定时开关机時間大於當前3分鐘，才能設定，不然就區下一次的定時開關機
            if (powerOnTime < powerOffTime && (powerOnTime - currentTimeSimple) > 300) {
                //如果最近的一次开机时间《关机时间，以最近的一次为准.倒退两分钟开机
                long onTimeLong = SimpleDateUtil.StringToLongTimePower(powerOnTime + "");
                long onTimeLongOff = onTimeLong - (1000 * 120);  //关机时间在此时间前减去2分钟再去换算时间
                MyLog.powerOnOff("===onTimeLongOff===" + onTimeLongOff);
                powerOffTime = SimpleDateUtil.formatBig(onTimeLongOff);
                MyLog.powerOnOff("===合理的时间===" + powerOnTime + " / " + powerOffTime);
                timeLongChangeEntitytoSetTime(powerOffTime, powerOnTime);
                return;
            }
            powerOnTime = TSchedule.getLastPowerOnTime(listPowerOnOff, powerOffTime);
            MyLog.powerOnOff("===获取的开机时间===" + powerOnTime);
            if (powerOffTime < 0 || powerOnTime < 0) {
                //这里可能获取时间异常了 ，不设置定时开关机
                MyLog.powerOnOff("===获取的开关机时间异常了，这里清理定时开关机");
                clearPowerOnOffTime();
                return;
            }
            timeLongChangeEntitytoSetTime(powerOffTime, powerOnTime);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 用来转化时间参数
     * 将long 类型的定时开关机转化成Entity 类型的数据
     *
     * @param powerOffTime
     * @param powerOnTime
     */
    private void timeLongChangeEntitytoSetTime(long powerOffTime, long powerOnTime) {
        MyLog.powerOnOff("===设定的时间===" + powerOnTime + " / " + powerOffTime);
        TimeEntity entityOff = SimpleDateUtil.getFormatLongTime(powerOffTime);
        TimeEntity entityOn = SimpleDateUtil.getFormatLongTime(powerOnTime);
        MyLog.powerOnOff("===设定的开机时间===" + powerOnTime + " / " + entityOn.toString());
        MyLog.powerOnOff("===设定的关机时间===" + powerOffTime + " / " + entityOff.toString());
        setToPowerOnOffTime(entityOn, entityOff);
    }

    private void setToPowerOnOffTime(TimeEntity entityOn, TimeEntity entityOff) {
        String onTimeSave = entityOn.getYear() + "-" + entityOn.getMonth() + "-" + entityOn.getDay() + " " + entityOn.getHour() + ":" + entityOn.getMinite() + ":00";
        String offTimeSave = entityOff.getYear() + "-" + entityOff.getMonth() + "-" + entityOff.getDay() + " " + entityOff.getHour() + ":" + entityOff.getMinite() + ":00";
        String createTime = SimpleDateUtil.formatCurrentTime();
        PoOnOffLogEntity poOnOffLogEntity = new PoOnOffLogEntity(offTimeSave, onTimeSave, createTime);
        boolean isSave = PowerOnOffLogDb.savePowerOnOffToWeb(poOnOffLogEntity);

        MyLog.powerOnOff("保存数据库的时间==" + offTimeSave + " / " + onTimeSave + " / " + createTime);
        int[] timeoffArray = new int[]{entityOff.getYear(), entityOff.getMonth(), entityOff.getDay(), entityOff.getHour(), entityOff.getMinite()};
        int[] timeonArray = new int[]{entityOn.getYear(), entityOn.getMonth(), entityOn.getDay(), entityOn.getHour(), entityOn.getMinite()};
        PowerOnOffManager powerManager = PowerOnOffManager.getInstance(context);
        powerManager.setPowerOnOff(timeonArray, timeoffArray);
    }

    List<ScheduleRecord> listPowerOnOff = new ArrayList<ScheduleRecord>();

    private void saveTimerTask(int dayOfWeek, int onHour, int onMin, int offHour, int offMin) {
        ScheduleRecord scheduleRecord = new ScheduleRecord(dayOfWeek, onHour, onMin, offHour, offMin);
//        MyLog.powerOnOff("===设定的定时开关机时间列表==" + scheduleRecord.toString());
        listPowerOnOff.add(scheduleRecord);
    }


    /***=========================================================================================================
     * 关机
     * @param context
     */
    public void setShowDownMethion(Context context) {
        try {
            PowerOnOffManager powerOnOffManager = PowerOnOffManager.getInstance(context);
            powerOnOffManager.shutdown();
        } catch (Exception e) {
        }
    }

    /***
     * 清理定时开关机任务
     */
    public void clearPowerOnOffTime() {
        try {
            //删除数据库中的数据
            MyLog.powerOnOff("准备清理定时开关机");
            manager.clearPowerOnOffTime();
            MyLog.powerOnOff("清理定时开关机 success");
        } catch (Exception e) {
            MyLog.powerOnOff("清理定时开关机error =" + e.toString());
        }
    }

    /***
     * 获取定时开机时间
     * @return
     */
    public static String getPowerOnTime(Context context) {
        PowerOnOffManager powerOnOffManager = PowerOnOffManager.getInstance(context);
        String time = powerOnOffManager.getPowerOnTime();
        return time;
    }

    /***
     * 获取关机的时间
     * @param context
     * @return
     */
    public static String getPowerOffTime(Context context) {
        PowerOnOffManager powerOnOffManager = PowerOnOffManager.getInstance(context);
        String time = powerOnOffManager.getPowerOffTime();
        return time;
    }

    /***
     * 获取定时开关机版本号
     * @return
     */
    public String getVersion(Context context) {
        String version = manager.getVersion();
        return version;
    }

    //==================背光相关操作=================================================================

    /***
     * 判断背光是否打开
     * @return
     */
    public static String isOpenLight() {
        String str = "";
        try {
            File file = new File("/sys/devices/fb.11/graphics/fb0/pwr_bl");
            FileReader reader = new FileReader(file);
            char[] bb = new char[1024];
            int n;
            while ((n = reader.read(bb)) != -1) {
                str += new String(bb, 0, n);
            }
            reader.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return str;
    }

    /***
     * 开关背光操作
     * @param paramInt
     */
    public static void openOrClose(int paramInt) {
        try {
            writeFile(paramInt);
            return;
        } catch (Exception localException) {
            localException.printStackTrace();
        }
    }

    public static void writeFile(int paramInt) {
        try {
            File localFile = new File("/sys/devices/fb.11/graphics/fb0/pwr_bl");
            localFile.setExecutable(true);
            localFile.setReadable(true);
            localFile.setWritable(true);
            if (paramInt == 0) {
                do_exec("busybox echo 0 > /sys/devices/fb.11/graphics/fb0/pwr_bl");
            } else if (paramInt == 1) {
                do_exec("busybox echo 1 > /sys/devices/fb.11/graphics/fb0/pwr_bl");
            }
        } catch (Exception localException) {
            localException.printStackTrace();
        }
    }

    public static void do_exec(String paramString) {
        try {
            Process localProcess = Runtime.getRuntime().exec("su");
            String str = paramString + "\nexit\n";
            localProcess.getOutputStream().write(str.getBytes());
            if (localProcess.waitFor() != 0) {
                System.out.println("cmd=" + paramString + " error!");
                throw new SecurityException();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //===================================================================================================

    /***
     * 把星期 开关机时间转化成时间戳形式
     * @param timerList
     * @return
     */
    public static List<ScheduleRecord> getPowerDateLocalList(List<TimeLocalEntity> timerList) {
        List<ScheduleRecord> listBack = new ArrayList<ScheduleRecord>();
        try {
            if (timerList == null || timerList.size() < 1) {
                return listBack;
            }
            List<TimedTaskListEntity> timeListSet = new ArrayList<>();
            for (int i = 0; i < timerList.size(); i++) {
                TimeLocalEntity timerDbTentity = timerList.get(i);
                String starttTime = timerDbTentity.getTtOnTime();
                String enTime = timerDbTentity.getTtOffTime();
                String mon = timerDbTentity.getTtMon();
                String tue = timerDbTentity.getTtTue();
                String wed = timerDbTentity.getTtWed();
                String thu = timerDbTentity.getTtThu();
                String fri = timerDbTentity.getTtFri();
                String sta = timerDbTentity.getTtSat();
                String sun = timerDbTentity.getTtSun();
                TimedTaskListEntity entity = new TimedTaskListEntity("-1", starttTime, enTime, mon, tue, wed, thu, fri, sta, sun);
                timeListSet.add(entity);
            }
            listBack = addTimerInfoToList(timeListSet);
        } catch (Exception E) {
            E.printStackTrace();
        }
        return listBack;
    }

    public static List<ScheduleRecord> getPowerDateNetList(List<TimerDbEntity> timerList) {
        List<ScheduleRecord> listBack = new ArrayList<ScheduleRecord>();
        try {
            if (timerList == null || timerList.size() < 1) {
                return listBack;
            }
            List<TimedTaskListEntity> timeListSet = new ArrayList<>();
            for (int i = 0; i < timerList.size(); i++) {
                TimerDbEntity timerDbTentity = timerList.get(i);
                String starttTime = timerDbTentity.getTtOnTime();
                String enTime = timerDbTentity.getTtOffTime();
                String mon = timerDbTentity.getTtMon();
                String tue = timerDbTentity.getTtTue();
                String wed = timerDbTentity.getTtWed();
                String thu = timerDbTentity.getTtThu();
                String fri = timerDbTentity.getTtFri();
                String sta = timerDbTentity.getTtSat();
                String sun = timerDbTentity.getTtSun();
                TimedTaskListEntity entity = new TimedTaskListEntity("-1", starttTime, enTime, mon, tue, wed, thu, fri, sta, sun);
                timeListSet.add(entity);
            }
            listBack = addTimerInfoToList(timeListSet);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return listBack;
    }

    private static List<ScheduleRecord> addTimerInfoToList(List<TimedTaskListEntity> timeListSet) {
        List<ScheduleRecord> listBack = new ArrayList<ScheduleRecord>();
        try {
            for (int i = 0; i < timeListSet.size(); i++) {
                int dayOfWeek = -1;
                TimedTaskListEntity entity = timeListSet.get(i);
                String powerOnTime = entity.getTtOnTime();
                String powerOffTime = entity.getTtOffTime();
                if (powerOnTime.length() < 1 || powerOffTime.length() < 1) { //如果本次的数据为null，跳过本次循环
                    continue;
                }
                MyLog.powerOnOff("=====获取的开机关机时间===" + powerOnTime + " / " + powerOffTime);
                int onHour = Integer.parseInt(powerOnTime.substring(0, powerOnTime.indexOf(":")));
                int onMin = Integer.parseInt(powerOnTime.substring(powerOnTime.indexOf(":") + 1, powerOnTime.length()));
                int offHour = Integer.parseInt(powerOffTime.substring(0, powerOffTime.indexOf(":")));
                int offMin = Integer.parseInt(powerOffTime.substring(powerOffTime.indexOf(":") + 1, powerOffTime.length()));
                boolean ttMon = Boolean.parseBoolean(entity.getTtMon());
                if (ttMon) {
                    dayOfWeek = 2;
                    ScheduleRecord scheduleRecord = new ScheduleRecord(dayOfWeek, onHour, onMin, offHour, offMin);
                    listBack.add(scheduleRecord);
                }
                boolean ttTue = Boolean.parseBoolean(entity.getTtTue());
                if (ttTue) {
                    dayOfWeek = 3;
                    ScheduleRecord scheduleRecord = new ScheduleRecord(dayOfWeek, onHour, onMin, offHour, offMin);
                    listBack.add(scheduleRecord);
                }
                boolean ttWed = Boolean.parseBoolean(entity.getTtWed());
                if (ttWed) {
                    dayOfWeek = 4;
                    ScheduleRecord scheduleRecord = new ScheduleRecord(dayOfWeek, onHour, onMin, offHour, offMin);
                    listBack.add(scheduleRecord);
                }
                boolean ttThu = Boolean.parseBoolean(entity.getTtThu());
                if (ttThu) {
                    dayOfWeek = 5;
                    ScheduleRecord scheduleRecord = new ScheduleRecord(dayOfWeek, onHour, onMin, offHour, offMin);
                    listBack.add(scheduleRecord);
                }
                boolean ttFri = Boolean.parseBoolean(entity.getTtFri());
                if (ttFri) {
                    dayOfWeek = 6;
                    ScheduleRecord scheduleRecord = new ScheduleRecord(dayOfWeek, onHour, onMin, offHour, offMin);
                    listBack.add(scheduleRecord);
                }
                boolean ttSat = Boolean.parseBoolean(entity.getTtSat());
                if (ttSat) {
                    dayOfWeek = 7;
                    ScheduleRecord scheduleRecord = new ScheduleRecord(dayOfWeek, onHour, onMin, offHour, offMin);
                    listBack.add(scheduleRecord);
                }
                boolean ttSun = Boolean.parseBoolean(entity.getTtSun());
                if (ttSun) {
                    dayOfWeek = 1;
                    ScheduleRecord scheduleRecord = new ScheduleRecord(dayOfWeek, onHour, onMin, offHour, offMin);
                    listBack.add(scheduleRecord);
                }
            }
            if (listBack == null || listBack.size() < 1) {
                return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return parsenerTimerListSch(listBack);
    }

    /***
     * @param listSch
     * 定时开关机列表
     * on   表示开机时间
     * off  表示关机时间
     * @return
     */
    private static List<ScheduleRecord> parsenerTimerListSch(List<ScheduleRecord> listSch) {
        if (listSch == null || listSch.size() < 1) {
            return null;
        }
        List<ScheduleRecord> listBack = new ArrayList<ScheduleRecord>();
        try {
            Calendar calendar = Calendar.getInstance();
            int currentDayWeek = calendar.get(Calendar.DAY_OF_WEEK);  //今天的工作星期
            Long currentTimeMillis = System.currentTimeMillis();
            long simpleTime = SimpleDateUtil.formatBig(currentTimeMillis);
            //以今天基准，遍历所有日期，补全所有的时间数据
            TimeEntity entity = SimpleDateUtil.getFormatLongTime(simpleTime);
            for (int i = 0; i < listSch.size(); i++) {
                ScheduleRecord scheduleRecord = listSch.get(i);
                int weekList = scheduleRecord.getDayOfWeek();
                int distanceDay = 0;
                int year = entity.getYear();
                int month = entity.getMonth();
                int day = entity.getDay();

                if (weekList < currentDayWeek) {
                    distanceDay = 7 - currentDayWeek + weekList;
                } else {  //星期属大于今天
                    distanceDay = weekList - currentDayWeek;
                }
                day = day + distanceDay;
                if (month == 12 && day > 31) {  //跨年了
                    year = year + 1;
                    month = 1;
                    day = 1;
                } else if (month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12) {   //判断31天月份
                    if (day > 31) {
                        day = day - 31;
                        month = month + 1;
                    }
                } else if (month == 4 || month == 6 || month == 9 || month == 11) {           //判断30天的月份
                    if (day > 30) {
                        day = day - 30;
                        month = month + 1;
                    }
                } else if (month == 2) {                            //判断二月
                    if (year % 4 == 0 && year % 100 != 0) {//闰年
                        if (day > 29) {
                            day = day - 29;
                            month = month + 1;
                        }
                    } else if (year % 400 == 0) { //闰年
                        if (day > 29) {
                            day = day - 29;
                            month = month + 1;
                        }
                    } else {  //平年
                        if (day > 28) {
                            day = day - 28;
                            month = month + 1;
                        }
                    }
                }
                scheduleRecord.setYear(year);
                scheduleRecord.setMonth(month);
                scheduleRecord.setDay(day);
                listBack.add(scheduleRecord);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return listBack;
    }

}
