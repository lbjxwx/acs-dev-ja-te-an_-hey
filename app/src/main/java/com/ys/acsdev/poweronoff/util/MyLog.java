package com.ys.acsdev.poweronoff.util;

import android.util.Log;

public class MyLog {

    public static void i(String tag, String s) {
        Log.i(tag, s);
    }

    public static void e(String tag, String s) {
        Log.e(tag, s);
    }


    public static void http(String s) {
        e("http", s);
    }

    public static void powerOnOff(String s) {
        e("powerOnOff", s);
    }

}
