package com.huiming.common.ext

/**
 * Created by kermitye on 2018/11/13 11:18
 */
class BaseHttpException(val code: Int, val msg: String): Throwable()