package com.huiming.common.ext

/**
 * Created by kermitye on 2018/11/13 11:17
 */
data class BaseResp<out T>(
        val code: Int,
        val msg: String,
        val data: T?)