package com.ys.ysim.net

import com.google.gson.Gson
import com.huiming.common.ext.BaseHttpException
import com.huiming.common.ext.BaseResp
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Converter
import java.lang.reflect.Type


// Created by kermitye on 2019/10/19 11:15
class  GsonResponseBodyConverter <T>(var gson: Gson, var type: Type) : Converter<ResponseBody, T> {


    override fun convert(value: ResponseBody): T {

        val response = value.string()
//        val json = JSONObject(response)
//        val code = json.optInt("code")
//        val msg = json.optString("msg")
        //先将返回的json数据解析到Response中，如果code==200，则解析到我们的实体基类中，否则抛异常
        val httpResult = gson.fromJson(response, BaseResp::class.java)
        if (httpResult.code == 0) {
            //0的时候就直接解析，不可能出现解析异常。因为我们实体基类中传入的泛型，就是数据成功时候的格式
            return gson.fromJson<T>(response, type)
        } else {
            //抛一个自定义ResultException 传入失败时候的状态码，和信息
            throw BaseHttpException(httpResult.code, httpResult.msg)
        }
    }


}