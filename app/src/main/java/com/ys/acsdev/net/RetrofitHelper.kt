package com.huiming.common.net

import android.util.Log
import com.ys.acsdev.BuildConfig
import com.ys.acsdev.net.ResponseConverterFactory
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import java.util.concurrent.TimeUnit


/**
 * Created by kermitye on 2018/11/13 10:26
 */
object RetrofitHelper {

    val CONNECT_TIME_OUT = 60
    val READ_TIME_OUT = 60
    val WRITE_TIME_OUT = 60


    private fun getRetrofit(baseUrl: String): Retrofit {
        Log.e("http", "requestUrl =" + baseUrl)
        val retrofit: Retrofit by lazy {
            Retrofit.Builder()
                .baseUrl(baseUrl)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(ResponseConverterFactory.create())
                .client(getOkHttpClient())
                .build()
        }
        return retrofit
    }

    private fun getOkHttpClient(): OkHttpClient {
        val builder = OkHttpClient.Builder()
        builder.connectTimeout(CONNECT_TIME_OUT.toLong(), TimeUnit.SECONDS)
            .readTimeout(READ_TIME_OUT.toLong(), TimeUnit.SECONDS)
            .writeTimeout(WRITE_TIME_OUT.toLong(), TimeUnit.SECONDS)
            .sslSocketFactory(SSLSocketClient.getSSLSocketFactory(), SSLSocketClient.trustManager())
            .hostnameVerifier(SSLSocketClient.getHostnameVerifier())
        if (BuildConfig.DEBUG) {
            builder.addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
        } else {
            builder.addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.NONE))
        }
        //添加统一请求头
        builder.addInterceptor(object : Interceptor {
            override fun intercept(chain: Interceptor.Chain): Response {
                val original: Request = chain.request()
                val requestBuilder: Request.Builder = original.newBuilder()
                    .header("Accept-Language", "en-US")
                val request: Request = requestBuilder.build()
                return chain.proceed(request)
            }
        })
        return builder.build()
    }

    fun <T> createApi(clazz: Class<T>, url: String): T {
        return getRetrofit(url).create(clazz)
    }
}