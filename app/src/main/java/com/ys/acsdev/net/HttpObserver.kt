package com.yisheng.facerecog.net

import com.huiming.common.ext.BaseHttpException
import io.reactivex.Observer
import io.reactivex.disposables.Disposable
import retrofit2.HttpException

/**
 * Created by kermitye
 * Date: 2018/7/18 14:22
 * Desc:
 */
abstract class HttpObserver<T> : Observer<T> {

    companion object {
        const val HTTP_ERROR = -300
    }

    /**
     * 标记是否为特殊情况
     */
    private var resultNull: Boolean = true

    override fun onComplete() {
        // 特殊情况：当请求成功，但T == null时会跳过onNext，仍需当成功处理
        if (resultNull)
            onSuccess(null)
    }

    override fun onSubscribe(d: Disposable) {
        // 可在此处加上dialog
//        if (!NetUtils.WIFI_AVAILABLE) {
//            onFailed(0, "网络异常，请检查网络")
//            d.dispose()
//        }
    }

    override fun onError(e: Throwable) {
        if (e is BaseHttpException) {
            //可针对一些异常做全局处理
            /*if (e.code == Constants.ERROR_TIME_OUT) {
                onFailed(e.code, "登录过期，请重新登录")
                MyService.INSTANCE?.logout()
            } else {
                onFailed(e.code, e.msg)
            }*/
            onFailed(e.code, e.msg)
        } else if (e is HttpException) {
            onFailed(e.code(), e.message())
        } else {
            onFailed(0, e.message ?: "")
        }
    }

    override fun onNext(t: T) {
        resultNull = false
        onSuccess(t)
    }

    abstract fun onSuccess(t: T?)

    /**
     * 统一处理失败，比如登录失效等
     *
     * @param code
     * @param msg
     */
    open fun onFailed(code: Int, msg: String) {

    }

}