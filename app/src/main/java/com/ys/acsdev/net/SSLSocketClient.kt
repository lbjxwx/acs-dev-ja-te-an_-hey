package com.huiming.common.net

import java.security.KeyStore
import java.security.SecureRandom
import java.util.*
import javax.net.ssl.*

/**
 * Created by kermitye
 * Date: 2018/8/14 11:28
 * Desc:
 */
object SSLSocketClient {
    fun getSSLSocketFactory(): SSLSocketFactory {
        try {
            var sslContext = SSLContext.getInstance("SSL")
            sslContext.init(null, getTrustManager(), SecureRandom())
            return sslContext.socketFactory
        } catch (e: Exception) {
            throw RuntimeException(e);
        }
    }


    fun trustManager(): X509TrustManager {
        val trustManagerFactory =
            TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm())
        trustManagerFactory.init(null as KeyStore?)
        val trustManagers = trustManagerFactory.trustManagers
        check(!(trustManagers.size != 1 || trustManagers[0] !is X509TrustManager)) {
            "Unexpected default trust managers:" + Arrays.toString(
                trustManagers
            )
        }
        return trustManagers[0] as X509TrustManager
    }


    fun getTrustManager() = arrayOf<TrustManager>(object : X509TrustManager {
        override fun checkClientTrusted(
            chain: Array<java.security.cert.X509Certificate>,
            authType: String
        ) {
        }

        override fun checkServerTrusted(
            chain: Array<java.security.cert.X509Certificate>,
            authType: String
        ) {
        }

        override fun getAcceptedIssuers(): Array<java.security.cert.X509Certificate> {
            return arrayOf()
        }
    })

    fun getHostnameVerifier() = HostnameVerifier { s, sslSession -> true };

}
