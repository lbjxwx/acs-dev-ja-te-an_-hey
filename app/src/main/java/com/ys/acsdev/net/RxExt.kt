package com.huiming.common.ext

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import com.trello.rxlifecycle2.android.lifecycle.kotlin.bindUntilEvent
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.ObservableTransformer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 * Created by kermitye on 2018/11/13 11:05
 */
fun <T> Observable<T>.schedulers() =
    subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())

fun <T>Observable<T>.async() = subscribeOn(Schedulers.io()).observeOn(Schedulers.io())


fun <T> Observable<T>.bindLife(lifecycleOwner: LifecycleOwner) =
    bindUntilEvent(lifecycleOwner, Lifecycle.Event.ON_DESTROY)

object RxExt {
    fun <T> handleResult(): ObservableTransformer<in BaseResp<T>, out T?> {
        return ObservableTransformer { upstream ->
            upstream.flatMap {
                if (it.code != 0) {
                    Observable.error(BaseHttpException(it.code, it.msg))
                } else
                    Observable.just(it.data)
            }
        }
    }
}