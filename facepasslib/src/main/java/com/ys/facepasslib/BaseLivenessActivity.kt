package com.ys.facepasslib

import android.content.Context
import android.graphics.Matrix
import android.graphics.Point
import android.graphics.RectF
import android.hardware.Camera
import android.os.Bundle
import android.text.TextUtils
import android.util.DisplayMetrics
import android.util.Log
import android.util.Pair
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import com.ys.facepasslib.camera.CameraManager
import com.ys.facepasslib.camera.CameraPreview
import com.ys.facepasslib.camera.CameraPreviewData
import com.ys.facepasslib.camera.ComplexFrameHelper
import com.ys.facepasslib.view.FaceCircleViewNew
import mcv.facepass.FacePassException
import mcv.facepass.types.*
import java.util.*
import java.util.concurrent.ArrayBlockingQueue

// Created by kermitye on 2020/5/11 16:00
// 双目活体识别
abstract class BaseLivenessActivity : AppCompatActivity(), CameraManager.CameraListener {
    private var mCheckThread: Thread? = null
    private var mRecognizeThread: Thread? = null

    private var mIsKill = false

    @Volatile
    private var mIsChecking: Boolean = false

    @Volatile
    var isPauseRecognize: Boolean = false

    var mHasPerson = false

    //只检测不识别
    var mOnlyCheck = false

    //当发现人脸框左右镜像时可设置此参数
    var mFaceRectMir = 0
    var mNv21: ByteArray = byteArrayOf()
    private val mFaceManager by lazy { FaceManager.instance }
    private val mDetectResultQueue = ArrayBlockingQueue<CheckResult>(5)
    private val mCheckFaceQueue = ArrayBlockingQueue<CameraPreviewData>(1)
    private var mScreenWidth = 1080
    private var mScreenHeight = 1920
    public var mRectRotation = -1
    public var mPrewRotation = -1
    public var mIsFront = false

    //是否是双目
    public var mIsDoubleCamera = false

    //防止检测抖动
    private var mLastChange = 0L
    var mMaskEnable = false //检测口罩
    var mNoMaskTime = 0L
    var mCustomSize: Point? = null
    private var mResetTime: Long = 0L
    private var mCheckTime = 0L
    private var mIsCheck = false
    private var mIsFaceTime = false
    private var mFaceTime = 0L
    private var mNoPersonCount = 0


    /* 相机实例 */
    public val mCameraManager by lazy { CameraManager() }
    public val mIrCameraManager by lazy { CameraManager() }

    @Volatile
    var mIsReset = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val displayMetrics = DisplayMetrics()
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics)
        mScreenWidth = displayMetrics.widthPixels
        mScreenHeight = displayMetrics.heightPixels
    }

    override fun onStart() {
        super.onStart()
        initCamera()
    }

    override fun onResume() {
        super.onResume()
        initThread()
        mCameraManager.open(windowManager, mIsFront, getCameraView().width, getCameraView().height)
        if (mIsDoubleCamera) {
            mIrCameraManager.open(
                getWindowManager(),
                !mIsFront,
                getIrCameraView().width,
                getIrCameraView().height
            )
        }
    }

    override fun onPause() {
        stopThread()
        super.onPause()
    }

    override fun onStop() {
        mCameraManager.release()
        if (mIsDoubleCamera) {
            mIrCameraManager.release()
        }
        super.onStop()
    }

    override fun onDestroy() {
        mCameraManager.release()
        if (mIsDoubleCamera) {
            mIrCameraManager.release()
        }
        super.onDestroy()
    }


    override fun onRestart() {
        getFaceRectView().clearFaceList("===baseActivity--onRestart===")
        super.onRestart()
    }

    private fun initCamera() {
        if (mCameraManager.listener != null)
            return
        if (mCustomSize != null)
            mCameraManager.mCustomSize = mCustomSize
        mCameraManager.setPreviewDisplay(getCameraView())
        mCameraManager.setListener(this)
        if (mRectRotation == -1) {
            val windowRotation =
                (applicationContext.getSystemService(Context.WINDOW_SERVICE) as WindowManager).defaultDisplay.rotation * 90
            mRectRotation = when (windowRotation) {
                0 -> {
                    FacePassImageRotation.DEG90
                }
                90 -> {
                    FacePassImageRotation.DEG0
                }
                270 -> {
                    FacePassImageRotation.DEG180
                }
                else -> {
                    FacePassImageRotation.DEG270
                }
            }
        }
        mCameraManager.mRotate = mPrewRotation

        if (mIsDoubleCamera) {
            if (mCustomSize != null)
                mIrCameraManager.mCustomSize = mCustomSize

            mIrCameraManager.mRotate = mPrewRotation
            mIrCameraManager.setPreviewDisplay(getIrCameraView())
            mIrCameraManager.setListener(object : CameraManager.CameraListener {
                override fun onPictureTaken(cameraPreviewData: CameraPreviewData?) {
                    ComplexFrameHelper.addIRFrame(cameraPreviewData)
                }

                override fun onSupportedPreviewSizes(previewsSizes: MutableList<Point>?) {
                }
            })
        }
    }

    //启动检测与识别线程
    fun initThread() {
        mIsKill = false
        initCheckThread()
        initRecognizeThread()
    }

    //停检测与识别线程
    fun stopThread() {
        mIsKill = true
        if (mCheckThread != null) {
            try {
                mCheckThread?.interrupt()
                mCheckThread?.join()
            } catch (e: InterruptedException) {
                e.printStackTrace()
            }

        }
        if (mRecognizeThread != null) {
            try {
                mRecognizeThread?.interrupt()
                mRecognizeThread?.join()
            } catch (e: InterruptedException) {
                e.printStackTrace()
            }
        }
        mCheckFaceQueue.clear()
        mDetectResultQueue.clear()
    }


    //初始化检测线程
    private fun initCheckThread() {
        mCheckThread = Thread {
            while (!mIsKill && !Thread.interrupted()) {
                var framePair: Pair<CameraPreviewData?, CameraPreviewData?>? = null
                var cameraPreviewData: CameraPreviewData? = null
                try {
                    if (mIsDoubleCamera) {
                        framePair = ComplexFrameHelper.takeComplexFrame()
                    } else {
                        cameraPreviewData = mCheckFaceQueue.take()
                        mIsChecking = true
                    }
                } catch (e: InterruptedException) {
                    e.printStackTrace()
                    mIsChecking = false
                    logDebug("========check take result exception ${e.message}")
                    continue
                }
                if (mFaceManager.mFaceHandler == null ||
                    (mIsDoubleCamera && (framePair == null || framePair.first == null || framePair.second == null)) ||
                    (!mIsDoubleCamera && cameraPreviewData == null)
                ) {
                    mIsChecking = false
                    logTime("========faceTime====检测时长: ${System.currentTimeMillis() - mCheckTime}")
                    mIsCheck = false

                    logDebug("========check take result is null ${mFaceManager.mFaceHandler == null}")
                    continue
                }

                /* 将相机预览帧转成SDK算法所需帧的格式 FacePassImage */
                /* 将相机预览帧转成SDK算法所需帧的格式 FacePassImage */
//                val startTime = System.currentTimeMillis() //起始时间


                var imageRGB: FacePassImage
                var imageIR: FacePassImage? = null

                try {
                    if (mIsDoubleCamera) {
                        imageRGB = FacePassImage(
                            framePair!!.first!!.nv21Data,
                            framePair!!.first!!.width,
                            framePair!!.first!!.height,
                            FaceManager.instance.mFaceRotation,
                            FacePassImageType.NV21
                        )
                        imageIR = FacePassImage(
                            framePair.second!!.nv21Data,
                            framePair!!.second!!.width,
                            framePair!!.second!!.height,
                            FaceManager.instance.mFaceRotation,
                            FacePassImageType.NV21
                        )
                    } else {
                        imageRGB = FacePassImage(
                            cameraPreviewData!!.nv21Data,
                            cameraPreviewData!!.width,
                            cameraPreviewData!!.height,
                            FaceManager.instance.mFaceRotation,
                            FacePassImageType.NV21
                        )
                    }
                } catch (e: java.lang.Exception) {
                    e.printStackTrace()
                    mIsChecking = false
                    logDebug("========check take result imageRGB exception ${e.message}")
                    continue
                }


                /* 将每一帧FacePassImage 送入SDK算法， 并得到返回结果 */
                /* 将每一帧FacePassImage 送入SDK算法， 并得到返回结果 */
                var detectionResult: FacePassDetectionResult? = null
                try {
                    if (mIsDoubleCamera) {
                        detectionResult =
                            mFaceManager.mFaceHandler?.feedFrameRGBIR(imageRGB, imageIR);
                    } else {
                        detectionResult = mFaceManager.mFaceHandler?.feedFrame(imageRGB)
                    }
                } catch (e: FacePassException) {
                    logDebug("========check take result feedFrame  error ${e.message}")
                    e.printStackTrace()
                }

                if (detectionResult == null || detectionResult.faceList.size == 0) { /* 当前帧没有检出人脸 */
                    logDebug("========check take result 当前帧没有检出人脸")
                    runOnUiThread {
                        getFaceRectView().clearFaceList("========当前帧没有检出人脸=======")
                        if (mHasPerson) {
                            if (System.currentTimeMillis() - mLastChange > 500 && mNoPersonCount >= 5) {
                                mLastChange = System.currentTimeMillis()
                                mHasPerson = false
                                onHasFaceChange(mHasPerson)
                            } else if (mNoPersonCount < 5) {
                                //连续5帧没人才判断为没人防止抖动，解决重置时误触发
                                mNoPersonCount++
                            }
                        }
                        log("===================当前帧人脸数量为空")
                    }
                } else { /* 将识别到的人脸在预览界面中圈出，并在上方显示人脸位置及角度信息 */
                    if (!mHasPerson && System.currentTimeMillis() - mLastChange > 500) {
                        mNoPersonCount = 0
                        mLastChange = System.currentTimeMillis()
                        mHasPerson = true
                        mNoMaskTime = 0L
                        onHasFaceChange(mHasPerson)
                    }
                    val bufferFaceList = detectionResult.faceList
                    logDebug("===============识别到人 : ${bufferFaceList.size} / ${detectionResult.message}")
                    log("===============识别到人 : ${bufferFaceList.size} / ${detectionResult.message}")
                    runOnUiThread { showFacePassFace(imageRGB, bufferFaceList) }
                    /* bufferFaceList.forEach {
 //                        feedFrame错误码，仅返回feedFrame过程中第一次出现失败的错误码，具体对应关系如下：
 //                        0："FEEDFRAME_OK_CODE"，feedFrame过程中没有出现错误；
 //                        1："FEEDFRAME_ROLL_ERROR_CODE"，feedFrame过程中人脸质量检测的roll值大于阈值；
 //                        2： 人脸质量检测的pitch值大于阈值； " "， FEEDFRAME_PITCH_ERROR_CODE feedFrame过程中
 //                        3： 人脸质量检测的yaw值大于阈值； " "， FEEDFRAME_YAW_ERROR_CODE feedFrame过程中
 //                        4： 人脸质量检测的blur值大于阈值； " "， FEEDFRAME_BLUR_ERROR_CODE feedFrame过程中
 //                        5： 人脸质量检测的minface值小于阈值； " "， FEEDFRAME_MINFACE_ERROR_CODE feedFrame过程中
 //                        6： 人脸质量检测的halfface值小于阈值； " "， FEEDFRAME_HALFFACE_ERROR_CODE feedFrame过程中
 //                        7： 人脸质量检测被遮挡的部分大于阈值； " "， FEEDFRAME_OCCLUSION_ERROR_CODE feedFrame过程中
 //                        8： 人脸质量检测的brightness值没有处于阈值范围； " "， FEEDFRAME_BRIGHTNESS_ERROR_CODE feedFrame过程中
 //                        9：" "， FEEDFRAME_UNKNOWN_ERROR_CODE feedFrame过程中出现未知错误
                         log("======faceList: feedErrorCode: ${it.trackId} / ${it.facePassFeedFrameErrorCode}")
                     }*/


                    val firstFaceImage = detectionResult.images.firstOrNull()
                    /*离线模式，将识别到人脸的，message不为空的result添加到处理队列中*/
                    if (!isPauseRecognize && detectionResult != null && detectionResult.message.size != 0) {
                        //检测口罩模式
                        if (mMaskEnable) {
                            kotlin.run {
                                bufferFaceList.forEach {
                                    when (it.rcAttr.respiratorType) {
                                        FacePassRCAttribute.FacePassRespiratorType.SURGICAL,
                                        FacePassRCAttribute.FacePassRespiratorType.ANTI_POLLUTION,
                                        FacePassRCAttribute.FacePassRespiratorType.COMMON,
                                        FacePassRCAttribute.FacePassRespiratorType.KITCHEN_TRANSPARENT -> {
                                            log("====================检测到戴口罩", true)
                                            if (mOnlyCheck) {
                                                recognizeResult("", false, firstFaceImage)
                                                return@run
                                            }
                                            mDetectResultQueue.offer(
                                                CheckResult(
                                                    detectionResult.message,
                                                    1,
                                                    firstFaceImage
                                                )
                                            )
                                            return@run
                                        }
                                        else -> {
                                            log("====================检测到没戴口罩", true)
                                            if (System.currentTimeMillis() - mNoMaskTime > 2000) {
                                                mNoMaskTime = System.currentTimeMillis()
                                                if (mOnlyCheck) {
                                                    recognizeResult("", true, firstFaceImage)
                                                    return@run
                                                }
                                                mDetectResultQueue.offer(
                                                    CheckResult(
                                                        detectionResult.message,
                                                        0,
                                                        firstFaceImage
                                                    )
                                                )
                                            }
                                            return@run
                                        }
                                    }
                                }
                                log("====================无效人脸数据", true)
                            }
                        } else {
                            log("====================无口罩模式", true)
                            if (mOnlyCheck) {
                                recognizeResult("", false, firstFaceImage)
                            } else {
                                mDetectResultQueue.offer(
                                    CheckResult(
                                        detectionResult.message,
                                        -1,
                                        firstFaceImage
                                    )
                                )
                            }
                        }
                        log("====================结束检测", true)
                    } else {
                        log("====================face message为空", true)
                    }
                }
//                val endTime = System.currentTimeMillis() //结束时间
//                val runTime = endTime - startTime
//                log("======检测人脸时长: $runTime")
//                logTime("====facetime=======检测时长: $runTime")
                if (mIsCheck) {
                    logTime("========faceTime====检测时长: ${System.currentTimeMillis() - mCheckTime}")
                    mIsCheck = false
                }

                if (mIsReset) {
                    logTime("====facetime=======重置后检测时长: ${System.currentTimeMillis() - mResetTime}")
                    mIsReset = false
                }
                mIsChecking = false
            }
        }
    }


    open fun maskCheckFace(imageData: FacePassImage, rect: RectF) {

    }

    //初始化识别线程
    private fun initRecognizeThread() {
        mRecognizeThread = Thread(Runnable {
            while (!mIsKill && !Thread.interrupted()) {
                if (mFaceManager.mFaceHandler == null || isPauseRecognize) {
//                    log("==============handler error or pause")
                    continue
                }
                var detectionResult: CheckResult? = null
                try {
                    detectionResult = mDetectResultQueue.take()
                } catch (e: InterruptedException) {
                    log("=================take error")
                    e.printStackTrace()
                }

                if (detectionResult == null || detectionResult.message == null) {
                    log("===============人脸识别： detectionResult is null")
                    continue
                }

                log("===============开始识别")
                val start = System.currentTimeMillis()
                try {
                    val recognizeResult = mFaceManager.mFaceHandler!!.recognize(
                        FaceConstants.GROUP_NAME,
                        detectionResult.message
                    )
                    val end = System.currentTimeMillis()
                    val total = end - start
                    logTime("====facetime=======识别时长: $total")
                    if (mIsFaceTime) {
                        logTime("========识别总时长: ${System.currentTimeMillis() - mFaceTime}")
                        mIsFaceTime = false
                    }

                    var hasResult = false
                    var liveness = false
                    if (recognizeResult == null || recognizeResult.size == 0) {
                        log("======活体检测: 底库为空")
                        liveness = true
                    }

                    for (result in recognizeResult) {
                        val faceToken = String(result.faceToken)
                        log("===============识别结果：$faceToken")
                        log("==============识别结果11 livenessScore： ${result.detail.livenessScore}")
                        log("==============识别结果22 livenessThreshold： ${result.detail.livenessThreshold}")
                        log("==============识别结果33 recognitionState： ${result.recognitionState}")
                        log("==============识别结果33 livenessErrorCode： ${result.livenessErrorCode}")
//                        log("==============识别结果33 livenessErrorCode： ${result.}")
                        if (result.detail.livenessScore >= mFaceManager.mLivenessThreshold) {
                            liveness = true
                        }
                        if (!TextUtils.isEmpty(faceToken)) {
                            log("====================真的识别成功")
                            if (detectionResult.mask == -1 || detectionResult.mask == 1) {
                                recognizeResult(faceToken, false, detectionResult.image)
                            } else {
                                recognizeResult(faceToken, true, detectionResult.image)
                            }
                            hasResult = true
                            break
                        }
//                        if (FacePassRecognitionState.RECOGNITION_PASS == result.recognitionState) {
//                            log("====================真的识别成功")
//                            if (detectionResult.mask == -1 || detectionResult.mask == 1) {
//                                recognizeResult(faceToken, false, detectionResult.image)
//                            } else {
//                                recognizeResult(faceToken, true, detectionResult.image)
//                            }
//                            hasResult = true
//                            break
//                        }
                    }
                    if (!hasResult && liveness) {
                        if (detectionResult.mask == -1 || detectionResult.mask == 1) {
                            recognizeResult("", false, detectionResult.image)
                        } else {
                            recognizeResult("", true, detectionResult.image)
                        }
                    }
                } catch (e: FacePassException) {
                    e.printStackTrace()
                    recognizeResult("", false, detectionResult.image)
                    // 陌生人也会报recognize failed异常
//                    Logger.d("exception 识别出来陌生人")
                }

                // 能走到这一步，不为null说明是数据库有人，否则是陌生人，如果需要传识别记录照片，取检测时对应的NV21数据即可
                // 重置handler，避免识别出来一次人脸之后ksResult.message.length一直为0的问题
//                mFaceManager.mFaceHandler?.reset()
            }
        })

        mCheckThread?.start()
        mRecognizeThread?.start()
    }

    fun resetFace() {
        Log.e("BaseFaceActivity", "========重置人臉")
        mFaceManager.mFaceHandler?.reset()
        if (!mIsReset) {
            mResetTime = System.currentTimeMillis()
            mIsReset = true
        }
    }


    //接收相机帧开始检测
    fun onPreviewCallback(data: CameraPreviewData?) {
        logDebug("=====接收到相机帧：${data == null}")
        if (data != null) {
            if (mIsDoubleCamera) {
                ComplexFrameHelper.addRgbFrame(data)
            } else {
                if (!mIsCheck) {
                    mCheckTime = System.currentTimeMillis()
                    mIsCheck = true
                }

                if (!mIsFaceTime) {
                    mFaceTime = System.currentTimeMillis()
                    mIsFaceTime = true
                }
                if (!mIsChecking) {
                    mCheckFaceQueue.clear()
                    val offer = mCheckFaceQueue.offer(data)
                    logDebug("=====mCheckFaceQueue offer: $offer /  $mIsChecking / $mIsKill")
                }
            }

        }
    }


    private fun getSupportedPreviewSizes(camera: Camera) {
        val parameters = camera.parameters
        val supportedPreviewSizes =
            parameters.supportedPreviewSizes
        val previewsSizes: MutableList<Point> = arrayListOf()
        for (size in supportedPreviewSizes) {
            previewsSizes.add(Point(size.width, size.height))
        }
        onSupportedPreviewSizes(previewsSizes)
    }

    //提供给子类重写,人脸变化回调
    open fun onHasFaceChange(hasFace: Boolean) {

    }

    override fun onSupportedPreviewSizes(previewsSizes: MutableList<Point>) {

    }

    /**
     * 显示人脸框
     *
     * @param detectResult
     */
    private fun showFacePassFace(image: FacePassImage, detectResult: Array<FacePassFace>?) {
        //显示人脸框得时候，不用清理
//        getFaceRectView().clearFaceList("==========showFacePassFace======")
        if (detectResult == null)
            return

        detectResult.forEach { face ->
            var mirror = mCameraManager.front /* 前摄像头时mirror为true */
            if (mFaceRectMir == 1) {
                mirror = true
            } else if (mFaceRectMir == 2) {
                mirror = false
            }

            val mat = Matrix()
            val w: Int = getCameraView().getMeasuredWidth()
            val h: Int = getCameraView().getMeasuredHeight()

            val cameraHeight = mCameraManager.cameraheight
            val cameraWidth = mCameraManager.cameraWidth

            var left = 0f
            var top = 0f
            var right = 0f
            var bottom = 0f
            log("========人脸框角度：$mRectRotation")

            when (mRectRotation) {
                0 -> {
                    left = face.rect.left.toFloat()
                    top = face.rect.top.toFloat()
                    right = face.rect.right.toFloat()
                    bottom = face.rect.bottom.toFloat()
                    mat.setScale(if (mirror) (-1).toFloat() else 1.toFloat(), 1f)
                    mat.postTranslate(if (mirror) cameraWidth.toFloat() else 0f, 0f)
                    mat.postScale(
                        w.toFloat() / cameraWidth.toFloat(),
                        h.toFloat() / cameraHeight.toFloat()
                    )
                }
                90 -> {
                    mat.setScale(if (mirror) (-1).toFloat() else 1.toFloat(), 1f)
                    mat.postTranslate(if (mirror) cameraHeight.toFloat() else 0f, 0f)
                    mat.postScale(
                        w.toFloat() / cameraHeight.toFloat(),
                        h.toFloat() / cameraWidth.toFloat()
                    )
                    left = face.rect.top.toFloat()
                    top = cameraWidth - face.rect.right.toFloat()
                    right = face.rect.bottom.toFloat()
                    bottom = cameraWidth - face.rect.left.toFloat()
                }
                180 -> {
                    mat.setScale(1f, if (mirror) (-1).toFloat() else 1.toFloat())
                    mat.postTranslate(0f, if (mirror) cameraHeight.toFloat() else 0f)
                    mat.postScale(
                        w.toFloat() / cameraWidth.toFloat(),
                        h.toFloat() / cameraHeight.toFloat()
                    )
                    left = face.rect.right.toFloat()
                    top = face.rect.bottom.toFloat()
                    right = face.rect.left.toFloat()
                    bottom = face.rect.top.toFloat()
                }
                270 -> {
                    mat.setScale(if (mirror) (-1).toFloat() else 1.toFloat(), 1f)
                    mat.postTranslate(if (mirror) cameraHeight.toFloat() else 0f, 0f)
                    mat.postScale(
                        w.toFloat() / cameraHeight.toFloat(),
                        h.toFloat() / cameraWidth.toFloat()
                    )
                    left = cameraHeight - face.rect.bottom.toFloat()
                    top = face.rect.left.toFloat()
                    right = cameraHeight - face.rect.top.toFloat()
                    bottom = face.rect.right.toFloat()
                }
            }
            val drect = RectF()
            val srect = RectF(left, top, right, bottom)
            mat.mapRect(drect, srect)
            maskCheckFace(image, srect)
            getFaceRectView().addRect(drect)
        }
        getFaceRectView().invalidate()
    }

    //获取相机View
    abstract fun getCameraView(): CameraPreview

    abstract fun getIrCameraView(): CameraPreview

    //获取人脸框View
    abstract fun getFaceRectView(): FaceCircleViewNew

    //识别结果回调
    abstract fun recognizeResult(faceToken: String, noMask: Boolean, image: FacePassImage?)

    //相机帧回调
    abstract fun cameraPreview(nv21: ByteArray, width: Int, height: Int, rotation: Int)


    fun switchCamera(isFront: Boolean) {
        this.mIsFront = isFront
        mCameraManager.switchCamera(windowManager, isFront)
    }

    private fun nv21Roate(
        cameraStreamRatation: Int,
        nv21_data: ByteArray,
        width: Int,
        height: Int
    ): ByteArray {
        when (cameraStreamRatation) {
            0 -> return nv21_data
            1 -> return FacePassTool.NV21_rotate_to_90(nv21_data, width, height)
            2 -> return FacePassTool.NV21_rotate_to_180(nv21_data, width, height)
            3 -> return FacePassTool.NV21_rotate_to_270(nv21_data, width, height)
        }
        return nv21_data
    }


    private fun log(msg: String, error: Boolean = false) {
//        if (error) {
//            Log.e("BaseFaceActivity", msg)
//        } else {
//            Log.i("BaseFaceActivity", msg)
//        }
    }

    private fun logTest(msg: String) {
//        Log.e("BaseFaceActivity", msg)
    }

    private fun logTime(msg: String) {
//        Log.e("FaceTime", msg)
    }

    private fun logDebug(msg: String) {
//        Log.d("BaseFaceActivity", "debug:" + msg)
    }

    override fun onPictureTaken(cameraPreviewData: CameraPreviewData?) {
        onPreviewCallback(cameraPreviewData)
        cameraPreviewData?.let {
            cameraPreview(
                cameraPreviewData.nv21Data,
                cameraPreviewData.width,
                cameraPreviewData.height,
                cameraPreviewData.rotation
            )
        }
    }


}