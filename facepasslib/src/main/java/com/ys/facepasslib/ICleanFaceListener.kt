package com.ys.facepasslib

// Created by kermitye on 2019/11/5 15:52
interface ICleanFaceListener {
    fun onCleanFaceListener(isSuccess: Boolean, msg: String)
}