package com.ys.facepasslib

// Created by kermitye on 2019/11/4 11:56
interface FaceInitListener {
    fun onSuccess()
    fun onError(code: Int, msg: String)
}