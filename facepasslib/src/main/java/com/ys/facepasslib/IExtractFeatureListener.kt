package com.ys.facepasslib

// Created by kermitye on 2019/11/11 11:28
interface IExtractFeatureListener {
    fun onExtractFeature(success: Boolean, msg: String)
}