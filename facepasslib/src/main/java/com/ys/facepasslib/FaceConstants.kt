package com.ys.facepasslib

import android.os.Environment
import java.io.File

// Created by kermitye on 2019/11/4 11:59
object FaceConstants {

    //人脸识别相关存放路径
    @JvmStatic
    val FACE_PATH = Environment.getExternalStorageDirectory().path + File.separator + "face/facepass"

    //特征码文件存放路径
    @JvmStatic
    val FEATURE_PATH = FACE_PATH + "${File.separator}feature"

    //底库名称
    const val GROUP_NAME = "ys_group"

}