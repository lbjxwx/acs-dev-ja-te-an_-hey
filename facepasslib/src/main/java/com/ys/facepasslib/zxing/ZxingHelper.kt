package com.ys.facepasslib.zxing

import android.app.Activity
import android.graphics.Bitmap
import com.google.zxing.Result
import com.ys.facepasslib.camera.CameraManagerZxing
import com.ys.facepasslib.util.FaceLogUtil
import java.lang.ref.WeakReference

// Created by kermitye on 2020/12/7 4:02 PM
class ZxingHelper(var scanView: ViewfinderView?, var activity: Activity, var cameraManager: CameraManagerZxing?) :
    OnCaptureListener {

    //扫码结果回调
    private var listener: ScanResultListener? = null
    private var context: WeakReference<Activity>
    private var captureHandler: CaptureHandler? = null


    init {
        context = WeakReference(activity)
    }


    fun initConfig() {
        captureHandler = CaptureHandler(context.get(), scanView, this, null, null, null, cameraManager)
    }

    fun setScanResultListener(listener: ScanResultListener?) {
        this.listener = listener
    }

    //开始扫码模式
    fun startScan() {
        captureHandler?.restartPreviewAndDecode()
    }

    //关闭扫码模式
    fun stopScan() {
        captureHandler?.stopScan()
//        captureHandler?.quitSynchronously()
    }


    fun release() {
        captureHandler?.quitSynchronously()
        listener = null
        scanView = null
    }

    override fun onHandleDecode(result: Result?, barcode: Bitmap?, scaleFactor: Float) {
        FaceLogUtil.d("=======onHandleDecode: ${result?.text}")
        //识别成功后会自动停止扫码
        listener?.onScanResult(result?.text ?: "")

    }


}