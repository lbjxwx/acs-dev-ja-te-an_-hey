package com.ys.facepasslib.zxing

// Created by kermitye on 2020/12/8 3:55 PM
interface ScanResultListener {
    fun onScanResult(text: String)
}