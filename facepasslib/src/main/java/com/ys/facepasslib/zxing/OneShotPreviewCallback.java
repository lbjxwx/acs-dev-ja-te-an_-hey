package com.ys.facepasslib.zxing;

import android.graphics.Point;
import android.hardware.Camera;
import android.os.Handler;
import android.os.Message;

import com.ys.facepasslib.util.FaceLogUtil;

// Created by kermitye on 2020/12/8 4:47 PM
public class OneShotPreviewCallback implements Camera.PreviewCallback {

    private static final String TAG = OneShotPreviewCallback.class.getSimpleName();

    private Handler previewHandler;
    private int previewMessage;
    private int width = -1;
    private int height = -1;

    public void setSize(int width, int height) {
        this.width = width;
        this.height = height;
    }

    public void setHandler(Handler previewHandler, int previewMessage) {
        this.previewHandler = previewHandler;
        this.previewMessage = previewMessage;
    }

    @Override
    public void onPreviewFrame(byte[] data, Camera camera) {
        FaceLogUtil.d("=====OneShotPreviewCallback onPreviewFrame");
        Handler thePreviewHandler = previewHandler;
        if (width != -1 && thePreviewHandler != null) {
            FaceLogUtil.d("send msg: " + previewMessage);
            Message message = thePreviewHandler.obtainMessage(previewMessage, width,
                    height, data);
            message.sendToTarget();
            previewHandler = null;
        } else {
            FaceLogUtil.d("Got preview callback, but no handler or resolution available");
        }
    }

}