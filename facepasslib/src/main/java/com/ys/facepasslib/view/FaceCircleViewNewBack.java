package com.ys.facepasslib.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.RequiresApi;

import com.ys.facepasslib.listener.FaceRecDisListener;
import com.ys.facepasslib.util.WavaFilterUtil;

import java.util.ArrayList;
import java.util.List;

/***
 * 针对 8 寸屏幕设置的
 * 不适配大屏
 */
public class FaceCircleViewNewBack extends View {

    public FaceCircleViewNewBack(Context context) {
        this(context, null);
    }

    public FaceCircleViewNewBack(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public FaceCircleViewNewBack(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initData();
    }

    Paint paint;
    //用来封装绘制锚点得集合
    List<Rect> rectList = null;

    //额头的坐标位置
    public Point forehead = new Point();


    private void initData() {
        paint = new Paint();
        rectList = new ArrayList<Rect>();
        paint.setColor(defaultColor);
        paint.setStyle(Paint.Style.FILL);
        paint.setStrokeWidth(5);
    }

    public void addRect(RectF rect) {
        Rect buffer = new Rect();
        buffer.left = (int) rect.left;
        buffer.top = (int) rect.top;
        buffer.right = (int) rect.right;
        buffer.bottom = (int) rect.bottom;
        if (buffer != null) {
            rectList.add(buffer);
            backFaceRecDistance(rect);
            if (rectList.size() > WavaFilterUtil.algorithmum * 10) {
                rectList.remove(0);
            }
        }
    }

    //用来收集数据得集合
//    List<Float> floatList = new ArrayList<>();
    //用来表示人脸是否在画面中间
    boolean isCenterDraw = true;
    private int screenWidth = 800;
    private int screenHeight = 1280;


    /***
     * 这里得customId 参考 AppConfig里面得客户 ID
     * @param width
     * @param height
     */
    public void setScreenWidth(int width, int height) {
        this.screenWidth = width;
        this.screenHeight = height;
    }

    private int areaWidth = 200;
    private int areaHeight = 300;
    private int defaultColor = Color.WHITE;
    private int wearColor = Color.RED;

    private void backFaceRecDistance(RectF rect) {
        if (faceRecDisListener == null) {
            return;
        }
        if (screenHeight < 1 || screenWidth < 1) {
            return;
        }
        float left = rect.left;
        float right = rect.right;
        float top = rect.top;
        float bottom = rect.bottom;
        float distanceHeight = bottom - top;
        float centerHeightPosition = top + (distanceHeight / 2);
        float distanceWidth = right - left;
        float centerWidthPosition = left + (distanceWidth / 2);
        boolean isCenter = true;
        if (Math.abs(centerWidthPosition - screenWidth / 2) > areaWidth) {
            isCenter = false;
        }
        if (Math.abs(centerHeightPosition - screenHeight / 2) > areaHeight) {
            isCenter = false;
        }
        isCenterDraw = isCenter;
        float distanceNum = WavaFilterUtil.mathDistance(distanceHeight);
//        Log.e("cdl", "=====Group======" + distanceNum + " / " + distanceWidth);
        if (faceRecDisListener != null) {
            faceRecDisListener.rectDistance(distanceNum, isCenter, distanceHeight, forehead);
        }
//        floatList.add(distanceWidth);
//        if (floatList.size() > WavaFilterUtil.algorithmum * WavaFilterUtil.algorithmum) {
//            float distanceNum = WavaFilterUtil.dealalgorithmumInfo(floatList);
//            Log.e("cdl", "=====Group======" + distanceNum + " / " + distanceWidth);
//            if (faceRecDisListener != null) {
//                faceRecDisListener.rectDistance(distanceNum, isCenter, distanceHeight);
//            }
//            floatList.clear();
//        }
    }

    /***
     * 清理原则：
     * 1：程序启动，界面启动
     * 2：界面执行onReStart  onResume
     * 3: 检测不到人脸
     * @param tag
     */
    public void clearFaceList(String tag) {
        if (rectList != null) {
            rectList.clear();
        }
        invalidate();
    }

    int lineLength = 30;
    int lineWidth = 3;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (screenHeight < 1 || screenWidth < 1) {
            return;
        }
        if (rectList == null || rectList.size() < 2) {
            return;
        }
        if (isCenterDraw) {
            paint.setColor(defaultColor);
            lineLength = 30;
            lineWidth = 3;
        } else {
            paint.setColor(wearColor);
            lineLength = 50;
            lineWidth = 10;
        }
        Rect rect = rectList.get(rectList.size() - 1);
        int left = rect.left;
        int top = rect.top;
        int right = rect.right;
        int bottom = rect.bottom;
        //绘制识别区域
        //canvas.drawRect(200, 440, 600, 840, paint);
        //左上角两个点
        canvas.drawRect(left, top, left + lineLength, top + lineWidth, paint);
        canvas.drawRect(left, top, left + lineWidth, top + lineLength, paint);
        //右上角两个点
        canvas.drawRect(right - lineLength, top, right, top + lineWidth, paint);
        canvas.drawRect(right - lineWidth, top, right, top + lineLength, paint);
        //左下角得两个点
        canvas.drawRect(left, bottom - lineWidth, left + lineLength, bottom, paint);
        canvas.drawRect(left, bottom - lineLength, left + lineWidth, bottom, paint);
        //右下角得两个点
        canvas.drawRect(right - lineLength, bottom - lineWidth, right, bottom, paint);
        canvas.drawRect(right - lineWidth, bottom - lineLength, right, bottom, paint);
//        Log.d("FaceCircleViewNew", "========face rect position: left=" + left + ", top=" + top + ", right=" + right + ", bottom=" + bottom);
        forehead.x = (right - left) / 2 + left;
        if (forehead.x < 0)
            forehead.x = 0;
        forehead.y = top;
        if (forehead.y < 0)
            forehead.y = 0;
    }

    public void setFaceRectDisListener(FaceRecDisListener faceRecDisListener) {
        this.faceRecDisListener = faceRecDisListener;
    }

    FaceRecDisListener faceRecDisListener;

}
