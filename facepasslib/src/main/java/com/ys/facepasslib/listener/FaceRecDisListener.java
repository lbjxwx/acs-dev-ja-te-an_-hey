package com.ys.facepasslib.listener;

import android.graphics.Point;

/**
 * 识别距离计算方法
 */
public interface FaceRecDisListener {
    //识别距离
    //单位厘米
    void rectDistance(float distance, boolean isCenter, float rectWidth, Point headPoint);
}