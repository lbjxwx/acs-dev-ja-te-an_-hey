package com.ys.facepasslib.util;

import android.util.Log;

// Created by kermitye on 2020/12/8 4:51 PM
public class FaceLogUtil {
    private static final String TAG = "FaceLogUtil";


    public static void d(String msg) {
        Log.d(TAG, msg);
    }

}
