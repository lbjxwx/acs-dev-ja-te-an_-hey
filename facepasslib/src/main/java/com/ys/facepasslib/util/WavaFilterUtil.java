package com.ys.facepasslib.util;


import java.util.ArrayList;
import java.util.List;

public class WavaFilterUtil {

    /**
     * 这个参数是用来换算滤波算法得轮询值
     */
    public static final int algorithmum = 3;
//    public static final int algorithmum = 5;

    /***
     * 距离算法
     * @param floatList
     * @return
     */
    public static float dealalgorithmumInfo(List<Float> floatList) {
        List<Float> list1 = new ArrayList<Float>();
        List<Float> list2 = new ArrayList<Float>();
        List<Float> list3 = new ArrayList<Float>();
//        List<Float> list4 = new ArrayList<Float>();
//        List<Float> list5 = new ArrayList<Float>();
        float Group1 = 0;
        float Group2 = 0;
        float Group3 = 0;
//        float Group4 = 0;
//        float Group5 = 0;
        float groupAverage = 0;
        for (int i = 0; i < floatList.size(); i++) {
            if (i < algorithmum) {
                Group1 = (Group1 + floatList.get(i)) / algorithmum;
                list1.add(floatList.get(i));
            } else if ((i > algorithmum - 1) && i < algorithmum * 2) {
                Group2 = (Group2 + floatList.get(i)) / algorithmum;
                list2.add(floatList.get(i));
            } else if ((i > algorithmum * 2 - 1) && i < algorithmum * 3) {
                Group3 = (Group3 + floatList.get(i)) / algorithmum;
                list3.add(floatList.get(i));
            }
//            else if ((i > algorithmum * 3 - 1) && i < algorithmum * 4) {
//                Group4 = (Group4 + floatList.get(i)) / algorithmum;
//                list4.add(floatList.get(i));
//            } else if ((i > algorithmum * 4 - 1) && i < algorithmum * 5) {
//                Group5 = (Group5 + floatList.get(i)) / algorithmum;
//                list5.add(floatList.get(i));
//            }
        }
        //尺寸框得大小
//        groupAverage = (Group1 + Group2 + Group3 + Group4 + Group5) / algorithmum;
        groupAverage = (Group1 + Group2 + Group3) / algorithmum;
        return mathDistance(groupAverage);
    }

    public static float mathDistance(float groupAverage) {
        float backDistanceNum = 30;
        if (groupAverage > 800 && groupAverage < 1150) {
            backDistanceNum = 20;
        } else if (groupAverage > 700 && groupAverage < 800) {
            backDistanceNum = 20 + ((groupAverage - 700) / 10);
        } else if (groupAverage > 600 && groupAverage < 700) {
            backDistanceNum = 30;
        } else if (groupAverage > 535 && groupAverage < 600) {
            backDistanceNum = 30 + ((groupAverage - 535) / ((600 - 535) / 10));
        } else if (groupAverage > 410 && groupAverage < 535) {
            backDistanceNum = 40;
        } else if (groupAverage > 340 && groupAverage < 420) {
            backDistanceNum = 50;
        } else if (groupAverage > 280 && groupAverage < 350) {
            backDistanceNum = 60;
        } else if (groupAverage > 240 && groupAverage < 300) {
            backDistanceNum = 70;
        } else if (groupAverage > 200 && groupAverage < 260) {
            backDistanceNum = 80;
        } else if (groupAverage > 0 && groupAverage < 200) {
            backDistanceNum = 100;
        }
        return backDistanceNum;
    }


//    private static float mathDistance(float groupAverage) {
//        //10  200 197
////        Log.e("cdl", "===========距离===" + groupAverage);
//        if (groupAverage > 120 && groupAverage < 138) {
//            //15    128-130  127 138
//            return 15;
//        } else if (groupAverage > 110 && groupAverage < 121) {
//            //20     110  120
//            return 20;
//        } else if (groupAverage > 95 && groupAverage < 111) {
//            //25    100-102
//            return 25;
//        } else if (groupAverage > 80 && groupAverage < 96) {
//            //30    80
//            return 30;
//        } else if (groupAverage > 70 && groupAverage < 81) {
//            //35    74
//            return 30;
//        } else if (groupAverage > 60 && groupAverage < 71) {
//            //40    68/65
//            return 40;
//        } else if (groupAverage > 55 && groupAverage < 61) {
//            //45    58
//            return 45;
//        } else if (groupAverage > 45 && groupAverage < 56) {
//            //50 cm 55
//            return 50;
//        } else if (groupAverage > 38 && groupAverage < 46) {
//            //70    40
//            return 70;
//        } else if (groupAverage > 30 && groupAverage < 39) {
//            //80    35
//            return 80;
//        } else if (groupAverage < 31) {
//            return 101;
//        } else {
//            return 10;
//        }
//    }
}
