//package com.ys.facepasslib.util;
//
//import java.io.BufferedOutputStream;
//import java.io.File;
//import java.io.FileOutputStream;
//
//public class FacePassTool {
//
//    private byte[] NV21_rotate_to_270(byte[] nv21_data, int width, int height) {
//        int y_size = width * height;
//        int buffser_size = y_size * 3 / 2;
//        byte[] nv21_rotated = new byte[buffser_size];
//        int i = 0;
//        // Rotate the Y luma
//        for (int x = width; x > 0; x--) {
//            int offset = 0;
//            for (int y = 0; y < height; y++) {
//                nv21_rotated[i] = nv21_data[offset + x];
//                i++;
//                offset += width;
//            }
//        }
//        for (int x = width; x > 0; x--) {
//            int offset = 0;
//            for (int y = 0; y < height; y++) {
//                nv21_rotated[i] = nv21_data[offset + x];
//                i++;
//                offset += width;
//            }
//        }
//        // Rotate the U and V color components
//        i = y_size;
//        int x = width - 1;
//        while (x > 0) {
//            int offset = y_size;
//            for (int y = 0; y < height / 2; y++) {
//                nv21_rotated[i] = nv21_data[offset + (x - 1)];
//                i++;
//                nv21_rotated[i] = nv21_data[offset + x];
//                i++;
//                offset += width;
//            }
//            x = x - 2;
//        }
//        return nv21_rotated;
//    }
//
//    public byte[] NV21_rotate_to_180(byte[] nv21_data, int width, int height) {
//        long startTime = System.currentTimeMillis();
//        int y_size = width * height;
//        int buffser_size = y_size * 3 / 2;
//        byte[] nv21_rotated = new byte[buffser_size];
//        int i = 0;
//        int count = 0;
//        i = y_size - 1;
//        while (i >= 0) {
//            nv21_rotated[count] = nv21_data[i];
//            count++;
//            i--;
//        }
//
//        i = buffser_size - 1;
//        while (i >= y_size) {
//            nv21_rotated[count++] = nv21_data[i - 1];
//            nv21_rotated[count++] = nv21_data[i];
//            i -= 2;
//        }
//        long endTime = System.currentTimeMillis();
//        long total = endTime - startTime;
//        // Logger.d("cameraview", "旋转耗时：" + total);
//        return nv21_rotated;
//    }
//
//    public byte[] NV21_rotate_to_90(byte[] nv21_data, int width, int height) {
//        int y_size = width * height;
//        int buffser_size = y_size * 3 / 2;
//        byte[] nv21_rotated = new byte[buffser_size];
//        // Rotate the Y luma
//        int i = 0;
//        int startPos = (height - 1) * width;
//        for (int x = 0; i < width; i++) {
//            int offset = startPos;
//            for (int y = height; y > 0; y--) {
//                nv21_rotated[i] = nv21_data[offset + x];
//                i++;
//                offset -= width;
//            }
//        }
//        i = buffser_size - 1;
//        int x = width - 1;
//        while (x > 0) {
//            int offset = y_size;
//
//            for (int y = 0; y < height / 2; i++) {
//                nv21_rotated[i] = nv21_data[offset + x];
//                i--;
//                nv21_rotated[i] = nv21_data[offset + (x - 1)];
//                i--;
//                offset += width;
//            }
//            x = x - 2;
//        }
//        return nv21_rotated;
//    }
//
//    public static void deleteFile(File file) {
//        if (!file.exists())
//            return;
//        if (file.isFile()) {
//            file.delete();
//            return;
//        }
//        File[] files = file.listFiles();
//        if (files == null || file.length() < 1) {
//            return;
//        }
//        for (int i = 0; i < files.length; i++) {
//            File fileDir = files[i];
//            deleteFile(fileDir);
//        }
//    }
//
//    public void saveFile(byte[] data, String path) {
//        try {
//            File file = new File(path);
//            BufferedOutputStream mBos = new BufferedOutputStream(new FileOutputStream(file));
//            mBos.write(data);
//            mBos.flush();
//            mBos.close();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//}
