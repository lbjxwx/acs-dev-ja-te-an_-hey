package com.ys.facepasslib

// Created by kermitye on 2019/11/5 15:12
interface IRegisterFaceListener {
    fun onSuccess(faceToken: String, featurePath: String)
    fun onError(code: Int, msg: String)
}