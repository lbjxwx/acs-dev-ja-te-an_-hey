package com.ys.facepasslib;

import mcv.facepass.types.FacePassImage;

// Created by kermitye on 2020/3/17 17:09
public class CheckResult {
    public byte[] message;
    public int mask;
    public FacePassImage image;

    public CheckResult(byte[] message, int mask) {
        this(message, mask, null);
    }

    public CheckResult(byte[] message, int mask, FacePassImage image) {
        this.message = message;
        this.mask = mask;
        this.image = image;
    }
}
