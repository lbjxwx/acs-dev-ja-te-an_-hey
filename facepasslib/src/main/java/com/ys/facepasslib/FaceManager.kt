package com.ys.facepasslib

import android.content.Context
import android.content.SharedPreferences
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Handler
import android.os.Looper
import android.os.Message
import android.util.Log
import android.view.WindowManager
import androidx.core.content.edit
import com.ys.ysfacelib.FaceAuthListener
import com.ys.ysfacelib.FaceHelper
import mcv.facepass.FacePassException
import mcv.facepass.FacePassHandler
import mcv.facepass.types.*
import java.io.File
import java.util.concurrent.ArrayBlockingQueue
import java.util.concurrent.ExecutorService
import java.util.concurrent.ThreadPoolExecutor
import java.util.concurrent.TimeUnit
import kotlin.concurrent.thread


// Created by kermitye on 2019/11/4 11:14
class FaceManager {

    companion object {
        @JvmStatic
        val instance: FaceManager by lazy { FaceManager() }
    }

    private var FACE_APPID = ""
    private val MSG_INIT_SDK = 1


    private var mContext: Context? = null

    private var mInitListener: FaceInitListener? = null
    private var mConfig: FacePassConfig? = null

    @Volatile
    var mFaceHandler: FacePassHandler? = null
    var mFaceRotation = 270  //识别算法角度
    var mFaceMinThreshold = 130 //最小人脸尺寸
    var mMaskEnable = false    //口罩检测
    var mIrLivessEnable = false //双目活体检测
    var mSingleLivessEnable = true //单目活体默认开
    var mSearchThreshold = 80f   //识别阀值
    var mLivenessThreshold = 65f //活体阀值

    lateinit var sharedPref: SharedPreferences

    @Volatile
    var mIsInit = false

    var mInitSdkRunn: Runnable? = null

    var executorService: ExecutorService = ThreadPoolExecutor(
        1, 10,
        0, TimeUnit.SECONDS,
        ArrayBlockingQueue(10)  // 使用有界队列，避免OOM
    )

    private var mHandler = object : Handler(Looper.getMainLooper()) {
        override fun handleMessage(msg: Message?) {
            super.handleMessage(msg)
            val what = msg?.what ?: -1
            when (what) {
                MSG_INIT_SDK -> {
                    if (mIsInit)
                        return
                    initSDK()
                }
            }
        }
    }

    //初始化
    fun init(appContext: Context, appId: String, listener: FaceInitListener?) {
        sharedPref = appContext.getSharedPreferences("facepasslib", Context.MODE_PRIVATE)
        FACE_APPID = appId
        mInitListener = listener
        mContext = appContext
//        mContext = WeakReference(appContext)
        var auth = sharedPref.getBoolean("auth", false)
        if (!auth) {
            auth(appContext, appId)
        } else {
            log("已授权过一次，跳过授权，初始化人脸识别")
            FacePassHandler.initSDK(mContext)
            mHandler.sendEmptyMessageDelayed(MSG_INIT_SDK, 500)
        }

//        FaceHelper.instance.setAuthListener(object : FaceAuthListener {
//            override fun onAuth(isSuccess: Boolean, msg: String) {
//                log("=====================onAuth: $isSuccess / $msg")
//                if (isSuccess) {
//                    FacePassHandler.initSDK(appContext)
//                    mHandler.sendEmptyMessageDelayed(MSG_INIT_SDK, 500)
//                } else {
//                    mInitListener?.onError(1, msg)
//                }
//            }
//        })
//        FaceHelper.instance.getAuth(appContext, FACE_APPID, AUTH_LINK)
    }


    private fun auth(appContext: Context, appId: String) {
        FaceHelper.instance.setAuthListener(object : FaceAuthListener {
            override fun onAuth(code: Int, msg: String) {
                log("=====================onAuth: $code / $msg")
                if (code == 0) {
                    //授权成功
                    sharedPref.edit { putBoolean("auth", true) }
                    FacePassHandler.initSDK(mContext)
                    mHandler.sendEmptyMessageDelayed(MSG_INIT_SDK, 500)
                } else {
                    mInitListener?.onError(code, msg)
                }
            }
        })
        FaceHelper.instance.getAuth(appContext, appId)
    }

    fun initSDK() {
        if (mInitSdkRunn == null) {
            mInitSdkRunn = object : Runnable {
                override fun run() {
                    mIsInit = true
                    log("=======================initSDK: ${FacePassHandler.isAvailable()}")
                    if (FacePassHandler.isAvailable()) {
                        //授权成功
                        val config = if (mConfig == null) buildConfig(mContext) else mConfig
                        try {
                            mFaceHandler = FacePassHandler(config)
                            if (mFaceHandler == null) {
                                mInitListener?.onError(1, "SDK初始化失败: FaceHandler is null")
                                return
                            }
                            //设置注册的阀值
                            val addFaceConfig: FacePassConfig = mFaceHandler!!.getAddFaceConfig()
                            addFaceConfig.faceMinThreshold = 100
                            addFaceConfig.blurThreshold = 0.8f
                            addFaceConfig.brightnessSTDThreshold = 120f
                            //addFaceConfig.rotation = 0
                            mFaceHandler!!.setAddFaceConfig(addFaceConfig)
                            /* // 设置入库的参数开始 放到底 10，50，0.8，10，250，250时可以全部入库
                             // 设置入库的参数开始 正常参数 50，30，0.4，50，210，80时可以全部入库
                             val addFaceConfig = mFaceHandler!!.addFaceConfig
                             addFaceConfig.faceMinThreshold = 10
                             addFaceConfig.poseThreshold = FacePassPose(50f, 50f, 50f)
                             // 模糊的，越大越容易通过
                             addFaceConfig.blurThreshold = 0.6f
                             addFaceConfig.lowBrightnessThreshold = 10f
                             addFaceConfig.highBrightnessThreshold = 250f
                             addFaceConfig.brightnessSTDThreshold = 250f
                             mFaceHandler!!.setAddFaceConfig(addFaceConfig)
                             // 设置入库的参数结束*/
                            //创建底库
                            val isSuccess = createGroup()
                            log("=========创建底库: $isSuccess")
                            //至此初始化成功
                            mInitListener?.onSuccess()
                            mIsInit = false
                            addDefaultFace()
                        } catch (e: FacePassException) {
                            e.printStackTrace()
                            mInitListener?.onError(2, "SDK初始化失败: " + e.message)
                            mHandler.sendEmptyMessageDelayed(MSG_INIT_SDK, 1500)
                            mIsInit = false
                        }
                    } else {
                        //授权失败或还未授权完成,500毫秒后再次检测
                        mHandler.sendEmptyMessageDelayed(MSG_INIT_SDK, 1000)
                        mIsInit = false
                    }
                }
            }
        }
        executorService.execute(mInitSdkRunn)
    }


    fun setInitListener(listener: FaceInitListener?) {
        mInitListener = listener
    }

    //设置参数,在init之前设置，或者设置完后调用initSDK重新初始化SDK
    fun setConfig(config: FacePassConfig?) {
        this.mConfig = config
    }

    //获取配置
    fun buildConfig(appContext: Context?): FacePassConfig? {
        if (appContext == null) {
            return null
        }
        if (mContext == null) {
            mContext = appContext
        }

        var config = FacePassConfig()
        config.poseBlurModel = FacePassModel.initModel(
            mContext!!.getAssets(),
            "attr.pose_blur.align.av200.190630.bin"
        )

        config.livenessModel = FacePassModel.initModel(
            mContext!!.getAssets(),
            "liveness.CPU.rgb.int8.E.bin"
        )

        if (mIrLivessEnable) {
            //双目使用CPU rgbir活体模型
            config.rgbIrLivenessModel = FacePassModel.initModel(
                mContext!!.getAssets(),
                "liveness.CPU.rgbir.int8.E.bin"
            )
        }

        //当单目或者双目有一个使用GPU活体模型时，请设置livenessGPUCache
        //config.livenessGPUCache = FacePassModel.initModel(getApplicationContext().getAssets(), "liveness.GPU.AlgoPolicy.D.cache");

        config.searchModel = FacePassModel.initModel(
            mContext!!.getAssets(),
            "feat2.arm.H.v1.0_1core.bin"
        )
        config.detectModel = FacePassModel.initModel(
            mContext!!.getAssets(),
            "detector.arm.E.bin"
        )
        config.detectRectModel = FacePassModel.initModel(
            mContext!!.getAssets(),
            "detector_rect.arm.E.bin"
        )
        config.landmarkModel = FacePassModel.initModel(
            mContext!!.getAssets(),
            "pf.lmk.arm.D.bin"
        )

        config.rcAttributeModel = FacePassModel.initModel(
            mContext!!.getAssets(),
            "attr.RC.gray.12M.arm.200229.bin"
        )


        //                            config.smileModel = FacePassModel.initModel(OpenAIApplication.getInstance().getAssets(), "attr.blur.align.gray.general.mgf29.0.1.1.181229.bin");
        //                            config.ageGenderModel = FacePassModel.initModel(OpenAIApplication.getInstance().getAssets(), "age_gender.v2.bin");
        //如果不需要表情和年龄性别功能，smileModel和ageGenderModel可以为null
        config.smileModel = null
        config.ageGenderModel = null
        config.rcAttributeEnabled = true
        config.searchThreshold = mSearchThreshold //未带口罩时，识别使用的阈值
        //活体阀值
        config.livenessThreshold = mLivenessThreshold
        //单目活体检测开关
        config.livenessEnabled = mSingleLivessEnable
        Log.e("liveCheck", "=====活体检测===" + mIrLivessEnable)
        //双目活体开关
        config.rgbIrLivenessEnabled = mIrLivessEnable
        // ageGenderEnabledGlobal = (config.ageGenderModel != null);
        //最小人脸值
        config.faceMinThreshold = mFaceMinThreshold
        config.poseThreshold = FacePassPose(30f, 30f, 30f)
        config.blurThreshold = 0.8f //0.5
        // config.blurThreshold = 0.2f;
        //人脸平均照度阈值。
        config.lowBrightnessThreshold = 45f //70
        //人脸平均照度阈值。
        config.highBrightnessThreshold = 220f   //210
        //人脸照度标准差阈值。
        config.brightnessSTDThreshold = 100f //60f
        config.retryCount = 5
        config.smileEnabled = false
        config.maxFaceEnabled = true

        //系统相机角度
        var cameraRotation = 0
        val windowRotation =
            (mContext!!.getSystemService(Context.WINDOW_SERVICE) as WindowManager)
                .defaultDisplay.rotation * 90
        if (windowRotation == 0) {
            cameraRotation = FacePassImageRotation.DEG90
        } else if (windowRotation == 90) {
            cameraRotation = FacePassImageRotation.DEG0
        } else if (windowRotation == 270) {
            cameraRotation = FacePassImageRotation.DEG180
        } else {
            cameraRotation = FacePassImageRotation.DEG270
        }
        //设置算法角度
//        config.rotation =
//            if (mFaceRotation == -1) cameraRotation else mFaceRotation

        val file = File(FaceConstants.FACE_PATH)
        if (!file.exists()) {
            file.mkdirs()
        }
        config.fileRootPath = FaceConstants.FACE_PATH
        return config
    }

    fun registerByFile(file: File, listener: IRegisterFaceListener) {
        if (!file.exists()) {
            log("register By file file not exists")
            listener.onError(-1, "register By file file not exists")
            return
        }
        val bitmap = BitmapFactory.decodeFile(file.absolutePath)
        if (bitmap == null) {
            log("register By file bitmap is null")
            listener.onError(-2, "register By file bitmap is null")
            return
        }
        registerFace(bitmap, listener)
    }

    /**
     * 根据bitmap注册人脸
     */
    fun registerFace(bitmap: Bitmap, listener: IRegisterFaceListener? = null) {
        registerFace(bitmap, false, listener)
    }


    /**
     * 注册人脸
     * [getFeature] 是否获取特征码文件
     */
    fun registerFace(
        bitmap: Bitmap,
        getFeature: Boolean = false,
        listener: IRegisterFaceListener? = null
    ) {
        if (mFaceHandler == null) {
            listener?.onError(1, "faceHandler is null")
            return
        }
        if (bitmap == null) {
            listener?.onError(10, "bitmap is null")
            return
        }
        mFaceHandler?.let {
            //            val faceResult = it.addFace(bitmap.copy(bitmap.config, bitmap.isMutable))
            val faceResult = it.addFace(bitmap)
            if (faceResult == null) {
                listener?.onError(2, "faceResult is null")
                return
            }
            when (faceResult.result) {
                1 -> listener?.onError(3, "No face information was detected")
                2 -> listener?.onError(4, "Picture quality problem")
                0 -> {
                    val faceToken = String(faceResult.faceToken)
                    val bind = it.bindGroup(FaceConstants.GROUP_NAME, faceResult.faceToken)
                    if (!bind) {
                        delFaceByToken(faceToken)
                        listener?.onError(5, "Binding library failed")
                    } else {
                        if (getFeature) {
                            extractFeature(
//                                bitmap.copy(bitmap.config, bitmap.isMutable),
                                bitmap,
                                "${faceToken}.fd",
                                object : IExtractFeatureListener {
                                    override fun onExtractFeature(
                                        success: Boolean,
                                        msg: String
                                    ) {
                                        if (success) {
                                            listener?.onSuccess(faceToken, msg)
                                        } else {
                                            delFaceByToken(faceToken)
                                            log("========提取特征码失败：$msg")
                                            listener?.onError(8, "Feature code extraction failed")
                                        }
                                    }
                                })
                        } else {
                            log("=================注册人脸成功啦~~~")
                            listener?.onSuccess(faceToken, "")
                        }
                    }
                }
                else -> listener?.onError(5, "unknown error")
            }
        }
    }


    /**
     * 根据特征码注册人脸
     * [file] 特征码文件
     */
    fun registerFaceByFeature(file: File, listener: IRegisterFaceListener? = null) {
        if (mFaceHandler == null) {
            listener?.onError(1, "faceHandler is null")
            return
        }
        if (!file.exists()) {
            listener?.onError(6, "Signature file not found")
            return
        }
        try {
            val data = file.readBytes()
            val faceToken = mFaceHandler!!.insertFeature(data, null)
            val bind =
                mFaceHandler!!.bindGroup(FaceConstants.GROUP_NAME, faceToken.toByteArray())
            if (faceToken.isNullOrEmpty()) {
                listener?.onError(9, "unknown error")
                return
            }
            if (!bind) {
                delFaceByToken(faceToken)
                listener?.onError(5, "Binding library failed")
            } else {
                listener?.onSuccess(faceToken, file.absolutePath)
            }
        } catch (e: Exception) {
            e.printStackTrace()
            listener?.onError(7, e.message ?: "unknown error")
        }
    }


    //提取特征码
    fun extractFeature(
        bitmap: Bitmap,
        fileName: String,
        listener: IExtractFeatureListener? = null
    ) {
        if (mFaceHandler == null) {
            listener?.onExtractFeature(false, "faceHandler is null")
            return
        }
        try {
            val result = mFaceHandler!!.extractFeature(bitmap)
            when (result.result) {
                1 -> listener?.onExtractFeature(false, "No face was detected")
                2 -> listener?.onExtractFeature(
                    false,
                    "The face was detected, but the quality was not judged"
                )
                0 -> {
                    if (result.featureData.size == 0) {
                        listener?.onExtractFeature(false, "No signature")
                        return
                    }
                    val file = File(FaceConstants.FEATURE_PATH + "/" + fileName)
                    if (!file.parentFile.exists()) {
                        file.parentFile.mkdirs()
                    }
                    FacePassTool.saveFile(result.featureData, file.absolutePath)
                    listener?.onExtractFeature(true, file.absolutePath)
                }
            }

        } catch (e: Exception) {
            e.printStackTrace()
            listener?.onExtractFeature(false, e.message ?: "unknown error")
        }

    }


    //提取特征值
    fun extractFeature(bitmap: Bitmap): FacePassExtractFeatureResult? {
        if (mFaceHandler == null) {
            return null
        }
        try {
            val result = mFaceHandler!!.extractFeature(bitmap)
            when (result.result) {
                0 -> {
                    if (result.featureData.size == 0) {
//                        listener?.onExtractFeature(false, "No signature")
                        return null
                    }
                    return result
                }
                else -> return null
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return null
    }


    //1v1比对
    fun singleComparison(source: Bitmap, target: Bitmap): FacePassCompareResult? {
        if (mFaceHandler == null)
            return null
        try {
            return mFaceHandler!!.compare(source, target, false)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return null
    }


    //根据faceToken删除人脸
    fun delFaceByToken(faceToken: String): Boolean {
        if (mFaceHandler == null) {
            return false
        }
        try {
            return mFaceHandler?.deleteFace(faceToken.toByteArray()) ?: false
        } catch (e: Exception) {
            e.printStackTrace()
            return false
        }
    }


    //根据faceToken获取人脸小图
    fun getFaceImage(faceToken: String): Bitmap? {
        if (faceToken == null || faceToken.length < 3) {
            return null
        }
        if (mFaceHandler == null) {
            return null
        }
        try {
            return mFaceHandler!!.getFaceImage(faceToken.toByteArray())
        } catch (e: FacePassException) {
            e.printStackTrace()
            return null
        }
    }


    //新建底库
    fun createGroup(): Boolean {
        if (mFaceHandler == null)
            return false
        try {
            val localGroups = mFaceHandler!!.localGroups
            if (localGroups == null || localGroups.size == 0) {
                return mFaceHandler!!.createLocalGroup(FaceConstants.GROUP_NAME)
            } else {
                val isExites = localGroups.contains(FaceConstants.GROUP_NAME)
                if (!isExites) {
                    log("=========创建底库 by localGroups not exites")
                    return mFaceHandler!!.createLocalGroup(FaceConstants.GROUP_NAME)
                } else {
                    return true
                }
            }
        } catch (e: FacePassException) {
            log("========创建底库失败：${e.message}")
            return false
        }
    }

    //删除底库
    fun delLocalGroup(): Boolean {
        if (mFaceHandler == null)
            return false
        try {
            val localGroups = mFaceHandler!!.localGroups
            if (localGroups == null || localGroups.size == 0) {
                return true
            } else {
                val isExites = localGroups.contains(FaceConstants.GROUP_NAME)
                if (isExites) {
                    return mFaceHandler!!.deleteLocalGroup(FaceConstants.GROUP_NAME)
                } else {
                    return true
                }
            }
        } catch (e: FacePassException) {
            log("========删除底库失败：${e.message}")
            return false
        }
    }

    //清空人脸数据
    fun cleanAllFace(listener: ICleanFaceListener? = null) {
        thread(start = true) {
            val isSuccess = delLocalGroup()
            if (!isSuccess) {
                listener?.onCleanFaceListener(false, "Failed to delete the base database")
                return@thread
            }
            FacePassTool.deleteFile(File(FaceConstants.FACE_PATH))
            val rootFile = File(FaceConstants.FACE_PATH)
            if (!rootFile.exists()) {
                rootFile.mkdirs()
            }
            initSDK()
            val create = checkGroup()
            if (create) {
                listener?.onCleanFaceListener(true, "Empty successfully")
            } else {
                listener?.onCleanFaceListener(false, "Failed to create base database")
            }
        }
    }


    //注册一张默认的人脸，防止底库为空时，活体检测失败
    fun addDefaultFace() {
        executorService.execute {
            if (mFaceHandler == null)
                return@execute
            if (mContext == null)
                return@execute
            if (!checkGroup())
                return@execute
            val faceTokens: Array<ByteArray>? =
                mFaceHandler?.getLocalGroupInfo(FaceConstants.GROUP_NAME)
            if (faceTokens == null || faceTokens.size == 0) {
                val bitmap =
                    BitmapFactory.decodeResource(mContext!!.getResources(), R.mipmap.default_face)
                registerFace(bitmap, object : IRegisterFaceListener {
                    override fun onSuccess(faceToken: String, featurePath: String) {
                        log("添加默认人脸成功")
                    }

                    override fun onError(code: Int, msg: String) {
                        log("添加默认人脸失败: $code / $msg")
                    }
                })
            }
        }
    }

    fun getFaceImagePath(faceToken: String): String? {
        return mFaceHandler?.getFaceImagePath(faceToken.toByteArray())
    }

    fun compareFace(face1: Bitmap?, face2: Bitmap?): FacePassCompareResult? {
        if (face1 == null || face2 == null) {
            return null;
        }
        return mFaceHandler?.compare(face1, face2, false)
    }

    fun destory() {
        if (mInitListener != null) {
            mInitListener = null
        }
        if (mContext != null) {
            mContext = null
        }
    }

    /**
     * 检查是否有底库
     */
    fun checkGroup(): Boolean {
        val localGroups = mFaceHandler!!.localGroups
        if (localGroups == null || localGroups.size == 0) {
            return false
        } else {
            return localGroups.contains(FaceConstants.GROUP_NAME)
        }
    }

    fun log(msg: String) {
        Log.i("FaceManager", msg)
    }
}