package com.ys.temperaturelib.entity;

/***
 * 温度补偿值范围
 */
public class TempCompairEntity {
    public float personTemp;  //人体温度
    public int rectDistance;  //识别距离
    public float rangeStart;  //目标物体温度开始
    public float rangeEnd;  //目标物体温度结束
    public float compensate;  //补偿值

    public TempCompairEntity(float personTemp, int rectDistance, float rangeStart, float rangeEnd, float compensate) {
        this.personTemp = personTemp;
        this.rectDistance = rectDistance;
        this.rangeStart = rangeStart;
        this.rangeEnd = rangeEnd;
        this.compensate = compensate;
    }

    @Override
    public String toString() {
        return "TempCompairEntity{" +
                "personTemp=" + personTemp +
                ", rectDistance=" + rectDistance +
                ", rangeStart=" + rangeStart +
                ", rangeEnd=" + rangeEnd +
                ", compensate=" + compensate +
                '}';
    }
}
