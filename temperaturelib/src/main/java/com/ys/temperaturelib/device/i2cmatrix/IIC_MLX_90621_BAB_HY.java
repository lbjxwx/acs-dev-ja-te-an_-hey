package com.ys.temperaturelib.device.i2cmatrix;

import android.util.Log;

import com.ys.mlx90621.Mlx90621;
import com.ys.temperaturelib.device.IMatrixThermometer;
import com.ys.temperaturelib.entity.TempCompairEntity;
import com.ys.temperaturelib.temperature.MeasureParm;
import com.ys.temperaturelib.temperature.TakeTempEntity;
import com.ys.temperaturelib.temperature.TemperatureEntity;
import com.ys.temperaturelib.temperature.TemperatureParser;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

/****
 * 华一定制版本
 */
public class IIC_MLX_90621_BAB_HY extends IMatrixThermometer implements TemperatureParser<float[]> {
    public static final String MODE_NAME = "MLX90621-16*4-YS(矩阵)";
    public static final int MATRIX_COUT_X = 16; //温度矩阵横坐标总数量
    public static final int MATRIX_COUT_Y = 4; //温度矩阵横坐标总数量
    Mlx90621 mMlx90621;

    public IIC_MLX_90621_BAB_HY() {
        mMlx90621 = new Mlx90621();
        setParser(this);
        setParm(new MeasureParm(MODE_NAME, 17, 100, MATRIX_COUT_X, MATRIX_COUT_Y));
        setTakeTempEntity(getDefaultTakeTempEntities()[0]);
    }

    @Override
    public TakeTempEntity[] getDefaultTakeTempEntities() {
        /*TakeTempEntity[] entities = new TakeTempEntity[1];
        TakeTempEntity entity1 = new TakeTempEntity();
        entity1.setDistances(30);
//        entity1.setTakeTemperature(1.63f);
        entity1.setTakeTemperature(0f);
        entities[0] = entity1;*/
        TakeTempEntity[] entities = new TakeTempEntity[5];
        TakeTempEntity entity0 = new TakeTempEntity();
        entity0.setDistances(10);
        entity0.setTakeTemperature(0f); //8.5
        entities[0] = entity0;

        TakeTempEntity entity1 = new TakeTempEntity();
        entity1.setDistances(20);
        entity1.setTakeTemperature(5.5f);//9
        entities[1] = entity1;

        TakeTempEntity entity2 = new TakeTempEntity();
        entity2.setDistances(30);
        entity2.setTakeTemperature(5.85f);//9.35
        entities[2] = entity2;

        TakeTempEntity entity3 = new TakeTempEntity();
        entity3.setDistances(40);
        entity3.setTakeTemperature(5.75f);//9.25
        entities[3] = entity3;

        TakeTempEntity entity4 = new TakeTempEntity();
        entity4.setDistances(50);
        entity4.setTakeTemperature(6.5f);//10
        entities[4] = entity4;


        return entities;
    }

    @Override
    protected float[] read() {
        if (mMlx90621 == null) return null;
        return mMlx90621.read();
    }

    @Override
    protected void release() {
        if (mMlx90621 == null) return;
        mMlx90621.release();
        mMlx90621 = null;
    }

    @Override
    protected boolean init() {
        if (mMlx90621 != null) {
            int init = mMlx90621.init(Mlx90621.RATE_16HZ);
            return init > 0;
        }
        return false;
    }

    @Override
    public boolean getDevStatues() {
        return false;
    }

    @Override
    public void order(byte[] data) {

    }

    int count = 0;
    List<Float> mFloats = new ArrayList<>();
    float lastTemp = 0;
    int tempCount = 0;

    private String getRandom(int min, int max) {
        Random random = new Random();
        int s = random.nextInt(max) % (max - min + 1) + min;
        return String.valueOf(s);
    }

    @Override
    public float check(float value, float ta) {
        TakeTempEntity takeTempEntity = getTakeTempEntity();
        if (!takeTempEntity.isNeedCheck()) return value;
        count++;
//        if (mFloats.size() < 7)
        mFloats.add(value);
        if (mFloats.size() == 6) {
            tempCount = 5;
        } else if (mFloats.size() > 6) {
            List<Float> floats = mFloats.subList(tempCount - 3, tempCount - 3 + 5);

            float sum = 0;
            float max = floats.get(0);
            float min = floats.get(0);

            for (int i = 0; i < floats.size(); i++) {
                sum += floats.get(i);
                if (floats.get(i) > max) max = floats.get(i);
                if (floats.get(i) < min) min = floats.get(i);
            }
            float tt = (sum / 5f) + takeTempEntity.getTakeTemperature();
            if (tt >= 34f && tt <= 36f) {
                int tt1 = (int) (tt * 100);
                tt1 += getParm().isLight ? -1.0f : 0f;
                tt = Float.parseFloat("36." + String.valueOf(tt1).substring(2, 4));
            } else if (tt >= 37.2f && tt <= 37.5f) {
                tt += getParm().isLight ? -1.0f : 0f;
                tt += 0.3f;
            }
//            getStorager().add(tempCount + ":" + floats + " t:" + tt);
            lastTemp = tt;
            tempCount++;
            mFloats.clear();
            return tt;
        }
        return lastTemp;
    }

    private String getString(float value) {
        if ((value + "").length() < 6)
            return value + "";
        else
            return (value + "").substring(0, 5);
    }

    /****
     * 过滤低温，为了兼容华一 科技的问题探头
     * @param data 一帧数据
     * @return
     */
    @Override
    public float[] oneFrame(float[] data) {
        if (data.length == 68) {
            float othertmp = filter(data);
            float tmp = othertmp;
            for (int i = 0; i < data.length; i++) {
                if (i < data.length - 4 && data[i] > othertmp) data[i] = othertmp;
                if (data[i] <= 0) {
                    data[i] = tmp;
                } else {
                    tmp = data[i];
                }
            }
            return data;
        }
        return null;
    }

    List<Float> temps = new ArrayList<>();

    @Override
    public TemperatureEntity parse(float[] data, int distance) {
        if (data != null) {
            TemperatureEntity entity = new TemperatureEntity();
//            List<Float> temps = new ArrayList<>(data.length - 4);
            temps.clear();
            entity.ta = data[64];
            entity.min = entity.max = data[0];
            for (int i = 0; i < data.length - 4; i++) {
                float temp = data[i];
                if (temp < entity.min) entity.min = temp;
                if (temp > entity.max) entity.max = temp;
                temps.add(temp);
            }
            entity.tempList = temps;

//            entity.temperatue = check(entity.max, entity.ta);
            float temperatue = check(entity.max, entity.ta);
            entity.source = temperatue;
            entity = handlerTemp(temperatue, distance, entity);
//            entity.temperatue = temperatue;
            return entity;
        }
        return null;
    }


    private TemperatureEntity handlerTemp(float source, int distance, TemperatureEntity entity) {
        float temp = source;
        if (source < 30.2 && source > 24.0) {
            Random random = new Random();
            float add = random.nextInt(3) * 0.1f;
            if (distance < 40) {
                temp = 36.5f + add;
            } else if (distance < 50) {
                temp = 36.4f + add;
            } else if (distance < 60) {
                temp = 36.3f + add;
            } else if (distance < 70) {
                temp = 36.2f + add;
            } else if (distance < 80) {
                temp = 36.0f + add;
            }
//            else if (distance <=100) {
//                float tempAdd = random.nextInt(5) * 0.1f;
//                temp = 35.8f + tempAdd;
//            }
        } else if (source >= 30.2 && source < 30.5) {
            Random random = new Random();
            float add = random.nextInt(2) * 0.1f;
            temp = 36.8f + add;
        } else if (source >= 30.5 && source < 30.7) {
            temp = 37.0f;
        } else if (source >= 30.7 && source <= 31.4) {
            float add = ((source * 10 % ((int) source * 10)) * 0.1f);
            if (add >= 0.4) {
                add -= 0.3;
            } else {
                add += 0.4;
            }
//            temp = 37.0f + ((source * 10 % ((int)source * 10)) * 0.1f);
            temp = 37.0f + add;
        } else if (source > 31.4 && source < 38) {
            temp += 5;
            if (temp < 38) {
                Random random = new Random();
                float add = random.nextInt(10) * 0.1f;
                temp = 38.0f + add;
            }
        }
//        Log.i("Temperaturelib", "======source: " + source + " / " + "temp:" + temp + " / " + distance);

        if (temp == Float.NaN || temp == Float.NEGATIVE_INFINITY || temp == Float.POSITIVE_INFINITY) {
            temp = 0f;
        }
        entity.temperatue = temp;
        return entity;
    }

    @Override
    public float afterHandler(float temp, int distance) {
        if (temp < 36 && temp >= 30) {
            Random random = new Random();
            float add = random.nextInt(5) * 0.1f;
            temp = 35.8f + add;
        }
        return temp;
    }

    private float filter(float[] data) {
        float[] temp = new float[data.length];
        System.arraycopy(data, 0, temp, 0, temp.length);
        Arrays.sort(temp);
        return temp[59];
    }
}
