package com.ys.temperaturelib.device.serialport;

import com.ys.temperaturelib.temperature.MeasureParm;
import com.ys.temperaturelib.temperature.TemperatureEntity;
import com.ys.temperaturelib.temperature.TemperatureParser;

// Created by kermitye on 2020/4/17 9:57
public class SP_XRJ_S60 extends ProductImp implements TemperatureParser<byte[]> {
    public static final String DEFAULT_MODE_NAME = "XRJ_S60"; //型号

    static final int MATRIX_COUT_X = 0; //温度矩阵横坐标总数量
    static final int MATRIX_COUT_Y = 0; //温度矩阵纵坐标总数量

    static final byte[] ORDER_DATA_OUTPUT = new byte[]{(byte) 0xAA, (byte) 0xAA, 0x06, 0x22, 0x12, 0x72}; //查询输出数据指令

    public SP_XRJ_S60(String device, int baudrate) {
        super(device, baudrate, new MeasureParm(DEFAULT_MODE_NAME, 0, 10, MATRIX_COUT_X, MATRIX_COUT_Y));
        setTemperatureParser(this);
        setWriteInThread(true);
    }

    @Override
    public byte[] getOrderDataOutputType(boolean isAuto) {
        return ORDER_DATA_OUTPUT;
    }

    @Override
    public byte[] getOrderDataOutputQuery() {
        return ORDER_DATA_OUTPUT;
    }

    @Override
    public boolean isPoint() {
        return false;
    }

    @Override
    public byte[] oneFrame(byte[] data) {
        return data;
    }

    @Override
    public TemperatureEntity parse(byte[] data, int distance) {
        if (data.length > 0) {
            String buffer = "";
            for (int i = 0; i < data.length; i++) {
                String hex = Integer.toHexString(data[i] & 0xFF);
                if (hex.length() == 1) {
                    hex = '0' + hex;
                }
                buffer += hex.toUpperCase();
            }

            TemperatureEntity entity = new TemperatureEntity();
            if (buffer != null && buffer.startsWith("A5A50A2212") && data.length == 10) {
                String temp1 = buffer.substring(14, 16);
                String temp2 = buffer.substring(12, 14);//A5A50A2212AB B703 0F04
                float temp12 = (float) Integer.parseInt(temp1 + temp2, 16) / 20 - 24;
//                if ((temp12 + "").length() > 4) {
//                    tvEnvironment.setText("环境温度：" + (temp12 + "").substring(0, 5) + "℃");
//
//                } else {
//                    tvEnvironment.setText("环境温度：" + temp12 + "℃");
//                }
                entity.ta = temp12;

                String temp3 = buffer.substring(18, 20);
                String temp4 = buffer.substring(16, 18);
                float tempBody = (float) Integer.parseInt(temp3 + temp4, 16) / 20 - 24;
//                if ((tempBody + "").length() > 4)
//                    tvBody.setText("人体温度：" + (tempBody + "").substring(0, 5) + "℃");
//                else
//                    tvBody.setText("人体温度：" + tempBody + "℃");
                entity.temperatue = tempBody;

                return entity;
            }
//            onDataReceiveListener.onDataReceive(r,size);
        }

        return null;
    }
}
