package com.ys.temperaturelib.device.serialport;

import android.util.Log;

import com.ys.temperaturelib.temperature.MeasureParm;
import com.ys.temperaturelib.temperature.TemperatureEntity;
import com.ys.temperaturelib.temperature.TemperatureParser;

import java.util.ArrayList;
import java.util.List;

public class SP_MLX_90621_BAA extends ProductImp implements TemperatureParser<byte[]> {
    public static final String DEFAULT_MODE_NAME = "MLX90621-RR(矩阵)"; //型号
    static final int DEFAULT_DEVICE = 4; //设备号 "/dev/ttyS3"
    static final int DEFAULT_RATE = 4; //波特率 115200

    static final int MATRIX_COUT_X = 16; //温度矩阵横坐标总数量
    static final int MATRIX_COUT_Y = 4; //温度矩阵纵坐标总数量

//    static final byte[] ORDER_DATA_OUTPUT = new byte[]{(byte) 0xA5, 0x55, 0x01, (byte) 0xFB}; //查询输出数据指令
    static final byte[] ORDER_DATA_OUTPUT = new byte[]{(byte) 0xA5, 0x35, (byte) 0xF1, (byte) 0xCB}; //查询输出数据指令

    int mRectDistance; //距离

    public SP_MLX_90621_BAA(String device, int rare) {
        super(device, rare,
                new MeasureParm(DEFAULT_MODE_NAME, 40, 300, MATRIX_COUT_X, MATRIX_COUT_Y));
        setTemperatureParser(this);
        setWriteInThread(true);
    }


    @Override
    public byte[] getOrderDataOutputType(boolean isAuto) {
        return ORDER_DATA_OUTPUT;
    }

    @Override
    public byte[] getOrderDataOutputQuery() {
        return ORDER_DATA_OUTPUT;
    }

    @Override
    public boolean isPoint() {
        return false;
    }

    @Override
    public byte[] oneFrame(byte[] data) {
        return data;
    }

    @Override
    public TemperatureEntity parse(byte[] data, int distance) {
        if (data == null || data.length < 7) return null;
        if ((data[0] & 0xFF) == 0xa5 && (data[1] & 0xFF) == 0xa5) {
            int length = (data[2] & 0xFF) + 256 * (data[3] & 0xFF);
            log("data length:" + length + " / " + data.length);

            TemperatureEntity entity = new TemperatureEntity();
            List<Float> temps = new ArrayList<>();
            entity.min = entity.max = ((data[4] & 0xFF) + 256 * (data[5] & 0xFF)) / 100f;
            if (entity.min > 120 || entity.max > 120) {
                entity.min = entity.max = 36.0f;
            }
            for (int i = 4; i < data.length - 2; i = i + 2) {
//                int sum = (data[i] & 0xFF) << 8 | (data[i + 1] & 0xFF);
                float temp = ((data[i] & 0xFF) + 256 * (data[i + 1] & 0xFF)) / 100f;
                if (temp > 120)
                    continue;
                if (temp < entity.min) entity.min = temp;
                if (temp > entity.max) entity.max = temp;
                temps.add(temp);
            }
            log("temperatue:" + entity.max);
            entity.temperatue = entity.max;
            return entity;

//            TemperatureEntity entity = new TemperatureEntity();
//            entity.temperatue = ((data[2] & 0xFF) + 256 * (data[3] & 0xFF)) / 100f;
//            log("======temperatue:" + entity.temperatue);
//            return entity;
        }
        return null;
    }

    private void log(String msg) {
        Log.e("SMLX90621_BAA", msg);
    }
}
