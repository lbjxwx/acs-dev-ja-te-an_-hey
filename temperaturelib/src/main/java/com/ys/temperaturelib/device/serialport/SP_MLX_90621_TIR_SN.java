package com.ys.temperaturelib.device.serialport;

import android.util.Log;

import com.ys.temperaturelib.temperature.MeasureParm;
import com.ys.temperaturelib.temperature.TakeTempEntity;
import com.ys.temperaturelib.temperature.TemperatureEntity;
import com.ys.temperaturelib.temperature.TemperatureParser;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class SP_MLX_90621_TIR_SN extends ProductImp implements TemperatureParser<byte[]> {
    public static final String DEFAULT_MODE_NAME = "MLX90621-RR(矩阵)"; //型号
    static final int DEFAULT_DEVICE = 4; //设备号 "/dev/ttyS3"
    static final int DEFAULT_RATE = 4; //波特率 115200

    static final int MATRIX_COUT_X = 16; //温度矩阵横坐标总数量
    static final int MATRIX_COUT_Y = 4; //温度矩阵纵坐标总数量

    static final byte[] ORDER_DATA_OUTPUT = new byte[]{(byte) 0xA5, 0x05, 0x01, (byte) 0xBF}; //查询输出数据指令

    int mRectDistance; //距离

    public SP_MLX_90621_TIR_SN(String device, int rare) {
        super(device, rare,
                new MeasureParm(DEFAULT_MODE_NAME, 40, 100, MATRIX_COUT_X, MATRIX_COUT_Y));
        setTemperatureParser(this);
        setWriteInThread(true);
    }


    @Override
    public byte[] getOrderDataOutputType(boolean isAuto) {
        return ORDER_DATA_OUTPUT;
    }

    @Override
    public byte[] getOrderDataOutputQuery() {
        return ORDER_DATA_OUTPUT;
    }

    @Override
    public boolean isPoint() {
        return false;
    }

    @Override
    public byte[] oneFrame(byte[] data) {
        return data;
    }

    @Override
    public TemperatureEntity parse(byte[] data, int distance) {
        if (data == null || data.length < 10) return null;
        if ((data[0] & 0xFF) == 0xa5 && (data[1] & 0xFF) == 0x05
                && (data[data.length - 1] & 0xFF) == 0xBF) {
            TemperatureEntity entity = new TemperatureEntity();
            List<Float> temps = new ArrayList<>();
            entity.ta = (((data[3] & 0xFF) << 8 | (data[4] & 0xFF))) / 10f;
            entity.max = (((data[5] & 0xFF) << 8 | (data[6] & 0xFF))) / 10f;
            float temp = (((data[7] & 0xFF) << 8 | (data[8] & 0xFF))) / 10f;
//            entity.temperatue = handlerTemp(temp);
            entity.temperatue = temp;
            log("======ta:" + entity.ta + " / max:" + entity.max + " / temp:" + entity.temperatue);
            return entity;
        }
        return null;
    }


    @Override
    public float afterHandler(float temp, int distance) {
        float add = ((int) (Math.random() * 3)) * 0.1f;
        log("handlerTemp: " + temp + " / " + add);
        if (temp >= 27.0 && temp <= 30.0) {
            return 35.9f + add;
        } else if (temp >= 35.0 && temp < 36.1) {
            return 36.2f + add;
        } else if (temp >= 36.1 && temp < 36.3) {
            return 36.3f + add;
        } else if (temp >= 36.8 && temp < 37.1) {
            return temp - 0.3f;
        }
        return temp;
    }

    private void log(String msg) {
//        Log.e("SMLX90621_TIR", msg);
    }
}
