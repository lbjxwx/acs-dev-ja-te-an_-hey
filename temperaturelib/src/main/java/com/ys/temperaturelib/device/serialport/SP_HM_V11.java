package com.ys.temperaturelib.device.serialport;

import android.util.Log;

import com.ys.temperaturelib.device.MeasureResult;
import com.ys.temperaturelib.temperature.MeasureParm;
import com.ys.temperaturelib.temperature.TakeTempEntity;
import com.ys.temperaturelib.temperature.TemperatureEntity;
import com.ys.temperaturelib.temperature.TemperatureParser;
import com.ys.temperaturelib.utils.DataFormatUtil;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

/***
 * 众智模块
 */
public class SP_HM_V11 extends ProductImp implements TemperatureParser<byte[]> {
    public static final String DEFAULT_MODE_NAME = "SMLX-HM-V11(矩阵)"; //型号
    static final String DEFAULT_DEVICE = "/dev/ttyS3"; //设备号
    static final int DEFAULT_RATE = 115200; //波特率

    static final int MATRIX_COUT_X = 16; //温度矩阵横坐标总数量
    static final int MATRIX_COUT_Y = 4; //温度矩阵纵坐标总数量

    //    static final byte[] ORDER_DATA_OUTPUT = "VCMD=TMP".getBytes(); //查询输出数据指令
    static final byte[] ORDER_DATA_OUTPUT = "VCMD=FHT".getBytes(); //查询输出数据指令
    static final byte[] ORDER_DATA_GRIVITY = "VCMD=IRL".getBytes(); //查询输出数据指令

    public SP_HM_V11(String device, int rare) {
        super(device, rare,
                new MeasureParm(DEFAULT_MODE_NAME, 50, 150, MATRIX_COUT_X, MATRIX_COUT_Y));
        log("========SMLX_HM_V11 初始化");
        setTemperatureParser(this);
        setTakeTempEntity(getDefaultTakeTempEntities()[0]);
        setWriteInThread(true);
//        setReadEnalbe(false);
    }

    @Override
    public void startUp(MeasureResult result, long period) {
        super.startUp(result, period);
        order(ORDER_DATA_GRIVITY);
    }

    @Override
    public TakeTempEntity[] getDefaultTakeTempEntities() {
        TakeTempEntity[] entities = new TakeTempEntity[1];
        TakeTempEntity entity3 = new TakeTempEntity();
        entity3.setDistances(30);
        entity3.setTakeTemperature(0f);
        entities[0] = entity3;
        return entities;
    }

    @Override
    public byte[] getOrderDataOutputType(boolean isAuto) {
        return ORDER_DATA_OUTPUT;
    }

    @Override
    public byte[] getOrderDataOutputQuery() {
        return ORDER_DATA_OUTPUT;
    }

    @Override
    public boolean isPoint() {
        return false;
    }

    @Override
    public byte[] oneFrame(byte[] data) {
        return data;
    }


    int count = 0;
    List<Float> mFloats = new ArrayList<>();
    float lastTemp = 0;

    @Override
    public float check(float value, float ta) {
        mFloats.add(value);
        if (mFloats.size() < 7) {
            return lastTemp;
        }
        float max = Collections.max(mFloats);
        mFloats.remove(max);
        if (mFloats.size() < 1)
            return max;
        float min = Collections.min(mFloats);
        mFloats.remove(min);
        if (mFloats.size() < 1)
            return min;
        float sum = 0;
        for (float data : mFloats) {
            sum += data;
        }
        float result = sum / mFloats.size();
        lastTemp = result;
        mFloats.clear();
        return result;
    }

    @Override
    public TemperatureEntity parse(byte[] data, int distance) {
        if (data == null || data.length < 5)
            return null;
//        log("======未转换数据:" + DataFormatUtil.bytesToHex(data));
//        String tempData = new String(data);
//        log("=======得到转换后的数据：" + tempData);
        if ((data[0] & 0xFF) != 0x46 || (data[1] & 0xFF) != 0x6F || (data[2] & 0xFF) != 0x72)
            return null;
        try {
            String result = new String(data);
            log("--" + result);
            String[] split = result.split(":");
            float temp = 0;
            if (split.length > 1) {
                TemperatureEntity entity = new TemperatureEntity();
                if (split[1].contains("Forehead")) {
                    entity.temperatue = 36.5f;
                    return entity;
                }
                int maxInt = Integer.parseInt(split[1]);
                temp = maxInt / 10.0f;
                entity.temperatue = temp;
//                entity.tempList = new ArrayList<>();
                log("=====得到温度:" + entity.temperatue);
                return entity;
            }
        } catch (Exception e) {
            log("=====解析数据异常");
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public float afterHandler(float temp, int distance) {
        log("=====afterHandler start: " + temp + " / " + distance);
        if (temp >= 20 && temp < 30) {
            return 33.0f + (((int) (Math.random() * 2)) * 0.1f);
        } else if (temp >= 30 && temp < 33) {
            return 35.0f + (((int) (Math.random() * 8)) * 0.1f);
        } else if (temp >= 33 && temp < 36) {
            //线性处理
//            float flag = (float) ((demValue - 34) / 2f * 0.6);
            float flag = (float) ((temp - 33) / 3f * 0.6);
            temp = 36 + flag;
        } else if (temp >= 36.0 && temp < 36.3) {
            return 36.3f + (((int) (Math.random() * 3)) * 0.1f);
        }
        return temp;
    }

    private void log(String msg) {
//        Log.e("SMLX_HM_V11", msg);
    }

}