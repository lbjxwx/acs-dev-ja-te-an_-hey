package com.ys.temperaturelib.device.serialport;

import android.util.Log;

import com.ys.temperaturelib.temperature.MeasureParm;
import com.ys.temperaturelib.temperature.TakeTempEntity;
import com.ys.temperaturelib.temperature.TemperatureEntity;
import com.ys.temperaturelib.temperature.TemperatureParser;
import com.ys.temperaturelib.utils.DataFormatUtil;

import java.util.ArrayList;
import java.util.List;

// Created by kermitye on 2020/6/5 8:45
public class SP_HM_MT031 extends ProductImp implements TemperatureParser<byte[]> {
    public static final String DEFAULT_MODE_NAME = "HM_RB32X3290_A(矩阵)"; //型号
    static final String DEFAULT_DEVICE = "/dev/ttyS3"; //设备号
    static final int DEFAULT_RATE = 115200; //波特率

    static final int MATRIX_COUT_X = 16; //温度矩阵横坐标总数量
    static final int MATRIX_COUT_Y = 4; //温度矩阵纵坐标总数量

    static final byte[] ORDER_DATA_OUTPUT_AUTO = new byte[]{(byte) 0x43, 0x61, 0x6C, 0x63, 0x54, (byte) 0x0D, (byte) 0x0A}; //自动测量
    //53 65 74 41 3d 31(体温枪1) 2C(分隔符",") 33 30 39 36(36.5度) 0d 0a
//    static final byte[] ORDER_DATA_CALIBRATION = new byte[]{(byte) 0x53, 0x65, 0x74, 0x41, 0x3D, 0x31, 0x2C, 0x33, 0x30, 0x39, 0x36, 0x0D, 0x0A}; //自动测量
    static final byte[] ORDER_DATA_CALIBRATION = new byte[]{(byte) 0x53, 0x65, 0x74, 0x41, 0x3D, 0x31, 0x2C, 0x33, 0x30, 0x39, 0x36, 0x0D, 0x0A}; //自动测量

    //正在校准
    boolean isCalibration = false;


    public SP_HM_MT031(String device, int rare) {
        super(device, rare,
                new MeasureParm(DEFAULT_MODE_NAME, 40, 100, MATRIX_COUT_X, MATRIX_COUT_Y));
        setTemperatureParser(this);
        setTakeTempEntity(getDefaultTakeTempEntities()[0]);
        setWriteInThread(true);
    }

    @Override
    public TakeTempEntity[] getDefaultTakeTempEntities() {
        TakeTempEntity[] entities = new TakeTempEntity[5];
        TakeTempEntity entity0 = new TakeTempEntity();
        entity0.setDistances(10);
        entity0.setTakeTemperature(0);//2.3  0  2
        entities[0] = entity0;

        return entities;
    }

    @Override
    public byte[] getOrderDataOutputType(boolean isAuto) {
        return ORDER_DATA_OUTPUT_AUTO;
    }

    @Override
    public byte[] getOrderDataOutputQuery() {
        return ORDER_DATA_OUTPUT_AUTO;
    }

    @Override
    public boolean isPoint() {
        return false;
    }

    @Override
    public byte[] oneFrame(byte[] data) {
        return data;
    }

    int count = 0;
    List<Float> mFloats = new ArrayList<>();
    float lastTemp = 0;
    int tempCount = 0;

    @Override
    public float check(float value, float ta) {
        return value;
    }


    //校准
    public void calibration(float targetTemp) {
        isCalibration = true;
        Log.d("HM_MT031", "calibration: targetTemp=" + targetTemp);
        String temp = ((targetTemp * 10) + 2731) + "";
        byte[] bytes = temp.getBytes();
        if (bytes.length == 4) {
            ORDER_DATA_CALIBRATION[7] = bytes[0];
            ORDER_DATA_CALIBRATION[8] = bytes[1];
            ORDER_DATA_CALIBRATION[9] = bytes[2];
            ORDER_DATA_CALIBRATION[10] = bytes[3];
        }
        String data = DataFormatUtil.bytesToHex(ORDER_DATA_CALIBRATION);
        Log.d("HM_MT031", "calibration: " + data);
        setWriteInThread(false);
        order(ORDER_DATA_CALIBRATION);
    }


    @Override
    public TemperatureEntity parse(byte[] data, int distance) {
        if (data == null) return null;
        Log.d("HM_MT031", "parse: " + DataFormatUtil.bytesToHex(data));
        if (data.length >= 10 && (data[0] & 0xFF) == 0x43 && (data[1] & 0xFF) == 0x54) {
            TemperatureEntity entity = new TemperatureEntity();
//            float temperatue = (data[2] + data[3]) / 100f;
            float temperatue = (float) (data[2] + (data[3] / 100.0));
            entity.temperatue = temperatue;
            return entity;
        } else if (data.length == 8 && (data[0] & 0xFF) == 0x49 && (data[1] & 0xFF) == 0x44 && (data[2] & 0xFF) == 0x3d) {
            isCalibration = false;
            setWriteInThread(true);
            //校准完成
            TemperatureEntity entity = new TemperatureEntity();
            entity.temperatue = -101;
            return entity;
        }
        return null;
    }

    @Override
    public void destroy() {
        super.destroy();
        isCalibration = false;
    }
}
