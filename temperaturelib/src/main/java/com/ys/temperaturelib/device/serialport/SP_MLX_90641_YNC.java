package com.ys.temperaturelib.device.serialport;

import android.content.Context;
import android.util.Log;

import com.yannuo.tempserialportsdk.SerialHelper;
import com.yannuo.tempserialportsdk.TempListener;
import com.ys.temperaturelib.device.MeasureDevice;
import com.ys.temperaturelib.device.MeasureResult;
import com.ys.temperaturelib.temperature.MeasureParm;
import com.ys.temperaturelib.temperature.TemperatureEntity;

// Created by kermitye on 2020/8/28 14:16
public class SP_MLX_90641_YNC extends MeasureDevice implements TempListener {

    private SerialHelper serialHelper;
    private Context mContext;
    String mDevice;
    MeasureResult result;

    public SP_MLX_90641_YNC(String device, Context context) {
        this.mContext = context;
        mDevice = device;
        init();
    }

    @Override
    protected boolean init() {
        if (serialHelper != null) {
            serialHelper.closeSerial();
            serialHelper = null;
        }
        serialHelper = new SerialHelper(mContext);
        serialHelper.setOnTempListener(this);
        serialHelper.openSerial(mDevice);
        return true;
    }



    @Override
    public void startUp(MeasureResult result, long period) {
        this.result = result;
    }

    @Override
    public boolean getDevStatues() {
        return false;
    }

    @Override
    public void order(byte[] data) {

    }

    @Override
    public void destroy() {
        mContext = null;
        if (serialHelper != null) {
            serialHelper.closeSerial();
            serialHelper = null;
        }
    }

    @Override
    public float check(float value, float ta) {
        return 0;
    }

    @Override
    public boolean isPoint() {
        return false;
    }

    //温度数据回调
    @Override
    public void onAdjust(double sensorTemp, int distance, double maxTemp, double enTemp, double tempDouble) {
        log("======onAdjust:" + tempDouble);
        if (result != null) {
            TemperatureEntity entity = new TemperatureEntity();
            entity.temperatue = (float) tempDouble;
            result.onResult(entity, null);
        }
    }


    private void log(String msg) {
        Log.e("SMLX90641_YNC", msg);
    }
}
