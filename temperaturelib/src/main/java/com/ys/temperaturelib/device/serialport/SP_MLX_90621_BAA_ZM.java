package com.ys.temperaturelib.device.serialport;

import android.util.Log;

import com.ys.temperaturelib.temperature.MeasureParm;
import com.ys.temperaturelib.temperature.TemperatureEntity;
import com.ys.temperaturelib.temperature.TemperatureParser;

import java.util.ArrayList;
import java.util.List;


public class SP_MLX_90621_BAA_ZM extends ProductImp implements TemperatureParser<byte[]> {
    public static final String DEFAULT_MODE_NAME = "MLX90621-RR(矩阵)"; //型号

    //直接出来温度，大括号包裹
    public static final int PARSE_TYPE_STR = 0;
    public static final int PARSE_TYPE_HEX = 1;

    public int parseType = PARSE_TYPE_STR;

    static final int DEFAULT_DEVICE = 4; //设备号 "/dev/ttyS3"
    static final int DEFAULT_RATE = 4; //波特率 115200

    static final int MATRIX_COUT_X = 16; //温度矩阵横坐标总数量
    static final int MATRIX_COUT_Y = 4; //温度矩阵纵坐标总数量

    static final byte[] ORDER_DATA_OUTPUT = new byte[]{(byte) 0xA5, 0x55, 0x01, (byte) 0xFB}; //查询输出数据指令
//    static final byte[] ORDER_DATA_OUTPUT = new byte[]{(byte) 0xA5, 0x35, (byte) 0xF1, (byte) 0xCB}; //查询输出数据指令 64点矩阵

    int mRectDistance; //距离

    public SP_MLX_90621_BAA_ZM(String device, int rare, int parseType) {
        super(device, rare,
                new MeasureParm(DEFAULT_MODE_NAME, 40, 100, MATRIX_COUT_X, MATRIX_COUT_Y));
        this.parseType = parseType;
        setTemperatureParser(this);
        setWriteInThread(true);
    }


    @Override
    public byte[] getOrderDataOutputType(boolean isAuto) {
        return ORDER_DATA_OUTPUT;
    }

    @Override
    public byte[] getOrderDataOutputQuery() {
        return ORDER_DATA_OUTPUT;
    }

    @Override
    public boolean isPoint() {
        return false;
    }

    @Override
    public byte[] oneFrame(byte[] data) {
        return data;
    }

    @Override
    public TemperatureEntity parse(byte[] data, int distance) {
        if (data == null) {
            log("======temperatue: data==null");
            return null;
        }
        log("======temperatue: data==" + new String(data) + " / " + parseType);
        if (parseType == PARSE_TYPE_HEX) {
            if ((data[0] & 0xFF) == 0xa5 && (data[1] & 0xFF) == 0x55) {
                TemperatureEntity entity = new TemperatureEntity();
                entity.temperatue = ((data[2] & 0xFF) + 256 * (data[3] & 0xFF)) / 100f;
                log("======temperatue:" + entity.temperatue);
                return entity;
            }
            log("======temperatue: return null");
            return null;
        }

        String tempStr = new String(data);
        log("=====tempStr:" + tempStr);
        if (!tempStr.contains("{") || !tempStr.contains("}"))
            return null;
        try {
            TemperatureEntity entity = new TemperatureEntity();
            String temp = tempStr.replaceAll("\\{|\\}", "");
            log("======replace: " + temp);
            float temperatue = Float.parseFloat(temp);
            log("=====final value:" + temperatue);
            entity.temperatue = temperatue;
            return entity;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private void log(String msg) {
        Log.e("SMLX90621_BAA", msg);
    }
}
