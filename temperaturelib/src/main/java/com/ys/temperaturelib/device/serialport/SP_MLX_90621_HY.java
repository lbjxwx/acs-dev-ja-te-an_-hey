package com.ys.temperaturelib.device.serialport;

import android.util.Log;

import com.ys.temperaturelib.temperature.MeasureParm;
import com.ys.temperaturelib.temperature.TemperatureEntity;
import com.ys.temperaturelib.temperature.TemperatureParser;

import java.util.ArrayList;
import java.util.List;

public class SP_MLX_90621_HY extends ProductImp implements TemperatureParser<byte[]> {
    public static final String DEFAULT_MODE_NAME = "MLX90621-YS(矩阵)"; //型号
    static final String DEFAULT_DEVICE = "/dev/ttyS3"; //设备号
    static final int DEFAULT_RATE = 115200; //波特率

    static final int MATRIX_COUT_X = 16; //温度矩阵横坐标总数量
    static final int MATRIX_COUT_Y = 4; //温度矩阵纵坐标总数量


    public SP_MLX_90621_HY(String device, int rare) {
        super(device, rare,
                new MeasureParm(DEFAULT_MODE_NAME, 40, 50, MATRIX_COUT_X, MATRIX_COUT_Y));
        setTemperatureParser(this);
//        setWriteInThread(true);
//        setTakeTempEntity(getDefaultTakeTempEntities()[0]);
    }


    @Override
    public byte[] getOrderDataOutputType(boolean isAuto) {
        return null;
    }

    @Override
    public byte[] getOrderDataOutputQuery() {
        return null;
    }

    @Override
    public boolean isPoint() {
        return false;
    }

    @Override
    public byte[] oneFrame(byte[] data) {
        return data;
    }


    int count = 0;
    List<Float> mFloats = new ArrayList<>();
    float lastTemp = 0;
    int tempCount = 0;

    @Override
    public float check(float value, float ta) {
        return value;
    }

    @Override
    public TemperatureEntity parse(byte[] data, int distance) {
        if (data == null) return null;
        String tempStr = new String(data);
        log("=====tempStr:" + tempStr);
        if (!tempStr.contains("{") || !tempStr.contains("}"))
            return null;
        try {
            TemperatureEntity entity = new TemperatureEntity();
            String temp = tempStr.replaceAll("\\{|\\}", "");
            log("======replace: " + temp);
            float temperatue = Float.parseFloat(temp);
            log("=====final value:" + temperatue);
            entity.temperatue = temperatue;
            return entity;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    private void log(String msg) {
        Log.e("SMLX90621_HY", msg);
    }

}
