package com.ys.temperaturelib.device.serialport;

import com.ys.temperaturelib.temperature.MeasureParm;
import com.ys.temperaturelib.temperature.TakeTempEntity;
import com.ys.temperaturelib.temperature.TemperatureEntity;
import com.ys.temperaturelib.temperature.TemperatureParser;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SP_OTPA_16 extends ProductImp implements TemperatureParser<byte[]> {
    public static final String DEFAULT_MODE_NAME = "SMLX_MUC(矩阵)"; //型号
    static final int DEFAULT_DEVICE = 4; //设备号 "/dev/ttyS3"
    // 57600
    static final int DEFAULT_RATE = 4; //波特率 115200

    static final int MATRIX_COUT_X = 16; //温度矩阵横坐标总数量
    static final int MATRIX_COUT_Y = 4; //温度矩阵纵坐标总数量

    static final byte[] ORDER_DATA_OUTPUT = new byte[]{(byte) 0xEE, (byte) 0xE1, 0x01, 0X55, (byte)0xFF, (byte) 0xFC}; //查询输出数据指令

    int mRectDistance; //距离

    public SP_OTPA_16(String device, int rare) {
        super(device, rare,
                new MeasureParm(DEFAULT_MODE_NAME, 40, 100, MATRIX_COUT_X, MATRIX_COUT_Y));
        setTemperatureParser(this);
        setWriteInThread(true);
    }


    @Override
    public byte[] getOrderDataOutputType(boolean isAuto) {
        return ORDER_DATA_OUTPUT;
    }

    @Override
    public byte[] getOrderDataOutputQuery() {
        return ORDER_DATA_OUTPUT;
    }

    @Override
    public boolean isPoint() {
        return false;
    }

    @Override
    public byte[] oneFrame(byte[] data) {
        return data;
    }


    int count = 0;
    List<Float> mFloats = new ArrayList<>();
    float lastTemp = 0;

    @Override
    public float check(float value, float ta) {
        mFloats.add(value);
        if (mFloats.size() < 5) {
            return lastTemp;
        }
//        float max = Collections.max(mFloats);
//        mFloats.remove(max);
//        if (mFloats.size() < 1)
//            return max;
//        float min = Collections.min(mFloats);
//        mFloats.remove(min);
//        if (mFloats.size() < 1)
//            return min;
        float sum = 0;
        for (float data: mFloats) {
            sum += data;
        }
        float result = sum / mFloats.size();
        lastTemp = result;
        mFloats.clear();
        return result;
    }

    @Override
    public TemperatureEntity parse(byte[] data, int distance) {
        if (data == null || data.length < 8) return null;
        if ((data[0] & 0xFF) == 0x58 && (data[1] & 0xFF) == 0xa5) {
            TemperatureEntity entity = new TemperatureEntity();
            float to = (((data[3] & 0xFF) << 8 | (data[4] & 0xFF))) / 100f;
            float ta = ((data[5] & 0xFF) * 256 + (data[6] & 0xFF) - 27315) / 100f;
            entity.temperatue = check(to, ta);
            entity.ta = ta;
            return entity;
        }
        return null;
    }

    private void log(String msg) {
//        Log.e("SMLX_MUC", msg);
    }
}
