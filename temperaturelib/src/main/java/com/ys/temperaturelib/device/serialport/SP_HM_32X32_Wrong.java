package com.ys.temperaturelib.device.serialport;

import android.util.Log;

import com.ys.temperaturelib.temperature.MeasureParm;
import com.ys.temperaturelib.temperature.TemperatureEntity;
import com.ys.temperaturelib.temperature.TemperatureParser;
import com.ys.temperaturelib.utils.DataFormatUtil;
import com.ys.temperaturelib.utils.TempWrongUtil;

/***
 * 玉涂需要全部假数据版本，
 * 低于36温度，全部假数据
 */
public class SP_HM_32X32_Wrong extends ProductImp implements TemperatureParser<byte[]> {
    public static final String DEFAULT_MODE_NAME = "HM_RB32X3290_A(矩阵)"; //型号
    static final String DEFAULT_DEVICE = "/dev/ttyS3"; //设备号
    static final int DEFAULT_RATE = 115200; //波特率

    static final int MATRIX_COUT_X = 0; //温度矩阵横坐标总数量
    static final int MATRIX_COUT_Y = 0; //温度矩阵纵坐标总数量

    static final byte[] ORDER_DATA_OUTPUT_AUTO = new byte[]{(byte) 0xA5, 0x55, 0x01, (byte) 0xFB}; //自动测量  A50601BF

    static final byte[] ORDER_DATA_OUTPUT_RECT = new byte[]{(byte) 0xA5, 0x35, (byte) 0xF1, (byte) 0xCB}; //查询阵列数据

    public SP_HM_32X32_Wrong(String device, int rare) {
        super(device, rare,
                new MeasureParm(DEFAULT_MODE_NAME, 40, 150, MATRIX_COUT_X, MATRIX_COUT_Y));
        setTemperatureParser(this);
//        setTakeTempEntity(getDefaultTakeTempEntities()[0]);
        setWriteInThread(true);
    }

    @Override
    public byte[] getOrderDataOutputType(boolean isAuto) {
        return ORDER_DATA_OUTPUT_AUTO;
    }

    @Override
    public byte[] getOrderDataOutputQuery() {
        return ORDER_DATA_OUTPUT_AUTO;
    }

    @Override
    public boolean isPoint() {
        return true;
    }

    @Override
    public byte[] oneFrame(byte[] data) {
        return data;
    }

    @Override
    public float check(float value, float ta) {
        return value;
    }

    TemperatureEntity entity = new TemperatureEntity();

    @Override
    public TemperatureEntity parse(byte[] data, int distance) {
        if (data == null) {
            Log.d("SP_HM_32X32", "接收到的数据: =NULL");
            return null;
        }
        String s = DataFormatUtil.bytesToHex(data);
        Log.d("SP_HM_32X32", "接收到的数据: " + s);
        if (data.length == 7) {
            entity.temperatue = ((data[2] & 0xFF) + 256 * (data[3] & 0xFF)) / 100f;
            entity.ta = 0;
            return entity;
        }
        return null;
    }

    @Override
    public float afterHandler(float temp, int distance) {
        if (temp < 36) {
            return TempWrongUtil.getTempWrongInfo(temp, distance);
        }
        return temp;
    }

}
