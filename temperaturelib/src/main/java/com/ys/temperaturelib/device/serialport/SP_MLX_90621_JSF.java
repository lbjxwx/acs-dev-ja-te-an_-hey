package com.ys.temperaturelib.device.serialport;

import com.ys.temperaturelib.temperature.MeasureParm;
import com.ys.temperaturelib.temperature.TakeTempEntity;
import com.ys.temperaturelib.temperature.TemperatureEntity;
import com.ys.temperaturelib.temperature.TemperatureParser;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class SP_MLX_90621_JSF extends ProductImp implements TemperatureParser<byte[]> {
    public static final String DEFAULT_MODE_NAME = "MLX90621-YS(矩阵)"; //型号
    static final String DEFAULT_DEVICE = "/dev/ttyS3"; //设备号
    static final int DEFAULT_RATE = 115200; //波特率

    static final int MATRIX_COUT_X = 16; //温度矩阵横坐标总数量
    static final int MATRIX_COUT_Y = 4; //温度矩阵纵坐标总数量

    static final byte[] ORDER_DATA_OUTPUT_AUTO = new byte[]{(byte) 0xA5, 0x06, 0x00, 0x00, 0x00, 0x42,(byte) 0xBF}; //自动测量  0xA5,0x06,0x00,0x00,0x00,0x42,0xBF

    public SP_MLX_90621_JSF(String device, int rare) {
        super(device, rare,
                new MeasureParm(DEFAULT_MODE_NAME, 40, 50, MATRIX_COUT_X, MATRIX_COUT_Y));
        setTemperatureParser(this);
        setWriteInThread(true);
    }


    @Override
    public byte[] getOrderDataOutputType(boolean isAuto) {
        return ORDER_DATA_OUTPUT_AUTO;
    }

    @Override
    public byte[] getOrderDataOutputQuery() {
        return ORDER_DATA_OUTPUT_AUTO;
    }

    @Override
    public boolean isPoint() {
        return false;
    }

    @Override
    public byte[] oneFrame(byte[] data) {
        return data;
    }


    int count = 0;
    List<Float> mFloats = new ArrayList<>();
    float lastTemp = 0;
    int tempCount = 0;

    @Override
    public float check(float value, float ta) {
//        TakeTempEntity takeTempEntity = getTakeTempEntity();
//        if (!takeTempEntity.isNeedCheck()) return value;
        count++;
        mFloats.add(value);
        if (mFloats.size() == 4) {
            tempCount = 3;
        } else if (mFloats.size() > 4) {
            List<Float> floats = mFloats.subList(tempCount - 1, tempCount - 1 + 3);
            float sum = 0;
            float max = floats.get(0);
            float min = floats.get(0);

            for (int i = 0; i < floats.size(); i++) {
                sum += floats.get(i);
                if (floats.get(i) > max) max = floats.get(i);
                if (floats.get(i) < min) min = floats.get(i);
            }

            float tt = sum / 3f;
//            if (tt >= 34f && tt < 36f) {
//                int tt1 = (int) (tt * 100);
//                tt = Float.parseFloat("36." + String.valueOf(tt1).substring(2, 4));
//            } else if (tt >= 37.2f && tt <= 37.5f) {
//                tt += 0.3f;
//            }
//            getStorager().add(tempCount + ":" + floats + " t:" + tt);
            lastTemp = tt;
            tempCount++;
            mFloats.clear();
            return tt;
        }
        return lastTemp;
    }

    @Override
    public TemperatureEntity parse(byte[] data, int distance) {
        if (data == null) return null;
        if ((data[0] & 0xFF) == 0xa5 && (data[1] & 0xFF) == 0x06
                && (data[data.length - 1] & 0xFF) == 0xBF) {
            TemperatureEntity entity = new TemperatureEntity();
            List<Float> temps = new ArrayList<>();
            entity.min = entity.max = (((data[3] & 0xFF) << 8 | (data[4] & 0xFF))) / 100.0f;
//            entity.ta = ((data[data.length - 5] & 0xFF) << 8 | (data[data.length - 4] & 0xFF)) / 100.0f;
            for (int i = 3; i < data.length - 5; i = i + 2) {
                int sum = (data[i] & 0xFF) << 8 | (data[i + 1] & 0xFF);
                float temp = sum / 100f;
                if (temp < entity.min) entity.min = temp;
                if (temp > entity.max) entity.max = temp;
                temps.add(temp);
            }
            entity.tempList = temps;
//            entity.temperatue = check(entity.max, entity.ta);
//            float temperatue = check(entity.max, entity.ta);
            float temperatue = entity.max;
//            temperatue = handlerTemp(temperatue, distance);
            entity.temperatue = temperatue;
            return entity;
        }
        return null;
    }


    /*@Override
    public float afterHandler(float source) {
        float temp = source;
        //低于30则认定为探头被遮挡了
        if (source >= 29 && source <= 36) {
            Random random = new Random();
            float add = random.nextInt(5) * 0.1f;
            temp = 36.3f + add;
        }
        return temp;
    }*/
}
