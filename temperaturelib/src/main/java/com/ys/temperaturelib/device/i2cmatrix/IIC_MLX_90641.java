package com.ys.temperaturelib.device.i2cmatrix;


import android.util.Log;

import com.ys.mlx90641.Mlx90641;
import com.ys.temperaturelib.device.IMatrixThermometer;
import com.ys.temperaturelib.temperature.MeasureParm;
import com.ys.temperaturelib.temperature.TemperatureEntity;
import com.ys.temperaturelib.temperature.TemperatureParser;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class IIC_MLX_90641 extends IMatrixThermometer implements TemperatureParser<float[]> {
    public static final String MODE_NAME = "MLX90641-16*12";
    static final int MATRIX_COUT_X = 16; //温度矩阵横坐标总数量
    static final int MATRIX_COUT_Y = 12; //温度矩阵横坐标总数量
    Mlx90641 mMlx90641;

    public IIC_MLX_90641() {
        mMlx90641 = new Mlx90641();
        setParser(this);
        setParm(new MeasureParm(MODE_NAME, 50, 250, MATRIX_COUT_X, MATRIX_COUT_Y));
    }

    @Override
    protected float[] read() {
        if (mMlx90641 == null) return null;
        return mMlx90641.read();
    }

    @Override
    protected void release() {
        if (mMlx90641 == null) return;
        mMlx90641.release();
        mMlx90641 = null;
    }

    @Override
    protected boolean init() {
        if (mMlx90641 != null) {
            int init = mMlx90641.init(Mlx90641.RATE_8HZ);
            return init > 0;
        }
        return false;
    }

    @Override
    public boolean getDevStatues() {
        return false;
    }

    @Override
    public void order(byte[] data) {

    }

    int count = 0;
    List<Float> mFloats = new ArrayList<>();
    float lastTemp = 0;
    int tempCount = 0;

    @Override
    public float check(float value, float ta) {
//        TakeTempEntity takeTempEntity = getTakeTempEntity();
//        if (!takeTempEntity.isNeedCheck()) return value;
        count++;
        mFloats.add(value);
        if (mFloats.size() == 6) {
            tempCount = 5;
        } else if (mFloats.size() > 6) {
            List<Float> floats = mFloats.subList(tempCount - 3, tempCount - 3 + 5);
            float sum = 0;
            float max = floats.get(0);
            float min = floats.get(0);

            for (int i = 0; i < floats.size(); i++) {
                sum += floats.get(i);
                if (floats.get(i) > max) max = floats.get(i);
                if (floats.get(i) < min) min = floats.get(i);
            }

            float tt = (sum - max) / 4f; //+ takeTempEntity.getTakeTemperature();
//            if (tt >= 34f && tt < 36f) {
//                int tt1 = (int) (tt * 100);
//                tt = Float.parseFloat("36." + String.valueOf(tt1).substring(2, 4));
//            } else if (tt >= 37.2f && tt <= 37.5f) {
//                tt += 0.3f;
//            }
//            getStorager().add(tempCount + ":" + floats + " t:" + tt);
            lastTemp = tt;
            tempCount++;
            mFloats.clear();
            return tt;
        }
        return lastTemp;
    }

    @Override
    public float[] oneFrame(float[] data) {
        if (data.length == 193) {
            return data;
        }
        return null;
    }

    @Override
    public TemperatureEntity parse(float[] data, int distance) {
        if (data != null && data.length >= 193) {
            TemperatureEntity entity = new TemperatureEntity();
            List<Float> temps = new ArrayList<>();
            entity.min = entity.max = data[0] > 42 ? 42 : data[0];
            boolean isvalid = true;
            for (int i = 0; i < data.length - 2; i++) {
                if (data[i] < 0) {
                    isvalid = false;
                    break;
                }
                float temp = data[i];
                if (temp < entity.min) entity.min = temp;
                if (temp > entity.max) entity.max = temp;
                temps.add(temp);
            }
            entity.tempList = temps;
            entity.ta = data[192];
            float temp = check(entity.max, entity.ta);
            entity.source = temp;
            entity.temperatue = handlerTemp(temp, entity.ta, distance);
            return isvalid ? entity : null;
        }
        return null;
    }

    private float handlerTemp(float to, float ta, int distance) {
        float temp = to;
        float comp = 0;

        if (distance >= 100) {
            comp = 0;
        } else if (distance > 85) {
            comp = 0.9f;
        } else if (distance > 75) {
            comp = 0.8f;
        } else if (distance > 65) {
            comp = 0.7f;
        } else if (distance > 55) {
            comp = 0.6f;
        } else if (distance > 45) {
            comp = 0.5f;
        } else if (distance > 35) {
            comp = 0.4f;
        } else if (distance > 25) {
            comp = 0.3f;
        }
//        Log.d("IMLX90641", "handlerTemp: " + temp + " / " + comp);
        temp += comp;
        return temp;
    }


    @Override
    public float afterHandler(float temp, int distance) {
        float add = ((int) (Math.random() * 5)) * 0.1f;
        Log.d("IMLX90641_16x12", "afterHandler: " + temp + " / " + add);
        if (temp >= 30.0 && temp < 35.5) {
            return 35.8f + add;
        } else if (temp >= 35.5 && temp < 36.1) {
            return 36.2f + add;
        } else if (temp >= 36.1 && temp < 36.3) {
            return temp + 0.2f;
        }
        return temp;
    }

    private String getRandom(int min, int max) {
        Random random = new Random();
        int s = random.nextInt(max) % (max - min + 1) + min;
        return String.valueOf(s);
    }

}
