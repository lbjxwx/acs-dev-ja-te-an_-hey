package com.ys.temperaturelib.device.serialport;

import com.ys.temperaturelib.temperature.MeasureParm;
import com.ys.temperaturelib.temperature.TakeTempEntity;
import com.ys.temperaturelib.temperature.TemperatureEntity;
import com.ys.temperaturelib.temperature.TemperatureParser;
import com.ys.temperaturelib.utils.DataFormatUtil;
import com.ys.temperaturelib.utils.TempWrongUtil;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class SP_MLX_90621_STM extends ProductImp implements TemperatureParser<byte[]> {
    public static final String DEFAULT_MODE_NAME = "MLX90621-YS(矩阵)"; //型号
    static final String DEFAULT_DEVICE = "/dev/ttyS3"; //设备号
    static final int DEFAULT_RATE = 115200; //波特率

    static final int MATRIX_COUT_X = 16; //温度矩阵横坐标总数量
    static final int MATRIX_COUT_Y = 4; //温度矩阵纵坐标总数量

    static final byte[] ORDER_DATA_OUTPUT_AUTO = new byte[]{(byte) 0xA5, 0x06, 0x01, (byte) 0xBF}; //自动测量  A50601BF
    static final byte[] ORDER_DATA_OUTPUT_MANUAL = new byte[]{(byte) 0xA5, 0x06, 0x00, (byte) 0xBF}; //手动测量  A50600BF

    public SP_MLX_90621_STM(String device, int rare) {
        super(device, rare,
                new MeasureParm(DEFAULT_MODE_NAME, 40, 50, MATRIX_COUT_X, MATRIX_COUT_Y));
        setTemperatureParser(this);
        setTakeTempEntity(getDefaultTakeTempEntities()[0]);
        setWriteInThread(true);
    }


    @Override
    public TakeTempEntity[] getDefaultTakeTempEntities() {
        TakeTempEntity[] entities = new TakeTempEntity[5];
        TakeTempEntity entity0 = new TakeTempEntity();
        entity0.setDistances(10);
        entity0.setTakeTemperature(4.3f);//2.3  0  2
        entities[0] = entity0;

        TakeTempEntity entity1 = new TakeTempEntity();
        entity1.setDistances(20);
        entity1.setTakeTemperature(4.6f);//3.15  -0.05  1.5
        entities[1] = entity1;

        TakeTempEntity entity2 = new TakeTempEntity();
        entity2.setDistances(30);
        entity2.setTakeTemperature(4.75f);//3.7 -0.15  1.2
        entities[2] = entity2;

        TakeTempEntity entity3 = new TakeTempEntity();
        entity3.setDistances(40);
        entity3.setTakeTemperature(5.2f);//4.25  -0.05  1
        entities[3] = entity3;

        TakeTempEntity entity4 = new TakeTempEntity();
        entity4.setDistances(50);
        entity4.setTakeTemperature(5.1f);//4.6  -0.2  0.7
        entities[4] = entity4;
        return entities;
    }

    @Override
    public byte[] getOrderDataOutputType(boolean isAuto) {
        return isAuto ? ORDER_DATA_OUTPUT_AUTO : ORDER_DATA_OUTPUT_MANUAL;
    }

    @Override
    public byte[] getOrderDataOutputQuery() {
        return ORDER_DATA_OUTPUT_MANUAL;
    }

    @Override
    public boolean isPoint() {
        return false;
    }

    @Override
    public byte[] oneFrame(byte[] data) {
        return data;
    }


    int count = 0;
    LinkedList<Float> mFloats = new LinkedList<>();
    float lastTemp = 0;
    int tempCount = 0;

    @Override
    public float check(float value, float ta) {
        mFloats.add(value);
        if (mFloats.size() > 4) {
            mFloats.remove(0);
        }
        float sum = 0;
        for (Float temp : mFloats) {
            sum += temp;
        }
        float result = sum / mFloats.size();
        return result;


        /*TakeTempEntity takeTempEntity = getTakeTempEntity();
        if (!takeTempEntity.isNeedCheck()) return value;
        count++;
        mFloats.add(value);
        if (mFloats.size() == 4) {
            tempCount = 3;
        } else if (mFloats.size() > 4) {
            List<Float> floats = mFloats.subList(tempCount - 1, tempCount - 1 + 3);
            float sum = 0;
            float max = floats.get(0);
            float min = floats.get(0);

            for (int i = 0; i < floats.size(); i++) {
                sum += floats.get(i);
                if (floats.get(i) > max) max = floats.get(i);
                if (floats.get(i) < min) min = floats.get(i);
            }

            float tt = sum / 3f + takeTempEntity.getTakeTemperature();
//            if (tt >= 34f && tt < 36f) {
//                int tt1 = (int) (tt * 100);
//                tt = Float.parseFloat("36." + String.valueOf(tt1).substring(2, 4));
//            } else if (tt >= 37.2f && tt <= 37.5f) {
//                tt += 0.3f;
//            }
//            getStorager().add(tempCount + ":" + floats + " t:" + tt);
            lastTemp = tt;
            tempCount++;
            mFloats.clear();
            return tt;
        }
        return lastTemp;*/
    }

    @Override
    public TemperatureEntity parse(byte[] data, int distance) {
        if (data == null) return null;
        if (data.length > 7) {
            if ((data[0] & 0xFF) == 0xa5 && (data[1] & 0xFF) == 0x06
                    && (data[data.length - 1] & 0xFF) == 0xBF) {
                TemperatureEntity entity = new TemperatureEntity();
                List<Float> temps = new ArrayList<>();
                entity.min = entity.max = DataFormatUtil.getByteToInt(data[3], data[4]) / 100.0f;
                entity.ta = DataFormatUtil.getByteToInt(data[data.length - 5], data[data.length - 4]) / 100.0f;
                for (int i = 3; i < data.length - 5; i = i + 2) {
                    int sum = DataFormatUtil.getByteToInt(data[i], data[i + 1]);
                    float temp = sum / 100f;
                    if (temp < entity.min) entity.min = temp;
                    if (temp > entity.max) entity.max = temp;
                    temps.add(temp);
                }
                entity.tempList = temps;
                entity.temperatue = check(entity.max, entity.ta);
//                TemperatureStorager.getInstance().add("" + getSort(entity.tempList));
                return entity;
            }
        }
        return null;
    }

    @Override
    public float afterHandler(float source, int distance) {
        float temp = source;
        //低于30则认定为探头被遮挡了
        if (source >= 20 && source <= 36) {
            temp = TempWrongUtil.getTempWrongInfo(source, distance);
        } else if (source > 99) {
            Random randomHeight = new Random();
            float addHeight = randomHeight.nextInt(5) * 0.1f;
            temp = 36.2f + addHeight;
        }
        return temp;
    }
}
