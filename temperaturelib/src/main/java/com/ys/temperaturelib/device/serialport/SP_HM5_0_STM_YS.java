package com.ys.temperaturelib.device.serialport;

import android.util.Log;

import com.ys.temperaturelib.temperature.MeasureParm;
import com.ys.temperaturelib.temperature.TemperatureEntity;
import com.ys.temperaturelib.temperature.TemperatureParser;
import com.ys.temperaturelib.utils.DataFormatUtil;
import com.ys.temperaturelib.utils.TempWrongUtil;

// Created by kermitye on 2020/6/5 8:45
public class SP_HM5_0_STM_YS extends ProductImp implements TemperatureParser<byte[]> {
    public static final String DEFAULT_MODE_NAME = "SP_HM5_0_32X32_YS"; //型号
    static final String DEFAULT_DEVICE = "/dev/ttyS3"; //设备号
    static final int DEFAULT_RATE = 115200; //波特率

    static final int MATRIX_COUT_X = 0; //温度矩阵横坐标总数量
    static final int MATRIX_COUT_Y = 0; //温度矩阵纵坐标总数量

    static final byte[] ORDER_DATA_OUTPUT_AUTO = new byte[]{(byte) 0xA5, 0x05, 0x01, (byte) 0xBF}; //自动测量  A50601BF
    static final byte[] ORDER_DATA_OUTPUT_MANUAL = new byte[]{(byte) 0xA5, 0x05, 0x00, (byte) 0xBF}; //手动测量  A50600BF

    public SP_HM5_0_STM_YS(String device, int rare) {
        super(device, rare,
                new MeasureParm(DEFAULT_MODE_NAME, 50, 200, MATRIX_COUT_X, MATRIX_COUT_Y));
        setTemperatureParser(this);
        setWriteInThread(true);
    }

    @Override
    public byte[] getOrderDataOutputType(boolean isAuto) {
        return isAuto ? ORDER_DATA_OUTPUT_AUTO : ORDER_DATA_OUTPUT_MANUAL;
    }

    @Override
    public byte[] getOrderDataOutputQuery() {
        return ORDER_DATA_OUTPUT_AUTO;
    }

    @Override
    public boolean isPoint() {
        return true;
    }

    @Override
    public byte[] oneFrame(byte[] data) {
        return data;
    }

    @Override
    public float check(float value, float ta) {
        return value;
    }


    @Override
    public float check(float value, float ta, int distance) {
        //根据距离做补偿
        if (value <= 0) {
        } else if (distance <= 15) {
            value += 1.35f;
        } else if (distance <= 25) {
            value += 1.60f;
        } else if (distance <= 35) {
            value += 2.05f;
        } else if (distance <= 45) {
            value += 2.2f;
        } else if (distance <= 55) {
            value += 2.35f;
        } else if (distance <= 65) {
            value += 2.6f;
        } else if (distance <= 75) {
            value += 2.8f;
        } else {
            value += 3.15f;
        }
        return value;
    }

    @Override
    public TemperatureEntity parse(byte[] data, int distance) {
        log("接收到的数据：" + DataFormatUtil.bytesToHex(data));
        if (data == null || data.length < 12) return null;
        TemperatureEntity entity = new TemperatureEntity();
        entity.ta = ((data[4] & 0xFF) << 8) | (data[5] & 0xFF);
        entity.ta /= 10f;
        entity.max = ((data[6] & 0xFF) << 8) | (data[7] & 0xFF);
        entity.max /= 10f;
        entity.min = ((data[8] & 0xFF) << 8) | (data[9] & 0xFF);
        entity.min /= 10f;
        entity.temperatue = ((data[10] & 0xFF) << 8) | (data[11] & 0xFF);
        entity.temperatue /= 10f;
        entity.source = entity.temperatue;
        entity.temperatue = check(entity.temperatue, entity.ta, distance);
        return entity;
    }

    @Override
    public float afterHandler(float temp, int distance) {
        float tempBack = temp;
        if (temp >= 15 && temp < 36f) {
            tempBack = TempWrongUtil.getTempWrongInfo(temp, distance);
        } else if (temp >= 36f && temp < 36.3f) {
            tempBack += 0.3f;
        } else if (temp >= 36.9 && temp <= 37.3) {
            tempBack -= 0.4f;
        }
        Log.e("afterHandler", "=======afterHandler=======" + temp + " / " + distance + " / " + tempBack);
        return tempBack;
    }

    private void log(String msg) {
        Log.d(DEFAULT_MODE_NAME, msg);
    }

}
