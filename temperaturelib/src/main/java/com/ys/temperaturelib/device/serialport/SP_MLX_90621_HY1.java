package com.ys.temperaturelib.device.serialport;

import android.util.Log;

import com.ys.temperaturelib.temperature.MeasureParm;
import com.ys.temperaturelib.temperature.TakeTempEntity;
import com.ys.temperaturelib.temperature.TemperatureEntity;
import com.ys.temperaturelib.temperature.TemperatureParser;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class SP_MLX_90621_HY1 extends ProductImp implements TemperatureParser<byte[]> {
    public static final String DEFAULT_MODE_NAME = "MLX90621-YS(矩阵)"; //型号
    static final String DEFAULT_DEVICE = "/dev/ttyS3"; //设备号
    static final int DEFAULT_RATE = 115200; //波特率

    static final int MATRIX_COUT_X = 16; //温度矩阵横坐标总数量
    static final int MATRIX_COUT_Y = 4; //温度矩阵纵坐标总数量

    static final byte[] ORDER_DATA_OUTPUT_AUTO = new byte[]{(byte) 0xA5, 0x55, 0x02, (byte) 0xFB}; //自动测量  A50601BF

    public SP_MLX_90621_HY1(String device, int rare) {
        super(device, rare,
                new MeasureParm(DEFAULT_MODE_NAME, 40, 0, MATRIX_COUT_X, MATRIX_COUT_Y));
        setTemperatureParser(this);
//        setWriteInThread(false);
    }


    @Override
    public byte[] getOrderDataOutputType(boolean isAuto) {
        return isAuto ? ORDER_DATA_OUTPUT_AUTO : ORDER_DATA_OUTPUT_AUTO;
    }

    @Override
    public byte[] getOrderDataOutputQuery() {
        return ORDER_DATA_OUTPUT_AUTO;
    }

    @Override
    public boolean isPoint() {
        return false;
    }

    @Override
    public byte[] oneFrame(byte[] data) {
        return data;
    }


    int count = 0;
    List<Float> mFloats = new ArrayList<>();
    float lastTemp = 0;
    int tempCount = 0;


    @Override
    public TemperatureEntity parse(byte[] data, int distance) {
        if (data == null) return null;
        if ((data[0] & 0xFF) == 0xAA && (data[1] & 0xFF) == 0x55 && data.length == 6) {
            TemperatureEntity entity = new TemperatureEntity();

            float temperatue = 0;
            int intValue = data[3];
            int xValue = data[4];
            if (xValue < 10) {
                temperatue = intValue + (xValue / 10f);
            } else if (xValue < 100) {
                temperatue = intValue + (xValue / 100f);
            }
            entity.temperatue = temperatue;
//            Log.e("SMLX90621_HY1", "=====温度：" + temperatue);
            return entity;
        }
        return null;
    }
}
