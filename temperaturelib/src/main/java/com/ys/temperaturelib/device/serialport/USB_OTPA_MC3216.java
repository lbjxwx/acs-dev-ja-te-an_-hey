package com.ys.temperaturelib.device.serialport;

import android.util.Log;

import com.ys.temperaturelib.temperature.MeasureParm;
import com.ys.temperaturelib.temperature.TemperatureEntity;
import com.ys.temperaturelib.temperature.TemperatureParser;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class USB_OTPA_MC3216 extends ProductImp implements TemperatureParser<byte[]> {
    public static final String DEFAULT_MODE_NAME = "SMLX_MC3216"; //型号
    static final String DEFAULT_DEVICE = "/dev/ttyS3"; //设备号
    static final int DEFAULT_RATE = 115200; //波特率

    static final int MATRIX_COUT_X = 16; //温度矩阵横坐标总数量
    static final int MATRIX_COUT_Y = 4; //温度矩阵纵坐标总数量


    public USB_OTPA_MC3216(String device, int rare) {
        super(device, rare,
                new MeasureParm(DEFAULT_MODE_NAME, 40, 200, MATRIX_COUT_X, MATRIX_COUT_Y));
        setTemperatureParser(this);
    }


    @Override
    public byte[] getOrderDataOutputType(boolean isAuto) {
        return null;
    }

    @Override
    public byte[] getOrderDataOutputQuery() {
        return null;
    }

    @Override
    public boolean isPoint() {
        return false;
    }

    @Override
    public byte[] oneFrame(byte[] data) {
        return data;
    }


    @Override
    public float check(float value, float ta) {
        return value;
    }

    @Override
    public TemperatureEntity parse(byte[] data, int distance) {
        if (data == null) return null;
        String tempStr = new String(data);
        log("=====tempStr:" + tempStr);
        if (!tempStr.contains(","))
            return null;
        try {
            TemperatureEntity entity = new TemperatureEntity();
            String[] split = tempStr.split(",");
            if (split.length > 3) {
                float temperatue = Float.parseFloat(split[2]);
                entity.source = temperatue;
                //根据距离做补偿
                if (temperatue == 0) {
                    temperatue = 0;
                } else if (distance <= 25) {
                    temperatue += 0.25f;
                } else if (distance <= 35) {
                    temperatue += 1.0f;
                } else if (distance <= 45) {
                    temperatue += 1.5f;
                } else if (distance <= 55) {
                    temperatue += 1.75f;
                } else if (distance <= 65) {
                    temperatue += 1.95f;
                } else if (distance <= 75) {
                    temperatue += 2.1f;
                } else if (distance <= 85) {
                    temperatue += 2.5f;
                } else {
                    temperatue += 2.7f;
                }
                entity.temperatue = temperatue;
                entity.data = data;
                return entity;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    Random random = new Random();

    @Override
    public float afterHandler(float temp, int distance) {
        if (temp >= 35 && temp < 36) {
            float add = random.nextInt(4) * 0.1f;
            return 36.0f + add;
        } else if (temp >= 36 && temp <= 36.4) {
            return temp + 0.3f;
        } else if (temp >= 36.8 && temp <= 37.3) {
            return temp - 0.4f;
        }
        return temp;
    }

    private void log(String msg) {
        Log.e(DEFAULT_MODE_NAME, msg);
    }

}
