package com.ys.temperaturelib.device.serialport;

import com.ys.temperaturelib.temperature.MeasureParm;
import com.ys.temperaturelib.temperature.TemperatureEntity;
import com.ys.temperaturelib.temperature.TemperatureParser;

public class SP_OMR_D6T_4X4 extends ProductImp implements TemperatureParser<byte[]> {
    public static final String DEFAULT_MODE_NAME = "SMLX_OMRON_D6T_4x4(矩阵)"; //型号
    static final int DEFAULT_DEVICE = 4; //设备号 "/dev/ttyS3"
    static final int DEFAULT_RATE = 4; //波特率 115200

    static final int MATRIX_COUT_X = 16; //温度矩阵横坐标总数量
    static final int MATRIX_COUT_Y = 4; //温度矩阵纵坐标总数量


    int mRectDistance; //距离

    public SP_OMR_D6T_4X4(String device, int rare) {
        super(device, rare,
                new MeasureParm(DEFAULT_MODE_NAME, 40, 100, MATRIX_COUT_X, MATRIX_COUT_Y));
        setTemperatureParser(this);
//        setWriteInThread(true);
    }


    @Override
    public byte[] getOrderDataOutputType(boolean isAuto) {
        return null;
    }

    @Override
    public byte[] getOrderDataOutputQuery() {
        return null;
    }

    @Override
    public boolean isPoint() {
        return false;
    }

    @Override
    public byte[] oneFrame(byte[] data) {
        return data;
    }

    @Override
    public TemperatureEntity parse(byte[] data, int distance) {
        if (data == null || data.length < 6) return null;
        if ((data[0] & 0xFF) == 0xaa && (data[1] & 0xFF) == 0x55) {
            TemperatureEntity entity = new TemperatureEntity();

            float temperatue = 0;
            int intValue = data[3];
            int xValue = data[4];
            if (xValue < 10) {
                temperatue = intValue + (xValue / 10f);
            } else if (xValue < 100) {
                temperatue = intValue + (xValue / 100f);
            }
            entity.temperatue = temperatue;
            log("======temp:" + entity.temperatue);
            return entity;
        }
        return null;
    }


    private void log(String msg) {
//        Log.e("SMLX_OMRON_D6T_4x4", msg);
    }
}
