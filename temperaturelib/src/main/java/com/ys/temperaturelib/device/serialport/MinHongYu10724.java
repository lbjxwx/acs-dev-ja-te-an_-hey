//package com.ys.temperaturelib.device.serialport;
//
//
//import android.os.Handler;
//import android.os.Message;
//import android.util.Log;
//
//import androidx.annotation.NonNull;
//
//import com.dashu.temperature.DsTemperature;
//import com.ys.temperaturelib.device.MeasureDevice;
//import com.ys.temperaturelib.device.MeasureResult;
//import com.ys.temperaturelib.temperature.TemperatureEntity;
//
///***
// * 铭鸿宇客户定制温度检测
// */
//public class MinHongYu10724 extends MeasureDevice {
//
//    //串口编号
//    String mDevice;
//
//    public MinHongYu10724(String device, int rare) {
//        setDevice(device);
//    }
//
//    DsTemperature dsTemperature;
//
//    //    String serMsg = "/dev/ttyS3";
//    @Override
//    protected boolean init() {
//        dsTemperature = new DsTemperature();
//        dsTemperature.Ds_Initialize();
//        int statues = dsTemperature.Ds_OpenUart(mDevice);
//        if (statues == 0) {
//            return true;
//        } else {
//            return false;
//        }
//    }
//
//    boolean isTrue = true;
//    MeasureResult result;
//
//
//    public void setDevice(int device) {
//        switch (device) {
//            case 0:
//                mDevice = "/dev/ttyS0";
//                break;
//            case 1:
//                mDevice = "/dev/ttyS1";
//                break;
//            case 2:
//                mDevice = "/dev/ttyS2";
//                break;
//            case 3:
//                mDevice = "/dev/ttyS3";
//                break;
//            case 4:
//                mDevice = "/dev/ttyS4";
//                break;
//        }
//    }
//
//
//    @Override
//    public void startUp(MeasureResult result, long period) {
//        this.result = result;
//        isTrue = init();
//        startTemp(period);
//    }
//
//    public void updateEnvTemperature() {
//        if (dsTemperature != null) {
//            dsTemperature.Ds_UpdateEnvTemperature();
//        }
//    }
//
//    private void startTemp(long period) {
//        new Thread() {
//            @Override
//            public void run() {
//                super.run();
//                while (isTrue) {
//                    try {
//                        Thread.sleep(400);
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                    float temp = dsTemperature.Ds_GetBodyTemperature();
//                    TemperatureEntity tempentity = new TemperatureEntity();
//                    tempentity.temperatue = temp;
//                    result.onResult(tempentity, null);
//                    Log.e("cdl", "========temp==MHY======" + temp);
////                    Message message = new Message();
////                    message.what = CODE_MSG;
////                    message.obj = temp;
////                    handler.sendMessage(message);
//                }
//            }
//        }.start();
//    }
//
//    private static final int CODE_MSG = 2546;
//    private Handler handler = new Handler() {
//        @Override
//        public void handleMessage(@NonNull Message msg) {
//            super.handleMessage(msg);
////            switch (msg.what) {
////                case CODE_MSG:
////                    float tempNum = (float) msg.obj;
////                    result.onResult();
////                    break;
////            }
//        }
//    };
//
//
//    @Override
//    public boolean getDevStatues() {
//        if (dsTemperature == null) {
//            return false;
//        }
//        int statues = dsTemperature.Ds_GetUartState();
//        if (statues == 0) {
//            return true;
//        } else {
//            return false;
//        }
//    }
//
//    @Override
//    public void order(byte[] data) {
//
//    }
//
//    @Override
//    public void destroy() {
//        isTrue = false;
//        if (dsTemperature != null) {
//            dsTemperature.Ds_InInitialize();
//        }
//    }
//
//    @Override
//    public float check(float value, float ta) {
//        return 0;
//    }
//
//    @Override
//    public boolean isPoint() {
//        return false;
//    }
//
//}
