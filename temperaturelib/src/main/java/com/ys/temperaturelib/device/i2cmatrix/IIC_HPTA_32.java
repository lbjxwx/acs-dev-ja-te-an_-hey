package com.ys.temperaturelib.device.i2cmatrix;

import com.ys.libhtpa3232.htpa3232;
import com.ys.temperaturelib.device.IMatrixThermometer;
import com.ys.temperaturelib.temperature.MeasureParm;
import com.ys.temperaturelib.temperature.TakeTempEntity;
import com.ys.temperaturelib.temperature.TemperatureEntity;
import com.ys.temperaturelib.temperature.TemperatureParser;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class IIC_HPTA_32 extends IMatrixThermometer implements TemperatureParser<float[]> {
    public static final String MODE_NAME = "HTPA_32x32(矩阵)";
    public static final int MATRIX_COUT_X = 32; //温度矩阵横坐标总数量
    public static final int MATRIX_COUT_Y = 32; //温度矩阵横坐标总数量
    htpa3232 mHtpa3232;

    public IIC_HPTA_32() {
        mHtpa3232 = new htpa3232();
        setParser(this);
        setParm(new MeasureParm(MODE_NAME, 50, 100, MATRIX_COUT_X, MATRIX_COUT_Y));
        setTakeTempEntity(getDefaultTakeTempEntities()[0]);
    }

    @Override
    public TakeTempEntity[] getDefaultTakeTempEntities() {
        TakeTempEntity[] entities = new TakeTempEntity[8];
        TakeTempEntity entity1 = new TakeTempEntity();
        entity1.setDistances(10);
        entity1.setTakeTemperature(0.3f);
        entities[0] = entity1;
        TakeTempEntity entity2 = new TakeTempEntity();
        entity2.setDistances(20);
        entity2.setTakeTemperature(0.9f);
        entities[1] = entity2;
        TakeTempEntity entity3 = new TakeTempEntity();
        entity3.setDistances(30);
        entity3.setTakeTemperature(1.5f);
        entities[2] = entity3;
        TakeTempEntity entity4 = new TakeTempEntity();
        entity4.setDistances(40);
        entity4.setTakeTemperature(1.9f);
        entities[3] = entity4;
        TakeTempEntity entity5 = new TakeTempEntity();
        entity5.setDistances(50);
        entity5.setTakeTemperature(2.1f);
        entities[4] = entity5;
        TakeTempEntity entity6 = new TakeTempEntity();
        entity6.setDistances(60);
        entity6.setTakeTemperature(2.4f);
        entities[5] = entity6;
        TakeTempEntity entity7 = new TakeTempEntity();
        entity7.setDistances(70);
        entity7.setTakeTemperature(2.7f);
        entities[6] = entity7;
        TakeTempEntity entity8 = new TakeTempEntity();
        entity8.setDistances(100);
        entity8.setTakeTemperature(3f);
        entities[7] = entity8;
        return entities;
    }


    @Override
    protected float[] read() {
        if (mHtpa3232 == null) return null;
        int[] read = mHtpa3232.read();
        if (read == null) return null;
        float[] temps = new float[read.length];
        for (int i = 0; i < read.length; i++) {
            temps[i] = read[i] / 10f;
        }
        return temps;
    }

    @Override
    protected void release() {
        if (mHtpa3232 == null) return;
        mHtpa3232.release();
        mHtpa3232 = null;
    }

    @Override
    protected boolean init() {
        if (mHtpa3232 != null) {
            int init = mHtpa3232.init();
            return init > 0;
        }
        return false;
    }

    @Override
    public boolean getDevStatues() {
        return false;
    }

    @Override
    public void order(byte[] data) {

    }

    @Override
    public float check(float value, float ta) {
        return value;
    }


    @Override
    public float[] oneFrame(float[] data) {
        return data;
    }

    @Override
    public TemperatureEntity parse(float[] data, int distance) {
        if (data != null) {
            TemperatureEntity entity = new TemperatureEntity();
            List<Float> temps = new ArrayList<>();
            entity.ta = data[1024];
            entity.min = data[1025];
            entity.max = data[1026];
//            for (int i = 0; i < 1024; i++) {
//                float temp = check(data[i], entity);
//                temps.add(temp);
//            }
            float temperatue = data[1027];

            if (temperatue > 100)
                return null;

            entity.source = data[1027];
            //根据距离做补偿
            if (temperatue <= 0) {
            } else if (distance <= 15) {
            } else if (distance <= 25) {
                temperatue += 0.2f;
            } else if (distance <= 35) {
                temperatue += 0.5f;
            } else if (distance <= 45) {
                temperatue += 0.85f;
            } else if (distance <= 55) {
                temperatue += 1.2f;
            } else if (distance <= 65) {
                temperatue += 1.45f;
            } else {
                temperatue += 1.65f;
            }
            entity.tempList = temps;
            entity.temperatue = temperatue;
            return entity;
        }
        return null;
    }

    @Override
    public float afterHandler(float temp, int distance) {
        if (temp >= 34f && temp < 36f) {
            //线性处理
//            float flag = (float) ((demValue - 34) / 2f * 0.6);
            float flag = (float) ((temp - 34) / 2f * 0.6);
            temp = 36 + flag;
//            float[] teps = new float[]{36.0f, 36.1f, 36.2f, 36.3f};
//            int index = (int) (Math.random() * teps.length);
//            temp = teps[index];
        } else if (temp >= 36f && temp <= 36.4f) {
            temp += 0.3f;
        } else if (temp >= 36.8f && temp <= 37.2f) {
            temp -= 0.4f;
        }
        return temp;
    }
}
