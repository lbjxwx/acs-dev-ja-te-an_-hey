package com.ys.temperaturelib.device.serialport;

import android.util.Log;

import com.ys.temperaturelib.temperature.MeasureParm;
import com.ys.temperaturelib.temperature.TemperatureEntity;
import com.ys.temperaturelib.temperature.TemperatureParser;

public class SP_MLX_90641_STM extends ProductImp implements TemperatureParser<byte[]> {
    public static final String DEFAULT_MODE_NAME = "SMLX90641_STM(矩阵)"; //型号
    static final String DEFAULT_DEVICE = "/dev/ttyS3"; //设备号
    static final int DEFAULT_RATE = 115200; //波特率

    static final int MATRIX_COUT_X = 16; //温度矩阵横坐标总数量
    static final int MATRIX_COUT_Y = 4; //温度矩阵纵坐标总数量

//    static final byte[] ORDER_DATA_OUTPUT_AUTO = new byte[]{(byte) 0xA5, 0x06, 0x01, (byte) 0xBF}; //自动测量  A50601BF
//    static final byte[] ORDER_DATA_OUTPUT_MANUAL = new byte[]{(byte) 0xA5, 0x06, 0x00, (byte) 0xBF}; //手动测量  A50600BF

    static final byte[] ORDER_DATA_OUTPUT_AUTO = new byte[]{(byte) 0xA5, 0x05, 0x01, (byte) 0xBF}; //自动测量  A50601BF

    static final byte[] ORDER_DATA_OUTPUT_MANUAL = new byte[]{(byte) 0xA5, 0x05, 0x00, (byte) 0xBF}; //手动测量  A50600BF

    public SP_MLX_90641_STM(String device, int rare) {
        super(device, rare,
                new MeasureParm(DEFAULT_MODE_NAME, 40, 50, MATRIX_COUT_X, MATRIX_COUT_Y));
        setTemperatureParser(this);
//        setTakeTempEntity(getDefaultTakeTempEntities()[0]);
//        setWriteInThread(true);
    }


    @Override
    public byte[] getOrderDataOutputType(boolean isAuto) {
        return ORDER_DATA_OUTPUT_AUTO;
    }

    @Override
    public byte[] getOrderDataOutputQuery() {
        return ORDER_DATA_OUTPUT_AUTO;
    }

    @Override
    public boolean isPoint() {
        return false;
    }

    @Override
    public byte[] oneFrame(byte[] data) {
        return data;
    }

    TemperatureEntity entity = new TemperatureEntity();

    @Override
    public TemperatureEntity parse(byte[] data, int distance) {
        if (data == null) return null;
        if ((data[0] & 0xFF) == 0xA5) {
            entity.temperatue = ((data[10] & 0xFF) << 8 | (data[11] & 0xFF)) / 10f;
            entity.max = ((data[6] & 0xFF) << 8 | (data[7] & 0xFF)) / 10f;
            entity.ta = ((data[4] & 0xFF) << 8 | (data[5] & 0xFF)) / 10f;
            entity.min = ((data[8] & 0xFF) << 8 | (data[9] & 0xFF)) / 10f;
            return entity;
        }
        return null;


        /*if ((data[0] & 0xFF) == 0xa5 && (data[1] & 0xFF) == 0x05
                && (data[data.length - 1] & 0xFF) == 0xBF) {
//            log("获取到的数据:" + DataFormatUtil.bytesToHex(data));
            TemperatureEntity entity = new TemperatureEntity();
            List<Float> temps = new ArrayList<>();
            entity.min = entity.max = ((data[4] & 0xFF) << 8 | (data[5] & 0xFF)) / 10f;
//            log("temperatue start:" + entity.max);
//            if (entity.min > 120 || entity.max > 120) {
//                entity.min = entity.max = 36.0f;
//            }
            for (int i = 4; i < data.length - 2; i = i + 2) {
//                int sum = (data[i] & 0xFF) << 8 | (data[i + 1] & 0xFF);
                float temp = ((data[i] & 0xFF) << 8 | (data[i + 1] & 0xFF)) / 10f;
                if (temp > 120)
                    continue;
                if (temp < entity.min) entity.min = temp;
                if (temp > entity.max) entity.max = temp;
                temps.add(temp);
            }
            log("temperatue:" + entity.max);
            entity.temperatue = entity.max;
            return entity;
        }
        return null;*/
    }

    @Override
    public float afterHandler(float temp, int distance) {
        float add = ((int) (Math.random() * 5)) * 0.1f;
        if (temp >= 20.0 && temp < 35.5) {
            return 36.0f + add;
        } else if (temp >= 35 && temp < 36) {
            return 36.0f + add;
        } else if (temp >= 36.9 && temp <= 37.3) {
            temp -= 0.4;
        }
        return temp;
    }

    private void log(String msg) {
        Log.e("SMLX90641_STM", msg);
    }

}
