package com.ys.temperaturelib.device.serialport;

import com.ys.temperaturelib.device.MeasureResult;
import com.ys.temperaturelib.device.SThermometer;
import com.ys.temperaturelib.temperature.MeasureParm;

import java.util.List;

public abstract class ProductImp extends SThermometer {

    public ProductImp(String device, int baudrate, MeasureParm parm) {
        super();
        setDevice(device);
        setBaudrate(baudrate);
        setParm(parm);
    }

    public void startUp(MeasureResult result) {
        startUp(result, getParm().perid);
    }

    /**
     * 获取数据输出类型指令
     *
     * @param isAuto 是否自动输出数据
     * @return
     */
    public abstract byte[] getOrderDataOutputType(boolean isAuto);

    /**
     * 获取查询模式下输出指令
     *
     * @return
     */
    public abstract byte[] getOrderDataOutputQuery();

}
