package com.ys.temperaturelib.device;

import android.graphics.Point;

import com.ys.temperaturelib.temperature.TemperatureEntity;

public interface MeasureResult<T> {
    //额头坐标
    Point getHeadPoint();

    int getDistance();

    void onResult(TemperatureEntity entity, T oneFrame);
}
