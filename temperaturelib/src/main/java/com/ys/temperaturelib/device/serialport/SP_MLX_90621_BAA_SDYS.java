package com.ys.temperaturelib.device.serialport;

import android.util.Log;

import com.ys.temperaturelib.temperature.MeasureParm;
import com.ys.temperaturelib.temperature.TemperatureEntity;
import com.ys.temperaturelib.temperature.TemperatureParser;
import com.ys.temperaturelib.utils.TempWrongUtil;


public class SP_MLX_90621_BAA_SDYS extends ProductImp implements TemperatureParser<byte[]> {
    public static final String DEFAULT_MODE_NAME = "MLX90621-RR(矩阵)"; //型号

    static final int DEFAULT_DEVICE = 4; //设备号 "/dev/ttyS3"
    static final int DEFAULT_RATE = 4; //波特率 115200

    static final int MATRIX_COUT_X = 16; //温度矩阵横坐标总数量
    static final int MATRIX_COUT_Y = 4; //温度矩阵纵坐标总数量

    static final byte[] ORDER_DATA_OUTPUT = new byte[]{(byte) 0xA5, 0x55, 0x01, (byte) 0xFB}; //查询输出数据指令

    public SP_MLX_90621_BAA_SDYS(String device, int rare) {
        super(device, rare,
                new MeasureParm(DEFAULT_MODE_NAME, 40, 100, MATRIX_COUT_X, MATRIX_COUT_Y));
        setTemperatureParser(this);
        setWriteInThread(true);
    }


    @Override
    public byte[] getOrderDataOutputType(boolean isAuto) {
        return ORDER_DATA_OUTPUT;
    }

    @Override
    public byte[] getOrderDataOutputQuery() {
        return ORDER_DATA_OUTPUT;
    }

    @Override
    public boolean isPoint() {
        return false;
    }

    @Override
    public byte[] oneFrame(byte[] data) {
        return data;
    }

    @Override
    public TemperatureEntity parse(byte[] data, int distance) {
        if (data == null) {
            log("======temperatue: data==null");
            return null;
        }
        try {
            String tempStr = new String(data);
            log("=====tempStr:=====" + tempStr);
            TemperatureEntity entity = new TemperatureEntity();
            if (tempStr.contains("{") || tempStr.contains("}")) {
                String temp = tempStr.replaceAll("\\{|\\}", "");
                log("======replace: " + temp + "   这里是 TXT协议");
                float temperatue = TempWrongUtil.getTempWrongInfo(36.5f, distance);
                try {
                    temperatue = Float.parseFloat(temp);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                log("=====final value:" + temperatue);
                entity.temperatue = temperatue;
                return entity;
            }
            if ((data[0] & 0xFF) == 0xa5 && (data[1] & 0xFF) == 0x55) {
                entity.temperatue = ((data[2] & 0xFF) + 256 * (data[3] & 0xFF)) / 100f;
                log("======temperatue:" + entity.temperatue);
                return entity;
            }
            log("======temperatue: return null");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private void log(String msg) {
        Log.e("SMLX90621_BAA", msg);
    }
}
