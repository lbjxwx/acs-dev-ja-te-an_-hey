package com.ys.temperaturelib.device.serialport;

import com.ys.temperaturelib.temperature.MeasureParm;
import com.ys.temperaturelib.temperature.TemperatureEntity;
import com.ys.temperaturelib.temperature.TemperatureParser;
import com.ys.temperaturelib.utils.DataFormatUtil;

import java.util.ArrayList;
import java.util.List;

public class SP_KM_D801 extends ProductImp implements TemperatureParser<byte[]> {
    public static final String DEFAULT_MODE_NAME = "SMLX_KM_D801(矩阵)"; //型号
    static final int DEFAULT_DEVICE = 4; //设备号 "/dev/ttyS3"
    static final int DEFAULT_RATE = 4; //波特率 115200

    static final int MATRIX_COUT_X = 16; //温度矩阵横坐标总数量
    static final int MATRIX_COUT_Y = 4; //温度矩阵纵坐标总数量


    int mRectDistance; //距离

    public SP_KM_D801(String device, int rare) {
        super(device, rare,
                new MeasureParm(DEFAULT_MODE_NAME, 40, 100, MATRIX_COUT_X, MATRIX_COUT_Y));
        setTemperatureParser(this);
    }


    @Override
    public byte[] getOrderDataOutputType(boolean isAuto) {
        return null;
    }

    @Override
    public byte[] getOrderDataOutputQuery() {
        return null;
    }

    @Override
    public boolean isPoint() {
        return false;
    }

    @Override
    public byte[] oneFrame(byte[] data) {
        return data;
    }

    @Override
    public TemperatureEntity parse(byte[] data, int distance) {
        if (data == null || data.length < 6) return null;
        if ((data[0] & 0xFF) == 0xAA && (data[1] & 0xFF) == 0x55) {
            TemperatureEntity entity = new TemperatureEntity();

            String hightTxt = Integer.toHexString(data[3] & 0xFF);
            String lowTxt = Integer.toHexString(data[4] & 0xFF);
            float temp = 0;
            try {
                int hightValue = Integer.parseInt(hightTxt) * 10;
                float lowValue = Integer.parseInt(lowTxt) / 10f;
                temp = hightValue + lowValue;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
            entity.temperatue = temp;
            return entity;
        }
        return null;
    }


    private void log(String msg) {
//        Log.e("SMLX_KM_D801", msg);
    }
}
