package com.ys.temperaturelib.device;

import android.graphics.Point;
import android.os.SystemClock;
import android.util.Log;

import com.ys.temperaturelib.temperature.TemperatureEntity;
import com.ys.temperaturelib.temperature.TemperatureParser;

public abstract class IMatrixThermometer extends MeasureDevice {
    boolean enabled;
    DataRead mDataRead;
    TemperatureParser<float[]> mParser;
    TemperatureStorager mStorager;
    public Point headPoint;

    public IMatrixThermometer() {
        mStorager = new TemperatureStorager();
    }

    public TemperatureStorager getStorager() {
//        if (mStorager == null) {
//            mStorager = new TemperatureStorager();
//        }
        return mStorager;
    }

    public void setParser(TemperatureParser<float[]> parser) {
        mParser = parser;
    }

    public void startUp(MeasureResult result) {
        startUp(result, getParm().perid);
    }

    @Override
    public void startUp(MeasureResult result, long period) {
        enabled = init();
        if (enabled) {
            if (mDataRead != null) mDataRead.interrupt();
            mDataRead = null;
            mDataRead = new DataRead(result, period);
            mDataRead.start();
        }
    }

    public boolean isPoint() {
        return false;
    }

    protected abstract float[] read();

    protected abstract void release();

    @Override
    public void destroy() {
        enabled = false;
        if (mDataRead != null) mDataRead.interrupt();
        mDataRead = null;
        if (mStorager != null)
            mStorager.exit();
        mStorager = null;
    }

    /**
     * 数据读取线程
     */
    class DataRead extends Thread {
        MeasureResult mResult;
        long period;
        boolean isInterrupted;

        public DataRead(MeasureResult result, long period) {
            mResult = result;
            this.period = period;
        }

        @Override
        public void interrupt() {
            super.interrupt();
            isInterrupted = true;
        }

        @Override
        public void run() {
            super.run();
            while (true) {
                float[] read = read();
                if (read != null && mParser != null) {
                    float[] oneFrame = mParser.oneFrame(read);
                    if (oneFrame != null) {
                        headPoint = mResult == null ? null : mResult.getHeadPoint();
                        TemperatureEntity entity = mParser.parse(oneFrame, mResult == null ? 0 : mResult.getDistance());
                        if (mResult != null) mResult.onResult(entity, oneFrame);
                        if (mStorager != null) mStorager.add(entity);
                    }
                }
                if (isInterrupted) {
                    release();
                    break;
                }
                SystemClock.sleep(period);
            }
        }
    }
}
