//package com.ys.temperaturelib.dashu;
//
//public class DsTemperature {
//
//    /**
//     * @Description: 程序启动时调用
//     * @param: 无
//     * @return: void
//     */
//    public native void Ds_Initialize();
//
//    /**
//     * @Description:程序退出时调用
//     * @param: 无
//     * @return: void
//     */
//    public native void Ds_InInitialize();
//
//    /**
//     * @Description: 打开串口
//     * @param: pDev 串口设备节点，例："/dev/ttyAMA0","/dev/ttyAMA1","/dev/ttyAMA2"等
//     * @return: 0表示成功
//     */
//    public native int Ds_OpenUart(String pDev);
//
//    /**
//     * @Description: 关闭串口
//     * @return: 0表示成功
//     */
//    public native int Ds_CloseUart();
//
//
//
//
//    /**
//     * @Description: 获取测温模块串口通信状态
//     * @return: 0 表示通信状态正常  -1 表示通信状态异常
//     */
//    public native int Ds_GetUartState();
//
//    /**
//     * @Description: 更新环境温度，设备开机时、间隔一定的时间（如10min），一定要设备检测到无人时才能调用该函数更新环境温度，
//    并且要判断是否更新成功；该函数会通过测温模块自动读取环境温度，并且会阻塞5s
//     * @param: pEnvTemperature 输出当前环境温度
//     * @return: 0表示成功
//     */
//    public native float Ds_UpdateEnvTemperature();
//
//    /**
//     * @Description: 设置环境温度，设备开机时、间隔一定的时间（如10min），某些特殊场景下，由设备提供环境温度并且设置
//     * @param: fEnvTemperature 输入当前环境温度
//     * @return: 0表示成功
//     */
//    public native int Ds_SetEnvTemperature(float fEnvTemperature);
//
//    /**
//     * @Description: 获取人体温度
//     * @param: pBodyTemperature 人体温度
//     * @return: 0表示成功
//     */
//    public native float Ds_GetBodyTemperature();
//
//
//
//}
