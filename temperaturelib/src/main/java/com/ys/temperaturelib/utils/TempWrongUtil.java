package com.ys.temperaturelib.utils;

import android.util.Log;

import java.util.Random;

/***
 * 假数据处理工具类
 */
public class TempWrongUtil {

    /***
     * @param source
     * 原始温度
     * @param distance
     * @return
     */
    public static float getTempWrongInfo(float source, int distance) {
        if (distance > 100) {
            distance = 99;
        }
        float temp = source;
        Random random = new Random();
        float addCache = random.nextInt(3) * 0.1f;
        float numCache = (float) (100 - 23 - (distance * 1.0));
        if (numCache < 0) {
            numCache = 10f;
        }
        float num = numCache / 100;
        Log.e("tempAll", "=====getTempWrongInfo====" + distance + " / " + num);
        float add = num + addCache;
        if (add > 0.7) {
            add = 0.7f;
        }
        temp = 36.0f + add;
        return temp;
    }
}
