package com.ys.temperaturelib.temperature;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class TemperatureEntity implements Parcelable {

    public List<Float> tempList;
    public float max;
    public float min;
    public float ta;
    public float temperatue;
    public float source;
    public byte[] data;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(this.tempList);
        dest.writeFloat(this.max);
        dest.writeFloat(this.min);
        dest.writeFloat(this.ta);
        dest.writeFloat(this.temperatue);
        dest.writeFloat(this.source);
        dest.writeByteArray(this.data);
    }

    public TemperatureEntity() {
    }

    protected TemperatureEntity(Parcel in) {
        this.tempList = new ArrayList<Float>();
        in.readList(this.tempList, Float.class.getClassLoader());
        this.max = in.readFloat();
        this.min = in.readFloat();
        this.ta = in.readFloat();
        this.temperatue = in.readFloat();
        this.source = in.readFloat();
        this.data = in.createByteArray();
    }

    public static final Parcelable.Creator<TemperatureEntity> CREATOR = new Parcelable.Creator<TemperatureEntity>() {
        @Override
        public TemperatureEntity createFromParcel(Parcel source) {
            return new TemperatureEntity(source);
        }

        @Override
        public TemperatureEntity[] newArray(int size) {
            return new TemperatureEntity[size];
        }
    };
}
