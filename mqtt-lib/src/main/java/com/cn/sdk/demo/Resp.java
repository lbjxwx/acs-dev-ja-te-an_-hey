package com.cn.sdk.demo;

public class Resp<T> {
    public int code;
    public String msg;
    public T data;
}
