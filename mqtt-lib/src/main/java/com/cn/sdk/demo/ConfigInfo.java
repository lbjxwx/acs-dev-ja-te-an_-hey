package com.cn.sdk.demo;


import java.util.List;

public class ConfigInfo {

    // {"msg":"接口调用成功","code":0,"data":{"config":0,"faceMaskType":"2","healthCodeWay":1,"highTemperature":"39","isCtidCheck":1,"isGps":1,"isLignting":1,"isRiskArea":1,"isTemperatureCheck":1,"lowTemperature":"35","rctCardType":["0","48","54","49","55"],"reportType":1}}

    public int faceMaskType;//	口罩检测0 必须戴口罩过闸 1可过闸提醒戴口罩 2 口罩不限制
    public int isCtidCheck;//	是否人证合一检测 0否 1是	必填	Integer
    public int isTemperatureCheck;//	是否体温检测 0否 1是	必填	Integer
    public float highTemperature;//	高温阈值	必填	String
    public float lowTemperature;//	低温阈值	必填 	String
    public int healthCodeWay;//	健康码核验渠道 0 闽政通通道 1 国办通道	必填	Integer

    public int reportType;//	上报类型0 不上报通行记录 1 上报通行记录 2 上报通行记录加抓拍	必填	Integer

    public int isLignting;//	是否开启补光灯 0否 1是	必填	Integer
    public int isGps;//	是否开启实时位置上报 0否 1是	必填	Integer
    public String ctidAuthUrl;//	身份认证接口配置	选填	String
    public String mztUrl;
    public String gbUrl;
    public String verifyAddress;
    public String domainUrl;//	域名更换配置	选填	String
    public int isRiskArea;//	是否打开风险区域开关 0否 1是	选填	Integer		预留
    public int config;//	设备配置场景 0 地铁	选填	Integer预留

    //rctCardType":["0","48","49","55","54"]
    public List<String> rctCardType;//	允许的榕城通卡类 允许终端识别的榕城通卡类型，当前为学生卡、老人卡，后续可通过平台配置下发扩充支持
    //卡类型名称	卡类型对应值
    //学生卡	0300
    //老人卡	0800

    public String getRctCardType() {
        if (rctCardType == null) {
            return "";
        }
        StringBuilder builder = new StringBuilder();
        for(String ty : rctCardType) {
            builder.append(ty);
            builder.append(',');
        }
        if (builder.length() > 0) {
            builder.deleteCharAt(builder.length() -1);
        }
        return builder.toString();
    }

}
