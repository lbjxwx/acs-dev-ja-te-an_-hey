package com.cn.sdk.demo;


import android.content.Context;
import android.os.Build;
import android.os.RemoteException;
import android.os.SystemClock;
import android.util.Log;

import com.alibaba.fastjson.JSONObject;
import com.cn.device.mqtt.sdk.aidl.IHTTPListener;
import com.cn.device.mqtt.sdk.aidl.IMqttServerListener;
import com.cn.sdk.mqtt.MqttManager;
import com.cn.sdk.mqtt.util.MD5Utils;


import java.text.SimpleDateFormat;

public class MqttHelper {

    public static final String TMETHOD_BMM = "0";
    public static final String TMETHOD_GM = "1";
    public static final String TMETHOD_IDCARD = "2";
    public static final String TMETHOD_ICCARD = "3";
    public static final String TMETHOD_CTID = "4";

    private String TAG = "myaaaaa-mqtt";
    private Context context;
    private MqttManager mqttManager;
    private String sn = "test000001";

    public OnMqttListener onMqttListener;

    public void init(Context context, String devSn) {
        this.sn = devSn;
        this.context = context.getApplicationContext();
        SpUtils.init(context);
        System.out.println("myaaaaa-mqtt devsn:  " + devSn);
        initMqttManager();
    }

    /***
     * 断开重连
     */
    public void reStartLineMqtt() {
        if (mqttManager != null) {
            mqttManager.stop();
            mqttManager = null;
        }
        initMqttManager();
    }

    private void initMqttManager() {
        mqttManager = MqttManager.getInstance(context);
        mqttManager.start(new MqttManager.DeviceManagerListener() {
            @Override
            public int serviceEventNotify(int event) {
                System.out.println("aaaa serviceEventNotify " + event);
                if (event == MqttManager.DeviceManagerListener.EVENT_SERVICE_CONNECTED) {
                    Log.e(TAG, "EVENT_SERVICE_CONNECTED");
                    //sendMessage(null);
                    mqttManager.setMqttListener(sn, mListener);
                } else {
                    Log.e(TAG, "EVENT_SERVICE_DISCONNECTED");
                }
                return 0;
            }
        });
    }

    public void setMqttSn(String sn) {
        this.sn = sn;
        mqttManager.setMqttListener(sn, mListener);
    }

    private IMqttServerListener.Stub mListener = new IMqttServerListener.Stub() {
        @Override
        public void messageArrived(String topic, String msg) throws RemoteException {
            Log.e(TAG, "messageArrived" + topic + " " + msg);

            //sendMessage("MQTT接收"+topic+" "+msg);
            if (onMqttListener != null) {
                onMqttListener.onReceiveMsg(topic, msg);
            }
        }

        @Override
        public void onConect(boolean isConnect) throws RemoteException {
            //sendMessage("MQTT连接"+(isConnect?"成功":"失败"));
            if (!isConnect) {
                mqttManager.stop();
                new Thread() {
                    @Override
                    public void run() {
                        SystemClock.sleep(15000);
                        initMqttManager();
                    }
                }.start();
                //unbindMqttServer();
                //判断网络连接成功后再重连。
                //SystemClock.sleep(15000);
                //bindMqttserver();
            }
            if (onMqttListener != null) {
                onMqttListener.onConnect(isConnect);
            }
        }
    };

    public void getDeviceConfig(String version) {
        JSONObject json = new JSONObject();
        json.put("sn", sn);
        json.put("version", version);
        mqttManager.deviceConfig(json.toString(), new IHTTPListener.Stub() {
            @Override
            public void onNotify(int retCode, String retMsg) throws RemoteException {
                Log.e(TAG, "配置设备信息上报" + retCode + " " + (retMsg == null ? "" : retMsg));
                //sendMessage("配置设备信息上报"+retCode+" "+(retMsg==null?"":retMsg));
                if (onMqttListener != null) {
                    onMqttListener.onDevConfig(retCode, retMsg);
                }
            }
        });
    }

    public void savePassRecord(boolean health, String unique, String tempC, boolean highTemp, boolean riskArea, String tmethod, String rctCardType, String verifyAddr, String location) {
        JSONObject json = new JSONObject();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        long transitTime = System.currentTimeMillis();
        Log.e("mqtt", "savePassRecord=000==" + unique);
        if (unique == null) {
            unique = transitTime + "";
        }
        Log.e("mqtt", "savePassRecord=111==" + unique);
        unique = MD5Utils.TripleMd5Encrypt(unique);
        Log.e("mqtt", "savePassRecord=222==" + unique);
        json.put("sn", sn);
        json.put("uniqueCode", unique);
        json.put("healthCodeStatus", health ? "1" : "0");
        json.put("temperature", tempC);
        json.put("riskTemperature", highTemp ? "1" : "0");
        json.put("riskAreas", riskArea ? "1" : "0");
        json.put("transitTime", dateFormat.format(transitTime));
        json.put("verifyAddress", (verifyAddr != null ? verifyAddr : " "));
        json.put("transitMethod", tmethod);
        json.put("rctCardType", rctCardType);
        if (location != null) {
            json.put("position", location);
        }
        SpUtils.setLastReportTime(transitTime);
        System.out.println("savePassRecord----> " + json.toString());
        mqttManager.savePassRecord(json.toString(), new IHTTPListener.Stub() {
            @Override
            public void onNotify(int retCode, String retMsg) throws RemoteException {
                Log.e(TAG, "数据上报" + retCode + " " + (retMsg == null ? "" : retMsg));
                //sendMessage("数据上报"+retCode+" "+(retMsg==null?"":retMsg));
            }
        });
    }

    public void sendDeviceInfo(boolean devEnable, String devMemory, int vercode, String appName, ConfigInfo devConfig) {
        JSONObject json = new JSONObject();
        long syncTime = System.currentTimeMillis();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        json.put("sn", sn);
        json.put("deviceModel", Build.MODEL);
        json.put("deviceMemory", devMemory);
        json.put("systemVersion", "android" + Build.VERSION.RELEASE);
        json.put("versionNumber", vercode);
        json.put("softwareName", appName);
        json.put("reportTime", dateFormat.format(SpUtils.getLastReportTime()));
        json.put("deviceSetting", devConfig);
        json.put("syncTime", dateFormat.format(syncTime));
        json.put("status", devEnable ? 0 : 1);//status	设备当前状态  0 启用 1 禁用

        mqttManager.sendDeviceInfo(json.toString(), new IHTTPListener.Stub() {
            @Override
            public void onNotify(int retCode, String retMsg) throws RemoteException {
                Log.e(TAG, "设备信息上报" + retCode + " " + (retMsg == null ? "" : retMsg));
                //sendMessage("设备信息上报"+retCode+" "+(retMsg==null?"":retMsg));
            }
        });
    }

    public void setOnMqttListener(OnMqttListener listener) {
        this.onMqttListener = listener;
    }

    public void release() {
        if (mqttManager != null) {
            mqttManager.stop();
        }
    }

    /*public void sendInfo(View view){
        mHander.postDelayed(new Runnable() {
            @Override
            public void run() {
                mqttManager.onPublish("DEVICE_INFO/"+sn,sn);
            }
        },500);
    }
    public void savePassRecord(View view){
        mHander.postDelayed(new Runnable() {
            @Override
            public void run() {
                mqttManager.onPublish("DEVICE_CONFIGURATION/"+sn,sn);
            }
        },500);
    }
    public void deviceConfig(View view){
        mHander.postDelayed(new Runnable() {
            @Override
            public void run() {
                mqttManager.onPublish("UPGRADE_SYSTEM/"+sn,sn);
            }
        },500);

    }*/
}
