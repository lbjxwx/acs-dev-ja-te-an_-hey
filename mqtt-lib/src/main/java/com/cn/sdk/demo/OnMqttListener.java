package com.cn.sdk.demo;

public interface OnMqttListener {
    void onConnect(boolean isConnect);
    void onReceiveMsg(String topic,String msg);
    void onDevConfig(int ret, String retMsg);
}
