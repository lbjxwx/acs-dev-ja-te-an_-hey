package com.cn.sdk.demo;

import android.content.Context;
import android.content.SharedPreferences;

public class SpUtils {

    private static SharedPreferences Sp;

    public static void init(Context context) {
        Sp = context.getSharedPreferences("mqtt_cfg", Context.MODE_PRIVATE);
    }

    public static void setLastReportTime(long time) {
        Sp.edit().putLong("recent_report_time", time).apply();
    }

    public static long getLastReportTime() {
        return Sp.getLong("recent_report_time", System.currentTimeMillis());
    }

}
