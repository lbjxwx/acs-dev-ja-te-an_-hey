package com.ys.config;

import android.os.Environment;

public class ImageConfig {

    public static final String LIBRARY_BASE_PATH_INNER = Environment.getExternalStorageDirectory().getPath();
    public static final String CAPTURE_IMAGE_CUT_PATH = LIBRARY_BASE_PATH_INNER + "/capture_cat.jpg";
    public static final int IMAGE_CUT_BACK = 56234;  //裁剪返回的数据

    public static final int PHOTO_CAMERA = 0;
    public static final int PHOTO_LOCAL_CHOOICE = 1;

}
