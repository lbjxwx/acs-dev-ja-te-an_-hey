package com.ys.zip;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import com.ys.entity.FileEntity;
import com.ys.util.FileMatch;

import java.io.File;

/**
 * 图片压缩工具类
 */
public class CompressImageUtil {

    private CompressImageUtil() {
    }

    private static class SingletonHoler {
        public static final CompressImageUtil INSTANCE = new CompressImageUtil();
    }

    public static CompressImageUtil getInstance() {
        return SingletonHoler.INSTANCE;
    }

    public void compressPic(String path, String savePath, CompressImageListener listener) {
        compressPic(path, savePath, 320, 480, listener);
    }

    public void compressPic(String path, String savePath, int widthSize, int heightSize, CompressImageListener listener) {
        try {
            File file = new File(path);
            if (!file.exists()) {
                listener.backErrorDesc("File does not exist");
                return;
            }
            String filePath = file.getPath();
            int fileType = FileMatch.fileMatch(filePath);
            if (fileType != FileEntity.STYLE_FILE_IMAGE) {
                listener.backErrorDesc("File Is Not Image");
                return;
            }
            Bitmap mapChange = BitmapFactory.decodeFile(path);
            float imageWidth = mapChange.getWidth();
            float imageHeight = mapChange.getHeight();
            if (imageWidth < widthSize && imageHeight < heightSize) { //不需要转码直接返回
                listener.backImageSuccess(null, path);
                return;
            }
            while (imageWidth > widthSize || imageHeight > heightSize) {
                imageWidth = imageWidth * 3 / 4;
                imageHeight = imageHeight * 3 / 4;
            }
            Log.e("zip", "图片压缩:==" + imageWidth + " / " + imageHeight);
            CompressImageRunnable runnable = new CompressImageRunnable(file, savePath, imageWidth, imageHeight, listener);
            Thread thread = new Thread(runnable);
            thread.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
