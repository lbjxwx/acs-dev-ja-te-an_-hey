package com.ys.zip;


import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.os.Looper;

import java.io.File;
import java.io.FileOutputStream;

/**
 * 图片压缩
 */
public class CompressImageRunnable implements Runnable {

    File fileCompress;
    CompressImageListener listener;
    float width;
    float height;
    String savePath;

    public CompressImageRunnable(File fileCompress, String savePath, float width, float height, CompressImageListener listener) {
        this.savePath = savePath;
        this.fileCompress = fileCompress;
        this.listener = listener;
        this.width = width;
        this.height = height;
    }

    @Override
    public void run() {
        compressImage();
    }

    public void compressImage() {
        String imagePath = fileCompress.getPath();
        String basePath = imagePath.substring(0, imagePath.lastIndexOf("/") + 1);
        String fileName = fileCompress.getName();
        Bitmap bitmap = bitmapFactory(imagePath);
        if (bitmap == null) {
            backFailed("Image resolve failed");
            return;
        }
        String newPath = savePath;
        if (savePath == null || savePath.isEmpty()) {
            String nameCache = fileName.substring(0, fileName.indexOf(".")) + "_compress.jpg";
            newPath = basePath + nameCache;
        }
        boolean isSave = saveBitmapToSdcard(newPath, bitmap);
        if (!isSave) {
            backFailed("保存图片失败");
            return;
        }
        if (fileCompress.exists()) {
            fileCompress.delete();
        }
        backSuccess(newPath);
    }

    /**
     * 保存方法
     */
    public boolean saveBitmapToSdcard(String path, Bitmap bitmap) {
        try {
            File f = new File(path);
            if (f.exists()) {
                f.delete();
            }
            f.createNewFile();
            FileOutputStream out = new FileOutputStream(f);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 40, out);
            out.flush();
            out.close();
            return true;
        } catch (Exception e) {
            String messageError = e.toString();
            backFailed(messageError);
            e.printStackTrace();
        }
        return false;
    }

    /**
     * 压缩图片使用,采用BitmapFactory.decodeFile。这里是尺寸压缩
     */
    private Bitmap bitmapFactory(String imagePath) {
        Bitmap bm = null;
        try {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true; //获取当前图片的边界大小，而不是将整张图片载入在内存中，避免内存溢出
            BitmapFactory.decodeFile(imagePath, options);
            options.inJustDecodeBounds = false;
            options.inSampleSize = ImageUtil.caculateSampleSize(options, width, height);
            //避免出现内存溢出的情况，进行相应的属性设置。
            options.inPreferredConfig = Bitmap.Config.RGB_565;
            bm = BitmapFactory.decodeFile(imagePath, options); // 解码文件
        } catch (Exception e) {
            listener.backErrorDesc(e.toString());
            e.printStackTrace();
        }
        return bm;
    }

    private void backSuccess(final String imagePath) {
        try {
            if (listener == null) {
                return;
            }
            handler.post(new Runnable() {
                @Override
                public void run() {
                    String oldPath = fileCompress.getPath();
                    listener.backImageSuccess(oldPath, imagePath);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Handler handler = new Handler(Looper.getMainLooper());

    private void backFailed(final String desc) {
        try {
            if (listener == null) {
                return;
            }
            handler.post(new Runnable() {
                @Override
                public void run() {
                    listener.backErrorDesc(desc);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
