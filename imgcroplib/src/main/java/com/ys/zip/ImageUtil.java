package com.ys.zip;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;

import com.ys.config.ImageConfig;

import java.io.File;

/**
 * Created by Administrator on 2019-04-17.
 */

public class ImageUtil {

    Activity context;

    public ImageUtil(Activity context) {
        this.context = context;
    }

    /**
     * 计算出所需要压缩的大小
     *
     * @param options
     * @param reqWidth  我们期望的图片的宽，单位px
     * @param reqHeight 我们期望的图片的高，单位px
     * @return
     */
    public static int caculateSampleSize(BitmapFactory.Options options, float reqWidth, float reqHeight) {
        int sampleSize = 1;
        int picWidth = options.outWidth;
        int picHeight = options.outHeight;
        if (picWidth > reqWidth || picHeight > reqHeight) {
            int halfPicWidth = picWidth / 2;
            int halfPicHeight = picHeight / 2;
            while (halfPicWidth / sampleSize > reqWidth || halfPicHeight / sampleSize > reqHeight) {
                sampleSize *= 2;
            }
        }
        return sampleSize;
    }

    /***
     * 本地图片选择器
     * @param data
     */
    public void cutImageLocal(Intent data, String outPath) {
        Uri imageUri = data.getData();
        String url_out = outPath;
        File file_out = new File(url_out);
        if (file_out.exists()) {
            file_out.delete();
        }
        if (!file_out.getParentFile().exists()) {
            file_out.getParentFile().mkdirs();
        }
        Uri outputUri = Uri.fromFile(new File(url_out));
        cutImage(imageUri, outputUri);
    }

    public void cutImage(Uri imageUri, Uri outputUri) {
        try {
            Intent intent = new Intent("com.android.camera.action.CROP");
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            }
            intent.setDataAndType(imageUri, "image/*");
            intent.putExtra("crop", "true");
            //设置宽高比例
            intent.putExtra("aspectX", 1);
            intent.putExtra("aspectY", 1);
            intent.putExtra("scale", true);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, outputUri);
            intent.putExtra("outputFormat", Bitmap.CompressFormat.JPEG.toString());
            intent.putExtra("noFaceDetection", true);
            context.startActivityForResult(intent, ImageConfig.IMAGE_CUT_BACK);
        } catch (Exception e) {
        }
    }

}
