package com.ys.entity;

public class FileEntity {
    String fileName;
    String filePath;

    /**
     * 图片文件
     */
    public static final int STYLE_FILE_IMAGE = 0;
    /**
     * 视频文件
     */
    public static final int STYLE_FILE_VIDEO = 2;


    //文件夹
    public FileEntity(String fileName, String filePath) {
        this.fileName = fileName;
        this.filePath = filePath;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }


    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    @Override
    public String toString() {
        return "FileEntity{" +
                ", fileName='" + fileName + '\'' +
                ", filePath='" + filePath + '\'' +
                '}';
    }


}
