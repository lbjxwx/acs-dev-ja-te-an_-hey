package com.ys.util;


import com.ys.entity.FileEntity;

/**
 * 判断文件的格式
 */

public class FileMatch {

    public static int fileMatch(String name) {
        name = name.toLowerCase().trim();
        if (name.endsWith("jpg")
                || name.endsWith("jpeg")
                || name.endsWith("png")
                || name.endsWith("bmp")
                || name.endsWith("gif")
                || name.endsWith("webp")) {
            return FileEntity.STYLE_FILE_IMAGE;
        } else if (name.endsWith("mp4")
                || name.endsWith("3gp")
                || name.endsWith("mov")
                || name.endsWith("mpg")
                || name.endsWith("wmv")
                || name.endsWith("flv")
                || name.endsWith("avi")
                || name.endsWith("ts")
                || name.endsWith("rmvb")
                || name.endsWith("mkv")) {
            return FileEntity.STYLE_FILE_VIDEO;
        }
        return -1;
    }
}
