package com.ys.qrcodelib;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Handler;
import android.text.TextUtils;

import com.ys.qrcodelib.zxing.BarcodeFormat;
import com.ys.qrcodelib.zxing.BitMatrix;
import com.ys.qrcodelib.zxing.EncodeHintType;
import com.ys.qrcodelib.zxing.QRCodeWriter;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Hashtable;

/**
 * @ClassName: QRCodeUtil
 * @Description: 二维码工具类
 * @Author Wangnan
 * @Date 2017/2/7
 */
public class QRCodeUtil implements Runnable {

    String content;
    String imagePath;
    private Handler handler = new Handler();
    ErCodeBackListener listener;

    public QRCodeUtil(String content, String imagePath, ErCodeBackListener listener) {
        this.content = content;
        this.imagePath = imagePath;
        this.listener = listener;
    }

    private static int size = 200;


    @Override
    public void run() {
        createQRCodeBitmap();
    }

    private void createQRCodeBitmap() {
        FileOutputStream fos = null;
        if (TextUtils.isEmpty(content)) { // 字符串内容判空
            backErCodeState("content is null", false, imagePath);
            return;
        }
        File fileLocal = new File(imagePath);
        if (fileLocal.exists()) {
            long fileLength = fileLocal.length();
            if (fileLength > 5 * 1024) {
                backErCodeState("File is exists", true, imagePath);
                return;
            }
        }
        try {
            /** 2.设置二维码相关配置,生成BitMatrix(位矩阵)对象 */
            Hashtable<EncodeHintType, String> hints = new Hashtable<>();
            if (!TextUtils.isEmpty("UTF-8")) {
                hints.put(EncodeHintType.CHARACTER_SET, "UTF-8"); // 字符转码格式设置
            }
            if (!TextUtils.isEmpty("H")) {
                hints.put(EncodeHintType.ERROR_CORRECTION, "H"); // 容错级别设置
            }
            if (!TextUtils.isEmpty("4")) {
                hints.put(EncodeHintType.MARGIN, "4"); // 空白边距设置
            }
            BitMatrix bitMatrix = new QRCodeWriter().encode(content, BarcodeFormat.QR_CODE, size, size, hints);
            int[] pixels = new int[size * size];
            for (int y = 0; y < size; y++) {
                for (int x = 0; x < size; x++) {
                    if (bitMatrix.get(x, y)) { // 黑色色块像素设置
                        pixels[y * size + x] = Color.BLACK;
                    } else { // 白色色块像素设置
                        pixels[y * size + x] = Color.WHITE;
                    }
                }
            }
            /** 4.创建Bitmap对象,根据像素数组设置Bitmap每个像素点的颜色值,之后返回Bitmap对象 */
            Bitmap bitmap = Bitmap.createBitmap(size, size, Bitmap.Config.ARGB_8888);
            bitmap.setPixels(pixels, 0, size, 0, 0, size, size);
            if (!fileLocal.exists()) {
                fileLocal.createNewFile();
            }
            fos = new FileOutputStream(imagePath);
            boolean cr = (bitmap != null && bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos));
            backErCodeState("创建成功", true, imagePath);
        } catch (Exception e) {
            backErCodeState(e.toString(), false, "");
            e.printStackTrace();
        } finally {
            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void backErCodeState(final String errorDes, final boolean isCreate, final String path) {
        handler.post(new Runnable() {
            @Override
            public void run() {
                listener.createErCodeState(errorDes, isCreate, path);
            }
        });
    }

    public interface ErCodeBackListener {
        void createErCodeState(String errorDes, boolean isCreate, String path);
    }

}