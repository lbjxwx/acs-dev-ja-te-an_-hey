package com.device;

import android.util.Log;

import com.alibaba.fastjson.JSONObject;


public class PageTool {

    public static final String NETWORK_ERROR_MSG = "网络开小差，请稍后再试！";

    //功能包调用返回码定义
    public static final int GET_POSITION_RETURN = 10001;//定位返回
    public static final int CODE_CHECKDATA_RETURN = 10002;//二维码数据加密返回
    public static final int PAGE_ID_CARD_MSG = 10003;//刷身份证信息返回
    public static final int PAGE_RC_CARD_MSG = 10004;//刷榕城通信息返回
    public static final int PAGE_ERROR_CARD_MSG = 10005;//刷榕城通信息返回
    public static final int PAGE_CARD_START_MSG = 10006;//刷榕城通信息返回
    public static final int PAGE_CARD_LEAVE_MSG = 10007;//刷榕城通信息返回

    public static final int MSG_open_door = 10011;
    public static final int NEXT_VERIFY = 10012;//继续测试
    public static final int DEVICE_ERROR = 10013;//继续测试
    public static final int OPEN_ERROR = 10015;//继续测试

    //服务器通讯包
    public static final int MESSAGE_RETURN = 1021;//获取身份信息接口返回
    public static final int VERIFY_RETURN = 1022;//校验接口返回
    public static final int REPORT_RETURN = 1023;//上报接口返回


    //功能包返回字段
    public static final String DEVCE_RETURN_ERROR_MSG = "ErrorMsg";//错误信息
    public static final String ERROR_MSG_NETWORK = "networkError";

    //刷卡部分返回
    public static final String DEVCE_RETURN_NAME = "Name";//刷身份证-姓名
    public static final String DEVCE_RETURN_ID = "IDNum";//刷身份证-身份证号
    public static final String DEVCE_RC_ID = "RCid";//刷榕城通-卡号
    public static final String DEVCE_RC_TYPE = "RCtype";//刷榕城通-卡类型
    public static final String IDCARD_NAME = "identityName";//message返回
    public static final String IDCARD_ID = "identityId";//message返回


    //设置sp文件名
    public static final String CONFIG_NAME = "Q738Name";
    public static final String CONFIG_SERVER_ADDRESS = "Q738_server_address";

    public static String ReturnDataCheck(String strMsg)//检查子流程返回数据，是出错数据则恢复界面，否则返回true
    {
        Log.i("ReturnDataCheck", strMsg);
        try {
            JSONObject jsonObjectSend = new JSONObject().parseObject(strMsg);//结果解析
            Log.i("ReturnDataCheck", DEVCE_RETURN_ERROR_MSG + ": " + jsonObjectSend.getString(DEVCE_RETURN_ERROR_MSG));
            String strErrorMsg = jsonObjectSend.getString(DEVCE_RETURN_ERROR_MSG);
            if(strErrorMsg != null)//错误信息
            {
                return strErrorMsg;
            }
        } catch (Exception e) {
            return strMsg;
        }

        return "";
    }
}

