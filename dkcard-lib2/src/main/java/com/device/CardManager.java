package com.device;

import static com.card.DKCloudID.ClientDispatcher.SAM_V_FRAME_START_CODE;
import static com.card.DKCloudID.ClientDispatcher.SAM_V_INIT_COM;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.card.CardInfo;
import com.card.DKCloudID.IDCard;
import com.card.DKCloudID.IDCardData;
import com.card.Exception.CardNoResponseException;
import com.card.Exception.DKCloudIDException;
import com.card.Exception.DeviceNoResponseException;
import com.card.OnCardListener;
import com.card.Tool.StringTool;
import com.card.uartnfc.SamVIdCard;
import com.card.uartnfc.SerialManager;
import com.ys.http.network.HttpUtils;

import java.util.Arrays;

public class CardManager {
    String TAG = "CardManager";

    private volatile static CardManager mInstance;
    private static final String network_error = "网络异常数据解密失败";

    SerialManager serialManager;
    IDCard idCard = null;

    public static final int NUMBER_OF_REPARSING = 5;              /*解析失败时，重新解析的次数*/
    public static int iRCStep = -1;
    byte[] initData;

    private final Handler mHandler;
    private OnCardListener cardListener;

    CardManager() {
        serialManager = new SerialManager();
        mHandler = new Handler(Looper.getMainLooper(), msg -> {
            if (cardListener == null) {
                return true;
            }
            switch (msg.what) {
                case PageTool.PAGE_ID_CARD_MSG://二代证读取返回结果
                case PageTool.PAGE_RC_CARD_MSG://公交卡读取返回结果
                    CardInfo cardInfo = (CardInfo) msg.obj;
                    cardListener.onCardInfo(cardInfo);
                    break;
                case PageTool.PAGE_CARD_START_MSG:
                    cardListener.onStart();
                    break;
                case PageTool.PAGE_CARD_LEAVE_MSG:
                    cardListener.onCardLeave();
                    break;
                case PageTool.OPEN_ERROR:
                    cardListener.onInitError(msg.obj.toString());
                    break;
                case PageTool.DEVICE_ERROR:

                    break;
                case PageTool.PAGE_ERROR_CARD_MSG:
                    //读卡失败或者错误
                    cardListener.onCardFailure(msg.obj.toString());
                    break;
            }
            return true;
        });
    }

    public static CardManager getInstance() {
        if (mInstance == null) {
            synchronized (CardManager.class) {
                if (mInstance == null) {
                    mInstance = new CardManager();
                }
            }
        }
        return mInstance;
    }

    private void sendHander(int iMsg, CardInfo cardInfo) {
        mHandler.obtainMessage(iMsg, cardInfo).sendToTarget();
    }

    //是否支持刷卡器
    private boolean isOpenCarCardEnable = false;
    public void initCard(boolean carCardEnable,String serialPort, OnCardListener listener) {
        cardListener = listener;
        isOpenCarCardEnable = carCardEnable;
        new Thread(){
            @Override
            public void run() {
                try {
                    if (serialManager.open(serialPort, "115200")) {
                        byte[] bytes = serialManager.sendWithReturn(new byte[]{(byte) 0xAA, 0x01, (byte) 0xB0}, 300);
                        if (bytes[0] == (byte) 0xAA) {
                            Log.e("打开设备", "打开串口成功");

                            try {
                                serialManager.sendWithReturn(StringTool.hexStringToBytes("AA0495FF1476"));  //配置NFC模块
                            } catch (DeviceNoResponseException e1) {
                                e1.printStackTrace();
                            }
                        }
                        return;
                    }
                    mHandler.obtainMessage(PageTool.OPEN_ERROR, String.format("串口%s打开失败", serialPort)).sendToTarget();
                } catch (DeviceNoResponseException e) {
                    e.printStackTrace();
                    Log.e("打开设备", "失败");
                    serialManager.close();
                    mHandler.obtainMessage(PageTool.OPEN_ERROR, String.format("卡设备无响应: %s", e.getMessage())).sendToTarget();
                }
            }
        }.start();

        //设置串口数据接收监听
        serialManager.setOnReceiveDataListener(new SerialManager.onReceiveDataListener() {
            @Override
            public void OnReceiverData(String portNumberString, byte[] dataBytes) {
                final String portNumber = portNumberString;
                final byte[] data = dataBytes;

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        Log.d(TAG, portNumber + "接收(" + data.length + ")：" + StringTool.byteHexToSting(data) + "\r\n");

                        if (data[0] == (byte) 0xAA) {
                            //A卡
                            if (data[2] == (byte) 0xEA) {
                                Log.e(TAG, "卡移走");
                                mHandler.obtainMessage(PageTool.PAGE_CARD_LEAVE_MSG, "卡已移走").sendToTarget();
                                return;//卡移走
                            }

                            if (data[2] == (byte) 0x01) {
                                //寻卡成功
                                iRCStep = 0;
                                Log.e(TAG, "公交卡寻卡成功，开始激活");
                                mHandler.sendEmptyMessage(PageTool.PAGE_CARD_START_MSG);
                                serialManager.send(StringTool.hexStringToBytes("AA0115"));//激活
                            } else if (iRCStep == 0 && data[2] == (byte) 0xFE) {
                                //激活成功
                                iRCStep = 1;
                                Log.e(TAG, "激活成功，开始设置1");
                                serialManager.send(StringTool.hexStringToBytes("AA151700A404000E315041592E5359532E444446303100"));//设置当前文件后续命令可以通过那个逻辑信道隐式地引用该当前文件
                            } else if (iRCStep == 1 && data[2] == (byte) 0x17 && StringTool.byteHexToSting(data).endsWith("9000")) {
                                //设置1成功
                                iRCStep = 2;
                                Log.e(TAG, "设置1成功，开始设置2");
                                serialManager.send(StringTool.hexStringToBytes("AA101700A4040009A0000000038698070100"));//设置当前文件后续命令可以通过那个逻辑信道隐式地引用该当前文件
                            } else if (iRCStep == 2 && data[2] == (byte) 0x17 && StringTool.byteHexToSting(data).endsWith("9000")) {
                                //设置2成功
                                iRCStep = 3;
                                Log.e(TAG, "设置2成功，开始读卡");
                                serialManager.send(StringTool.hexStringToBytes("AA061700B0950000"));//设置当前文件后续命令可以通过那个逻辑信道隐式地引用该当前文件
                            } else if (iRCStep == 3 && data[2] == (byte) 0x17 && StringTool.byteHexToSting(data).endsWith("9000") && data.length == 53) {
                                if (!isOpenCarCardEnable){
                                    return;
                                }
                                try {
                                    //读卡成功
                                    iRCStep = -1;
                                    Log.e(TAG, "读卡成功！！！");
                                    String strRCData = StringTool.byteHexToSting(data);
                                    //AA3317544B35000000FFFF0101350035000001005527252017022120991231FFFF0800002199123101000000000000000000009000
                                    CardInfo cardInfo = new CardInfo();
                                    cardInfo.type = CardInfo.TYPE_BUS;
                                    cardInfo.cardNo = strRCData.substring(30, 46);
                                    cardInfo.rcType = strRCData.substring(66, 70);
                                    /*JSONObject jsonObject = new JSONObject();
                                    jsonObject.put(PageTool.DEVCE_RC_ID, strRCData.substring(30, 46));
                                    jsonObject.put(PageTool.DEVCE_RC_TYPE, strRCData.substring(66, 70));
                                    Log.e(TAG,jsonObject.toString());*/
                                    mHandler.obtainMessage(PageTool.PAGE_RC_CARD_MSG, cardInfo).sendToTarget();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    mHandler.obtainMessage(PageTool.PAGE_ERROR_CARD_MSG, "读卡失败,请重新刷卡").sendToTarget();
                                    //MainActivity.toast(activity,"请重新刷卡");
                                }
                            } else {
                                //出错
                                iRCStep = -1;
                                mHandler.obtainMessage(PageTool.PAGE_ERROR_CARD_MSG, "读卡失败,请重新刷卡").sendToTarget();
                                //MainActivity.toast(activity,"请重新刷卡");
                            }
                        } else if ((data.length >= 3) && (data[0] == SAM_V_FRAME_START_CODE) && (data[3] == SAM_V_INIT_COM)) {
                            //校验数据
                            try {
                                SamVIdCard.verify(data);
                            } catch (CardNoResponseException e) {
                                e.printStackTrace();

                                Log.e("读卡", "正在重新解析..");
                                serialManager.send(StringTool.hexStringToBytes("AA0118"));
                                return;
                            }

                            Log.d(TAG, "开始解析");
                            //MainActivity.toast(activity,"正在读卡，请勿移动身份证!" );

                            mHandler.sendEmptyMessage(PageTool.PAGE_CARD_START_MSG);
                            initData = Arrays.copyOfRange(data, 4, data.length - 1);
                            SamVIdCard samVIdCard = new SamVIdCard(serialManager, initData);

                            //关闭上一次的云解码
                            idCard = new IDCard(samVIdCard);

                            int cnt = 0;
                            do {
                                try {
                                    /**
                                     * 获取身份证数据，带进度回调，如果不需要进度回调可以去掉进度回调参数或者传入null
                                     * 注意：此方法为同步阻塞方式，需要一定时间才能返回身份证数据，期间身份证不能离开读卡器！
                                     */
                                    IDCardData idCardData = idCard.getIDCardData(new IDCard.onReceiveScheduleListener() {
                                        @Override
                                        public void onReceiveSchedule(int rate) {  //读取进度回调
                                            //MainActivity.toast(activity,"正在读取身份证信息,请不要移动身份证"+rate+"");
                                        }
                                    });

                                    /**
                                     * 读取成功，显示身份证数据，在此提示用户读取成功或者打开蜂鸣器提示
                                     */
                                    /*JSONObject jsonObject = new JSONObject();
                                    jsonObject.put(PageTool.DEVCE_RETURN_NAME, idCardData.Name.replaceAll("\\s*", ""));
                                    jsonObject.put(PageTool.DEVCE_RETURN_ID, idCardData.IDCardNo);
                                    sendHander(PageTool.PAGE_ID_CARD_MSG, jsonObject.toString());*/

                                    CardInfo cardInfo = new CardInfo();
                                    cardInfo.type = CardInfo.TYPE_IDCARD;
                                    cardInfo.sex = idCardData.Sex;
                                    cardInfo.name = idCardData.Name;
                                    cardInfo.cardNo = idCardData.IDCardNo;
                                    cardInfo.photo = idCardData.PhotoBmp;
                                    sendHander(PageTool.PAGE_ID_CARD_MSG, cardInfo);
                                    //返回读取成功
                                    return;
                                } catch (DKCloudIDException e) {   //服务器返回异常，重复5次解析
                                    e.printStackTrace();
                                    //显示错误信息 读卡失败,服务器返回异常
                                    mHandler.obtainMessage(PageTool.PAGE_ERROR_CARD_MSG, network_error).sendToTarget();
                                } catch (CardNoResponseException e) {    //卡片读取异常，直接退出，需要重新读卡
                                    e.printStackTrace();
                                    //显示错误信息 卡片读取异常
                                    Log.e("出错", e.getMessage());

                                    //返回读取失败
                                    //MainActivity.toast(activity,"请不要移动身份证");
                                    mHandler.obtainMessage(PageTool.PAGE_ERROR_CARD_MSG, network_error).sendToTarget();
                                    serialManager.send(StringTool.hexStringToBytes("AA0118"));
                                    return;
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    mHandler.obtainMessage(PageTool.PAGE_ERROR_CARD_MSG, network_error).sendToTarget();
                                }
                            } while (cnt++ < 5);  //如果服务器返回异常则重复读5次直到成功

                        }
                    }
                }).start();
            }
        });
    }

    public void release() {
        if (serialManager != null) {
            serialManager.close();
        }
    }
}
