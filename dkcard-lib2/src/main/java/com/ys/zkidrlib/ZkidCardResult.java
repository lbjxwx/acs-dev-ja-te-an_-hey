package com.ys.zkidrlib;

import android.graphics.Bitmap;

// Created by kermitye on 2021/1/21 14:42
public class ZkidCardResult {

    public ZkidCardResult(int type, String idCardNo, String name, String birth, String address, Bitmap photo) {
        this.type = type;
        this.idCardNo = idCardNo;
        this.name = name;
        this.birth = birth;
        this.address = address;
        this.photo = photo;
    }

    public int type;
    public String idCardNo;
    public String name;
    public String birth;
    public String address;
    public Bitmap photo;

    @Override
    public String toString() {
        return "ZkidCardResult{" +
                "type=" + type +
                ", idCardNo='" + idCardNo + '\'' +
                ", name='" + name + '\'' +
                ", birth='" + birth + '\'' +
                ", address='" + address + '\'' +
                ", phot=" + photo +
                '}';
    }
}
