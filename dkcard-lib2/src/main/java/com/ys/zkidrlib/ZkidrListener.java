package com.ys.zkidrlib;

// Created by kermitye on 2020/8/9 9:41
public interface ZkidrListener {

    void onIDCardCallBack(ZkidCardResult result);
}
