package com.ys.zkidrlib;//package com.ys.zkidrlib;
//
//import android.content.Context;
//import android.graphics.Bitmap;
//import android.os.Handler;
//import android.os.Looper;
//import android.util.Log;
//
//import com.zkteco.android.IDReader.IDPhotoHelper;
//import com.zkteco.android.IDReader.WLTService;
//import com.zkteco.android.biometric.core.device.ParameterHelper;
//import com.zkteco.android.biometric.core.device.TransportType;
//import com.zkteco.android.biometric.core.utils.LogHelper;
//import com.zkteco.android.biometric.module.idcard.IDCardReader;
//import com.zkteco.android.biometric.module.idcard.IDCardReaderFactory;
//import com.zkteco.android.biometric.module.idcard.exception.IDCardReaderException;
//import com.zkteco.android.biometric.module.idcard.meta.IDCardInfo;
//import com.zkteco.android.biometric.module.idcard.meta.IDPRPCardInfo;
//
//import java.util.HashMap;
//import java.util.Map;
//import java.util.concurrent.CountDownLatch;
//import java.util.concurrent.TimeUnit;
//
//public class ZkidrManagerBack {
//
//    public static final String PATH_TTYS1 = "/dev/ttyS1";
//    public static final String PATH_TTYS2 = "/dev/ttyS2";
//    public static final String PATH_TTYS3 = "/dev/ttyS3";
//    public static final String PATH_TTYS4 = "/dev/ttyS4";
//
//
//    private final int idBaudrate = 115200;
//    private IDCardReader idCardReader = null;
//    private Context context;
//    private Handler mHandler = new Handler(Looper.getMainLooper());
//    private CountDownLatch countdownLatch = null;
//    private boolean bStoped = false;
//    private ZkidrListener listener;
//
//    private ZkidrManagerBack() {
//    }
//
//    private static class SingletonHolder {
//        public static final ZkidrManagerBack INSTANCE = new ZkidrManagerBack();
//    }
//
//    public static ZkidrManagerBack getInstance() {
//        return SingletonHolder.INSTANCE;
//    }
//
//    public void init(Context context, String path) {
//        this.context = context;
//        LogHelper.setLevel(Log.VERBOSE);
//        if (idCardReader == null) {
//            Map idrparams = new HashMap();
//            idrparams.put(ParameterHelper.PARAM_SERIAL_SERIALNAME, path);
//            idrparams.put(ParameterHelper.PARAM_SERIAL_BAUDRATE, idBaudrate);
//            idCardReader = IDCardReaderFactory.createIDCardReader(context, TransportType.SERIALPORT, idrparams);
//            mHandler.postDelayed(new Runnable() {
//                @Override
//                public void run() {
//                    start();
//                }
//            }, 500);
//        }
//    }
//
//    public void setZkidrListener(ZkidrListener listener) {
//        this.listener = listener;
//    }
//
//    private void start() {
//        if (idCardReader == null) {
//            return;
//        }
//        log("连接设备成功");
//        try {
//            idCardReader.open(0);
//            bStoped = false;
//            countdownLatch = new CountDownLatch(1);
//            new Thread(new Runnable() {
//                public void run() {
//                    while (!bStoped) {
//                        try {
//                            Thread.sleep(500);
//                        } catch (InterruptedException e) {
//                            e.printStackTrace();
//                        }
//                        boolean ret = false;
//                        final long nTickstart = System.currentTimeMillis();
//                        try {
//                            idCardReader.findCard(0);
//                            idCardReader.selectCard(0);
//                        } catch (IDCardReaderException e) {
//                            //LogHelper.e("errcode:" + e.getErrorCode() + ",internalerrorcode:" + e.getInternalErrorCode());
//                            //continue;
//                        }
//                        try {
//                            Thread.sleep(50);
//                        } catch (InterruptedException e) {
//                            e.printStackTrace();
//                        }
//                        int retType = 0;
//                        try {
//                            retType = idCardReader.readCardEx(0, 0);
//                        } catch (IDCardReaderException e) {
//                            log("读卡失败，错误信息：" + e.getMessage());
//                        }
//                        if (retType == 1 || retType == 2 || retType == 3) {
//                            final long nTickUsed = (System.currentTimeMillis() - nTickstart);
//                            final int final_retType = retType;
//                            log("读卡成功：耗时：" + nTickUsed + "毫秒");
//                            if (final_retType == 1) {
//                                final IDCardInfo idCardInfo = idCardReader.getLastIDCardInfo();
//                                //姓名
//                                String strName = idCardInfo.getName();
//                                //民族
//                                String strNation = idCardInfo.getNation();
//                                //出生日期
//                                String strBorn = idCardInfo.getBirth();
//                                //住址
//                                String strAddr = idCardInfo.getAddress();
//                                //身份证号
//                                String strID = idCardInfo.getId();
//                                //有效期限
//                                String strEffext = idCardInfo.getValidityTime();
//                                //签发机关
//                                String strIssueAt = idCardInfo.getDepart();
//                                log("读取耗时：" + nTickUsed + "毫秒, 卡类型：居民身份证,姓名：" + strName +
//                                        "，民族：" + strNation + "，住址：" + strAddr + ",身份证号：" + strID);
//                                Bitmap photo = null;
//                                if (idCardInfo.getPhotolength() > 0) {
//                                    byte[] buf = new byte[WLTService.imgLength];
//                                    if (1 == WLTService.wlt2Bmp(idCardInfo.getPhoto(), buf)) {
//                                        photo = IDPhotoHelper.Bgr2Bitmap(buf);
//                                    }
//                                }
//                                if (listener != null)
//                                    listener.onIDCardCallBack(new ZkidCardResult(final_retType, strID, strName, strBorn, strAddr, photo));
//                            } else if (final_retType == 2) {
//                                final IDPRPCardInfo idprpCardInfo = idCardReader.getLastPRPIDCardInfo();
//                                //中文名
//                                String strCnName = idprpCardInfo.getCnName();
//                                //英文名
//                                String strEnName = idprpCardInfo.getEnName();
//                                //国家/国家地区代码
//                                String strCountry = idprpCardInfo.getCountry() + "/" + idprpCardInfo.getCountryCode();//国家/国家地区代码
//                                //出生日期
//                                String strBorn = idprpCardInfo.getBirth();
//                                //身份证号
//                                String strID = idprpCardInfo.getId();
//                                //有效期限
//                                String strEffext = idprpCardInfo.getValidityTime();
//                                //签发机关
//                                String strIssueAt = "公安部";
//                                log("读取耗时：" + nTickUsed + "毫秒, 卡类型：外国人永居证,中文名：" + strCnName + ",英文名：" +
//                                        strEnName + "，国家：" + strCountry + ",证件号：" + strID);
//                                Bitmap photo = null;
//                                if (idprpCardInfo.getPhotolength() > 0) {
//                                    byte[] buf = new byte[WLTService.imgLength];
//                                    if (1 == WLTService.wlt2Bmp(idprpCardInfo.getPhoto(), buf)) {
//                                        photo = IDPhotoHelper.Bgr2Bitmap(buf);
//                                    }
//                                }
//                                if (listener != null)
//                                    listener.onIDCardCallBack(new ZkidCardResult(final_retType, strID, strCnName, strBorn, "", photo));
//                            } else {
//                                final IDCardInfo idCardInfo = idCardReader.getLastIDCardInfo();
//                                //姓名
//                                String strName = idCardInfo.getName();
//                                //民族,港澳台不支持该项
//                                String strNation = "";
//                                //出生日期
//                                String strBorn = idCardInfo.getBirth();
//                                //住址
//                                String strAddr = idCardInfo.getAddress();
//                                //身份证号
//                                String strID = idCardInfo.getId();
//                                //有效期限
//                                String strEffext = idCardInfo.getValidityTime();
//                                //签发机关
//                                String strIssueAt = idCardInfo.getDepart();
//                                //通行证号
//                                String strPassNum = idCardInfo.getPassNum();
//                                //签证次数
//                                int visaTimes = idCardInfo.getVisaTimes();
//                                log("读取耗时：" + nTickUsed + "毫秒, 卡类型：港澳台居住证,姓名：" + strName +
//                                        "，住址：" + strAddr + ",身份证号：" + strID + "，通行证号码：" + strPassNum +
//                                        ",签证次数：" + visaTimes);
//                                Bitmap photo = null;
//                                if (idCardInfo.getPhotolength() > 0) {
//                                    byte[] buf = new byte[WLTService.imgLength];
//                                    if (1 == WLTService.wlt2Bmp(idCardInfo.getPhoto(), buf)) {
//                                        photo = IDPhotoHelper.Bgr2Bitmap(buf);
//                                    }
//                                }
//                                if (listener != null)
//                                    listener.onIDCardCallBack(new ZkidCardResult(final_retType, strID, strName, strBorn, strAddr, photo));
//                            }
//                        }
//                    }
//                    countdownLatch.countDown();
//                }
//            }).start();
//
//        } catch (IDCardReaderException e) {
//            e.printStackTrace();
//        }
//    }
//
//    public void destory() {
//        bStoped = true;
//        if (null != countdownLatch) {
//            try {
//                countdownLatch.await(2, TimeUnit.SECONDS);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//        }
//        try {
//            idCardReader.close(0);
//        } catch (IDCardReaderException e) {
//            e.printStackTrace();
//        }
//        if (idCardReader != null)
//            IDCardReaderFactory.destroy(idCardReader);
//    }
//
//
//    private void log(String msg) {
//        Log.e("ZkidrManager", msg);
//    }
//}
