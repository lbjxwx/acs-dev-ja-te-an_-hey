package com.card.api;

import com.card.Const;

import java.util.Map;

import io.reactivex.rxjava3.core.Observable;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;
import retrofit2.http.Body;
import retrofit2.http.HeaderMap;
import retrofit2.http.POST;
import retrofit2.http.Url;

public interface ServerApi {

    String idCardGetHealthInfoUrl = Const.serverUrl + Const.healthByIdAndName;
    String qrCodeGetHealthInfoUrl = Const.serverUrl + Const.JkmQrDataUrl;

    @POST
    Observable<ResponseBody> post(@Url String url, @HeaderMap Map<String, String> headers, @Body RequestBody body);
}
