package com.card.model;

import java.util.Date;
import java.util.List;

public class responseHealth {
    /**
     * healthMsg : {"head":{"message":"接口调用成功","status":"0"},"data":{"yqReports":[],"yqWarn":{"warnType":"无"},"healthyReport":[{"date":"2021-08-18","reason":"无","isVaccinated":"00","healthyId":"144-7942a9fb1ec6008ec5cd1a788160780195ccee5874e26602c21f922a43af754e","healthyStaus":"00","cdTimeVersion":3,"dataSource":"广东省"},{"date":"2021-05-24","healthyId":"111-7942a9fb1ec6008ec5cd1a788160780195ccee5874e26602c21f922a43af754e","healthyStaus":"00","cdTimeVersion":32,"dataSource":"北京市"},{"date":"2020-12-15","healthyId":"132-7942a9fb1ec6008ec5cd1a788160780195ccee5874e26602c21f922a43af754e","healthyStaus":"00","cdTimeVersion":4,"dataSource":"江苏省"},{"date":"2021-07-22","reason":"无","isVaccinated":"00","healthyId":"145-7942a9fb1ec6008ec5cd1a788160780195ccee5874e26602c21f922a43af754e","healthyStaus":"00","cdTimeVersion":1,"dataSource":"广西壮族自治区"},{"date":"2021-08-17","healthyId":"135-7942a9fb1ec6008ec5cd1a788160780195ccee5874e26602c21f922a43af754e","healthyStaus":"00","cdTimeVersion":95,"dataSource":"福建省"}],"hsjc":{"hsjcsj":"2021-08-11","hsjcjgmc":"深圳市龙华区人民医院","hsjcjg":"阴性"},"person":{"desensitizeCardId":"**************0739","desensitizeName":"林**"},"xcgj":[{"date":"2021-08-11 17:19:54","stopoverCountyName":"","pathStatus":"1","fxdj":"1","stopoverCity":"350100","id":"2021-08-117942a9fb1ec6008ec5cd1a788160780195ccee5874e26602c21f922a43af754e","stopoverCityName":"福建省福州市","stopoverCounty":""}]}}
     */

    private HealthMsgBean healthMsg;

    public HealthMsgBean getHealthMsg() {
        return healthMsg;
    }

    public void setHealthMsg(HealthMsgBean healthMsg) {
        this.healthMsg = healthMsg;
    }

    public static class HealthMsgBean {
        /**
         * head : {"message":"接口调用成功","status":"0"}
         * data : {"yqReports":[],"yqWarn":{"warnType":"无"},"healthyReport":[{"date":"2021-08-18","reason":"无","isVaccinated":"00","healthyId":"144-7942a9fb1ec6008ec5cd1a788160780195ccee5874e26602c21f922a43af754e","healthyStaus":"00","cdTimeVersion":3,"dataSource":"广东省"},{"date":"2021-05-24","healthyId":"111-7942a9fb1ec6008ec5cd1a788160780195ccee5874e26602c21f922a43af754e","healthyStaus":"00","cdTimeVersion":32,"dataSource":"北京市"},{"date":"2020-12-15","healthyId":"132-7942a9fb1ec6008ec5cd1a788160780195ccee5874e26602c21f922a43af754e","healthyStaus":"00","cdTimeVersion":4,"dataSource":"江苏省"},{"date":"2021-07-22","reason":"无","isVaccinated":"00","healthyId":"145-7942a9fb1ec6008ec5cd1a788160780195ccee5874e26602c21f922a43af754e","healthyStaus":"00","cdTimeVersion":1,"dataSource":"广西壮族自治区"},{"date":"2021-08-17","healthyId":"135-7942a9fb1ec6008ec5cd1a788160780195ccee5874e26602c21f922a43af754e","healthyStaus":"00","cdTimeVersion":95,"dataSource":"福建省"}],"hsjc":{"hsjcsj":"2021-08-11","hsjcjgmc":"深圳市龙华区人民医院","hsjcjg":"阴性"},"person":{"desensitizeCardId":"**************0739","desensitizeName":"林**"},"xcgj":[{"date":"2021-08-11 17:19:54","stopoverCountyName":"","pathStatus":"1","fxdj":"1","stopoverCity":"350100","id":"2021-08-117942a9fb1ec6008ec5cd1a788160780195ccee5874e26602c21f922a43af754e","stopoverCityName":"福建省福州市","stopoverCounty":""}]}
         */

        private HeadBean head;
        private DataBean data;

        public HeadBean getHead() {
            return head;
        }

        public void setHead(HeadBean head) {
            this.head = head;
        }

        public DataBean getData() {
            return data;
        }

        public void setData(DataBean data) {
            this.data = data;
        }

        public static class HeadBean {
            /**
             * message : 接口调用成功
             * status : 0
             */

            private String message;
            private String status;

            public String getMessage() {
                return message;
            }

            public void setMessage(String message) {
                this.message = message;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }
        }

        public static class DataBean {
            /**
             * yqReports : []
             * yqWarn : {"warnType":"无"}
             * healthyReport : [{"date":"2021-08-18","reason":"无","isVaccinated":"00","healthyId":"144-7942a9fb1ec6008ec5cd1a788160780195ccee5874e26602c21f922a43af754e","healthyStaus":"00","cdTimeVersion":3,"dataSource":"广东省"},{"date":"2021-05-24","healthyId":"111-7942a9fb1ec6008ec5cd1a788160780195ccee5874e26602c21f922a43af754e","healthyStaus":"00","cdTimeVersion":32,"dataSource":"北京市"},{"date":"2020-12-15","healthyId":"132-7942a9fb1ec6008ec5cd1a788160780195ccee5874e26602c21f922a43af754e","healthyStaus":"00","cdTimeVersion":4,"dataSource":"江苏省"},{"date":"2021-07-22","reason":"无","isVaccinated":"00","healthyId":"145-7942a9fb1ec6008ec5cd1a788160780195ccee5874e26602c21f922a43af754e","healthyStaus":"00","cdTimeVersion":1,"dataSource":"广西壮族自治区"},{"date":"2021-08-17","healthyId":"135-7942a9fb1ec6008ec5cd1a788160780195ccee5874e26602c21f922a43af754e","healthyStaus":"00","cdTimeVersion":95,"dataSource":"福建省"}]
             * hsjc : {"hsjcsj":"2021-08-11","hsjcjgmc":"深圳市龙华区人民医院","hsjcjg":"阴性"}
             * person : {"desensitizeCardId":"**************0739","desensitizeName":"林**"}
             * xcgj : [{"date":"2021-08-11 17:19:54","stopoverCountyName":"","pathStatus":"1","fxdj":"1","stopoverCity":"350100","id":"2021-08-117942a9fb1ec6008ec5cd1a788160780195ccee5874e26602c21f922a43af754e","stopoverCityName":"福建省福州市","stopoverCounty":""}]
             */

            private YqWarnBean yqWarn;
            private HsjcBean hsjc;
            private PersonBean person;
            private List<?> yqReports;
            private List<HealthyReportBean> healthyReport;
            private List<XcgjBean> xcgj;

            public YqWarnBean getYqWarn() {
                return yqWarn;
            }

            public void setYqWarn(YqWarnBean yqWarn) {
                this.yqWarn = yqWarn;
            }

            public HsjcBean getHsjc() {
                return hsjc;
            }

            public void setHsjc(HsjcBean hsjc) {
                this.hsjc = hsjc;
            }

            public PersonBean getPerson() {
                return person;
            }

            public void setPerson(PersonBean person) {
                this.person = person;
            }

            public List<?> getYqReports() {
                return yqReports;
            }

            public void setYqReports(List<?> yqReports) {
                this.yqReports = yqReports;
            }

            public List<HealthyReportBean> getHealthyReport() {
                return healthyReport;
            }

            public void setHealthyReport(List<HealthyReportBean> healthyReport) {
                this.healthyReport = healthyReport;
            }

            public List<XcgjBean> getXcgj() {
                return xcgj;
            }

            public void setXcgj(List<XcgjBean> xcgj) {
                this.xcgj = xcgj;
            }

            public static class YqWarnBean {
                /**
                 * warnType : 无
                 */

                private String warnType;

                public String getWarnType() {
                    return warnType;
                }

                public void setWarnType(String warnType) {
                    this.warnType = warnType;
                }
            }

            public static class HsjcBean {
                /**
                 * hsjcsj : 2021-08-11
                 * hsjcjgmc : 深圳市龙华区人民医院
                 * hsjcjg : 阴性
                 */

                private String hsjcsj;
                private String hsjcjgmc;
                private String hsjcjg;

                public String getHsjcsj() {
                    return hsjcsj;
                }

                public void setHsjcsj(String hsjcsj) {
                    this.hsjcsj = hsjcsj;
                }

                public String getHsjcjgmc() {
                    return hsjcjgmc;
                }

                public void setHsjcjgmc(String hsjcjgmc) {
                    this.hsjcjgmc = hsjcjgmc;
                }

                public String getHsjcjg() {
                    return hsjcjg;
                }

                public void setHsjcjg(String hsjcjg) {
                    this.hsjcjg = hsjcjg;
                }
            }

            public static class PersonBean {
                /**
                 * desensitizeCardId : **************0739
                 * desensitizeName : 林**
                 */

                private String desensitizeCardId;
                private String desensitizeName;

                public String getDesensitizeCardId() {
                    return desensitizeCardId;
                }

                public void setDesensitizeCardId(String desensitizeCardId) {
                    this.desensitizeCardId = desensitizeCardId;
                }

                public String getDesensitizeName() {
                    return desensitizeName;
                }

                public void setDesensitizeName(String desensitizeName) {
                    this.desensitizeName = desensitizeName;
                }
            }

            public static class HealthyReportBean {
                /**
                 * date : 2021-08-18
                 * reason : 无
                 * isVaccinated : 00
                 * healthyId : 144-7942a9fb1ec6008ec5cd1a788160780195ccee5874e26602c21f922a43af754e
                 * healthyStaus : 00
                 * cdTimeVersion : 3
                 * dataSource : 广东省
                 */

                private Date date;
                private String reason;
                private String isVaccinated;
                private String healthyId;
                private String healthyStaus;
                private int cdTimeVersion;
                private String dataSource;

                public Date getDate() {
                    return date;
                }

                public void setDate(Date date) {
                    this.date = date;
                }

                public String getReason() {
                    return reason;
                }

                public void setReason(String reason) {
                    this.reason = reason;
                }

                public String getIsVaccinated() {
                    return isVaccinated;
                }

                public void setIsVaccinated(String isVaccinated) {
                    this.isVaccinated = isVaccinated;
                }

                public String getHealthyId() {
                    return healthyId;
                }

                public void setHealthyId(String healthyId) {
                    this.healthyId = healthyId;
                }

                public String getHealthyStaus() {
                    return healthyStaus;
                }

                public void setHealthyStaus(String healthyStaus) {
                    this.healthyStaus = healthyStaus;
                }

                public int getCdTimeVersion() {
                    return cdTimeVersion;
                }

                public void setCdTimeVersion(int cdTimeVersion) {
                    this.cdTimeVersion = cdTimeVersion;
                }

                public String getDataSource() {
                    return dataSource;
                }

                public void setDataSource(String dataSource) {
                    this.dataSource = dataSource;
                }
            }

            public static class XcgjBean {
                /**
                 * date : 2021-08-11 17:19:54
                 * stopoverCountyName :
                 * pathStatus : 1
                 * fxdj : 1
                 * stopoverCity : 350100
                 * id : 2021-08-117942a9fb1ec6008ec5cd1a788160780195ccee5874e26602c21f922a43af754e
                 * stopoverCityName : 福建省福州市
                 * stopoverCounty :
                 */

                private String date;
                private String stopoverCountyName;
                private String pathStatus;
                private String fxdj;
                private String stopoverCity;
                private String id;
                private String stopoverCityName;
                private String stopoverCounty;

                public String getDate() {
                    return date;
                }

                public void setDate(String date) {
                    this.date = date;
                }

                public String getStopoverCountyName() {
                    return stopoverCountyName;
                }

                public void setStopoverCountyName(String stopoverCountyName) {
                    this.stopoverCountyName = stopoverCountyName;
                }

                public String getPathStatus() {
                    return pathStatus;
                }

                public void setPathStatus(String pathStatus) {
                    this.pathStatus = pathStatus;
                }

                public String getFxdj() {
                    return fxdj;
                }

                public void setFxdj(String fxdj) {
                    this.fxdj = fxdj;
                }

                public String getStopoverCity() {
                    return stopoverCity;
                }

                public void setStopoverCity(String stopoverCity) {
                    this.stopoverCity = stopoverCity;
                }

                public String getId() {
                    return id;
                }

                public void setId(String id) {
                    this.id = id;
                }

                public String getStopoverCityName() {
                    return stopoverCityName;
                }

                public void setStopoverCityName(String stopoverCityName) {
                    this.stopoverCityName = stopoverCityName;
                }

                public String getStopoverCounty() {
                    return stopoverCounty;
                }

                public void setStopoverCounty(String stopoverCounty) {
                    this.stopoverCounty = stopoverCounty;
                }
            }
        }
    }


//    /**
//     * healthMsg : {"head":{"message":"接口调用成功","status":"0"},"data":{"yqReports":[],"yqWarn":{"warnType":"无"},"healthyReport":[{"date":"2021-08-11","healthyId":"135-f25b761e90898af566bb6c04dff234f4bcf0e92a1d78752fb89333a2a425b6b2","healthyStaus":"00","cdTimeVersion":96,"dataSource":"福建省"}],"hsjc":{},"person":{"desensitizeCardId":"**************0379","desensitizeName":"李*"},"xcgj":[]}}
//     */
//
//    private HealthMsgBean healthMsg;
//
//    public HealthMsgBean getHealthMsg() {
//        return healthMsg;
//    }
//
//    public void setHealthMsg(HealthMsgBean healthMsg) {
//        this.healthMsg = healthMsg;
//    }
//
//    public static class HealthMsgBean {
//        /**
//         * head : {"message":"接口调用成功","status":"0"}
//         * data : {"yqReports":[],"yqWarn":{"warnType":"无"},"healthyReport":[{"date":"2021-08-11","healthyId":"135-f25b761e90898af566bb6c04dff234f4bcf0e92a1d78752fb89333a2a425b6b2","healthyStaus":"00","cdTimeVersion":96,"dataSource":"福建省"}],"hsjc":{},"person":{"desensitizeCardId":"**************0379","desensitizeName":"李*"},"xcgj":[]}
//         */
//
//        private HeadBean head;
//        private DataBean data;
//
//        public HeadBean getHead() {
//            return head;
//        }
//
//        public void setHead(HeadBean head) {
//            this.head = head;
//        }
//
//        public DataBean getData() {
//            return data;
//        }
//
//        public void setData(DataBean data) {
//            this.data = data;
//        }
//
//        public static class HeadBean {
//            /**
//             * message : 接口调用成功
//             * status : 0
//             */
//
//            private String message;
//            private String status;
//
//            public String getMessage() {
//                return message;
//            }
//
//            public void setMessage(String message) {
//                this.message = message;
//            }
//
//            public String getStatus() {
//                return status;
//            }
//
//            public void setStatus(String status) {
//                this.status = status;
//            }
//        }
//
//        public static class DataBean {
//            /**
//             * yqReports : []
//             * yqWarn : {"warnType":"无"}
//             * healthyReport : [{"date":"2021-08-11","healthyId":"135-f25b761e90898af566bb6c04dff234f4bcf0e92a1d78752fb89333a2a425b6b2","healthyStaus":"00","cdTimeVersion":96,"dataSource":"福建省"}]
//             * hsjc : {}
//             * person : {"desensitizeCardId":"**************0379","desensitizeName":"李*"}
//             * xcgj : []
//             */
//
//            private YqWarnBean yqWarn;
//            private HsjcBean hsjc;
//            private PersonBean person;
//            private List<?> yqReports;
//            private List<HealthyReportBean> healthyReport;
//            private List<?> xcgj;
//
//            public YqWarnBean getYqWarn() {
//                return yqWarn;
//            }
//
//            public void setYqWarn(YqWarnBean yqWarn) {
//                this.yqWarn = yqWarn;
//            }
//
//            public HsjcBean getHsjc() {
//                return hsjc;
//            }
//
//            public void setHsjc(HsjcBean hsjc) {
//                this.hsjc = hsjc;
//            }
//
//            public PersonBean getPerson() {
//                return person;
//            }
//
//            public void setPerson(PersonBean person) {
//                this.person = person;
//            }
//
//            public List<?> getYqReports() {
//                return yqReports;
//            }
//
//            public void setYqReports(List<?> yqReports) {
//                this.yqReports = yqReports;
//            }
//
//            public List<HealthyReportBean> getHealthyReport() {
//                return healthyReport;
//            }
//
//            public void setHealthyReport(List<HealthyReportBean> healthyReport) {
//                this.healthyReport = healthyReport;
//            }
//
//            public List<?> getXcgj() {
//                return xcgj;
//            }
//
//            public void setXcgj(List<?> xcgj) {
//                this.xcgj = xcgj;
//            }
//
//            public static class YqWarnBean {
//            }
//
//            public static class HsjcBean {
//                private String hsjcsj;
//
//                public String getHsjcjg() {
//                    return hsjcjg;
//                }
//
//                public void setHsjcjg(String hsjcjg) {
//                    this.hsjcjg = hsjcjg;
//                }
//
//                private String hsjcjg;
//
//                public String getHsjcjgmc() {
//                    return hsjcjgmc;
//                }
//
//                public void setHsjcjgmc(String hsjcjgmc) {
//                    this.hsjcjgmc = hsjcjgmc;
//                }
//
//                private String hsjcjgmc;
//
//                public String getHsjcsj() {
//                    return hsjcsj;
//                }
//
//                public void setHsjcsj(String hsjcsj) {
//                    this.hsjcsj = hsjcsj;
//                }
//            }
//
//            public static class PersonBean {
//                /**
//                 * desensitizeCardId : **************0379
//                 * desensitizeName : 李*
//                 */
//
//                private String desensitizeCardId;
//                private String desensitizeName;
//
//                public String getDesensitizeCardId() {
//                    return desensitizeCardId;
//                }
//
//                public void setDesensitizeCardId(String desensitizeCardId) {
//                    this.desensitizeCardId = desensitizeCardId;
//                }
//
//                public String getDesensitizeName() {
//                    return desensitizeName;
//                }
//
//                public void setDesensitizeName(String desensitizeName) {
//                    this.desensitizeName = desensitizeName;
//                }
//            }
//
//            public static class HealthyReportBean {
//                /**
//                 * date : 2021-08-11
//                 * healthyId : 135-f25b761e90898af566bb6c04dff234f4bcf0e92a1d78752fb89333a2a425b6b2
//                 * healthyStaus : 00
//                 * cdTimeVersion : 96
//                 * dataSource : 福建省
//                 */
//
//                private String date;
//                private String healthyId;
//                private String healthyStaus;
//                private int cdTimeVersion;
//                private String dataSource;
//
//                public String getDate() {
//                    return date;
//                }
//
//                public void setDate(String date) {
//                    this.date = date;
//                }
//
//                public String getHealthyId() {
//                    return healthyId;
//                }
//
//                public void setHealthyId(String healthyId) {
//                    this.healthyId = healthyId;
//                }
//
//                public String getHealthyStaus() {
//                    return healthyStaus;
//                }
//
//                public void setHealthyStaus(String healthyStaus) {
//                    this.healthyStaus = healthyStaus;
//                }
//
//                public int getCdTimeVersion() {
//                    return cdTimeVersion;
//                }
//
//                public void setCdTimeVersion(int cdTimeVersion) {
//                    this.cdTimeVersion = cdTimeVersion;
//                }
//
//                public String getDataSource() {
//                    return dataSource;
//                }
//
//                public void setDataSource(String dataSource) {
//                    this.dataSource = dataSource;
//                }
//            }
//        }
//    }

}
