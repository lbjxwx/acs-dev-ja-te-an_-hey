package com.card.model;

import android.util.Log;

import com.alibaba.fastjson.JSON;
import com.lzy.okgo.callback.StringCallback;
import com.lzy.okgo.model.Response;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

public abstract class jkmCallback<T> extends StringCallback {
    String TAG="callback:";
    public Class<T> entityClass;

    public jkmCallback() {
        Type type = this.getClass().getGenericSuperclass();
        entityClass = (Class<T>) ((ParameterizedType) type).getActualTypeArguments()[0];
    }

    @Override
    public void onSuccess(Response<String> response) {
        String s1=response.body();
        try {
            BaseResponse<T> result = JSON.parseObject(s1, BaseResponse.class);
            String code = result.getCode();
            if (code.equals("0")) {
                String msg = result.getMsg();
                if (result.getData() != null) {
                    String data = result.getData().toString();
                    T t = JSON.parseObject(data, entityClass);
                    onSuccess(msg, t);
                } else {
                    onSuccess(msg, null);
                }
            } else {
                Log.e(TAG,"error result:" + result.getMsg() + " data:" + result.getData());
                onFail(code, result.getMsg());
            }


        } catch (Exception e) {
            e.printStackTrace();
            //Log.e("error=",s1);
           Log.e(TAG,"onresponse:解析出错");
            try {

                JSONObject jsonObject = new JSONObject();
                String msg = jsonObject.getString("msg");
                onFail("-1", msg);
            } catch (JSONException e1) {
                e1.printStackTrace();
            }

        }
    }

    @Override
    public void onError(Response<String> response) {
        super.onError(response);
        onFail(response.code() + "","网络请求失败，请重试");
    }



    public abstract void onSuccess(String msg, T data);

    public abstract void onFail(String code, String msg);
}
