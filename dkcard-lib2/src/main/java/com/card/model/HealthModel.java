package com.card.model;

import android.content.Context;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.card.Const;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.model.HttpHeaders;


import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;

import cn.anicert.polymerizeDevice.DeviceManager;


public class HealthModel {
    private static  final String TAG="MainViewModel";
    public String gModSn=""; //模组sn
    int MAX_RANGE=9999;
    Bussiness bussiness = new Bussiness();
    String gBsn,gRandomNum;
    private static final String PUBLIC_KEY_VER="V1.0";//公钥版本
    private static final String PUBLIC_KEY =
            "3149B557D6E101D02D4FB3D67DEDB33DDE32484EC87EA4BDE5C250E7AFE164FE1068F7DA5C35D99317D92F6EEAE63CB153B442F69264D38FD4439EF2FCC28C62"; //生产公钥
    String result_IdentityChiper="";//身份码核验第一包请求返回的密文
    
    private DeviceManager deviceManager;
    private Handler handler = new Handler();
    private OnModelCallback callback;

    public HealthModel(){

    }

    //0、初始化端口 成功返回模组sn，失败返回错误代码
    public void initModel(DeviceManager manager, OnModelCallback callback){
        this.callback = callback;
        this.deviceManager = manager;
        gModSn = manager.getModSn();
    }

    //请求头部加入网关校验密文（公用方法）
    private void makeHeader(String gwAuthinfo){
        OkGo.getInstance()
                .addCommonHeaders(new HttpHeaders("checkdata",gwAuthinfo))
                .addCommonHeaders(new HttpHeaders("Content-Type","application/json;charset=utf-8"));
    }
    //组装请求bsn数据实体（公用方法）
    public void requestBsn() {
        int rnd = ThreadLocalRandom.current().nextInt(1000, MAX_RANGE);
        String time= bussiness.getUnitTime();
        if (time.length()==0){
            Log.e(TAG,"获取unixtime错误");
            return;
        }
        Log.e(TAG,gModSn +"  "+ time  + " "+ String.valueOf(rnd));
        String gwAuthInfo = deviceManager.deviceToGWIdentityAuthInfo(gModSn, String.valueOf(rnd), time);
        if (!TextUtils.isEmpty(gwAuthInfo)) {
            requestBsn _requestBsn = new requestBsn();
            _requestBsn.setBizType(7003);
            requestBsn.BizDataBean _bizdatabean = new requestBsn.BizDataBean();
            _bizdatabean.setAuthMode("0x42");
            _requestBsn.setBizData(_bizdatabean);
            String body = JSON.toJSONString(_requestBsn);
            getBsn(gwAuthInfo, body, new jkmCallback<responseBsn>() {
                @Override
                public void onSuccess(String msg, responseBsn data) {
                    try {
                        gBsn = data.getBsn();
                        gRandomNum = data.getRandomNumber();
                        if(result_IdentityChiper.length()>0){
                            result_IdentityChiper = deviceManager.deIdentityChiperAndGetCheckData(
                            result_IdentityChiper,PUBLIC_KEY,PUBLIC_KEY.length(),PUBLIC_KEY_VER,gRandomNum);
                            if (callback != null) {
                                handler.post(()->callback.callback(new MessageEvent(JSON.toJSONString(data), 4)));
                            }
                        } else{
                            if (callback != null) {
                                handler.post(()->callback.callback(new MessageEvent(JSON.toJSONString(data), 5)));
                            }
                        }

                    } catch (Exception ex) {
                        Log.e(TAG, ex.getMessage());
                        if (callback != null) {
                            handler.post(()->callback.callback(new MessageEvent(JSON.toJSONString(data), 0)));
                        }
                    }
                }
                @Override
                public void onFail(String code, String msg) {
                    if (callback != null) {
                        handler.post(()->callback.callback(new MessageEvent(code + " " + msg, -1)));
                    }
                }
            });
        } else{
            if (callback != null) {
                handler.post(()->callback.callback(new MessageEvent("生成网关鉴权密文失败",0)));
            }
        }
    }
    //获取bsn（公用方法）
    public void getBsn(String gwAuthinfo,String body,jkmCallback jkmCallback){
        String url = Const.serverUrl+Const.applyUrl;
        makeHeader(gwAuthinfo);
        OkGo.post(url).tag(this).upJson(body).execute(jkmCallback);
    }
    //1、身份两要素请求健康信息-生成身份信息签名密文
    public void requestHealthByIdAndName(String name,String id){
        int rnd = ThreadLocalRandom.current().nextInt(1000, MAX_RANGE);
        String gwAuthInfo = this.deviceManager.deviceToGWIdentityAuthInfo(gModSn, String.valueOf(rnd), bussiness.getUnitTime());
        JSONObject checkData = null;
        try {
            checkData = this.deviceManager.getIdentityCheckData(
                    //姓名
                    name,
                    //身份证号码
                    id ,
                    //公钥
                    PUBLIC_KEY,
                    //公钥版本号
                    PUBLIC_KEY_VER,
                    //服务端返回的随机数
                    gRandomNum
            );
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (!TextUtils.isEmpty(gwAuthInfo)) {
            getHealthReportByIdAndName(gwAuthInfo,checkData);
        }
        else{
            if (callback != null) {
                handler.post(()->callback.callback(new MessageEvent("用身份信息获取健康信息失败",0)));
            }
        }
    }
    //1.1 组装身份两要素请求健康信息body
    public void getHealthReportByIdAndName(String gwAuthInfo,JSONObject checkData){
        requestHealthByIdAndName _reqHealth=new requestHealthByIdAndName();
        _reqHealth.setRequestIdentityData("jkbg,xcgj,hsjc");//健康报告，行程轨迹，核酸检测
        _reqHealth.setBizType("7003");
        _reqHealth.setBsn(gBsn);
        requestHealthByIdAndName.BizDataBean _bizdata=new requestHealthByIdAndName.BizDataBean();
        _bizdata.setAuthMode("0x42");
        _bizdata.setRandomNumber(gRandomNum);
        requestHealthByIdAndName.BizDataBean.IdentityCheckDataBean _identity=JSON.parseObject(checkData.toJSONString(),requestHealthByIdAndName.BizDataBean.IdentityCheckDataBean.class);
        _bizdata.setIdentityCheckData(_identity);
        _reqHealth.setBizData(_bizdata);
        String body=JSON.toJSONString(_reqHealth);
        postRequestByIdAndName(gwAuthInfo, body, new jkmCallback<responseHealth>() {
            @Override
            public void onSuccess(String msg, responseHealth data) {
                try {
                    if (callback != null) {
                        handler.post(()->callback.callback(new MessageEvent(JSON.toJSONString(data), 1)));
                    }
                } catch (Exception ex) {
                    Log.e(TAG, ex.getMessage());
                    if (callback != null) {
                        handler.post(()->callback.callback(new MessageEvent(JSON.toJSONString(data), 0)));
                    }
                }
            }
            @Override
            public void onFail(String code, String msg) {
                if (callback != null) {
                    handler.post(()->callback.callback(new MessageEvent(code + " " + msg, -1)));
                }
            }
        });
    }


    //1.2 用身份两要素密文请求健康报告
    public void postRequestByIdAndName(String gwAuthinfo, String body, jkmCallback jkmCallback){
        String url=Const.serverUrl+Const.healthByIdAndName;
        makeHeader(gwAuthinfo);
        OkGo.post(url).tag(this).upJson(body).execute(jkmCallback);
    }



    //2、国康码核验
    public void requestHealthByQrCode(JSONObject _cd, String strTempture, String strPosition){
        Log.e("JSONObject _cd",_cd.toString());
        int rnd = ThreadLocalRandom.current().nextInt(1000, MAX_RANGE);
        String time=bussiness.getUnitTime();
        if (time.length()==0){
            Log.e(TAG,"获取unixtime错误");
            return;
        }
        Log.e(TAG,gModSn +"  "+ time  + " "+ String.valueOf(rnd));
        String gwAuthInfo = this.deviceManager.deviceToGWIdentityAuthInfo(gModSn, String.valueOf(rnd), time);
        requestHealthByQrCode _reqHealthByQrCode=new requestHealthByQrCode();
        _reqHealthByQrCode.setRequestIdentityData("jkbg,xcgj,hsjc");
        _reqHealthByQrCode.setBizType("7001");
        requestHealthByQrCode.BizDataBean _bizdata=new requestHealthByQrCode.BizDataBean();
        _bizdata.setFirmMark("QZ");//按实际填写厂家代码
        _bizdata.setBodyTemperature(strTempture);//实际测温数据
        _bizdata.setPosition(strPosition);//实测经纬度或poi信息
        _bizdata.setAuthMode("V033");
        _bizdata.setCheckData(_cd);
        _reqHealthByQrCode.setBizData(_bizdata);
        String body=JSON.toJSONString(_reqHealthByQrCode);
        Log.e("body",body);
        getHealthByQrCode(gwAuthInfo, body, new jkmCallback<responseHealth>() {
            @Override
            public void onSuccess(String msg, responseHealth data) {
                try {
                    if (callback != null) {
                        handler.post(()->callback.callback(new MessageEvent(JSON.toJSONString(data), 1)));
                    }
                } catch (Exception ex) {
                    Log.e(TAG, ex.getMessage());
                    if (callback != null) {
                        handler.post(()->callback.callback(new MessageEvent(JSON.toJSONString(data), 0)));
                    }
                }
            }
            @Override
            public void onFail(String code, String msg) {
                if (callback != null) {
                    handler.post(()->callback.callback(new MessageEvent(JSON.toJSONString(code + " " + msg), -1)));
                }
            }
        });
    }
    //2.1 用国康码请求健康信息
    public void getHealthByQrCode(String gwAuthinfo,String body,jkmCallback jkmCallback){
        String url=Const.serverUrl+Const.JkmQrDataUrl;
        makeHeader(gwAuthinfo);
        OkGo.post(url).tag(this).upJson(body).execute(jkmCallback);
    }


    //3、CTID身份码核验
    public void requestQrValidate(String qrdata){
        int rnd = ThreadLocalRandom.current().nextInt(1000, MAX_RANGE);
        String gwAuthInfo = this.deviceManager.deviceToGWIdentityAuthInfo(gModSn, String.valueOf(rnd), bussiness.getUnitTime());
        if (!TextUtils.isEmpty(gwAuthInfo)) {
            requestByQrGetIdentityChiper _req = new requestByQrGetIdentityChiper();
            _req.setCodeContent(qrdata);
            _req.setPhotoData("");
            _req.setAuthMode("V000");
            _req.setCtidAppId("01234001");
            _req.setRequestId(UUID.randomUUID().toString());
            _req.setRequestIdentityData("name,number");
            String body = JSON.toJSONString(_req);
            getIdentityChiperByQrdata(gwAuthInfo, body, new jkmCallback<responseHealthByQrCode>() {
                @Override
                public void onSuccess(String msg, responseHealthByQrCode data) {
                    result_IdentityChiper=data.getIdentityInfo();
                    if (callback != null) {
                        handler.post(()->callback.callback(new MessageEvent(result_IdentityChiper ,3)));
                    }
                }
                @Override
                public void onFail(String code, String msg) {
                    if (callback != null) {
                        handler.post(()->callback.callback(new MessageEvent(code + " " + msg, -1)));
                    }
                }
            });
        }
        else{
            if (callback != null) {
                handler.post(()->callback.callback(new MessageEvent("生成网关鉴权密文失败",0)));
            }
        }
    }
    //3.1 用身份码请求身份密文
    public void getIdentityChiperByQrdata(String gwAuthinfo,String body,jkmCallback jkmCallback){
        String url=Const.serverUrl+Const.CtidQrDataUrl;
        makeHeader(gwAuthinfo);
        OkGo.post(url).tag(this).upJson(body).execute(jkmCallback);
    }
    //3.2 用CTID身份码请求健康报告
    public void getHealthReportByCtidQrCode(){
        int rnd = ThreadLocalRandom.current().nextInt(1000, MAX_RANGE);
        String gwAuthInfo = this.deviceManager.deviceToGWIdentityAuthInfo(gModSn, String.valueOf(rnd), bussiness.getUnitTime());
        requestHealthByIdAndName _reqHealth=new requestHealthByIdAndName();
        _reqHealth.setRequestIdentityData("jkbg,xcgj,hsjc");//健康报告，行程轨迹，核酸检测
        _reqHealth.setBizType("7003");
        _reqHealth.setBsn(gBsn);
        requestHealthByIdAndName.BizDataBean _bizdata=new requestHealthByIdAndName.BizDataBean();
        _bizdata.setAuthMode("0x42");
        _bizdata.setRandomNumber(gRandomNum);
        requestHealthByIdAndName.BizDataBean.IdentityCheckDataBean _identity=JSON.parseObject(result_IdentityChiper,requestHealthByIdAndName.BizDataBean.IdentityCheckDataBean.class);
        _bizdata.setIdentityCheckData(_identity);
        _reqHealth.setBizData(_bizdata);
        String body=JSON.toJSONString(_reqHealth);
        postRequestByIdAndName(gwAuthInfo, body, new jkmCallback<responseHealth>() {
            @Override
            public void onSuccess(String msg, responseHealth data) {
                try {
                    result_IdentityChiper="";
                    if (callback != null) {
                        handler.post(()->callback.callback(new MessageEvent(JSON.toJSONString(data), 1)));
                    }
                } catch (Exception ex) {
                    result_IdentityChiper="";
                    Log.e(TAG, ex.getMessage());
                    if (callback != null) {
                        handler.post(()->callback.callback(new MessageEvent(JSON.toJSONString(data), 0)));
                    }
                }
            }
            @Override
            public void onFail(String code, String msg) {
                result_IdentityChiper="";
                if (callback != null) {
                    handler.post(()->callback.callback(new MessageEvent(code + " " + msg, -1)));
                }
            }
        });

    }

}
