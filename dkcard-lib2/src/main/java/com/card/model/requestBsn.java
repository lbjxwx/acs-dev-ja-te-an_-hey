package com.card.model;

import java.io.Serializable;

public class requestBsn implements Serializable {


    /**
     * bizType : 7003
     * bizData : {"authMode":"0x42"}
     */

    private int bizType;
    private BizDataBean bizData;

    public int getBizType() {
        return bizType;
    }

    public void setBizType(int bizType) {
        this.bizType = bizType;
    }

    public BizDataBean getBizData() {
        return bizData;
    }

    public void setBizData(BizDataBean bizData) {
        this.bizData = bizData;
    }

    public static class BizDataBean implements Serializable {
        /**
         * authMode : 0x42
         */

        private String authMode;

        public String getAuthMode() {
            return authMode;
        }

        public void setAuthMode(String authMode) {
            this.authMode = authMode;
        }
    }
}
