package com.card.model;

public class MessageEvent {
    private String message;
    private int msgClass;
    public MessageEvent(String msg, int msgClass){
        this.message=msg;
        this.msgClass=msgClass;
    }


    public String getMessage() {
        return message;
    }
    public int getMsgClass(){
        return  msgClass;
    }

    public void setMessage(String message,int msgClass) {
        this.message = message;
        this.msgClass= msgClass;
        return;
    }
}
