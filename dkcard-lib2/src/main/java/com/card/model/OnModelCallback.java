package com.card.model;

public interface OnModelCallback {
    void callback(MessageEvent event);
}
