package com.card.model;

import java.io.Serializable;

public class responseHealthByQrCode implements Serializable {


    /**
     * bid :
     * bizData :
     * identityInfo :
     */

    private String bid;
    private String bizData;
    private String identityInfo;

    public String getBid() {
        return bid;
    }

    public void setBid(String bid) {
        this.bid = bid;
    }

    public String getBizData() {
        return bizData;
    }

    public void setBizData(String bizData) {
        this.bizData = bizData;
    }

    public String getIdentityInfo() {
        return identityInfo;
    }

    public void setIdentityInfo(String identityInfo) {
        this.identityInfo = identityInfo;
    }
}
