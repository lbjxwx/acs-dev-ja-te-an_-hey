package com.card.model;

import java.io.Serializable;

public class responseBsn implements Serializable {


    /**
     * randomNumber : lwZwKVnTsxWNI4Jv5qH1WdKxw1BG7HsZkBw0wXalHFPC6ZnZ6ouUTWwEKJ8jFt6cnmNDR8WdCcMYSNnsIXZgQhpAhr0gTAsigwwIrOi88xZkDAJE13G6pf4D
     * bsn : 023E3E980000009D000179C12203AFC4
     */

    private String randomNumber;
    private String bsn;

    public String getRandomNumber() {
        return randomNumber;
    }

    public void setRandomNumber(String randomNumber) {
        this.randomNumber = randomNumber;
    }

    public String getBsn() {
        return bsn;
    }

    public void setBsn(String bsn) {
        this.bsn = bsn;
    }
}
