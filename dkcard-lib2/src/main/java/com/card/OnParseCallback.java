package com.card;

public interface OnParseCallback {
    void onParse(String name, String cardNo);
    void onFailure(String errorMsg);
}
