package com.card;


public interface OnCardListener {
    void onStart();
    void onRetry();
    void onCardLeave();
    void onCardInfo(CardInfo cardData);
    void onCardFailure(String errorMsg);
    void onInitError(String error);
}
