package com.card;

import android.graphics.Bitmap;

public class CardInfo {
    public static final int TYPE_BUS = 1; //公交卡
    public static final int TYPE_IDCARD = 2;//身份证

    public static final String RCCARD_STUDENT = "0300";
    public static final String RCCARD_OLDMENT = "0800";

    public int type;
    public String rcType;
    public String sex;
    public String name;
    public String cardNo;
    public Bitmap photo;
}
