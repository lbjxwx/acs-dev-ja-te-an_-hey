package com.card;

import android.util.Log;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.card.Tool.StringTool;
import com.card.api.ServerApi;
import com.device.PageTool;
import com.ys.http.api.ApiService;
import com.ys.http.network.Body;
import com.ys.http.network.Callback;
import com.ys.http.network.Header;
import com.ys.http.network.HttpHelper;
import com.ys.http.network.HttpUtils;
import com.ys.http.network.SchedulerProvider;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import cn.hutool.core.codec.Base64;
import cn.hutool.crypto.symmetric.SM4;
import cn.hutool.crypto.symmetric.SymmetricCrypto;
import io.reactivex.rxjava3.annotations.NonNull;
import okhttp3.Call;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;

public class CardHelper {

    private final SymmetricCrypto sm4 = new SM4("CFB", "NoPadding", hexStringToBytes(Const.ENCRYPT_KEY), hexStringToBytes(Const.ENCRYPT_IV));//初始化加密对象;//SM4对象

    static {
        //HttpHelper.setShowHttpLog(true);
    }

    /**
     * 通过公交卡去获取身份证信息
     */
    public void getIdentityByBusCardId(String url, String cardId, String strSN, String address, OnParseCallback callback) {
        //构造json数据
        JSONObject jsonObjectBizData = new JSONObject();
        jsonObjectBizData.put("position", (address != null ? address : ""));
        jsonObjectBizData.put("sn", strSN);
        jsonObjectBizData.put("cardId", cardId);

        //初始化json对象
        JSONObject jsonObjectSend = new JSONObject();
        jsonObjectSend.put("bizType", "1001");
        jsonObjectSend.put("bizData", jsonObjectBizData);
        Log.i("json数据格式", jsonObjectSend.toString());

        // 请求header参数
        String random = UUID.randomUUID().toString();
        String curTime = String.valueOf(System.currentTimeMillis() / 1000L);
        String checkSum = CheckSumBuilder.getCheckSum(Const.APP_SECRET, random, curTime);

        // 加密请求数据
        //Log.i(TAG,"客户端发送未加密数据：" + requestData);
        String requestData= sm4.encryptBase64(jsonObjectSend.toString());//数据加密
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), requestData);

        Map<String, String> headers = new HashMap<>();
        headers.put("AppKey", Const.APP_KEY);
        headers.put("Random", random);
        headers.put("CurTime", curTime);
        headers.put("Sign", checkSum);
        HttpUtils.createApi(ServerApi.class)
                .post(url, headers, requestBody)
                .compose(SchedulerProvider.applyScheduler())
                .subscribe(new Callback<ResponseBody>() {
                    @Override
                    public void onSuccess(ResponseBody body) {
                        try {
                            String str = sm4.decryptStr(body.string());
                            System.out.println("response: " + str);

                            JSONObject jsonObject = JSON.parseObject(str);
                            if (!jsonObject.getString("code").equals("0")) {
                                //SendResultToUI(PageTool.MESSAGE_RETURN, jsonObjectResultAlly.getString("msg"));
                                if (callback != null) {
                                    callback.onFailure(jsonObject.getString("msg"));
                                }
                            } else {
                                String strMsgChiper = jsonObject.getJSONObject("data").getString("userMsg");//Log.i(TAG, "userMsg:" + strMsgChiper);//取出数据
                                String strMsgClear = decryptByAes(strMsgChiper, Const.AES_KEY, Const.AES_IV);//Log.i(TAG, "strMsgClear:" + strMsgClear);//解密
                                JSONObject identity = JSON.parseObject(strMsgClear);
                                if (callback != null) {
                                    callback.onParse(identity.getString("identityName"), identity.getString("identityId"));
                                }
                                //{"identityName":"姓名","identityId":"56265689659878554"}
                                System.out.println("json---:  " + identity.toJSONString());
                            }


                        } catch (Exception e) {
                            e.printStackTrace();
                            if (callback != null) {
                                callback.onFailure("数据解密出错");
                            }
                            //SendResultToUI(PageTool.MESSAGE_RETURN,"数据解密出错");
                            //Log.i(TAG,"解密前数据：" + strChiperData);
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        if (callback != null) {
                            callback.onFailure("网络异常数据解密失败");
                        }
                    }
                });
    }

    /**
     * 解密
     * @param data
     * @param key
     * @return
     * @throws Exception
     */
    private static String decryptByAes(String data,String key,String iv) {
        try {
            byte[] encrypted1 = Base64.decode(data);;
            Cipher cipher = Cipher.getInstance("AES/CBC/NoPadding");
            SecretKeySpec keyspec = new SecretKeySpec(key.getBytes(), "AES");
            IvParameterSpec ivspec = new IvParameterSpec(iv.getBytes());
            cipher.init(Cipher.DECRYPT_MODE, keyspec, ivspec);
            byte[] original = cipher.doFinal(encrypted1);
            return new String(original);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("AES 解密发生异常",e.toString());
        }
        return null;
    }

    private static byte[] hexStringToBytes(String str) {
        if (str != null && !str.trim().equals("")) {
            byte[] bytes = new byte[str.length() / 2];

            for(int i = 0; i < str.length() / 2; ++i) {
                String subStr = str.substring(i * 2, i * 2 + 2);
                bytes[i] = (byte)Integer.parseInt(subStr, 16);
            }
            return bytes;
        } else {
            return new byte[0];
        }
    }
}
